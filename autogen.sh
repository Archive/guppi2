#!/bin/sh
# Run this to generate all the initial makefiles, etc.

echo " "
echo "*** This module is dead"
echo "*** Check out CVS module \"guppi3\" instead"
echo " "
exit

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Guppi"

(test -f $srcdir/configure.in \
  && test -d $srcdir/src \
  && test -f $srcdir/src/guppi.cc) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

. $srcdir/macros/autogen.sh
