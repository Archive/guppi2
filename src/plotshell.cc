// -*- C++ -*-

/* 
 * plotshell.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plotshell.h"
#include "guppi.h"
#include "plotlist.h"
#include "gdk/gdkkeysyms.h"

#include "libguppi/tool.h"
#include "libguppi/menuentry.h"
#include "libguppi/plotstore.h"

static void wait_for_gtk_idles();

struct zoom_info {
  double factor;
  const gchar* label;
};

static struct zoom_info zooms[] = {
  { .1, N_("1%") },
  { .25, N_("25%") },
  { .50, N_("50%") },
  { 1.0, N_("100%") },
  { 1.5, N_("150%") },
  { 2.0, N_("200%") },
  { 3.0, N_("300%") },
  { 5.0, N_("500%") },
  { 10.0, N_("1000%") }, 
  { 20.0, N_("2000%") },
  { 50.0, N_("5000%") },
  { 100.0, N_("10,000%") }
};

static const guint nzooms = sizeof(zooms)/sizeof(zooms[0]);

void 
PlotShell::zoom_cb(GtkWidget* w, gpointer data)
{
  zoom_cbdata* zcbd = static_cast<zoom_cbdata*>(data);

  g_return_if_fail(zcbd != 0);

  zcbd->ps->zoom(zooms[zcbd->index].factor);
}

void 
PlotShell::toolbox_cb(GtkWidget* w, gpointer data)
{
  PlotShell* ps = static_cast<PlotShell*>(data);

  ps->show_toolbox();
}

void 
PlotShell::autosize_cb(GtkWidget* w, gpointer data)
{
  if (data == 0)
    {
      // We set the toggle ourselves, it didn't go via gnome-popup-menu 
      return;
    }

  PlotShell* ps = static_cast<PlotShell*>(data);

  ps->set_snap_size_mode(GTK_CHECK_MENU_ITEM(w)->active);
}

void 
PlotShell::newview_cb(GtkWidget* w, gpointer data)
{
  PlotShell* ps = static_cast<PlotShell*>(data);

  // can't do this because it gets the size allocation too soon...
  //  PlotShell* newps = guppi_plot_shell_new(ps->plot()->new_view());

  PlotShell* newps = new PlotShell;

  // If you don't like sick hacks, don't look down.
  // Fixes welcome... I couldn't think of anything.

  // We want to retain the original plot's size, rather than sizing
  // the original plot to match the size of the new window.
  
  g_debug("newps: %p setting...", newps);

  newps->set_size_on_allocation_ = false;

  newps->set_plot(ps->plot()->new_view());
  guppi_plotlist()->add(newps);

  gtk_widget_show_now(newps->app_);

  // wait for all size allocations to take place,
  // and the new window to be entirely on-screen.
  while (!GTK_WIDGET_VISIBLE(newps->canvas_))
    {
      while (gtk_events_pending())
        gtk_main_iteration();
      wait_for_gtk_idles();
    }

  newps->set_size_on_allocation_ = true;

  g_debug("newps: %p unsetting...", newps);
}

static GnomeUIInfo empty_menu[] = {

  GNOMEUIINFO_END
};

static GnomeUIInfo popup_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Toolbox"), N_("Show tool box"), PlotShell::toolbox_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
#define ZOOM_SUBTREE 1
  GNOMEUIINFO_SUBTREE(N_("Scale"), empty_menu),
#define FIT_WINDOW_MENUITEM 2
  { GNOME_APP_UI_TOGGLEITEM, N_("Fit Plot to Window"), N_("Auto-size the plot to match the window"), PlotShell::autosize_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
  { GNOME_APP_UI_ITEM, N_("New View"), N_("Create a new window showing this same plot"), PlotShell::newview_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
  GNOMEUIINFO_END
};

PlotShell::PlotShell()
  : plot_(0), app_(0), canvas_(0), appbar_(0), 
    owner_(0), 
    snap_size_mode_(true), set_size_on_allocation_(true), 
    edge_(10.0), popup_(0)
{
  zcbdata_ = new zoom_cbdata[nzooms];

  app_ = gnome_app_new("guppi", _("Guppi - Plot"));

  guppi_setup_window(GTK_WINDOW(app_), "plot");

  appbar_ = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_NEVER);

  gnome_appbar_set_default(GNOME_APPBAR(appbar_), 
                           _("Right click on the plot for a menu"));

  gnome_app_set_statusbar(GNOME_APP(app_), appbar_);

  gtk_window_set_default_size(GTK_WINDOW(app_), 500, 400);

  gtk_signal_connect(GTK_OBJECT(app_),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     this);

  gtk_signal_connect(GTK_OBJECT(app_),
                     "key_press_event",
                     GTK_SIGNAL_FUNC(key_event_cb),
                     this);

  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  canvas_ = gnome_canvas_new_aa ();
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  gtk_signal_connect(GTK_OBJECT(canvas_),
                     "size_allocate",
                     GTK_SIGNAL_FUNC(allocation_cb),
                     this);

  popup_ = gnome_popup_menu_new(popup_menu);

  gnome_app_install_menu_hints(GNOME_APP(app_), popup_menu);

  // invokes a self-callback with NULL data, so we can ignore it.
  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(popup_menu[FIT_WINDOW_MENUITEM].widget),
                                 snap_size_mode_);

  
  GtkWidget* menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(popup_menu[ZOOM_SUBTREE].widget), menu);
  guint z = 0;
  while (z < nzooms)
    {
      GtkWidget* mi = gtk_menu_item_new_with_label(_(zooms[z].label));

      zcbdata_[z].index = z;
      zcbdata_[z].ps    = this;

      gtk_signal_connect(GTK_OBJECT(mi), "activate",
                         GTK_SIGNAL_FUNC(zoom_cb),
                         &zcbdata_[z]);

      gtk_menu_append(GTK_MENU(menu), mi);
      
      gtk_widget_show(mi);

      ++z;
    }

  gnome_popup_menu_attach(popup_, canvas_, this);

  GtkWidget* frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

  GtkWidget* sw = gtk_scrolled_window_new(NULL,NULL);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  gtk_container_add(GTK_CONTAINER(frame), sw);
  
  gtk_container_add(GTK_CONTAINER(sw), canvas_);

  gnome_app_set_contents(GNOME_APP(app_), frame);
}

PlotShell::~PlotShell()
{
  if (plot_) 
    {
      plot_->plot_model.remove_view(this);
      plot_->unrealize();
      if (plot_->unref() == 0)
        delete plot_;
    }
  if (app_) 
    gtk_widget_destroy(app_);

  delete [] zcbdata_;
}

static void
unref_menuentry(gpointer p)
{
  MenuEntry* me = static_cast<MenuEntry*>(p);

  if (me->unref() == 0)
    delete me;
}

void 
PlotShell::set_plot(Plot* p)
{
  g_return_if_fail(plot_ == 0);
  g_return_if_fail(p != 0);

  plot_ = p;

  plot_->ref();                         

  //  toolbox_.set_plot(plot_);

  GtkWidget* menu = popup_;

  const vector<MenuEntry*> entries = guppi_plot_store()->menuentries(plot_->id());

  vector<MenuEntry*>::const_iterator i = entries.begin();
  while (i != entries.end())
    {
      GtkWidget* mi = gtk_menu_item_new_with_label((*i)->name().c_str());

      (*i)->ref();

      gtk_object_set_data_full(GTK_OBJECT(mi), "MenuEntry", *i, unref_menuentry);

      gtk_signal_connect(GTK_OBJECT(mi), "activate",
                         GTK_SIGNAL_FUNC(menuentry_activate_cb),
                         this);

      g_debug("Adding menu entry %s", (*i)->name().c_str());

      gtk_menu_append(GTK_MENU(menu), mi);

      guppi_install_menuitem_hint(mi, GNOME_APPBAR(appbar_), (*i)->tooltip());

      gtk_widget_show(mi);

      ++i;
    }

  plot_->plot_model.add_view(this);

  plot_->realize(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas_)->root));

  change_size(plot_);
  change_name(plot_, plot_->name());

  gtk_widget_show_all(app_);
}

void 
PlotShell::menuentry_activate_cb(GtkWidget* mi, gpointer data)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(mi), "MenuEntry");

  PlotShell* ps = static_cast<PlotShell*>(data);

  g_return_if_fail(p != 0);
  g_return_if_fail(ps != 0);

  MenuEntry* me = static_cast<MenuEntry*>(p);

  me->invoke(ps->plot());
}

//////// 

void 
PlotShell::change_size(Plot* p)
{
  g_return_if_fail(p == plot_);

  g_debug("Shell %p got change size, %g x %g", this, plot_->width(), plot_->height());

  gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas_), -edge_,-edge_, plot_->width() + edge_, plot_->height() + edge_);
}

void 
PlotShell::change_status(Plot* p, const string& status)
{
  gnome_appbar_set_status(GNOME_APPBAR(appbar_), status.c_str());
}

void 
PlotShell::change_name(Plot* p, const string& name)
{
  const gchar* format = _("Guppi - %s");
  guint bufsize = name.size() + strlen(format) + 50;
  char* buf = new char[bufsize];
  g_snprintf(buf, bufsize, format, name.c_str());
  gtk_window_set_title(GTK_WINDOW(app_), buf);
  delete [] buf;
}

void 
PlotShell::destroy_model()
{
  plot_ = 0;  
#ifdef GNOME_ENABLE_DEBUG
  g_warning("Plot destroyed from underneath PlotShell!");
#endif
}

/////////

void 
PlotShell::set_plot_edge(double edge)
{
  edge_ = edge;

  if (canvas_ != 0)
    {
      allocation(&canvas_->allocation);
    }
}

void
PlotShell::zoom(double factor)
{
  g_return_if_fail(canvas_ != 0);

  gnome_canvas_set_pixels_per_unit(GNOME_CANVAS(canvas_), factor);
}

void
PlotShell::show_toolbox()
{
  g_return_if_fail(plot_ != 0);

  guppi_plot_store()->show_toolbox(plot_->id());
}

gint 
PlotShell::delete_event_cb(GtkWidget* w, GdkEventAny* event, gpointer data)
{
  PlotShell* ps = static_cast<PlotShell*>(data);

  return ps->delete_event();
}

gint 
PlotShell::delete_event()
{
  gtk_widget_destroy(app_);
  
  app_ = 0;

  if (owner_) 
    owner_->kill_me_please(this);

  return TRUE;
}

gint 
PlotShell::key_event_cb(GtkWidget* w, GdkEventKey* event, gpointer data)
{
  PlotShell* ps = static_cast<PlotShell*>(data);

  return ps->key(event);
}

gint 
PlotShell::key(GdkEventKey* event)
{
  switch (event->keyval)
    {
    case GDK_Escape:
      delete_event();
      return TRUE;
      break;
    default:
      break;
    }
  return FALSE;
}

void
PlotShell::allocation_cb(GtkWidget* w, GtkAllocation* a, gpointer data)
{
  PlotShell* ps = static_cast<PlotShell*>(data);

  ps->allocation(a);
}

// See allocation() below for the explanation...
static gint
low_priority_idle(gpointer data)
{
  bool* idle_has_run = static_cast<bool*>(data);

  *idle_has_run = true;

  return FALSE; // disconnect
}

static void
wait_for_gtk_idles()
{
  // we'd never terminate...
  g_return_if_fail(gtk_main_level() > 0);

  // We add an idle at a lower priority than the Gtk resize/redraw idles,
  //  then go until our idle has been run.
  
  bool ours_has_run = false;
  
  gtk_idle_add_priority(MAX(GTK_PRIORITY_REDRAW, GTK_PRIORITY_RESIZE)+10, 
                        low_priority_idle, &ours_has_run);
  
  while (!ours_has_run)
    {
      g_return_if_fail(gtk_main_level() > 0);
      gtk_main_iteration();
    }
}

void 
PlotShell::allocation(GtkAllocation* a)
{
  g_debug("%p got a size allocation, and set_size_on_allocation_ is %s",
          this, set_size_on_allocation_ ? "true" : "false");

  if (snap_size_mode_ && set_size_on_allocation_)
    {
      if (canvas_ != 0 && plot_ != 0)
        {

          // We have to do this locking because otherwise we get in an
          // infinite loop when there are two plotshells displaying
          // the same plot state object; (one allocates, which changes
          // the scroll region on the other when
          // Plot::View::change_size is called, which causes an
          // allocation on the other, which then changes the scroll
          // region on the first, etc.)

          // This is the cleanest solution I could come up with...

          if (plot_->get_resize_lock())
            {
              double iw, ih;
              
              // We fit to window at 100% scaling.
              iw = double(a->width);  //  / double(GNOME_CANVAS(canvas_)->pixels_per_unit);
              ih = double(a->height); //  / double(GNOME_CANVAS(canvas_)->pixels_per_unit);
              
              // Account for edges
              
              if (iw > edge_*2)
                {
                  iw -= edge_*2;
                }
              else 
                {
                  iw = 0.0;
                }
              
              if (ih > edge_*2)
                {
                  ih -= edge_*2;
                }
              else 
                {
                  ih = 0.0;
                }
              
              if (iw != plot_->width() || ih != plot_->height())
                {
                  // Results in a change_size() which sets the scroll region
                  plot_->set_size(iw, ih);
                }

              // If there are multiple views, size allocations have probably 
              //  been queued on the others; we have to clear out those idle functions.
              // This is a sick hack; but it's restricted to these few lines of 
              //  code. Other possible hacks would be much broader in scope 
              //  and probably not work anyway.
              
              wait_for_gtk_idles();

              plot_->release_resize_lock();
            }
        }
    }
}

void 
PlotShell::set_owner(PlotList* pl)
{
  owner_ = pl;
}



