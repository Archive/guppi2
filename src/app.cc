// -*- C++ -*-

/* 
 * app.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "app.h"
#include "guppidata.h"
#include "plotshell.h"
#include "libguppi/barplot.h"
#include "libguppi/xyplot.h"
#include "libguppi/pieplot.h"
#include "plotlist.h"
#include "guppixmltags.h"
#include "term.h"
#include "textimport.h"

#include <gnome-xml/parser.h>

#include <errno.h>

static void exit_cb(GtkWidget* w, gpointer data);
static void about_cb(GtkWidget* w, gpointer data);

#ifdef GNOME_ENABLE_DEBUG
static void debug_datasets_cb(GtkWidget* w, gpointer data)
{
  guppi_create_debug_datasets(DebugDataAll);
}
static void debug_basic_cb(GtkWidget* w, gpointer data)
{
  guppi_create_debug_datasets(DebugDataBasic);
}
static void debug_pie_cb(GtkWidget* w, gpointer data)
{
  guppi_create_debug_datasets(DebugDataPie);
}
static void debug_big_cb(GtkWidget* w, gpointer data)
{
  guppi_create_debug_datasets(DebugDataBig);
}
static void debug_price_cb(GtkWidget* w, gpointer data)
{
  guppi_create_debug_datasets(DebugDataPrice);
}
#endif

#ifdef GNOME_ENABLE_DEBUG
static GnomeUIInfo debug_menu[] = {
    { GNOME_APP_UI_ITEM, N_("Make All _Debug Datasets"), N_("Create some datasets for debugging and playing around"), debug_datasets_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, NULL, 
    'd', (GdkModifierType) GDK_CONTROL_MASK, NULL},
    { GNOME_APP_UI_ITEM, N_("Make Pie Chart Debug Datasets"), N_("Create some pie-chart-appropriate data for debugging and playing around"), debug_pie_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, NULL, 
    0, (GdkModifierType) 0, NULL},
    { GNOME_APP_UI_ITEM, N_("Make Big Debug Datasets"), N_("Create some large datasets for debugging and playing around"), debug_big_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, NULL, 
    0, (GdkModifierType) 0, NULL},
    { GNOME_APP_UI_ITEM, N_("Make Basic Debug Datasets"), N_("Create some basic data for debugging and playing around"), debug_basic_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, NULL, 
    0, (GdkModifierType) 0, NULL},
    { GNOME_APP_UI_ITEM, N_("Make Price Bar Debug Datasets"), N_("Create some price data for debugging and playing around"), debug_price_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, NULL, 
    0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_END
};
#endif

static GnomeUIInfo file_menu[] = {
  GNOMEUIINFO_MENU_OPEN_ITEM(App::open_cb,NULL),
  { GNOME_APP_UI_ITEM, N_("Text _Import"), N_("Load data from a text file"), App::import_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN, 
    0, (GdkModifierType) 0, NULL},
  GNOMEUIINFO_MENU_SAVE_ITEM(App::save_cb,NULL),
  GNOMEUIINFO_MENU_SAVE_AS_ITEM(App::save_as_cb,NULL),
  { GNOME_APP_UI_ITEM, N_("_Terminal"), N_("Open an interactive Guile terminal"), App::term_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
  GNOMEUIINFO_MENU_EXIT_ITEM(exit_cb,NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo plot_menu[] = {
  { GNOME_APP_UI_ITEM, N_("_Bar"), N_("Make a bar chart"), App::barplot_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
  { GNOME_APP_UI_ITEM, N_("_Pie"), N_("Make a pie chart"), App::pieplot_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
  { GNOME_APP_UI_ITEM, N_("_XY"), N_("Make an XY plot"), App::scatterplot_cb, NULL, NULL, \
    GNOME_APP_PIXMAP_NONE, 0, 0, (GdkModifierType) 0, NULL},
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_HELP((gpointer)"guppi"),
	GNOMEUIINFO_MENU_ABOUT_ITEM(about_cb,NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),
	GNOMEUIINFO_SUBTREE(N_("_Plot"), plot_menu),
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
#ifdef GNOME_ENABLE_DEBUG
	GNOMEUIINFO_SUBTREE(N_("_Debug"), debug_menu),
#endif
	GNOMEUIINFO_END
};


App::App()
  : app_(0), list_(guppi_datastore()), compression_(0)
{
  app_ = gnome_app_new("guppi", _("Guppi - Main Window"));

  guppi_setup_window(GTK_WINDOW(app_), "main_window");

  gnome_app_set_contents(GNOME_APP(app_), list_.widget());

  gnome_app_create_menus_with_data(GNOME_APP(app_), main_menu, this);

  gtk_window_set_default_size(GTK_WINDOW(app_), 200, 300);

  gtk_signal_connect(GTK_OBJECT(app_),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     this);

  term_ = new Term;

  gtk_widget_show_all(app_);
}

App::~App()
{
  if (term_ != 0) {
    delete term_;
    term_ = 0;
  }
  if (app_ != 0) gtk_widget_destroy(app_);
}


gint 
App::delete_event_cb(GtkWidget* w, GdkEventAny* event, gpointer data)
{
  App* a = static_cast<App*>(data);

  return a->delete_event();
}

gint 
App::delete_event()
{
  guppi_quit();

  return TRUE; // don't destroy the app_
}


void 
App::barplot_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);

  a->barplot();
}

void
App::barplot()
{
  guppi_plot_shell_new(new BarPlot(guppi_datastore())); 
}

void 
App::scatterplot_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);

  a->scatterplot();
}

void
App::scatterplot()
{
  guppi_plot_shell_new(new XyPlot(guppi_datastore())); 
}

void 
App::pieplot_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);
  
  a->pieplot();
}

void
App::pieplot()
{
  guppi_plot_shell_new(new PiePlot(guppi_datastore())); 
}



void 
App::import_cb(GtkWidget* w, gpointer data)
{
  guppi_gui_text_import();
}

void 
App::save_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);

  a->save();
}

void
App::save()
{
  if (filename_.empty())
    {
      save_as();
      return;
    }

  xmlDocPtr doc;

  doc = xmlNewDoc((const xmlChar*)"1.0");
  doc->root = xmlNewDocNode(doc, NULL, GUPPI_ROOT_TAG, NULL);

  xmlSetDocCompressMode(doc, compression_);

  // Save the compression so that the save as dialog has the same
  //  default next time we load.
  gchar buf[256];
  g_snprintf(buf, 255, "%d", compression_);

  xmlSetProp(doc->root, (const xmlChar*)"compression", (const xmlChar*)buf); 

  list_.xml(doc->root);

  if (xmlSaveFile(filename_.c_str(), doc) == -1)
    {
      // FIXME pop up a dialog, etc.
      g_warning("File save failed, %s", g_strerror(errno));
    }

  xmlFreeDoc(doc);
}

void 
App::open_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);

  a->open();
}

void
App::open()
{
  GtkWidget * dialog;
  
  dialog = gnome_dialog_new(_("Guppi: Open"), 
                            GNOME_STOCK_BUTTON_OK,
                            GNOME_STOCK_BUTTON_CANCEL,
                            NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog), TRUE);

  guppi_setup_dialog(dialog);

  GtkWidget* fileentry = gnome_file_entry_new("guppi:guppi_loadsave_history",
                                              _("Guppi: Browse Files For Open"));

  gnome_dialog_editable_enters(GNOME_DIALOG(dialog), 
                               GTK_EDITABLE(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(fileentry))));
  gnome_dialog_set_default(GNOME_DIALOG(dialog), GNOME_OK);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),
                     fileentry,
                     TRUE, TRUE, GNOME_PAD);
  

  gtk_widget_show_all(dialog);

  int reply = gnome_dialog_run(GNOME_DIALOG(dialog));
  
  if (reply == GNOME_OK)
    {      
      gchar* s = 
        gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(fileentry),
                                       TRUE);
      
      string file;

      if (s != 0)
        file = s;

      g_free(s);
      s = 0;

      string error;

      if (!file.empty())
        {
          xmlDocPtr doc = xmlParseFile(file.c_str());
          
          if (doc != 0 && doc->root != 0)
            {
              const gchar* t = (const gchar*)doc->root->name;
              if (t == 0 || (g_ustrcmp(t, GUPPI_ROOT_TAG) != 0))
                {
                  error = _("This does not look like a Guppi file.");
                }
              else
                {
                  const gchar* comp = (const gchar*)xmlGetProp(doc->root, (const xmlChar*)"compression");
                  if (comp)
                    {
                      compression_ = atoi(comp);
                    }

                  xmlNodePtr node = doc->root->childs;

                  while (node != 0)
                    {
                      t = (const gchar*)node->name;
                      if (t != 0)
                        {
                          if (g_ustrcmp(t, DATA_LIST_TAG) == 0)
                            {
                              list_.set_xml(node);
                            }
                          else 
                            {
                              gchar buf[256];
                              g_snprintf(buf, 255, 
                                         _("Did not understand tag %s\n"),
                                         t);
                              error += buf;
                            }
                        }
                      node = node->next;
                    }
                }
            }
          else 
            {
              error = _("Failed to understand this file. Perhaps it is corrupted\n"
                        "or not a Guppi file.");
            }
          
          if (doc) xmlFreeDoc(doc);
        }
      else 
        {
          error = _("No such file or no filename given.");
        }

      if (error.size() > 0)
        {
          gnome_error_dialog(error.c_str());
        }
    }
  
  gtk_widget_destroy(dialog);
}

void 
App::save_as_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);

  a->save_as();
}

void
App::save_as()
{
  // We get a filename and then save()

  GtkWidget * dialog;

  dialog = gnome_dialog_new(_("Guppi: Save As"), 
                            GNOME_STOCK_BUTTON_OK,
                            GNOME_STOCK_BUTTON_CANCEL,
                            NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog), TRUE);

  guppi_setup_dialog(dialog);

  GtkWidget* fileentry = gnome_file_entry_new("guppi:guppi_loadsave_history",
                                              _("Guppi: Browse Files For Save As"));

  gnome_dialog_editable_enters(GNOME_DIALOG(dialog), 
                               GTK_EDITABLE(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(fileentry))));
  gnome_dialog_set_default(GNOME_DIALOG(dialog), GNOME_OK);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),
                     fileentry,
                     TRUE, TRUE, GNOME_PAD);

  GtkWidget* hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
  GtkWidget* label    = gtk_label_new(_("Compression Level"));
  GtkWidget* compress = gtk_option_menu_new();

  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, GNOME_PAD);
  gtk_box_pack_end(GTK_BOX(hbox), compress, TRUE, TRUE, GNOME_PAD);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),
                     hbox,
                     TRUE, TRUE, GNOME_PAD);

  GtkWidget* menu = gtk_menu_new();

  // I getting so sick of GtkMenu not having a get_active_index()
  map<GtkWidget*, int> indices;

  int i = 0;
  while (i < 10)
    {
      gchar buf[256];
      if (i == 0)
        g_snprintf(buf, 255, "%s", _("None"));
      else
        g_snprintf(buf, 255, "%d", i);

      GtkWidget* mi = gtk_menu_item_new_with_label(buf);
      
      gtk_menu_append(GTK_MENU(menu), mi);

      indices[mi] = i;

      ++i;
    }

  gtk_option_menu_set_menu(GTK_OPTION_MENU(compress), menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(compress), compression_);

  gtk_widget_show_all(dialog);

  int reply = gnome_dialog_run(GNOME_DIALOG(dialog));
  
  if (reply == GNOME_OK)
    {
      GtkWidget* act = gtk_menu_get_active(GTK_MENU(menu));

      map<GtkWidget*,gint>::iterator j = indices.find(act);

      g_return_if_fail(j != indices.end());

      compression_ = j->second;
      
      gchar* s = 
        gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(fileentry),
                                       FALSE);

      if (s != 0)
        filename_ = s;
      else
        filename_ = "";

      g_free(s);
      s = 0;

      // Notice that save() will call this function again if filename_
      //  is empty.
      save();
    }
  
  gtk_widget_destroy(dialog);  
}


void 
App::term_cb(GtkWidget* w, gpointer data)
{
  App* a = static_cast<App*>(data);

  a->term();
}

void
App::term()
{
  term_->create_term();
}

void
guppi_about_dialog()
{
  const gchar * authors [] = { "Havoc Pennington <hp@pobox.com>",
                               "Jon Trowbridge <trow@emccta.com>",
			       NULL };

  GtkWidget* about = 
    gnome_about_new("Guppi: The GNU/GNOME Useful Plot Production Interface",
		    VERSION,
		    "Copyright (C) 1999 EMC Capital Management Inc.,"
		    " GNU General Public License.",
		    authors,
		    _("Guppi allows you to create several different kinds of plots "
		      "for data visualization. Please file bugs on http://bugs.gnome.org. Ask questions on guppi-list@gnome.org. For more information, see http://www.gnome.org/guppi/."),
		    "guppi.xpm" );

  gtk_widget_show(about);
}


static void exit_cb(GtkWidget* w, gpointer data)
{
  guppi_quit();
}

static void about_cb(GtkWidget* w, gpointer data)
{
  guppi_about_dialog();
}

