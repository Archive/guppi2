// -*- C++ -*-

/* 
 * datalist.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATALIST_H
#define GUPPI_DATALIST_H

#include "guppi.h"
#include "libguppi/datastore.h"
#include "dataedit.h"

#include <gnome-xml/tree.h>

class DataList : public DataStore::View {
public:
  DataList(DataStore* ds);
  virtual ~DataList();

  GtkWidget* widget();

  virtual void add_data(DataStore* ds, Data* d);
  virtual void remove_data(DataStore* ds, Data* d);
  virtual void change_data_values(DataStore* ds, Data* d);
  virtual void change_data_values(DataStore* ds, Data* d, const vector<guint> & which);
  virtual void change_data_name(DataStore* ds, Data* d, const string & name);
  virtual void destroy_model();

  void set_store(DataStore* ds);

  xmlNodePtr xml(xmlNodePtr parent) const;
  void set_xml(xmlNodePtr parent);
  
private:
  DataStore* store_;

  GtkWidget* sw_;
  GtkWidget* clist_;

  DataEdit edit_;

  void append_data(Data* d);

  void release_store();

  static void select_row_cb(GtkWidget* w, int row, int col, GdkEvent* event, gpointer data);
  void select_row(int row);

  void update_numbers(int row, Data* d);
};

#endif


