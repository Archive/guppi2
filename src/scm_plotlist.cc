// -*- C++ -*-

/* 
 * scm_plotlist.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/plot.h"
#include "plotlist.h"

#include "scm_guppi_gnome/scm_guppi_gnome.h"

///////////////////// Access to the Guppi PlotList

GUPPI_PROC(get_plots,"guppi-plots",0,0,0,())
{
  gh_defer_ints();

  vector<Plot*> plots;

  guppi_plotlist()->get_plots(plots);

  SCM list = SCM_LIST0;

  vector<Plot*>::iterator i = plots.begin();
  while (i != plots.end())
    {
      list = gh_cons(plot2scm(*i), list);

      ++i;
    }

  gh_allow_ints();

  return list;
}

GUPPI_PROC(make_plot_window,"guppi-plot-make-window",1,0,0,(SCM plot))
{
  SCM_ASSERT(scm_plotp(plot), plot, SCM_ARG1, "guppi-plot-make-window");

  Plot* plt = scm2plot(plot);  

  gh_defer_ints();

  guppi_plot_shell_new(plt);

  gh_allow_ints();

  return SCM_UNSPECIFIED; // If we ever bother to have PlotShell bindings we'll return it.
}

//////////////// Init

void
init_scm_plotlist()
{

#include "scm_plotlist.x"
}
