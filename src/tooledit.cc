 // -*- C++ -*-

/* 
 * tooledit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "tooledit.h"
#include "libguppi/tool.h"
#include "guppi.h"

struct button_mask {
  gint button;
  GdkModifierType mask;
};

static struct button_mask buttons_and_masks[] = {
  { 1, (GdkModifierType)0 },
  { 2, (GdkModifierType)0 },
  { 1, (GdkModifierType)GDK_CONTROL_MASK },
  { 2, (GdkModifierType)GDK_CONTROL_MASK },
  { 1, (GdkModifierType)GDK_SHIFT_MASK },
  { 2, (GdkModifierType)GDK_SHIFT_MASK },
  { 1, (GdkModifierType)GDK_MOD1_MASK },
  { 2, (GdkModifierType)GDK_MOD1_MASK },
};
  
ToolEdit::ToolEdit()
  : tool_(0), window_(0)
{

}

ToolEdit::~ToolEdit()
{
  if (window_ != 0)
    {
      gtk_widget_destroy(window_);
      window_ = 0;
    }

  vector<Row*>::iterator i = rows_.begin();
  while (i != rows_.end())
    {
      delete *i;
      ++i;
    }
}

void
ToolEdit::set_tool(Tool* t)
{
  g_return_if_fail(tool_ == 0);
  g_return_if_fail(t != 0);

  tool_ = t;

  guint i = 0;
  while (i < sizeof(buttons_and_masks)/sizeof(buttons_and_masks[0]))
    {
      Row* r = new Row(buttons_and_masks[i].button, buttons_and_masks[i].mask,
                       this, tool_);

      rows_.push_back(r);

      ++i;
    }
}

void 
ToolEdit::show()
{
  g_return_if_fail(tool_ != 0);

  if (window_ == 0)
    {
      window_ = gtk_window_new(GTK_WINDOW_TOPLEVEL);

      gtk_window_set_title(GTK_WINDOW(window_), _("Tool Editor"));

      gtk_signal_connect(GTK_OBJECT(window_),
                         "delete_event",
                         GTK_SIGNAL_FUNC(delete_event_cb),
                         this);

      GtkWidget* vbox = gtk_vbox_new(TRUE,0);

      gtk_container_add(GTK_CONTAINER(window_), vbox);

      const vector<Action*> & actions = tool_->plot()->actions();

      if (actions.empty())
        {
          gtk_box_pack_start(GTK_BOX(vbox),
                             gtk_label_new(_("This kind of plot has no actions.")),
                             TRUE, TRUE, 0);
          
        }
      else 
        {
          vector<Row*>::iterator ri = rows_.begin();
          while (ri != rows_.end())
            {
              gtk_box_pack_start(GTK_BOX(vbox),
                                 (*ri)->widget(),
                                 TRUE, TRUE, 0);
              ++ri;
            }
        }

      gtk_widget_show_all(vbox);
    }

  gtk_widget_show(window_);
}

void 
ToolEdit::hide()
{
  if (window_ != 0)
    {
      gtk_widget_hide(window_);
    }
}
  
ToolEdit::Row::Row(gint button, GdkModifierType mask, 
                  ToolEdit* owner, Tool* tool)
  : button_(button), mask_(mask), 
    owner_(owner), tool_(tool), hbox_(0), 
    active_(-1), self_toggle_(false)
{
  g_warn_if_fail(tool_ != 0);
  g_warn_if_fail(owner_ != 0);
}

ToolEdit::Row::~Row()
{

}

GtkWidget*
ToolEdit::Row::widget()
{
  if (hbox_ == 0)
    {
      hbox_ = gtk_hbox_new(FALSE, 0);

      GtkWidget* label = 0;

      const char* modname = 0;
      if (mask_ & GDK_CONTROL_MASK)
        {
          modname = _(" + Ctrl");
        }
      else if (mask_ & GDK_SHIFT_MASK)
        {
          modname = _(" + Shift");
        }
      else if (mask_ & GDK_MOD1_MASK)
        {
          modname = _(" + Alt");
        }
      else 
        {
          modname = "";
        }

      gchar buf[128];
      
      g_snprintf(buf, 128, _("Button %d%s"), button_, modname);
     
      label = gtk_label_new(buf);

      gtk_box_pack_start(GTK_BOX(hbox_), label,
                         FALSE, FALSE, 4);

      GtkWidget* bbox = gtk_hbox_new(TRUE, 0);

      const vector<Action*> & actions = tool_->plot()->actions();

      g_warn_if_fail(!actions.empty());

      vector<Action*>::const_iterator i = actions.begin();

      while (i != actions.end())
        {
          GtkWidget* button = gtk_toggle_button_new_with_label((*i)->name().c_str());
          
          guppi_set_tooltip(button, (*i)->tooltip());

          gtk_box_pack_start(GTK_BOX(bbox), button,
                             TRUE, TRUE, 0);

          const Action* a = tool_->get_action(button_, mask_);

          gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), 
                                      (a == *i));

          gtk_signal_connect(GTK_OBJECT(button),
                             "toggled",
                             GTK_SIGNAL_FUNC(toggled_cb),
                             this);

          buttons_.push_back(ActionButton(*i,button));

          ++i;
        }

      gtk_box_pack_end(GTK_BOX(hbox_), bbox,
                       FALSE, FALSE, 0);

    }

  return hbox_;
}

const Action* 
ToolEdit::Row::active()
{
  if (active_ < 0) 
    return 0;
  else 
    return buttons_[active_].first;
}

void 
ToolEdit::Row::toggled_cb(GtkWidget* button, gpointer data)
{
  Row* r = static_cast<Row*>(data);

  r->toggled(button);
}
    
void 
ToolEdit::Row::toggled(GtkWidget* button)
{
  if (self_toggle_) 
    return;

  if (active_ >= 0 && buttons_[active_].second == button)
    {
      // deactivate
      g_warn_if_fail(!GTK_TOGGLE_BUTTON(button)->active);
      active_ = -1;
    }
  else 
    {
      if (active_ >= 0) 
        {
          // Untoggle the old one
          self_toggle_ = true;
          gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(buttons_[active_].second), FALSE);
          self_toggle_ = false;
        }

      // Now we need to set active_ accordingly...
      
      // O(n) search, naughty
      guint count = 0;
      bool found = false;
      vector<ActionButton>::const_iterator i = buttons_.begin();
      while (i != buttons_.end())
        {
          if (i->second == button)
            {
              found = true;
              break;
            }
          
          ++count;
          ++i;
        }
      
      g_warn_if_fail(found);
      
      active_ = count;
    }

  owner_->row_changed(this);
}

  
void 
ToolEdit::row_changed(Row* r)
{
  const Action* a = r->active();

  tool_->set_action(a, r->button(), r->mask());
}

gint 
ToolEdit::delete_event_cb(GtkWidget* window, gpointer data)
{
  g_return_val_if_fail(GTK_IS_WINDOW(window), TRUE);
  gtk_widget_hide(window);
  return TRUE; // don't destroy
}
