// -*- C++ -*-

/* 
 * corba_guppi.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "corba_guppi.h"
#include "scm_guppi/scm_guppi.h"

#include <guile-server/Guile.h>

#include "config.h"

static CORBA_ORB orb = CORBA_OBJECT_NIL;
static CORBA_Environment ev;
static Gnome_Guile gg = CORBA_OBJECT_NIL;
static gchar *guile_ior = 0;

CORBA_ORB 
guppi_orb()
{
  return orb;
}

CORBA_Environment*
guppi_corba_env()
{
  return &ev;
}

const gchar* 
guppi_guile_ior()
{
  return guile_ior;
}


static CORBA_boolean guile_eval (PortableServer_Servant servant, 
                                     const CORBA_char * expr, 
                                     CORBA_char ** result, 
                                     CORBA_char ** output,
                                     CORBA_char ** error,
                                     CORBA_Environment * ev);

static CORBA_boolean guile_get_pending (PortableServer_Servant servant, 
                                        CORBA_char ** result, 
                                        CORBA_char ** output,
                                        CORBA_char ** error,
                                        CORBA_Environment * ev);

static void guile_clear_pending (PortableServer_Servant servant, 
                                 CORBA_Environment * ev);

static void guile_prompt (PortableServer_Servant servant, 
                          CORBA_char ** prompt,
                          CORBA_Environment * ev);

PortableServer_ServantBase__epv base_epv = {
  NULL, //  ORB private data
  NULL, // Custom finalize function could go here
  NULL  // Custom POA-obtaining function could go here
};
POA_Gnome_Guile__epv Guile_epv = 
{  
  NULL, // ORB private data
  &guile_eval,
  &guile_get_pending,
  &guile_clear_pending,
  &guile_prompt
};

POA_Gnome_Guile__vepv poa_Guile_vepv = { &base_epv, &Guile_epv };
POA_Gnome_Guile poa_Guile_servant = { NULL, &poa_Guile_vepv };


static void die(const char* err)
{
  fprintf(stderr, "%s\n", err);
  exit(1);
}

void 
guppi_corba_init(int* argc, char** argv)
{
  CORBA_exception_init (&ev);
  
  orb = gnome_CORBA_init("guppi", VERSION, argc, argv, GNORBA_INIT_SERVER_FUNC, &ev);

  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't get ORB, aborting...");
    }

  PortableServer_ObjectId objid = {0, sizeof("guppi_gnome_guile_interface"), (unsigned char*)"guppi_gnome_guile_interface"};
  PortableServer_POA poa;

  CORBA_Object *server;

  poa = (PortableServer_POA) CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't get POA");
    }

  POA_Gnome_Guile__init(&poa_Guile_servant, &ev);

  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't init Guile interface");
    }


  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);

  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't activate the POAManager");
    }
  
  PortableServer_POA_activate_object_with_id(poa, 
                                             &objid, &poa_Guile_servant, 
                                             &ev);

  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't do some function I don't understand");
    }

  gg = PortableServer_POA_servant_to_reference((PortableServer_POA)orb->root_poa, 
                                               &poa_Guile_servant, &ev);

  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't get a reference to the Guile server");
    }

  guile_ior = CORBA_ORB_object_to_string(orb, gg, &ev);

  if (guppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", guppi_corba_strerror());
      die("Couldn't get Guile IOR");
    }

}

void 
guppi_corba_shutdown()
{
  if (gg != CORBA_OBJECT_NIL)
    {
      CORBA_Object_release(gg, &ev);
      gg = CORBA_OBJECT_NIL;
    }

  if (orb != CORBA_OBJECT_NIL)
    {
      CORBA_Object_release((CORBA_Object)orb, &ev);
      orb = CORBA_OBJECT_NIL;
    }
}

struct {
  gchar* id;
} last_ex = { 0 };

GuppiCORBAErr 
guppi_corba_error(CORBA_Environment* ev)
{
  GuppiCORBAErr retval = GCE_NONE;
  g_free(last_ex.id);
  last_ex.id = 0;
  switch (ev->_major)
    {
    case CORBA_NO_EXCEPTION:
      retval = GCE_NONE;
      break;
    case CORBA_USER_EXCEPTION:
      retval = GCE_FATAL;
      last_ex.id = g_strdup(CORBA_exception_id(ev));
      break;
    default:
      retval = GCE_FATAL;
      last_ex.id = g_strdup(CORBA_exception_id(ev));
      break;
    }

  CORBA_exception_free(ev);

  return retval;
}

const gchar* 
guppi_corba_strerror()
{
  if (last_ex.id) return last_ex.id;
  else return _("(no error recorded)");
}

/* This stuff is taken from SCWM */

static SCM make_output_strport(char *fname)
{
  return scm_mkstrport(SCM_INUM0, scm_make_string(SCM_MAKINUM(30), 
                                                  SCM_UNDEFINED),
                       SCM_OPN | SCM_WRTNG,
                       fname);
}

static SCM get_strport_string(SCM port)
{
  SCM answer;
  {
    gh_defer_ints();
    answer = scm_makfromstr (SCM_CHARS (gh_cdr (SCM_STREAM (port))),
                             SCM_INUM (gh_car (SCM_STREAM (port))),
                             0);
    gh_allow_ints();
  }
  return answer;
}

/* Not exact but close enough */
#define STUPIDASS_ORBIT_LIMIT 8000

typedef struct _Pending Pending;

struct _Pending {
  unsigned char* result;
  int rlen;
  int rpos;

  unsigned char* output;
  int olen;
  int opos;

  unsigned char* error;

  int elen;
  int epos;

};

Pending pending = {NULL, 0, 0, NULL, 0, 0, NULL, 0, 0};

static CORBA_boolean 
guile_eval (PortableServer_Servant servant, 
            const CORBA_char * expr, 
            CORBA_char ** result,
            CORBA_char ** output,
            CORBA_char ** error,
            CORBA_Environment * ev)
{ 
  SCM val = SCM_UNDEFINED, str_val = SCM_UNDEFINED;
  unsigned char *ret, *out, *err;
  SCM o_port, e_port;
  SCM saved_def_e_port;

#ifdef GNOME_ENABLE_DEBUG 
  printf("Guppi: Received expression: %s\n", expr);
#endif

  guile_clear_pending(servant, ev);
       
  /* Temporarily redirect output and error to string ports. 
     Note that the port setting functions return the current previous
     port. */
  o_port=scm_set_current_output_port(make_output_strport("guile_eval"));
  e_port=scm_set_current_error_port(make_output_strport("guile_eval"));

  /* Workaround for a problem with older Guiles */
  saved_def_e_port = scm_def_errp;
  scm_def_errp = scm_current_error_port();
  
  /* Evaluate the request expression and free it. */
  val = guppi_eval_str(expr);

  str_val=scm_strprint_obj(val);
  ret = (unsigned char *) gh_scm2newstr(str_val, &pending.rlen);

  /* restore output and error ports. */
  o_port=scm_set_current_output_port(o_port);
  e_port=scm_set_current_error_port(e_port);
  scm_def_errp = saved_def_e_port;

  /* Retrieve output and errors */
  out = (unsigned char *) gh_scm2newstr(get_strport_string(o_port),
                                        &pending.olen);
  err = (unsigned char *) gh_scm2newstr(get_strport_string(e_port),
                                        &pending.elen);
        
  pending.result = ret;
  pending.output = out;
  pending.error = err;

  pending.rpos = 0;
  pending.opos = 0;
  pending.epos = 0;

  return guile_get_pending(servant, result, output, error, ev);
}


static CORBA_boolean 
guile_get_pending (PortableServer_Servant servant, 
                   CORBA_char ** result, 
                   CORBA_char ** output,
                   CORBA_char ** error,
                   CORBA_Environment * ev)
{
  int rlen = pending.rlen - pending.rpos;
  int olen = pending.olen - pending.opos;
  int elen = pending.elen - pending.epos;

  if (rlen > STUPIDASS_ORBIT_LIMIT) rlen = STUPIDASS_ORBIT_LIMIT;
  if (olen > STUPIDASS_ORBIT_LIMIT) olen = STUPIDASS_ORBIT_LIMIT;
  if (elen > STUPIDASS_ORBIT_LIMIT) elen = STUPIDASS_ORBIT_LIMIT;
        
  if (rlen > 0)
    {
      *result = CORBA_string_alloc(rlen+2);
      g_snprintf(*result, rlen+1, "%s", &pending.result[pending.rpos]);
    }
  else 
    {
      *result = CORBA_string_dup("");
    }

  if (olen > 0)
    {
      *output = CORBA_string_alloc(olen+2);
      g_snprintf(*output, olen+1, "%s",  &pending.output[pending.opos]);
    }
  else 
    {
      *output = CORBA_string_dup("");
    }

  if (elen > 0)
    {
      *error = CORBA_string_alloc(elen+2);
      g_snprintf(*error, elen+1, "%s", &pending.error[pending.epos]);
    }
  else
    {
      *error = CORBA_string_dup("");
    }
  
  pending.rpos += rlen;
  pending.opos += olen;
  pending.epos += elen;

  if (pending.rpos >= pending.rlen && 
      pending.opos >= pending.olen &&
      pending.epos >= pending.elen) return FALSE;
  else return TRUE;
}

static void 
guile_clear_pending (PortableServer_Servant servant, 
                     CORBA_Environment * ev)
{
  if (pending.result) free(pending.result);
  if (pending.output) free(pending.output);
  if (pending.error) free(pending.error);

  memset(&pending, '\0', sizeof(Pending));
}


static void 
guile_prompt (PortableServer_Servant servant, 
              CORBA_char ** prompt,
              CORBA_Environment * ev)
{
  *prompt = CORBA_string_dup("guppi> ");
}
