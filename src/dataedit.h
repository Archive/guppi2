// -*- C++ -*-

/* 
 * dataedit.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATAEDIT_H
#define GUPPI_DATAEDIT_H

#include "guppi.h"
#include "libguppi/data.h"
#include "libguppi/datagroupedit.h"

class DataEdit : public Data::View {
public:
  DataEdit();
  virtual ~DataEdit();

  void set_data(Data* d);

  Data* data() { return data_; }

  virtual void change_values(Data* d, const vector<guint> & which);
  virtual void change_values(Data* d);
  virtual void change_name(Data* d, const string & name);

  virtual void destroy_model();
  
private:
  Data* data_;

  GtkWidget* window_;
  GtkWidget* name_entry_;
  GtkWidget* scalar_value_entry_;
  GtkWidget* string_value_entry_;
  GtkWidget* clist_;
  GtkWidget* sw_;
  
  DataGroupEdit group_edit_;

  bool changing_name_;
  bool changing_value_;

  int editing_row_;

  void ensure_widgets();
  void release_data();
  void update_entries();

  static gint delete_event_cb(GtkWidget* w, GdkEventAny* event, gpointer data);
  gint delete_event();

  static gint key_event_cb(GtkWidget* w, GdkEventKey* event, gpointer data);
  gint key(GdkEventKey* event);

  static void select_row_cb(GtkWidget* w, int row, int col, GdkEvent* event, gpointer data);
  void select_row(int row);

  static void name_entry_cb(GtkWidget* w, gpointer data);
  void name_entry_changed();

  static void value_entry_cb(GtkWidget* w, gpointer data);
  void value_entry_changed();
};

#endif
