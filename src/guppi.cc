// -*- C++ -*-

/* 
 * guppi.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "guppi.h"
#include "app.h"
#include "scm_init.h"
#include "corba_guppi.h"
#include "guppidata.h"
#include "plotlist.h"

#include <goose/Exception.h>

#include <algorithm>
#include <iostream>

static GtkWidget* main_window = 0;

static void 
real_main(void* closure, int argc, char** argv)
{
  // Ensure FILE* and iostreams don't get confused
  // I think this should maybe be in ios_base:: according to the standard...
  ios::sync_with_stdio();

  // Initialize the i18n stuff
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  // Also inits Gnome
  guppi_corba_init(&argc, argv);

  // Bring up the splash screen...
  guppi_start_splash();

  while (gtk_events_pending())
    gtk_main_iteration();

  // Init libguppi-gnome
  if (!guppi_gnome_init_library())
    {
      GtkWidget* dialog = gnome_error_dialog(_("Failure initializing libraries; exiting."));
      gnome_dialog_run(GNOME_DIALOG(dialog));
      exit(1);
    }

  while (gtk_events_pending())
    gtk_main_iteration();

  guppi_scm_startup(); // This assumes Gnome is up.

  while (gtk_events_pending())
    gtk_main_iteration();

  try {
    App* app = new App;

    main_window = app->widget();

    // Zero main_window if the main window gets nuked.
    // Total paranoia.
    gtk_signal_connect(GTK_OBJECT(main_window),
                       "destroy",
                       GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                       &main_window);

    while (gtk_events_pending())
      gtk_main_iteration();

#ifdef GNOME_ENABLE_DEBUG
    printf("%s\n", guppi_guile_ior());
#endif

    while (gtk_events_pending())
      gtk_main_iteration();

    // Take down the splash screen
    guppi_end_splash();

    gtk_main();

    // These are quit actions that we maybe want to run after leaving Gtk.
    // Other quit actions go in guppi_quit().
    main_window = 0;
    delete app;
    app = 0;
    guppi_replace_datastore(0); // delete the data store
    
    // Finally, shut down libguppi-gnome
    guppi_gnome_shutdown_library();
  }
  catch (Exception & e) {
    cerr << "Unhandled exception from Goose library: " << e.what() << endl;
    exit(1);
  }
  catch (exception & e) {
    cerr << "Unhandled C++ library exception: " << e.what() << endl;
    exit(1);
  }

  exit(0);
}

int
main(int argc, char * argv[])
{
  scm_boot_guile(argc, argv, real_main, 0);
  return 1; // not reached
}


void
guppi_quit()
{
  g_return_if_fail(gtk_main_level() == 1);

  // We're busy during the whole shutdown process; 
  // busy cursor won't be set on any modal dialogs 
  // that come up so that's fine.
  guppi_start_busy();

  guppi_plotlist_shutdown(); // kill plot windows
  scm_gc(); // clean up scheme objects, just to be tidy.
  guppi_corba_shutdown();
  gtk_main_quit();
}


void 
guppi_setup_dialog(GtkWidget* dialog)
{
  g_return_if_fail(GTK_IS_WINDOW(dialog));

  if (main_window == 0) return; // FIXME

  if (GNOME_IS_DIALOG(dialog))
    gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW(main_window));
  else 
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(main_window));
}

void 
guppi_start_busy(GtkWidget* w)
{
  if (main_window == 0) return; // FIXME

  if (!GTK_WIDGET_REALIZED(main_window)) return;

  GdkCursor* cursor = gdk_cursor_new(GDK_WATCH);

  gdk_window_set_cursor(main_window->window, cursor);

  if (w != 0 && GTK_WIDGET_REALIZED(w))
    gdk_window_set_cursor(w->window, cursor);

  gdk_cursor_destroy(cursor);

  // Nope - this causes weirdness (say, if one of the events results
  //  in odd side effects we don't expect).
  // Make sure we're up-to-date before we go into calculation mode.
  //  while (gtk_events_pending())
  //    gtk_main_iteration();
}

void 
guppi_end_busy(GtkWidget* w)
{
  if (main_window == 0) return; // FIXME

  if (!GTK_WIDGET_REALIZED(main_window)) return;

  gdk_window_set_cursor(main_window->window, NULL);

  if (w != 0 && GTK_WIDGET_REALIZED(w))
    gdk_window_set_cursor(w->window, NULL);
}

#include "time.h"

//#include "guppi.xpm"

static GtkWidget* splash = 0;
static time_t splash_start = 0;

void 
guppi_start_splash()
{
  g_debug("Splashing!");

  g_return_if_fail(splash == 0);

  splash_start = time(NULL);

  splash = gtk_window_new(GTK_WINDOW_DIALOG);

  guppi_setup_window(GTK_WINDOW(splash), "splash");

  gtk_signal_connect(GTK_OBJECT(splash),
                     "destroy",
                     GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                     &splash);

  gtk_window_set_position(GTK_WINDOW(splash), GTK_WIN_POS_CENTER);

  gtk_window_set_policy(GTK_WINDOW(splash), FALSE, FALSE, FALSE);

  gtk_window_set_title(GTK_WINDOW(splash), _("Welcome to Guppi"));

  gchar* logo = guppi_logo_file();

  GtkWidget* pix;
  if (logo != 0)
    {
      pix = gnome_pixmap_new_from_file(logo);
    }
  else
    {
      pix = gtk_label_new(_("[Guppi Logo] (file not found)"));
    }

  if (logo != 0)
    g_free(logo);

  GtkWidget* vbox = gtk_vbox_new(FALSE, 0);

  GtkWidget* label = gtk_label_new(_("Loading Guppi..."));

  gtk_misc_set_alignment(GTK_MISC(label), 0.5, 0.5);
  
  gtk_box_pack_start(GTK_BOX(vbox), pix, TRUE, TRUE, 0);
  gtk_box_pack_end(GTK_BOX(vbox), label, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(splash), vbox);

  gtk_widget_show_all(splash);

  while (gtk_events_pending())
    gtk_main_iteration();
}

void 
guppi_end_splash()
{
  g_debug("Un-splashing!");

  g_return_if_fail(splash != 0);
  
  time_t splash_end = time(NULL);

  if ((splash_end - splash_start) < 2)
    {
      g_warning("FIXME, didn't display splash screen long enough!");
    }

  if (splash != 0)
    gtk_widget_destroy(splash);

  splash = 0;
}
