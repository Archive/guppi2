// -*- C++ -*-

/* 
 * plotlist.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "plotlist.h"

PlotList::PlotList()
  : cleanup_pending_(false)
{

}

PlotList::~PlotList()
{
  if (cleanup_pending_)
    {
      gtk_idle_remove(idle_tag_);
      cleanup();
    }

  set<PlotShell*>::iterator i = plots_.begin();
  while (i != plots_.end())
    {
      delete *i;
      ++i;
    }
}

void 
PlotList::add(PlotShell* ps)
{
  g_return_if_fail(ps != 0);

  ps->set_owner(this);
  plots_.insert(ps);
}

void 
PlotList::kill_me_please(PlotShell* ps)
{
  set<PlotShell*>::iterator i = plots_.find(ps);
  g_return_if_fail(i != plots_.end());
  
  plots_.erase(i);
  
  doomed_.push_back(ps);

  queue_cleanup();
}

void 
PlotList::get_plots(vector<Plot*> & plots)
{
  set<PlotShell*>::iterator i = plots_.begin();
  while (i != plots_.end())
    {
      plots.push_back((*i)->plot());
      ++i;
    }
}

void 
PlotList::queue_cleanup()
{
  if (cleanup_pending_) return;

  idle_tag_ = gtk_idle_add(deletion_idle, this);

  cleanup_pending_ = true;
}

gint 
PlotList::deletion_idle(gpointer data)
{
  PlotList* pl = static_cast<PlotList*>(data);
  
  pl->cleanup();

  return FALSE; // remove idle
}

void 
PlotList::cleanup()
{
  g_return_if_fail(cleanup_pending_);

  vector<PlotShell*>::iterator i = doomed_.begin();
  while (i != doomed_.end())
    {
      delete *i;
      ++i;
    }
  doomed_.clear();

  cleanup_pending_ = false;
}


static PlotList* pl = 0;

PlotList* 
guppi_plotlist()
{
  if (pl == 0)
    {
      pl = new PlotList;
    }

  return pl;
}

// Gtk crashes if you destroy widgets in static 
// destructors
void 
guppi_plotlist_shutdown()
{
  if (pl != 0)
    delete pl;
  pl = 0;
}

PlotShell*
guppi_plot_shell_new(Plot* p)
{
  PlotShell* ps = new PlotShell;
  ps->set_plot(p);
  guppi_plotlist()->add(ps);
  return ps;
}
