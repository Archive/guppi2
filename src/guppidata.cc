// -*- C++ -*-

/* 
 * guppidata.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "guppidata.h"
#include "libguppi/scalardata.h"
#include "libguppi/labeldata.h"
#include "goose/RealSet.h"
#include "libguppi/util.h"
#include "libguppi/datagroup.h"
#include "libguppi/dataspecstore.h"
#include "libguppi/datedata.h"

static DataStore* ds = 0;

DataStore* 
guppi_datastore()
{
  if (ds == 0)
    {
      ds = new DataStore;
    }

  return ds;
}

void 
guppi_replace_datastore(DataStore* newds)
{
  if (ds != 0)
    {
      delete ds;
      ds = 0;
    }
  
  ds = newds;
}

#ifdef GNOME_ENABLE_DEBUG
void 
guppi_create_debug_datasets(DebugDataType type)
{
  if (ds == 0)
    {
      gdk_beep();
      g_warning("No datastore, can't make data sets");
      return;
    }

  g_debug("Creating debug data sets.");

  switch (type) {
  case DebugDataAll:
    {
      int i = 0;
      while (i < DebugDataTypeEnd)
        {
          if (i != (int)DebugDataAll)
            guppi_create_debug_datasets((DebugDataType)i);

          ++i;
        }
    }
    break;

  case DebugDataBasic:
    {
      {
        LabelData* d;
        int z;
      
        d = new LabelData;
        d->set_name("Label Data 0");
        z = 0;
        while (z < 20)
          {
            if ((z % 2) == 0)
              d->add("Foo");
            else
              d->add("Bar");
            ++z;
          }

        ds->insert(d);

        d = new LabelData;
        d->set_name("Label Data 1");
        z = 0;
        while (z < 20)
          {
            if ((z % 2) == 0)
              d->add("Boing");
            else
              d->add("Sproing");
            ++z;
          }

        ds->insert(d);
      }

      {
        ScalarData* d;
        int z;

        d = new ScalarData;
        d->set_name("Demo Points 0");
        z = 0;
        while (z < 205)
          {
            d->add(z*3.3);
            ++z;
          }
        ds->insert(d);

        d = new ScalarData;
        d->set_name("Demo Points 1");
        z = 0;
        while (z < 205)
          {
            d->add(z*5.0);
            ++z;
          }
        ds->insert(d);

        d = new ScalarData;
        d->set_name("Sequential/Time Data");
        z = 3;
        while (z < 300)
          {
            d->add(z*4);
            ++z;
          }
        ds->insert(d);

        d = new ScalarData;
        d->set_name("Empty Data 0");
        ds->insert(d);

        d = new ScalarData;
        d->set_name("Empty Data 1");
        ds->insert(d);

        d = new ScalarData;
        d->set_name("One Point 0");
        d->add(1000.0);
        ds->insert(d);

        d = new ScalarData;
        d->set_name("One Point 1");
        d->add(2000.0);
        ds->insert(d);

        d = new ScalarData;
        d->set_name("Duplicate Points 0");
        z = 0;
        while (z < 205)
          {
            d->add(2000.0);
            ++z;
          }
        ds->insert(d);

        d = new ScalarData;
        d->set_name("Duplicate Points 1");
        z = 0;
        while (z < 205)
          {
            d->add(1000.0);
            ++z;
          }
        ds->insert(d);
      }
    }
    break;

  case DebugDataPie:
    {
      ScalarData* d;
      int z;

      d = new ScalarData;
      d->set_name("Pie Data 0");
      z = 3;
      while (z < 5)
        {
          d->add(z*3.3);
          ++z;
        }
      ds->insert(d);

      d = new ScalarData;
      d->set_name("Pie Data 1");
      z = 3;
      while (z < 6)
        {
          d->add(z*3.3);
          ++z;
        }
      ds->insert(d);

      d = new ScalarData;
      d->set_name("Pie Data 2");
      z = 3;
      while (z < 10)
        {
          d->add(z*4.1);
          ++z;
        }
      ds->insert(d);
    }
    break;

  case DebugDataBig:
    {
      double max = 15000000;
      int n = 0;
      while (n < 10)
        {
          // Put in some stupid ass fake data for debugging
          ScalarData* d = new ScalarData;
          
          bool ascend = (rand() < RAND_MAX/2);

          double add = ascend ? 10000 : 6000;
          double sub = ascend ? 6000 : 10000;

          double i = ascend ? (0.0 + rand()/(RAND_MAX/max)) : ((max-1.0) - rand()/(RAND_MAX/max));
          while ((ascend && i < max) || (!ascend && i > -1000.0))
            {
              d->add(i/1000);
              int r = rand();
              // on average, move in a single direction.
              i += (double)r/(RAND_MAX/add);
              i -= (double)r/(RAND_MAX/sub);
            }

          g_debug("Created random scalar data");

          if (rand() < RAND_MAX/2)
            {
              RealSet* rs = d->checkout_realset();

              g_debug("Scrambling this random data");

              rs->scramble();

              d->checkin_realset(rs, true, false);
            }

          // vary the number of elements
          max += rand()/(RAND_MAX/850000.0);
          max -= rand()/(RAND_MAX/300000.0);

          char buf[100];
          g_snprintf(buf, 99, "Test data %d", n);
          
          d->set_name(buf);
          
          ds->insert(d);

          ++n;
        }
    }

#define NPOINTS 250000
    {
      ScalarData* d;
      double* xv;
      double* yv;

      xv = new double[NPOINTS];
      yv = new double[NPOINTS];
        
      g_debug("Making %d-point data sets...", NPOINTS);
      guint i = 0;
      double v, v2;
      while (i < NPOINTS)
        {
#if 0
          xv[i] = 0.0;
          yv[i] = 0.0;
#else
          int r = rand();
          v = (double(r)/(((double)RAND_MAX)/2000.0));
          r = rand();
          v2 = (double(r)/(((double)RAND_MAX)/2000.0));
            
          if (rand() > (RAND_MAX/2)) v = -v;
            
          xv[i] = CLAMP(v, -v2, v2);
            
          r = rand();
          v = (double(r)/(((double)RAND_MAX)/2000.0));
          r = rand();
          v2 = (double(r)/(((double)RAND_MAX)/2000.0));
            
          if (rand() > (RAND_MAX/2)) v = -v;
            
          yv[i] = CLAMP(v, -v2, v2);
#endif

          ++i;
        }
        
      d = new ScalarData;
      d->set_name("Huge data set 1");
      RealSet* rs = d->checkout_realset();
      rs->add(xv, NPOINTS);
      d->checkin_realset(rs, true, false);
      ds->insert(d);
        
      d = new ScalarData;
      d->set_name("Huge data set 2");
      rs = d->checkout_realset();
      rs->add(yv, NPOINTS);
      d->checkin_realset(rs, true, false);
      ds->insert(d);      
        
      delete [] xv;
      delete [] yv;
    }
    break;

  case DebugDataPrice:
    {
      double* hi = new double[1000];
      double* lo = new double[1000];
      double* open = new double[1000];
      double* close = new double[1000];

      double last_v = 500.0;

      int i = 0;
      while (i < 1000)
        {
          double v = (double(rand())/(((double)RAND_MAX)/50.0));
          if (v > 25)
            v = -(v-25);

          v = last_v + v;

          hi[i] = v + 10.0;
          lo[i] = v - 10.0;
          open[i] = v + 4.0;
          close[i] = v - 5.0;

          last_v = v;
          
          ++i;
        }

      DataGroupSpec* spec = guppi_price_data_spec();

      // The grp's five slots are Dates, Opens, His, Los, Closes
      DataGroup* grp = new DataGroup(spec);
      
      grp->set_name("Price Data Group");

      ScalarData* sd = new ScalarData;
      sd->set_name("Highs");
      RealSet* rs = sd->checkout_realset();
      rs->add(hi, 1000);
      sd->checkin_realset(rs, true, false);
      ds->insert(sd);
      grp->set_data(PriceHighs, sd);

      sd = new ScalarData;
      sd->set_name("Lows");
      rs = sd->checkout_realset();
      rs->add(lo, 1000);
      sd->checkin_realset(rs, true, false);
      ds->insert(sd);
      grp->set_data(PriceLows, sd);

      sd = new ScalarData;
      sd->set_name("Opens");
      rs = sd->checkout_realset();
      rs->add(open, 1000);
      sd->checkin_realset(rs, true, false);
      ds->insert(sd);
      grp->set_data(PriceOpens, sd);
      
      sd = new ScalarData;
      sd->set_name("Closes");
      rs = sd->checkout_realset();
      rs->add(close, 1000);
      sd->checkin_realset(rs, true, false);
      ds->insert(sd);
      grp->set_data(PriceCloses, sd);

      delete [] hi;
      delete [] lo;
      delete [] open;
      delete [] close;

      DateData* dd = new DateData;
      
      GDate date;
      g_date_clear(&date, 1);
      g_date_set_dmy(&date, 1, G_DATE_JANUARY, 1978);
      i = 0;
      while (i < 1000)
        {
          dd->add(date);
          g_date_add_days(&date, 1);
          ++i;
        }
      dd->set_name("Price Dates");
      ds->insert(dd);
      
      grp->set_data(PriceDates, dd);

      ds->insert(grp);

      printf("price group has type %d, spec has %d, group %s an instance of its own type\n",
             grp->spec()->id(), guppi_price_data_spec()->id(), 
             grp->is_a(guppi_price_data_spec()->id()) ? "is" : "is not");
    }
    break;

  default:
    g_assert_not_reached();
    break;
  }

  g_debug("Done.");
}
#endif // GNOME_ENABLE_DEBUG

