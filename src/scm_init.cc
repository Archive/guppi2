// -*- C++ -*-

/* 
 * scm_init.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "scm_init.h"

#include "scm_guppi_gnome/scm_guppi_gnome.h"

#include "guppi.h"

GUPPI_PROC(guppi_scm_quit,"guppi-quit",0,0,0,())
{
  guppi_quit();
  return SCM_UNSPECIFIED;
}

extern void init_scm_guppidata();
extern void init_scm_plotlist();

void 
guppi_scm_startup()
{
  guppi_gnome_scm_init();

  init_scm_guppidata();
  init_scm_plotlist();

#include "scm_init.x"

  // Keep people from quitting Guppi in a bad way.
  gh_eval_str("(define (quit) (guppi-quit))");
  
  gchar* startupfile = g_concat_dir_and_file(GUPPI_GUILE_DIR, "guppi.scm");
  puts("***** ");
  puts(startupfile);
  puts("\n");
  
  if (startupfile && g_file_exists(startupfile))
    {
      guppi_safe_load(startupfile);
    }
#ifdef GNOME_ENABLE_DEBUG
  else if (g_file_exists("./guppi.scm"))
    {
      guppi_safe_load("./guppi.scm");
    }
#endif
  else
    {
      GtkWidget* dialog = gnome_warning_dialog(_("Startup file \"guppi.scm\" not found.\n"
                                                 "Guppi may not work properly."));
      guppi_setup_dialog(dialog);
    }


  g_free(startupfile);
  startupfile = 0;

  const gchar* homedir = g_get_home_dir();
  if (homedir)
    startupfile = g_concat_dir_and_file(homedir, ".guppi.scm");
  if (startupfile && g_file_exists(startupfile))
    {
      guppi_safe_load(startupfile);
    }
  
  g_free(startupfile);
  startupfile = 0;
}
