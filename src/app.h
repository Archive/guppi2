// -*- C++ -*-

/* 
 * app.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_APP_H
#define GUPPI_APP_H

#include "guppi.h"
#include "datalist.h"

class Term;

class App {
public:
  App();
  virtual ~App();

  GtkWidget* widget() { return app_; }

  // Not really public, ignore.
  static void open_cb(GtkWidget* w, gpointer data);
  static void import_cb(GtkWidget* w, gpointer data);
  static void save_cb(GtkWidget* w, gpointer data);
  static void save_as_cb(GtkWidget* w, gpointer data);
  static void term_cb(GtkWidget* w, gpointer data);
  static void barplot_cb(GtkWidget* w, gpointer data);
  static void pieplot_cb(GtkWidget* w, gpointer data);
  static void scatterplot_cb(GtkWidget* w, gpointer data);

private:
  GtkWidget* app_;

  DataList list_;

  string filename_;
  gint compression_;

  Term* term_;

  static gint delete_event_cb(GtkWidget* w,  GdkEventAny* event, gpointer data);
  gint delete_event();

  void open();
  void save();
  void save_as();
  void term();

  void barplot();
  void pieplot();
  void scatterplot();
};

#endif
