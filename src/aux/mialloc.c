/* Wrappers for standard storage allocation functions.  The tests for zero
   size, etc., are necessitated by the calling conventions of the MI scan
   conversion module. */

#include "mi.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef NULL
#define NULL ((void*)0)
#endif

#define BOMB_IF_NULL(p) \
do { \
  if (p == NULL)            \
    {                        \
      fprintf (stderr, "libguppi, MI module: "); \
      perror ("out of memory");                  \
      exit (1);         \
    } \
   } while(0)

/* wrapper for malloc() */
void*
mi_xmalloc (unsigned int size)
{
  void* p;

  if (size == 0)
    return NULL;

  p = (void*) malloc (size);

  BOMB_IF_NULL(p);

  return p;
}

/* wrapper for calloc() */
void* 
mi_xcalloc (unsigned int nmemb, unsigned int size)
{
  void* p;

  if (size == 0)
    return NULL;

  p = (void*) calloc (nmemb, size);

  BOMB_IF_NULL(p);

  return p;
}

/* wrapper for realloc() */
void* 
mi_xrealloc (void* p, unsigned int size)
{
  if (p == NULL)
    return mi_xmalloc (size);
  else
    {
      if (size == 0)
	{
	  free (p);
	  return NULL;
	}
      
      p = (void*) realloc (p, size);

      BOMB_IF_NULL(p);

      return p;
    }
}

void
mi_xfree(void* p)
{
  if (p == NULL)
    return;
  else 
    free(p);
}

void* 
mi_xmemmove(void *dest, const void *src, unsigned int n)
{
  return memmove(dest, src, n);
}
