
#include "mi.h"

static miBool
samePixel(miPixel pix1, miPixel pix2)
{
  /* This could probably be faster. */

  return 
    ((pix1.r == pix2.r) && 
     (pix1.g == pix2.g) && 
     (pix1.b == pix2.b) &&
     (pix1.a == pix2.a));
}

/* The basic goal is to emulate X, when it makes sense. */

void 
miInitGC(miGC* pGC)
{
  static unsigned int default_dashes[] = { 4, 4 }; /* Same as X */

  pGC->samePixel      = samePixel;
  pGC->drawable       = NULL;
  pGC->width          = 0;
  pGC->height         = 0;

  pGC->fgPixel.r = pGC->fgPixel.g = pGC->fgPixel.b = 0x0;  /* black */
  pGC->bgPixel.r = pGC->bgPixel.g = pGC->bgPixel.b = 0xff; /* white */

  /* Both are fully opaque. */
  pGC->fgPixel.a      = 0xff;
  pGC->bgPixel.a      = 0xff;

  pGC->fillStyle      = miFillSolid;
  pGC->fillRule       = miEvenOddRule;
  pGC->joinStyle      = miJoinMiter;
  pGC->capStyle       = miCapButt;
  pGC->lineStyle      = miLineSolid;
  pGC->lineWidth      = 0;
  pGC->miterLimit     = 10.43;  /* same as X, according to mi.h comment */
  pGC->arcMode        = miArcPieSlice;

  pGC->dash           = default_dashes;
  pGC->numInDashList  = 2;
  pGC->dashOffset     = 0;
  
  /* I am hoping tiles/stipples can just be ignored; if not we'll have do 
     some defaults. */
  pGC->stipple        = NULL;
  pGC->stippleWidth   = 0;
  pGC->stippleHeight  = 0;
  pGC->stippleXorigin = 0;
  pGC->stippleYorigin = 0;

  pGC->tile           = NULL;
  pGC->tileWidth      = 0;
  pGC->tileHeight     = 0;
  pGC->tileXorigin    = 0;
  pGC->tileYorigin    = 0;
}

void 
miBitmapizePixbuf(unsigned char* pixbuf,  unsigned char* bitmap[], 
                  unsigned int rowstride, unsigned int nrows)
{
  unsigned int i = 0;
  unsigned char* current_row = pixbuf;

  while (i < nrows)
    {
      bitmap[i] = current_row;
      current_row += rowstride;
      ++i;
    }
}
