#define EPSILON	0.000001
#define ISEQUAL(a,b) (MI_FABS((a) - (b)) <= EPSILON)
#define UNEQUAL(a,b) (MI_FABS((a) - (b)) > EPSILON)
#define PTISEQUAL(a,b) (ISEQUAL(a.x,b.x) && ISEQUAL(a.y,b.y))

/* Point with sub-pixel positioning.  In this case we use doubles, but
 * see mifpolycon.c for other possibilities.
 */
typedef struct
{
  double	x, y;
} SppPoint;

/* Arc with sub-pixel positioning. */
typedef struct 
{
  double	x, y, width, height;
  double	angle1, angle2;
} SppArc;

void miFillSppPoly (miGC *pGC, int count, const SppPoint *ptsIn, int xTrans, int yTrans, double xFtrans, double yFtrans);
