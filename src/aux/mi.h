/* This is the master include file for the MI (machine independent)
   scan-conversion module, which is based on source files in the X Window
   System distribution.  The module consists of the files g_mi*.c and
   g_mi*.h.  For an X Consortium copyright notice, see the file README-X.
   GNU extensions copyright (c) 1998-99 by the Free Software Foundation.

   The MI module is essentially self-contained, with only a few drawing
   functions that serve as entry points (they are declared at the bottom of
   this file).  So in principle, you could remove the MI module from the
   libplot/libplotter distribution, and include it in other code.  The
   functions in the module draw into a bitmap (a 2-D array of miPixels).
   They take an `augmented graphics context' (a structure including such a
   bitmap, and a standard set of drawing parameters) as first argument.
   You would typedef miPixel to whatever is appropriate in your
   application, and also provide a definition of the samePixel() function,
   which tests two pixel values for equality.

   If you use the module in other code, you would need to include various
   system header files (e.g., <stdlib.h>, <limits.h> and <math.h>) for it
   to compile.  You would also need to delete the #include lines in each
   file that refer to "sys-defines.h" and "extern.h", which are specific to
   libplot/libplotter.  And you would need to define a few macros like
   ____P(), _HAVE_PROTOS, Voidptr, and IROUND(), but that is a trivial
   matter (the definitions for them are all in ../include/sys-defines.h).  */

#ifndef MI_MI_H
#define MI_MI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <config.h>
#include <math.h>
#include <limits.h> /* We may want to switch to G_INT_MAX etc. */

#ifndef NULL
#define NULL ((void*)0)
#endif

typedef int miBool;

/* I would rather have these be TRUE/FALSE and fix all instances... */
#ifndef __cplusplus
#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif
#endif /* __cplusplus */

/** Math macros lifted from libplot. I'm not sure why the gcc ones are
    faster.  I think the inline standard library functions might be
    faster still in any case. Hmm. */

#ifdef __GNUC__
#define MI_DMAX(a,b) ({double _a = (a), _b = (b); _a > _b ? _a : _b; })
#define MI_DMIN(a,b) ({double _a = (a), _b = (b); _a < _b ? _a : _b; })
#define MI_IMAX(a,b) ({int _a = (a), _b = (b); _a > _b ? _a : _b; })
#define MI_IMIN(a,b) ({int _a = (a), _b = (b); _a < _b ? _a : _b; })
#define MI_UMAX(a,b) ({unsigned int _a = (a), _b = (b); _a > _b ? _a : _b; })
#define MI_UMIN(a,b) ({unsigned int _a = (a), _b = (b); _a < _b ? _a : _b; })
#define MI_IROUND(x) ({double _x = (x); int _i; \
                    if (_x >= INT_MAX) _i = INT_MAX; \
                    else if (_x <= -(INT_MAX)) _i = -(INT_MAX); \
                    else _i = (_x > 0.0 ? (int)(_x + 0.5) : (int)(_x - 0.5)); \
                    _i;})
#define MI_FROUND(x) ({double _x = (x); float _f; \
                    if (_x >= FLT_MAX) _f = FLT_MAX; \
                    else if (_x <= -(FLT_MAX)) _f = -(FLT_MAX); \
                    else _f = _x; \
                    _f;})
#define MI_FABS(x) ((x) >= 0.0 ? (x) : -(x))
#define MI_ICEIL(x) ({double _x = (x); int _i = (int)_x; \
		   ((_x == _i) || (_x < 0.0)) ? _i : _i + 1;})
#define MI_IFLOOR(x) ({double _x = (x); int _i = (int)_x; \
		   ((_x == _i) || (_x > 0.0)) ? _i : _i - 1;})
#else
#define MI_DMAX(a,b) ((a) > (b) ? (a) : (b))
#define MI_DMIN(a,b) ((a) < (b) ? (a) : (b))
#define MI_IMAX(a,b) ((a) > (b) ? (a) : (b))
#define MI_IMIN(a,b) ((a) < (b) ? (a) : (b))
#define MI_UMAX(a,b) ((a) > (b) ? (a) : (b))
#define MI_UMIN(a,b) ((a) < (b) ? (a) : (b))
#define MI_IROUND(x) ((int) ((x) > 0 ? (x) + 0.5 : (x) - 0.5))
#define MI_FROUND(x) ((float)(x))
#define MI_FABS(x) ((x) >= 0.0 ? (x) : -(x))
#define MI_ICEIL(x) ((int)ceil(x))
#define MI_IFLOOR(x) ((int)floor(x))
#endif

/***** The real stuff starts here *******/

/* structure that defines a point (e.g. the points that define a polyline
   or polygon) */
typedef struct
{
  int x, y;			/* integer coordinates, y goes downward */
} miIntPoint;

/* structure that defines a rectangle */
typedef struct
{
  int x, y;			/* upper left corner */
  unsigned int width, height;	/* pixel range */
} miRectangle;

/* structure that defines an `arc' (a segment of an ellipse aligned with
   the coordinate axes) */
typedef struct
{
  int x, y;			/* upper left corner of ellipse's bbox */
  unsigned int width, height;	/* dimensions of ellipse */
  int angle1, angle2;	/* starting angle and angle range, in 1/64 degrees */
} miArc;

/* Pixel value datatype.  Should be able to hold all pixel values.  In
   libplot/libplotter we use a union, so that this module can be used both
   by GIF Plotters (in which pixel values are 8-bit color indices) and by
   PNM Plotters (for which pixel values are 24-bit RGB values). */

/* In libplot/libplotter, the official definition of miPixel appears in
   plotter.h, so this definition is commented out.  Uncomment and modify
   appropriately if using this module in other code. */

/* For Guppi, we have an alpha channel! Whee! */

typedef struct
{
  unsigned char r;
  unsigned char g;
  unsigned char b;
  unsigned char a;
} miPixel;

/* values for fill style */
typedef enum 
{ 
  miFillSolid, miFillTiled, miFillStippled, miFillOpaqueStippled 
} miFillStyle;

/* values for fill rule */
typedef enum 
{ 
  miEvenOddRule, miWindingRule 
} miFillRule;

/* values for join style */
typedef enum 
{ 
  miJoinMiter, miJoinRound, miJoinBevel, miJoinTriangular 
} miJoinStyle;

/* values for cap style */
typedef enum 
{ 
  miCapNotLast, miCapButt, miCapRound, miCapProjecting, miCapTriangular 
} miCapStyle;

/* values for line style */
typedef enum 
{ 
  miLineSolid, miLineOnOffDash, miLineDoubleDash 
} miLineStyle;

/* values for arc mode */
typedef enum 
{ 
  miArcChord, miArcPieSlice 
} miArcMode;


/* An `augmented graphics context', including a drawable (a 2-D array of
   pixels), and a graphics context (GC).  A pointer to one of these
   structures is passed as the first argument to each of the externally
   accessible drawing functions.

   The drawing functions treat the structure almost as a `const struct'.
   (The fgPixel and fillStyle members of the structure may be temporarily
   modified, but if so, they are restored; other members are not modified.)

   All fields should be filled in.  See g_miter.c for comments on the
   miter limit. */
typedef struct
{
  /* test of pixels for equality */
  miBool (*samePixel) (miPixel pixel1, miPixel pixel2);
  /* 2D array of pixels; (0,0) is upper left corner */
  /* Each pixel is 3 unsigned char, so each row is width*3 in length. */
  /* We don't use miPixel because there's no typedef guaranteed to be 
     24 bits in length in C. */
  unsigned char** drawable;
  unsigned int width;
  unsigned int height;
  /* GC entries */
  miPixel fgPixel;		/* color of pen, for most drawing operations */
  miPixel bgPixel;		/* used only in drawing double-dashed lines */
  miFillStyle fillStyle;
  miFillRule  fillRule;		
  miJoinStyle joinStyle;	
  miCapStyle capStyle;		
  miLineStyle lineStyle;	
  unsigned int lineWidth;	/* line thickness in pixels(for `wide lines')*/
  double miterLimit;		/* X Windows uses 10.43 for this */
  miArcMode arcMode;		
  unsigned int *dash;		/* dash array (lengths of dashes in pixels) */
  int numInDashList;		/* length of dash array */
  int dashOffset;		/* pixel offset of first dash */
  /* for stippling and tiling */
  int **stipple;
  unsigned int stippleWidth;
  unsigned int stippleHeight;
  int stippleXorigin, stippleYorigin;
  miPixel **tile;               /* That's right - alpha composited tiles... 
                                   I haven't tested it though. :-) */
  unsigned int tileWidth;
  unsigned int tileHeight;
  int tileXorigin, tileYorigin;
} miGC;

/*----------------------------------------------------------------------*/
/* Declarations of internal functions */

/* internal, low-level raster operation (all painting of the 2-D array of
   miPixels goes through this) */
void miFillSpans (miGC *pGC, int n, const miIntPoint *ppt, const unsigned int *pwidth, miBool sorted);

/* wrappers for storage allocation, see g_mialloc.c */
void* mi_xcalloc (unsigned int nmemb, unsigned int size);
void* mi_xmalloc (unsigned int size);
void* mi_xrealloc (void* p, unsigned int size);
void  mi_xfree (void* p);
void* mi_xmemmove(void *dest, const void *src, unsigned int n);

/* misc. internal functions */
void miFillConvexPoly (miGC *pGC, int count, const miIntPoint *ptsIn);
void miFillGeneralPoly (miGC *pGC, int count, const miIntPoint *ptsIn);
void miStepDash (int dist, int *pDashIndex, const unsigned int *pDash, int numInDashList, int *pDashOffset);

/*----------------------------------------------------------------------*/

/* The drawing functions (i.e., entry points) supplied by the module. */

/* 1. Polyline-related drawing functions.

   The final three arguments of each are a `mode' (see below), a specified
   number of points, and an array of points, of the specified length.
   miFillPolygon draws a filled polygon, miPolyPoint draws a cloud of
   points, and miWideDash, miWideLine, miZeroDash, miZeroLine respectively
   draw a dashed wide polyline, an undashed (solid) wide polyline, a dashed
   zero-width polyline, and an undashed (solid) zero-width polyline.
   miWideDash and miWideLine simply invoke miZeroDash and miZeroLine if the
   line width in terms of pixels is zero.

   The final argument for miFillPolygon and miPolyPoint is an 
   (miIntPoint *), not a (const miIntPoint *), because if the mode is
   miCoordModePrevious, i.e. the points after the first are specified in
   relative coordinates, they rewrite the point array in absolute
   coordinates.  This should be fixed. */

/* Values for the `mode' argument (are the points in the points array,
   after the first point, specified in absolute or relative
   coordinates?). */
typedef enum { miCoordModeOrigin, miCoordModePrevious } miCoordMode;


/* Values for the `shape' argument of miFillPolygon(): general (i.e., not
   necessarily convex, with self-intersections OK), or convex and not
   self-intersecting?  Latter case can be drawn more rapidly. */
typedef enum { miComplex, miConvex } miPolygonShape;

void miFillPolygon (const miGC *pGC, miPolygonShape shape, miCoordMode mode, int count, miIntPoint *pPts);
void miPolyPoint (const miGC *pGC, miCoordMode mode, int npt, miIntPoint *pPts);
void miWideDash (const miGC *pGC, miCoordMode mode, int npt, const miIntPoint *pPts);
void miWideLine (const miGC *pGC, miCoordMode mode, int npt, const miIntPoint *pPts);
void miZeroDash (const miGC *pGC, miCoordMode mode, int npt, const miIntPoint *pPts);
void miZeroLine (const miGC *pGC, miCoordMode mode, int npt, const miIntPoint *pPts);

/* 2. Rectangle-related drawing functions.

   miPolyFillRect fills a specified number of rectangles, supplied as an
   array of miRectangles. */

void miPolyFillRect (const miGC *pGC, int nrectFill, const miRectangle *prectInit);

/* 3. Arc-related drawing functions.

   Each of these takes as argument a multi-arc, i.e. a list of elliptic
   arcs.  Here, an elliptic arc is a piece of an ellipse whose axes are
   aligned with the coordinate axes (see above).  The arcs are not required
   to be contiguous.

   miPolyArc draws a wide multi-arc, which may be dashed.  If the arcs are
   contiguous, they will be joined in the usual way.  miZeroPolyArc draws a
   zero-width multi-arc, which may be dashed, using a fast algorithm.
   (miPolyArc does not automatically invoke miZeroPolyArc if the line width
   is zero.)  miPolyFillArc draws a sequence of filled arcs. */

void miPolyArc (const miGC *pGC, int narcs, const miArc *parcs);
void miPolyFillArc (const miGC *pGC, int narcs, const miArc *parcs);
void miZeroPolyArc (const miGC *pGC, int narcs, const miArc *parcs);


/***** Functions written for Guppi *********/

/* Initialize a GC object. 
   The default values are similar to those of an X GC. */
void miInitGC(miGC* pGC);

/* Fill an array of row pointers with the start of each row in pixbuf.
   That is, pixbuf should contain rowlength*nrows pixels where each
   pixel is three unsigned char; bitmap should be able to hold nrows
   unsigned char*'s. bitmap is filled with pointers into pixbuf. In
   particular, you can use this to convert from a libart/GnomeCanvas
   RGB buffer to the bitmap this library expects. */
void miBitmapizePixbuf(unsigned char* pixbuf,  unsigned char* bitmap[], 
                       /* Rowstride and the number of columns may 
                          be different, for optimization purposes.
                          But rowstride must be >= ncolumns. */
                       unsigned int rowstride,
                       unsigned int nrows);

#ifdef __cplusplus
}
#endif

#endif /* MI_MI_H */
