// -*- C++ -*-

/* 
 * dataedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "dataedit.h"
#include "libguppi/scalardata.h"
#include "libguppi/labeldata.h"
#include "libguppi/datedata.h"
#include "guppidata.h"

DataEdit::DataEdit()
  : data_(0),
    window_(0),
    name_entry_(0),
    scalar_value_entry_(0),
    string_value_entry_(0),
    clist_(0),
    changing_name_(false),
    changing_value_(false),
    editing_row_(-1)
{
  group_edit_.set_store(guppi_datastore());
}

DataEdit::~DataEdit()
{
  if (window_ != 0) 
    {
      gtk_widget_destroy(window_);
      window_ = 0;
    }

  release_data();
}

void 
DataEdit::set_data(Data* d)
{
  if (d == data_) {
    if (window_ != 0)
      {
        guppi_raise_and_show(GTK_WINDOW(window_));
      }
    return;
  }

  release_data();

  data_ = d;

  if (data_ != 0)
    {
      ensure_widgets();
      data_->ref();
      data_->data_model.add_view(this);
      change_values(data_);
      change_name(data_, data_->name());
      update_entries();
      if (data_->is_a(Data::Group))
        {
          group_edit_.set_data(data_->cast_to_group());
          gtk_widget_hide(sw_);
          gtk_widget_show(group_edit_.widget());
        }
      else
        {
          group_edit_.set_data(0);
          gtk_widget_show(sw_);
          gtk_widget_hide(group_edit_.widget());
        }

      guppi_raise_and_show(GTK_WINDOW(window_));
    }
  else 
    {
      if (window_ != 0) 
        {
          gtk_widget_destroy(window_);
          window_ = 0;
        }
    }
}

void 
DataEdit::release_data()
{
  if (data_ != 0)
    {
      data_->data_model.remove_view(this);
      if (data_->unref() == 0) 
        delete data_;
      data_ = 0;
    }
}

void 
DataEdit::change_values(Data* d, const vector<guint> & which)
{
  if (d->is_a(Data::Scalar))
    {
      vector<guint>::const_iterator i = which.begin();
      while (i != which.end())
        {
          ScalarData* sd = data_->cast_to_scalar();
          double v = sd->get_scalar(*i);
          
          char buf[100];
          g_snprintf(buf, 99, "%g", v);
          gtk_clist_set_text(GTK_CLIST(clist_), *i, 0, buf);

          // sync edit entry
          if (editing_row_ == (gint)*i)
            update_entries();
          
          ++i;
        }
    }
  else if (d->is_a(Data::Label))
    {
      vector<guint>::const_iterator i = which.begin();
      while (i != which.end())
        {
          LabelData* ld = data_->cast_to_label();
          const string& v = ld->get_string(*i);
          
          gtk_clist_set_text(GTK_CLIST(clist_), *i, 0, v.c_str());

          // sync edit entry
          if (editing_row_ == (gint)*i)
            update_entries();
          
          ++i;
        }
    }
  else if (d->is_a(Data::Date))
    {
      vector<guint>::const_iterator i = which.begin();
      while (i != which.end())
        {
          DateData* dd = data_->cast_to_date();
          const string v = dd->get_string(*i);
          
          gtk_clist_set_text(GTK_CLIST(clist_), *i, 0, v.c_str());

          // sync edit entry
          if (editing_row_ == (gint)*i)
            update_entries();
          
          ++i;
        }
    }
}

void 
DataEdit::change_values(Data* d)
{
  if (changing_value_) return;

  if (window_ != 0)
    {
      if (d->is_a(Data::Group))
        {
          update_entries();
        }
      else
        {
          guppi_start_busy(window_);

          gtk_clist_freeze(GTK_CLIST(clist_));
      
          gtk_clist_clear(GTK_CLIST(clist_));
      
          if (d->is_a(Data::Scalar))
            {
              ScalarData* sd = d->cast_to_scalar();
              gsize N = d->size();
              const double* scalars = sd->scalars();
              if (scalars == 0)
                {
                  g_assert(N == 0);
                  // do nothing
                }
              else
                {
                  char buf[100];
              
                  gsize i = 0;
                  while (i < N)
                    {
                      g_snprintf(buf, 99, "%g", scalars[i]);
                      char* text[1];
                      text[0] = buf;
                  
                      gint row = gtk_clist_append(GTK_CLIST(clist_), text);
                  
                      ++i;
                    }
                }
            }
          else if (d->is_a(Data::Label))
            {
              LabelData* ld = d->cast_to_label();
          
              gsize N = d->size();

              gsize i = 0;
              while (i < N)
                {
                  const char* text[1];
                  text[0] = ld->get_string(i).c_str();

                  gint row = gtk_clist_append(GTK_CLIST(clist_), 
                                              (gchar**)text);

                  ++i;
                }
            }
          else if (d->is_a(Data::Date))
            {
              DateData* dd = d->cast_to_date();
          
              gsize N = d->size();

              gsize i = 0;
              while (i < N)
                {
                  string v = dd->get_string(i);
                  const char* text[1];
                  text[0] = v.c_str();

                  gint row = gtk_clist_append(GTK_CLIST(clist_), 
                                              (gchar**)text);

                  ++i;
                }
            }
      
          gtk_clist_thaw(GTK_CLIST(clist_));

          guppi_end_busy(window_);

          if (static_cast<gsize>(editing_row_) >= d->size()) 
            editing_row_ = -1;

          update_entries();
        }
    }
}

void 
DataEdit::change_name(Data* d, const string & name)
{
  if (changing_name_) return;

  if (window_ != 0)
    {
      changing_name_ = true;
      gtk_entry_set_text(GTK_ENTRY(name_entry_), name.c_str());

      gchar buf[256];
      g_snprintf(buf, 255, _("Guppi Data: %s"), name.c_str());

      gtk_window_set_title(GTK_WINDOW(window_), buf);
      changing_name_ = false;
    }
}

void 
DataEdit::destroy_model()
{
  data_ = 0;
}


void
DataEdit::ensure_widgets()
{
  if (window_ == 0)
    {
      window_ = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      
      guppi_setup_window(GTK_WINDOW(window_), "data_editor");

      GtkWidget* vbox = gtk_vbox_new(FALSE, 0);
      
      gtk_container_add(GTK_CONTAINER(window_), vbox);

      name_entry_ = gtk_entry_new();
      gtk_box_pack_start(GTK_BOX(vbox), name_entry_, FALSE, FALSE, 0);

      GtkObject* adj = gtk_adjustment_new(0.0, 
                                          -G_MAXFLOAT, 
                                          G_MAXFLOAT, 
                                          10.0,
                                          100.0,
                                          100.0);
      scalar_value_entry_ = gtk_spin_button_new(GTK_ADJUSTMENT(adj), 2.0, 5);
      gtk_box_pack_start(GTK_BOX(vbox), scalar_value_entry_, FALSE, FALSE, 0);
      
      string_value_entry_ = gtk_entry_new();
      gtk_box_pack_start(GTK_BOX(vbox), string_value_entry_, FALSE, FALSE, 0);

      clist_ = gtk_clist_new(1);

      sw_ = gtk_scrolled_window_new(NULL,NULL);
      gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw_),
                                     GTK_POLICY_AUTOMATIC,
                                     GTK_POLICY_AUTOMATIC);
      gtk_container_add(GTK_CONTAINER(sw_), clist_);

      // Add both, but only one will be visible
      gtk_box_pack_start(GTK_BOX(vbox), sw_, TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(vbox), group_edit_.widget(), TRUE, TRUE, 0);

      gtk_signal_connect(GTK_OBJECT(window_),
                         "delete_event",
                         GTK_SIGNAL_FUNC(delete_event_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(window_),
                         "key_press_event",
                         GTK_SIGNAL_FUNC(key_event_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(clist_),
                         "select_row",
                         GTK_SIGNAL_FUNC(select_row_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(name_entry_),
                         "changed",
                         GTK_SIGNAL_FUNC(name_entry_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(scalar_value_entry_),
                         "changed",
                         GTK_SIGNAL_FUNC(value_entry_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(string_value_entry_),
                         "changed",
                         GTK_SIGNAL_FUNC(value_entry_cb),
                         this);

      // hack because of a bug in WindowMaker or something; 
      // window doesn't show up on the screen (but appears in 
      // the window list)
      gtk_window_set_position(GTK_WINDOW(window_), GTK_WIN_POS_MOUSE);

      gtk_widget_show_all(vbox);
    }
}

void 
DataEdit::update_entries()
{
  if (window_ != 0 && editing_row_ >= 0)
    {
      changing_value_ = true;
      if (data_->is_a(Data::Scalar))
        {
          ScalarData* sd = data_->cast_to_scalar();
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(scalar_value_entry_), sd->get_scalar(editing_row_));
          gtk_widget_hide(string_value_entry_);
          gtk_widget_show(scalar_value_entry_);
        }
      else if (data_->is_a(Data::Label))
        {
          LabelData* ld = data_->cast_to_label();
          gtk_entry_set_text(GTK_ENTRY(string_value_entry_),
                             ld->get_string(editing_row_).c_str());
          gtk_widget_hide(scalar_value_entry_);
          gtk_widget_show(string_value_entry_);
        }
      else
        {
          // DataGroup, or other unknown
          gtk_widget_hide(scalar_value_entry_);
          gtk_widget_hide(string_value_entry_);
        }
      changing_value_ = false;
    }
  else
    {
      gtk_widget_hide(scalar_value_entry_);
      gtk_widget_hide(string_value_entry_);
    }
}

gint 
DataEdit::delete_event_cb(GtkWidget* w, GdkEventAny* event, gpointer data)
{
  DataEdit* de = static_cast<DataEdit*>(data);

  return de->delete_event();
}

gint 
DataEdit::delete_event()
{
  gtk_widget_destroy(window_);

  window_ = 0;

  return TRUE;
}

gint 
DataEdit::key_event_cb(GtkWidget* w, GdkEventKey* event, gpointer data)
{
  DataEdit* de = static_cast<DataEdit*>(data);

  return de->key(event);
}

gint 
DataEdit::key(GdkEventKey* event)
{
  switch (event->keyval)
    {
    case GDK_Escape:
      delete_event();
      return TRUE;
      break;
    default:
      break;
    }
  return FALSE;
}


void 
DataEdit::select_row_cb(GtkWidget* w, int row, int col, 
                        GdkEvent* event, gpointer data)
{
  DataEdit* de = static_cast<DataEdit*>(data);

  de->select_row(row);
}

void 
DataEdit::select_row(int row)
{
  editing_row_ = row;
  
  update_entries();
}

void 
DataEdit::name_entry_cb(GtkWidget* w, gpointer data)
{
  DataEdit* de = static_cast<DataEdit*>(data);
  de->name_entry_changed();
}

void 
DataEdit::name_entry_changed()
{
  if (changing_name_) return;

  changing_name_ = true;
  if (data_ != 0)
    {
      gchar* text = gtk_entry_get_text(GTK_ENTRY(name_entry_));
      gtk_window_set_title(GTK_WINDOW(window_), text);
      data_->set_name(text);
    }
  changing_name_ = false;
}

void 
DataEdit::value_entry_cb(GtkWidget* w, gpointer data)
{
  DataEdit* de = static_cast<DataEdit*>(data);
  de->value_entry_changed();
}

void 
DataEdit::value_entry_changed()
{
  if (changing_value_) return;

  changing_value_ = true;
  if (data_ != 0 && editing_row_ >= 0)
    {
      if (data_->is_a(Data::Scalar))
        {
          ScalarData* sd = data_->cast_to_scalar();
          double v = 
            gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(scalar_value_entry_));
          sd->set_scalar(editing_row_, v);
          char buf[100];
          g_snprintf(buf, 99, "%g", v);
          gtk_clist_set_text(GTK_CLIST(clist_), editing_row_, 0, buf);
        }
      else if (data_->is_a(Data::Label))
        {
          LabelData* ld = data_->cast_to_label();
          const gchar* text = gtk_entry_get_text(GTK_ENTRY(string_value_entry_));
          ld->set_string(editing_row_, text);
          gtk_clist_set_text(GTK_CLIST(clist_), editing_row_, 0, text);
        }
    }
  changing_value_ = false;
}
