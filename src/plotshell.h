// -*- C++ -*-

/* 
 * plotshell.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTSHELL_H
#define GUPPI_PLOTSHELL_H

// A "shell" - a toplevel window that can contain the embeddable Plot
// object. Eventually maybe via CORBA, but not for now.

#include "libguppi/plot.h"

class PlotList;

class PlotShell : public Plot::View {
public:
  PlotShell();
  virtual ~PlotShell();

  GtkWidget* widget() { return app_; }

  // PlotShell takes ownership of the plot
  void set_plot(Plot* p);

  Plot* plot() { return plot_; }

  // Plot::View
  virtual void change_size(Plot* p);
  virtual void change_status(Plot* p, const string& status);
  virtual void change_name(Plot* p, const string& name);

  // ::View
  virtual void destroy_model();

  // Ignore
  static void zoom_cb(GtkWidget* w, gpointer data);
  static void toolbox_cb(GtkWidget* w, gpointer data);
  static void autosize_cb(GtkWidget* w, gpointer data);
  static void newview_cb(GtkWidget* w, gpointer data);

  void set_plot_edge(double edge);

  void set_snap_size_mode(bool setting) { snap_size_mode_ = setting; }

protected:
  friend class PlotList;
  void set_owner(PlotList* pl);

private:
  Plot* plot_;

  GtkWidget* app_;
  GtkWidget* canvas_;
  GtkWidget* appbar_;

  static gint delete_event_cb(GtkWidget* w, GdkEventAny* event, gpointer data);
  gint delete_event();
  static gint key_event_cb(GtkWidget* w, GdkEventKey* event, gpointer data);
  gint key(GdkEventKey* event);
  static void allocation_cb(GtkWidget* w, GtkAllocation* a, gpointer data);
  void allocation(GtkAllocation* a);

  void zoom(double factor);
  void show_toolbox();

  PlotList* owner_;

  // whether to keep the plot item snapped to the visible canvas size
  bool snap_size_mode_;

  // This must also be set for snap_size_mode_ to work. 
  // it allows us to avoid snap size in some cases (first appearance
  //  of a new view on an old plot)
  bool set_size_on_allocation_;

  // space around the plot added to the scroll region.
  double edge_;

  // The GtkMenu for the popup, so we can append stuff to it.
  GtkWidget* popup_;

  struct zoom_cbdata {
    guint index;
    PlotShell* ps;
  };
  
  struct zoom_cbdata* zcbdata_;

  static void menuentry_activate_cb(GtkWidget* mi, gpointer data);
};

#endif

