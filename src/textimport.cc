// -*- C++ -*-

/* 
 * textimport.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "textimport.h"

#include "libguppi/scalardata.h"
#include "libguppi/dataimport.h"
#include "guppidata.h"

#include "goose/RealSet.h"

#include <errno.h>

class ProgressDialog : public GooseProgress {
public:
  ProgressDialog(const char* title);
  virtual ~ProgressDialog();

  virtual void start(); // Fire up the progress meter
  virtual void stop();  // shut it down

  // If we set percent, we know at this time how much is remaining.
  // If we pulse, we don't.
  virtual void set_percent(double percent); // [0.0,1.0]
  virtual void pulse();

  virtual bool cancelled() { return cancelled_; }

  virtual void set_operation(const string& opname);

private:
  GtkWidget* dialog_;
  GtkWidget* bar_;
  GtkWidget* label_;

  bool cancelled_;
};


// fillme should be empty if an error is returned.
gchar*
guppi_text_import(const string& filename, vector<Data*> & fillme, bool with_gui)
{
  StackWarningController wc;    
  vector<string> errors;
  vector<vector<double> > columns;
  
  wc.set_see_warnings(10); // at most 10 warnings are displayed.
  
  ProgressDialog pd(_("Importing file..."));


  guppi_import_doubles(filename.c_str(),
                       columns,
                       wc,
                       errors,
                       &pd);
  
  if (!errors.empty())
    {
      string all_errors;
      
      vector<string>::const_iterator ei = errors.begin();
      while (ei != errors.end())
        {
          all_errors += *ei;
          all_errors += "\n";
          
          ++ei;
        }
      
      return g_strdup(all_errors.c_str());
    }
  else 
    {
      guint i = 0;
      while (i < columns.size())
        {
          g_debug("%u values in column %u\n", columns[i].size(), i);

          ScalarData* sd = new ScalarData;

          // This is gratuitously slow; should be just a memcpy
          vector<double>::const_iterator ci = columns[i].begin();
          while (ci != columns[i].end())
            {
              sd->add(*ci);
              ++ci;
            }

          char* tmp = g_strdup_printf(_("Imported Data %u"), i);
          sd->set_name(tmp);
          g_free(tmp);

          fillme.push_back(sd);

          ++i;
        }
    }

  string all_warnings;
  
  const vector<string>& warns = wc.get_warnings();
  vector<string>::const_iterator wi = warns.begin();
  while (wi != warns.end())
    {
      all_warnings += *wi;
      all_warnings += "\n";
      
      ++wi;
    }

  if (!all_warnings.empty() && with_gui)
    {
      GtkWidget* dialog = gnome_warning_dialog(all_warnings.c_str());
      guppi_setup_dialog(dialog);
    }

  return 0;
}

void
guppi_gui_text_import()
{
  GtkWidget * dialog;
  
  dialog = gnome_dialog_new(_("Guppi: Import Text File"), 
                            GNOME_STOCK_BUTTON_OK,
                            GNOME_STOCK_BUTTON_CANCEL,
                            NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog), TRUE);

  guppi_setup_dialog(dialog);

  GtkWidget* fileentry = gnome_file_entry_new("guppi:guppi_textimport_history",
                                              _("Guppi - Browse Files For Text Import"));

  gnome_dialog_editable_enters(GNOME_DIALOG(dialog), 
                               GTK_EDITABLE(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(fileentry))));
  gnome_dialog_set_default(GNOME_DIALOG(dialog), GNOME_OK);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),
                     fileentry,
                     TRUE, TRUE, GNOME_PAD);
  
  gtk_widget_show_all(dialog);

  int reply = gnome_dialog_run(GNOME_DIALOG(dialog));
  
  if (reply == GNOME_OK)
    {      
      gchar* s = 
        gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(fileentry),
                                       TRUE);
      
      string file;

      if (s != 0)
        file = s;

      g_free(s);
      s = 0;

      string error;

      if (!file.empty())
        {
          vector<Data*> imported;

          gchar* problem = guppi_text_import(file, imported, true);

          if (problem)
            {
              g_assert(imported.empty());
              error = problem;
              g_free(problem);
            }
          else
            {
              vector<Data*>::iterator di = imported.begin();
              while (di != imported.end())
                {
                  guppi_datastore()->insert(*di);

                  ++di;
                }
            }
        }
      else 
        {
          error = _("No such file or no filename given.");
        }

      if (error.size() > 0)
        {
          GtkWidget* edialog = gnome_error_dialog(error.c_str());
          guppi_setup_dialog(edialog);
        }
    }
  
  gtk_widget_destroy(dialog);  
}


static gboolean close_cb(GtkWidget* dialog, gpointer data)
{
  bool* cancelled = static_cast<bool*>(data);

  *cancelled = true;

  return TRUE;
}

ProgressDialog::ProgressDialog(const char* title)
  : dialog_(0), bar_(0), label_(0), cancelled_(false)
{
  dialog_ = gnome_dialog_new(title,
                             GNOME_STOCK_BUTTON_CANCEL, 
                             NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog_), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog_), TRUE);

  gtk_signal_connect(GTK_OBJECT(dialog_), "close",
                     GTK_SIGNAL_FUNC(close_cb),
                     &cancelled_);

  guppi_setup_dialog(dialog_);

  gtk_window_set_default_size(GTK_WINDOW(dialog_), 300, -1);

  bar_ = gtk_progress_bar_new();

  gtk_progress_set_show_text(GTK_PROGRESS(bar_), TRUE);

  label_ = gtk_label_new("");

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox),
                     label_, 
                     TRUE, TRUE, GNOME_PAD);
  
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox),
                     bar_, 
                     TRUE, TRUE, GNOME_PAD);

  gtk_widget_show_all(dialog_);
}

ProgressDialog::~ProgressDialog()
{
  gtk_widget_destroy(dialog_);
  dialog_ = bar_ = label_ = 0;
}

void
ProgressDialog::start()
{
  if (GTK_PROGRESS(bar_)->activity_mode)
    gtk_progress_set_activity_mode(GTK_PROGRESS(bar_), FALSE);

  gtk_progress_set_percentage(GTK_PROGRESS(bar_), 0.0);

  while (gtk_events_pending())
    gtk_main_iteration();
}

void
ProgressDialog::stop()
{
  if (GTK_PROGRESS(bar_)->activity_mode)
    gtk_progress_set_activity_mode(GTK_PROGRESS(bar_), FALSE);

  gtk_progress_set_percentage(GTK_PROGRESS(bar_), 1.0);

  while (gtk_events_pending())
    gtk_main_iteration();
}

void
ProgressDialog::set_percent(double percent)
{
  if (GTK_PROGRESS(bar_)->activity_mode)
    gtk_progress_set_activity_mode(GTK_PROGRESS(bar_), FALSE);

  gtk_progress_set_percentage(GTK_PROGRESS(bar_), percent);

  while (gtk_events_pending())
    gtk_main_iteration();
}

void
ProgressDialog::pulse()
{
  if (!GTK_PROGRESS(bar_)->activity_mode)
    gtk_progress_set_activity_mode(GTK_PROGRESS(bar_), TRUE);

  // This is just to trigger motion of the activity block.
  //  Really lame Gtk interface.
  gfloat new_val;
  GtkAdjustment *adj;
  
  adj = GTK_PROGRESS(bar_)->adjustment;
  
  new_val = adj->value + 1;
  if (new_val > adj->upper)
    new_val = adj->lower;
  
  gtk_progress_set_value (GTK_PROGRESS (bar_), new_val);

  while (gtk_events_pending())
    gtk_main_iteration();
}

void 
ProgressDialog::set_operation(const string& opname)
{
  if (label_ != 0)
    gtk_label_set(GTK_LABEL(label_), opname.c_str());

  while (gtk_events_pending())
    gtk_main_iteration();
}


// Old cruft
#if 0
#include "goose/AsciiImport.h"
// If we return an error, then fillme should remain empty.
gchar*
guppi_text_import(const string& filename, vector<Data*> & fillme, bool with_gui)
{
  FILE* f = fopen(filename.c_str(), "r");
  
  if (f == NULL) 
    {
      gchar buf[256];
      g_snprintf(buf, 256, _("Unable to open `%s', %s"), filename.c_str(), strerror(errno)); 
      return g_strdup(buf);
    }


  try {
    StackWarningController wc;    
    
    wc.set_see_warnings(10); // at most 10 warnings are displayed.

    ProgressDialog pd(_("Importing file..."));

    FILEAsciiReader car(f, wc);

    AsciiImport ai(car, wc, &pd);
  
    ai.autosetup();
  
    ai.import();
  
    const vector<string>& names = ai.headings();
    const vector<DataSet*>& sets = ai.datasets();
  
    vector<string>::const_iterator ni = names.begin();
    vector<DataSet*>::const_iterator i = sets.begin();
    while (i != sets.end())
      {
        RealSet* rs = 0; 
          
        if ((*i)->type() == RealSet::typecode)
          rs = static_cast<RealSet*>(*i);
        else 
          {
            wc.warn("Non-scalar column ignored in import file");
            // it will never be gc'd since we aren't creating a smob 
            //  from it, so nuke it.
            delete *i;
          }
              
        if (ni != names.end())
          {
            if (rs != 0)
              {
                if ((*ni).empty())
                  rs->set_label(_("Imported Data"));
                else
                  rs->set_label(*ni);
              }
            ++ni;
          }

        if (rs != 0)
          {
            Data* d = new ScalarData(rs);
            fillme.push_back(d);
          }
          
        ++i;
      }

    // Clean up the AsciiImport
    AsciiImport::delete_dataconverters(ai.dataconverters());

    string all_warnings;

    const vector<string>& warns = wc.get_warnings();
    vector<string>::const_iterator wi = warns.begin();
    while (wi != warns.end())
      {
        all_warnings += *wi;
        all_warnings += "\n";
        
        ++wi;
      }

    if (with_gui)
      {
        GtkWidget* dialog = gnome_warning_dialog(all_warnings.c_str());
        guppi_setup_dialog(dialog);
      }
  }
  catch (Exception & e)
    {
      fclose(f); // check error?
      return g_strconcat(_("Failed to import: "), e.what().c_str(), NULL);
    }
  catch (...)
    {
      // What the fuck, over
      g_warning("Should not happen, unknown exception in text import");

      fclose(f);

      return g_strdup(_("Text import failed mysteriously; some sort of bug in the software."));
    }

  fclose(f);

  return 0;
}
#endif
