// -*- C++ -*-

/* 
 * axis.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "axis.h"

#include "vfont.h"
#include "util.h"

Axis::Axis(const Axis& src)
  : start_(src.start_), stop_(src.stop_),
    ticks_(src.ticks_), label_(src.label_),
    label_color_(src.label_color_),
    line_color_(src.line_color_),
    pos_(src.pos_),
    label_font_(src.label_font_),
    default_tick_font_(src.default_tick_font_),
    line_(src.line_), 
    hidden_(src.hidden_), label_hidden_(src.label_hidden_)
{
  if (label_font_ != 0)
    label_font_->ref();

  if (default_tick_font_ != 0)
    default_tick_font_->ref();
}

Axis& 
Axis::operator=(const Axis& src)
{
  if (&src == this)
    return *this;

  start_ = src.start_;
  stop_ = src.stop_;
  ticks_ = src.ticks_;
  label_ = src.label_;
  label_color_ = src.label_color_;
  line_color_ = src.line_color_;
  pos_ = src.pos_;
  line_ = src.line_;
  hidden_ = src.hidden_;
  label_hidden_ = src.label_hidden_;

  if (label_font_ != 0)
    if (label_font_->unref() == 0)
      delete label_font_;

  label_font_ = src.label_font_;
  
  if (label_font_ != 0)
    label_font_->ref();

  
  if (default_tick_font_ != 0)
    if (default_tick_font_->unref() == 0)
      delete default_tick_font_;

  default_tick_font_ = src.default_tick_font_;
  
  if (default_tick_font_ != 0)
    default_tick_font_->ref();  

  return *this;
}

Axis::~Axis()
{
  if (label_font_ != 0)
    {
      if (label_font_->unref() == 0)
        delete label_font_;
    }

  if (default_tick_font_ != 0)
    {
      if (default_tick_font_->unref() == 0)
        delete default_tick_font_;
    }
}

VFont* 
Axis::label_font() const
{
  return label_font_;
}

void 
Axis::set_label_font(VFont* vfont)
{
  if (vfont == label_font_)
    return;

  if (vfont != 0)
    {
      vfont->ref();
    }

  if (label_font_ != 0)
    {
      if (label_font_->unref() == 0)
        delete label_font_;
      
      label_font_ = 0;
    }

  label_font_ = vfont;
}

VFont* 
Axis::default_tick_font() const
{
  return default_tick_font_;
}

void 
Axis::set_default_tick_font(VFont* vfont)
{
  if (vfont == default_tick_font_)
    return;

  if (vfont != 0)
    {
      vfont->ref();
    }

  if (default_tick_font_ != 0)
    {
      if (default_tick_font_->unref() == 0)
        delete default_tick_font_;
      
      default_tick_font_ = 0;
    }

  default_tick_font_ = vfont;
}

