// -*- C++ -*-

/* 
 * gfont.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gfont.h"

#include "libgnomeprint/gnome-font.h"

#include <math.h>

// Eventually we might have two kinds of internal, one based on t1lib
// and one using gnome-print. Or we might pick one. Anyway, we're
// keeping this info nice and private.
class GFontInternal {
public:
  ~GFontInternal();

  double string_width(const gchar* str) const;
  double font_height() const;
  GdkFont* gdk_font(double scale);

  const gchar* font_name() const;

  static GFontInternal* get_font(const gchar* name, GFont::FontWeightType weight, bool italic, gint size);

  GnomeFont* gnome_font() { return font_; }

private:  
  // const has no meaning for the gnome-print API
  mutable GnomeFont* font_;
  mutable GnomeDisplayFont* cached_display_font_;
  mutable GdkFont* fallback_;

  GFontInternal(GnomeFont* font);
  GFontInternal();
  friend class FooBarBaz; // eliminate egcs warning about private constructors
};

static GHashTable* font_hash = 0;

GFont::GFont(GFontInternal* i)
  : internal_(i)
{


}

GFont::~GFont()
{
  delete internal_;
}

double 
GFont::string_width(const gchar* str) const
{
  return internal_->string_width(str);
}

double 
GFont::font_height() const
{
  return internal_->font_height();
}

GdkFont* 
GFont::gdk_font(double scale)
{
  return internal_->gdk_font(scale);
}

const gchar* 
GFont::font_name() const
{
  return internal_->font_name();
}

GFont* 
GFont::get_font(const gchar* name, FontWeightType weight, bool italic, gint size)
{
  GFontInternal* internal = GFontInternal::get_font(name, weight, italic, size);

  if (internal != 0)
    return new GFont(internal);
  else 
    return 0;
}

GnomeFont* 
GFont::gnome_font()
{
  return internal_->gnome_font();
}

//////////////////

static GFontList* guppi_fontlist = 0;

GFontList* 
guppi_get_font_list()
{
  if (guppi_fontlist == 0)
    {
      guppi_fontlist = new GFontList;
    }

  return guppi_fontlist;
}

void
guppi_shutdown_fonts()
{
  // This doesn't do anything meaningful for now
  // GFontList::unref() has taken over
}

GFontList::GFontList()
{
  g_return_if_fail(guppi_fontlist == 0);

  GList* fonts = gnome_font_family_list((GnomeFontClass*)gtk_type_class(gnome_font_get_type()));
  GList* tmp = fonts;

  while (tmp != NULL)
    {
      GFont* gf = GFont::get_font(static_cast<gchar*>(tmp->data),
                                  GFont::Normal, false, 18);

      if (gf != 0)
        {
          list_.push_back(gf);
          gf->ref();
        }
      else
        g_warning("Failed to load font `%s'", (gchar*)tmp->data);

      tmp = g_list_next(tmp);
    }

  gnome_font_family_list_free(fonts);
}

GFontList::~GFontList()
{
  if (guppi_fontlist != 0)
    {
      g_assert(guppi_fontlist == this);
      guppi_fontlist = 0;
    }
      
  iterator i = begin();
  while (i != end())
    {
      if ((*i)->unref() == 0)
        delete *i;

      ++i;
    }
}

guint 
GFontList::ref()
{
  return rc_.ref();
}

guint 
GFontList::unref()
{
  g_assert(guppi_fontlist == this);

  guint count = rc_.unref();

  // We're about to be deleted; unset the 
  //  static variable.
  if (count == 0)
    guppi_fontlist = 0;

  return count;
}


///////////

GFontInternal*
GFontInternal::get_font(const gchar* name, GFont::FontWeightType weight, bool italic, gint size)
{
  GnomeFont* font;
  
  GnomeFontWeight gnome_weight;

  switch (weight)
    {
    case GFont::Normal:
      gnome_weight = GNOME_FONT_BOOK;
      break;
    case GFont::Bold:
      gnome_weight = GNOME_FONT_BOLD;
      break;
    case GFont::Light:
      gnome_weight = GNOME_FONT_LIGHT;
      break;
    default:
      g_warning("Invalid font weight in %s", __FUNCTION__);
      gnome_weight = GNOME_FONT_BOOK;
      break;
    }
  
  font = gnome_font_new_closest(name, gnome_weight, italic, size);

  if (font != 0)
    return new GFontInternal(font);
  else
    return 0;
}

GFontInternal::GFontInternal(GnomeFont* font)
  : font_(font), cached_display_font_(0), fallback_(0)
{
  
}

GFontInternal::~GFontInternal()
{
  if (font_ != 0)
    gtk_object_unref(GTK_OBJECT(font_));

  if (fallback_ != 0)
    gdk_font_unref(fallback_);
}

double 
GFontInternal::string_width(const gchar* str) const
{
  if (font_ != 0)
    return gnome_font_get_width_string(font_, str);
  else 
    return 0.0;
}

double
GFontInternal::font_height() const
{
  if (font_ != 0)
    return font_->size; // I guess; I'm not sure this is right.
  else 
    return 0.0;
}

const gchar* 
GFontInternal::font_name() const
{
  if (font_ != 0)
    return font_->fontmap_entry->familyname;
  else
    return "[***Invalid Font***]";
}

GdkFont* 
GFontInternal::gdk_font(double scale) 
{
  if (font_ == 0)
    return 0;

  // FIXME This does the wrong thing wrt the scale; GnomeFont 
  //  is where scaling is actually kept.

  if (cached_display_font_ == 0 || 
      (fabs(cached_display_font_->scale - scale) < 1e-10))
    {
      // Note that we don't own the memory for cached_display_font_, 
      // it's in an internal gnome-print hash.
      cached_display_font_ = gnome_font_get_display_font(font_);
    }
  
  if (cached_display_font_ != 0 && cached_display_font_->gdk_font != 0)
    {
      return cached_display_font_->gdk_font;
    }
  else 
    {
      if (fallback_ == 0)
        {
          // we own the implicit initial reference count
          fallback_ = gdk_font_load("-*-*-medium-r-*-*-*-*-*-*-*-*-*-*");
          if (fallback_ == 0)
            fallback_ = gdk_font_load("fixed");
          if (fallback_ == 0)
            fallback_ = gdk_font_load("helvetica");

          g_debug("Falling back to random X font because gnome-print returned no appropriate match for %s %s", font_name(), fallback_ ? "" : "(fallback failed to load!)");
        }

      return fallback_;
    }
}


