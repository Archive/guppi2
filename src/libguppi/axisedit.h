// -*- C++ -*-

/* 
 * axisedit.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_AXISEDIT_H
#define GUPPI_AXISEDIT_H

// The AxisEdit GUI is heavily ripped off of XmGrace...

#include "plotutils.h"
#include "gnome-util.h"
#include "callbacks.h"
#include "fontoption.h"

class AxisEdit {
public:
  AxisEdit();
  ~AxisEdit();

  GtkWidget* widget();

  // The callback is called when the axis is modified.
  void connect(CallbackList::Callback cb, gpointer data) { 
    cblist_.connect(cb,data); 
  }
  void disconnect(CallbackList::Callback cb) {
    cblist_.disconnect(cb);
  }

  // Apply the current GUI settings to this axis.
  void apply_to(Axis* a);

  // Update the GUI to show this axis
  void fill_from(const Axis& a);

private:
  Axis axis_;
  
  CallbackList cblist_;

  GtkWidget* widget_;

  GtkWidget* axis_page_;
  GtkWidget* global_tick_page_;
  GtkWidget* manual_tick_page_;

  GtkWidget* name_entry_;
  GtkWidget* hidden_name_toggle_;
  GtkWidget* hidden_toggle_;

  FontOption name_font_;

  GtkWidget* start_spin_;
  GtkWidget* stop_spin_;

  // changes since last apply
  bool dirty_;

  AxisEdit(const AxisEdit &);
  const AxisEdit& operator=(const AxisEdit&);

  static void font_selected(gpointer emitter, gpointer data);
  static void name_entry_changed(GtkWidget* entry, gpointer data);
  static void hidden_name_toggled(GtkWidget* toggle, gpointer data);
  static void hidden_axis_toggled(GtkWidget* toggle, gpointer data);
  static void start_changed(GtkWidget* spin, gpointer data);
  static void stop_changed(GtkWidget* spin, gpointer data);

  void mark_dirty();
};

#endif
