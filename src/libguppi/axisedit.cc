// -*- C++ -*-

/* 
 * axisedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "axisedit.h"

AxisEdit::AxisEdit()
  : cblist_((void*)this), widget_(0), 
    axis_page_(0), global_tick_page_(0), manual_tick_page_(0),
    name_entry_(0), hidden_name_toggle_(0), hidden_toggle_(0),
    start_spin_(0), stop_spin_(0), dirty_(false)
{
  // no need to disconnect, since the name_font_ is destroyed with us.
  name_font_.connect(font_selected, this); 
}

AxisEdit::~AxisEdit()
{

}

static GtkWidget* 
make_framed_widget(const gchar* label, GtkWidget* child)
{
  g_return_val_if_fail(GTK_IS_CONTAINER(child), NULL);

  GtkWidget* frame = gtk_frame_new(label);

  gtk_container_add(GTK_CONTAINER(frame), child);

  gtk_container_set_border_width(GTK_CONTAINER(child), GNOME_PAD_SMALL);

  return frame;
}

GtkWidget* 
AxisEdit::widget()
{
  if (widget_ == 0)
    {
      GtkWidget* frame;
      GtkWidget* box;
      GtkWidget* vbox;

      widget_ = gtk_notebook_new();

      gtk_signal_connect(GTK_OBJECT(widget_),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &widget_);

      /////

      axis_page_ = gtk_table_new(1, 4, FALSE);


      vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

      name_entry_ = gtk_entry_new();

      gtk_signal_connect(GTK_OBJECT(name_entry_), "changed",
                         GTK_SIGNAL_FUNC(name_entry_changed), this);

      box = guppi_make_labelled_widget(_("Name:"), name_entry_);

      gtk_box_pack_start(GTK_BOX(vbox), box, TRUE, TRUE, 0);

      gtk_box_pack_start(GTK_BOX(vbox), name_font_.widget(), TRUE, TRUE, 0);
      
      hidden_name_toggle_ = gtk_check_button_new_with_label(_("Hide Axis Name"));

      gtk_signal_connect(GTK_OBJECT(hidden_name_toggle_),
                         "toggled",
                         GTK_SIGNAL_FUNC(hidden_name_toggled),
                         this);

      gtk_box_pack_start(GTK_BOX(vbox), hidden_name_toggle_, TRUE, TRUE, 0);
      
      frame = make_framed_widget(_("Axis name"), vbox);

      gtk_table_attach(GTK_TABLE(axis_page_), 
                       frame,
                       0, 1, // left/right
                       1, 2, // top/bottom
                       (GtkAttachOptions)(GTK_FILL | GTK_EXPAND),
                       (GtkAttachOptions)(GTK_FILL | GTK_EXPAND),
                       GNOME_PAD_SMALL, GNOME_PAD_SMALL);


      vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

      frame = make_framed_widget(_("General"), vbox);

      hidden_toggle_ = gtk_check_button_new_with_label(_("Hide Entire Axis"));

      gtk_signal_connect(GTK_OBJECT(hidden_toggle_),
                         "toggled",
                         GTK_SIGNAL_FUNC(hidden_axis_toggled),
                         this);

      gtk_box_pack_start(GTK_BOX(vbox), hidden_toggle_, TRUE, TRUE, 0);

      GtkObject* adj = gtk_adjustment_new(0.0, 
                                          -G_MAXFLOAT, 
                                          G_MAXFLOAT, 
                                          10.0,
                                          100.0,
                                          100.0);

      start_spin_ = gtk_spin_button_new(GTK_ADJUSTMENT(adj), 2.0, 5);

      adj = gtk_adjustment_new(0.0, 
                               -G_MAXFLOAT, 
                               G_MAXFLOAT, 
                               10.0,
                               100.0,
                               100.0);

      stop_spin_ = gtk_spin_button_new(GTK_ADJUSTMENT(adj), 2.0, 5);

      gtk_signal_connect(GTK_OBJECT(start_spin_),
                         "changed",
                         GTK_SIGNAL_FUNC(start_changed),
                         this);

      gtk_signal_connect(GTK_OBJECT(stop_spin_),
                         "changed",
                         GTK_SIGNAL_FUNC(stop_changed),
                         this);

      box = guppi_make_labelled_widget(_("Start:"), start_spin_);

      gtk_box_pack_start(GTK_BOX(vbox), box, TRUE, TRUE, 0);
      
      box = guppi_make_labelled_widget(_("Stop:"), stop_spin_);
      
      gtk_box_pack_start(GTK_BOX(vbox), box, TRUE, TRUE, 0);

      gtk_table_attach(GTK_TABLE(axis_page_), 
                       frame,
                       0, 1, // left/right
                       0, 1, // top/bottom
                       (GtkAttachOptions)(GTK_FILL | GTK_EXPAND),
                       (GtkAttachOptions)(GTK_FILL | GTK_EXPAND),
                       GNOME_PAD_SMALL, GNOME_PAD_SMALL);
                       
      gtk_notebook_append_page(GTK_NOTEBOOK(widget_), axis_page_,
                               gtk_label_new(_("Main")));

      /////

      global_tick_page_ = gtk_table_new(1, 4, FALSE);


      gtk_notebook_append_page(GTK_NOTEBOOK(widget_), global_tick_page_,
                               gtk_label_new(_("Ticks and Labels")));      

      manual_tick_page_ = gtk_table_new(1, 4, FALSE);

      gtk_notebook_append_page(GTK_NOTEBOOK(widget_), manual_tick_page_,
                               gtk_label_new(_("Manual Ticks")));      

      fill_from(axis_);

      gtk_widget_show_all(widget_);
    } 

  return widget_;
}

void 
AxisEdit::apply_to(Axis* a)
{
  g_return_if_fail(a != 0);

  *a = axis_;

  dirty_ = false;
}

void 
AxisEdit::fill_from(const Axis& a)
{
  // fill_from is also used to sync the widgetry with the axis_ 
  //  when the widgets are first created.
  if (&axis_ != &a)
    axis_ = a;

  if (widget_ != 0)
    {
      // Don't call callbacks for this
      gtk_signal_handler_block_by_data(GTK_OBJECT(name_entry_), this);
      gtk_signal_handler_block_by_data(GTK_OBJECT(hidden_name_toggle_), this);
      gtk_signal_handler_block_by_data(GTK_OBJECT(hidden_toggle_), this);
      gtk_signal_handler_block_by_data(GTK_OBJECT(start_spin_), this);
      gtk_signal_handler_block_by_data(GTK_OBJECT(stop_spin_), this);

      gtk_entry_set_text(GTK_ENTRY(name_entry_), axis_.label().c_str());

      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(hidden_name_toggle_),
                                  axis_.label_hidden());

      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(hidden_toggle_),
                                  axis_.hidden());

      gtk_spin_button_set_value(GTK_SPIN_BUTTON(start_spin_),
                                axis_.start());

      gtk_spin_button_set_value(GTK_SPIN_BUTTON(stop_spin_),
                                axis_.stop());

      gtk_signal_handler_unblock_by_data(GTK_OBJECT(name_entry_), this);
      gtk_signal_handler_unblock_by_data(GTK_OBJECT(hidden_name_toggle_), this);
      gtk_signal_handler_unblock_by_data(GTK_OBJECT(hidden_toggle_), this);
      gtk_signal_handler_unblock_by_data(GTK_OBJECT(start_spin_), this);
      gtk_signal_handler_unblock_by_data(GTK_OBJECT(stop_spin_), this);
    }
  
  dirty_ = false;
}


void 
AxisEdit::mark_dirty()
{
  dirty_ = true;
  cblist_.invoke();
}

void 
AxisEdit::font_selected(gpointer emitter, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)data;
  FontOption* fo = (FontOption*)emitter;
  
  if (fo == &(ae->name_font_))
    {
      GFont* gf = fo->get_font();
      ae->axis_.set_label_font(gf);
      ae->mark_dirty();
      g_debug("Selected font `%s' for axis label", gf ? gf->font_name() : "None");
    }
}

void 
AxisEdit::name_entry_changed(GtkWidget* entry, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)data;

  const gchar* txt = gtk_entry_get_text(GTK_ENTRY(entry));

  ae->axis_.set_label(txt);

  ae->mark_dirty();
}

void 
AxisEdit::hidden_name_toggled(GtkWidget* toggle, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)data;

  ae->axis_.set_label_hidden(GTK_TOGGLE_BUTTON(toggle)->active);

  ae->mark_dirty();
}

void 
AxisEdit::hidden_axis_toggled(GtkWidget* toggle, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)data;

  ae->axis_.set_hidden(GTK_TOGGLE_BUTTON(toggle)->active);

  ae->mark_dirty();
}

void 
AxisEdit::start_changed(GtkWidget* spin, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)data;

  ae->axis_.set_start(gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(spin)));

  ae->mark_dirty();
}

void 
AxisEdit::stop_changed(GtkWidget* spin, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)data;

  ae->axis_.set_stop(gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(spin)));

  ae->mark_dirty();
}
