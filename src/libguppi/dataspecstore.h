// -*- C++ -*-

/* 
 * dataspecstore.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATASPECSTORE_H
#define GUPPI_DATASPECSTORE_H

class DataGroupSpec;

#include "util.h"
#include "refcount.h"
#include <vector>

/// Object to store the available DataGroupSpec objects.
/// When we have an "editor" object to create and destroy these specs,
/// we'll need a model/view thing here similar to the one for DataStore

class DataGroupSpecStore {
public:
  DataGroupSpecStore();
  ~DataGroupSpecStore();
  
  typedef vector<DataGroupSpec*>::const_iterator const_iterator;
  typedef vector<DataGroupSpec*>::iterator iterator;

  void insert(DataGroupSpec* d);

  const_iterator begin() const;
  const_iterator end() const;

  iterator begin();
  iterator end();

  const_iterator find(DataGroupSpec* d) const;
  iterator find(DataGroupSpec* d);

  void erase(iterator i);
  void erase(iterator b, iterator e);

  gsize size() const { return specs_.size(); }

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

private:
  RC rc_;
  vector<DataGroupSpec*> specs_;  

  void grab_spec(DataGroupSpec* d);
  void release_spec(DataGroupSpec* d);
};

#endif
