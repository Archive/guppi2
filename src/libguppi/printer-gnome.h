// -*- C++ -*-

/* 
 * printer-gnome.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PRINTER_GNOME_H
#define GUPPI_PRINTER_GNOME_H

#include "gnome-util.h"
#include "printer.h"

struct _GnomePrinter;
struct _GnomePrintContext;

// gnome-print concrete subclass of Printer

class GPrinter : public Printer {
public:
  GPrinter();
  // use this constructor, please; GPrinter owns the GnomePrinter
  GPrinter(struct _GnomePrinter* printer);
  virtual ~GPrinter();

  virtual void newpath();
  virtual void moveto(double x, double y);
  virtual void lineto(double x, double y);
  virtual void curveto(double x1, double y1, double x2, double y2, double x3, double y3);
  virtual void closepath();
  // alpha may not be supported.
  virtual void setcolor(const Color & c);

  virtual void fill();
  virtual void eofill();
  virtual void setlinewidth(double w);
  virtual void setmiterlimit(double limit);
  virtual void setlinejoin(int jointype);
  virtual void setlinecap(int captype);
  virtual void setdash(int n_values, double* values, double offset);
  virtual void strokepath();
  virtual void stroke();
  virtual void setfont(VFont* font);
  virtual void show(const char* text);
  virtual void concat(double matrix[6]);
  virtual void concat(const Affine&);
  virtual void setmatrix(double matrix[6]);
  virtual void setmatrix(const Affine&);
  virtual void translate(double x, double y);
  virtual void rotate(double degrees);
  virtual void scale(double xfactor, double yfactor);
  virtual void gsave();
  virtual void grestore();
  virtual void clip();
  virtual void eoclip();
  virtual void grayimage(const guchar* data, int width, int height, int rowstride);
  virtual void rgbimage(const guchar* data, int width, int height, int rowstride);
  virtual void showpage();

  virtual bool sync();

private:

  struct _GnomePrinter* printer_;
  struct _GnomePrintContext* context_;
  
  const GPrinter& operator=(const GPrinter&);
  GPrinter(const GPrinter &);

};

// this is just an alternative to the callback/data approach

class PrintAction {
public:
  virtual ~PrintAction() {}
  virtual void print(Printer* p) = 0;
};

void guppi_get_printer_and_print(PrintAction* pa, bool destroy_print_action);

#endif
