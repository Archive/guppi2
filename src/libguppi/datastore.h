// -*- C++ -*-

/* 
 * datastore.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATASTORE_H
#define GUPPI_DATASTORE_H

#include "data.h"
#include "view.h"

#include <vector>

#include <gnome-xml/tree.h>

/* We keep our Data objects in a DataStore. For now this is pretty
   much just a vector so some operations are hugely inefficient, but
   we might make it more efficient later, to handle large numbers of
   Data objects. */

class DataStore : public Data::View {
public:

  DataStore();
  virtual ~DataStore();

  typedef vector<Data*>::const_iterator const_iterator;
  typedef vector<Data*>::iterator iterator;

  void insert(Data* d);

  const_iterator begin() const;
  const_iterator end() const;

  iterator begin();
  iterator end();

  const_iterator find(Data* d) const;
  iterator find(Data* d);

  void erase(iterator i);
  void erase(iterator b, iterator e);

  xmlNodePtr xml(xmlNodePtr parent) const;
  void set_xml(xmlNodePtr node);
  void merge_xml(xmlNodePtr node);

  gsize size() const { return data_.size(); }

  class View : public ::View 
  {
  public:
    virtual void add_data(DataStore* ds, Data* d) = 0;
    virtual void remove_data(DataStore* ds, Data* d) = 0;
    // These are sort of questionable, since views could monitor each Data...
    virtual void change_data_values(DataStore* ds, 
                                    Data* d, 
                                    const vector<guint> & which) = 0;
    virtual void change_data_values(DataStore* ds, Data* d) = 0;
    virtual void change_data_name(DataStore* ds, Data* d, const string & name) = 0;
  };

  class Model : public ::Model<DataStore::View*> {
  public:
    void data_added(DataStore* ds, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->add_data(ds,d);
	++i;
      }
      unlock_views();
    }
    void data_removed(DataStore* ds, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->remove_data(ds,d);
	++i;
      }
      unlock_views();
    }

    void data_values_changed(DataStore* ds, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data_values(ds,d);
	++i;
      }
      unlock_views();
    }    


    void data_values_changed(DataStore* ds, Data* d, const vector<guint> & which) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data_values(ds,d,which);
	++i;
      }
      unlock_views();
    }    

    void data_name_changed(DataStore* ds, Data* d, const string & name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data_name(ds,d,name);
	++i;
      }
      unlock_views();
    }    

  };
  
  Model store_model;  

  virtual void change_values(Data* d, const vector<guint> & which) 
    {
      store_model.data_values_changed(this, d, which);
    }

  virtual void change_values(Data* d) 
    {
      store_model.data_values_changed(this, d);
    }
  
  virtual void change_name(Data* d, const string & name)
    {
      store_model.data_name_changed(this, d, name);
    }

  virtual void destroy_model() {} // we don't know which it is, plus we probably caused it ourselves

  

private:
  vector<Data*> data_;  

  void grab_data(Data* d);
  void release_data(Data* d);
};


#endif
