// -*- C++ -*-

/* 
 * canvas-axis.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomeui/gnome-canvas.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>
#include <libart_lgpl/art_rgb.h>
#include <libgnomeui/gnome-canvas-util.h>
/* #include <libgnomeui/gnome-canvas-text.h> */
#include "guppi-canvas-text.h"

#include "canvas-axis.h"
#include "util.h"
#include "rgbdraw.h"
#include "plotutils.h"
#include "gfont.h"

#include <math.h>

#include "guppi-math.h"

enum {
  ARG_0
};


static void guppi_axis_class_init (GuppiAxisClass *klass);
static void guppi_axis_init       (GuppiAxis      *ga);
static void guppi_axis_destroy    (GtkObject            *object);
static void guppi_axis_set_arg    (GtkObject            *object,
                                   GtkArg               *arg,
                                   guint                 arg_id);
static void guppi_axis_get_arg    (GtkObject            *object,
                                   GtkArg               *arg,
                                   guint                 arg_id);

static void   guppi_axis_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flaga);
static void   guppi_axis_realize     (GnomeCanvasItem *item);
static void   guppi_axis_unrealize   (GnomeCanvasItem *item);
static void   guppi_axis_draw        (GnomeCanvasItem *item, GdkDrawable *drawable,
                                      int x, int y, int width, int height);
static double guppi_axis_point       (GnomeCanvasItem *item, double x, double y,
                                      int cx, int cy, GnomeCanvasItem **actual_item);
static void   guppi_axis_translate   (GnomeCanvasItem *item, double dx, double dy);
static void   guppi_axis_bounds      (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);
static void   guppi_axis_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);


static GnomeCanvasItemClass *parent_class;


GtkType
guppi_axis_get_type (void)
{
  static GtkType ga_type = 0;

  if (!ga_type) {
    GtkTypeInfo ga_info = {
      "GuppiAxis",
      sizeof (GuppiAxis),
      sizeof (GuppiAxisClass),
      (GtkClassInitFunc) guppi_axis_class_init,
      (GtkObjectInitFunc) guppi_axis_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    ga_type = gtk_type_unique (gnome_canvas_item_get_type (), &ga_info);
  }

  return ga_type;
}

static void
guppi_axis_class_init (GuppiAxisClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = 
    (GnomeCanvasItemClass*)gtk_type_class (gnome_canvas_item_get_type ());

  object_class->destroy = guppi_axis_destroy;
  object_class->set_arg = guppi_axis_set_arg;
  object_class->get_arg = guppi_axis_get_arg;

  item_class->update = guppi_axis_update;
  item_class->realize = guppi_axis_realize;
  item_class->unrealize = guppi_axis_unrealize;
  item_class->draw = guppi_axis_draw;
  item_class->point = guppi_axis_point;
  item_class->translate = guppi_axis_translate;
  item_class->bounds = guppi_axis_bounds;

  item_class->render = guppi_axis_render;
}

static void
guppi_axis_init (GuppiAxis *ga)
{
  ga->info_func = NULL;
  ga->user_data = NULL;
  ga->axis_svp = NULL;
  ga->texts = NULL;
  ga->lines = NULL;
  ga->nticks = 0;
}

static void
destroy_texts(GSList* texts)
{
  if (texts == NULL) // strictly speaking we don't need to check this I guess
    return; 

  GSList* tmp = texts;
  while (tmp != NULL)
    {
      gtk_object_destroy(GTK_OBJECT(tmp->data));
      tmp = g_slist_next(tmp);
    }

  g_slist_free(texts);
}

static void
guppi_axis_destroy (GtkObject *object)
{
  GuppiAxis *ga;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GUPPI_IS_AXIS(object));

  ga = GUPPI_AXIS (object);

  if (ga->axis_svp != 0) 
    art_svp_free(ga->axis_svp);

  destroy_texts(ga->texts);

  if (ga->lines)
    g_free(ga->lines);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
get_info(GuppiAxis* ga, 
         const Axis** axis,
         const Rectangle** rect, 
         const Transform** trans)
{
  if (ga->info_func)
    (*ga->info_func)(ga, ga->user_data, axis, rect, trans);
  else
    {
      if (axis)
        *axis = 0;
      if (rect)
        *rect = 0;
      if (trans)
        *trans = 0;
    }
} 

static void
guppi_axis_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GuppiAxis *ga;

  item = GNOME_CANVAS_ITEM (object);
  ga = GUPPI_AXIS (object);

  switch (arg_id) {

  default:
    break;
  }
}

static void
guppi_axis_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GuppiAxis *ga;

  ga = GUPPI_AXIS (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void
guppi_axis_render (GnomeCanvasItem *item,
                   GnomeCanvasBuf *buf)
{
  GuppiAxis *ga;
  const Axis* axis;
  const Rectangle* rect; 
  const Transform* trans;

  ga = GUPPI_AXIS (item);

  get_info(ga, &axis, &rect, &trans);

  if (ga->axis_svp != 0)
    {
      gnome_canvas_render_svp (buf, ga->axis_svp, axis->line_color().rgba());
    }

  if (axis && trans && ga->lines)
    {
      const guint width = buf->rect.x1 - buf->rect.x0;
      const guint height = buf->rect.y1 - buf->rect.y0;

      RGB rgb(buf->buf, buf->buf_rowstride, 
              width, height);

      //      g_debug("Rendering axis %p to %u x %u buffer", ga, width, height);

      // Create a separate item to buffer affine
      Affine i2buf(ga->update_affine);
      i2buf.translate( - buf->rect.x0,
                       - buf->rect.y0 );

      const vector<Tick> & ticks = axis->ticks();

      // Now draw the tick marks.
      guint ti = 0;
      while (ti < ga->nticks)
        {
          Affine::Point t0;
          Affine::Point t1;          
              
          t0.x = ga->lines[ti*4];
          t0.y = ga->lines[ti*4+1];
          t1.x = ga->lines[ti*4+2];
          t1.y = ga->lines[ti*4+3];

#if 0
          g_debug("(render) Item coords %g,%g %g,%g", t0.x, t0.y, t1.x, t1.y);
#endif

          i2buf.transform(t0);
          i2buf.transform(t1);

          RGB::Context ctx(rgb);
          ctx.set_color(ticks[ti].color());

          // Start with signed int so we can clip the negative coords
          gint x1 = static_cast<gint>(rint(t0.x));
          gint y1 = static_cast<gint>(rint(t0.y));
          gint x2 = static_cast<gint>(rint(t1.x));
          gint y2 = static_cast<gint>(rint(t1.y));

#if 0
          g_debug("(render) Buf coords %d,%d %d,%d", x1, y1, x2, y2);
#endif

          rgb.paint_line(x1, y1, x2, y2, ctx);

          ++ti;
        }
    }

}

static void
guppi_axis_update (GnomeCanvasItem *item, double *update_affine, ArtSVP *clip_path, int flags)
{
  GuppiAxis *ga;
  const Axis* axis;
  const Rectangle* rect; 
  const Transform* trans;

  ga = GUPPI_AXIS (item);

  if (parent_class->update)
    (* parent_class->update) (item, update_affine, clip_path, flags);

  ga->update_affine.set(update_affine);

  if (item->canvas->aa) 
    {
      gnome_canvas_item_reset_bounds (item);

      if (ga->axis_svp != 0) 
        {
          art_svp_free(ga->axis_svp);
          ga->axis_svp = 0;
        }

      get_info(ga, &axis, &rect, &trans);

      // nothing to draw in this case
      if (axis == 0 || trans == 0 || rect == 0) return;

      ArtVpath vpath[3];
      ArtVpath *vpath2;
      double x0, y0, x1, y1;

      g_debug("Axis rect for %p is %g,%g   %g x %g",
              ga, rect->x(), rect->y(),
              rect->width(), rect->height());

      switch (axis->pos())
        {
        case PlotUtil::NORTH:
        case PlotUtil::SOUTH:
          x0 = trans->transform(axis->start());
          x1 = trans->transform(axis->stop());
          y0 = rect->y();
          y1 = rect->y();
          if (axis->pos() == PlotUtil::NORTH)
            {
              y0 += rect->height();
              y1 += rect->height();
            }
          break;
        case PlotUtil::WEST:
        case PlotUtil::EAST:
          y0 = trans->transform(axis->start());
          y1 = trans->transform(axis->stop());
          x0 = rect->x();
          x1 = rect->x();
          if (axis->pos() == PlotUtil::WEST)
            {
              x0 += rect->width();
              x1 += rect->width();
            }
          break;
        default:
          g_warning("Unplanned switch default");
          x0 = x1 = y0 = y1 = 0.0;
          break;
        }

      if ((fabs(x1 - x0) < PlotUtil::EPSILON) &&
          (fabs(y1 - y0) < PlotUtil::EPSILON)) return; // too small

      // FIXME clip to the rectangle

      vpath[0].code = ART_MOVETO;
      vpath[0].x = x0;
      vpath[0].y = y0;
      vpath[1].code = ART_LINETO;
      vpath[1].x = x1;
      vpath[1].y = y1;
      vpath[2].code = ART_END;
      vpath[2].x = 0;
      vpath[2].y = 0;

      g_debug("Updating canvas axis; line is %g,%g to %g,%g",
              x0,y0,x1,y1);

      vpath2 = art_vpath_affine_transform (vpath, update_affine);
      
      ArtSVP* svp = art_svp_vpath_stroke (vpath2,
                                          ART_PATH_STROKE_JOIN_MITER,
                                          ART_PATH_STROKE_CAP_BUTT,
                                          1.0, // width
                                          4,
                                          0.5);

      gnome_canvas_item_update_svp_clip (item, &ga->axis_svp, 
                                         svp, clip_path);
      art_free (vpath2);


      ///////////////// Now do the tick marks

      if (axis && trans)
        {
          const vector<Tick> & ticks = axis->ticks();

          // Make number of text items equal to the number of ticks.

          gint n_texts = g_slist_length(ga->texts);

          while (n_texts < gint(ticks.size()))
            {
              // This is so unbelievably broken (for example, doesn't show-hide right)
              GnomeCanvasItem* txt = 
                gnome_canvas_item_new(GNOME_CANVAS_GROUP(ga->item.parent),
                                      guppi_canvas_text_get_type(),
                                      "fill_color", "black",
                                      NULL);
          
              ga->texts = g_slist_prepend(ga->texts, (gpointer)txt);
              ++n_texts;
            }
      
          // Urgh, this is not efficient but the numbers are small
          while (n_texts > gint(ticks.size()))
            {
              GnomeCanvasItem* txt = (GnomeCanvasItem*)ga->texts->data;
          
              gtk_object_destroy(GTK_OBJECT(txt));

              ga->texts = g_slist_remove(ga->texts, ga->texts->data);
          
              --n_texts;
            }

          g_assert(n_texts == gint(ticks.size()));
          g_assert(g_slist_length(ga->texts) == ticks.size());

          // Create buffer for tick line coords
          if (ga->lines != NULL)
            g_free(ga->lines);
              

          if (ticks.size() > 0)
            {
              ga->lines = g_new(double, 4*ticks.size());
              ga->nticks = ticks.size();
            }
          else
            {
              ga->lines = NULL;
              ga->nticks = 0;
            }

          vector<Tick>::const_iterator ti = ticks.begin();

          GSList* textlist = ga->texts;

          guint tick_number = 0;

          while (ti != ticks.end())
            {
              GnomeCanvasItem* txt = (GnomeCanvasItem*)textlist->data;
              textlist = g_slist_next(textlist);

              Affine::Point t0;
              Affine::Point t1;          

              GtkAnchorType txt_anchor = GTK_ANCHOR_CENTER;
              double txt_x = 0.0;
              double txt_y = 0.0;

              switch (axis->pos())
                {
                case PlotUtil::NORTH:
                case PlotUtil::SOUTH:
                  t0.x = trans->transform(ti->pos());
                  t1.x = t0.x;
                  txt_x = t0.x;
              
                  switch (axis->pos())
                    {
                    case PlotUtil::NORTH:
                      t0.y = rect->y() + rect->height();
                      t1.y = t0.y - ti->len();
                      txt_anchor = GTK_ANCHOR_SOUTH;
                      break;

                    case PlotUtil::SOUTH:
                      t0.y = rect->y();
                      t1.y = t0.y + ti->len();
                      txt_anchor = GTK_ANCHOR_NORTH;
                      break;
                  
                    default:
                      break;
                    }

                  txt_y = t1.y;
                  break;
              
                case PlotUtil::WEST:
                case PlotUtil::EAST:
                  t0.y = trans->transform(ti->pos());
                  t1.y = t0.y;
              
                  txt_y = t0.y;

                  switch (axis->pos())
                    {
                    case PlotUtil::WEST:
                      t0.x = rect->x() + rect->width();
                      t1.x = t0.x - ti->len();
                      txt_anchor = GTK_ANCHOR_EAST;
                      break;

                    case PlotUtil::EAST:
                      t0.x = rect->x();
                      t1.x = t0.x + ti->len();
                      txt_anchor = GTK_ANCHOR_WEST;
                      break;
                  
                    default:
                      break;
                    }

                  txt_x = t1.x;
                  break;
                default:
                  g_warning("Unplanned switch default");
                  break;
                }
#if 0
              g_debug("Item coords %g,%g %g,%g", t0.x, t0.y, t1.x, t1.y);
#endif

              GFont* gfont = static_cast<GFont*>(ti->font());
              
              if (gfont == 0)
                {
                  Frontend* fe = guppi_frontend();
                  if (fe != 0)
                    {
                      gfont = static_cast<GFont*>(fe->default_font());
                    }
                }

              // Get 1.0 scale font, since GnomeCanvasText does scaling for us.
              GdkFont* gdkfont = gfont ? gfont->gdk_font(1.0) : 0;

              if (gdkfont != 0)
                {
                  // keep in sync with the else!
                  gnome_canvas_item_set(txt, 
                                        "anchor", txt_anchor,
                                        "x", txt_x,
                                        "y", txt_y,
                                        "text", ti->label().c_str(),
                                        "font_gdk", gdkfont,
                                        "fill_color_rgba", ti->label_color().rgba(),
                                        NULL);
                }
              else
                {
                  // keep in sync with the prev. block!
                  gnome_canvas_item_set(txt, 
                                        "anchor", txt_anchor,
                                        "x", txt_x,
                                        "y", txt_y,
                                        "text", ti->label().c_str(),
                                        "font", "fixed", // kinda bad; breaks our GuppiCanvasText cache
                                        "fill_color_rgba", ti->label_color().rgba(),
                                        NULL);
                }

              g_assert(tick_number < ga->nticks);
              g_assert(ga->lines != NULL);

              ga->lines[tick_number*4] = t0.x;
              ga->lines[tick_number*4+1] = t0.y;
              ga->lines[tick_number*4+2] = t1.x;
              ga->lines[tick_number*4+3] = t1.y;

              ++ti;
              ++tick_number;
            }
          g_assert(textlist == NULL);
        }

      if (item->object.flags & GNOME_CANVAS_ITEM_VISIBLE)
        {
          /* Show all the text items */
          GSList* tmp = ga->texts;
          while (tmp != NULL)
            {
              gnome_canvas_item_show(GNOME_CANVAS_ITEM(tmp->data));
              
              tmp = g_slist_next(tmp);
            }
        }
      else 
        {
          /* Hide all the text items */
          GSList* tmp = ga->texts;
          while (tmp != NULL)
            {
              gnome_canvas_item_hide(GNOME_CANVAS_ITEM(tmp->data));
              
              tmp = g_slist_next(tmp);
            }
        }
    } 
  else 
    {
      g_warning("axis item requires AA mode");
    }
}

static void
guppi_axis_realize (GnomeCanvasItem *item)
{
  GuppiAxis *ga;

  ga = GUPPI_AXIS (item);

  if (parent_class->realize)
    (* parent_class->realize) (item);
}

static void
guppi_axis_unrealize (GnomeCanvasItem *item)
{
  GuppiAxis *ga;

  ga = GUPPI_AXIS (item);

  if (parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
guppi_axis_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
                    int x, int y, int width, int height)
{
  GuppiAxis *ga;

  ga = GUPPI_AXIS (item);

  g_warning("GuppiAxis does not work on the Gdk canvas");
}

static double
guppi_axis_point (GnomeCanvasItem *item, double x, double y,
                  int cx, int cy, GnomeCanvasItem **actual_item)
{
  GuppiAxis *ga;

  ga = GUPPI_AXIS(item);

  *actual_item = item;

  return 1.0; // FIXME
}

static void
guppi_axis_translate (GnomeCanvasItem *item, double dx, double dy)
{
  GuppiAxis *ga;
  ga = GUPPI_AXIS (item);

  g_warning("axis translate isn't implemented");
}

static void
guppi_axis_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2)
{
  GuppiAxis *ga;
  const Rectangle* rect; 

  ga = GUPPI_AXIS (item);

  get_info(ga, 0, &rect, 0);

  if (rect == 0)
    {
      *x1 = *y1 = *x2 = *y2 = 0.0;
      return;
    }

  *x1 = rect->x();
  *x2 = *x1 + rect->width();

  *y1 = rect->y();
  *y2 = *y2 + rect->height();
}

void 
guppi_axis_set_info_func(GuppiAxis* ga, 
                         GuppiAxisInfoFunc info_func,
                         gpointer user_data)
{
  g_return_if_fail(ga != 0);
  g_return_if_fail(GUPPI_IS_AXIS(ga));

  ga->info_func = info_func;
  ga->user_data = user_data;

  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(ga));
}

