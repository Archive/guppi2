// -*- C++ -*-

/* 
 * markerpaletteedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "markerpaletteedit.h"

#include <map>

struct markertype_menuitem {
  const gchar* name;
  MarkerStyle::Type type;
};

static markertype_menuitem markermenuitems[] = {
  { N_("One Pixel"), MarkerStyle::DOT },
  { N_("Unfilled Box"), MarkerStyle::BOX },
  { N_("Unfilled Circle"), MarkerStyle::CIRCLE },
  { N_("Unfilled Diamond"), MarkerStyle::DIAMOND },
  { N_("Filled Box"), MarkerStyle::FILLED_BOX },
  { N_("Filled Circle"), MarkerStyle::FILLED_CIRCLE },
  { N_("Filled Diamond"), MarkerStyle::FILLED_DIAMOND },
  { N_("Plus"), MarkerStyle::PLUS },
  { N_("Times"), MarkerStyle::TIMES },
  { N_("Horizontal Line"), MarkerStyle::HORIZONTAL_TICK },
  { N_("Vertical Line"), MarkerStyle::VERTICAL_TICK }
};

static guint n_markermenuitems = sizeof(markermenuitems)/sizeof(markermenuitems[0]);

class MarkerStyleEdit {
public:
  MarkerStyleEdit();
  ~MarkerStyleEdit();

  GtkWidget* widget();

  void set_style(guint32 id);

  void set_no_style();

private:
  guint32 id_;

  GtkWidget* widget_;

  GtkWidget* entry_;

  GtkWidget* picker_;

  GtkWidget* spinbutton_;

  GtkAdjustment* size_adj_;

  GtkWidget* markermenu_;

  map<GtkWidget*,guint> menuentry_indices_;

  bool have_style_;

  bool in_update_;

  void update();

  static void entry_changed_cb(GtkWidget* entry, gpointer data);
  void entry_changed();

  static void color_set_cb(GtkWidget* picker, guint r, guint g, guint b, guint a, gpointer data);
  void color_set(guchar r, guchar g, guchar b, guchar a);

  static void size_change_cb(GtkAdjustment* adj, gpointer data);
  void size_change();

  static void typemenu_activate_cb(GtkWidget* mi, gpointer data);
  void typemenu_activate(GtkWidget* mi);
};

MarkerStyleEdit::MarkerStyleEdit()
  : id_(MarkerPalette::DEFAULT),
    widget_(0), entry_(0),
    have_style_(false), 
    in_update_(false)
{

}

MarkerStyleEdit::~MarkerStyleEdit()
{
  // we don't destroy the widget, we assume it's in a container.
}

GtkWidget* 
MarkerStyleEdit::widget()
{
  if (widget_ == 0)
    {
      widget_ = gtk_frame_new(_("Style Properties"));

      gtk_signal_connect(GTK_OBJECT(widget_), "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &widget_);
      
      GtkWidget* vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

      gtk_container_set_border_width(GTK_CONTAINER(vbox), 8);
      
      gtk_container_add(GTK_CONTAINER(widget_), vbox);
      
      entry_ = gtk_entry_new();

      guppi_set_tooltip(entry_, _("Change the name of the marker style (not allowed on the default style)."));
    
      gtk_signal_connect(GTK_OBJECT(entry_), "changed",
                         GTK_SIGNAL_FUNC(entry_changed_cb),
                         this);
  
      gtk_box_pack_start(GTK_BOX(vbox), entry_, FALSE, FALSE, 0);

      ///

      GtkWidget* hbox;
      GtkWidget* label;

      hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);

      label = gtk_label_new(_("Color: "));

      gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

      picker_ = gnome_color_picker_new();

      guppi_set_tooltip(picker_, _("Change the color of the marker style"));

      gnome_color_picker_set_use_alpha(GNOME_COLOR_PICKER(picker_), TRUE);

      gtk_signal_connect(GTK_OBJECT(picker_), "color_set",
                         GTK_SIGNAL_FUNC(color_set_cb),
                         this);

      gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
      gtk_box_pack_end(GTK_BOX(hbox), picker_, TRUE, TRUE, 0);

      gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

      ////

      hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);

      label = gtk_label_new(_("Size: "));

      gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

      size_adj_ = GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 10000.0, .25, 1.0, 0.0));

      spinbutton_ = gtk_spin_button_new(size_adj_, 1.0, 2);

      guppi_set_tooltip(spinbutton_, _("Change the size of the marker style; doesn't work on single pixel styles"));

      gtk_signal_connect(GTK_OBJECT(size_adj_), "value_changed",
                         GTK_SIGNAL_FUNC(size_change_cb),
                         this);

      gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
      gtk_box_pack_end(GTK_BOX(hbox), spinbutton_, TRUE, TRUE, 0);

      gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

      ///

      GtkWidget* menu = gtk_menu_new();

      markermenu_ = gtk_option_menu_new();

      gtk_option_menu_set_menu(GTK_OPTION_MENU(markermenu_), menu);
      
      guint i = 0;
      while (i < n_markermenuitems)
        {
          GtkWidget* mi = gtk_menu_item_new_with_label(_(markermenuitems[i].name));

          gtk_signal_connect(GTK_OBJECT(mi), "activate",
                             GTK_SIGNAL_FUNC(typemenu_activate_cb),
                             this);

          gtk_menu_append(GTK_MENU(menu), mi);

          menuentry_indices_[mi] = i;

          ++i;
        }

      guppi_set_tooltip(markermenu_, _("Change the shape of the marker style."));

      gtk_box_pack_start(GTK_BOX(vbox), markermenu_, FALSE, FALSE, 0);

      update();
    }
  return widget_;
}

void 
MarkerStyleEdit::set_style(guint32 id)
{
  id_ = id;

  have_style_ = true;

  update();
}

void 
MarkerStyleEdit::set_no_style()
{
  have_style_ = false;
  update();
}

void
MarkerStyleEdit::update()
{
  in_update_ = true;

  if (widget_ != 0)
    {
      bool set_it = false;
      if (have_style_)
        {
          const MarkerStyle* style = guppi_marker_palette()->get(id_);
          
          if (style != 0)
            {
              set_it = true;

              gtk_widget_set_sensitive(widget_, TRUE);
              
              // Now the frame is just "style properties"
              // gtk_frame_set_label(GTK_FRAME(widget_), style->name().c_str());

              gnome_color_picker_set_title(GNOME_COLOR_PICKER(picker_), style->name().c_str());
             
              gnome_color_picker_set_i8(GNOME_COLOR_PICKER(picker_),
                                        style->color().r(), 
                                        style->color().g(), 
                                        style->color().b(), 
                                        style->color().a());
 
              // Can't change the name of the default entry
              if (id_ == MarkerPalette::DEFAULT)
                gtk_widget_set_sensitive(entry_, FALSE);
              else 
                gtk_widget_set_sensitive(entry_, TRUE);
              
              gtk_entry_set_text(GTK_ENTRY(entry_), style->name().c_str());

              gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbutton_), style->size());

              // Meaningless to set size of single-pixel style
              gtk_widget_set_sensitive(spinbutton_, 
                                       (style->type() != MarkerStyle::DOT));

              guint i = 0;
              while (i < n_markermenuitems)
                {
                  if (markermenuitems[i].type == style->type())
                    {
                      gtk_option_menu_set_history(GTK_OPTION_MENU(markermenu_),
                                                  i);
                      break;
                    }
                  
                  ++i;
                }
            }
        }

      if (!set_it)
        {
          gtk_widget_set_sensitive(widget_, FALSE);

          // Now it's just a fixed label
          //          gtk_frame_set_label(GTK_FRAME(widget_), NULL);

          gtk_entry_set_text(GTK_ENTRY(entry_), "");
        }
    }
  
  in_update_ = false;
}

void 
MarkerStyleEdit::entry_changed_cb(GtkWidget* entry, gpointer data)
{
  MarkerStyleEdit* mse = static_cast<MarkerStyleEdit*>(data);

  mse->entry_changed();
}

void 
MarkerStyleEdit::entry_changed()
{
  if (in_update_)
    return;

  g_return_if_fail(entry_ != 0);

  MarkerStyle* style = guppi_marker_palette()->checkout(id_);

  if (style != 0)
    {
      gchar* text = gtk_entry_get_text(GTK_ENTRY(entry_));
      
      style->set_name(text);

      guppi_marker_palette()->checkin(style, id_);
    }
}

void 
MarkerStyleEdit::color_set_cb(GtkWidget* picker, guint r, guint g, guint b, guint a, 
                              gpointer data)
{
  MarkerStyleEdit* mse = static_cast<MarkerStyleEdit*>(data);

  mse->color_set(r, g, b, a);
}

void 
MarkerStyleEdit::color_set(guchar r, guchar g, guchar b, guchar a)
{
  if (in_update_)
    return;

  MarkerStyle* style = guppi_marker_palette()->checkout(id_);

  if (style != 0)
    {
      Color c;
      c.set(r, g, b, a);

      style->set_color(c);

      guppi_marker_palette()->checkin(style, id_);
    }
}

void 
MarkerStyleEdit::size_change_cb(GtkAdjustment* adj, gpointer data)
{
  MarkerStyleEdit* mse = static_cast<MarkerStyleEdit*>(data);

  mse->size_change();
}

void 
MarkerStyleEdit::size_change()
{
  if (in_update_)
    return;

  MarkerStyle* style = guppi_marker_palette()->checkout(id_);

  if (style != 0)
    {
      style->set_size(size_adj_->value);

      guppi_marker_palette()->checkin(style, id_);
    }
}

void 
MarkerStyleEdit::typemenu_activate_cb(GtkWidget* mi, gpointer data)
{
  MarkerStyleEdit* mse = static_cast<MarkerStyleEdit*>(data);

  mse->typemenu_activate(mi);
}

void 
MarkerStyleEdit::typemenu_activate(GtkWidget* mi)
{
  if (in_update_)
    return;
  
  map<GtkWidget*,guint>::iterator i = menuentry_indices_.find(mi);
  
  g_return_if_fail(i != menuentry_indices_.end());

  MarkerStyle* style = guppi_marker_palette()->checkout(id_);
  
  if (style != 0)
    {
      style->set_type(markermenuitems[i->second].type);

      guppi_marker_palette()->checkin(style, id_);
    }
}

/////////////////////////

MarkerPaletteEdit::MarkerPaletteEdit()
  : dialog_(0), palette_(0), clist_(0), mse_(0)
{
  mse_ = new MarkerStyleEdit;
}

MarkerPaletteEdit::~MarkerPaletteEdit()
{
  if (dialog_ != 0)
    {
      gtk_widget_destroy(dialog_);
      dialog_ = 0;
    }

  delete mse_;

  release_palette();
}

// MarkerPalette::View
void 
MarkerPaletteEdit::add_entry(MarkerPalette* palette, guint32 entryid)
{
  if (dialog_ != 0)
    {
      gint row = append_entry(entryid);
      if (row >= 0)
        {
          gtk_clist_select_row(GTK_CLIST(clist_), row, -1);
        }
    }
}

void 
MarkerPaletteEdit::remove_entry(MarkerPalette* palette, guint32 entryid)
{
  if (dialog_ != 0)
    {
      remove_entry(entryid);
    }
}

void 
MarkerPaletteEdit::change_entry(MarkerPalette* palette, guint32 entryid)
{
  if (dialog_ != 0)
    {
      change_entry(entryid);
    }
}

void 
MarkerPaletteEdit::change_current(MarkerPalette* palette, 
                                  guint32 old, 
                                  guint32 newcurrent)
{
  if (dialog_ != 0)
    {
      set_current_entry(newcurrent);
    }
}

// ::View
void 
MarkerPaletteEdit::destroy_model()
{
  palette_ = 0;
}

void 
MarkerPaletteEdit::set_palette(MarkerPalette* palette)
{
  if (palette_ == palette) 
    return;

  if (palette_ != 0)
    {
      release_palette();
    }

  g_assert(palette_ == 0);

  grab_palette(palette);
}

void 
MarkerPaletteEdit::edit()
{
  g_return_if_fail(palette_ != 0);

  if (dialog_ == 0)
    {
      dialog_ = gnome_dialog_new(_("Scatter Plot Marker Palette"),
                                 GNOME_STOCK_BUTTON_CLOSE,
                                 NULL);

      gnome_dialog_set_close(GNOME_DIALOG(dialog_), TRUE);
      
      gtk_window_set_policy(GTK_WINDOW(dialog_), FALSE, TRUE, FALSE);
      
      gtk_signal_connect(GTK_OBJECT(dialog_),
                         "close",
                         GTK_SIGNAL_FUNC(close_cb),
                         this);
      
      GtkWidget* hbox = gtk_hbox_new(FALSE, GNOME_PAD);
      
      GtkWidget* frame = gtk_frame_new(_("Marker Styles"));

      gtk_box_pack_start(GTK_BOX(hbox), frame, 
                         TRUE, TRUE, 0);

      GtkWidget* vbox = gtk_vbox_new(FALSE, 0);

      gtk_container_set_border_width(GTK_CONTAINER(vbox), 8);

      GtkWidget* sw = gtk_scrolled_window_new(NULL,NULL);

      gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                     GTK_POLICY_AUTOMATIC,
                                     GTK_POLICY_AUTOMATIC);

      clist_ = gtk_clist_new(1);

      gtk_container_add(GTK_CONTAINER(sw), clist_);

      gtk_clist_column_titles_hide(GTK_CLIST(clist_));

      gtk_signal_connect(GTK_OBJECT(clist_), "select_row",
                         GTK_SIGNAL_FUNC(select_row_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(clist_), "unselect_row",
                         GTK_SIGNAL_FUNC(unselect_row_cb),
                         this);

      gtk_container_add(GTK_CONTAINER(frame), vbox);

      gtk_box_pack_start(GTK_BOX(vbox), sw, TRUE, TRUE, 0);

      GtkWidget* bbox = gtk_hbutton_box_new();

      gtk_button_box_set_layout(GTK_BUTTON_BOX(bbox), GTK_BUTTONBOX_END);

      gtk_box_pack_end(GTK_BOX(vbox), bbox, FALSE, FALSE, 0);

      GtkWidget* button = gtk_button_new_with_label(_("Add"));

      guppi_set_tooltip(button, _("Add a new marker style"));

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(add_button_clicked_cb),
                         this);

      gtk_container_add(GTK_CONTAINER(bbox), button);

      button = gtk_button_new_with_label(_("Remove"));

      guppi_set_tooltip(button, _("Remove the selected marker style"));

      gtk_signal_connect(GTK_OBJECT(button), "clicked",
                         GTK_SIGNAL_FUNC(remove_button_clicked_cb),
                         this);

      gtk_container_add(GTK_CONTAINER(bbox), button);

      
      gtk_box_pack_end(GTK_BOX(hbox), mse_->widget(),
                       TRUE, TRUE, 0);


      gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox),
                         hbox,
                         TRUE, TRUE, 0);

      gtk_widget_show_all(hbox);

      // Put all the initial entries into the clist.
      gtk_clist_freeze(GTK_CLIST(clist_));
      MarkerPalette::const_iterator i = palette_->begin();
      while (i != palette_->end())
        {
          guint32 id = *i;

          append_entry(id);

          ++i;
        }
      gtk_clist_thaw(GTK_CLIST(clist_));
    }

  gtk_widget_show(dialog_);
  if (GTK_WIDGET_REALIZED(dialog_))
    {
      gdk_window_show(dialog_->window);
      gdk_window_raise(dialog_->window);
    }
}

void 
MarkerPaletteEdit::grab_palette(MarkerPalette* palette)
{
  if (palette != 0)
    {
      palette->palette_model.add_view(this);
      g_return_if_fail(palette_ == 0);
      palette_ = palette;
    }
}

void 
MarkerPaletteEdit::release_palette()
{
  if (palette_ != 0)
    {
      palette_->palette_model.remove_view(this);
      palette_ = 0;
    }
}

gint
MarkerPaletteEdit::append_entry(guint32 entryid)
{
  if (dialog_ != 0)
    {
      const MarkerStyle* style = palette_->get(entryid);

      if (style == 0)
        return -1;

      const gchar* text[] = { style->name().c_str() };

      gint row = gtk_clist_append(GTK_CLIST(clist_), (gchar**)text);

      gtk_clist_set_row_data(GTK_CLIST(clist_), row, (gpointer)style);

      return row;
    }
  return -1;
}

void
MarkerPaletteEdit::remove_entry(guint32 entryid)
{
  if (dialog_ != 0)
    {
      const MarkerStyle* style = palette_->get(entryid);

      if (style == 0)
        return;

      gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_),
					      (gpointer)style);

      g_return_if_fail(row >= 0);

      gtk_clist_remove(GTK_CLIST(clist_), row);
    }
}

void 
MarkerPaletteEdit::change_entry(guint32 entryid)
{
  if (dialog_ != 0)
    {
      const MarkerStyle* style = palette_->get(entryid);

      if (style == 0)
        return;

      gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_),
					      (gpointer)style);

      g_return_if_fail(row >= 0);

      gtk_clist_set_text(GTK_CLIST(clist_), row, 0, style->name().c_str());

      // force an update, hacky but so is this whole file.
      mse_->set_style(entryid);
    }
}

void 
MarkerPaletteEdit::set_current_entry(guint32 entryid)
{
  if (dialog_ != 0)
    {
      const MarkerStyle* style = palette_->get(entryid);

      if (style == 0)
        return;

      gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_),
					      (gpointer)style);

      g_return_if_fail(row >= 0);

      bool already_selected = false;

      GList* tmp = GTK_CLIST(clist_)->selection;
      while (tmp)
        {
          gint srow = GPOINTER_TO_INT(tmp->data);
          
          if (srow == row)
            {
              already_selected = true;
              break;
            }

          tmp = g_list_next(tmp);
        }

      if (!already_selected)
        gtk_clist_select_row(GTK_CLIST(clist_), row, -1);
    }
}

gint 
MarkerPaletteEdit::close_cb(GtkWidget* w, gpointer data)
{
  MarkerPaletteEdit* mpe = static_cast<MarkerPaletteEdit*>(data);
  
  return mpe->close();
}

gint 
MarkerPaletteEdit::close()
{
  if (dialog_)
    {
      gtk_widget_destroy(dialog_);
      dialog_ = 0;
    }
  
  return TRUE; // prevent second destroy
}


void 
MarkerPaletteEdit::select_row_cb(GtkWidget* w, gint row, gint col, 
                                 GdkEvent* e, gpointer data)
{
  MarkerPaletteEdit* mpe = static_cast<MarkerPaletteEdit*>(data);
  mpe->select_row(row);
}

void 
MarkerPaletteEdit::unselect_row_cb(GtkWidget* w, gint row, gint col, 
                                   GdkEvent* e, gpointer data)
{
  MarkerPaletteEdit* mpe = static_cast<MarkerPaletteEdit*>(data);
  mpe->unselect_row(row);
}

void 
MarkerPaletteEdit::select_row(gint row)
{
  gpointer p = gtk_clist_get_row_data(GTK_CLIST(clist_), row);

  g_return_if_fail(p != 0);

  // This is mildly dubious, assuming that the style is still valid;
  //  but should be OK since we monitor style removals.
  guint32 id = palette_->find(static_cast<MarkerStyle*>(p));
  mse_->set_style(id);

  // If we selected the row ourselves in response to a change in the 
  //  currently selected entry, then we won't do this. 
  // If the user selected the row, the internal palette has to be 
  //  updated.
  if (id != palette_->current())
    palette_->set_current(id);
}

void 
MarkerPaletteEdit::unselect_row(gint row)
{
  mse_->set_no_style();

  // This is dubious; there's still a current row...
  // GtkVTree will save us here I hope.
}

void 
MarkerPaletteEdit::add_button_clicked_cb(GtkWidget* w, gpointer data)
{
  MarkerPaletteEdit* mpe = static_cast<MarkerPaletteEdit*>(data);
  mpe->add();
}

void 
MarkerPaletteEdit::remove_button_clicked_cb(GtkWidget* w, gpointer data)
{
  MarkerPaletteEdit* mpe = static_cast<MarkerPaletteEdit*>(data);
  mpe->remove();
}

void 
MarkerPaletteEdit::add()
{
  if (palette_ != 0)
    {
      // Triggers appropriate callbacks blah blah
      palette_->add(new MarkerStyle);
    }
}

void 
MarkerPaletteEdit::remove()
{
  if (palette_ != 0)
    {
      gint sel = -1;
      if (GTK_CLIST(clist_)->selection)
        sel = GPOINTER_TO_INT(GTK_CLIST(clist_)->selection->data);
      else 
        {
          gnome_warning_dialog(_("There is no row selected to remove"));
          return;
        }
      
      gpointer p = gtk_clist_get_row_data(GTK_CLIST(clist_), sel);

      g_return_if_fail(p != 0);

      // This is mildly dubious, assuming that the style is still valid;
      //  but should be OK since we monitor style removals.
      MarkerStyle* s = static_cast<MarkerStyle*>(p);

      // potentially slow, should fix.
      guint32 id = palette_->find(s);

      if (id == MarkerPalette::DEFAULT)
        {
          gnome_warning_dialog(_("You can't remove the default style."));
          return;
        }
      else 
        {
          // Results in appropriate signals to adjust the clist.
          palette_->remove(id);
        }
    }
}

