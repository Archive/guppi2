// -*- C++ -*-

/* 
 * data.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATA_H
#define GUPPI_DATA_H

#include "util.h"
#include "view.h"
#include "refcount.h"

#include <string>
#include <vector>

#include <gnome-xml/tree.h>

/* "Data" is just something we can plot. It has various
   characteristics that can be queried, that make it 
   suitable for different kinds of plot. */

class ScalarData;     
class CategoricalData;
class DateData;
class LabelData;  
class DataGroup;
class TypeRegistry;         
 
class Data {
private:
  static TypeRegistry* type_registry_;
public:
  static TypeRegistry* type_registry();

  // Our own little RTTI, just to annoy C++ purists
  typedef enum {
    Invalid = -1,
    Scalar = 0,
    Label = 1,
    Categorical = 2,
    Date = 3,
    Group = 4
  } DataType;

  gconstpointer cast_to_type(DataType type) const;

  gpointer cast_to_type(DataType type);

  // All these are just sugar for cast_to_type()
  ScalarData* cast_to_scalar();     
  CategoricalData* cast_to_categorical();
  DateData* cast_to_date();
  LabelData* cast_to_label();
  DataGroup* cast_to_group();

  const ScalarData* cast_to_scalar() const;     
  const CategoricalData* cast_to_categorical() const;
  const DateData* cast_to_date() const;
  const LabelData* cast_to_label() const;
  const DataGroup* cast_to_group() const;

  // Sugar for obtaining the type registry and checking is_a
  bool is_a(DataType type) const;

  virtual ~Data();
                   
  //  DataType type() const { return type_; };

  // Some methods are meaningless no-ops on DataGroup. I know it's
  // ugly, sorry, I didn't think it through in advance, so shoot me.

  // number of elements
  virtual gsize size() const = 0;

  virtual const string& name() const = 0;

  virtual void set_name(const string & name) = 0;

  virtual xmlNodePtr xml(xmlNodePtr parent) const = 0;

  guint ref();
  guint unref(); // if returns 0 then delete
  virtual gsize byte_size() = 0; // used in Scheme bindings

  class View : public ::View 
  {
  public:
    // Only one of the change_values() is emitted for any values change,
    // depending on whether we know which ones changed.
    // Also, the second one could mean a change in the *number* of values 
    // (i.e. the size of the dataset) but the first never does.
    virtual void change_values(Data* d, const vector<guint> & which) = 0;
    virtual void change_values(Data* d) = 0;
    virtual void change_name(Data* d, const string & name) = 0;
  };

  class Model : public ::Model<Data::View*> {
  public:
    void values_changed(Data* d, const vector<guint> & which) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_values(d, which);
	++i;
      }
      unlock_views();
    }
    void values_changed(Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_values(d);
	++i;
      }
      unlock_views();
    }
    void name_changed(Data* d, const string & name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_name(d, name);
	++i;
      }
      unlock_views();
    }
  };
  
  Model data_model;  

protected:
  Data(DataType type);

  DataType type_;

  RC rc_;

private:
  Data(const Data &);
  const Data& operator=(const Data&);
};


#endif
