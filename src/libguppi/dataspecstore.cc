// -*- C++ -*-

/* 
 * dataspecstore.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "dataspecstore.h"
#include "datagroup.h"

DataGroupSpecStore::DataGroupSpecStore()
{
  
}

DataGroupSpecStore::~DataGroupSpecStore()
{
  iterator i = begin();
  while (i != end())
    {
      release_spec(*i);
      ++i;
    }
  specs_.clear();
}

void 
DataGroupSpecStore::insert(DataGroupSpec* d)
{
  specs_.push_back(d);
  grab_spec(d);
}

DataGroupSpecStore::const_iterator 
DataGroupSpecStore::begin() const
{
  return specs_.begin();
}

DataGroupSpecStore::const_iterator 
DataGroupSpecStore::end() const
{
  return specs_.end();
}

DataGroupSpecStore::iterator 
DataGroupSpecStore::begin()
{
  return specs_.begin();
}

DataGroupSpecStore::iterator 
DataGroupSpecStore::end()
{
  return specs_.end();
}

DataGroupSpecStore::const_iterator 
DataGroupSpecStore::find(DataGroupSpec* d) const
{
  const_iterator i = begin();
  while (i != end())
    {
      if (*i == d)
        break;
      ++i;
    }
  return i;
}
 
DataGroupSpecStore::iterator 
DataGroupSpecStore::find(DataGroupSpec* d)
{
  iterator i = begin();
  while (i != end())
    {
      if (*i == d)
        break;
      ++i;
    }
  return i;
}

void 
DataGroupSpecStore::erase(iterator i)
{
  specs_.erase(i);
}

void 
DataGroupSpecStore::erase(iterator b, iterator e)
{
  specs_.erase(b,e);
}

void 
DataGroupSpecStore::grab_spec(DataGroupSpec* d)
{
  if (d != 0)
    {
      d->ref();

    }
}

void 
DataGroupSpecStore::release_spec(DataGroupSpec* d)
{
  if (d != 0)
    {
      if (d->unref() == 0)
        delete d;
    }
}
