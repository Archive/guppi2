// -*- C++ -*-

/* 
 * fontoption.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_FONTOPTION_H
#define GUPPI_FONTOPTION_H

// Widget for selecting a Font from the FontStore

#include "callbacks.h"
#include "gfont.h"

#include <map>

#include "gnome-util.h"

class FontOption
{
public:
  FontOption();
  virtual ~FontOption();

  void set_font(GFont* f);
  GFont* get_font();

  GtkWidget* widget();

  // The callback is called when the current font object changes.
  void connect(CallbackList::Callback cb, gpointer font) { 
    cblist_.connect(cb,font); 
  }
  void disconnect(CallbackList::Callback cb) {
    cblist_.disconnect(cb);
  }

  // FIXME add a way to specify which types of Font can be 
  //  chosen.

private:
  GtkWidget* option_;
  GtkWidget* menu_;
  
  GFont* current_;

  map<GFont*,gint> indexes_;

  // set when we're doing it ourselves
  bool changing_font_;

  CallbackList cblist_;

  void make_font_menu();

  static void font_activated_cb(GtkWidget* w, gpointer font);
  void font_activated(GFont* f);

  FontOption(const FontOption &);
  const FontOption& operator=(const FontOption&);
};

#endif
