// -*- C++ -*-

/* 
 * categoricaldata.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "categoricaldata.h"
#include <goose/CategoricalSet.h>

CategoricalData::CategoricalData()
  : Data(Categorical), set_(0), checked_out_(false)
{
  set_ = new CategoricalSet;
}

CategoricalData::CategoricalData(CategoricalSet* cs)
  : Data(Categorical), set_(cs), checked_out_(false)
{

}
  
CategoricalData::~CategoricalData()
{

}

gsize 
CategoricalData::size() const
{

}

const string& 
CategoricalData::name() const
{

}

void 
CategoricalData::set_name(const string & name)
{

}

void 
CategoricalData::set_string(size_t index, const string& value)
{

}

const string& 
CategoricalData::get_string(size_t index) const
{

}

xmlNodePtr 
CategoricalData::xml(xmlNodePtr parent) const
{

}
  
void 
CategoricalData::set_xml(xmlNodePtr node)
{

}


RealSet* 
CategoricalData::checkout_categoricalset()
{


}

void 
CategoricalData::checkin_categoricalset(CategoricalSet* set, 
                                        bool values_changed, 
                                        bool other_changed)
{

}

gsize 
CategoricalData::byte_size()
{
  return sizeof(CategoricalData);
}

