// -*- C++ -*-

/* 
 * plot.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOT_H
#define GUPPI_PLOT_H

#include "datastore.h"
#include "refcount.h"
#include "gnome-util.h"

class Tool;
class Action;
class MenuEntry;
class PlotState;

class Plot {
public:
  Plot(const char* id, DataStore* ds);
  virtual ~Plot();

  // Return the state object this is one of the views for.
  // Try not to use this function. Try really really hard not 
  // to cast the return value to a specific state type; if you do 
  // that, I get to kick your ass.
  virtual PlotState* plot_state() = 0;

  // Create a new "view" with the same state this one has.
  virtual Plot* new_view() = 0;

  // Can be used to type-check plots. Each plot subclass will
  //  define this differently. Also used to match tools 
  //  with plot types, iterate over available plot types, etc.
  const string& id() const { return id_; }

  // Convenience function to see if the plot's type is the one we want.
  bool check(const string& checkid) { return id_ == checkid; }

  ///

  virtual GnomeCanvasItem* item() = 0;

  // Create the plot in this canvas group. Should only be called
  // once. Calling item before calling this is an error.
  virtual void realize(GnomeCanvasGroup* group) = 0;
  // Disconnect from canvas
  virtual void unrealize() = 0;

  ///

  // The size allocation/request stuff should eventually be virtual 
  // on the level of the PlotState, which didn't exist when I wrote this.
  // then we can handle the resize_lock stuff automatically


  // Item coordinates of the canvas item (top-left of the plot)
  virtual void set_x(double x) = 0;
  virtual void set_y(double y) = 0;

  virtual double x() const = 0;
  virtual double y() const = 0;

  virtual void set_size(double width, double height) = 0;

  virtual double width() = 0;
  virtual double height() = 0;

  virtual void size_request(double* width, double* height) = 0;

  // In cases where an infinite loop is possible - for example,
  // if resizing might cause another view on the same state
  // to resize, which might cause us to resize, add infinitum - 
  // we do this before calling set_size(). 
  bool get_resize_lock();
  void release_resize_lock();

  ////

  void set_status(const string& status);

  virtual const string& name() const = 0;

  guint ref();
  guint unref();

  class View : public ::View 
  {
  public:
    virtual void change_size(Plot* p) = 0;
    virtual void change_status(Plot* p, const string& status) = 0;

    // There's no set_name() because the name is a property of the plot 
    //  state, not the view (Plot is a view). Status, on the other hand,
    //  is a view property.

    virtual void change_name(Plot* p, const string& name) = 0;
  };

  class Model : public ::Model<Plot::View*> {
  public:
    void size_changed(Plot* p) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_size(p);
	++i;
      }
      unlock_views();
    }
    void status_changed(Plot* p, const string& status) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_status(p, status);
	++i;
      }
      unlock_views();
    }
    void name_changed(Plot* p, const string& name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_name(p, name);
	++i;
      }
      unlock_views();
    }
  };

  Model plot_model;

#if 0 // Future interface features

  void queue_resize();
#endif

protected:
  DataStore* ds() { return ds_; }

private:
  const string id_;
  DataStore* ds_;
  RC rc_;
};


#endif
