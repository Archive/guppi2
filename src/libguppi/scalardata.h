// -*- C++ -*-

/* 
 * scalardata.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_SCALARDATA_H
#define GUPPI_SCALARDATA_H

#include "data.h"

class RealSet;

class ScalarData : public Data {
public:

  ScalarData();
  ScalarData(RealSet* rs); // ScalarData will own the RealSet
  virtual ~ScalarData();

  // number of elements
  virtual gsize size() const;

  // pointer to an array of size() doubles
  const double* scalars() const;

  // min/max values
  double min() const;
  double max() const;

  // Data sorted in ascending order; if the data is in a SortedPair
  //  use that instead though... this will make another copy
  const double* sorted_scalars();

  void add(double d);

  virtual const string& name() const;

  virtual void set_name(const string & name);

  void set_scalar(size_t index, double value);
  double get_scalar(size_t index) const;

  virtual xmlNodePtr xml(xmlNodePtr parent) const;
  
  void set_xml(xmlNodePtr node);

  const RealSet* realset() const { return set_; }

  // This is our little mini-protocol to allow direct 
  //  write access to the RealSet.
  
  // Returns 0 if someone else has it
  RealSet* checkout_realset();

  // values_changed should be true if the RealSet's values were modified
  // other_changed should be true if anything else was modified
  // RealSet must be the same one that was checked out.
  void     checkin_realset(RealSet* set, bool values_changed = true, bool other_changed = false);

  virtual gsize byte_size();

protected:
  RealSet* set_;

  bool checked_out_;

private:

  ScalarData(const ScalarData &);
  const ScalarData& operator=(const ScalarData&);
};


// Keeps parallel sorted versions of two ScalarData objects
//  (so that ordered pairs are maintained)
// Only valid for MIN(keydata->size(), otherdata->size())

// FIXME  Now that I'm storing backconvert_ and convert_ anyway,
// may as well *not* store the actual sorted copies of the data.
// It's only used to create the PointTiles anyway, after that we 
// don't need it. So with a little re-arranging should be able
// to save quite a bit of RAM.

class PointTiles;

class SortedPair : public Data::View {
public:
  SortedPair();
  ~SortedPair();

  void set_key(ScalarData* data);
  void set_other(ScalarData* data);

#if 0
  // These return the sorted data. Their use is discouraged, since we
  // may not keep the sorted data around in the future; see note
  // above.
  const double* keydata() const;
  const double* otherdata() const;
#endif

  // These functions all have to be called after calling tiles(),
  //  right now. This is kind of broken and stuff, but...
    
  // Translate an index into the ScalarData to an index into the 
  //  sorted version we're keeping.
  guint real_index_to_sorted(guint i) const { 
    g_return_val_if_fail(real2sorted_ != 0, 0);
    return real2sorted_[i];
  }

  guint sorted_index_to_real(guint i) const {
    g_return_val_if_fail(sorted2real_ != 0, 0);
    return sorted2real_[i];
  }

  double sorted_key_value(guint sortedindex) const {
    g_return_val_if_fail(sorted2real_ != 0, 0);
    g_return_val_if_fail(keydata_ != 0, 0);
    return keydata_->get_scalar(sorted2real_[sortedindex]);
  }

  double sorted_other_value(guint sortedindex) const {
    g_return_val_if_fail(sorted2real_ != 0, 0);
    g_return_val_if_fail(otherdata_ != 0, 0);
    return otherdata_->get_scalar(sorted2real_[sortedindex]);
  }

  double real_key_value(guint realindex) const {
    g_return_val_if_fail(keydata_ != 0, 0);
    return keydata_->get_scalar(realindex);
  }

  double real_other_value(guint realindex) const {
    g_return_val_if_fail(otherdata_ != 0, 0);
    return otherdata_->get_scalar(realindex);
  }

  // Fetch the tiled values
  // Won't exist if there's only one of key/other data, 
  //  or if one of them has zero elements. tiles() returns 0 then.
  PointTiles* tiles() const;

  virtual void change_values(Data* d, const vector<guint> & which);
  virtual void change_values(Data* d);
  virtual void change_name(Data* d, const string & name);

  virtual void destroy_model();

private:
  ScalarData* keydata_;
  ScalarData* otherdata_;

  // We are chewing up some serious RAM here, if there are 
  //  a lot of points. See FIXME note above
  mutable guint* real2sorted_;
  mutable guint* sorted2real_;

  mutable double* keysorted_;
  mutable double* othersorted_;

  mutable PointTiles* tiles_;

  void grab_data(ScalarData** d);
  void release_data(ScalarData** d);
  void clear();
  void recreate() const;
  void update(guint realindex, double newval, bool is_key);

  /* Say the user moves the pointer all over, and we get a bunch of
   * motion notify events. We want to only do work in the canvas idle
   * loop; if we update our sorted point on every motion notify, it's
   * quite sluggish. So we store a history of changes to be made; if
   * more than a small number accumulate before the idle loop, we nuke
   * them and regenerated the sorted array and tiles from scratch. If
   * there are only a few, we update the sorted array and tiles
   * in-place using the change history. Pretty neat, huh?  
   */
  struct Change {
    guint realindex;
    double newval;
    bool is_key;
  };

  vector<Change> changes_;
};


#endif
