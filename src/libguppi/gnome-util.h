// -*- C++ -*-

/* 
 * gnome-util.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_GNOMEUTIL_H
#define GUPPI_GNOMEUTIL_H


#include "util.h"

// Part of a temporary hack; since we want to i18n libguppi but 
//  don't want to link with Gnome. See util.h
#ifdef _
#undef _
#endif

#ifdef N_
#undef N_
#endif

#include <gnome.h>

#include <string>

void guppi_spew_on_destroy (GtkObject* obj);

// Initialize the Gnome Guppi library.
bool guppi_gnome_init_library();
// Vice-versa
void guppi_gnome_shutdown_library();

void guppi_set_tooltip(GtkWidget* w, const string& tip);
void guppi_raise_and_show(GtkWindow* w); // show and/or raise as needed

void guppi_install_menuitem_hint(GtkWidget* menuitem, 
                                 GnomeAppBar* bar, 
                                 const string& hint);

gchar* guppi_logo_file();

void guppi_setup_window(GtkWindow* window, const gchar* wmname);

GtkWidget* guppi_make_labelled_widget(const gchar* label, GtkWidget* child);

#endif /* GUPPI_GNOMEUTIL_H */



