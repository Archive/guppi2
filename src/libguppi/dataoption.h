// -*- C++ -*-

/* 
 * dataoption.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_DATAOPTION_H
#define GUPPI_DATAOPTION_H

// Widget for selecting a Data from the DataStore

#include "datastore.h"
#include "callbacks.h"

#include <map>

#include "gnome-util.h"

class DataOption : public DataStore::View
{
public:
  DataOption();
  virtual ~DataOption();

  virtual void add_data(DataStore* ds, Data* d);
  virtual void remove_data(DataStore* ds, Data* d);
  virtual void change_data_values(DataStore* ds, Data* d);
  virtual void change_data_values(DataStore* ds, Data* d, const vector<guint> & which);
  virtual void change_data_name(DataStore* ds, Data* d, const string & name);
  virtual void destroy_model();

  void restrict_type(Data::DataType t);

  void set_store(DataStore* store);

  void set_data(Data* d);
  Data* get_data();

  GtkWidget* widget();

  // The callback is called when the current data object changes.
  void connect(CallbackList::Callback cb, gpointer data) { 
    cblist_.connect(cb,data); 
  }
  void disconnect(CallbackList::Callback cb) {
    cblist_.disconnect(cb);
  }

  // FIXME add a way to specify which types of Data can be 
  //  chosen.

private:
  DataStore* store_;
  GtkWidget* option_;
  GtkWidget* menu_;
  
  Data* current_;

  Data::DataType show_type_;

  // labels to set when data names change
  map<Data*,GtkWidget*> labels_;
  map<Data*,gint> indexes_;

  // set when we're doing it ourselves
  bool changing_data_;

  CallbackList cblist_;

  void make_data_menu();

  static void data_activated_cb(GtkWidget* w, gpointer data);
  void data_activated(Data* d);

  DataOption(const DataOption &);
  const DataOption& operator=(const DataOption&);
};

#endif
