// -*- C++ -*-

/* 
 * view.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

// This file is documented later, keep scrolling down.

#ifndef GUPPI_VIEW_H
#define GUPPI_VIEW_H

#include <glib.h>
#include <vector>

// A view is something that is controlled by a model

class View {
public:
  View() {}
  virtual ~View() {}

  virtual void destroy_model() = 0;    
};

template <class T>
class Model {
  // not meant to be used; only the pointer version matters.
  //  but this is apparently needed by egcs?
};

class Model<void*> {
public:
  typedef vector<void*>::iterator iterator;
  typedef vector<void*>::const_iterator const_iterator;

  iterator begin()             { 
#ifdef GNOME_ENABLE_DEBUG
    if (!views_locked_) g_warning("begin() without a lock");
#endif
    return views_.begin(); 
  }
  const_iterator begin() const { 
#ifdef GNOME_ENABLE_DEBUG
    if (!views_locked_) g_warning("begin() without a lock");
#endif
    return views_.begin(); 
  }

  iterator end()               { 
    return views_.end();
  }
  const_iterator end() const   { 
    return views_.end(); 
  }
  
  void add_view(void* v) const { 
    lock_views();
#ifdef GNOME_ENABLE_DEBUG
    const_iterator i = find(v);
    if (i != views_.end()) g_warning("View added twice");
#endif
    views_.push_back(v); 
    unlock_views();
  }
  
  // *important* not to do this in a view action,
  //  since the model will be iterating over the 
  //  views. This is one of the major bugs we're trying to 
  //  catch with lock_views(), etc.
  void remove_view(void* v) const {
    lock_views();
    iterator i = const_cast<Model<void*>*>(this)->find(v); // blah blah
    if (i != views_.end())
      views_.erase(i);
#ifdef GNOME_ENABLE_DEBUG
    else 
      g_warning("View not known to model");
#endif
    unlock_views();
  }

  bool view_known(void* v) const { 
    lock_views();
    const_iterator i = find(v);
    unlock_views();
    return (i != views_.end());
  }

  void lock_views() const {
#ifdef GNOME_ENABLE_DEBUG
    if (!views_locked_) views_locked_ = true;
    else 
      g_warning("Attempt to lock already-locked viewlist");
#endif
  }

  void unlock_views() const {
#ifdef GNOME_ENABLE_DEBUG
    if (views_locked_) views_locked_ = false;
    else 
      g_warning("Attempt to unlock already-unlocked viewlist");
#endif
  }

  Model() 
#ifdef GNOME_ENABLE_DEBUG
    : views_locked_(false) 
#endif
    {
    }
  
  virtual ~Model() {
      lock_views();
      iterator i = begin();
      while (i != end()) {
        static_cast<View*>(*i)->destroy_model();
        ++i;
      }
      // don't unlock - we're destroyed,
      //  so you can't retrieve the lock.
  }
  
private:
  // we just make it mutable; disallowing views on const 
  //  objects is too annoying.
  mutable vector<void*> views_;

  // BTW, a set<> may be stupid. It is slower to iterate over views 
  //  and uses more space, just so adding and removing views is faster; 
  //  it may well make sense to use a vector instead, because 
  //  there is no real speed issue when adding/removing views.
  // But this can be changed at any time.

#ifdef GNOME_ENABLE_DEBUG
  mutable bool views_locked_;
#endif

  iterator find(void* v) {
    iterator i = views_.begin();
    while (i != views_.end())
      {
        if (*i == v)
          break;
        ++i;
      }
    return i;
  }

  const_iterator find(void* v) const {
    const_iterator i = views_.begin();
    while (i != views_.end())
      {
        if (*i == v)
          break;
        ++i;
      }
    return i;
  }
};


template <class T>
class Model<T*> : private Model<void*> {
public:
  typedef vector<T*>::iterator iterator;
  typedef vector<T*>::const_iterator const_iterator;
  typedef Model<void*> base;

  iterator begin()             { 
    return reinterpret_cast<iterator>(base::begin());
  }

  const_iterator begin() const { 
    return reinterpret_cast<iterator>(base::begin());
  }

  iterator end()               { 
    return reinterpret_cast<iterator>(base::end());
  }
  const_iterator end() const   { 
    return reinterpret_cast<iterator>(base::end());
  }
  
  void add_view(T* v) const { 
    base::add_view(static_cast<void*>(v));
  }
  
  void remove_view(T* v) const {
    base::remove_view(static_cast<void*>(v));
  }

  bool view_known(T* v) const { 
    return base::view_known(static_cast<void*>(v));
  }

  void lock_views() const {
    base::lock_views();
  }

  void unlock_views() const {
    base::unlock_views();
  }

  Model() 
    {}
  
  virtual ~Model() 
    {

    }
  
};

/*******  Model/View documentation

          A 'Model' is an object that can send certain messages to a
          corresponding set of 'View's. It's templatized according to
          the kind of view it matches.

          A Guppi object owns a model that sends the kind of messages
          it wants to send. A simple example is the Named object; it 
          has a model with a name_changed message. Say you have 
          'Named n'; you can send the message by calling 
          n.named_model.name_changed("new name") - this will iterate
          over the views the model owns, and tell each one about
          the change.
          
          The member model is called "named_model" instead of just
          "model" because inheritance allows a single object to 
          own multiple models.

          Views are abstract interfaces to be implemented by any object
          that wants to track the model. This keeps us from forgetting 
          one of the things to keep track of, since we must implement
          the entire interface.

          A concrete View implementation must model.add_view(*this) on 
          creation, and model.remove_view(*this) on destruction. 
          Conventionally, when a View gets the model_destroyed
          message, it sets its pointer to the object to 0, in 
          order to avoid further references.
          
          add_view() and remove_view() should always be nicely paired.
          To check,
          egrep 'remove_view|add_view' `find -name "*.h" -o -name "*.cc"` | sort | less
          is a nice tool. Also, remove_view() should be conditional 
          on the object still existing, so if(model) remove_view() is 
          a common construct.

          We could imitate signals and do remove_view() automatically,
          but it would involve adding data members to View, which 
          is bad IMO.

          Conventionally, the message in the model is in the past
          tense, like name_changed, and the action on the view is in
          the imperative, e.g. change_name. But the two functions
          should take the same args.

          

****************/

#endif // #ifdef GUPPI_VIEW_H
