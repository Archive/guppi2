// -*- C++ -*-

/* 
 * xylayers.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xylayers.h"
#include "scalardata.h"
#include "markerstyle.h"
#include "typeregistry.h"
#include "datagroup.h"

TypeRegistry* 
XyLayer::type_registry_ = 0;

TypeRegistry* 
XyLayer::type_registry()
{
  return type_registry_;
}

// And, in the stupid short-term hack category...
static string
sequential_name() 
{
  static char next = 'A';
  
  const char name[2] = {
    next, '\0'
  };
  
  ++next;
  if (next > 'Z')
    next = 'A';

  return name;
}

XyLayer::XyLayer(LayerType type)
  : type_(type), 
    x_axis_(PlotUtil::SOUTH), y_axis_(PlotUtil::WEST),
    hidden_(false), masked_(false)
{
  if (type_registry_ == 0)
    {
      type_registry_ = new TypeRegistry;
      type_registry_->add_builtin_type(ScatterLayer, DataLayer, 
                                       "ScatterLayer");
      type_registry_->add_builtin_type(LineLayer, DataLayer, 
                                       "LineLayer");
      type_registry_->add_builtin_type(DataLayer,  TypeRegistry::INVALID_TYPE, 
                                       "DataLayer");
      type_registry_->add_builtin_type(RelationLayer, TypeRegistry::INVALID_TYPE, 
                                       "RelationLayer");
      type_registry_->add_builtin_type(PriceLayer, TypeRegistry::INVALID_TYPE, 
                                       "PriceLayer");
    }

  type_registry_->ref();
  
  name_ = sequential_name();
}

XyLayer::~XyLayer()
{
  g_assert(type_registry_ != 0); // since we exist, refcount must be > 0

  if (type_registry_->unref() == 0)
    {
      delete type_registry_;
      type_registry_ = 0;
    }

}

gpointer 
XyLayer::cast_to_type(LayerType type)
{
  // Optimize this case first
  if (type == type_)
    return static_cast<gpointer>(this);
  else if (type_registry_->is_a(type_, type))
    return static_cast<gpointer>(this);
  else 
    return 0;
}

PlotUtil::PositionType
XyLayer::x_axis() const
{
  
  return x_axis_;
}

PlotUtil::PositionType 
XyLayer::y_axis() const
{

  return y_axis_;
}
  
void 
XyLayer::set_x_axis(PlotUtil::PositionType pos)
{
  if (pos == x_axis_)
    return;

  PlotUtil::PositionType old = x_axis_;

  x_axis_ = pos;

  layer_model.x_axis_changed(this, old, pos);
}

void 
XyLayer::set_y_axis(PlotUtil::PositionType pos)
{
  if (pos == y_axis_)
    return;

  PlotUtil::PositionType old = y_axis_;

  y_axis_ = pos;

  layer_model.y_axis_changed(this, old, pos);
}


void 
XyLayer::set_hidden(bool setting)
{
  if (setting == hidden_)
    return;

  hidden_ = setting;

  layer_model.hidden_changed(this, hidden_);
}

void 
XyLayer::set_masked(bool setting)
{
  if (setting == masked_)
    return;

  masked_ = setting;

  layer_model.masked_changed(this, masked_);
}

void 
XyLayer::set_name(const string& name)
{
  name_ = name;
  layer_model.name_changed(this, name_);
}

//////////////////////////


XyDataLayer::XyDataLayer(LayerType type, DataStore* ds)
  : XyLayer(type), store_(ds), pair_(0)
{
  g_return_if_fail(store_ != 0);

  store_->store_model.add_view(this);
  
  pair_ = new XyPair;

  grab_xypair(pair_);
  
  Data* data1 = 0;
  Data* data2 = 0;

  // Default to first two scalar data sets
  DataStore::iterator i = store_->begin();
  while (i != store_->end())
    {
      if ((*i)->is_a(Data::Scalar))
        {
          if (data1 != 0)
            {
              data2 = *i;
              break;
            }
          else
            data1 = *i;
        }
      ++i;
    }

  g_debug("Default data %p and %p", data1, data2);

  if (data1 != 0) 
    pair_->set_x_data(data1);
  if (data2 != 0) 
    pair_->set_y_data(data2);
}

XyDataLayer::~XyDataLayer()
{
  store_->store_model.remove_view(this);

  release_xypair();
}

const SortedPair& 
XyDataLayer::sorted() const
{
  return pair_->sorted();
}

void 
XyDataLayer::set_x_data(Data* d)
{
  if (d == (Data*)pair_->x_data()) return;
  
  if (d && d->is_a(Data::Scalar))
    {
      g_warning("Setting nonscalar data for XyDataLayer...");
      return;
    }

  // Causes view notification which updates everything.
  pair_->set_x_data(d);
}

ScalarData* 
XyDataLayer::x_data()
{
  return pair_->x_data();
}

const ScalarData* 
XyDataLayer::x_data() const
{
  return pair_->x_data();
}


void 
XyDataLayer::set_y_data(Data* d)
{
  if (d == (Data*)pair_->y_data()) return;

  if (d && d->is_a(Data::Scalar))
    {
      g_warning("Setting nonscalar data for XyDataLayer...");
      return;
    }
  
  // Causes view notification which updates everything.
  pair_->set_y_data(d);
}

ScalarData* 
XyDataLayer::y_data()
{
  return pair_->y_data();
}

const ScalarData* 
XyDataLayer::y_data() const
{
  return pair_->y_data();
}

guint 
XyDataLayer::npoints() const
{
  return pair_->npoints();
}

string 
XyDataLayer::generate_name() const
{
  string name; 

  if (pair_->x_data() == 0 && pair_->y_data() == 0)
    {
      name = _("No data to plot");
    }
  else
    {
      ScalarData* xdat = pair_->x_data();
      ScalarData* ydat = pair_->y_data();
      const string & xname = xdat ? xdat->name() : _("(no data)");
      const string & yname = ydat ? ydat->name() : _("(no data)");

      // 50 for the quotes and `vs.' in whatever language.
      guint bufsize = xname.size() + yname.size() + 50;
      char* buf = new char[bufsize];

      g_snprintf(buf, bufsize, _("`%s' vs. `%s'"), xname.c_str(), yname.c_str());
      
      name = buf;

      delete [] buf;
    }
  
  return name;
}

void 
XyDataLayer::share_datapair_with(XyDataLayer* partner)
{
  partner->set_xypair(pair_);
}

void 
XyDataLayer::hoard_datapair()
{
  XyPair* mine = pair_->clone();

  set_xypair(mine);
}

/// XyPair::View

void 
XyDataLayer::change_x_data(XyPair* pair, Data* old_data, Data* data)
{
  update_N_points();

  data_layer_model.x_data_changed(this, old_data, data);
}

void 
XyDataLayer::change_y_data(XyPair* pair, Data* old_data, Data* data)
{
  update_N_points();

  data_layer_model.y_data_changed(this, old_data, data);
}


/// DataStore::View
void 
XyDataLayer::add_data(DataStore* ds, Data* d)
{
  // Nothing
}

void 
XyDataLayer::remove_data(DataStore* ds, Data* d)
{
  // Setting the data results in view notification...

  if (d == pair_->x_data())
    {
      pair_->set_x_data(0);
    }

  // Note: not "else if," you can plot something vs. itself

  if (d == pair_->y_data())
    {
      pair_->set_y_data(0);
    }
}

void 
XyDataLayer::change_data_values(DataStore* ds, 
                                Data* d, 
                                const vector<guint> & which)
{
  data_layer_model.data_values_changed(this, d);
}

void 
XyDataLayer::change_data_values(DataStore* ds, Data* d)
{
  if (d == pair_->x_data() || d == pair_->y_data())
    update_N_points();  

  data_layer_model.data_values_changed(this, d);
}

void 
XyDataLayer::change_data_name(DataStore* ds, Data* d, const string & name)
{
  // Nothing  
}

/// ::View
void 
XyDataLayer::destroy_model()
{
  // Nothing
}

XyPair* 
XyDataLayer::xypair()
{
  return pair_;
}

void 
XyDataLayer::set_xypair(XyPair* pair)
{
  g_return_if_fail(pair != 0);

  if (pair == pair_)
    return;
  
  Data* old_x = pair_->x_data();
  Data* old_y = pair_->y_data();

  grab_xypair(pair);
  
  release_xypair();
  
  pair_ = pair;
 
  update_N_points();

  if(old_x != (Data*)pair_->x_data())
    data_layer_model.x_data_changed(this, old_x, pair_->x_data());

  if (old_y != (Data*)pair_->y_data())
    data_layer_model.y_data_changed(this, old_y, pair_->y_data());
}

void 
XyDataLayer::grab_xypair(XyPair* pair)
{
  if (pair != 0)
    {
      pair->ref();
      
      pair->pair_model.add_view(this);
    }
}
void 
XyDataLayer::release_xypair()
{
  if (pair_ != 0)
    {
      pair_->pair_model.remove_view(this);
      if (pair_->unref() == 0)
        {
          delete pair_;
        }
      pair_ = 0;
    }
}


void 
XyDataLayer::update_N_points()
{
  // Nothing; subclasses implement it
}

//////////////////////////

XyScatter::XyScatter(DataStore* ds)
  : XyDataLayer(XyLayer::ScatterLayer, ds), markers_(0)
{
  markers_ = new XyMarkers;

  grab_xymarkers(markers_);
}

XyScatter::~XyScatter()
{
  release_xymarkers();

}


void 
XyScatter::update_N_points()
{
  guint npts = npoints();
  
  // Make sure the markers object has enough entries.
  if (npts > 0)
    markers_->reserve(npts);
  
  g_assert(markers_->size() >= npts);
}


void 
XyScatter::set_style(guint point, guint style_id)
{
  markers_->set_style(point, style_id);
}

void 
XyScatter::set_global_style(guint style_id)
{
  markers_->set_global_style(style_id);
}


void 
XyScatter::set_style(const vector<guint> & points, guint style_id)
{
  markers_->set_style(points, style_id);
}

double 
XyScatter::largest_style()
{
  return markers_->largest_style();
}

bool   
XyScatter::style_in_use(guint32 styleid)
{
  return markers_->style_in_use(styleid);
}

bool 
XyScatter::using_global_style() const
{
  return markers_->using_global_style();
}

guint 
XyScatter::global_style() const
{
  return markers_->global_style();
}

bool 
XyScatter::using_small_styles() const
{
  return markers_->using_small_styles();
}

const guint8* 
XyScatter::small_styles() const
{
  g_assert(markers_->size() >= npoints());
  return markers_->small_styles();
}

const guint16* 
XyScatter::large_styles() const
{
  g_assert(markers_->size() >= npoints());
  return markers_->large_styles();
}


void 
XyScatter::change_styles(XyMarkers* markers)
{
  xy_scatter_model.styles_changed(this);
}

void 
XyScatter::destroy_model()
{
  // nothing
}

void 
XyScatter::share_markers_with(XyScatter* partner)
{
  partner->set_xymarkers(markers_);
}

void 
XyScatter::hoard_markers()
{
  XyMarkers* mine = markers_->clone();

  // Releases the old one.
  set_xymarkers(mine);
}

void 
XyScatter::grab_xymarkers(XyMarkers* markers)
{
  if (markers != 0)
    {
      markers->ref();

      // Make sure the size of the markers array is 
      // large enough for all scatter plots using it.
      markers->reserve(npoints());
      
      markers->markers_model.add_view(this);
    }

}

void 
XyScatter::release_xymarkers()
{
  if (markers_ != 0)
    {
      markers_->markers_model.remove_view(this);
      if (markers_->unref() == 0)
        {
          delete markers_;
        }
      markers_ = 0;
    }
}

XyMarkers* 
XyScatter::xymarkers()
{
  return markers_;
}

void 
XyScatter::set_xymarkers(XyMarkers* markers)
{
  g_return_if_fail(markers != 0);

  if (markers == markers_)
    return;

  grab_xymarkers(markers);

  release_xymarkers();

  markers_ = markers;

  xy_scatter_model.styles_changed(this);
}

/////////////////////////

XyLine::XyLine(DataStore* ds)
  : XyDataLayer(XyLayer::LineLayer, ds)
{
  
}

XyLine::~XyLine()
{


}

void 
XyLine::update_N_points()
{
  guint npts = npoints();

  // um, nothing yet (since this object isn't implemented...)
}

//////////////////////

XyRelation::XyRelation()
  : XyLayer(XyLayer::RelationLayer), precision_(5.0)
{

}

XyRelation::~XyRelation()
{

}

void 
XyRelation::set_precision(double prec)
{
  precision_ = prec;
  xy_relation_model.relation_changed(this);
}

string 
XyRelation::generate_name() const
{
  return _("Untitled Relation");
}

//////////////////////////////////////////////


XyPriceBars::XyPriceBars(DataStore* ds)
  : XyLayer(XyLayer::PriceLayer), store_(ds), data_(0)
{

}

XyPriceBars::~XyPriceBars()
{
  release_data(data_);
  data_ = 0;
}

void 
XyPriceBars::set_data(Data* d)
{
  Data* old = data_;

  if (data_ != 0)
    {
      old->ref();
      release_data(data_);
      data_ = 0;
    }

  if (d && d->is_a(guppi_price_data_spec()->id()))
    {
      data_ = d->cast_to_group();

      grab_data(data_);
    }
  else
    g_warning("Wrong type of data set for price bars.");

  price_model.data_changed(this, old, data_);
  if (old != 0 && 
      old->unref() == 0)
    delete old;
}

DataGroup* 
XyPriceBars::data()
{
  return data_;
}


const DataGroup* 
XyPriceBars::data() const
{
  return data_;
}

guint 
XyPriceBars::nbars() const
{
  // FIXME we could cache this like XyDataLayer does.
  if (data_ != 0)
    {
      Data* opens, *his, *los, *closes, *dates;

      dates = data_->get_data(PriceDates);
      opens = data_->get_data(PriceOpens);
      his = data_->get_data(PriceHighs);
      los = data_->get_data(PriceLows);
      closes = data_->get_data(PriceCloses);

      if (his && los)
        {
          guint minsize = dates->size();

          minsize = MIN(minsize, his->size());
          minsize = MIN(minsize, los->size());
          if (opens != 0)
            minsize = MIN(minsize, opens->size());
          if (closes != 0)
            minsize = MIN(minsize, closes->size());

          return minsize;
        }
      else
        return 0;
    }
  else 
    return 0;
}

string 
XyPriceBars::generate_name() const
{
  return "Price Bars"; // FIXME
}

void 
XyPriceBars::add_data(DataStore* ds, Data* d)
{
  // Nothing
  
}


void 
XyPriceBars::remove_data(DataStore* ds, Data* d)
{
  // Remember that we can have multiple references to a single dataset

  if (d == data_)
    set_data(0);
}


void 
XyPriceBars::change_data_values(DataStore* ds, 
                                Data* d, 
                                const vector<guint> & which)
{  
  if (!our_data(d))
    return;

  price_model.data_values_changed(this, d);
}

void 
XyPriceBars::change_data_values(DataStore* ds, Data* d)
{
  if (!our_data(d))
    return;

  // Number of points in d may have changed; check this if we ever cache
  //  nbars

  price_model.data_values_changed(this, d);
}

void 
XyPriceBars::change_data_name(DataStore* ds, Data* d, const string & name)
{
  // nothing
}

void 
XyPriceBars::destroy_model()
{
  // nothing
}

void 
XyPriceBars::grab_data(Data* d)
{
  if (d != 0)
    d->ref();
}

void 
XyPriceBars::release_data(Data* d)
{
  if (d != 0)
    if (d->unref() == 0)
      delete d;
}

bool 
XyPriceBars::our_data(Data* d)
{
  if (d == data_)
    return true;

  if (data_ != 0)
    {
      Data* dat;
      dat = data_->get_data(PriceDates);
      if (dat == d)
        return true;

      dat = data_->get_data(PriceOpens);
      if (dat == d)
        return true;

      dat = data_->get_data(PriceHighs);
      if (dat == d)
        return true;
      
      dat = data_->get_data(PriceLows);
      if (dat == d)
        return true;

      dat = data_->get_data(PriceCloses);
      if (dat == d)
        return true;
    }
  
  return false;
}
