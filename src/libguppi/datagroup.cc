// -*- C++ -*-

/* 
 * datagroup.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "datagroup.h"
#include "typeregistry.h"

DataGroupSpec::DataGroupSpec()
  : locked_(false), type_id_(Data::Invalid)
{
  TypeRegistry* tr = Data::type_registry();
  g_assert(tr != 0);

  tr->ref();

  type_id_ = static_cast<Data::DataType>(tr->add_runtime_type(Data::Group, "unnamed datagroup subtype"));
}

DataGroupSpec::~DataGroupSpec()
{
  TypeRegistry* tr = Data::type_registry();
  if (tr->unref() == 0)
    delete tr;
}

void 
DataGroupSpec::add_slot(Data::DataType type, const string& name)
{
  g_return_if_fail(!locked_);
  slot_types_.push_back(type);
  slot_names_.push_back(name);
}

void
DataGroupSpec::set_name(const string& name)
{
  g_return_if_fail(!locked_);
  name_ = name;
  Data::type_registry()->set_type_name(type_id_, name_.c_str());
}

///////////////////////////

DataGroup::DataGroup(DataGroupSpec* spec)
  : Data(spec->id()), spec_(0)
{
  g_return_if_fail(spec != 0);
  g_return_if_fail(spec->locked());

  guint N = spec->n_slots();
  
  g_return_if_fail(N > 0);

  slot_contents_.reserve(N);

  guint i = 0;
  while (i < N)
    {
      slot_contents_.push_back(0);
      ++i;
    }

  spec_ = spec;
  spec_->ref();
}

DataGroup::~DataGroup()
{
  if (spec_->unref() == 0)
    delete spec_;

  vector<Data*>::iterator i = slot_contents_.begin();
  while (i != slot_contents_.end())
    {
      if (*i != 0)
        if ((*i)->unref() == 0)
          delete *i;
      ++i;
    }
}

void 
DataGroup::set_data(guint slot, Data* d)
{
  // save typing, easier to read
  Data** space = &slot_contents_[slot];

  if (*space != 0)
    {
      if ((*space)->unref() == 0)
        delete *space;

      *space = 0;
    }

  *space = d;

  if (*space != 0)
    (*space)->ref();

  data_model.values_changed(this);
}

Data* 
DataGroup::get_data(guint slot) 
{ 
  g_return_val_if_fail(slot < slot_contents_.size(), 0);
  return slot_contents_[slot]; 
}

const Data* 
DataGroup::get_data(guint slot) const
{ 
  g_return_val_if_fail(slot < slot_contents_.size(), 0);
  return slot_contents_[slot]; 
}

gsize 
DataGroup::size() const
{
  g_warning("FIXME, requesting size of DataGroup");
  return 0;
}

const string& 
DataGroup::name() const
{
  return name_;
}

void 
DataGroup::set_name(const string & name)
{
  name_ = name;
  data_model.name_changed(this, name_);
}

xmlNodePtr 
DataGroup::xml(xmlNodePtr parent) const
{
  GUPPI_NOT_IMPLEMENTED;
  return 0;
}

gsize 
DataGroup::byte_size()
{
  return sizeof(DataGroup);
}

