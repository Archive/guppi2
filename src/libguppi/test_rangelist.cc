

#include <stdio.h>
#include <stdlib.h>

#include "rangelist.h"

static void print_rangelist(rangelist<int>& rl)
{
  printf(" - ");
  rangelist<int>::iterator i = rl.begin();
  while (i != rl.end())
    {
      double start, stop;
      int val;
      i.dereference(&start, &stop, &val);

      printf("([%g,%g] %d) ", start, stop, val);
      
      ++i;
    }
  printf(" - \n");
}

static void
check_rangelist(rangelist<int>& rl)
{
  double last_start = -G_MAXFLOAT;
  double last_stop  = -G_MAXFLOAT;
  int count = 0;

  rangelist<int>::iterator i = rl.begin();
  while (i != rl.end())
    {
      double start, stop;
      int val;
      i.dereference(&start, &stop, &val);

      if (start < last_stop)
        {
          printf("Error in rangelist: start of node %d is less than end of previous node\n",
                 count);
        }
      
      if (stop < start)
        {
          printf("Error in rangelist: stop of node %d less than its start\n",
                 count);
        }

      last_start = start;
      last_stop = stop;
      
      ++count;
      ++i;
    }
}

int main(int argc, char** argv)
{
  rangelist<int> rl;

  int seq_no = 0;
  while (seq_no < 1000000)
    {
      double v1 = ((double)rand())/(double)(RAND_MAX/10000);
      double v2 = ((double)rand())/(double)(RAND_MAX/10000);

      //      printf("       Inserting %g - %g (value %d)\n", MIN(v1,v2), MAX(v1,v2), seq_no);
      
      rl.insert(MIN(v1,v2), MAX(v1,v2), seq_no);

      ++seq_no;
    }
  
  printf("Random ranges:\n");
  check_rangelist(rl);
  print_rangelist(rl);

  // This makes the list long and we start to get yucky performance due to lame algorithms.
  while (seq_no < 2000000)
    {
      double v1 = ((double)rand())/(double)(RAND_MAX/100);
      double v2 = ((double)rand())/(double)(RAND_MAX/100);
      double offset = ((double)rand())/(double)(RAND_MAX/9900);

      v1 += offset;
      v2 += offset;

      //      printf("       Inserting %g - %g (value %d)\n", MIN(v1,v2), MAX(v1,v2), seq_no);
      
      rl.insert(MIN(v1,v2), MAX(v1,v2), seq_no);

      ++seq_no;
    }

  printf("Small ranges over top of random ranges:\n");
  check_rangelist(rl);
  print_rangelist(rl);

  return 0;
}
