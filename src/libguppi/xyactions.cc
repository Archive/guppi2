// -*- C++ -*-

/* 
 * xyactions.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xyactions.h"
#include "xyplot.h"
#include "ezrubberband.h"
#include "markerstyle.h"
#include "pointtiles.h"
#include "scalardata.h"
#include "fixedtip.h"
#include "typeregistry.h"
#include <algorithm>

static void 
get_points_in_region(XyDataLayer* layer, 
                     double vx1, double vy1, double vx2, double vy2, 
                     vector<guint>& fillme)
{
  g_return_if_fail(layer != 0);

  PointTiles* tiles = layer->sorted().tiles();

  if (tiles == 0)
    return;

  vector<const PointTiles::Tile*> edgetiles;
  vector<const PointTiles::Tile*> intiles;
  
  tiles->tiles_in_rect(edgetiles, intiles, vx1, vy1, vx2, vy2);

  vector<const PointTiles::Tile*>::const_iterator ti = edgetiles.begin();
  vector<const PointTiles::Tile*>::const_iterator tend = edgetiles.end();

  const SortedPair& pair = layer->sorted();

  while (ti != tend)
    {
      const PointTiles::Tile* t = *ti;

      // Iterate over points in the tile, there's much room for
      // optimization here...

      vector<guint>::const_iterator ci = t->contents.begin();
      vector<guint>::const_iterator cend = t->contents.end();
      while (ci != cend)
        {
          double x = pair.real_key_value(*ci);
          double y = pair.real_other_value(*ci);

          if (x >= vx1 && x <= vx2 && y >= vy1 && y <= vy2)
            {
              fillme.push_back(*ci);
            }
      
          ++ci;
        }
      ++ti;

      // Temporary hack to catch the intiles
      if (ti == edgetiles.end())
        {
          ti = intiles.begin();
          tend = intiles.end();
        }
    }
}

static bool
get_nearest_point(XyPlot* xyp,
                  double x, double y,
                  XyDataLayer** best_layer,
                  guint* best_point)
{
  *best_layer = 0;
  *best_point = 0;

  // Collect all scatter/line plots that we want to affect.
  vector<XyDataLayer*> layers;
      
  XyPlotState::iterator i   = xyp->state()->begin();
  XyPlotState::iterator end = xyp->state()->end();
      
  while (i != end)
    {
      if (!(*i)->hidden() && !(*i)->masked() &&
          (*i)->type_registry()->is_a((*i)->type(), XyLayer::DataLayer))
        {
          XyDataLayer* layer = (XyDataLayer*)(*i)->cast_to_type(XyLayer::DataLayer);
          
          g_assert(layer != 0);
          
          layers.push_back(layer);
        }

      ++i;
    }

  // Reverse the plots, because we want to prefer the one 
  //  on top.
  reverse(layers.begin(), layers.end());

  bool found_points = false;
  double best_so_far = 0.0;

  vector<XyDataLayer*>::iterator j = layers.begin();

  while (j != layers.end())
    {
      XyDataLayer* layer = *j;

      TypeRegistry* tr = layer->type_registry();

      g_assert(tr != 0);

      PlotUtil::PositionType xaxis = layer->x_axis();
      PlotUtil::PositionType yaxis = layer->y_axis();
      const Transform & xtrans = xyp->state()->trans(xaxis);
      const Transform & ytrans = xyp->state()->trans(yaxis);

      double vx1, vy1, vx2, vy2;

      double closeness;
      
      if (tr->is_a(layer->type(), XyLayer::ScatterLayer))
        closeness = 
          ((XyScatter*)layer->cast_to_type(XyLayer::ScatterLayer))->largest_style();
      else
        closeness = 1.0; // for "corners" of a line plot

      vx1 = xtrans.inverse(x - closeness);
      vy1 = ytrans.inverse(y + closeness);
      vx2 = xtrans.inverse(x + closeness);
      vy2 = ytrans.inverse(y - closeness);

      vector<guint> found;

      get_points_in_region(layer,
                           vx1, vy1, vx2, vy2, 
                           found);      

      if (!found.empty())
        {
          ScalarData* xdat = layer->x_data();
          ScalarData* ydat = layer->y_data();

          g_assert(xdat && ydat);

          vector<double> xs;
          vector<double> ys;

          vector<guint>::iterator fi = found.begin();           
          while (fi != found.end())
            {
              xs.push_back(xdat->get_scalar(*fi));
              ys.push_back(ydat->get_scalar(*fi));
                               
              ++fi;
            }

          g_assert(xs.size() == ys.size());

          // Find best match
          guint pi = 0;
          double best = closeness*10;
          guint best_index = 0; // best index into found; found contains real index
          while (pi < xs.size())
            {
              // hypot() can take negative arguments, I'm declaring.
              double dist = PlotUtil::hypot((xs[pi] - x), 
                                            (ys[pi] - y));

              if (dist <= best)
                {
                  best = dist;
                  best_index = pi;
                }

              ++pi;
            }

          if (!found_points || (best < best_so_far))
            {
              best_so_far = best;
              *best_layer = layer;
              *best_point = found[best_index];
            }

          found_points = true;
        }

      ++j;
    }

  return found_points;
}


// Select a rectangular area and resize axes to match it
// (i.e., zoom in on a rectangular area.)
// We want to have this action keep a stack of zooms and be associated
//  with an AxisExpander which moves back up the stack
class AxisShrinker : public Action {
public:
  AxisShrinker() 
    : Action("guppi-xy-axis-shrinker"),
      name_(_("Crop")),
      
      tooltip_(_("Selects a rectangular region and zooms the plot to that region.")),
      band_(0), current_(0)
    {}
  virtual ~AxisShrinker() 
    {
      if (band_ != 0) 
        {
          gtk_object_destroy(GTK_OBJECT(band_));
          band_ = 0;
        }
    }
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);
  
private:
  string name_;
  string path_;
  string tooltip_;

  EZRubberband* band_;

  // what we're rubberbanding over
  XyPlot* current_;

  static void snapped_cb(GnomeCanvasItem* rb, 
                         double x1, double y1, 
                         double x2, double y2,
                         gpointer data);

  void snapped(double x1, double y1,
               double x2, double y2);

  static void cancelled_cb(GnomeCanvasItem* rb,
                           gpointer data);

  void cancelled();
};

gint
AxisShrinker::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the AxisShrinker action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* xy = static_cast<XyPlot*>(p);

  if (band_ != 0)
    {
      // Already rubberbanding, nothing for us to do.
      // Hopefully the existing rubberband is not on another plot!
      return FALSE;
    }

  if (event->type == GDK_BUTTON_PRESS)
    {
      grab(p);

      current_ = xy;

      band_ = ez_rubberband_new(xy->item(),
                                event->button.x,
                                event->button.y,
                                event->button.time,
                                event->button.button,
                                0);

      gtk_signal_connect(GTK_OBJECT(band_),
                         "snapped",
                         GTK_SIGNAL_FUNC(snapped_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(band_),
                         "cancelled",
                         GTK_SIGNAL_FUNC(cancelled_cb),
                         this);

      return TRUE;
    }

  return FALSE;
}

void 
AxisShrinker::snapped_cb(GnomeCanvasItem* rb, 
                         double x1, double y1, 
                         double x2, double y2,
                         gpointer data)
{
  AxisShrinker* as = static_cast<AxisShrinker*>(data);

  as->snapped(x1,y1,x2,y2);
}

void 
AxisShrinker::snapped(double x1, double y1,
                      double x2, double y2)
{
  XyPlot* sp = current_;
  
  gtk_object_destroy(GTK_OBJECT(band_));
  band_ = 0;
  current_ = 0;

  g_return_if_fail(sp != 0);

  vector<PlotUtil::PositionType> positions;
  vector<double> starts;
  vector<double> stops;

  int i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      const PlotUtil::PositionType pos = (PlotUtil::PositionType) i;      

      // Don't affect hidden axes
      if (sp->state()->axis(pos).hidden())
        {
          ++i;
          continue;
        }

      const Transform & trans = sp->state()->trans(pos);
      double v1, v2;

      // Since the transform contains a flip, we need to map 'y2' which is
      // toward the bottom of the screen to 'v1' which is also toward the
      // bottom of the screen.      

      if (PlotUtil::vertical(pos))
        {
          v1 = trans.inverse(y2);
          v2 = trans.inverse(y1);
        }
      else 
        {
          v1 = trans.inverse(x1);
          v2 = trans.inverse(x2);
        }

      positions.push_back(pos);
      starts.push_back(v1);
      stops.push_back(v2);

      ++i;
    }

  // Move to new area.
  if (!positions.empty())
    sp->state()->push_axis_bounds(positions, starts, stops);
  else
    {
      sp->set_status(_("All axes are hidden."));
      gdk_beep();
    }

  ungrab(sp);
}

void 
AxisShrinker::cancelled_cb(GnomeCanvasItem* rb,
                           gpointer data)
{
  AxisShrinker* as = static_cast<AxisShrinker*>(data);

  as->cancelled();
}

void 
AxisShrinker::cancelled()
{
  gtk_object_destroy(GTK_OBJECT(band_));
  band_ = 0;
  ungrab(current_);
  current_ = 0;
}


// Opposite of AxisShrinker
class AxisExpander : public Action {
public:
  AxisExpander() 
    : Action("guppi-xy-axis-expander"),
      name_(_("UnZoom")),

      tooltip_(_("Re-widens the axes after you've used Crop or Telescope"))
    {}
  virtual ~AxisExpander() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

};

gint
AxisExpander::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the AxisExpander action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      if (sp->state()->axis_bounds_stack_size() == 0)
        {
          gdk_beep();
          sp->set_status(_("Nothing else in the zoom history."));
        }
      else 
        sp->state()->pop_axis_bounds();

      return TRUE;
    }

  return FALSE;
}

/// Panning

// pan proportional to distance of mouse pointer from the middle 
//  of the plot
static void 
pan_axis(XyPlot* xyp, PlotUtil::PositionType pos, double item_coord)
{
  const Transform & trans = xyp->state()->trans(pos);

  double v = trans.inverse(item_coord);

  double old1 = xyp->state()->axis(pos).start();
  double old2 = xyp->state()->axis(pos).stop();

  double middle = old1 + (old2 - old1)/2;

  double delta = v - middle;

  double new1 = old1 + delta;
  double new2 = old2 + delta;

  Axis* a = xyp->state()->checkout_axis(pos);  
  
  if (a != 0)
    {
      a->set_start(new1);
      a->set_stop(new2);
      
      xyp->state()->checkin_axis(pos, a);
    }
  else 
    g_warning("Couldn't check out axis to modify it!");
}

/// Horizontal Panning 

class HPanner : public Action {
public:
  HPanner() 
    : Action("guppi-xy-horizontal-panner"),
      name_(_("Horizontal Pan")),

      tooltip_(_("Horizontal panning action, moves X axis range left and right"))
    {}
  virtual ~HPanner() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;
  
};

gint
HPanner::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the HPanner action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      const Transform & strans = sp->state()->trans(PlotUtil::SOUTH);

      double x = event->button.x;
      double dummy = 0.0;

      gnome_canvas_item_w2i(sp->item(), &x, &dummy);

      pan_axis(sp, PlotUtil::NORTH, x);
      pan_axis(sp, PlotUtil::SOUTH, x);
     
      return TRUE;
    }

  return FALSE;
}

/// Vertical Panning 

class VPanner : public Action {
public:
  VPanner() 
    : Action("guppi-xy-vertical-panner"),
      name_(_("Vertical Pan")),

      tooltip_(_("Vertical panning action, moves Y axis range left and right"))
    {}
  virtual ~VPanner() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  
};

gint
VPanner::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the VPanner action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      double y = event->button.y;
      double dummy = 0.0;

      gnome_canvas_item_w2i(sp->item(), &dummy, &y);

      pan_axis(sp, PlotUtil::WEST, y);
      pan_axis(sp, PlotUtil::EAST, y);
           
      return TRUE;
    }

  return FALSE;
}


// Zoom in to a clicked point action

class Telescope : public Action {
public:
  Telescope() 
    : Action("guppi-xy-telescope"),
      name_(_("Telescope")),
      tooltip_(_("Zoom in on the clicked point")),
      percent_(0.9)
    {}
  virtual ~Telescope() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

  void set_percent(double percent) { percent_ = percent; }

private:
  string name_;
  string path_;
  string tooltip_;

  // Percent of old dimensions (X/Y) to retain
  double percent_;
};

gint
Telescope::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the Telescope action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      double x = event->button.x;
      double y = event->button.y;

      gnome_canvas_item_w2i(sp->item(), &x, &y);

      vector<PlotUtil::PositionType> positions;
      vector<double> starts;
      vector<double> stops;
      
      int i = 0;
      while (i < PlotUtil::PositionTypeEnd)
        {
          const PlotUtil::PositionType pos = (PlotUtil::PositionType) i;      
          
          // Don't affect hidden axes
          if (sp->state()->axis(pos).hidden())
            {
              ++i;
              continue;
            }
          
          const Transform & trans = sp->state()->trans(pos);
          double v;
          
          double v1 = sp->state()->axis(pos).start();
          double v2 = sp->state()->axis(pos).stop();
                    
          if (PlotUtil::vertical(pos))
            {
              v = trans.inverse(y);
            }
          else 
            {
              v = trans.inverse(x);
            }

          double newv1 = v - (v - v1) * percent_;
          double newv2 = v + (v2 - v) * percent_;

          positions.push_back(pos);
          starts.push_back(newv1);
          stops.push_back(newv2);
          
          ++i;
        }
      
      // Move to new area.
      if (!positions.empty())
        sp->state()->push_axis_bounds(positions, starts, stops);
      else
        {
          sp->set_status(_("All axes are hidden."));
          gdk_beep();
        }
 
      return TRUE;
    }

  return FALSE;
}

// Brush action, applies a style to points around the mouse pointer.

class Brush : public Action {
public:
  Brush() 
    : Action("guppi-xy-brush"),
      name_(_("Brush")),
      tooltip_(_("Apply a style to scatter plot points by \"brushing\" them with the mouse"))
    {}
  virtual ~Brush() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  bool brushing_;
};

gint
Brush::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the Brush action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (brushing_ && event->type == GDK_BUTTON_RELEASE)
    {
      brushing_ = false;
      ungrab(sp);
      gnome_canvas_item_ungrab(sp->item(), event->button.time);
      return TRUE;
    }

  else if (event->type == GDK_BUTTON_PRESS || 
           (brushing_ && event->type == GDK_MOTION_NOTIFY))
    {
      gdouble x, y;

      if (event->type == GDK_BUTTON_PRESS)
        {
          GdkCursor* ptr = gdk_cursor_new(GDK_DOTBOX);
          gnome_canvas_item_grab(sp->item(), 
                                 GDK_POINTER_MOTION_MASK | 
                                 GDK_BUTTON_RELEASE_MASK,
                                 ptr,
                                 event->button.time);
          gdk_cursor_destroy(ptr);
          x = event->button.x;
          y = event->button.y;
          grab(sp);

          brushing_ = true;
        }
      else if (event->type == GDK_MOTION_NOTIFY)
        {
          gint wx, wy;
          GdkModifierType state;
          gdk_window_get_pointer(GTK_WIDGET(sp->item()->canvas)->window,
                                 &wx, &wy, &state);
          gnome_canvas_window_to_world(sp->item()->canvas, wx, wy, &x, &y);
        }
      else
        {
          g_warning("Should not happen, %s", __FUNCTION__);
          x = y = 0;
        }
      
      gnome_canvas_item_w2i(sp->item(), &x, &y);

      static const double brush_size = 8; // size of 1/2 GDK_DOTBOX on XFree, lame

      double v1s[PlotUtil::PositionTypeEnd];
      double v2s[PlotUtil::PositionTypeEnd];

      int j = 0;
      while (j < PlotUtil::PositionTypeEnd)
        {
          PlotUtil::PositionType pos = (PlotUtil::PositionType) j;

          const Transform & trans = sp->state()->trans(pos);

          // Since the transform contains a flip, we need to 
          // do X and Y in opposite directions
          
          if (PlotUtil::vertical(pos))
            {
              v1s[pos] = trans.inverse(y + brush_size);
              v2s[pos] = trans.inverse(y - brush_size);
            }
          else
            {
              v1s[pos] = trans.inverse(x - brush_size);
              v2s[pos] = trans.inverse(x + brush_size);
            }
          
          ++j;
        }
     
      XyPlotState::iterator i   = sp->state()->begin();
      XyPlotState::iterator end = sp->state()->end();
      
      while (i != end)
        {
          if (!(*i)->hidden() && !(*i)->masked() &&
              (*i)->type_registry()->is_a((*i)->type(), XyLayer::ScatterLayer))
            {
              XyScatter* layer = (XyScatter*)(*i)->cast_to_type(XyLayer::ScatterLayer);

              g_assert(layer != 0);

              PlotUtil::PositionType xaxis = layer->x_axis();
              PlotUtil::PositionType yaxis = layer->y_axis();

              vector<guint> toset;

              g_debug("%g,%g  %g,%g is the data region", 
                      v1s[xaxis], v1s[yaxis], 
                      v2s[xaxis], v2s[yaxis]);

              get_points_in_region(layer,
                                   v1s[xaxis], v1s[yaxis], 
                                   v2s[xaxis], v2s[yaxis], 
                                   toset);
              
              g_debug("%u points to set on layer %p", toset.size(), layer);

              if (!toset.empty())
                layer->set_style(toset, guppi_marker_palette()->current());
            }

          ++i;
        }

      return TRUE;
    }
  else 
    {
      return FALSE;
    }
}


//////////////// PointID
// Shows nearby points in statusbar

class PointID : public Action {
public:
  PointID() 
    : Action("guppi-xy-pointid"),
      name_(_("Identify Point")),

      tooltip_(_("Show values of nearby scatter/line layer points")),
      in_drag_(false), fixedtip_(0)
    {}
  virtual ~PointID() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  // not really a drag, but... the button is down
  bool in_drag_;
  GtkWidget* fixedtip_;

  // update tooltip and statusbar
  void do_update(XyPlot* sp);
};

gint
PointID::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the PointID action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      if (in_drag_)
        return FALSE;

      GdkCursor* ptr = gdk_cursor_new(GDK_HAND2);

      gnome_canvas_item_grab(sp->item(),
                             GDK_POINTER_MOTION_MASK | 
                             GDK_BUTTON_RELEASE_MASK | 
                             GDK_BUTTON_PRESS_MASK,
                             ptr,
                             event->button.time);
              
      gdk_cursor_destroy(ptr);

      grab(sp);

      in_drag_ = true;

      fixedtip_ = gtk_fixedtip_new();

      do_update(sp);

      return TRUE;
    }
  else if (event->type == GDK_MOTION_NOTIFY)
    {
      if (in_drag_)
        {
          do_update(sp);
          return TRUE;
        }
    }
  else if (event->type == GDK_BUTTON_RELEASE)
    {
      // we also end the drag if a button other than the initial one is 
      // pressed; should be harmless.
      if (in_drag_)
        {
          gnome_canvas_item_ungrab(sp->item(),
                                   event->button.time);
          ungrab(sp);
          
          in_drag_ = false;

          if (fixedtip_ != 0)
            {
              gtk_widget_destroy(fixedtip_);
              fixedtip_ = 0;
            }
          return TRUE;
        }
    }

  return FALSE;
}

void 
PointID::do_update(XyPlot* sp)
{
  g_assert(fixedtip_ != 0);
  g_assert(in_drag_);

  gdouble x, y;
  gint wx, wy;
  GdkModifierType state;
  gdk_window_get_pointer(GTK_WIDGET(sp->item()->canvas)->window,
                         &wx, &wy, &state);
  gnome_canvas_window_to_world(sp->item()->canvas, wx, wy, &x, &y);
  gnome_canvas_item_w2i(sp->item(), &x, &y);

  // Collect all scatter/line plots that we want to affect.
  vector<XyDataLayer*> layers;
      
  XyPlotState::iterator i   = sp->state()->begin();
  XyPlotState::iterator end = sp->state()->end();
      
  while (i != end)
    {
      if (!(*i)->hidden() && !(*i)->masked() &&
          (*i)->type_registry()->is_a((*i)->type(), XyLayer::DataLayer))
        {
          XyDataLayer* layer = (XyDataLayer*)(*i)->cast_to_type(XyLayer::DataLayer);

          g_assert(layer != 0);

          layers.push_back(layer);

        }

      ++i;
    }

  // Reverse the plots, because we want to prefer the one 
  //  on top.
  reverse(layers.begin(), layers.end());

  bool found_points = false;
  double best_so_far = 0.0;

  string tip_string;
  string status_string;

  vector<XyDataLayer*>::iterator j = layers.begin();

  while (j != layers.end())
    {
      XyDataLayer* layer = *j;

      TypeRegistry* tr = layer->type_registry();

      g_assert(tr != 0);

      PlotUtil::PositionType xaxis = layer->x_axis();
      PlotUtil::PositionType yaxis = layer->y_axis();
      const Transform & xtrans = sp->state()->trans(xaxis);
      const Transform & ytrans = sp->state()->trans(yaxis);

      double vx1, vy1, vx2, vy2;

      double closeness;
      
      if (tr->is_a(layer->type(), XyLayer::ScatterLayer))
        closeness = 
          ((XyScatter*)layer->cast_to_type(XyLayer::ScatterLayer))->largest_style();
      else
        closeness = 1.0; // for "corners" of a line plot

      vx1 = xtrans.inverse(x - closeness);
      vy1 = ytrans.inverse(y + closeness);
      vx2 = xtrans.inverse(x + closeness);
      vy2 = ytrans.inverse(y - closeness);

      vector<guint> found;

      get_points_in_region(layer,
                           vx1, vy1, vx2, vy2, 
                           found);      

      if (!found.empty())
        {
          ScalarData* xdat = layer->x_data();
          ScalarData* ydat = layer->y_data();

          g_assert(xdat && ydat);

          vector<double> xs;
          vector<double> ys;

          vector<guint>::iterator fi = found.begin();           
          while (fi != found.end())
            {
              xs.push_back(xdat->get_scalar(*fi));
              ys.push_back(ydat->get_scalar(*fi));
                               
              ++fi;
            }

          g_assert(xs.size() == ys.size());

          // Find best match
          guint pi = 0;
          double best = closeness*10;
          guint best_index = 0; // best index into found; found contains real index
          while (pi < xs.size())
            {
              // hypot() can take negative arguments, I'm declaring.
              double dist = PlotUtil::hypot((xs[pi] - x), 
                                            (ys[pi] - y));

              if (dist <= best)
                {
                  best = dist;
                  best_index = pi;
                }

              ++pi;
            }

          if (!found_points || (best < best_so_far))
            {
              best_so_far = best;

              string stats;
              gchar buf[128];
              g_snprintf(buf, 127, _("%u points nearby in layer %s "), 
                         found.size(), layer->name().c_str()); 
              
              stats += buf;

              g_snprintf(buf, 128, _("Closest index %u: (%g, %g)"), 
                         found[best_index], xs[best_index], ys[best_index]);
              stats += buf;

              status_string = stats;
                            
              g_snprintf(buf, 128, _("Layer: %s\nPoint: %u\nX: %g\nY: %g"),
                         layer->name().c_str(),
                         found[best_index],
                         xs[best_index], ys[best_index]);

              tip_string = buf;
            }

          found_points = true;
        }

      ++j;
    }

  if (!found_points)
    {
      // The snprintf is pointless now, I know.
      gchar buf[128];
      g_snprintf(buf, 127, _("No points nearby."));
      
      gtk_widget_hide(fixedtip_);
      
      sp->set_status(buf);
    }
  else
    {
      sp->set_status(status_string);

      gtk_fixedtip_set_text(GTK_FIXEDTIP(fixedtip_), tip_string.c_str());
      
      GdkWindow* canvas_window = GTK_WIDGET(sp->item()->canvas)->window;
      
      gtk_fixedtip_popup(GTK_FIXEDTIP(fixedtip_), wx, wy + 15, canvas_window);      
    }
}

/////////////////// Recenter, RecenterX, RecenterY

static void
recenter_axes(XyPlot* xyp, 
              double x, double y,
              const PlotUtil::PositionType axes[], 
              guint naxes)
{
  vector<PlotUtil::PositionType> positions;
  vector<double> starts;
  vector<double> stops;

  int i = 0;
  while (i < (int)naxes)
    {
      PlotUtil::PositionType pos = axes[i];

      if (xyp->state()->axis(pos).hidden())
        {
          ++i;
          continue;
        }

      const Transform & trans = xyp->state()->trans(pos);      

      double v1 = xyp->state()->axis(pos).start();
      double v2 = xyp->state()->axis(pos).stop();

      double v;

      if (PlotUtil::vertical(pos))
        {
          v = trans.inverse(y);
        }
      else 
        {
          v = trans.inverse(x);
        }

      double len = v2 - v1;

      double new_v1 = v - len/2;
      double new_v2 = v + len/2;
      
      positions.push_back(pos);
      starts.push_back(new_v1);
      stops.push_back(new_v2);

      ++i;
    }
  
  if (!positions.empty())
    xyp->state()->push_axis_bounds(positions, starts, stops);
  else
    {
      xyp->set_status(_("All axes hidden!"));
      gdk_beep();
    }
}

class Recenter : public Action {
public:
  Recenter() 
    : Action("guppi-xy-recenter"),
      name_(_("Recenter")),

      tooltip_(_("Center the axes around the clicked point"))
    {}
  virtual ~Recenter() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  
};

gint
Recenter::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the Recenter action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      double x, y;

      x = event->button.x;
      y = event->button.y;

      gnome_canvas_item_w2i(sp->item(), &x, &y);

      static const PlotUtil::PositionType axes[] = {
        PlotUtil::NORTH,
        PlotUtil::SOUTH,
        PlotUtil::EAST,
        PlotUtil::WEST
      };

      recenter_axes(sp, x, y, axes, sizeof(axes)/sizeof(axes[0]));

      return TRUE;
    }

  return FALSE;
}


class RecenterY : public Action {
public:
  RecenterY() 
    : Action("guppi-xy-recenter-y"),
      name_(_("Recenter Y")),

      tooltip_(_("Center the Y axis around the clicked point"))
    {}
  virtual ~RecenterY() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  
};

gint
RecenterY::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the RecenterY action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      double x, y;

      x = event->button.x;
      y = event->button.y;

      gnome_canvas_item_w2i(sp->item(), &x, &y);

      static const PlotUtil::PositionType axes[] = {
        PlotUtil::EAST,
        PlotUtil::WEST
      };

      recenter_axes(sp, x, y, axes, sizeof(axes)/sizeof(axes[0]));

      return TRUE;
    }

  return FALSE;
}


class RecenterX : public Action {
public:
  RecenterX() 
    : Action("guppi-xy-recenter-x"),
      name_(_("Recenter X")),

      tooltip_(_("Center the X axis around the clicked point"))
    {}
  virtual ~RecenterX() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  
};

gint
RecenterX::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the RecenterX action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      double x, y;

      x = event->button.x;
      y = event->button.y;

      gnome_canvas_item_w2i(sp->item(), &x, &y);

      static const PlotUtil::PositionType axes[] = {
        PlotUtil::NORTH,
        PlotUtil::SOUTH
      };

      recenter_axes(sp, x, y, axes, sizeof(axes)/sizeof(axes[0]));

      return TRUE;
    }

  return FALSE;
}

////////////////// Select-and-style


class Styler : public Action {
public:
  Styler() 
    : Action("guppi-xy-styler"),
      name_(_("Style Region")),

      tooltip_(_("Select a rectangular region and apply a scatter point style to it")),
      band_(0)
    {}
  virtual ~Styler() 
    {
      if (band_ != 0) 
        {
          gtk_object_destroy(GTK_OBJECT(band_));
          band_ = 0;
        }
    }
  
  virtual const string& name() const { return name_; }

  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  EZRubberband* band_;

  XyPlot* current_;

  static void snapped_cb(GnomeCanvasItem* rb, 
                         double x1, double y1, 
                         double x2, double y2,
                         gpointer data);

  void snapped(double x1, double y1,
               double x2, double y2);

  static void cancelled_cb(GnomeCanvasItem* rb,
                           gpointer data);

  void cancelled();

};

gint
Styler::event(Plot* p, GdkEvent* event)
{
  g_debug("Event to the Styler action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (band_ != 0)
    {
      // Already rubberbanding, nothing for us to do.
      return FALSE;
    }

  if (event->type == GDK_BUTTON_PRESS)
    {
      grab(p);

      current_ = sp;

      band_ = ez_rubberband_new(sp->item(),
                                event->button.x,
                                event->button.y,

                                event->button.time,
                                event->button.button,
                                0);

      gtk_signal_connect(GTK_OBJECT(band_),
                         "snapped",
                         GTK_SIGNAL_FUNC(snapped_cb),
                         this);

      gtk_signal_connect(GTK_OBJECT(band_),
                         "cancelled",
                         GTK_SIGNAL_FUNC(cancelled_cb),
                         this);

      return TRUE;
    }

  return FALSE;
}

void 
Styler::snapped_cb(GnomeCanvasItem* rb, 
                   double x1, double y1, 
                   double x2, double y2,
                   gpointer data)
{
  Styler* as = static_cast<Styler*>(data);

  as->snapped(x1,y1,x2,y2);
}

void 
Styler::snapped(double x1, double y1,
                double x2, double y2)
{
  gtk_object_destroy(GTK_OBJECT(band_));
  band_ = 0;
  
  XyPlot* sp = current_;

  current_ = 0;

  g_return_if_fail(sp != 0);

  double v1s[PlotUtil::PositionTypeEnd];
  double v2s[PlotUtil::PositionTypeEnd];
  
  int j = 0;
  while (j < PlotUtil::PositionTypeEnd)
    {
      PlotUtil::PositionType pos = (PlotUtil::PositionType) j;
      
      const Transform & trans = sp->state()->trans(pos);
      
      // Since the transform contains a flip, we need to 
      // do X and Y in opposite directions
      
      if (PlotUtil::vertical(pos))
        {
          v1s[pos] = trans.inverse(y2);
          v2s[pos] = trans.inverse(y1);
        }
      else
        {
          v1s[pos] = trans.inverse(x1);
          v2s[pos] = trans.inverse(x2);
        }
      
      ++j;
    }
      
  XyPlotState::iterator i   = sp->state()->begin();
  XyPlotState::iterator end = sp->state()->end();
  
  while (i != end)
    {
      if (!(*i)->hidden() && !(*i)->masked() &&
          (*i)->type_registry()->is_a((*i)->type(), XyLayer::ScatterLayer))
        {
          XyScatter* layer = (XyScatter*)(*i)->cast_to_type(XyLayer::ScatterLayer);
          
          g_assert(layer != 0);

          PlotUtil::PositionType xaxis = layer->x_axis();
          PlotUtil::PositionType yaxis = layer->y_axis();

          vector<guint> toset;
          
          get_points_in_region(layer, 
                               v1s[xaxis], v1s[yaxis], 
                               v2s[xaxis], v2s[yaxis], 
                               toset);

          if (!toset.empty())
            layer->set_style(toset, guppi_marker_palette()->current());
        }
      
      ++i;
    }

  ungrab(sp);
}

void 
Styler::cancelled_cb(GnomeCanvasItem* rb,
                     gpointer data)
{
  Styler* as = static_cast<Styler*>(data);

  as->cancelled();
}

void 
Styler::cancelled()
{
  gtk_object_destroy(GTK_OBJECT(band_));
  band_ = 0;
  ungrab(current_);
  current_ = 0;
}


//////////////// PointDrag
// Shows nearby points in statusbar

class PointDrag : public Action {
public:
  PointDrag() 
    : Action("guppi-xy-pointdrag"),
      name_(_("Drag Point")),

      tooltip_(_("Change value of a point by dragging it")),
      in_drag_(false),
      drag_point_(0),
      drag_layer_(0),
      fixedtip_(0)
    {}
  virtual ~PointDrag() {}
  
  virtual const string& name() const { return name_; }

  // tooltip describing the action
  virtual const string& tooltip() const { return tooltip_; }

  virtual gint event(Plot* p, GdkEvent* event);

private:
  string name_;
  string path_;
  string tooltip_;

  bool in_drag_;
  guint drag_point_;
  XyDataLayer* drag_layer_;

  GtkWidget* fixedtip_;

  void end_drag(XyPlot* xyp, guint32 when);
};

gint
PointDrag::event(Plot* p, GdkEvent* event)
{
  //  g_debug("Event to the PointDrag action");

  g_return_val_if_fail(p->check(XyPlot::ID), FALSE);

  XyPlot* sp = static_cast<XyPlot*>(p);

  if (event->type == GDK_BUTTON_PRESS)
    {
      if (in_drag_)
        return FALSE;

      gdouble x, y;
      gint wx, wy;
      GdkModifierType state;
      gdk_window_get_pointer(GTK_WIDGET(sp->item()->canvas)->window,
                             &wx, &wy, &state);
      gnome_canvas_window_to_world(sp->item()->canvas, wx, wy, &x, &y);
      gnome_canvas_item_w2i(sp->item(), &x, &y);

      guint best_point;
      XyDataLayer* best_layer;

      if (get_nearest_point(sp, x, y, &best_layer, &best_point))
        {
          g_assert(best_layer != 0);
          
          ScalarData* xdat = best_layer->x_data();
          ScalarData* ydat = best_layer->y_data();

          g_assert(xdat && ydat); // because there were points

          char buf[128];
          g_snprintf(buf, 128, _("Dragging point %u: (%g, %g) Layer %s"),
                     best_point,
                     xdat->get_scalar(best_point), 
                     ydat->get_scalar(best_point),
                     best_layer->name().c_str());

          drag_point_ = best_point;
          drag_layer_ = best_layer;
              
          GdkCursor* ptr = gdk_cursor_new(GDK_HAND2);
          
          gnome_canvas_item_grab(sp->item(),
                                 GDK_POINTER_MOTION_MASK | 
                                 GDK_BUTTON_RELEASE_MASK | 
                                 GDK_BUTTON_PRESS_MASK,
                                 ptr,
                                 event->button.time);
              
          gdk_cursor_destroy(ptr);

          grab(p);

          in_drag_ = true;

          sp->set_status(string(buf));

          // Show status in a GtkFixedtip to the upper left of the window...

          fixedtip_ = gtk_fixedtip_new();

          g_snprintf(buf, 128, _("Layer: %s\nPoint: %u\nX: %g\nY: %g"),
                     drag_layer_->name().c_str(),
                     best_point,
                     xdat->get_scalar(best_point), 
                     ydat->get_scalar(best_point));

          gtk_fixedtip_set_text(GTK_FIXEDTIP(fixedtip_), buf);

          GdkWindow* canvas_window = GTK_WIDGET(sp->item()->canvas)->window;

          gtk_fixedtip_popup(GTK_FIXEDTIP(fixedtip_), -50, -50, canvas_window);
          
        }
      else
        {
          gchar buf[128];
          g_snprintf(buf, 127, _("No points nearby."));
          
          sp->set_status(buf);
          gdk_beep();
        }

      return TRUE;
    }
  else if (in_drag_)
    {
      if (event->type == GDK_BUTTON_RELEASE)
        {
          end_drag(sp, event->button.time);
          return TRUE;
        }
      else if (event->type == GDK_MOTION_NOTIFY)
        {
          double x, y;
          gint wx, wy;
          GdkModifierType state;
          gdk_window_get_pointer(GTK_WIDGET(sp->item()->canvas)->window,
                                 &wx, &wy, &state);
          gnome_canvas_window_to_world(sp->item()->canvas, wx, wy, &x, &y);
          
          gnome_canvas_item_w2i(sp->item(), &x, &y);

          const Transform & xtrans = sp->state()->trans(drag_layer_->x_axis());
          const Transform & ytrans = sp->state()->trans(drag_layer_->y_axis());
  
          double vx, vy;
          
          // Since the transform contains a flip, we need to map 'y2' which is
          // toward the bottom of the screen to 'vy1' which is also toward the
          // bottom of the screen.
          vx = xtrans.inverse(x);
          vy = ytrans.inverse(y);

          // vx and vy are our new values for the point, but we have to make 
          // sure the point hasn't disappeared...

          if (drag_point_ < drag_layer_->npoints())
            {
              ScalarData* xdat = drag_layer_->x_data();
              ScalarData* ydat = drag_layer_->y_data();

              // Because we have a grab, we assume no one has removed the data
              g_assert(xdat && ydat);

              xdat->set_scalar(drag_point_, vx);
              ydat->set_scalar(drag_point_, vy);

              char buf[128];
              g_snprintf(buf, 128, _("Point %u (%g, %g) from Layer %s"), 
                         drag_point_, vx, vy,
                         drag_layer_->name().c_str());
              sp->set_status(string(buf));

              // Also show status in the fixed tip
              g_snprintf(buf, 128, _("Layer: %s\nPoint: %u\nX: %g\nY: %g"),
                         drag_layer_->name().c_str(),
                         drag_point_, 
                         vx, vy);

              gtk_fixedtip_set_text(GTK_FIXEDTIP(fixedtip_), buf);

              GdkWindow* canvas_window = GTK_WIDGET(sp->item()->canvas)->window;
              
              gtk_fixedtip_popup(GTK_FIXEDTIP(fixedtip_), -50, -50, canvas_window);
            }
          else
            {
              sp->set_status(_("Point no longer exists!"));
              gdk_beep();
              end_drag(sp, event->button.time);
            }
          
          return TRUE;
        }
    }

  return FALSE;
}

void
PointDrag::end_drag(XyPlot* xyp, guint32 when)
{
  in_drag_ = false;
  
  drag_point_ = 0;
  drag_layer_ = 0;

  gtk_widget_destroy(fixedtip_);
  
  gnome_canvas_item_ungrab(xyp->item(), when);
  
  ungrab(xyp);
}

/////////////////////////// Menu entries


#include "markerpaletteedit.h"
#include "menuentry.h"

class PaletteEdit : public MenuEntry {
public:
  PaletteEdit();
  
  void invoke(Plot* p);
};

PaletteEdit::PaletteEdit() 
  : MenuEntry("guppi-xy-show-palette-editor", 
              _("Marker Style Palette"),
              _("Change the current and available marker styles"))
{

}

void
PaletteEdit::invoke(Plot* p)
{
  guppi_marker_palette_edit()->edit();
}

///

class OptimalAxes : public MenuEntry {
public:
  OptimalAxes();
  
  void invoke(Plot* p);
};

OptimalAxes::OptimalAxes() 
  : MenuEntry("guppi-xy-set-optimal-axes", 
              _("Optimize Axes"),
              _("Fit the axes to the data, so all data is on the plot"))
{

}

void
OptimalAxes::invoke(Plot* p)
{
  XyPlot* sp = static_cast<XyPlot*>(p);

  sp->state()->optimize_all_axes();
}

///

class AddLayer : public MenuEntry {
public:

  AddLayer(const char* id, const char* name, const char* hint);

  void invoke(Plot* p);

protected:
  virtual XyLayer* make_layer(XyPlot* plot) = 0;
};

AddLayer::AddLayer(const char* id, const char* name, const char* hint) 
  : MenuEntry(id,
              name, 
              hint)
{

}

void
AddLayer::invoke(Plot* p)
{
  XyPlot* sp = static_cast<XyPlot*>(p);

  XyLayer* layer = make_layer(sp);

  sp->state()->add_layer(layer);
}


/// 

class AddScatter : public AddLayer {
public:
  AddScatter()
    : AddLayer("guppi-xy-add-scatter-layer", 
               _("Add Scatter Plot Layer"),
               _("Add a new scatter plot to the XY plot"))
    {}
protected:
  virtual XyLayer* make_layer(XyPlot* plot) {
    return new XyScatter(plot->state()->store());
  }
};


class AddLine : public AddLayer {
public:
  AddLine()
    : AddLayer("guppi-xy-add-line-layer", 
               _("Add Line Plot Layer"),
               _("Add a new line plot to the XY plot"))
    {}
protected:
  virtual XyLayer* make_layer(XyPlot* plot) {
    return new XyLine(plot->state()->store());
  }
};

class AddPrice : public AddLayer {
public:
  AddPrice()
    : AddLayer("guppi-xy-add-price-layer", 
               _("Add Price Bars Layer"),
               _("Add price bars to the XY plot"))
    {}
protected:
  virtual XyLayer* make_layer(XyPlot* plot) {
    return new XyPriceBars(plot->state()->store());
  }
};


///

class XyProperties : public MenuEntry {
public:
  XyProperties();
  
  void invoke(Plot* p);
};

XyProperties::XyProperties() 
  : MenuEntry("guppi-xy-show-layer-properties", 
              _("Layer Properties"),
              _("Layer properties dialog for the XY plot"))
{

}

void
XyProperties::invoke(Plot* p)
{
  XyPlot* sp = static_cast<XyPlot*>(p);
  
  sp->edit();
}

///

class XyAxisProperties : public MenuEntry {
public:
  XyAxisProperties();
  
  void invoke(Plot* p);
};

XyAxisProperties::XyAxisProperties() 
  : MenuEntry("guppi-xy-show-axis-properties", 
              _("Axis Properties"),
              _("Axis properties dialog for the XY plot"))
{

}

void
XyAxisProperties::invoke(Plot* p)
{
  XyPlot* sp = static_cast<XyPlot*>(p);
  
  sp->axis_edit();
}

///

#include "printer-gnome.h"
#include "state-print.h"

class XyPrint : public MenuEntry {
public:
  XyPrint();
  
  void invoke(Plot* p);
};

XyPrint::XyPrint() 
  : MenuEntry("guppi-xy-print", 
              _("Print"),
              _("Print the plot"))
{

}
class XyPrintAction : public PrintAction {
public:
  XyPrintAction(XyPlot* p) 
    : plot_(p)
    {
      g_return_if_fail(plot_ != 0);

      plot_->ref();
    }

  ~XyPrintAction() {
    if (plot_->unref() == 0)
      delete plot_;
  }

  void print(Printer* p) {
    guppi_print_xy(p, plot_->state());
    if (!p->sync())
      gnome_error_dialog(_("Printing didn't work for some reason. We're taking patches to improve this error message. :-)"));
    else
      gnome_ok_dialog(_("Printing complete."));
  }

private:
  XyPlot* plot_;

};

void
XyPrint::invoke(Plot* p)
{
  g_return_if_fail(p->check(XyPlot::ID));

  XyPlot* sp = static_cast<XyPlot*>(p);
  
  PrintAction* pa = new XyPrintAction(sp);
  
  // get the printer via a dialog, print, and destroy the PrintAction
  guppi_get_printer_and_print(pa, true);
}

///////////////////////////

#include "plotstore.h"

void 
guppi_gnome_init_xy()
{
  PlotStore* ps = guppi_plot_store();

  ps->add_plot(XyPlot::ID);

  ps->add_action(XyPlot::ID, new AxisShrinker);
  ps->add_action(XyPlot::ID, new AxisExpander);
  ps->add_action(XyPlot::ID, new Brush);
  ps->add_action(XyPlot::ID, new PointID);
  ps->add_action(XyPlot::ID, new Styler);
  ps->add_action(XyPlot::ID, new HPanner);
  ps->add_action(XyPlot::ID, new VPanner);
  ps->add_action(XyPlot::ID, new Telescope);
  ps->add_action(XyPlot::ID, new PointDrag);
  ps->add_action(XyPlot::ID, new Recenter);
  ps->add_action(XyPlot::ID, new RecenterX);
  ps->add_action(XyPlot::ID, new RecenterY);

  ps->add_menuentry(XyPlot::ID, new XyPrint);
  ps->add_menuentry(XyPlot::ID, new XyProperties);
  ps->add_menuentry(XyPlot::ID, new XyAxisProperties);
  ps->add_menuentry(XyPlot::ID, new PaletteEdit);
  ps->add_menuentry(XyPlot::ID, new OptimalAxes);
  ps->add_menuentry(XyPlot::ID, new AddScatter);
  ps->add_menuentry(XyPlot::ID, new AddLine);
  ps->add_menuentry(XyPlot::ID, new AddPrice);
}



