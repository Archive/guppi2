// -*- C++ -*-

/* 
 * data.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "data.h"
#include "typeregistry.h"

TypeRegistry* 
Data::type_registry_ = 0;

TypeRegistry* 
Data::type_registry()
{
  if (type_registry_ == 0)
    {
      type_registry_ = new TypeRegistry;
      type_registry_->add_builtin_type(Scalar, TypeRegistry::INVALID_TYPE,
                                       "Scalar");
      type_registry_->add_builtin_type(Label, TypeRegistry::INVALID_TYPE,
                                       "Label");
      type_registry_->add_builtin_type(Categorical, TypeRegistry::INVALID_TYPE,
                                       "Categorical");
      type_registry_->add_builtin_type(Date, TypeRegistry::INVALID_TYPE,
                                       "Date");
      type_registry_->add_builtin_type(Group, TypeRegistry::INVALID_TYPE,
                                       "Group");

    }
  return type_registry_;
}

Data::Data(DataType type) 
  : type_(type)
{
  type_registry()->ref();
}

Data::~Data()
{
#ifdef DEBUG_RC
  g_debug("Destroying data %p refcount %u", this, rc_.count());
#endif
  g_assert(type_registry_ != 0);

  if (type_registry_->unref() == 0)
    {
      delete type_registry_;
      type_registry_ = 0;
    }
}


gconstpointer 
Data::cast_to_type(DataType type) const
{
  // Optimize this case first
  if (type == type_)
    return static_cast<gconstpointer>(this);
  else if (type_registry_->is_a(type_, type))
    return static_cast<gconstpointer>(this);
  else 
    return 0;
}

gpointer 
Data::cast_to_type(DataType type)
{
  // Optimize this case first
  if (type == type_)
    return static_cast<gpointer>(this);
  else if (type_registry_->is_a(type_, type))
    return static_cast<gpointer>(this);
  else 
    return 0;
}


ScalarData* 
Data::cast_to_scalar()
{
  return static_cast<ScalarData*>(cast_to_type(Scalar));
}

CategoricalData* 
Data::cast_to_categorical()
{
  return static_cast<CategoricalData*>(cast_to_type(Categorical));
}

DateData* 
Data::cast_to_date()
{
  return static_cast<DateData*>(cast_to_type(Date));
}

LabelData* 
Data::cast_to_label()
{
  return static_cast<LabelData*>(cast_to_type(Label));
}

DataGroup*
Data::cast_to_group()
{
  return static_cast<DataGroup*>(cast_to_type(Group));
}

const ScalarData* 
Data::cast_to_scalar() const
{
  return static_cast<const ScalarData*>(cast_to_type(Scalar));
}

const CategoricalData* 
Data::cast_to_categorical() const
{
  return static_cast<const CategoricalData*>(cast_to_type(Categorical));
}

const DateData* 
Data::cast_to_date() const
{
  return static_cast<const DateData*>(cast_to_type(Date));
}

const LabelData* 
Data::cast_to_label() const
{
  return static_cast<const LabelData*>(cast_to_type(Label));
}

const DataGroup*
Data::cast_to_group() const
{
  return static_cast<const DataGroup*>(cast_to_type(Group));
}

bool 
Data::is_a(DataType type) const
{
  if (type == type_) // optimization
    return true;
  else
    return type_registry_->is_a(type_, type);
}

guint
Data::ref()
{
#ifdef DEBUG_RC
  g_debug("Referencing data %p oldcount %u", this, rc_.count());
#endif
  return rc_.ref();
}

guint
Data::unref()
{

#ifdef DEBUG_RC
  g_debug("Unreferencing data %p oldcount %u", this, rc_.count());
#endif

  return rc_.unref();
}
