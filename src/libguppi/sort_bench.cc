

#include <time.h>
#include <stdio.h>

#include "plotutils.h"
#include "util.h"

#include <algorithm>

static inline void start_time(guint* store)
{
  *store = clock();
}
static inline void check_time(const char* name, guint* store, guint* elapsed)
{
  guint now = clock();
  g_debug("   %s: %u clocks elapsed", name, now - *store);
  *elapsed += (now - *store);
}

static const guint NVALS = 250000;

static double* tmp_buf = 0;
static double* random_values = 0;
static double* duplicate_values = 0;
static double* ascending_values = 0;
static double* descending_values = 0;
static double* stepping = 0;
static double* scratch_values = 0;

int doublecmp(const void* a, const void* b)
{
  if ((*(double*)a) > (*(double*)b))
    return 1;
  else if ((*(double*)a) < (*(double*)b))
    return -1;
  else 
    return 0;
}

void
make_stepping_up(double* array, guint N, guint interval)
{
  double d = 0.0;
  guint i = 0;
  while (i < N)
    {
      array[i] = d;

      if (N % interval == 0)
        d += 5.0;

      ++i;
    }
}

void
make_stepping_down(double* array, guint N, guint interval)
{
  double d = 0.0;
  guint i = 0;
  while (i < N)
    {
      array[i] = d;

      if (N % interval == 0)
        d -= 5.0;

      ++i;
    }
}


void
make_stepping_alternate(double* array, guint N, guint interval, guint flip_interval)
{
  double d = 0.0;
  double inc = 5.0;
  guint steps = 0;

  guint i = 0;
  while (i < N)
    {
      array[i] = d;

      if (N % interval == 0)
        {
          if (steps % flip_interval == 0)
            {
              inc = -inc;
            }

          d += inc;
          ++steps;
        }

      ++i;
    }
}


void
run_qsort_bench(const char* name, double* xvalues)
{
  guint elapsed = 0;
  int i = 0;
  while (i < 4)
    {
      memcpy(tmp_buf, xvalues, sizeof(double)*NVALS);
      guint store;
      start_time(&store);
      // Not really fair; it doesn't sort the Y values, only the X array
      qsort(tmp_buf, NVALS, sizeof(double), doublecmp);
      check_time(name, &store, &elapsed);
      ++i;
    }
  g_debug("Total %s: %u clocks, average %u (%g sec)", name, 
          elapsed, elapsed/10,
          (double)elapsed/(double)CLOCKS_PER_SEC/10.0);
}

void
run_stl_bench(const char* name, double* xvalues)
{
  guint elapsed = 0;
  int i = 0;
  while (i < 4)
    {
      memcpy(tmp_buf, xvalues, sizeof(double)*NVALS);
      guint store;
      start_time(&store);
      // Not really fair; it doesn't sort the Y values, only the X array
      sort(tmp_buf, tmp_buf+NVALS);
      check_time(name, &store, &elapsed);
      ++i;
    }
  g_debug("Total %s: %u clocks, average %u (%g sec)", name, 
          elapsed, elapsed/10,
          (double)elapsed/(double)CLOCKS_PER_SEC/10.0);
}

void
run_bench(const char* name, double* xvalues)
{
  guint elapsed = 0;
  int i = 0;
  while (i < 4)
    {
      memcpy(tmp_buf, xvalues, sizeof(double)*NVALS);
      guint store;
      start_time(&store);
      PlotUtil::sort_axes(tmp_buf, scratch_values, NVALS);
      check_time(name, &store, &elapsed);
      ++i;
    }
  g_debug("Total %s: %u clocks, average %u (%g sec)", name, 
          elapsed, elapsed/10,
          (double)elapsed/(double)CLOCKS_PER_SEC/10.0);
}

int main(int argc, char** argv)
{
  printf("Preparing... ");

  tmp_buf = new double[NVALS];
  random_values = new double[NVALS];
  duplicate_values = new double[NVALS];
  ascending_values = new double[NVALS];
  descending_values = new double[NVALS];
  scratch_values = new double[NVALS];

  guint i = 0;
  while (i < NVALS)
    {
      random_values[i] = rand();
      ++i;
    }

  i = 0;
  while (i < NVALS)
    {
      duplicate_values[i] = 5.0;
      ++i;
    }
  
  i = 0;
  while (i < NVALS)
    {
      ascending_values[i] = i*5.0;
      ++i;
    }

  i = 0;
  while (i < NVALS)
    {
      descending_values[i] = i*-5.0;
      ++i;
    }

  i = 0;
  while (i < NVALS)
    {
      scratch_values[i] = i*5.0;
      ++i;
    }

  printf("done.\n");

  run_bench("Random", random_values);

  // qsort is much slower due to function calls, STL is faster for the
  // single array, but that's not comparable really.
  // run_qsort_bench("Random qsort (one array)", random_values);
  // run_stl_bench("Random STL (one array)", random_values);

  run_bench("Ascending", ascending_values);
  run_bench("Descending", descending_values);
  run_bench("Duplicate", duplicate_values);

  stepping = new double[NVALS];

  i = 2;
  while (i < 7)
    {
      make_stepping_up(stepping, NVALS, i);
      char* tmp = g_strdup_printf("Upward steps, length %u", i);
      run_bench(tmp, stepping);
      g_free(tmp);
      ++i;
    }

  i = 2;
  while (i < 7)
    {
      make_stepping_down(stepping, NVALS, i);
      char* tmp = g_strdup_printf("Downward steps, length %u", i);
      run_bench(tmp, stepping);
      g_free(tmp);
      ++i;
    }

  i = 2;
  while (i < 6)
    {
      guint j = 1;
      while (j < 5)
        {
          make_stepping_alternate(stepping, NVALS, i, j);
          char* tmp = g_strdup_printf("Alternate steps, length %u flip %u", i, j);
          run_bench(tmp, stepping);
          g_free(tmp);
          ++j;
        }
      ++i;
    }

  delete [] tmp_buf;
  delete [] random_values;
  delete [] duplicate_values;
  delete [] ascending_values;
  delete [] descending_values;
  delete [] scratch_values;
  delete [] stepping;
}
