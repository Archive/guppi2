/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */
/* Arc item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * Author: Havoc Pennington <hp@pobox.com>
 */

#ifndef GNOME_CANVAS_ARC_H
#define GNOME_CANVAS_ARC_H

#include <libgnome/gnome-defs.h>

/* Change this to include local "gnome-canvas.h" once we go in gnome-libs */
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-rect-ellipse.h>

#include <libart_lgpl/art_svp.h>

BEGIN_GNOME_DECLS


#define GNOME_TYPE_CANVAS_ARC            (gnome_canvas_arc_get_type ())
#define GNOME_CANVAS_ARC(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_ARC, GnomeCanvasArc))
#define GNOME_CANVAS_ARC_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_ARC, GnomeCanvasArcClass))
#define GNOME_IS_CANVAS_ARC(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_ARC))
#define GNOME_IS_CANVAS_ARC_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_ARC))


typedef struct _GnomeCanvasArc GnomeCanvasArc;
typedef struct _GnomeCanvasArcClass GnomeCanvasArcClass;

struct _GnomeCanvasArc {
	GnomeCanvasRE re;
        double angle1, angle2;
};

struct _GnomeCanvasArcClass {
	GnomeCanvasREClass parent_class;
};


/* Standard Gtk function */
GtkType gnome_canvas_arc_get_type (void);


END_GNOME_DECLS

#endif
