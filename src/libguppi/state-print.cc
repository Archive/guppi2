// -*- C++ -*-

/* 
 * state-print.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "state-print.h"

#include "xyplotstate.h"
#include "barplotstate.h"
#include "printer.h"
#include "markerstyle.h"
#include "typeregistry.h"
#include "scalardata.h"
#include "plotutils.h"

// Remember throughout: PostScript coordinate system starts in lower left,
// plot state transforms and item coordinates are for X/GnomeCanvas, so they
// start in top left.

// These functions mostly just draw junk right now.
// Feel free to make them draw something remotely related to 
//  the plot...

////////// FIXME neither of these functions do anything useful; please
////////// write them!

void 
guppi_print_bar    (Printer* p, BarPlotState* state)
{  
  p->gsave();
  
  p->setcolor(Color(255,0,0,255));

  p->newpath();
  p->moveto(145,590);
  p->lineto(155 + 300, 590);
  p->lineto(155 + 300, 590);
  p->lineto(145,630);

  p->fill();

  p->grestore();
}
 
void 
guppi_print_pie    (Printer* p, PiePlotState* state)
{  
  p->gsave();
  
  p->setcolor(Color(255,0,0,255));

  p->newpath();
  p->moveto(145,590);
  p->lineto(155 + 300, 590);
  p->lineto(155 + 300, 590);
  p->lineto(145,630);

  p->fill();

  p->grestore();
}


//////////////////// XY printing


static void 
print_xy_internal(Printer* p, XyPlotState* state, 
                  const Transform& x_i2ps,
                  const Transform& y_i2ps);

void 
guppi_print_xy(Printer* p, XyPlotState* state)
{
  g_return_if_fail(state != 0);
  g_return_if_fail(p != 0);

  p->gsave();

  Transform x_i2ps;
  Transform y_i2ps;

  // Right now the transforms just vertically flip the image to fit PS
  // coordinates, but later they'll also position us on the page, etc.
  // The 30.0 is to help see what's going on while I debug
  x_i2ps.set_range_bounds(0.0, state->width());
  x_i2ps.set_screen_bounds(30.0, state->width()+30);

  y_i2ps.set_range_bounds(0.0, state->height());
  y_i2ps.set_screen_bounds(state->height()+30.0, 30.0);

  print_xy_internal(p, state, x_i2ps, y_i2ps);

  p->grestore();
}

static void
print_scatter_layer(Printer* p, XyPlotState* state,
                    XyScatter* layer,
                    const Transform& xtrans, 
                    const Transform& ytrans,
                    const Transform& x_i2ps,
                    const Transform& y_i2ps)
{
  const guint N = layer->npoints();

  if (N == 0) 
    return;  

  const SortedPair& pair = layer->sorted();

  MarkerPalette* palette = guppi_marker_palette();

  g_assert(palette != 0);

  const MarkerStyle* global_style = 0;
  const guint8* small_styles = 0;
  const guint16* large_styles = 0;
  if (layer->using_global_style())
    {
      global_style = palette->get(layer->global_style());
    }
  else if (layer->using_small_styles())
    {
      small_styles = layer->small_styles();
    }
  else 
    {
      large_styles = layer->large_styles();
    }

  guint i = 0;
  while (i < N)
    {
      p->gsave();

      Affine::Point pt;

      pt.x = pair.real_key_value(i);
      pt.y = pair.real_other_value(i);


      pt.x = xtrans.transform(pt.x);
      pt.y = ytrans.transform(pt.y);

      pt.x = x_i2ps.transform(pt.x);
      pt.y = y_i2ps.transform(pt.y);

      const MarkerStyle* s = 0;
      if (global_style) s = global_style;
      else if (small_styles)
        {
          s = palette->get(small_styles[i]);
        }
      else if (large_styles)
        {
          s = palette->get(large_styles[i]);
        }

      Color c;
      MarkerStyle::Type t = MarkerStyle::DOT;

      if (s)
        {
          c = s->color();
          t = s->type();
        }

      p->setcolor(c);

      if (t == MarkerStyle::DOT)
        {
          // special-case this, since size is irrelevant
          // should be a circle, but for now it's a rect - I don't know how to use curveto()
          p->newpath();
          p->moveto(pt.x-0.5, pt.y-0.5);
          p->lineto(pt.x+0.5, pt.y-0.5);
          p->lineto(pt.x+0.5, pt.y+0.5);
          p->lineto(pt.x-0.5, pt.y+0.5);
          p->closepath();
          p->fill();
        }      
      else 
        {
          g_assert(s != 0); // because we default to DOT if no style

          double markersize = 1.0;
          if (s) 
            markersize = s->size();

          // The +0.5 is fine instead of rint() since markersize is always positive
          const double markerhalf = markersize/2;

          switch (t)
            {
            case MarkerStyle::FILLED_BOX:
            case MarkerStyle::BOX:
              {
                p->newpath();
                p->moveto(pt.x-markerhalf, pt.y-markerhalf);
                p->lineto(pt.x+markerhalf, pt.y-markerhalf);
                p->lineto(pt.x+markerhalf, pt.y+markerhalf);
                p->lineto(pt.x-markerhalf, pt.y+markerhalf);
                p->closepath();
                if (t == MarkerStyle::FILLED_BOX)
                  p->fill();
                else
                  p->stroke();
              }
              break;

            case MarkerStyle::FILLED_CIRCLE:
            case MarkerStyle::CIRCLE:
              {
                // FIXME this is just a rectangle
                p->newpath();
#if 0
                p->moveto(pt.x-markerhalf, pt.y-markerhalf);
                p->lineto(pt.x+markerhalf, pt.y-markerhalf);
                p->lineto(pt.x+markerhalf, pt.y+markerhalf);
                p->lineto(pt.x-markerhalf, pt.y+markerhalf);
#endif
                p->circle(pt.x, pt.y, markerhalf);
                p->closepath();
                if (t == MarkerStyle::FILLED_CIRCLE)
                  p->fill();
                else
                  p->stroke();
              }
              break;

            case MarkerStyle::FILLED_DIAMOND:
            case MarkerStyle::DIAMOND:
              {
                p->newpath();
                p->moveto(pt.x-markerhalf, pt.y);
                p->lineto(pt.x, pt.y+markerhalf);
                p->lineto(pt.x+markerhalf, pt.y);
                p->lineto(pt.x, pt.y-markerhalf);
                p->closepath();
                if (t == MarkerStyle::FILLED_DIAMOND)
                  p->fill();
                else
                  p->stroke();
              }
              break;
                      
            case MarkerStyle::PLUS:
              p->newpath();
              p->moveto(pt.x-markerhalf, pt.y);
              p->lineto(pt.x+markerhalf, pt.y);
              p->stroke();
              p->newpath();
              p->moveto(pt.x, pt.y+markerhalf);
              p->lineto(pt.x, pt.y-markerhalf);
              p->stroke();
              break;
                      
            case MarkerStyle::TIMES:
              p->newpath();
              p->moveto(pt.x-markerhalf, pt.y+markerhalf);
              p->lineto(pt.x+markerhalf, pt.y-markerhalf);
              p->stroke();
              p->newpath();
              p->moveto(pt.x+markerhalf, pt.y+markerhalf);
              p->lineto(pt.x-markerhalf, pt.y-markerhalf);
              p->stroke();
              break;
                                          
            case MarkerStyle::HORIZONTAL_TICK:
              p->newpath();
              p->moveto(pt.x-markerhalf, pt.y);
              p->lineto(pt.x+markerhalf, pt.y);
              p->stroke();
              break;
                                      
            case MarkerStyle::VERTICAL_TICK: 
              p->newpath();
              p->moveto(pt.x, pt.y+markerhalf);
              p->lineto(pt.x, pt.y-markerhalf);
              p->stroke();
              break;

            default:
              g_warning("Style type not yet supported by printing code!");
              break;
            }
        }
      
      p->grestore();

      ++i;
    }      
}

static void
print_line_layer(Printer* p, XyPlotState* state,
                 XyLine* layer,
                 const Transform& xtrans, 
                 const Transform& ytrans,
                 const Transform& x_i2ps,
                 const Transform& y_i2ps)
{
  const guint N = layer->npoints();

  if (N < 2) 
    return;  
  
  const SortedPair& pair = layer->sorted();

  // we only call this to force an update in the sorted data.
  // basically a hack.
  PointTiles* tiles = pair.tiles();

  // shouldn't happen, but paranoia.
  if (tiles == 0)
    return;

  p->gsave();
  p->setcolor(Color(0,0,0));

  Affine::Point pt;
  guint i = 0;

  while (i < N)
    {
      pt.x = pair.sorted_key_value(i);
      pt.y = pair.sorted_other_value(i);

      // data to item
      
      pt.x = xtrans.transform(pt.x);
      pt.y = ytrans.transform(pt.y);

      // item to PS

      pt.x = x_i2ps.transform(pt.x);
      pt.y = y_i2ps.transform(pt.y);

      if (i == 0)
        {
          p->newpath();
          p->moveto(pt.x, pt.y);
        }
      else
        p->lineto(pt.x, pt.y);

      ++i;
    }

  p->stroke();
  p->grestore();
}

static void
print_xy_axis(Printer* p, XyPlotState* state,
              PlotUtil::PositionType pos,
              const Transform & x_i2ps,
              const Transform & y_i2ps)
{
  const Axis* axis = &state->axis(pos);
  const Rectangle* rect = &state->axis_rect(pos);
  const Transform* trans = &state->trans(pos);

  if (axis->hidden())
    return;

  double x0, y0, x1, y1;

  switch (axis->pos())
    {
    case PlotUtil::NORTH:
    case PlotUtil::SOUTH:
      x0 = trans->transform(axis->start());
      x1 = trans->transform(axis->stop());
      y0 = rect->y();
      y1 = rect->y();
      if (axis->pos() == PlotUtil::NORTH)
        {
          y0 += rect->height();
          y1 += rect->height();
        }
      break;
    case PlotUtil::WEST:
    case PlotUtil::EAST:
      y0 = trans->transform(axis->start());
      y1 = trans->transform(axis->stop());
      x0 = rect->x();
      x1 = rect->x();
      if (axis->pos() == PlotUtil::WEST)
        {
          x0 += rect->width();
          x1 += rect->width();
        }
      break;
    default:
      g_warning("Unplanned switch default");
      x0 = x1 = y0 = y1 = 0.0;
      break;
    }

  if ((fabs(x1 - x0) < PlotUtil::EPSILON) &&
      (fabs(y1 - y0) < PlotUtil::EPSILON)) 
    return; // too small

  x0 = x_i2ps.transform(x0);
  x1 = x_i2ps.transform(x1);
  y0 = y_i2ps.transform(y0);
  y1 = y_i2ps.transform(y1);

  if (axis->line())
    {
      p->setcolor(axis->line_color());
      p->newpath();
      p->moveto(x0,y0);
      p->lineto(x1,y1);
      p->stroke();
    }

  const vector<Tick> & ticks = axis->ticks();
  vector<Tick>::const_iterator ti = ticks.begin();
          
  guint tick_number = 0;

  while (ti != ticks.end())
    {
      p->gsave();

      Affine::Point t0;
      Affine::Point t1;          

      VFont* vfont = ti->font();
              
      if (vfont == 0)
        {
          Frontend* fe = guppi_frontend();
          if (fe != 0)
            {
              vfont = fe->default_font();
            }
        }


      const gchar* str = ti->label().c_str();
      
      const double str_w = vfont ? vfont->string_width(str) : 0;
      const double str_h = vfont ? vfont->font_height() : 0;

      // We want the bottom left corner of the text's
      // bounding box

      Affine::Point txt;

      double text_rotate = 0.0;

      switch (axis->pos())
        {
        case PlotUtil::NORTH:
        case PlotUtil::SOUTH:
          t0.x = trans->transform(ti->pos());
          t1.x = t0.x;
          txt.x = t0.x - str_w/2;
              
          switch (axis->pos())
            {
            case PlotUtil::NORTH:
              t0.y = rect->y() + rect->height();
              t1.y = t0.y - ti->len();
              txt.y = t1.y;
              break;

            case PlotUtil::SOUTH:
              t0.y = rect->y();
              t1.y = t0.y + ti->len();
              txt.y = t1.y + str_h;
              break;
                  
            default:
              break;
            }

          break;
              
        case PlotUtil::WEST:
        case PlotUtil::EAST:
          t0.y = trans->transform(ti->pos());
          t1.y = t0.y;
              
          txt.y = t0.y + str_h/2;

          switch (axis->pos())
            {
            case PlotUtil::WEST:
              t0.x = rect->x() + rect->width();
              t1.x = t0.x - ti->len();
              txt.x = t1.x - str_w;
              //              text_rotate = 90;
              break;

            case PlotUtil::EAST:
              t0.x = rect->x();
              t1.x = t0.x + ti->len();
              txt.x = t1.x;
              //              text_rotate = 270;
              break;
                  
            default:
              break;
            }
          break;
        default:
          g_warning("Unplanned switch default");
          break;
        }

      t0.x = x_i2ps.transform(t0.x);
      t1.x = x_i2ps.transform(t1.x);
      txt.x = x_i2ps.transform(txt.x);
      t0.y = y_i2ps.transform(t0.y);
      t1.y = y_i2ps.transform(t1.y);
      txt.y = y_i2ps.transform(txt.y);

      // Draw tick mark
      p->setcolor(ti->color());
      p->newpath();
      p->moveto(t0.x, t0.y);
      p->lineto(t1.x, t1.y);
      p->stroke();

      // Draw tick label

      if (vfont != 0)
        {
          p->setcolor(ti->label_color());
          p->setfont(vfont);
#if 0
          p->translate(txt.x, txt.y);

          if (text_rotate != 0.0)
            p->rotate(text_rotate);

          p->newpath();

          if (text_rotate != 0.0)
            p->moveto(-(str_w/2), -(str_h/2)); // center string on origin
          else
            p->moveto(0.0, 0.0);
#else
          p->newpath();
          p->moveto(txt.x, txt.y);
#endif

          g_debug("Label `%s' at %g, %g", str, txt.x, txt.y);

          p->show(str);
        }

      ++ti;
      ++tick_number;

      p->grestore();
    }
}

static void 
print_xy_internal(Printer* p, XyPlotState* state, 
                  const Transform & x_i2ps,
                  const Transform & y_i2ps)
{
  // Draw background rectangle

  p->gsave();

  p->setcolor(state->background());

  Affine::Point pts[4];

  pts[0].x = state->plot_rect().x();
  pts[0].y = state->plot_rect().y();

  pts[2].x = pts[0].x + state->plot_rect().width();
  pts[2].y = pts[0].y + state->plot_rect().height();

  pts[1].x = pts[2].x;
  pts[1].y = pts[0].y;

  pts[3].x = pts[0].x;
  pts[3].y = pts[2].y;

  int i = 0;
  while (i < 4)
    {
      pts[i].x = x_i2ps.transform(pts[i].x);
      pts[i].y = y_i2ps.transform(pts[i].y);

      ++i;
    }

  p->newpath();
  p->moveto(pts[0].x, pts[0].y);

  p->lineto(pts[1].x, pts[1].y);
  p->lineto(pts[2].x, pts[2].y);
  p->lineto(pts[3].x, pts[3].y);
  p->closepath();
  p->fill();

  p->grestore();

  // Draw points

  p->gsave();

  // Set clip to the plot rect

  p->newpath();
  p->moveto(pts[0].x, pts[0].y);

  p->lineto(pts[1].x, pts[1].y);
  p->lineto(pts[2].x, pts[2].y);
  p->lineto(pts[3].x, pts[3].y);
  p->closepath();
  p->clip();

  XyPlotState::iterator li = state->begin();
  while (li != state->end())
    {
      if (!(*li)->hidden())
        {
          if ((*li)->type_registry()->is_a((*li)->type(), XyLayer::ScatterLayer))
            {
              XyScatter* scatter = (XyScatter*)(*li)->cast_to_type(XyLayer::ScatterLayer);

              print_scatter_layer(p, state, scatter, 
                                  state->trans(scatter->x_axis()), 
                                  state->trans(scatter->y_axis()),
                                  x_i2ps, y_i2ps);
            }
          else if ((*li)->type_registry()->is_a((*li)->type(), XyLayer::LineLayer))
            {
              XyLine* line = (XyLine*)(*li)->cast_to_type(XyLayer::LineLayer);
              
              print_line_layer(p, state, line, 
                               state->trans(line->x_axis()), 
                               state->trans(line->y_axis()),
                               x_i2ps, y_i2ps);
            }
        }
          
      ++li;
    }

  p->grestore();

  i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      p->gsave();
      
      print_xy_axis(p, state, (PlotUtil::PositionType)i,
                    x_i2ps, y_i2ps);

      p->grestore();
      ++i;
    }
}
