/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */
/* Arc item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * Author: Havoc Pennington <hp@pobox.com>
 */

#if 0      /* FIXME once in libgnomeui, uncomment this */
#include <config.h>
#endif
#include <math.h>
#include "gnome-canvas-arc.h"

/* Change this to include local "gnome-canvas-util.h" once we go in gnome-libs */
#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>

enum {
	ARG_0,
        ARG_START,
        ARG_DISTANCE
};


static void gnome_canvas_arc_class_init (GnomeCanvasArcClass *class);
static void gnome_canvas_arc_init       (GnomeCanvasArc      *arc);
static void gnome_canvas_arc_destroy    (GtkObject          *object);
static void gnome_canvas_arc_set_arg    (GtkObject          *object,
                                         GtkArg             *arg,
                                         guint               arg_id);
static void gnome_canvas_arc_get_arg    (GtkObject          *object,
                                         GtkArg             *arg,
                                         guint               arg_id);

static void gnome_canvas_arc_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);

static void gnome_canvas_arc_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags);


static void   gnome_canvas_arc_draw   (GnomeCanvasItem *item, GdkDrawable *drawable, int x, int y,
                                       int width, int height);
static double gnome_canvas_arc_point  (GnomeCanvasItem *item, double x, double y, int cx, int cy,
                                       GnomeCanvasItem **actual_item);


static GnomeCanvasREClass *arc_parent_class;


GtkType
gnome_canvas_arc_get_type (void)
{
	static GtkType arc_type = 0;

	if (!arc_type) {
		GtkTypeInfo arc_info = {
			"GnomeCanvasArc",
			sizeof (GnomeCanvasArc),
			sizeof (GnomeCanvasArcClass),
			(GtkClassInitFunc) gnome_canvas_arc_class_init,
			(GtkObjectInitFunc) gnome_canvas_arc_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		arc_type = gtk_type_unique (gnome_canvas_re_get_type (), &arc_info);
	}

	return arc_type;
}

static void
gnome_canvas_arc_class_init (GnomeCanvasArcClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	arc_parent_class = gtk_type_class (gnome_canvas_item_get_type ());

	gtk_object_add_arg_type ("GnomeCanvasArc::start", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_START);
	gtk_object_add_arg_type ("GnomeCanvasArc::distance", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_DISTANCE);

	object_class->destroy = gnome_canvas_arc_destroy;
	object_class->set_arg = gnome_canvas_arc_set_arg;
	object_class->get_arg = gnome_canvas_arc_get_arg;

	item_class->render = gnome_canvas_arc_render;

	item_class->draw = gnome_canvas_arc_draw;
	item_class->point = gnome_canvas_arc_point;
	item_class->update = gnome_canvas_arc_update;
}

static void
gnome_canvas_arc_init (GnomeCanvasArc *arc)
{
        arc->angle1 = 0.0;
        arc->angle2 = 0.0;
}

static void
gnome_canvas_arc_destroy (GtkObject *object)
{
	GnomeCanvasArc *arc;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_IS_CANVAS_ARC (object));

	if (GTK_OBJECT_CLASS (arc_parent_class)->destroy)
		(* GTK_OBJECT_CLASS (arc_parent_class)->destroy) (object);
}

/* FIXME when we go into gnome-libs, this is duplicated in gnome-canvas-rect-ellipse.c */
static void get_bounds (GnomeCanvasRE *re, double *px1, double *py1, double *px2, double *py2)
{
	GnomeCanvasItem *item;
	double x1, y1, x2, y2;
	int cx1, cy1, cx2, cy2;
	double hwidth;

#ifdef VERBOSE
	g_print ("re get_bounds\n");
#endif
	item = GNOME_CANVAS_ITEM (re);

	if (re->width_pixels)
		hwidth = (re->width / item->canvas->pixels_per_unit) / 2.0;
	else
		hwidth = re->width / 2.0;

	x1 = re->x1;
	y1 = re->y1;
	x2 = re->x2;
	y2 = re->y2;

	gnome_canvas_item_i2w (item, &x1, &y1);
	gnome_canvas_item_i2w (item, &x2, &y2);
	gnome_canvas_w2c (item->canvas, x1 - hwidth, y1 - hwidth, &cx1, &cy1);
	gnome_canvas_w2c (item->canvas, x2 + hwidth, y2 + hwidth, &cx2, &cy2);
	*px1 = cx1;
	*py1 = cy1;
	*px2 = cx2;
	*py2 = cy2;

	/* Some safety fudging */

	*px1 -= 2;
	*py1 -= 2;
	*px2 += 2;
	*py2 += 2;
}

static void
gnome_canvas_arc_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	GnomeCanvasItem *item;
	GnomeCanvasArc *arc;
	GdkColor color;
	GdkColor *colorp;

	item = GNOME_CANVAS_ITEM (object);
	arc = GNOME_CANVAS_ARC (object);

	switch (arg_id) {
	case ARG_START:
		arc->angle1 = GTK_VALUE_DOUBLE (*arg);

		gnome_canvas_item_request_update (item);
		break;

	case ARG_DISTANCE:
		arc->angle2 = GTK_VALUE_DOUBLE (*arg);

		gnome_canvas_item_request_update (item);
		break;

	default:
		break;
	}
}

static void
gnome_canvas_arc_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	GnomeCanvasArc *arc;

	arc = GNOME_CANVAS_ARC (object);

	switch (arg_id) {
	case ARG_START:
		GTK_VALUE_DOUBLE (*arg) = arc->angle1;
		break;

	case ARG_DISTANCE:
		GTK_VALUE_DOUBLE (*arg) = arc->angle2;
		break;
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
}


/* FIXME duplicated in gnome-canvas-rect-ellipse.c */
/* Convenience function to set a GC's foreground color to the specified pixel value */
static void
set_gc_foreground (GdkGC *gc, gulong pixel)
{
	GdkColor c;

	if (!gc)
		return;

	c.pixel = pixel;
	gdk_gc_set_foreground (gc, &c);
}

/* FIXME duplicated in gnome-canvas-rect-ellipse.c */
/* Sets the stipple pattern for the specified gc */
static void
set_stipple (GdkGC *gc, GdkBitmap **internal_stipple, GdkBitmap *stipple, int reconfigure)
{
	if (*internal_stipple && !reconfigure)
		gdk_bitmap_unref (*internal_stipple);

	*internal_stipple = stipple;
	if (stipple && !reconfigure)
		gdk_bitmap_ref (stipple);

	if (gc) {
		if (stipple) {
			gdk_gc_set_stipple (gc, stipple);
			gdk_gc_set_fill (gc, GDK_STIPPLED);
		} else
			gdk_gc_set_fill (gc, GDK_SOLID);
	}
}

/* FIXME duplicated in gnome-canvas-rect-ellipse.c */
/* Recalculate the outline width of the rectangle/ellipse and set it in its GC */
static void
set_outline_gc_width (GnomeCanvasRE *re)
{
	int width;

	if (!re->outline_gc)
		return;

	if (re->width_pixels)
		width = (int) re->width;
	else
		width = (int) (re->width * re->item.canvas->pixels_per_unit + 0.5);

	gdk_gc_set_line_attributes (re->outline_gc, width,
				    GDK_LINE_SOLID, GDK_CAP_PROJECTING, GDK_JOIN_MITER);
}



/* FIXME duplicated in gnome-canvas-rect-ellipse.c */
static void
gnome_canvas_arc_update_shared (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags)
{
	GnomeCanvasArc *arc;
        GnomeCanvasRE *re;

	arc = GNOME_CANVAS_ARC (item);
	re = GNOME_CANVAS_RE (item);

	if (GNOME_CANVAS_ITEM_CLASS(arc_parent_class)->update)
		(* GNOME_CANVAS_ITEM_CLASS(arc_parent_class)->update) (item, affine, clip_path, flags);

	if (!item->canvas->aa) {
		set_gc_foreground (re->fill_gc, re->fill_pixel);
		set_gc_foreground (re->outline_gc, re->outline_pixel);
		set_stipple (re->fill_gc, &re->fill_stipple, re->fill_stipple, TRUE);
		set_stipple (re->outline_gc, &re->outline_stipple, re->outline_stipple, TRUE);
		set_outline_gc_width (re);
	} 
}

static void
gnome_canvas_arc_render (GnomeCanvasItem *item,
                         GnomeCanvasBuf *buf)
{
	GnomeCanvasArc *arc;
        GnomeCanvasRE *re;
	guint32 fg_color, bg_color;

	re = GNOME_CANVAS_RE (item);
        arc = GNOME_CANVAS_ARC (item);

#ifdef VERBOSE
	g_print ("gnome_canvas_arc_render (%d, %d) - (%d, %d) fill=%08x outline=%08x\n",
		 buf->rect.x0, buf->rect.y0, buf->rect.x1, buf->rect.y1, arc->fill_color, arc->outline_color);
#endif

	if (re->fill_svp != NULL) {
		gnome_canvas_render_svp (buf, re->fill_svp, re->fill_color);
	}

	if (re->outline_svp != NULL) {
		gnome_canvas_render_svp (buf, re->outline_svp, re->outline_color);
	}
}

static void
gnome_canvas_arc_draw (GnomeCanvasItem *item, GdkDrawable *drawable, int x, int y, int width, int height)
{
	GnomeCanvasArc *arc;
	GnomeCanvasRE *re;
	double i2w[6], w2c[6], i2c[6];
	int x1, y1, x2, y2;
	ArtPoint i1, i2;
	ArtPoint c1, c2;

	re = GNOME_CANVAS_RE (item);
        arc = GNOME_CANVAS_ARC (item);

	/* Get canvas pixel coordinates */

	gnome_canvas_item_i2w_affine (item, i2w);
	gnome_canvas_w2c_affine (item->canvas, w2c);
	art_affine_multiply (i2c, i2w, w2c);

	i1.x = re->x1;
	i1.y = re->y1;
	i2.x = re->x2;
	i2.y = re->y2;
	art_affine_point (&c1, &i1, i2c);
	art_affine_point (&c2, &i2, i2c);
	x1 = c1.x;
	y1 = c1.y;
	x2 = c2.x;
	y2 = c2.y;
	
	if (re->fill_set) {
		if (re->fill_stipple)
			gnome_canvas_set_stipple_origin (item->canvas, re->fill_gc);
		
		gdk_draw_arc (drawable,
			      re->fill_gc,
			      TRUE,
			      x1 - x,
			      y1 - y,
			      x2 - x1,
			      y2 - y1,
			      rint(arc->angle1*64.0),
			      rint(arc->angle2*64.0));
	}

	if (re->outline_set) {
		if (re->outline_stipple)
			gnome_canvas_set_stipple_origin (item->canvas, re->outline_gc);

		gdk_draw_arc (drawable,
			      re->outline_gc,
			      FALSE,
			      x1 - x,
			      y1 - y,
			      x2 - x1,
			      y2 - y1,
			      rint(arc->angle1*64.0),
			      rint(arc->angle2*64.0));
	}
}

/* FIXME this still assumes an ellipse... */
static double
gnome_canvas_arc_point (GnomeCanvasItem *item, double x, double y, int cx, int cy, GnomeCanvasItem **actual_item)
{
	GnomeCanvasArc *arc;
        GnomeCanvasRE *re;
	double dx, dy;
	double scaled_dist;
	double outline_dist;
	double center_dist;
	double width;
	double a, b;
	double diamx, diamy;

	re = GNOME_CANVAS_RE (item);
        arc = GNOME_CANVAS_ARC (item);
        
	*actual_item = item;

	if (re->outline_set) {
		if (re->width_pixels)
			width = re->width / item->canvas->pixels_per_unit;
		else
			width = re->width;
	} else
		width = 0.0;

	/* Compute the distance between the center of the ellipse and
	 * the point, with the ellipse considered as being scaled to a
	 * circle.  */

	dx = x - (re->x1 + re->x2) / 2.0;
	dy = y - (re->y1 + re->y2) / 2.0;
	center_dist = sqrt (dx * dx + dy * dy);

	a = dx / ((re->x2 + width - re->x1) / 2.0);
	b = dy / ((re->y2 + width - re->y1) / 2.0);
	scaled_dist = sqrt (a * a + b * b);

	/* If the scaled distance is greater than 1, then we are outside.  Compute the distance from
	 * the point to the edge of the circle, then scale back to the original un-scaled coordinate
	 * system.
	 */

	if (scaled_dist > 1.0)
		return (center_dist / scaled_dist) * (scaled_dist - 1.0);

	/* We are inside the outer edge of the arc.  If it is filled, then we are "inside".
	 * Otherwise, do the same computation as above, but also check whether we are inside the
	 * outline.
	 */

	if (re->fill_set)
		return 0.0;

	if (scaled_dist > GNOME_CANVAS_EPSILON)
		outline_dist = (center_dist / scaled_dist) * (1.0 - scaled_dist) - width;
	else {
		/* Handle very small distance */

		diamx = re->x2 - re->x1;
		diamy = re->y2 - re->y1;

		if (diamx < diamy)
			outline_dist = (diamx - width) / 2.0;
		else
			outline_dist = (diamy - width) / 2.0;
	}

	if (outline_dist < 0.0)
		return 0.0;

	return outline_dist;
}

#define N_PTS 126

static void
gnome_canvas_gen_arc (ArtVpath *vpath, 
                      double x0, double y0,
                      double x1, double y1,
                      double start_angle,  /* angle1 */
                      double distance)     /* angle2 */
{

	int i = 0;

        double radians_start = start_angle * (M_PI/180.0);
        double radians_end = (start_angle + distance) * (M_PI/180.0);
        double radians_distance = 0.0; 
        double centerx = x0 + (x1 - x0)/2.0;
        double centery = y0 + (y1 - y0)/2.0;
        
        g_return_if_fail(distance <= 360.0);

        radians_distance = radians_end - radians_start;

        /* Line from center to start of curve */
        vpath[0].code = ART_MOVETO;
        vpath[0].x = centerx;
        vpath[0].y = centery;

        i = 0;
	while (i < N_PTS) {
		double th;

		th = radians_start + (radians_distance * i) / (N_PTS-1);

                /* + 1 to skip the line from the center */
                vpath[i+1].code = ART_LINETO;
                vpath[i+1].x = (x0 + x1) * 0.5 + (x1 - x0) * 0.5 * cos (th);
                vpath[i+1].y = (y0 + y1) * 0.5 - (y1 - y0) * 0.5 * sin (th);

                ++i;
	}
        
        /* Line from end of curve to center */
        vpath[i+1].code = ART_LINETO;
        vpath[i+1].x = centerx;
        vpath[i+1].y = centery;
}

static void
gnome_canvas_arc_update (GnomeCanvasItem *item, double affine[6], ArtSVP *clip_path, gint flags)
{
	GnomeCanvasArc *arc;
        GnomeCanvasRE *re;
	ArtVpath vpath[N_PTS + N_PTS + 5];
	ArtVpath *vpath2;
	double x0, y0, x1, y1;
	int i;

#ifdef VERBOSE
	g_print ("gnome_canvas_rect_update item %x\n", item);
#endif

	gnome_canvas_arc_update_shared (item, affine, clip_path, flags);
	re = GNOME_CANVAS_RE (item);
        arc = GNOME_CANVAS_ARC (item);

	if (item->canvas->aa) {

#ifdef VERBOSE
		{
			char str[128];

			art_affine_to_string (str, affine);
			g_print ("g_c_r_e affine = %s\n", str);
		}
		g_print ("item %x (%g, %g) - (%g, %g)\n",
			 item,
			 re->x1, re->y1, re->x2, re->y2);
#endif

		if (re->fill_set) {
			gnome_canvas_gen_arc (vpath, re->x1, re->y1, re->x2, re->y2, 
                                              arc->angle1, arc->angle2);
			vpath[N_PTS + 2].code = ART_END;
			vpath[N_PTS + 2].x = 0;
			vpath[N_PTS + 2].y = 0;

			vpath2 = art_vpath_affine_transform (vpath, affine);

			gnome_canvas_item_update_svp_clip (item, &re->fill_svp, art_svp_from_vpath (vpath2), clip_path);
			art_free (vpath2);
		} else
			gnome_canvas_item_update_svp (item, &re->fill_svp, NULL);

		if (re->outline_set) {
			double halfwidth;

			if (re->width_pixels)
				halfwidth = (re->width / item->canvas->pixels_per_unit) * 0.5;
			else
				halfwidth = re->width * 0.5;

			if (halfwidth < 0.25)
				halfwidth = 0.25;

			i = 0;
			gnome_canvas_gen_arc (vpath + i,
                                              re->x1 - halfwidth, re->y1 - halfwidth,
                                              re->x2 + halfwidth, re->y2 + halfwidth,
                                              arc->angle1, arc->angle2);
			i = N_PTS + 2;
			if (re->x2 - halfwidth > re->x1 + halfwidth &&
			    re->y2 - halfwidth > re->y1 + halfwidth) {
				gnome_canvas_gen_arc (vpath + i,
                                                      re->x1 + halfwidth, re->y1 - halfwidth,
                                                      re->x2 - halfwidth, re->y2 + halfwidth,
                                                      arc->angle1, arc->angle2);
				i += N_PTS + 1;
			}
			vpath[i+1].code = ART_END;
			vpath[i+1].x = 0;
			vpath[i+1].y = 0;

			vpath2 = art_vpath_affine_transform (vpath, affine);

			gnome_canvas_item_update_svp_clip (item, &re->outline_svp, art_svp_from_vpath (vpath2), clip_path);
			art_free (vpath2);
		} else
			gnome_canvas_item_update_svp (item, &re->outline_svp, NULL);
	} else {
		/* xlib rendering - just update the bbox */
		get_bounds (re, &x0, &y0, &x1, &y1);
		gnome_canvas_update_bbox (item, x0, y0, x1, y1);
	}

}


