 // -*- C++ -*-

/* 
 * toolbox.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "toolbox.h"
#include "tool.h"
#include "plotstore.h"
  
ToolBox::ToolBox(const string& plotid)
  : plotid_(plotid), window_(0)
{

}

ToolBox::~ToolBox()
{
  if (window_ != 0)
    {
      gtk_widget_destroy(window_);
      window_ = 0;
    }

  map<GtkWidget*,Tool*>::iterator i = tools_.begin();
  while (i != tools_.end())
    {
      if (i->second->unref() == 0)
        delete i->second;
      
      ++i;
    }
}

void 
ToolBox::show()
{
  if (window_ == 0)
    {
      window_ = gtk_window_new(GTK_WINDOW_TOPLEVEL);

      gtk_window_set_title(GTK_WINDOW(window_), _("Guppi - Tool Box"));

      gtk_signal_connect(GTK_OBJECT(window_),
                         "delete_event",
                         GTK_SIGNAL_FUNC(delete_event_cb),
                         this);

      GtkWidget* vbox = gtk_vbox_new(TRUE,0);

      gtk_container_add(GTK_CONTAINER(window_), vbox);

      vector<Tool*> tools = guppi_plot_store()->tools(plotid_);

      if (tools.empty())
        {
          GtkWidget* label = gtk_label_new(_("There are no tools for this plot type"));
          gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
        }
      else 
        {
          vector<Tool*>::iterator i = tools.begin();
          while (i != tools.end())
            {
              GtkWidget* button = gtk_button_new_with_label((*i)->name().c_str());
              
              guppi_set_tooltip(button, (*i)->tooltip());
              
              gtk_box_pack_start(GTK_BOX(vbox), button, TRUE, TRUE, 0);
              
              gtk_signal_connect(GTK_OBJECT(button), "clicked",
                                 GTK_SIGNAL_FUNC(clicked_cb), this);
              
              tools_[button] = *i;
              
              (*i)->ref();
              
              ++i;
            }
        }

      gtk_widget_show_all(window_);
    }
  else 
    {
      guppi_raise_and_show(GTK_WINDOW(window_));
    }
}

void 
ToolBox::hide()
{
  if (window_ != 0)
    {
      gtk_widget_hide(window_);
    }
}

gint 
ToolBox::delete_event_cb(GtkWidget* window, gpointer data)
{
  g_return_val_if_fail(GTK_IS_WINDOW(window), TRUE);
  gtk_widget_hide(window);
  return TRUE; // don't destroy
}

void 
ToolBox::clicked_cb(GtkWidget* button, gpointer data)
{
  ToolBox* tb = static_cast<ToolBox*>(data);

  tb->clicked(button);
}
  
void 
ToolBox::clicked(GtkWidget* w)
{
  map<GtkWidget*,Tool*>::iterator i = tools_.find(w);

  g_return_if_fail(i != tools_.end());

  g_debug("Setting current tool to %s", i->second->name().c_str());

  guppi_plot_store()->set_current_tool(plotid_, i->second);
}
