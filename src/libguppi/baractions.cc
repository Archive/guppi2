// -*- C++ -*-

/* 
 * baractions.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "baractions.h"
#include "barplot.h"

/////////////////


//////////////////////////

#include "menuentry.h"

///


///

#include "printer-gnome.h"
#include "state-print.h"

class BarPrint : public MenuEntry {
public:
  BarPrint();
  
  void invoke(Plot* p);
};

BarPrint::BarPrint() 
  : MenuEntry("guppi-bar-print", 
              _("Print"),
              _("Print the plot (doesn't work yet, just writes junk to test.ps)"))
{

}

void
BarPrint::invoke(Plot* p)
{
  g_return_if_fail(p->check(BarPlot::ID));

  BarPlot* bp = static_cast<BarPlot*>(p);
  
  Printer* pr = new GPrinter;

  guppi_print_bar(pr, bp->state());

  if (!pr->sync())
    gnome_error_dialog(_("Printing didn't work, but it doesn't work anyway."));
  else
    gnome_ok_dialog(_("Printing (which is not really implemented) wrote some stuff to test.ps"));

  delete pr;
}

///////////////////////////

#include "plotstore.h"

void 
guppi_gnome_init_bar()
{
  PlotStore* ps = guppi_plot_store();

  ps->add_plot(BarPlot::ID);

  ps->add_menuentry(BarPlot::ID, new BarPrint);
}



