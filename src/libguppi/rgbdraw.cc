// -*- C++ -*-

/* 
 * rgbdraw.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "rgbdraw.h"

#include <src/aux/mi.h>

RGB::Context::Context(RGB& rgb)
  : rgb_(rgb), clipx_(0), clipy_(0), 
    clipw_(rgb_.width()), cliph_(rgb_.height()),
    filled_(false), line_width_(0),
    hidden_(0)
{
  hidden_ = new miGC;

  miGC* gc = static_cast<miGC*>(hidden_);

  miInitGC(gc);

  sync_internal_clip();
}

RGB::Context::~Context()
{
  miGC* gc = static_cast<miGC*>(hidden_);
  if (gc->drawable != 0)
    {
      delete gc->drawable;
    }
  delete gc;
  hidden_ = 0;
}

void 
RGB::Context::set_filled(bool setting)
{
  filled_ = setting;
}

void 
RGB::Context::set_color(const Color & c)
{
  color_ = c;

  miGC* gc = static_cast<miGC*>(hidden_);

  gc->fgPixel.r = color_.r();
  gc->fgPixel.g = color_.g();
  gc->fgPixel.b = color_.b();
  gc->fgPixel.a = color_.a();
}

void 
RGB::Context::set_line_width(guint width)
{
  line_width_ = width;

  miGC* gc = static_cast<miGC*>(hidden_);

  gc->lineWidth = line_width_;
}

void 
RGB::Context::sync_internal_clip()
{
  // Could do some caching with this clip stuff, to speed us up.

  miGC* gc = static_cast<miGC*>(hidden_);

  if (gc->drawable != 0)
    {
      delete gc->drawable;

      gc->drawable = 0;
    }

  if (clipw_ != 0 && cliph_ != 0)
    {
      gc->drawable = new unsigned char*[cliph_]; // array of pixel rows
      gc->width = clipw_;
      gc->height = cliph_;
      // Get a pointer to the start of the clip region.
      unsigned char* pixbuf = rgb_.buf() + rgb_.rowstride() * clipy_ + clipx_*3;
      // Create a bitmap of the relevant region.
      miBitmapizePixbuf(pixbuf, 
                        gc->drawable, rgb_.rowstride(), cliph_);
    }

  // gc->drawable should be NULL only if we have no area.
  g_assert((clip_area() == 0 && gc->drawable == 0) || 
           (clip_area() != 0 && gc->drawable != 0));
  
}

void 
RGB::Context::clip(guint x, guint y, guint w, guint h)
{
  if (x > rgb_.width() || y > rgb_.height())
    {
      clipx_ = clipy_ = clipw_ = cliph_ = 0;
    }
  else 
    {
      clipx_ = x;
      clipy_ = y;
      clipw_ = w;
      cliph_ = h;
      
      if (clipw_ > (rgb_.width() - x))
        {
          clipw_ = rgb_.width() - x;
        }
      
      if (cliph_ > rgb_.height() - y)
        {
          cliph_ = rgb_.height() - y;
        }
    }

  sync_internal_clip();
}

void 
RGB::Context::unclip() 
{
  clipx_ = 0;
  clipy_ = 0;
  clipw_ = rgb_.width();
  cliph_ = rgb_.height();

  sync_internal_clip();
}

void 
RGB::paint_convex_poly(guint npoints, const Point points[], const Context& ctx)
{
  if (ctx.clip_area() == 0)
    return;

  const miGC* gc = static_cast<miGC*>(ctx.hidden());

  g_assert(gc->drawable != 0); // because we have some clip area

  miIntPoint* pts = new miIntPoint[npoints];

  guint i = 0;
  while (i < npoints)
    {
      pts[i].x = points[i].x;
      pts[i].y = points[i].y;

      ctx.clip_relative(&pts[i].x, &pts[i].y);

      ++i;
    }

  if (ctx.filled())
    {
      miFillPolygon(gc, miConvex, miCoordModeOrigin, npoints, pts);  
    }
  else
    {
      if (gc->lineWidth == 0)
        miZeroLine(gc, miCoordModeOrigin, npoints, pts);
      else
        miWideLine(gc, miCoordModeOrigin, npoints, pts);
    }
  
  delete [] pts;
}

void 
RGB::paint_line(gint x1, gint y1, 
                gint x2, gint y2,
                const Context & ctx)
{
  if (ctx.clip_area() == 0)
    return;
  
  const miGC* gc = static_cast<miGC*>(ctx.hidden());

  g_assert(gc->drawable != 0); // because we have some clip area
  
  miIntPoint pts[2] = { { x1, y1 }, { x2, y2 } };

  ctx.clip_relative(&pts[0].x, &pts[0].y);
  ctx.clip_relative(&pts[1].x, &pts[1].y);
  
  if (gc->lineWidth == 0)
    miZeroLine(gc, miCoordModeOrigin, 2, pts);
  else
    miWideLine(gc, miCoordModeOrigin, 2, pts);
}

void 
RGB::paint_line(guint npoints, 
                const Point points[],
                const Context & ctx)
{
  g_return_if_fail(npoints > 0);

  if (ctx.clip_area() == 0)
    return;
  
  if (npoints == 0)
    return;

  const miGC* gc = static_cast<miGC*>(ctx.hidden());

  g_assert(gc->drawable != 0); // because we have some clip area

  // This copy is really pretty gratuitous. FIXME

  miIntPoint pts[npoints];

  guint i = 0;
  while (i < npoints)
    {
      pts[i].x = points[i].x;
      pts[i].y = points[i].y;
      ctx.clip_relative(&pts[i].x, &pts[i].y);
      ++i;
    }
  
  if (gc->lineWidth == 0)
    miZeroLine(gc, miCoordModeOrigin, npoints, pts);
  else
    miWideLine(gc, miCoordModeOrigin, npoints, pts);
}

void 
RGB::paint_segments(guint npoints,
                    const Point points[],
                    const Context& ctx)
{
  g_return_if_fail((npoints % 2) == 0);
  g_return_if_fail(npoints > 0);

  if (ctx.clip_area() == 0)
    return;
  
  if (npoints == 0)
    return;

  guint i = 0;
  while (i < npoints)
    {
      paint_line(points[i].x, points[i].y,
                 points[i+1].x, points[i+1].y,
                 ctx);
      i += 2;
    }
}

void 
RGB::paint_rect(gint x1, gint y1, 
                gint x2, gint y2,
                const Context & ctx)
{
  g_return_if_fail(x2 >= x1);
  g_return_if_fail(y2 >= y1);

  if (ctx.clip_area() == 0)
    return;

  const miGC* gc = static_cast<miGC*>(ctx.hidden());

  g_assert(gc->drawable != 0); // because we have some clip area

  ctx.clip_relative(&x1, &y1);
  ctx.clip_relative(&x2, &y2);

  miIntPoint pts[5] = { { x1, y1 }, { x1, y2 }, { x2, y2 }, { x2, y1 }, { x1, y1 } };

  if (ctx.filled())
    {
      miFillPolygon(gc, miConvex, miCoordModeOrigin, 5, pts);  
    }
  else
    {
      if (gc->lineWidth == 0)
        miZeroLine(gc, miCoordModeOrigin, 5, pts);
      else
        miWideLine(gc, miCoordModeOrigin, 5, pts);
    }
}

void RGB::paint_arc(gint x, gint y, gint width, gint height, 
                    gint angle1, gint angle2, const Context& ctx)
{
  if (ctx.clip_area() == 0)
    return;
  
  const miGC* gc = static_cast<miGC*>(ctx.hidden());

  g_assert(gc->drawable != 0); // because we have some clip area
  
  ctx.clip_relative(&x, &y);

  miArc arc = { x, y, width, height, angle1, angle2 };

  if (ctx.filled())
    {
      miPolyFillArc(gc, 1, &arc);
    }
  else
    {
      if (gc->lineWidth == 0)
        miZeroPolyArc(gc, 1, &arc);
      else 
        miPolyArc(gc, 1, &arc);
    }
}



