// -*- C++ -*-

/* 
 * pieactions.cc
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "pieactions.h"
#include "pieplot.h"

#include "menuentry.h"

#include "printer-gnome.h"
#include "state-print.h"

class PiePrint : public MenuEntry {
	public:

	PiePrint();
  
	void invoke(Plot * p);
};

PiePrint::PiePrint() : MenuEntry("guppi-pie-print", _("Print"),
	_("Print the plot (doesn't work yet, just writes junk to test.ps)")) {
}

void PiePrint::invoke(Plot * p) {
	g_return_if_fail(p->check(PiePlot::ID));

	PiePlot* bp = static_cast<PiePlot*>(p);
  
	Printer* pr = new GPrinter;

	guppi_print_pie(pr, bp->state());

	if (!pr->sync())
		gnome_error_dialog(_("Printing didn't work, but it doesn't work anyway."));
	else
		gnome_ok_dialog(_("Printing (which is not really implemented) wrote some stuff to test.ps"));

	delete pr;
}

///////////////////////////

#include "plotstore.h"

void guppi_gnome_init_pie()
{
	PlotStore* ps = guppi_plot_store();

	ps->add_plot(PiePlot::ID);

	ps->add_menuentry(PiePlot::ID, new PiePrint);
}
