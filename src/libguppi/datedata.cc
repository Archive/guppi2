// -*- C++ -*-

/* 
 * datedata.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "datedata.h"
// Goose 0.0.9 and earlier doesn't include the right headers in DateSet.h
#include <goose/DateSet.h>

static const GDate INVALID_DATE = {
  G_DATE_BAD_JULIAN,
  FALSE,
  FALSE,
  G_DATE_BAD_DAY,
  G_DATE_BAD_MONTH,
  G_DATE_BAD_YEAR
};

DateData::DateData()
  : Data(Date), set_(0), checked_out_(false)
{
  set_ = new DateSet;
}

DateData::DateData(DateSet* ds)
  : Data(Date), set_(ds), checked_out_(false)
{

}

DateData::~DateData()
{
  if (set_ != 0)
    delete set_;
}


gsize 
DateData::size() const
{
  return set_->size();
}

const string& 
DateData::name() const
{
  return const_cast<const DateSet*>(set_)->label();
}

void 
DateData::set_name(const string & name)
{
  set_->set_label(name);
  data_model.name_changed(this, name);
}


void 
DateData::set_date(guint index, GDate gdate)
{
  g_return_if_fail(index < set_->size());

  set_->set(index, gdate);

  vector<guint> which;
  which.push_back(index);
  data_model.values_changed(this, which);
}

GDate 
DateData::get_date(guint index) const
{
  g_return_val_if_fail(index < set_->size(), INVALID_DATE);

  return set_->data(index);
}

void 
DateData::set_date_julian(guint index, guint date)
{
  GDate gdate;

  g_date_clear(&gdate, 1);
  g_date_set_julian(&gdate, date);

  set_date(index, gdate);
}

guint 
DateData::get_date_julian(guint index) const
{
  GDate date = get_date(index);
  return g_date_julian(&date);
}

guint 
DateData::min_julian() const
{
  // FIXME broken; needs Goose changes
  if (size() > 0)
    return get_date_julian(0);
  else
    return 0;
}

guint 
DateData::max_julian() const
{
  // FIXME broken; needs Goose changes
  if (size() > 0)
    return get_date_julian(size()-1);
  else
    return 0;
}

string 
DateData::get_string(guint index) const
{
  return set_->as_string(index);
}

void 
DateData::add(const GDate& gdate)
{
  set_->add(gdate);

  vector<guint> which;
  which.push_back(set_->size() - 1);
  data_model.values_changed(this, which);
}

xmlNodePtr 
DateData::xml(xmlNodePtr parent) const
{
  GUPPI_NOT_IMPLEMENTED;
  return 0;
}
  
void 
DateData::set_xml(xmlNodePtr node)
{
  GUPPI_NOT_IMPLEMENTED;
}

DateSet* 
DateData::checkout_dateset()
{
  if (checked_out_) 
    return 0;
  else 
    {
      checked_out_ = true;
      return set_;
    }
}

void 
DateData::checkin_dateset(DateSet* set, 
                          bool values_changed, bool other_changed)
{
  g_return_if_fail(checked_out_);
  g_return_if_fail(set == set_);

  if (values_changed)
    {
      data_model.values_changed(this);
    }

  if (other_changed)
    {
      data_model.name_changed(this, set_->label());
    }
  
  checked_out_ = false;
}

gsize 
DateData::byte_size()
{
  return sizeof(DateData) + sizeof(DateSet);
}
