// -*- C++ -*-

/* 
 * pointtiles.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_POINTTILES_H
#define GUPPI_POINTTILES_H

#include "util.h"

#include <vector>
//#include <list>

// This class sorts X and Y ordered pairs into "tiles" corresponding 
// to regions of a plane. The region represented by a tile is 
// chosen so that each tile holds approximately the same number of 
// points.
// Tiles store the real (unsorted) array indices of the ordered pair.
// Tiles are not uniform in size.
// Tiles do not necessarily meet at the edges, if there are no 
// points to be covered. In fact they probably don't meet at the 
// edges. However, they may sometimes overlap infinitesimally (if there are 
// identical values in the data being tiled)

class PointTiles {
public:
  PointTiles();
  ~PointTiles();

  // Set a target number of points per tile;
  //  then we do our best. Must call before set_points()
  void set_points_per_tile(guint ppt);

  // Delete current tiles and generate tiles for these points.
  // xvalues must be sorted in ascending order!
  // xmin/xmax ymin/ymax are the (inclusive) bounds of the region
  //  to be tiled. xvalues or yvalues outside the region
  //  are discarded. (they are basically a clip region)
  // converter is an array which stores the real index of each 
  //  sorted index (look at SortedPair in scalardata.h/scalardata.cc)
  void set_points(const double* xvalues, const double* yvalues, 
                  const guint* converter, guint N,
                  double xmin, double ymin,  double xmax, double ymax);

  struct Tile {
    // [x1, x2]  [y1, y2]   x1 <= x2   y1 <= y2
    double x1;
    double y1;
    double x2;
    double y2;
    
    // Indices are indices into the real array, not the sorted array passed
    // to set_points() (they used to be sorted indices, so if you find code
    //  that gets it wrong please fix)
    vector<guint> contents;

  };

  // Can return an empty vector if no tiles
  // "edgefuzz" is 1/2 the "width" of the edge. Anything 
  // overlapping edge +/- fuzz it is considered to be on the edge.
  void tiles_in_rect(vector<const Tile*> & edgetiles, // on the edge 
                     vector<const Tile*> & intiles,   // completely contained
                     double x1, double y1, double x2, double y2,
                     double edgefuzz = 0) const;
  // Can return 0 if no tile at the point
  const Tile* tile_at_point(double x, double y) const;

  // Move this changed value to the correct tile
  void update(guint index, double oldx, double oldy, double newx, double newy);

private:
  class Band : public vector<Tile*> 
  {
  public:
    double x1, x2;
  };

  vector<Band*> tiles_;

  guint ppt_;

  // already-allocated tiles, for speed
  vector<Tile*> tilepool_;
  // mem pools to destroy in destructor
  vector<Tile*> tilechunks_;

  // Can return 0 if no tile
  Tile* tile_at_point(double x, double y);

  Tile* new_tile();
  void  free_tile(Tile* t);

  void check_invariants();

  // x/y are potentially ambiguous if they are right on a boundary.
  // the index is used to get the *exact* tile.
  Tile* tile_at_point_and_index(double x, double y, guint index);
  Tile* tile_at_point_and_index_in_band(double x, double y, guint index, Band* band);
  bool tile_contains_index(Tile* t, guint index);
};

#endif
