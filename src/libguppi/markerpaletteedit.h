// -*- C++ -*-

/* 
 * markerpaletteedit.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_MARKERPALETTEEDIT_H
#define GUPPI_MARKERPALETTEEDIT_H

#include "markerstyle.h"
#include "gnome-util.h"

class MarkerStyleEdit;

class MarkerPaletteEdit : public MarkerPalette::View
{
public:
  MarkerPaletteEdit();
  virtual ~MarkerPaletteEdit();

  // MarkerPalette::View
  virtual void add_entry(MarkerPalette* palette, guint32 entryid);
  virtual void remove_entry(MarkerPalette* palette, guint32 entryid);
  virtual void change_entry(MarkerPalette* palette, guint32 entryid);
  virtual void change_current(MarkerPalette* palette, guint32 old, guint32 newcurrent);

  // ::View
  virtual void destroy_model();

  void set_palette(MarkerPalette* palette);

  void edit();

private:
  GtkWidget* dialog_;

  MarkerPalette* palette_;

  // List of styles that can be edited
  GtkWidget* clist_; 

  MarkerStyleEdit* mse_;

  void grab_palette(MarkerPalette* palette);
  void release_palette();

  gint append_entry(guint32 entryid);
  void remove_entry(guint32 entryid);
  void change_entry(guint32 entryid);
  void set_current_entry(guint32 entryid);

  static gint close_cb(GtkWidget* w, gpointer data);
  gint close();

  static void select_row_cb(GtkWidget* w, gint row, gint col, GdkEvent* e, gpointer data);
  static void unselect_row_cb(GtkWidget* w, gint row, gint col, GdkEvent* e, gpointer data);

  void select_row(gint row);
  void unselect_row(gint row);

  static void add_button_clicked_cb(GtkWidget* w, gpointer data);
  static void remove_button_clicked_cb(GtkWidget* w, gpointer data);

  void add();
  void remove();
};

MarkerPaletteEdit* guppi_marker_palette_edit();

#endif
