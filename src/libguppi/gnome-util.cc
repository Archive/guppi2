// -*- C++ -*-

/* 
 * gnome-util.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gnome-util.h"

static void
spew_cb(GtkObject* obj, gpointer data)
{
  g_debug("Destroying %s %p", gtk_type_name(obj->klass->type), obj);
}

void 
guppi_spew_on_destroy (GtkObject* obj)
{
#ifdef GNOME_ENABLE_DEBUG
  g_return_if_fail(obj != NULL);
  g_return_if_fail(GTK_IS_OBJECT(obj));
  
  gtk_signal_connect(GTK_OBJECT(obj),
                     "destroy",
                     GTK_SIGNAL_FUNC(spew_cb),
                     NULL);
#endif
}

////////////////////

#include "xyactions.h"
#include "baractions.h"
#include "pieactions.h"
#include "plotstore.h"
#include "markerpaletteedit.h"
#include "gfont.h"

static PlotStore* ps = 0;

PlotStore*
guppi_plot_store()
{
  g_warn_if_fail(ps != 0);
  return ps;
}

static MarkerPaletteEdit* mpe;

MarkerPaletteEdit* 
guppi_marker_palette_edit()
{
  g_warn_if_fail(mpe != 0);
  // OK to do this multiple times
  mpe->set_palette(guppi_marker_palette());
  return mpe;
}

class GnomeFrontend : public Frontend {
public:
  GnomeFrontend();
  virtual VFont* default_font();

private:
  GFont* default_font_;
  bool already_tried_default_font_;
};

static GnomeFrontend gnomefe;

bool
guppi_gnome_init_library()
{
  g_return_val_if_fail(ps == 0, false); // check for duplicate init

  if (!guppi_init_library())
    return false;

  guppi_register_frontend(&gnomefe);

  ps = new PlotStore;
  mpe = new MarkerPaletteEdit;

  guppi_gnome_init_xy();
  guppi_gnome_init_bar();
  guppi_gnome_init_pie();

  return true;
}

void 
guppi_gnome_shutdown_library()
{
  if (ps != 0)
    {
      delete ps;
      ps = 0;
    }

  if (mpe != 0)
    {
      delete mpe;
      mpe = 0;
    }

  guppi_shutdown_fonts();

  guppi_shutdown_library();
}


void
guppi_set_tooltip(GtkWidget* w, const string& tip)
{
  GtkTooltips* t = gtk_tooltips_new();

  gtk_tooltips_set_tip (t, w, tip.c_str(), NULL);
}

void 
guppi_raise_and_show(GtkWindow* w)
{
  if (w != 0)
    {
      GtkWidget* wid = GTK_WIDGET(w);

      if (!GTK_WIDGET_VISIBLE(wid))
        {
          gtk_widget_show(wid);
        }
      else
        { 
          gdk_window_show(wid->window);
          gdk_window_raise(wid->window);
        }
    }
}

/* This stuff is from gnome-libs, I wrote it there too though. */

/* Callback to display hint in the statusbar when a menu item is 
 * activated. For GnomeAppBar.
 */

static void
put_hint_in_appbar(GtkWidget* menuitem, gpointer data)
{
  gchar* hint =(char*)gtk_object_get_data (GTK_OBJECT(menuitem),
                                           "guppi_appbar_hint");
  GtkWidget* bar = (GtkWidget*)data;

  g_return_if_fail (hint != NULL);
  g_return_if_fail (bar != NULL);
  g_return_if_fail (GNOME_IS_APPBAR(bar));

  gnome_appbar_set_status (GNOME_APPBAR(bar), hint);
}

/* Callback to remove hint when the menu item is deactivated.
 * For GnomeAppBar.
 */
static void
remove_hint_from_appbar(GtkWidget* menuitem, gpointer data)
{
  GtkWidget* bar = (GtkWidget*)data;

  g_return_if_fail (bar != NULL);
  g_return_if_fail (GNOME_IS_APPBAR(bar));

  gnome_appbar_refresh (GNOME_APPBAR(bar));
}

/* Install a hint for a menu item
 */
void
guppi_install_menuitem_hint(GtkWidget* menuitem, GnomeAppBar* bar, 
                            const string& hint)
{
  /* This is mildly fragile; if someone destroys the appbar
     but not the menu, chaos will ensue. */

  gtk_object_set_data_full (GTK_OBJECT(menuitem),
                            "guppi_appbar_hint",
                            g_strdup(hint.c_str()),
                            g_free);
  
  gtk_signal_connect (GTK_OBJECT (menuitem),
                      "select",
                      GTK_SIGNAL_FUNC(put_hint_in_appbar),
                      bar);
  
  gtk_signal_connect (GTK_OBJECT (menuitem),
                      "deselect",
                      GTK_SIGNAL_FUNC(remove_hint_from_appbar),
                      bar);
}


GtkWidget* 
guppi_make_labelled_widget(const gchar* text, GtkWidget* child)
{
  GtkWidget* hbox = gtk_hbox_new(FALSE, 0);
  GtkWidget* label = gtk_label_new(text);

  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_end(GTK_BOX(hbox), child, TRUE, TRUE, 0);

  return hbox;
}


gchar* 
guppi_logo_file()
{
  gchar* logo = gnome_pixmap_file("guppi.xpm");  
  
  if (!g_file_exists(logo))
    {
      g_free(logo);
      logo = 0;
    }

  if (logo == 0)
    {
      if (g_file_exists(GUPPI_PIXMAPS"/guppi.xpm"))
        logo = g_strdup(GUPPI_PIXMAPS"/guppi.xpm");
    }

#ifdef GNOME_ENABLE_DEBUG
  if (logo == 0)
    {
      if (g_file_exists("./guppi.xpm"))
        logo = g_strdup("./guppi.xpm");
      else if (g_file_exists("../guppi.xpm"))
        logo = g_strdup("../guppi.xpm");
      else if (g_file_exists("./src/guppi.xpm"))
        logo = g_strdup("./src/guppi.xpm");
    }
#endif  

  return logo;
}

static GdkWindow* 
icon_window_new()
{
  gchar* logo = guppi_logo_file();

  GdkPixmap* pix = 0;

  if (logo != 0)
    {
      GdkImlibImage* im;
          
      im = gdk_imlib_load_image(logo);
          
      if (im != 0)
        {
          gdk_imlib_render(im, 48, 24);
          pix = gdk_imlib_move_image(im);
          gdk_imlib_destroy_image(im);
          im = 0;
        }
      else
        g_warning("Failed to load %s", logo);
    }
  else
    g_warning("Logo pixmap file not found");

  if (logo != 0)
    g_free(logo); 

  if (pix != 0)
    {
      GdkWindowAttr attributes;
      gint attributes_mask;
      GdkWindow* window;
      
      attributes.title = NULL;
      attributes.wmclass_name = NULL;
      attributes.wmclass_class = NULL;
      attributes.width = 48;
      attributes.height = 48;
      attributes.wclass = GDK_INPUT_OUTPUT;
      attributes.visual = gdk_imlib_get_visual ();
      attributes.colormap = gdk_imlib_get_colormap ();
      attributes.event_mask = 0;
      
      attributes_mask = GDK_WA_VISUAL | GDK_WA_COLORMAP;
      
      window = gdk_window_new (NULL, &attributes, attributes_mask);     

      gdk_window_set_back_pixmap(window, pix, FALSE);

      gdk_pixmap_unref(pix);

      return window;
    }

  return 0;
}

static void 
onrealize_seticon(GtkWindow* window, gpointer data)
{
  g_return_if_fail(GTK_WIDGET(window)->window != 0);

  GdkWindow* icon = icon_window_new();
  if (icon != 0)
    {
      g_debug("Setting icon for window %p", GTK_WIDGET(window)->window);

      gdk_window_set_icon(GTK_WIDGET(window)->window, icon, NULL, NULL);

      gdk_window_unref(icon);
    }
}

// Make our windows behave nicely
void 
guppi_setup_window(GtkWindow* window, const gchar* wmname)
{
  gtk_window_set_wmclass(window, wmname, "Guppi");

  // We always connect; we might want to reset the icon if we're unrealized
  //  and then re-realized
  gtk_signal_connect_after(GTK_OBJECT(window), "realize",
                           GTK_SIGNAL_FUNC(onrealize_seticon),
                           NULL);

  if (GTK_WIDGET_REALIZED(window))
    {
      onrealize_seticon(window, NULL);
    }
}

/////////////////// GnomeFrontend

GnomeFrontend::GnomeFrontend()
  : default_font_(0),
    already_tried_default_font_(false)
{


}

VFont* 
GnomeFrontend::default_font()
{
  if (default_font_ == 0 && !already_tried_default_font_)
    {
      default_font_ = GFont::get_font("Times", GFont::Normal, false, 18);

      if (default_font_ == 0)
        default_font_ = GFont::get_font("Helvetica", GFont::Normal, false, 18);

      if (default_font_ == 0)
        default_font_ =  GFont::get_font("fixed", GFont::Normal, false, 18);

      if (default_font_ == 0)
        {
          // pick first font in list
          GFontList* fl = guppi_get_font_list();
          
          g_assert(fl != 0);
          
          GFontList::iterator i = fl->begin();

          if (i != fl->end())
            {
              default_font_ = *i;
            }
        }

      if (default_font_ != 0)
        {
          default_font_->ref(); // default font is never freed

          g_debug("Default font: %s", default_font_->font_name());
        }
      else
        {
          gnome_error_dialog(_("Couldn't get a default font from the GNOME print library.\n"
                               "This indicates a misconfigured GNOME print that can't find any fonts.\n"
                               "Guppi will use an X font instead, but it will be slow, and layout and printing may not work properly."));
        }

      already_tried_default_font_ = true;
    }
  
  return default_font_;
}
