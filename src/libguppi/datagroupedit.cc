// -*- C++ -*-

/* 
 * datagroupedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "datagroupedit.h"
#include "dataoption.h"
#include "datastore.h"

DataGroupEdit::DataGroupEdit()
  : data_(0), store_(0), vbox_(0), no_data_label_(0), changing_data_(false)
{
  
}

DataGroupEdit::~DataGroupEdit()
{
  set_data(0); // to release our data.
  set_store(0); // remove from viewlist
}

GtkWidget* 
DataGroupEdit::widget() 
{
  ensure_widgets();
  g_return_val_if_fail(vbox_ != 0, 0);
  return vbox_;
}

void 
DataGroupEdit::set_data(DataGroup* dg)
{
  if (dg == data_)
    return;

  if (data_ != 0)
    {
      data_->data_model.remove_view(this);

      if (data_->unref() == 0)
        delete data_;
    }

  data_ = dg;

  if (data_ != 0)
    {
      data_->ref();
      data_->data_model.add_view(this);
    }

  if (vbox_ != 0)
    rebuild_dataoptions();
  else
    ensure_widgets();
}


void 
DataGroupEdit::set_store(DataStore* store)
{
  if (store_ == store) 
    return;

  if (store_ != 0)
    {
      //      store_->store_model.remove_view(this);
    }

  store_ = store;

  if (store_ != 0)
    {
      //      store_->store_model.add_view(this);
    }

  vector<DataOption*>::iterator i = options_.begin();
  while (i != options_.end())
    {
      (*i)->set_store(store_);
      ++i;
    }
}

void 
DataGroupEdit::ensure_widgets()
{
  if (vbox_ == 0)
    {
      vbox_ = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

      // Zero vbox_ if it's destroyed
      gtk_signal_connect(GTK_OBJECT(vbox_),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &vbox_);
    
      rebuild_dataoptions();
    }
}

void 
DataGroupEdit::clear_dataoptions()
{
  vector<DataOption*>::iterator i = options_.begin();
  while (i != options_.end())
    {
      (*i)->disconnect(data_chosen_cb);
      delete *i;
      ++i;
    }
  options_.clear();

  if (vbox_ != 0)
    {
      // Nukes either the option menus or the "No Data" label
      GList* option_widgets = gtk_container_children(GTK_CONTAINER(vbox_));
      GList* tmp = option_widgets;
      while (tmp != 0)
        {
          gtk_widget_destroy(GTK_WIDGET(tmp->data));
          tmp = g_list_next(tmp);
        }
      g_list_free(option_widgets);
    }
}

void 
DataGroupEdit::rebuild_dataoptions()
{
  g_return_if_fail(vbox_ != 0);

  clear_dataoptions();

  if (data_ == 0)
    {
      no_data_label_ = gtk_label_new(_("No Data Group"));

      gtk_container_add(GTK_CONTAINER(vbox_), no_data_label_);
    }
  else 
    {
      // Notice that the vector of options is parallel to the vector
      // of slots (i.e. the slot number is an index into the vector of
      // options)
      const DataGroupSpec* spec = data_->spec();

      guint N = spec->n_slots();
      guint i = 0;
      while (i < N)
        {
          DataOption* opt = new DataOption;

          opt->set_store(store_);
          opt->restrict_type(spec->slot_type(i));

          options_.push_back(opt);

          Data* d = data_->get_data(i);

          if (d != 0)
            opt->set_data(d);

          GtkWidget* child = guppi_make_labelled_widget(spec->slot_name(i).c_str(),
                                                        opt->widget());

          gtk_box_pack_start(GTK_BOX(vbox_), child, TRUE, TRUE, 0);

          gtk_widget_show_all(child);

          opt->connect(data_chosen_cb, (gpointer)this);

          ++i;
        }
    }
}

void 
DataGroupEdit::refresh_dataoptions()
{
  guint i = 0;
  while (i < options_.size())
    {
      options_[i]->set_data(data_->get_data(i));
      ++i;
    }
}

void 
DataGroupEdit::data_chosen_cb(gpointer emitter, gpointer data)
{
  DataGroupEdit* dge = static_cast<DataGroupEdit*>(data);

  dge->data_chosen((DataOption*)emitter);
}

void 
DataGroupEdit::data_chosen(DataOption* emitter)
{
  g_return_if_fail(data_ != 0);

  if (changing_data_)
    return;

  const DataGroupSpec* spec = data_->spec();

  guint N = spec->n_slots();

  g_return_if_fail(options_.size() == N);

  guint i = 0;
  while (i < N)
    {
      if (options_[i] == emitter)
        break;
      ++i;
    }
  g_return_if_fail(i < N);

  Data* d = emitter->get_data();

  if (d != data_->get_data(i))
    {
      data_->set_data(i, d);
    }
}


void 
DataGroupEdit::change_values(Data* d, const vector<guint> & which)
{
  // Should not happen for DataGroup
  g_assert_not_reached();
}

void 
DataGroupEdit::change_values(Data* d)
{
  changing_data_ = true;
  refresh_dataoptions();
  changing_data_ = false;
}

void 
DataGroupEdit::change_name(Data* d, const string & name)
{
  // we don't edit the name
  
}


void 
DataGroupEdit::destroy_model()
{
  // nothing

}

