// -*- C++ -*-

/* 
 * typeregistry.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_TYPEREGISTRY_H
#define GUPPI_TYPEREGISTRY_H

#include "util.h"
#include "refcount.h"

#include <string>
#include <vector>

// Used when we want to allow user registration of new types,
//  using an enumeration to discriminate the builtin types.
// Type IDs are integers, begin with 0, and are thus appropriate for 
//  indexing an array.
class TypeRegistry {
public:
  TypeRegistry();
  ~TypeRegistry();

  // Will be -1, i.e. not a valid enumeration value
  static const int INVALID_TYPE;

  // Must be added in order; type_id's must start with 
  // 0, and go up by 1 each time.
  void add_builtin_type(int type_id, // the enum value
                        int base_type_id, // parent type, or INVALID_TYPE if none
                        const char* name);

  // returns type_id
  int add_runtime_type(int base_type_id, // INVALID_TYPE if no parent
                       const char* name);

  bool is_valid(int type_id);

  int parent_type(int type_id); // returns INVALID_TYPE if none

  bool is_a(int type_id, int parent_type_id);

  string type_name(int type_id);

  void set_type_name(int type_id, const char* name);

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

private:
  RC rc_;

  vector<int> parents_; // indexed by type_id, filled with parent types
  vector<string> names_; // indexed by type_id
};


#endif
