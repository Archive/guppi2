// -*- C++ -*-

/* 
 * labeldata.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "labeldata.h"
#include <goose/StringSet.h>
#include <string>

LabelData::LabelData()
  : Data(Label), set_(0), checked_out_(false)
{
  set_ = new StringSet;
}

LabelData::LabelData(StringSet* ss)
  : Data(Label), set_(ss), checked_out_(false)
{

}
  
LabelData::~LabelData()
{
#ifdef GNOME_ENABLE_DEBUG
  if (checked_out_) g_warning("LabelData deleted with outstanding checkout!");
#endif

  if (set_ != 0) 
    delete set_;
}

gsize 
LabelData::size() const
{
  return set_->size();
}

const string& 
LabelData::name() const
{
  // The cast calls the label() which returns a reference 
  // instead of the one that returns a copy.
  return const_cast<const StringSet*>(set_)->label();
}

void 
LabelData::set_name(const string & name)
{
  set_->set_label(name);
  data_model.name_changed(this, name);
}

void 
LabelData::set_string(guint index, const string& value)
{
  g_return_if_fail(index < set_->size());
  
  set_->set_string(index, value);
  
  vector<guint> which;

  which.push_back(index);
  data_model.values_changed(this, which);
}

static const string empty_string("");

const string& 
LabelData::get_string(guint index) const
{
  g_return_val_if_fail(index < set_->size(), empty_string);

  return set_->get_string(index);
}

void 
LabelData::add(const string& value)
{
  set_->add_string(value);
}

xmlNodePtr 
LabelData::xml(xmlNodePtr parent) const
{
  g_warning("%s not implemented", __FUNCTION__);
  return 0;
}
  
void 
LabelData::set_xml(xmlNodePtr node)
{
  g_warning("%s not implemented", __FUNCTION__);
}

StringSet* 
LabelData::checkout_stringset()
{
  if (checked_out_) 
    return 0;
  else 
    {
      checked_out_ = true;
      return set_;
    }
}

void 
LabelData::checkin_stringset(StringSet* set, 
                             bool values_changed, 
                             bool other_changed)
{
  g_return_if_fail(checked_out_);
  g_return_if_fail(set == set_);

  if (values_changed)
    {
      data_model.values_changed(this);
    }
  if (other_changed)
    {
      data_model.name_changed(this, set_->label());
    }
  
  checked_out_ = false;
}

gsize 
LabelData::byte_size()
{
  return sizeof(LabelData);
}

