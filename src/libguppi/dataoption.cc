// -*- C++ -*-

/* 
 * dataoption.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "dataoption.h"

DataOption::DataOption()
  : store_(0), option_(0), menu_(0), current_(0), 
    show_type_(Data::Invalid),
    changing_data_(false), cblist_(this)
{

}

DataOption::~DataOption()
{
  if (store_ != 0)
    {
      store_->store_model.remove_view(this);
    }
}

void 
DataOption::restrict_type(Data::DataType t)
{
  show_type_ = t;
  if (option_ != 0)
    make_data_menu();
}

void 
DataOption::set_store(DataStore* store)
{
  if (store_ == store) 
    return;

  if (store_ != 0)
    {
      store_->store_model.remove_view(this);
    }

  store_ = store;

  if (store_ != 0)
    {
      store_->store_model.add_view(this);
    }

  if (option_ != 0)
    make_data_menu();
}

void
DataOption::add_data(DataStore* ds, Data* d)
{
  if (option_ == 0) return;
  make_data_menu();
}

void
DataOption::remove_data(DataStore* ds, Data* d)
{
  if (option_ == 0) return;
  make_data_menu();
}

void
DataOption::change_data_values(DataStore* ds, Data* d)
{
  // nothing

}

void
DataOption::change_data_values(DataStore* ds, Data* d, const vector<guint> & which)
{
  // nothing

}

void
DataOption::change_data_name(DataStore* ds, Data* d, const string & name)
{
  if (option_ == 0) return;

  map<Data*,GtkWidget*>::iterator i = labels_.find(d);
  g_return_if_fail(i != labels_.end());

  gtk_label_set_text(GTK_LABEL(i->second), name.c_str());
}

void
DataOption::destroy_model()
{
  store_ = 0;
  // This legitimately happens when Guppi is exited.
  //   g_warning("Store destroyed from under DataOption!");
}

void
DataOption::set_data(Data* d)
{
  if (current_ == d) return;

  current_ = d;

  if (option_ != 0)
    {
      map<Data*,gint>::iterator i = indexes_.find(current_);
      g_return_if_fail(i != indexes_.end());
      gtk_option_menu_set_history(GTK_OPTION_MENU(option_), i->second);
    }  

  // notify callback listeners.
  cblist_.invoke();
}

Data* 
DataOption::get_data()
{
  return current_;
}

GtkWidget* 
DataOption::widget()
{
  if (option_ == 0)
    {
      option_ = gtk_option_menu_new();
 
      // Zero option_ if it's destroyed
      gtk_signal_connect(GTK_OBJECT(option_),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &option_);
 
      menu_ = 0; // should be already, though.
      
      make_data_menu();
      
      gtk_widget_show(option_);
    }

  return option_;
}

void
DataOption::make_data_menu()
{
  g_return_if_fail(option_ != 0);

  if (menu_ != 0) { // if the contents of the DataStore change
    gtk_widget_destroy(menu_);
    labels_.clear();
    indexes_.clear();
  }

  menu_ = gtk_menu_new();

  if (store_ != 0)
    {
      DataStore::iterator i = store_->begin();
      DataStore::iterator end = store_->end();

      int active = -1;
      int count = 0;

      // We have to special-case "no data"/NULL pointer
      // FIXME should not simply cut-and-paste this code,
      // write a function
      {
        GtkWidget* mi = gtk_menu_item_new();
        GtkWidget* l = gtk_label_new(_("No Data Chosen"));

        gtk_container_add(GTK_CONTAINER(mi), l);

        labels_[0] = l;
        indexes_[0] = count;

        gtk_menu_append(GTK_MENU(menu_), mi);
        
        gtk_object_set_data(GTK_OBJECT(mi), "Data", 0);

        gtk_signal_connect(GTK_OBJECT(mi), "activate",
                           GTK_SIGNAL_FUNC(data_activated_cb),
                           this);

        if (current_ == 0)
          {
            active = count;
          }

        gtk_widget_show_all(mi);

        ++count;
      }

      while (i < end)
        {
          Data* d = *i;

          // Don't show data of the wrong type.
          if (show_type_ != Data::Invalid && 
              !d->is_a(show_type_))
            {
              ++i;
              continue;
            }

          GtkWidget* mi = gtk_menu_item_new();
          GtkWidget* l = gtk_label_new(d->name().c_str());

          gtk_container_add(GTK_CONTAINER(mi), l);
      
          labels_[d] = l;
          indexes_[d] = count;

          gtk_menu_append(GTK_MENU(menu_), mi);

          gtk_object_set_data(GTK_OBJECT(mi), "Data", d);

          gtk_signal_connect(GTK_OBJECT(mi), "activate",
                             GTK_SIGNAL_FUNC(data_activated_cb),
                             this);

          if (current_ == d)
            {
              active = count;
            }

          gtk_widget_show_all(mi);

          ++count;
          ++i;
        }

      gtk_option_menu_set_menu(GTK_OPTION_MENU(option_), menu_);

      if (active >= 0)
        {
          gtk_option_menu_set_history(GTK_OPTION_MENU(option_), active);
        }
    }
#ifdef GNOME_ENABLE_DEBUG
  else
    g_warning("No DataStore for DataOption?");
#endif
}

void
DataOption::data_activated_cb(GtkWidget* w, gpointer data)
{
  gpointer vd = gtk_object_get_data(GTK_OBJECT(w), "Data");

  // vd may be 0, for the "no data chosen" case!

  Data* d = static_cast<Data*>(vd);

  DataOption* dop = static_cast<DataOption*>(data);

  g_return_if_fail(dop != 0);

  dop->data_activated(d);
}

void
DataOption::data_activated(Data* d)
{
  set_data(d);
}

