// -*- C++ -*-

/* 
 * canvas-drawable.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_CANVASDRAWABLE_H
#define GUPPI_CANVASDRAWABLE_H

#include "util.h"

#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_pixbuf.h>

/* Drawable item for the canvas. Just lets us draw to the canvas in "raw" mode.
 * Based on GnomeCanvasImage.
 * The following arguments are available:
 *
 * name		type			read/write	description
 * -------------------------------------------------------------------------
 * x		double			RW		X coordinate of anchor point
 * y		double			RW		Y coordinate of anchor point
 * width	double			RW		Set width of drawable
 * height	double			RW		Set height of drawable
 * anchor	GtkAnchorType		RW		Anchor (corner where X,Y is)
 */


#define GNOME_TYPE_CANVAS_DRAWABLE            (gnome_canvas_drawable_get_type ())
#define GNOME_CANVAS_DRAWABLE(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_DRAWABLE, GnomeCanvasDrawable))
#define GNOME_CANVAS_DRAWABLE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_DRAWABLE, GnomeCanvasDrawableClass))
#define GNOME_IS_CANVAS_DRAWABLE(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_DRAWABLE))
#define GNOME_IS_CANVAS_DRAWABLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_DRAWABLE))


typedef struct _GnomeCanvasDrawable GnomeCanvasDrawable;
typedef struct _GnomeCanvasDrawableClass GnomeCanvasDrawableClass;

struct _GnomeCanvasDrawable {
  GnomeCanvasItem item;
  
  GdkPixmap* buffer;           /* The drawable */
  
  double x, y;			/* Position at anchor, item relative */
  double width, height;		/* Size of drawable, item relative */
  GtkAnchorType anchor;		/* Anchor side for drawable */
  
  int cx, cy;			/* Top-left canvas coordinates for display */
  int cwidth, cheight;		/* Rendered size in pixels */
  GdkGC *gc;			/* GC for drawing drawable */
  
  ArtPixBuf *pixbuf;		/* A pixbuf, for aa rendering */
  double affine[6];               /* The item -> canvas affine */

  guint recreate_pixmap : 1;   /* Need to re-make the pixmap */
};

struct _GnomeCanvasDrawableClass {
  GnomeCanvasItemClass parent_class;
};


/* Standard Gtk function */
GtkType gnome_canvas_drawable_get_type (void);

GdkPixmap* gnome_canvas_drawable_get_drawable(GnomeCanvasDrawable* d);

#endif
