// -*- C++ -*-

/* 
 * tool.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_TOOL_H
#define GUPPI_TOOL_H

#include <string>
#include <gdk/gdk.h>
#include "refcount.h"

class Plot;

// Actions/Tools are part of the Gnome frontend.

// Actions are "sub-tools," actions a tool can invoke
//  on mouse button presses. This makes it easy to define 
//  custom tools.

class Action {
public:
  Action(const char* id) : id_(id) {}
  virtual ~Action() {}

  const string& id() const { return id_; }

  // user-visible name of the action
  virtual const string& name() const = 0;

  // tooltip describing the action
  virtual const string& tooltip() const = 0;
  
  virtual gint event(Plot* p, GdkEvent* e) = 0;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

protected:
  // Grab and ungrab all events that go to the tool;
  //  should also grab the pointer when using these, 
  //  so the user doesn't do anything to prevent
  //  the ungrab taking place.
  void grab(Plot* p);
  void ungrab(Plot* p);

private:
  const string id_;
  RC rc_;

};

#include <map>

// This is used by Tool to store the currently selected actions

class ActionChest {
public:
  ActionChest();
  ~ActionChest();

  // a can be 0, to unset an action
  void set_action(Action* a, gint button, GdkModifierType mask);
  Action* get_action(gint button, GdkModifierType mask) const;

private:
  map<gint, map<GdkModifierType,Action*> > actions_;

};

class Plot;

// A Tool is a "mode" that a plot can be in, which affects what happens 
//  when the user manipulates the plot. Basically it's a collection of 
//  actions bound to buttons, plus possibly other things.
// (Eventually you'll be able to define them in scheme; the main barrier 
//  to that now is that we aren't using guile-gtk and thus no GdkEvent
//  in scheme.)

class Tool {
public:
  Tool(const char* id);
  virtual ~Tool();

  const string& id() const { return id_; }

  // Get and set action are used to customize the tool;
  // Tool::event() automatically invokes actions if needed.

  // Set the current action, must be in the return list from
  // Plot::actions() Action will be bound to the button and modmask
  // given To unset a action set it to 0
  void set_action(const Action* t, gint button, GdkModifierType mask);

  // Get the current action for button/mask, guaranteed to be in the
  // list from actions() or 0 if no binding for that button/mask
  // By default, bindings are to buttons 1 and 2 only.
  // Containers can set others if they want to.
  const Action* get_action(gint button, GdkModifierType mask) const;

  // user-visible name of the tool
  virtual const string& name() const = 0;

  // tooltip describing the tool
  virtual const string& tooltip() const = 0;

  // pixmap for the tool
  virtual const string& icon_name() const = 0;

  // There is a default implementation of this that sees whether 
  //  any actions are bound to this event and invokes them if so.
  //  Subclasses can call the default implementation as appropriate.
  virtual gint event(Plot* p, GdkEvent* event);

  // Called when this tool is made current
  virtual void activate() = 0;

  // Called when it's made non-current.
  virtual void deactivate() = 0;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

private:
  const string id_;
  ActionChest chest_;

  map<Plot*,Action*> grabs_;

  RC rc_;

  friend class Action;

  void action_grab(Plot* p, Action* a);
  void action_ungrab(Plot* p);

  const Tool& operator=(const Tool&);
  Tool(const Tool&);
};

// Your basic tool, simply invokes any
// actions that are bound.
// Allows some tool-defining in scheme since you can set up 
//  action bindings without having to know anything about GdkEvent
//  in scheme-land.
class ActionTool : public Tool {
public:
  ActionTool(const char* id, 
             const char* name, const char* tooltip, const char* icon_name);
  virtual ~ActionTool();

  // user-visible name of the tool
  virtual const string& name() const;

  // tooltip describing the tool
  virtual const string& tooltip() const;

  // pixmap for the tool
  virtual const string& icon_name() const;

  // There is a default implementation of this that sees whether 
  //  any actions are bound to this event and invokes them if so.
  //  Subclasses can call the default implementation as appropriate.
  virtual gint event(Plot* p, GdkEvent* event);

  // Called when this tool is made current
  virtual void activate();

  // Called when it's made non-current.
  virtual void deactivate();

private:
  string name_;
  string tooltip_;
  string icon_;

};

#endif
