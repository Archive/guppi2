/* 
 * action-manager.c
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "action-manager.h"
#include <gnome.h>

typedef enum {
  ACTION,
  GROUP,
  SEPARATOR,
  CHECK,
  RADIO
} ActionType;

typedef struct _Action Action;

struct _Action {
  GnomeActionManager* am;
  ActionType type;
  guint refcount;
  gchar* id;
  GnomeActionFunc callback;
  gpointer user_data;
  gchar* user_visible_name;
  gchar* editor_name;
  gchar* tooltip;
  gchar* small_icon;
  gchar* large_icon;
  gchar* stock_icon;
  GSList* parents;
  GSList* children;
  GSList* widgets;
};

typedef struct _View View;

struct _View {
  GnomeActionManager* am;
  GtkWidget* hint_bar;
  gchar* history_group_id;
};

static void action_ref(Action* a);
static void action_unref(Action* a);
static void action_remove_widget(Action* a, GtkWidget* w);
static Action* widget_get_action(GtkWidget* w);
static gpointer widget_get_model(GtkWidget* w);
static gpointer widget_get_view(GtkWidget* w);
static Action* widget_get_parent(GtkWidget* w);
static void widget_set_parent(GtkWidget* w, Action* parent);
static GtkWidget* action_make_menuitem(Action* a, gpointer model, gpointer view);
static GtkWidget* action_make_button(Action* a, gpointer model, gpointer view);
static View* am_get_view(GnomeActionManager* am, gpointer view);
static Action* am_must_get_action(GnomeActionManager* am, const gchar* action_id);
static void action_append_unique(Action* parent, Action* child);
static void action_prepend_unique(Action* parent, Action* child);
static void action_remove_widget(Action* a, GtkWidget* w);
static void action_remove(Action* parent, Action* child);

static View* 
view_new(GnomeActionManager* am)
{
  View* v = g_new0(View, 1);

  v->am = am;

  return v;
}

static void
view_destroy(View* v)
{
  if (v->hint_bar)
    gtk_widget_unref(v->hint_bar);

  if (v->history_group_id)
    g_free(v->history_group_id);
  
  g_free(v);
}

static void
view_set_hint_bar(View* v, GtkWidget* bar)
{
  if (v->hint_bar == bar)
    return;

  if (v->hint_bar)
    gtk_widget_unref(v->hint_bar);

  v->hint_bar = bar;

  gtk_widget_ref(v->hint_bar);
}

static void
view_set_history_group(View* v, const gchar* group_id)
{
  if (v->history_group_id)
    g_free(v->history_group_id);

  if (group_id)
    v->history_group_id = g_strdup(group_id);
  else
    v->history_group_id = NULL;
}

static void
action_append(Action* parent, Action* child)
{
  GSList* tmp;

  g_return_if_fail(parent->type == GROUP);

  parent->children = g_slist_append(parent->children, child);

  child->parents = g_slist_prepend(child->parents, parent);

  action_ref(child);

  /* For each widget associated with parent, add a child widget */
  tmp = parent->widgets;
  while (tmp != NULL)
    {
      GtkWidget* w = tmp->data;
      GtkWidget* cw = NULL;

      if (GTK_IS_MENU(w))
        {
          cw = action_make_menuitem(child, 
                                    widget_get_model(w),
                                    widget_get_view(w));
          gtk_menu_append(GTK_MENU(w), cw); 
        }
      else if (GTK_IS_MENU_BAR(w))
        {
          cw = action_make_menuitem(child, 
                                    widget_get_model(w),
                                    widget_get_view(w));

          gtk_menu_bar_append(GTK_MENU_BAR(w), cw);
        }
      else if (GTK_IS_TOOLBAR(w))
        {
          cw = action_make_button(child, 
                                  widget_get_model(w),
                                  widget_get_view(w));
          gtk_toolbar_append_widget(GTK_TOOLBAR(w),
                                    cw,
                                    NULL, NULL);
        }

      if (cw)
        widget_set_parent(cw, parent);

      tmp = g_slist_next(tmp);
    }
}

static void
action_prepend(Action* parent, Action* child)
{
  GSList* tmp;

  g_return_if_fail(parent->type == GROUP);

  parent->children = g_slist_prepend(parent->children, child);

  child->parents = g_slist_prepend(child->parents, parent);

  action_ref(child);

  /* For each widget associated with parent, add a child widget */
  tmp = parent->widgets;
  while (tmp != NULL)
    {
      GtkWidget* w = tmp->data;
      GtkWidget* cw = NULL;

      if (GTK_IS_MENU(w))
        {
          cw = action_make_menuitem(child, 
                                    widget_get_model(w),
                                    widget_get_view(w));
          gtk_menu_prepend(GTK_MENU(w), cw);

        }
      else if (GTK_IS_MENU_BAR(w))
        {
          cw = action_make_menuitem(child, 
                                    widget_get_model(w),
                                    widget_get_view(w));
          gtk_menu_bar_prepend(GTK_MENU_BAR(w), cw);
        }
      else if (GTK_IS_TOOLBAR(w))
        {
          cw = action_make_button(child, 
                                  widget_get_model(w),
                                  widget_get_view(w));
          gtk_toolbar_prepend_widget(GTK_TOOLBAR(w),
                                     cw,
                                     NULL, NULL);

        }
      
      if (cw)
        widget_set_parent(cw, parent);

      tmp = g_slist_next(tmp);
    }
}

static gboolean
action_is_child_of(Action* parent, Action* child)
{
  GSList* tmp = parent->children;
  while (tmp != NULL)
    {
      if (((Action*)tmp->data) == child)
        return TRUE;
      
      tmp = g_slist_next(tmp);
    }
  return FALSE;
}


static void 
action_move_to_top(Action* parent, Action* child)
{
  /* Lazy inefficient way */
  action_remove(parent, child);
  action_prepend(parent, child);
}

static void 
action_move_to_bottom(Action* parent, Action* child)
{
  /* Lazy inefficient way */
  action_remove(parent, child);
  action_append(parent, child);
}

static void
action_append_unique(Action* parent, Action* child)
{
  if (action_is_child_of(parent, child))
    action_move_to_bottom(parent, child);
  else
    action_append(parent, child);
}

static void
action_prepend_unique(Action* parent, Action* child)
{
  if (action_is_child_of(parent, child))
    action_move_to_top(parent, child);
  else
    action_prepend(parent, child);
}

static void
action_remove(Action* parent, Action* child)
{
  GSList* tmp;
  GSList* to_remove = NULL;

  g_return_if_fail(parent->type == GROUP);

  parent->children = g_slist_remove(parent->children, child);

  child->parents = g_slist_remove(child->parents, parent);

  action_unref(child);

  /* Remove child widgets that are children of this group */
  tmp = child->widgets;
  while (tmp != NULL)
    {
      GtkWidget* w = tmp->data;
      Action* p = widget_get_parent(w);
      
      if (p == parent)
        to_remove = g_slist_prepend(to_remove, w);

      tmp = g_slist_next(tmp);
    }

  tmp = to_remove;
  while (tmp != NULL)
    {
      GtkWidget* w = tmp->data;

      if (w->parent)
        gtk_container_remove(GTK_CONTAINER(w->parent), w);

      action_remove_widget(child, w);

      tmp = g_slist_next(tmp);
    }
  g_slist_free(to_remove);
}

static void
action_detach(Action* a)
{
  action_ref(a);
  while (a->parents)
    {
      action_remove(a->parents->data, a);
    }
  action_unref(a);
}

static Action* 
action_new(GnomeActionManager* am, ActionType type, const gchar* id, 
           GnomeActionFunc callback, gpointer user_data)
{
  Action* a = g_new0(Action, 1);
  
  a->am = am;
  a->refcount = 0;
  a->type = type;
  a->id = g_strdup(id);
  a->callback = callback;
  a->user_data = user_data;

  return a;
}

static void
action_ref(Action* a)
{
  a->refcount += 1;
}

static void 
action_unref(Action* a)
{
  g_return_if_fail(a->refcount > 0);

  a->refcount -= 1;

  if (a->refcount == 0)
    {
      g_assert(a->parents == NULL);

      /* This is efficient, since we remove from the front of the list */
      while (a->children)
        action_remove(a, a->children->data); 

      while (a->widgets)
        action_remove_widget(a, a->widgets->data);

      if (a->id)
        g_free(a->id);
      
      if (a->user_visible_name)
        g_free(a->user_visible_name);

      if (a->tooltip)
        g_free(a->tooltip);

      if (a->small_icon)
        g_free(a->small_icon);

      if (a->large_icon)
        g_free(a->small_icon);
      
      if (a->stock_icon)
        g_free(a->stock_icon);       

      g_free(a);
    }
}

#define WIDGET_ACTION_KEY "{-*gnome-am-action*-}"
#define WIDGET_MODEL_KEY "{-*gnome-am-model*-}"
#define WIDGET_VIEW_KEY "{-*gnome-am-view*-}"
#define WIDGET_LABEL_KEY "*-{gnome-am-label}-*"
#define WIDGET_PIXMAP_KEY "*-{gnome-am-pixmap}-*"
#define WIDGET_PARENT_KEY "*-{gnome-am-parent}-*"

static void widget_destroyed_callback(GtkWidget* w, gpointer data);
static void widget_invoke_callback(GtkWidget* w, gpointer data);

static void
action_add_widget(Action* a, GtkWidget* w, gpointer model, gpointer view)
{
  a->widgets = g_slist_prepend(a->widgets, w);
  gtk_widget_ref(w);
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_ACTION_KEY, a);
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_MODEL_KEY, model);
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_VIEW_KEY, view);

  gtk_signal_connect(GTK_OBJECT(w),
                     "destroy",
                     GTK_SIGNAL_FUNC(widget_destroyed_callback),
                     a);

  /* The widget refs the action so the action will be valid in the callback */
  action_ref(a);
}

static void 
action_remove_widget(Action* a, GtkWidget* w)
{
  a->widgets = g_slist_remove(a->widgets, w);
  gtk_widget_unref(w);
  action_unref(a);
}

static void
widget_destroyed_callback(GtkWidget* w, gpointer data)
{
  Action* a = (Action*)data;
  
  action_remove_widget(a, w);
}

static void 
widget_invoke_callback(GtkWidget* w, gpointer data)
{
  Action* a = (Action*)data;
  gpointer view = widget_get_view(w);
  View* v = am_get_view(a->am, view);

  (*a->callback)(a->am, a->id, 
                 widget_get_model(w), view,
                 a->user_data);

  /* Add to the history, if appropriate */
  if (v->history_group_id)
    {
      Action* history = am_must_get_action(a->am, v->history_group_id);

      action_prepend_unique(history, a);
    }
}

static Action* 
widget_get_action(GtkWidget* w)
{
  Action* a = gtk_object_get_data(GTK_OBJECT(w), WIDGET_ACTION_KEY);
  g_return_val_if_fail(a != NULL, NULL);
  return a;
}

static gpointer 
widget_get_model(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_MODEL_KEY);
  return p;
}

static gpointer 
widget_get_view(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_VIEW_KEY);
  return p;
}

static void
widget_set_label(GtkWidget* w, GtkWidget* label)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_LABEL_KEY, label);
}

static void 
widget_set_pixmap(GtkWidget* w, GtkWidget* pixmap)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_PIXMAP_KEY, pixmap);
}

static void 
widget_set_parent(GtkWidget* w, Action* parent)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_PARENT_KEY, parent);
}

static GtkWidget*
widget_get_label(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_LABEL_KEY);
  return p;
}

static GtkWidget*
widget_get_pixmap(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_PIXMAP_KEY);
  return p;
}

static Action*
widget_get_parent(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_PARENT_KEY);
  return p;
}


static GtkWidget*
create_label (char* label_text, guint* keyval)
{
  guint kv;
  GtkWidget *label;

  label = gtk_accel_label_new (label_text);

  kv = gtk_label_parse_uline (GTK_LABEL (label), label_text);
  if (keyval)
    *keyval = kv;

  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_widget_show (label);

  return label;
}

static GtkWidget* action_make_menu(Action* a, gpointer model, gpointer view);

static GtkWidget*
action_make_menuitem(Action* a, gpointer model, gpointer view)
{
  GtkWidget* mi = NULL;
  GtkWidget* pixmap = NULL;
  GtkWidget* label = NULL;

  if (a->small_icon || a->stock_icon)
    {
      if (gnome_config_get_bool("/Gnome/Icons/MenusUseIcons=true") &&
          gnome_preferences_get_menus_have_icons())
        {
          mi = gtk_pixmap_menu_item_new();

          if (a->stock_icon)
            {
              pixmap = gnome_stock_pixmap_widget(mi, a->stock_icon);
            }
          else if (a->small_icon)
            {
              gchar* fn = gnome_pixmap_file(a->small_icon);
              
              if (fn == NULL)
                g_warning("Couldn't find pixmap %s", a->small_icon);
              else
                {
                  pixmap = gnome_pixmap_new_from_file(fn);
                  g_free(fn);
                }
            }

          if (pixmap != NULL)
            {
              gtk_widget_show(pixmap);
              gtk_pixmap_menu_item_set_pixmap(GTK_PIXMAP_MENU_ITEM(mi),
                                              pixmap);
              widget_set_pixmap(mi, pixmap);
            }
        }
    }
  else
    {
      mi = gtk_menu_item_new();
    }

  if (a->type == GROUP || a->type == ACTION)
    {
      label = create_label(a->user_visible_name, NULL);
  
      gtk_container_add(GTK_CONTAINER(mi), label);

      widget_set_label(mi, label);
    }

  action_add_widget(a, mi, model, view);

  gtk_widget_show(mi);

  if (a->type == ACTION)
    {
      gtk_signal_connect(GTK_OBJECT(mi), 
                         "activate",
                         GTK_SIGNAL_FUNC(widget_invoke_callback),
                         a);
    }
  else if (a->type == GROUP)
    {
      GtkWidget* menu = action_make_menu(a, model, view);
      
      gtk_menu_item_set_submenu(GTK_MENU_ITEM(mi), menu);
    }

  return mi;
}

static GtkWidget*
action_make_menu(Action* a, gpointer model, gpointer view)
{
  GtkWidget* menu = NULL;
  GSList* tmp = NULL;

  g_return_val_if_fail(a->type == GROUP, NULL);

  menu = gtk_menu_new();

  tmp = a->children;
  while (tmp != NULL)
    {
      Action* child = (Action*)tmp->data;
      GtkWidget* mi = action_make_menuitem(child, model, view);

      gtk_menu_append(GTK_MENU(menu), mi);

      widget_set_parent(mi, a);

      tmp = g_slist_next(tmp);
    }

  action_add_widget(a, menu, model, view);

  return menu;
}

static GtkWidget*
action_make_menubar(Action* a, gpointer model, gpointer view)
{
  GtkWidget* menubar = NULL;
  GSList* tmp = NULL;

  menubar = gtk_menu_bar_new();

  tmp = a->children;
  while (tmp != NULL)
    {
      GtkWidget* mi = action_make_menuitem((Action*)tmp->data, model, view);

      gtk_menu_bar_append(GTK_MENU_BAR(menubar), mi);

      widget_set_parent(mi, a);

      tmp = g_slist_next(tmp);
    }

  action_add_widget(a, menubar, model, view);

  gtk_widget_show(menubar);

  return menubar;
}

static GtkWidget* 
action_make_button(Action* a, gpointer model, gpointer view)
{
  GtkWidget* button = NULL;
  GtkWidget* pixmap = NULL;
  GtkWidget* label = NULL;
  GtkWidget* vbox = NULL;

  g_return_val_if_fail(a->type != GROUP, NULL);
  g_return_val_if_fail(a->user_visible_name != NULL, NULL);

  button = gtk_button_new();
  vbox = gtk_vbox_new(FALSE, 2);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 2);
  gtk_container_add(GTK_CONTAINER(button), vbox);

  gtk_widget_show(vbox);

  if (a->small_icon || a->stock_icon)
    {
      if (gnome_config_get_bool("/Gnome/Icons/MenusUseIcons=true") &&
          gnome_preferences_get_menus_have_icons())
        {
          if (a->stock_icon)
            {
              pixmap = gnome_stock_pixmap_widget(button, a->stock_icon);
            }
          else if (a->large_icon)
            {
              gchar* fn = gnome_pixmap_file(a->small_icon);
              
              if (fn == NULL)
                g_warning("Couldn't find pixmap %s", a->small_icon);
              else
                {
                  pixmap = gnome_pixmap_new_from_file(fn);
                  g_free(fn);
                }
            }

          if (pixmap == NULL)
            {
              /* Kind of wrong, but I'm over it */
              pixmap = gtk_type_new (gnome_pixmap_get_type ());
            }

          gtk_box_pack_start(GTK_BOX(vbox), pixmap, FALSE, FALSE, 0);
          gtk_widget_show(pixmap);
          widget_set_pixmap(button, pixmap);
        }
    }

  label = create_label(a->user_visible_name, NULL);
  
  gtk_box_pack_end(GTK_BOX(vbox), label, FALSE, FALSE, 0);

  widget_set_label(button, label);

  action_add_widget(a, button, model, view);

  if (a->type == ACTION)
    {
      gtk_signal_connect(GTK_OBJECT(button), 
                         "clicked",
                         GTK_SIGNAL_FUNC(widget_invoke_callback),
                         a);
    }

  gtk_widget_show(button);

  return button;
}

static GtkWidget*
action_make_toolbar(Action* a, gpointer model, gpointer view, GtkOrientation orientation)
{
  GtkWidget* toolbar = NULL;
  GSList* tmp = NULL;

  g_return_val_if_fail(a->type == GROUP, NULL);

  toolbar = gtk_toolbar_new(orientation, GTK_TOOLBAR_BOTH);

  tmp = a->children;
  while (tmp != NULL)
    {
      Action* child = (Action*)tmp->data;
      GtkWidget* button;

      if (child->type != ACTION)
        {
          g_warning("Wrong child type for toolbar group");
        }
      else
        {
          button = action_make_button(child, model, view);
          
          gtk_toolbar_append_widget(GTK_TOOLBAR(toolbar), button, NULL, NULL);
          
          widget_set_parent(button, a);
        }

      tmp = g_slist_next(tmp);
    }

  action_add_widget(a, toolbar, model, view);

  gtk_widget_show(toolbar);

  return toolbar;  
}

static void 
action_set_name(Action* a, const gchar* name)
{
  GSList* tmp = NULL;

  if (a->user_visible_name)
    g_free(a->user_visible_name);

  if (name)
    a->user_visible_name = g_strdup(name);
  else 
    a->user_visible_name = NULL;

  tmp = a->widgets;
  while (tmp != NULL)
    {
      GtkWidget* label = widget_get_label(GTK_WIDGET(tmp->data));

      if (label != NULL)
        gtk_label_set_text(GTK_LABEL(label), 
                           a->user_visible_name ? a->user_visible_name : "");

      tmp = g_slist_next(tmp);
    }
}


static void 
action_set_editor_name(Action* a, const gchar* name)
{
  if (a->editor_name)
    g_free(a->editor_name);

  if (name)
    a->editor_name = g_strdup(name);
  else
    a->editor_name = NULL;

  /* FIXME emit signal? */
}

static void
action_set_tooltip(Action* a, const gchar* tooltip)
{
  if (a->tooltip)
    g_free(a->tooltip);

  if (tooltip)
    a->tooltip = g_strdup(tooltip);
  else 
    a->tooltip = NULL;

  /* FIXME update widgets */
}

static void 
action_set_small_icon(Action* a, const gchar* filename)
{
  if (a->small_icon)
    g_free(a->small_icon);
      
  if (filename)
    a->small_icon = g_strdup(filename);
  else 
    a->small_icon = NULL;

  /* FIXME update widgets */
}

static void 
action_set_large_icon(Action* a, const gchar* filename)
{
  if (a->large_icon)
    g_free(a->large_icon);
      
  if (filename)
    a->large_icon = g_strdup(filename);
  else 
    a->large_icon = NULL;

  /* FIXME update widgets */
}

static void 
action_set_stock_icon(Action* a, const gchar* name)
{
  if (a->stock_icon)
    g_free(a->stock_icon);

  if (name)
    a->stock_icon = g_strdup(name);
  else
    a->stock_icon = NULL;

  /* FIXME update widgets */
}

static void 
action_set_sensitive(Action* a, gboolean sensitivity)
{
  GSList* tmp = a->widgets;
  while (tmp != NULL)
    {
      gtk_widget_set_sensitive(GTK_WIDGET(tmp->data), sensitivity);

      tmp = g_slist_next(tmp);
    }
}

static void 
action_set_sensitive_by_model(Action* a, gpointer model, gboolean sensitivity)
{
  GSList* tmp = a->widgets;
  while (tmp != NULL)
    {
      gpointer m = widget_get_model(GTK_WIDGET(tmp->data));

      if (m == model)
        gtk_widget_set_sensitive(GTK_WIDGET(tmp->data), sensitivity);

      tmp = g_slist_next(tmp);
    }
}

static void 
action_set_sensitive_by_view(Action* a, gpointer view, gboolean sensitivity)
{
  GSList* tmp = a->widgets;
  while (tmp != NULL)
    {
      gpointer v = widget_get_view(GTK_WIDGET(tmp->data));

      if (v == view)
        gtk_widget_set_sensitive(GTK_WIDGET(tmp->data), sensitivity);
      
      tmp = g_slist_next(tmp);
    }
}


/********/

static void 
am_real_add(GnomeActionManager* am,
            Action* a)
{
  action_ref(a); /* hash table ref */

  /* note that a->id is the hash key, so it shouldn't be freed, etc. */
  g_hash_table_insert(am->action_hash, a->id, a);

  /* emit "action added" signal */
}

static void 
am_real_remove(GnomeActionManager* am,
               Action* a)
{
  action_detach(a);

  g_hash_table_remove(am->action_hash, a->id);

  action_unref(a);
}

static Action*
am_get_action(GnomeActionManager* am, const gchar* action_id)
{
  gpointer key = 0;
  gpointer value = 0;

  if (g_hash_table_lookup_extended(am->action_hash, action_id, &key, &value))
    {
      g_assert(key == ((Action*)value)->id); /* check our internals */
      return (Action*)value;
    }  
  else
    return NULL;
}

static Action*
am_must_get_action(GnomeActionManager* am, const gchar* action_id)
{
  Action* a = am_get_action(am, action_id);

  g_return_val_if_fail(a != NULL, NULL);

  return a;
}

static View* 
am_get_view(GnomeActionManager* am, gpointer view)
{
  gpointer key = 0;
  gpointer value = 0;
  
  if (g_hash_table_lookup_extended(am->view_hash, view, &key, &value))
    {
      return (View*)value;
    }  
  else
    {
      View* v = view_new(am);
      
      g_hash_table_insert(am->view_hash, view, v);

      printf("New view for %p\n", view);

      return v;
    }
}

static void
am_forget_view(GnomeActionManager* am, gpointer view)
{
  gpointer key = NULL;
  gpointer value = NULL;

  if (g_hash_table_lookup_extended(am->view_hash, view, &key, &value))
    {
      View* v = (View*) value;

      view_destroy(v);

      g_hash_table_remove(am->view_hash, view);      
    }  
  else
    g_warning("No view %p to forget!", view);
}

/******** Public API *************/

GnomeActionManager* 
gnome_action_manager_new()
{
  GnomeActionManager* am = g_new0(GnomeActionManager, 1);

  am->action_hash = g_hash_table_new(g_str_hash, g_str_equal);
  am->view_hash = g_hash_table_new(g_direct_hash, NULL);

  return am;
}

void 
gnome_action_manager_destroy(GnomeActionManager* am)
{
  g_hash_table_destroy(am->action_hash);
  g_hash_table_destroy(am->view_hash);
  g_free(am);
}

void
gnome_action_manager_add (GnomeActionManager* am,
                          const gchar* action_id,
                          GnomeActionFunc callback,
                          gpointer user_data)
{
  Action* a = action_new(am, ACTION, action_id, callback, user_data);

  am_real_add(am, a);
}

void
gnome_action_manager_add_group (GnomeActionManager* am,
                                const gchar* action_id)
{
  Action* a = action_new(am, GROUP, action_id, NULL, NULL);
  
  am_real_add(am, a);
}

#if 0
void
gnome_action_manager_add_dynamic (GnomeActionManager* am,
                                  const gchar* action_id)
{
  Action* a = action_new(am, DYNAMIC, action_id, NULL, NULL);
  
  am_real_add(am, a);
}
#endif

void
gnome_action_manager_set_info (GnomeActionManager* am,
                               const gchar* action_id,
                               const gchar* user_visible_name,
                               const gchar* editor_name,
                               const gchar* tooltip,
                               const gchar* small_icon,
                               const gchar* large_icon,
                               const gchar* stock_icon)

{
  Action* a = am_must_get_action(am, action_id);
  
  action_set_name(a, user_visible_name);
  action_set_editor_name(a, editor_name);
  action_set_tooltip(a, tooltip);
  action_set_small_icon(a, small_icon);
  action_set_large_icon(a, large_icon);
  action_set_stock_icon(a, stock_icon);
}

void 
gnome_action_manager_set_name (GnomeActionManager* am,
                               const gchar* action_id,
                               const gchar* user_visible_name)
{

  Action* a = am_must_get_action(am, action_id);
  
  action_set_name(a, user_visible_name);
}

void 
gnome_action_manager_set_editor_name (GnomeActionManager* am,
                                      const gchar* action_id,
                                      const gchar* editor_name)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_editor_name(a, editor_name);
}

void 
gnome_action_manager_set_tooltip (GnomeActionManager* am,
                                  const gchar* action_id,
                                  const gchar* tooltip)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_tooltip(a, tooltip);
}

void
gnome_action_manager_set_small_icon (GnomeActionManager* am,
                                     const gchar* action_id,
                                     const gchar* filename)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_small_icon(a, filename);
}

void
gnome_action_manager_set_large_icon (GnomeActionManager* am,
                                     const gchar* action_id,
                                     const gchar* filename)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_large_icon(a, filename);
}

void
gnome_action_manager_set_stock_icon (GnomeActionManager* am,
                                     const gchar* action_id,
                                     const gchar* stock_icon)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_stock_icon(a, stock_icon);
}

void
gnome_action_manager_remove_action (GnomeActionManager* am,
                                    const gchar* action_id)
{
  Action* a = am_must_get_action(am, action_id);

  am_real_remove(am, a);
}

void
gnome_action_manager_append (GnomeActionManager* am,
                             const gchar* group_id, /* Or dynamic */
                             const gchar* action_id)
{
  Action* parent = am_must_get_action(am, group_id);
  Action* child = am_must_get_action(am, action_id);
 
  action_append(parent, child);
}

void
gnome_action_manager_insert (GnomeActionManager* am,
                             const gchar* group_id, /* Or dynamic */
                             const gchar* action_id,
                             gint position)
{

}

void
gnome_action_manager_remove (GnomeActionManager* am,
                             const gchar* group_id, /* or dynamic */
                             const gchar* action_id)
{
  Action* parent = am_must_get_action(am, group_id);
  Action* child = am_must_get_action(am, action_id);
 
  action_remove(parent, child);
}

void
gnome_action_manager_invoke_action (GnomeActionManager* am,
                                    /* a real action, not a group or dynamic */
                                    const gchar* action_id,
                                    gpointer model,
                                    gpointer view)
{
  Action* a = am_must_get_action(am, action_id);

  g_return_if_fail(a->type != GROUP);

  (*a->callback)(am, a->id, model, view, a->user_data);
}

void
gnome_action_manager_set_sensitive (GnomeActionManager* am,
                                    const gchar* action_id,
                                    gboolean sensitive)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_sensitive(a, sensitive);
}

void
gnome_action_manager_set_sensitive_by_model (GnomeActionManager* am,
                                             const gchar* action_id,
                                             gpointer model,
                                             gboolean sensitive)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_sensitive_by_model(a, model, sensitive);
}

void
gnome_action_manager_set_sensitive_by_view (GnomeActionManager* am,
                                            const gchar* action_id,
                                            gpointer view,
                                            gboolean sensitive)
{
  Action* a = am_must_get_action(am, action_id);

  action_set_sensitive_by_view(a, view, sensitive);  

}

GtkWidget* 
gnome_action_manager_make_menubar (GnomeActionManager* am,
                                   const gchar* group_id,
                                   gpointer model,
                                   gpointer view)
{
  Action* a = am_must_get_action(am, group_id);
  
  GtkWidget* menubar = action_make_menubar(a, model, view);

  return menubar;
}

GtkWidget* 
gnome_action_manager_make_menu (GnomeActionManager* am,
                                const gchar* group_id,
                                gpointer model, 
                                gpointer view)
{
  Action* a = am_must_get_action(am, group_id);
  
  GtkWidget* menu = action_make_menu(a, model, view);

  return menu;
}

GtkWidget* 
gnome_action_manager_make_toolbar (GnomeActionManager* am,
                                   const gchar* group_id,
                                   gpointer model,
                                   gpointer view,
                                   GtkOrientation orientation)
{
  Action* a = am_must_get_action(am, group_id);

  GtkWidget* toolbar = action_make_toolbar(a, model, view, orientation);

  return toolbar;
}

GtkWidget* 
gnome_action_manager_make_menuitem (GnomeActionManager* am,
                                    const gchar* action_id,
                                    gpointer model,
                                    gpointer view)
{
  Action* a = am_must_get_action(am, action_id);

  GtkWidget* mi = action_make_menuitem(a, model, view);

  return mi;
}

GtkWidget* 
gnome_action_manager_make_button (GnomeActionManager* am,
                                  const gchar* action_id,
                                  gpointer model, 
                                  gpointer view)
{
  Action* a = am_must_get_action(am, action_id);
  
  GtkWidget* button = action_make_button(a, model, view);

  return button;
}

void
gnome_action_manager_fill_from_gnomeuiinfo (GnomeActionManager* am,
                                            GnomeUIInfo* info)
{

}

void 
gnome_action_manager_set_hint_bar(GnomeActionManager* am,
                                  gpointer view, 
                                  GtkWidget* bar)
{
  
  
}

void 
gnome_action_manager_set_history_group(GnomeActionManager* am,
                                       const gchar* group_id,
                                       gpointer view)
{
  View* v = am_get_view(am, view);

  view_set_history_group(v, group_id);
}

void 
gnome_action_manager_forget_view(GnomeActionManager* am,
                                 gpointer view)
{
  am_forget_view(am, view);
}
