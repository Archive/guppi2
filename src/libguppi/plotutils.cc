// -*- C++ -*-

/* 
 * plotutils.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <plotutils.h>

#include <math.h>
#include <string.h>

#include <glib.h>

#include <algorithm>

#include <config.h>

#include "vfont.h"
#include "util.h"

#ifndef M_PI
#define	M_PI		3.14159265358979323846
#endif

#ifdef GNOME_ENABLE_DEBUG
#include <time.h>
static inline void start_time(guint* store)
{
  *store = clock();
}
static inline void check_time(guint* store)
{
  guint now = clock();
  g_debug("%u clocks elapsed", now - *store);
}
#endif

void 
PlotUtil::round_numbers (int goal,
                         double* min, double* max,
                         bool range_locked,
                         const vector<int> & divisors,
                         vector<double> & results)
{  
  g_return_if_fail(goal > 0);
  g_return_if_fail(min != 0);
  g_return_if_fail(max != 0);
  g_return_if_fail(*max >= *min);
  g_return_if_fail(divisors.size() > 0);
  g_return_if_fail(results.empty());

  double width = *max - *min;
  double mag = ceil(log10(width/goal));

  double start_best=0, step_best=1;
  int pts_best=0, error_best=10000;

  vector<int>::const_iterator i = divisors.begin();
  vector<int>::const_iterator e = divisors.end();
  while (i != e) {
    int div=*i;
    double step = pow(10,mag)/div;
    double start = ceil(*min/step)*step;
    int pts = 1+(int)floor((*max-start)/step);

    if (! range_locked) {
      if (fabs(start-step-*min)<step/2) {
	start -= step;
	++pts;
      }
      if (fabs(start+pts*step-*max) < step/2)
	++pts;
    }

    int error = abs(pts-goal);
    if (error < error_best) {
      start_best = start;
      step_best = step;
      pts_best = pts;
      error_best = error;
    }

    ++i;
  }

  if (!range_locked) {
    if (start_best < *min) {
      *min = start_best - step_best/10; // small padding
    }
    if (*max < start_best+(pts_best-1)*step_best) {
      *max = start_best+(pts_best-1)*step_best + step_best/10; // small padding
    }
  }

  for(int i=0; i<pts_best; ++i) {
    double t = start_best + step_best*i;
    results.push_back(t);
  }
}

void 
PlotUtil::canonicalize_rectangle(double* x1, double* y1,
                                 double* x2, double* y2)
{
  g_return_if_fail(x1 != 0);
  g_return_if_fail(y1 != 0);
  g_return_if_fail(x2 != 0);
  g_return_if_fail(y2 != 0);

  if (*x1 > *x2)
    {
      double tmp = *x1;
      *x1 = *x2;
      *x2 = tmp;
    }

  if (*y1 > *y2)
    {
      double tmp = *y1;
      *y1 = *y2;
      *y2 = tmp;
    }
}


void 
PlotUtil::canonicalize_rectangle(int* x1, int* y1,
                                 int* x2, int* y2)
{
  g_return_if_fail(x1 != 0);
  g_return_if_fail(y1 != 0);
  g_return_if_fail(x2 != 0);
  g_return_if_fail(y2 != 0);

  if (*x1 > *x2)
    {
      int tmp = *x1;
      *x1 = *x2;
      *x2 = tmp;
    }

  if (*y1 > *y2)
    {
      int tmp = *y1;
      *y1 = *y2;
      *y2 = tmp;
    }
}

double 
PlotUtil::hypot(double x, double y)
{
#ifndef NEVER_DEFINED_NOPE // HAVE_HYPOT
  return sqrt((x)*(x) + (y)*(y));
#else
  // I was getting weird hangs here.
  return hypot(x, y);
#endif
}

static const gchar* pos_strings[] = {
  N_("South"),
  N_("North"),
  N_("West"),
  N_("East")
};

const gchar* 
PlotUtil::position_string(PositionType pos)
{
  g_return_val_if_fail(pos < PositionTypeEnd, 0);

  return _(pos_strings[pos]);
}

/////////////////////// Sorts

template <class SType>
static inline void
psort_swap(SType* left, SType* right)
{
  SType tmp = *left;
  *left = *right;
  *right = tmp;
}

// The shell sort isn't a total lost cause if the array has lots of 
//  duplicate values; so the qsort bails out to this if necessary.
template <class XType, class YType>
static inline void
parallel_shell_sort(XType* xsorted, YType* ysorted, int N)
{
  // h is our increment

  int h = 1;
  if (N < 14)
    h = 1;
  else 
    {
      while (h < N) 
        h = 3*h + 1;
      
      h /= 3;
      h /= 3;
    }

  while (h > 0) 
    {
      // Insertion sort

      int i = h;
      while (i < N)
        {
          XType x_pivot_value = xsorted[i];
          YType y_pivot_value = ysorted[i];

          g_assert(i >= h);

          int j = i - h;

          while (j >= 0 && xsorted[j] > x_pivot_value)
            {
              g_assert(j+h < N);
              g_assert(j+h >= 0);
              xsorted[j+h] = xsorted[j];
              ysorted[j+h] = ysorted[j];

              j -= h;
            }
          xsorted[j+h] = x_pivot_value;
          ysorted[j+h] = y_pivot_value;
          
          ++i;
        }

      h /= 3;
    }

#ifdef GNOME_ENABLE_DEBUG_SLOW
  if (N > 1)
    {
      guint q = 1;
      XType last_x = xsorted[0];
      while (q < N)
        {
          if (last_x > xsorted[q])
            g_warning("Shell sort: %u out of order!", q);

          last_x = xsorted[q];
          ++q;
        }
    }
#endif
}

// For small sections that qsort doesn't like, this is simpler 
//  and faster than the shell sort
template <class XType, class YType>
static inline void
parallel_insertion_sort(XType* xsorted, YType* ysorted, int N)
{
  // not done yet

#ifdef GNOME_ENABLE_DEBUG_SLOW
  if (N > 1)
    {
      guint q = 1;
      XType last_x = xsorted[0];
      while (q < N)
        {
          if (last_x > xsorted[q])
            g_warning("Insertion sort: %u out of order!", q);

          last_x = xsorted[q];
          ++q;
        }
    }
#endif
}

template <class XType, class YType>
static void parallel_sort(XType* xsorted, YType* ysorted, unsigned int N)
{
  if (N < 2) 
    return;

  // FIXME figure out the max stack size for real.
  // MAX_UINT on 64-bit is 2^64; log-base-2 of 2^64 is 64; we need two ints per 
  //  stack entry; thus the stack must be 128 ints; double it for 128-bit-safety 
  //  and add a few for luck :-)
  // If you are better at math than me and think this is wrong, please fix. :-)
#define PSORT_STACK_SIZE 260
#define PSORT_STACK_SIZE_MINUS_FOUR 256
  unsigned int stck[PSORT_STACK_SIZE];
  unsigned int top = 2;
  
  // The stack contains pairs of indices denoting ranges to sort
  stck[0] = 0;
  stck[1] = N;

  while (top != 0)
    {
      unsigned int strt = stck[top-2];
      unsigned int stop = stck[top-1];

      unsigned int diff = stop - strt;     
      
      if (diff > 3)
        {
          // qsort, baby
          const unsigned int pivot = strt + diff/2;
          const unsigned int last = stop - 1; // remember that stop is one past
      
          // Rearrange hi, lo, and pivot values to be in order; this 
          //  gives us a better pivot and lets us instantly reduce the 
          //  partition size. Idea from glibc
      
          if (xsorted[pivot] < xsorted[strt])
            {
              psort_swap(&xsorted[strt], &xsorted[pivot]);
              psort_swap(&ysorted[strt], &ysorted[pivot]);
            }
      
          if (xsorted[last] < xsorted[pivot])
            {
              psort_swap(&xsorted[last], &xsorted[pivot]);
              psort_swap(&ysorted[last], &ysorted[pivot]);
          
              if (xsorted[pivot] < xsorted[strt])
                {
                  psort_swap(&xsorted[strt], &xsorted[pivot]);
                  psort_swap(&ysorted[strt], &ysorted[pivot]);
                }
            }

          //          g_assert(xsorted[strt] <= xsorted[pivot]);
          // g_assert(xsorted[pivot] <= xsorted[last]);

          // The above ensures the last value is >= pivot so we don't
          // check it again.

          // Move pivot value to front out of the way
          
          psort_swap(&xsorted[strt], &xsorted[pivot]);
          psort_swap(&ysorted[strt], &ysorted[pivot]);
          
          // Partition around pivot value
          const XType pivot_value = xsorted[strt];
          unsigned int lo = strt + 1;
          unsigned int hi = stop - 1; // We know the last value is >= pivot
          
          // Simple swap partitioning - note that the glibc way 
          //  is much faster. 
          
          while (lo < hi)
            {
              while (lo < hi && xsorted[lo] < pivot_value)
                ++lo;
              
              // Now go the other way
              
              while (lo < hi && xsorted[--hi] >= pivot_value)
                ; // empty loop
              
              // Either lo == hi or 
              // xsorted[lo] >= pivot_value && xsorted[hi] < pivot_value
              
#ifdef GNOME_ENABLE_DEBUG_SLOW
              g_assert((lo == hi) || 
                       (xsorted[lo] >= pivot_value && xsorted[hi] < pivot_value));
#endif
              
              if (lo < hi)
                {
                  psort_swap(&xsorted[lo], &xsorted[hi]);
                  psort_swap(&ysorted[lo], &ysorted[hi]);
                  ++lo;
                  // remember that hi remains one past the one to check...
                }           
            }          

          // put pivot back
          --lo; // lo is at the pivot point
          
          // lo is the new pivot point; it won't be sorted again

          // In the worst case (bad pivot), we move forward only 1;
          //  we'll switch to shell sort in that case, since most likely
          //  we have a bunch of duplicate points - also, the stored pivot
          //  value is already in the right spot, so we don't need to 
          //  put it back.
          
          top -= 2; // pop strt/stp

          if (strt == lo)
            {
          
#ifdef GNOME_ENABLE_DEBUG_SLOW
              // Check our invariant
              {
                guint i = strt;
                while (i < stop)
                  {
                    if (i < lo)
                      g_assert(xsorted[i] < xsorted[lo]);
                    else if (i > lo)
                      g_assert(xsorted[i] >= xsorted[lo]);
                
                    ++i;
                  }
              }
#endif

              // We know strt == lo so there's only the second partition left to sort
              parallel_shell_sort(&xsorted[lo+1], &ysorted[lo+1], stop-lo-1);
            }
          else 
            {
              // Put pivot back; not necessary in little_progress case
              psort_swap(&xsorted[lo], &xsorted[strt]);
              psort_swap(&ysorted[lo], &ysorted[strt]);

          
#ifdef GNOME_ENABLE_DEBUG_SLOW
              // Check our invariant
              {
                guint i = strt;
                while (i < stop)
                  {
                    if (i < lo)
                      g_assert(xsorted[i] < xsorted[lo]);
                    else if (i > lo)
                      g_assert(xsorted[i] >= xsorted[lo]);
                
                    ++i;
                  }
              }
#endif

              g_assert(top < PSORT_STACK_SIZE_MINUS_FOUR);

              // if (strt < lo) // already known, since we have progress.

              stck[top] = strt;
              stck[top+1] = lo; // pivot is the limit
              top += 2;
              
              ++lo; // one past pivot
              if (lo < stop)
                {
                  stck[top] = lo;
                  stck[top+1] = stop;
                  top += 2;
                }
            }
        }
      else
        {
          switch (diff)
            {

            case 3:
              {
                const unsigned int pivot = strt + 1;

                --stop;

                if (xsorted[pivot] < xsorted[strt])
                  {
                    psort_swap(&xsorted[strt], &xsorted[pivot]);
                    psort_swap(&ysorted[strt], &ysorted[pivot]);
                  }
      
                if (xsorted[stop] < xsorted[pivot])
                  {
                    psort_swap(&xsorted[stop], &xsorted[pivot]);
                    psort_swap(&ysorted[stop], &ysorted[pivot]);
          
                    if (xsorted[pivot] < xsorted[strt])
                      {
                        psort_swap(&xsorted[strt], &xsorted[pivot]);
                        psort_swap(&ysorted[strt], &ysorted[pivot]);
                      }
                  }

                //          g_assert(xsorted[strt] <= xsorted[pivot]);
                //          g_assert(xsorted[pivot] <= xsorted[stop]);

              }
              break;

            case 2:
              {
                --stop;
                if (xsorted[strt] > xsorted[stop])
                  {
                    psort_swap(&xsorted[strt], &xsorted[stop]);
                    psort_swap(&ysorted[strt], &ysorted[stop]);
                  }
              }
              break;

            default:
              // range of 0 or 1, do nothing
              break;
            }

          // Pop the stack
          top -= 2;
        } // else diff is small
    } // while (top != 0) // i.e. while there's stuff on the stack

#ifdef GNOME_ENABLE_DEBUG
  //  check_time(&timer);

  if (N > 1)
    {
      guint q = 1;
      XType last_x = xsorted[0];
      while (q < N)
        {
          if (last_x > xsorted[q])
            g_warning("%u out of order!", q);

          last_x = xsorted[q];
          ++q;
        }
    }
  //  check_time(&timer);

#endif
}

void
PlotUtil::sort_axes(double* xsorted, double* ysorted, unsigned int N)
{
  parallel_sort(xsorted, ysorted, N);
}

void 
PlotUtil::sort_indices(double* sorted, unsigned int* indices, unsigned int N)
{
  parallel_sort(sorted, indices, N);
}

unsigned int 
PlotUtil::first_ge(const double* values, unsigned int N, double target)
{
  unsigned int begin = 0;
  unsigned int end = N;
  
  while (begin < end)
    {
      // values[0..begin-1] < target
      // values[end...N] > target

      unsigned int guess = (begin + end)/2;

      if (values[guess] < target)
        {
          begin = guess + 1;
        }
      else
        {
          end = guess;
        }
    }
  
  if (begin < N) return begin; // the first element >= xmin
  else return N;
}

void 
PlotUtil::find_range(const double* xvalues, const double* yvalues,
                     unsigned int N,
                     double xmin, double xmax, double ymin, double ymax,
                     unsigned int* start, unsigned int* stop)
{
  assert(xvalues != 0);  
  assert(yvalues != 0);
  assert(start != 0);
  assert(stop != 0);
  assert(xmax >= xmin);
  assert(ymax >= ymin);
  assert(N > 0);

  // First find the closest to xmin
  *start = first_ge(xvalues, N, xmin);

  *stop = first_ge(xvalues, N, xmax);
}

const double PlotUtil::EPSILON = 1e-6;                   

//////////////////// Affine

// An affine represents a matrix, basically like this:
// a[0] a[2] a[4] 
// a[1] a[3] a[5]
//  0    0    1
// To transform a point, you multiply with a point vector:
//   x
//   y
//   1
// For point-transformation purposes then, the last row (0 0 1) can be 
//  ignored; it's implicitly added when composing two affines.
// 
// a[0] thru a[3] are a composition of rotation and scaling;
// a[4] and a[5] are the translation.
//
// a[2] contains the horizontal shear, and a[1] the vertical; but 
// for now we have no support for that.

Affine::Affine()
{
  // Setup identity affine
  a[0] = 1;
  a[1] = 0;
  a[2] = 0;
  a[3] = 1;
  a[4] = 0;
  a[5] = 0;
}

Affine::Affine(const double src[6])
{
  memcpy(a,src,sizeof(a[0])*6);
}

Affine::Affine(double a0,double a1,double a2,double a3,double a4,double a5)
{
  a[0] = a0;
  a[1] = a1;
  a[2] = a2;
  a[3] = a3;
  a[4] = a4;
  a[5] = a5;
}

Affine::Affine(const Affine& src)
{
  if (src.a == a) return;

  memcpy(a,src.a,sizeof(a[0])*6);
}

Affine::~Affine()
{
  // nothing
}

const Affine& 
Affine::operator=(const Affine& src)
{
  if (src.a == a) return *this;

  memcpy(a,src.a,sizeof(a[0])*6);

  return *this;
}

void
Affine::set_identity()
{
  a[0] = 1;
  a[1] = 0;
  a[2] = 0;
  a[3] = 1;
  a[4] = 0;
  a[5] = 0;
}

void 
Affine::set(const double src[6])
{
  if (src == a) return;

  memcpy(a,src,sizeof(a[0])*6);
}

Affine* 
Affine::new_inverted() const
{
  double r_det;
  double dst[6];

  r_det = 1.0 / (a[0] * a[3] - a[1] * a[2]);
  dst[0] = a[3] * r_det;
  dst[1] = -a[1] * r_det;
  dst[2] = -a[2] * r_det;
  dst[3] = a[0] * r_det;
  dst[4] = -a[4] * dst[0] - a[5] * dst[2];
  dst[5] = -a[4] * dst[1] - a[5] * dst[3];

  return new Affine(dst);
}

void 
Affine::compose(const Affine& with)
{
  compose(with.a);
}

void 
Affine::compose(const double with[6])
{
  double d[6];

  d[0] = a[0] * with[0] + a[1] * with[2];
  d[1] = a[0] * with[1] + a[1] * with[3];
  d[2] = a[2] * with[0] + a[3] * with[2];
  d[3] = a[2] * with[1] + a[3] * with[3];
  d[4] = a[4] * with[0] + a[5] * with[2] + with[4];
  d[5] = a[4] * with[1] + a[5] * with[3] + with[5];

  memcpy(a,d,sizeof(a[0])*6);
}

//////////// FIXME All the compose-with-transform functions are flawed because 
//////////// the transform potentially includes a *flip* of the coordinate system.
//////////// I don't think this can be worked in to the Affine framework. ?

void 
Affine::compose(const Transform & xtrans, const Transform & ytrans)
{
  g_warning("Composing a Transform with an Affine is broken");

  // Create scaling/translating matrix with transform stuff
  double s[6];
  s[0] = xtrans.pixels_per_unit();
  s[1] = 0;
  s[2] = 0;
  s[3] = ytrans.pixels_per_unit();
  s[4] = xtrans.translation();
  s[5] = ytrans.translation();

  compose(s);
}

void 
Affine::x_compose(const Transform & xtrans)
{
  g_warning("Composing a Transform with an Affine is broken");

  double s[6];
  s[0] = xtrans.pixels_per_unit();
  s[1] = 0;
  s[2] = 0;
  s[3] = 1;
  s[4] = xtrans.translation();
  s[5] = 0;

  compose(s);
}

void 
Affine::y_compose(const Transform & ytrans)
{
  g_warning("Composing a Transform with an Affine is broken");

  double s[6];
  s[0] = 1;
  s[1] = 0;
  s[2] = 0;
  s[3] = ytrans.pixels_per_unit();
  s[4] = 0;
  s[5] = ytrans.translation();
  
  compose(s);
}


void 
Affine::scale(double sx, double sy)
{
  // Create scaling matrix
  double s[6];
  s[0] = sx;
  s[1] = 0;
  s[2] = 0;
  s[3] = sy;
  s[4] = 0;
  s[5] = 0;

  compose(s);
}

void 
Affine::rotate(double theta)
{
  // Create rotation matrix
  double r[6];
  double s, c;

  s = sin (theta * M_PI / 180.0);
  c = cos (theta * M_PI / 180.0);
  r[0] = c;
  r[1] = s;
  r[2] = -s;
  r[3] = c;
  r[4] = 0;
  r[5] = 0;

  compose(r);
}

void
Affine::translate(double dx, double dy)
{
  double t[6];

  t[0] = 1;
  t[1] = 0;
  t[2] = 0;
  t[3] = 1;
  t[4] = dx;
  t[5] = dy;

  compose(t);
}

void 
Affine::get_transformed_bounds(double* xmin, double* ymin,
                               double* xmax, double* ymax) const
{
  Point nw(*xmin, *ymin);
  Point se(*xmax, *ymax);
  Point ne(*xmax, *ymin);
  Point sw(*xmin, *ymax);

  transform(nw);
  transform(se);
  transform(ne);
  transform(sw);
 
  *xmin = MIN(MIN(nw.x, ne.x), MIN(sw.x, se.x));
  *ymin = MIN(MIN(nw.y, ne.y), MIN(sw.y, se.y));
  *xmax = MAX(MAX(nw.x, ne.x), MAX(sw.x, se.x));
  *ymax = MAX(MAX(nw.y, ne.y), MAX(sw.y, se.y));
}

const double Affine::EPSILON = PlotUtil::EPSILON;

bool 
Affine::rectilinear () const 
{

  return ((fabs (a[1]) < EPSILON && fabs (a[2]) < EPSILON) ||
          (fabs (a[0]) < EPSILON && fabs (a[3]) < EPSILON));
}

///////////////////////////

void 
Axis::set(double start, double stop)
{ 
  start_ = start;
  stop_ = stop;
}

void 
Axis::set_start(double start) 
{ 
  start_ = start; 
}

void 
Axis::set_stop(double stop) 
{ 
  stop_ = stop; 
}

void
Axis::generate_ticks(int nticks, double length)
{
  g_return_if_fail(nticks > 0);

  double min = MIN(start_,stop_);
  double max = MAX(start_, stop_);

  vector<int> divisors;
  divisors.push_back(2);
  divisors.push_back(5);
  divisors.push_back(10);

  vector<double> results;

  PlotUtil::round_numbers (nticks, &min, &max,
                           true, // don't change min/max
                           divisors,
                           results);
  
  // Remove all non-manual ticks
  vector<Tick> preserve;
  vector<Tick>::const_iterator ti = ticks_.begin();
  while (ti != ticks_.end())
    {
      if (ti->manual())
        preserve.push_back(*ti);
      
      ++ti;
    }

  ticks_ = preserve;

  // Note that ticks are not sorted, the manual ones end up at the
  // beginning.

  vector<double>::const_iterator i = results.begin();

  while (i != results.end())
    {
      Tick t;

      t.set_pos(*i);
      t.set_len(length);

      gchar buf[128];
      g_snprintf(buf, 128, "%g", *i);

      t.set_label(buf);

      ticks_.push_back(t);

      ++i;
    }

  // Set defaults for all non-manual ticks
  if (default_tick_font_ != 0)
    {
      vector<Tick>::iterator j = ticks_.begin();
      while (j != ticks_.end())
        {
          if (!j->manual())
            {
              j->set_font(default_tick_font_);
            }
          
          ++j;
        }
    }
}



/////////// Rectangle

void 
Rectangle::move_edges(double inc)
{
  double inc2 = inc*2;

  if (fabs(inc2) > w_)
    {
      x_ += w_/2; // go to center
      w_ = 0.0;
    }
  else 
    {
      x_ -= inc;
      w_ += inc2;
    }

  if (fabs(inc2) > h_)
    {
      y_ += h_/2;
      h_ = 0.0;
    }
  else 
    {
      y_ -= inc;
      h_ += inc2;
    }
}


///////////////////// Tick

Tick::Tick()
  : pos_(0.0), len_(0.0), font_(0),
    in_(false), out_(true), ruled_(false), manual_(false)
{
  
}

Tick::~Tick()
{
  if (font_ != 0)
    if (font_->unref() == 0)
      delete font_;
}

Tick::Tick(const Tick& src)
  : pos_(src.pos_), len_(src.len_),
    color_(src.color_), label_(src.label_),
    font_(src.font_),
    in_(src.in_), out_(src.out_), 
    ruled_(src.ruled_), manual_(src.manual_)
{  
  if (font_ != 0)
    font_->ref();
}

Tick& 
Tick::operator=(const Tick& src)
{
  if (&src == this)
    return *this;

  pos_ = src.pos_;
  len_ = src.len_;
  color_ = src.color_;
  label_ = src.label_;
  in_ = src.in_;
  out_ = src.out_;
  ruled_ = src.ruled_;
  manual_ = src.manual_;

  if (font_ != 0)
    if (font_->unref() == 0)
      delete font_;
  
  font_ = src.font_;
  
  if (font_ != 0)
    font_->ref();

  return *this;
}

void 
Tick::set_font(VFont* vfont)
{
  if (vfont == font_)
    return;

  if (vfont != 0)
    {
      vfont->ref();
    }

  if (font_ != 0)
    {
      if (font_->unref() == 0)
        delete font_;
      
      font_ = 0;
    }

  font_ = vfont;
}

VFont* 
Tick::font() const
{
  return font_;
}

////////////////// Axis

Axis::Axis(const Axis& src)
  : start_(src.start_), stop_(src.stop_),
    ticks_(src.ticks_), label_(src.label_),
    label_color_(src.label_color_),
    line_color_(src.line_color_),
    pos_(src.pos_),
    label_font_(src.label_font_),
    default_tick_font_(src.default_tick_font_),
    line_(src.line_), 
    hidden_(src.hidden_), label_hidden_(src.label_hidden_)
{
  if (label_font_ != 0)
    label_font_->ref();

  if (default_tick_font_ != 0)
    default_tick_font_->ref();
}

Axis& 
Axis::operator=(const Axis& src)
{
  if (&src == this)
    return *this;

  start_ = src.start_;
  stop_ = src.stop_;
  ticks_ = src.ticks_;
  label_ = src.label_;
  label_color_ = src.label_color_;
  line_color_ = src.line_color_;
  pos_ = src.pos_;
  line_ = src.line_;
  hidden_ = src.hidden_;
  label_hidden_ = src.label_hidden_;

  if (label_font_ != 0)
    if (label_font_->unref() == 0)
      delete label_font_;

  label_font_ = src.label_font_;
  
  if (label_font_ != 0)
    label_font_->ref();

  
  if (default_tick_font_ != 0)
    if (default_tick_font_->unref() == 0)
      delete default_tick_font_;

  default_tick_font_ = src.default_tick_font_;
  
  if (default_tick_font_ != 0)
    default_tick_font_->ref();  

  return *this;
}

Axis::~Axis()
{
  if (label_font_ != 0)
    {
      if (label_font_->unref() == 0)
        delete label_font_;
    }

  if (default_tick_font_ != 0)
    {
      if (default_tick_font_->unref() == 0)
        delete default_tick_font_;
    }
}

VFont* 
Axis::label_font() const
{
  return label_font_;
}

void 
Axis::set_label_font(VFont* vfont)
{
  if (vfont == label_font_)
    return;

  if (vfont != 0)
    {
      vfont->ref();
    }

  if (label_font_ != 0)
    {
      if (label_font_->unref() == 0)
        delete label_font_;
      
      label_font_ = 0;
    }

  label_font_ = vfont;
}

VFont* 
Axis::default_tick_font() const
{
  return default_tick_font_;
}

void 
Axis::set_default_tick_font(VFont* vfont)
{
  if (vfont == default_tick_font_)
    return;

  if (vfont != 0)
    {
      vfont->ref();
    }

  if (default_tick_font_ != 0)
    {
      if (default_tick_font_->unref() == 0)
        delete default_tick_font_;
      
      default_tick_font_ = 0;
    }

  default_tick_font_ = vfont;
}

