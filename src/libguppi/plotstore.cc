// -*- C++ -*-

/* 
 * plotstore.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plotstore.h"

#include "tool.h"
#include "toolbox.h"
#include "menuentry.h"

#include <map>

////////// FIXME menuentries, tools, and actions are all handled pretty much 
////////// the same with cut-and-paste code. Someone ambitious could fix 
////////// this...

// Map type / Value type templatized function

template <class MK, class MV>  
void 
delete_func(gpointer key, gpointer value, gpointer data)
{
  map<MK,MV>* m = static_cast<map<MK,MV>*>(value);

  map<MK,MV>::iterator i = m->begin();
  while (i != m->end())
    {
      MV v = i->second;
      if (v && v->unref() == 0)
        delete v;

      ++i;
    }

  delete m;

  // Free the key string (plot ID)
  g_free(key);
}

typedef map<string,Tool*> ToolMap;
typedef map<string,Action*> ActionMap;
typedef map<string,MenuEntry*> MenuEntryMap;

PlotStore::PlotStore()
{
  tools_   = g_hash_table_new(g_str_hash, g_str_equal);
  actions_ = g_hash_table_new(g_str_hash, g_str_equal);
  menuentries_ = g_hash_table_new(g_str_hash, g_str_equal);
}

// This is an egcs bug workaround; if you put delete_func<whatever>
//  in the args to g_hash_table_foreach(), it gets all upset.
static GHFunc tdfunc = delete_func<string,Tool*>;
static GHFunc adfunc = delete_func<string,Action*>;
static GHFunc medfunc = delete_func<string,MenuEntry*>;

PlotStore::~PlotStore()
{
  g_hash_table_foreach(tools_, tdfunc, NULL);
  g_hash_table_foreach(actions_, adfunc, NULL);
  g_hash_table_foreach(menuentries_, medfunc, NULL);

  g_hash_table_destroy(tools_);
  g_hash_table_destroy(actions_);
  g_hash_table_destroy(menuentries_);

  map<string,ToolBox*>::iterator tbi = toolboxes_.begin();
  while (tbi != toolboxes_.end())
    {
      delete tbi->second;
      ++tbi;
    }

  map<string,Tool*>::iterator cti = current_tools_.begin();
  while (cti != current_tools_.end())
    {
      if (cti->second->unref() == 0)
        delete cti->second;
      ++cti;
    }
}

void 
PlotStore::add_plot(const string& id)
{
  plots_.insert(id);

  ToolMap* tm = new ToolMap;
  g_hash_table_insert(tools_, g_strdup(id.c_str()), tm);

  ActionMap* am = new ActionMap;
  g_hash_table_insert(actions_, g_strdup(id.c_str()), am);

  MenuEntryMap* mem = new MenuEntryMap;
  g_hash_table_insert(menuentries_, g_strdup(id.c_str()), mem);
}

void 
PlotStore::remove_plot(const string& id)
{
  set<string>::iterator i = plots_.find(id);

  g_return_if_fail(i != plots_.end());

  if (i != plots_.end())
    {
      plots_.erase(i);

      map<string,Tool*>::iterator ti = current_tools_.find(id);
      if (ti != current_tools_.end())
        {
          if (ti->second->unref() == 0)
            delete ti->second;

          current_tools_.erase(ti);
        }
      // OK for there to be no current tool

      map<string,ToolBox*>::iterator tbi = toolboxes_.find(id);
      if (tbi != toolboxes_.end())
        {
          delete tbi->second;
          toolboxes_.erase(tbi);
        }
      // OK if there's no toolbox for a tool.

      gpointer value = 0;
      gpointer key = 0;

      if (g_hash_table_lookup_extended(actions_, id.c_str(), &key, &value))
        {
          g_return_if_fail(value != 0);
          g_return_if_fail(key != 0);

          g_hash_table_remove(actions_, key);

          delete_func<string,Action*>(key, value, 0);
        }
#ifdef GNOME_ENABLE_DEBUG
      else 
        {
          g_warning("Plot didn't have an actions hash entry!");
        }
#endif

      key = value = 0;

      if (g_hash_table_lookup_extended(tools_, id.c_str(), &key, &value))
        {
          g_return_if_fail(value != 0);
          g_return_if_fail(key != 0);
          
          g_hash_table_remove(tools_, key);

          delete_func<string,Tool*>(key, value, 0);
        }
#ifdef GNOME_ENABLE_DEBUG
      else 
        {
          g_warning("Plot didn't have an actions hash entry!");
        }
#endif      
      key = value = 0;

      if (g_hash_table_lookup_extended(menuentries_, id.c_str(), &key, &value))
        {
          g_return_if_fail(value != 0);
          g_return_if_fail(key != 0);
          
          g_hash_table_remove(menuentries_, key);

          delete_func<string,MenuEntry*>(key, value, 0);
        }
#ifdef GNOME_ENABLE_DEBUG
      else 
        {
          g_warning("Plot didn't have a menuentries hash entry!");
        }
#endif      
    }
}
  
bool 
PlotStore::plot_exists(const string& id) const
{
  set<string>::iterator i = plots_.find(id);

  return (i != plots_.end()); 
}


void 
PlotStore::add_tool(const string& id, Tool* t)
{
  g_return_if_fail(t != 0);

  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(tools_, id.c_str(), &key, &value))
    {
      g_return_if_fail(value != 0);
      g_return_if_fail(key != 0);
      
      
      ToolMap* tm = static_cast<ToolMap*>(value);
      
      t->ref();

      (*tm)[t->id()] = t;
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a tool hash entry!");
    }
#endif
}

void 
PlotStore::remove_tool(const string& id, const string& toolid)
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(tools_, id.c_str(), &key, &value))
    {
      g_return_if_fail(value != 0);
      g_return_if_fail(key != 0);
      
      ToolMap* tm = static_cast<ToolMap*>(value);

      ToolMap::iterator i = tm->find(toolid);
      
      if (i != tm->end())
        {
          Tool* t = i->second;

          if (t->unref() == 0)
            delete t;

          tm->erase(i);
        }
#ifdef GNOME_ENABLE_DEBUG
      else 
        {
          g_warning("No tool found for id!");
        }
#endif
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a tool hash entry!");
    }
#endif
}

bool 
PlotStore::tool_exists(const string& id, const string& toolid) const
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(tools_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, false);
      g_return_val_if_fail(key != 0, false);
      
      ToolMap* tm = static_cast<ToolMap*>(value);

      ToolMap::iterator i = tm->find(toolid);
      
      return (i != tm->end());
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a tool hash entry!");
      return false;
    }
#endif
}

Tool* 
PlotStore::get_tool(const string& id, const string& toolid)
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(tools_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, 0);
      g_return_val_if_fail(key != 0, 0);
      
      ToolMap* tm = static_cast<ToolMap*>(value);

      ToolMap::iterator i = tm->find(toolid);
      
      if (i != tm->end())
        {
          return i->second;
        }
      else 
        {
          return 0;
        }
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a tool hash entry!");
      return 0;
    }
#endif
}

void
PlotStore::add_action(const string& id, Action* a)
{
  g_return_if_fail(a != 0);

  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(actions_, id.c_str(), &key, &value))
    {
      g_return_if_fail(value != 0);
      g_return_if_fail(key != 0);
            
      ActionMap* am = static_cast<ActionMap*>(value);

      a->ref();

      (*am)[a->id()] = a;
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have an actions hash entry!");
    }
#endif
}
  
void 
PlotStore::remove_action(const string& id, const string& actionid)
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(actions_, id.c_str(), &key, &value))
    {
      g_return_if_fail(value != 0);
      g_return_if_fail(key != 0);
            
      ActionMap* am = static_cast<ActionMap*>(value);

      ActionMap::iterator i = am->find(actionid);

      if (i != am->end())
        {
          Action* a = i->second;

          if (a->unref() == 0)
            delete a;

          am->erase(i);
        }
#ifdef GNOME_ENABLE_DEBUG
      else 
        {
          g_warning("No action found for id!");
        }
#endif
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have an actions hash entry!");
    }
#endif
}

bool 
PlotStore::action_exists(const string& id, const string& actionid) const
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(actions_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, false);
      g_return_val_if_fail(key != 0, false);
            
      ActionMap* am = static_cast<ActionMap*>(value);

      ActionMap::iterator i = am->find(actionid);

      return (i != am->end());
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have an actions hash entry!");
      return false;
    }
#endif
}

Action* 
PlotStore::get_action(const string& id, const string& actionid)
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(actions_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, 0);
      g_return_val_if_fail(key != 0, 0);
            
      ActionMap* am = static_cast<ActionMap*>(value);

      ActionMap::iterator i = am->find(actionid);
      
      if (i != am->end())
        {
          return i->second;
        }
      else 
        {
          return 0;
        }
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have an actions hash entry!");
      return 0;
    }
#endif
}

vector<string> 
PlotStore::plot_types() const
{
  vector<string> retval;

  set<string>::const_iterator i = plots_.begin();
  while (i != plots_.end())
    {
      retval.push_back(*i);

      ++i;
    }
  
  return retval;
}

vector<Tool*> 
PlotStore::tools(const string& id) const
{
  vector<Tool*> retval;

  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(tools_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, retval);
      g_return_val_if_fail(key != 0, retval);
            
      ToolMap* tm = static_cast<ToolMap*>(value);

      ToolMap::iterator i = tm->begin();
      
      while (i != tm->end())
        {
          retval.push_back(i->second);
          ++i;
        }

      return retval;
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have an tool hash entry!");
      
      return retval;
    }
#endif
}

vector<Action*> 
PlotStore::actions(const string& id) const
{
  vector<Action*> retval;

  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(actions_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, retval);
      g_return_val_if_fail(key != 0, retval);
            
      ActionMap* am = static_cast<ActionMap*>(value);

      ActionMap::iterator i = am->begin();
      
      while (i != am->end())
        {
          retval.push_back(i->second);
          ++i;
        }

      return retval;
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have an actions hash entry!");
      
      return retval;
    }
#endif
}


void 
PlotStore::set_current_tool(const string& id, Tool* t)
{
#ifdef GNOME_ENABLE_DEBUG
  g_return_if_fail(plot_exists(id));

  if (t != 0)
    {
      g_return_if_fail(tool_exists(id, t->id()));
    }
#endif

  map<string,Tool*>::iterator i = current_tools_.find(id);

  if (i != current_tools_.end())
    {
      if (i->second == t)
        return; // no change
      else 
        {
          if (i->second->unref() == 0)
            delete i->second;
            
          current_tools_.erase(i);
        }
    }

  // Note that we don't put 0's in the map
  if (t != 0)
    {
      t->ref();
      current_tools_[id] = t;

      g_debug("Current tool for %s set to %s", id.c_str(), t->name().c_str());
    }
}

Tool* 
PlotStore::get_current_tool(const string& id)
{
  g_return_val_if_fail(plot_exists(id), 0);

  map<string,Tool*>::iterator i = current_tools_.find(id);

  if (i != current_tools_.end())
    return i->second;
  else 
    return 0;
}

void 
PlotStore::show_toolbox(const string& id)
{
  g_return_if_fail(plot_exists(id));

  map<string,ToolBox*>::iterator i = toolboxes_.find(id);
  
  if (i != toolboxes_.end())
    i->second->show();
  else 
    {
      ToolBox* tb = new ToolBox(id);
      
      toolboxes_[id] = tb;

      tb->show();
    }
}

/////////////////////////

void 
PlotStore::add_menuentry(const string& id, MenuEntry* me)
{
  g_return_if_fail(me != 0);

  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(menuentries_, id.c_str(), &key, &value))
    {
      g_return_if_fail(value != 0);
      g_return_if_fail(key != 0);      
      
      MenuEntryMap* mem = static_cast<MenuEntryMap*>(value);
      
      me->ref();

      (*mem)[me->id()] = me;
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a menuentry hash entry!");
    }
#endif
}

void 
PlotStore::remove_menuentry(const string& id, const string& menuentryid)
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(menuentries_, id.c_str(), &key, &value))
    {
      g_return_if_fail(value != 0);
      g_return_if_fail(key != 0);
      
      MenuEntryMap* mem = static_cast<MenuEntryMap*>(value);

      MenuEntryMap::iterator i = mem->find(menuentryid);
      
      if (i != mem->end())
        {
          MenuEntry*me = i->second;

          if (me->unref() == 0)
            delete me;

          mem->erase(i);
        }
#ifdef GNOME_ENABLE_DEBUG
      else 
        {
          g_warning("No menuentry found for id!");
        }
#endif
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a menuentry hash entry!");
    }
#endif
}

bool 
PlotStore::menuentry_exists(const string& id, const string& menuentryid) const
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(menuentries_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, false);
      g_return_val_if_fail(key != 0, false);
      
      MenuEntryMap* mem = static_cast<MenuEntryMap*>(value);

      MenuEntryMap::iterator i = mem->find(menuentryid);
      
      return (i != mem->end());
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a menuentry hash entry!");
      return false;
    }
#endif
}

MenuEntry* 
PlotStore::get_menuentry(const string& id, const string& menuentryid)
{
  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(menuentries_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, 0);
      g_return_val_if_fail(key != 0, 0);
      
      MenuEntryMap* mem = static_cast<MenuEntryMap*>(value);

      MenuEntryMap::iterator i = mem->find(menuentryid);
      
      if (i != mem->end())
        {
          return i->second;
        }
      else 
        {
          return 0;
        }
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a menuentry hash entry!");
      return 0;
    }
#endif
}

vector<MenuEntry*> 
PlotStore::menuentries(const string& id) const
{
  vector<MenuEntry*> retval;

  gpointer key = 0, value = 0;
  
  if (g_hash_table_lookup_extended(menuentries_, id.c_str(), &key, &value))
    {
      g_return_val_if_fail(value != 0, retval);
      g_return_val_if_fail(key != 0, retval);
            
      MenuEntryMap* mem = static_cast<MenuEntryMap*>(value);

      MenuEntryMap::iterator i = mem->begin();
      
      while (i != mem->end())
        {
          retval.push_back(i->second);
          ++i;
        }

      return retval;
    }
#ifdef GNOME_ENABLE_DEBUG
  else 
    {
      g_warning("Plot didn't have a menuentry hash entry!");
      
      return retval;
    }
#endif
}

