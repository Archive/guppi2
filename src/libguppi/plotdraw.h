// -*- C++ -*-

/* 
 * plotdraw.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTDRAW_H
#define GUPPI_PLOTDRAW_H

#include "rgbdraw.h"
#include "plotutils.h"

class XyScatter;
class XyPriceBars;

// Higher-level drawing stuff (draws to RGB buffer using the rgbdraw.h 
//  primitives)

// I want to migrate drawing code in here, so all the backends can use 
//  RGB buffers and use the same drawing routines.

// Honors the line thickness of the context
void guppi_draw_price_bar(RGB& rgb, const RGB::Context& ctx,
                          gint time, gint open, gint hi, gint lo, gint close);

void guppi_draw_price_bar_no_close(RGB& rgb, const RGB::Context& ctx,
                                   gint time, gint open, gint hi, gint lo);

void guppi_draw_price_bar_no_open(RGB& rgb, const RGB::Context& ctx,
                                   gint time, gint hi, gint lo, gint close);

void guppi_draw_price_bar_no_open_or_close(RGB& rgb, const RGB::Context& ctx,
                                           gint time, gint hi, gint lo);



void guppi_draw_scatter(XyScatter* layer, 
                        RGB& rgb, const Affine& i2c,
                        const Transform& xtrans, 
                        const Transform& ytrans,
                        double pixels_per_unit,
                        gint rgb_x0, gint rgb_y0);

void guppi_draw_pricebars(XyPriceBars* layer, 
                          RGB& rgb, const Affine& i2c,
                          const Transform& xtrans, 
                          const Transform& ytrans,
                          double pixels_per_unit,
                          gint rgb_x0, gint rgb_y0);


#endif
