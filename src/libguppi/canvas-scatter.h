// -*- C++ -*-

/* 
 * canvas-scatter.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_CANVASSCATTER_H
#define GUPPI_CANVASSCATTER_H

#include <libgnomeui/gnome-canvas.h>

#include "plotutils.h"
#include "xyplotstate.h"

// there is no reason to maintain strict C in here, but keep C++-isms
//  to a minimum just in case.

#define GUPPI_TYPE_SCATTER            (guppi_scatter_get_type ())
#define GUPPI_SCATTER(obj)            (GTK_CHECK_CAST ((obj), GUPPI_TYPE_SCATTER, GuppiScatter))
#define GUPPI_SCATTER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GUPPI_TYPE_SCATTER, GuppiScatterClass))
#define GUPPI_IS_SCATTER(obj)         (GTK_CHECK_TYPE ((obj), GUPPI_TYPE_SCATTER))
#define GUPPI_IS_SCATTER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GUPPI_TYPE_SCATTER))

struct _GuppiScatter;

// This is a callback used to retrieve the info for various operations.
// It used to retrieve more than the ScatterPlotState, 
// that's why it's a callback. Pointless now.
typedef void (*ScatterInfoFunc)(struct _GuppiScatter* gs, 
                                gpointer user_data, 
                                XyPlotState** state,
                                XyScatter** layer);

class PointTiles;

typedef struct _GuppiScatter GuppiScatter;
typedef struct _GuppiScatterClass GuppiScatterClass;

struct _GuppiScatter {
  GnomeCanvasItem item;
  
  ScatterInfoFunc get_info;
  gpointer info_data;

  // Since we compute the rectangle to render in the render function,
  //  we have to store this in update().
  double a[6];
};

struct _GuppiScatterClass {
  GnomeCanvasItemClass parent_class;
  
  
};


/* Standard Gtk function */
GtkType guppi_scatter_get_type (void);

void guppi_scatter_set_info_func(GuppiScatter* gs, 
                                 ScatterInfoFunc func,
                                 gpointer user_data);


#endif
