// -*- C++ -*-

/* 
 * datagroup.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATAGROUP_H
#define GUPPI_DATAGROUP_H

#include "data.h"
#include <string>
#include <vector>

class DataGroupSpec {
public:
  DataGroupSpec();
  ~DataGroupSpec();

  guint n_slots() const { return slot_types_.size(); }

  Data::DataType slot_type(guint slot) const { return slot_types_[slot]; }
  const string& slot_name(guint slot) const { return slot_names_[slot]; }

  // user-visible name
  const string& name() const { return name_; }

  // Unique identifier
  Data::DataType id() const { return type_id_; }

  void add_slot(Data::DataType type, const string& name);
  void set_name(const string& name);

  guint ref() { return rc_.ref(); } 
  guint unref() { return rc_.unref(); }

  // Must be locked before you can create a DataGroup following the spec;
  // no way to unlock once locked.
  void lock() { locked_ = true; }

  bool locked() const { return locked_; }

private:
  RC rc_;
  bool locked_;
  vector<Data::DataType> slot_types_;
  vector<string> slot_names_;
  string name_;
  Data::DataType type_id_;
};

class DataGroup : public Data {
public:
  DataGroup(DataGroupSpec* spec);
  virtual ~DataGroup();

  const DataGroupSpec* spec() const { return spec_; }

  void set_data(guint slot, Data* d);

  // returns 0 for none
  Data* get_data(guint slot);
  const Data* get_data(guint slot) const;
 
  virtual gsize size() const;

  virtual const string& name() const;

  virtual void set_name(const string & name);

  virtual xmlNodePtr xml(xmlNodePtr parent) const;

  virtual gsize byte_size(); // used in Scheme bindings

private:
  DataGroupSpec* spec_;
  string name_;
  vector<Data*> slot_contents_;

};



#endif
