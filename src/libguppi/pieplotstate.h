// -*- C++ -*-

/* 
 * pieplotstate.h 
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PIEPLOTSTATE_H
#define GUPPI_PIEPLOTSTATE_H

#include "data.h"
#include "datastore.h"
#include "plotutils.h"
#include "plotstate.h"

class ScalarData;
class PiePlotState;
class LabelData;

class Slice {
public:

  Slice(PiePlotState * s) :
    state_(s), index_(0), posX_(50.0), posY_(50.0),
    radius_(20.0), width_(10.0), start_(45.0), color_(255,0,0),
    labelX_(0), labelY_(0),
    labelPos_(50.0), labelRadius_(20.0 * 1.2)
    { }

  ~Slice() {}

  // Position of slice center 
  double posX() const { return posX_; }
  double posY() const { return posY_; }

  // Position of label center 
  double labelX() const { return labelX_; }
  double labelY() const { return labelY_; }

  // Properties of slice
  double start() const { return start_; }
  double width() const { return width_; }
  double radius() const { return radius_; }

  // Properties of label
  const string& label() const { return label_; }
  double labelPos() const { return labelPos_; }
  double labelRadius() const { return labelRadius_; }

  const Color & color() const { return color_; }

  // Setting should only be done by PiePlotState
  void set_color(const Color & c) { color_ = c; }
  void set_index(guint i) { index_ = i; }
  void set_posX(double x) { posX_ = x; }
  void set_posY(double y) { posY_ = y; }
  void set_label(const string& l) { label_ = l; }
  void set_labelX(double x) { labelX_ = x; }
  void set_labelY(double y) { labelY_ = y; }

  void set_width(double w) {
    width_ = w;
    // TODO: flexible label position
    labelPos_ = start_ + (width_ ? width_ / 2.0 : 0);
  }
  void set_start(double s) {
    start_ = s;
    // TODO: flexible label position
    labelPos_ = start_ + (width_ ? width_ / 2.0 : 0);
  }
  void set_radius(double r) {
    radius_ = r;
    labelRadius_ = r * 1.2; // TODO: flexible distance for labels
  }

private:

  PiePlotState * state_;

  guint index_;
  double posX_, posY_;
  double radius_;
  double width_;	// angle
  double start_;	// angle
  Color color_;

  //  label:
  string label_;
  double labelX_, labelY_; // center of text
  
  double labelPos_;	// angle
  double labelRadius_;
};

class PiePlotState : public PlotState, public Data::View {
public:

  PiePlotState(DataStore * ds);
  virtual ~PiePlotState();

  void set_data(Data * d);
  ScalarData* data();
  
  void set_labels(Data* d);
  LabelData* labels(); 

  DataStore* store() { return store_; }

  typedef vector<Slice>::iterator iterator;
  typedef vector<Slice>::const_iterator const_iterator;

  iterator begin() { return slices_.begin(); }
  iterator end() { return slices_.end(); }
  const_iterator begin() const { return slices_.begin(); }
  const_iterator end() const { return slices_.end(); }

  gsize size() { return slices_.size(); }

  const Color & background() const { return background_; }

  void set_background(const Color & c) { 
    background_ = c; 
    state_model.background_changed(this, background_); 
  }

  const string& name() const;
  void set_name(const string & name);

  // Whether to automatically set the name when the data changes
  bool name_from_data() const;
  void set_name_from_data(bool setting);

  // Width/Height are in display coords (e.g. canvas item coords)
  double width() const { return width_; }
  double height() const { return height_; }

  void set_width(double w); 
  void set_height(double h);

  void size_request(double* w, double* h);

  const Rectangle & plot_rect() const;

  // Data::View

  virtual void change_values(Data* d);

  virtual void change_values(Data* d, const vector<guint> & which);

  virtual void change_name(Data* d, const string & name);

  // ::View

  virtual void destroy_model();

  class View : public ::View {
  public:

    virtual void change_slices(PiePlotState* state) = 0;
    virtual void change_data(PiePlotState* state, Data* data) = 0;
    virtual void change_background(PiePlotState* state, const Color & bg) = 0;
    virtual void change_size(PiePlotState* state, double width, double height) = 0;
    virtual void change_name(PiePlotState* state, const string& name) = 0;
  };

  class Model : public ::Model<PiePlotState::View*> {
  public:

    void slices_changed(PiePlotState* state) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
        (*i)->change_slices(state);
        ++i;
      }
      unlock_views();
    }

    void data_changed(PiePlotState* state, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
        (*i)->change_data(state,d);
        ++i;
      }
      unlock_views();
    }

    void background_changed(PiePlotState* state, const Color & bg) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
        (*i)->change_background(state,bg);
        ++i;
      }
      unlock_views();
    }

    void size_changed(PiePlotState* state, double width, double height) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
        (*i)->change_size(state,width,height);
        ++i;
      }
      unlock_views();
    }

    void name_changed(PiePlotState* state, const string & name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
        (*i)->change_name(state,name);
        ++i;
      }
      unlock_views();
    }
  };

  Model state_model;  

private:

  ScalarData * data_;
  LabelData* labels_;

  DataStore * store_;
  vector<Slice> slices_;

  double width_;
  double height_;

  Color background_;

  Rectangle plot_rect_;

  string name_;

  bool set_name_from_data_;

  void rebuild_slices();
  void grab_data(Data* d);
  void release_data();
  void grab_labels(Data* d);
  void release_labels();
  void recalc_layout();
  void update_name_from_data();
};

#endif
