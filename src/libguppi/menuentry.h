// -*- C++ -*-

/* 
 * menuentry.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_MENUENTRY_H
#define GUPPI_MENUENTRY_H

#include <string>

#include "refcount.h"

class Plot;

// Part of the Gnome frontend

// Describes a menu item

class MenuEntry {
public:
  static const guint32 DEFAULT_TYPEMAGIC;
  MenuEntry(const char* id, const char* name, const char* tooltip, 
            guint32 typemagic = DEFAULT_TYPEMAGIC)
    : typemagic_(typemagic), id_(id), name_(name), tooltip_(tooltip) {}
  virtual ~MenuEntry() {}

  const string& id() const { return id_; }

  // user-visible name of the menu item
  const string& name() const { return name_; }
  
  // tooltip describing the menu item
  const string& tooltip() const { return tooltip_; }

  const guint32 typemagic() const { return typemagic_; }

  // What to do if the menu item is selected.  For menu items
  // displayed on the plot popup, p is the corresponding plot; for
  // other menu items, p will be NULL (lame cruft - please fix by
  // making the Plot case an instance of a more general case rather
  // than vice versa)
  virtual void invoke(Plot* p) = 0;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

private:
  guint32 typemagic_; /* the scheme bindings need this to distinguish
                       * which entries contain a SCM object to be marked during
                       * mark-and-sweep 
                       */
  const string id_;
  const string name_;
  const string tooltip_;
  
  RC rc_;
};

#endif
