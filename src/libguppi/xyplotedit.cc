// -*- C++ -*-

/* 
 * xyplotedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xyplotedit.h"
#include "scalardata.h"
#include "typeregistry.h"
#include "datagroup.h"

#define OUR_PAD 3

////////// XyLayerEdit

class XyLayerEdit : public XyLayer::View
{
public:
  XyLayerEdit(XyLayer* layer, 
              XyPlotState* state) 
    : layer_(layer), state_(state), widget_(0), 
      layer_widget_(0), name_label_(0),
      x_axis_(0), y_axis_(0), 
      hidden_toggle_(0), masked_toggle_(0),
      remove_button_(0), raise_button_(0), lower_button_(0)
    {
      layer_->ref();
      layer_->layer_model.add_view(this);
    }
  virtual ~XyLayerEdit() 
    {
      layer_->layer_model.remove_view(this);
      if (layer_->unref() == 0)
        delete layer_;
    }

  virtual GtkWidget* widget() { 
    ensure_top_widget();
    return widget_; 
  };

  virtual GtkWidget* name_label() {
    if (name_label_ == 0)
      {
        name_label_ = gtk_label_new(layer_->name().c_str());
        gtk_widget_show(name_label_);
      }
    return name_label_;
  }

  void forget_widget() {
    zero_widget_pointers();
  }

  
  // XyLayer::View

  virtual void change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_hidden(XyLayer* layer,
                             bool hidden);

  virtual void change_name(XyLayer* layer, 
                           const string& name);

  virtual void change_masked(XyLayer* layer, bool maskedness);

  // ::View
  virtual void destroy_model();

protected:
  XyLayer* layer_;
  XyPlotState* state_;
  GtkWidget* widget_;
  GtkWidget* layer_widget_;
  GtkWidget* name_label_;

  void ensure_layer_widget(); 

  virtual void ensure_top_widget();

  virtual void zero_widget_pointers() {
    widget_ = 0;
    layer_widget_ = 0;
    x_axis_ = 0;
    y_axis_ = 0;
    hidden_toggle_ = 0;
    masked_toggle_ = 0;
    remove_button_ = 0;
    raise_button_ = 0;
    lower_button_ = 0;
    name_label_ = 0;
    axis_items_.clear();
  }

private:
  GtkWidget* x_axis_;
  GtkWidget* y_axis_;
  GtkWidget* hidden_toggle_;
  GtkWidget* masked_toggle_;
  GtkWidget* remove_button_;
  GtkWidget* raise_button_;
  GtkWidget* lower_button_;

  void make_axis_option(const gchar* label, 
                        PlotUtil::PositionType first, 
                        PlotUtil::PositionType second, 
                        GtkWidget** top,
                        GtkWidget** option);

  static void toggle_hidden_cb(GtkWidget* w, gpointer data);
  static void toggle_masked_cb(GtkWidget* w, gpointer data);
  
  map<GtkWidget*,PlotUtil::PositionType> axis_items_;

  static void activate_axisitem_cb(GtkWidget* w, gpointer data);


  static void remove_cb(GtkWidget* w, gpointer data);
  static void raise_cb(GtkWidget* w, gpointer data);
  static void lower_cb(GtkWidget* w, gpointer data);
};

void
XyLayerEdit::make_axis_option(const gchar* text, 
                              PlotUtil::PositionType first, 
                              PlotUtil::PositionType second, 
                              GtkWidget** top,
                              GtkWidget** option)
{  
  GtkWidget* hbox;
  GtkWidget* label;
  GtkWidget* w;
  GtkWidget* menu;
  GtkWidget* mi;

  hbox = gtk_hbox_new(FALSE, OUR_PAD);
  label = gtk_label_new(text);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  w = gtk_option_menu_new();
  menu = gtk_menu_new();

  mi = gtk_menu_item_new_with_label(PlotUtil::position_string(first));

  gtk_menu_append(GTK_MENU(menu), mi);
  axis_items_[mi] = first;
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
                     GTK_SIGNAL_FUNC(activate_axisitem_cb),
                     this);

  mi = gtk_menu_item_new_with_label(PlotUtil::position_string(second));

  gtk_menu_append(GTK_MENU(menu), mi);
  axis_items_[mi] = second;
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
                     GTK_SIGNAL_FUNC(activate_axisitem_cb),
                     this);

  gtk_option_menu_set_menu(GTK_OPTION_MENU(w), menu);

  gtk_box_pack_start(GTK_BOX(hbox), w, TRUE, TRUE, 0);  

  gtk_widget_show_all(hbox);

  *top = hbox;
  *option = w;
}

void
XyLayerEdit::ensure_top_widget()
{
  if (widget_ != 0)
    return;

  widget_ = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

  ensure_layer_widget();

  gtk_box_pack_start(GTK_BOX(widget_), layer_widget_,
                     FALSE, FALSE, 0);

  gtk_widget_show_all(widget_);
}

void
XyLayerEdit::ensure_layer_widget()
{
  if (layer_widget_ != 0)
    return;

  axis_items_.clear(); // should be superfluous, but just as paranoia...

  layer_widget_ = gtk_frame_new(_("General"));

  GtkWidget* vbox = gtk_vbox_new(FALSE, OUR_PAD);
  
  ///

  GtkWidget* top = 0;

  top = gtk_hbox_new(FALSE, OUR_PAD);

  gtk_box_pack_start(GTK_BOX(vbox), top, FALSE, FALSE, OUR_PAD);

  remove_button_ = gtk_button_new_with_label(_("Remove"));
  
  gtk_signal_connect(GTK_OBJECT(remove_button_), "clicked",
                     GTK_SIGNAL_FUNC(remove_cb),
                     this);

  gtk_box_pack_start(GTK_BOX(top), remove_button_, FALSE, FALSE, OUR_PAD);

  raise_button_ = gtk_button_new_with_label(_("Raise"));

  gtk_signal_connect(GTK_OBJECT(raise_button_), "clicked",
                     GTK_SIGNAL_FUNC(raise_cb),
                     this);

  gtk_box_pack_start(GTK_BOX(top), raise_button_, FALSE, FALSE, OUR_PAD);

  lower_button_ = gtk_button_new_with_label(_("Lower"));

  gtk_signal_connect(GTK_OBJECT(lower_button_), "clicked",
                     GTK_SIGNAL_FUNC(lower_cb),
                     this);

  gtk_box_pack_start(GTK_BOX(top), lower_button_, FALSE, FALSE, OUR_PAD);

  ///
  
  make_axis_option(_("X Axis: "),
                   PlotUtil::NORTH,
                   PlotUtil::SOUTH,
                   &top,
                   &x_axis_);

  gtk_option_menu_set_history(GTK_OPTION_MENU(x_axis_),
                              (layer_->x_axis() == PlotUtil::NORTH) ? 0 : 1);

  guppi_set_tooltip(x_axis_, _("Changes which scale the layer is associated with."));

  gtk_box_pack_start(GTK_BOX(vbox), top, FALSE, FALSE, OUR_PAD);

  ///

  make_axis_option(_("Y Axis: "),
                   PlotUtil::EAST,
                   PlotUtil::WEST,
                   &top,
                   &y_axis_);

  gtk_option_menu_set_history(GTK_OPTION_MENU(y_axis_),
                              (layer_->y_axis() == PlotUtil::EAST) ? 0 : 1);

  guppi_set_tooltip(y_axis_, _("Changes which scale the layer is associated with."));

  gtk_box_pack_start(GTK_BOX(vbox), top, TRUE, TRUE, OUR_PAD);

  ///

  hidden_toggle_ = gtk_check_button_new_with_label(_("Hide Layer"));

  guppi_set_tooltip(hidden_toggle_, 
                    _("Hidden layers aren't displayed, and tools do not affect them."));

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(hidden_toggle_), layer_->hidden());

  gtk_signal_connect(GTK_OBJECT(hidden_toggle_), "toggled",
                     GTK_SIGNAL_FUNC(toggle_hidden_cb),
                     this);

  gtk_box_pack_start(GTK_BOX(vbox), hidden_toggle_, FALSE, FALSE, OUR_PAD);

  /// 

  masked_toggle_ = gtk_check_button_new_with_label(_("Mask Layer"));

  guppi_set_tooltip(masked_toggle_, 
                    _("Tools do not affect masked layers."));

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(masked_toggle_), layer_->masked());

  gtk_signal_connect(GTK_OBJECT(masked_toggle_), "toggled",
                     GTK_SIGNAL_FUNC(toggle_masked_cb),
                     this);

  gtk_box_pack_start(GTK_BOX(vbox), masked_toggle_, FALSE, FALSE, OUR_PAD);
  
  ///

  gtk_container_set_border_width(GTK_CONTAINER(vbox), OUR_PAD);

  gtk_container_add(GTK_CONTAINER(layer_widget_), vbox);

  gtk_widget_show_all(layer_widget_);
}

void 
XyLayerEdit::toggle_hidden_cb(GtkWidget* w, gpointer data)
{
  XyLayerEdit* xle = static_cast<XyLayerEdit*>(data);

  bool new_state = (GTK_TOGGLE_BUTTON(xle->hidden_toggle_)->active != FALSE);

  xle->layer_->set_hidden(new_state);
}


void 
XyLayerEdit::toggle_masked_cb(GtkWidget* w, gpointer data)
{
  XyLayerEdit* xle = static_cast<XyLayerEdit*>(data);

  bool new_state = (GTK_TOGGLE_BUTTON(xle->masked_toggle_)->active != FALSE);

  xle->layer_->set_masked(new_state);
}

void 
XyLayerEdit::activate_axisitem_cb(GtkWidget* w, gpointer data)
{
  XyLayerEdit* xle = static_cast<XyLayerEdit*>(data);

  map<GtkWidget*,PlotUtil::PositionType>::iterator i = xle->axis_items_.find(w);

  g_assert(i != xle->axis_items_.end());

  if (PlotUtil::vertical(i->second))
    {
      xle->layer_->set_y_axis(i->second);
    }
  else
    {
      xle->layer_->set_x_axis(i->second);
    }
}

void 
XyLayerEdit::remove_cb(GtkWidget* w, gpointer data)
{
  XyLayerEdit* xle = static_cast<XyLayerEdit*>(data);

  XyPlotState::iterator i = xle->state_->find(xle->layer_);

  g_return_if_fail(i != xle->state_->end());

  // Kind of risky; results in self-deletion, since we'll be deleted 
  //  when the XyPlotEdit gets notified the layer was removed.
  // I *think* it is OK; but it's fragile.
  xle->state_->remove_layer(i);
}

void 
XyLayerEdit::raise_cb(GtkWidget* w, gpointer data)
{
  XyLayerEdit* xle = static_cast<XyLayerEdit*>(data);

  XyPlotState::iterator i = xle->state_->find(xle->layer_);

  g_return_if_fail(i != xle->state_->end());

  xle->state_->raise_layer(i);
}

void 
XyLayerEdit::lower_cb(GtkWidget* w, gpointer data)
{
  XyLayerEdit* xle = static_cast<XyLayerEdit*>(data);

  XyPlotState::iterator i = xle->state_->find(xle->layer_);

  g_return_if_fail(i != xle->state_->end());
  
  xle->state_->lower_layer(i);
}

// XyLayer::View

void
XyLayerEdit::change_x_axis(XyLayer* layer, 
                           PlotUtil::PositionType old_pos, 
                           PlotUtil::PositionType new_pos)
{
  gtk_option_menu_set_history(GTK_OPTION_MENU(x_axis_),
                              (layer_->x_axis() == PlotUtil::NORTH) ? 0 : 1);
}

void
XyLayerEdit::change_y_axis(XyLayer* layer, 
                           PlotUtil::PositionType old_pos, 
                           PlotUtil::PositionType new_pos)
{
  gtk_option_menu_set_history(GTK_OPTION_MENU(y_axis_),
                              (layer_->y_axis() == PlotUtil::EAST) ? 0 : 1);
}

void
XyLayerEdit::change_hidden(XyLayer* layer,
                           bool hidden)
{
  bool old_state = (GTK_TOGGLE_BUTTON(hidden_toggle_)->active != FALSE);

  if (hidden != old_state)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(hidden_toggle_), layer_->hidden());
}

void 
XyLayerEdit::change_name(XyLayer* layer, 
                         const string& name)
{
  if (name_label_ != 0)
    {
      gtk_label_set(GTK_LABEL(name_label_), name.c_str());
    }
}

void 
XyLayerEdit::change_masked(XyLayer* layer, bool maskedness)
{
  bool old_state = (GTK_TOGGLE_BUTTON(hidden_toggle_)->active != FALSE);

  if (maskedness != old_state)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(masked_toggle_), layer_->masked());
}

void
XyLayerEdit::destroy_model()
{

}

//////

class XyDataLayerEdit : public XyLayerEdit,
                        public XyDataLayer::View
{
public:
  XyDataLayerEdit(XyDataLayer* layer, XyPlotState* state) 
    : XyLayerEdit(layer, state), 
      data_layer_(layer), data_widget_(0),
      changing_data_(false), 
      x_optimize_toggle_(0), y_optimize_toggle_(0)
    {
      // Only let the user choose scalar data
      x_option_.restrict_type(Data::Scalar);
      y_option_.restrict_type(Data::Scalar);

      // We never disconnect since the DataOption dies with us.
      x_option_.connect(data_chosen_cb, this);
      y_option_.connect(data_chosen_cb, this);

      x_option_.set_store(state_->store());
      y_option_.set_store(state_->store());
      x_option_.set_data(data_layer_->x_data());
      y_option_.set_data(data_layer_->y_data());

      data_layer_->data_layer_model.add_view(this);
    }

  virtual ~XyDataLayerEdit() {
    data_layer_->data_layer_model.remove_view(this);
  }

  // XyDataLayer::View
  
  virtual void change_x_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data);
  virtual void change_y_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data);
  
  virtual void change_data_values(XyDataLayer* layer, Data* data);

  // ::View
  virtual void destroy_model() {}

protected:
  XyDataLayer* data_layer_; // pure lazy, avoid casts.

  GtkWidget* data_widget_;

  void ensure_data_widget();

  virtual void ensure_top_widget();

  virtual void zero_widget_pointers();

private:
  bool changing_data_;

  DataOption x_option_;
  DataOption y_option_;

  GtkWidget* x_optimize_toggle_;
  GtkWidget* y_optimize_toggle_;

  static void data_chosen_cb(gpointer emitter, gpointer data);
  void data_chosen(DataOption* emitter);  
};

  
void 
XyDataLayerEdit::change_x_data(XyDataLayer* layer, 
                               Data* old_data, Data* new_data)
{
  if (!changing_data_)
    {
      x_option_.set_data(new_data);
    }
}

void 
XyDataLayerEdit::change_y_data(XyDataLayer* layer, 
                               Data* old_data, Data* new_data)
{
  if (!changing_data_)
    {
      y_option_.set_data(new_data);
    }
}

void 
XyDataLayerEdit::change_data_values(XyDataLayer* layer, Data* data)
{
  // Nothing
}

void
XyDataLayerEdit::ensure_data_widget()
{
  if (data_widget_ != 0)
    return;

  data_widget_ = gtk_frame_new(_("XY Data"));

  GtkWidget* vbox = gtk_vbox_new(FALSE, OUR_PAD);

  GtkWidget* label = gtk_label_new(_("X Data: "));
  
  GtkWidget* hbox = gtk_hbox_new(GNOME_PAD, FALSE);
  
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, OUR_PAD);
  gtk_box_pack_end(GTK_BOX(hbox), x_option_.widget(), TRUE, TRUE, OUR_PAD);

  guppi_set_tooltip(hbox, _("Change the data set used for the X axis"));

  gtk_box_pack_start(GTK_BOX(vbox), 
                     hbox, FALSE, FALSE, OUR_PAD);

  label = gtk_label_new(_("Y Data: "));
  
  hbox = gtk_hbox_new(GNOME_PAD, FALSE);
  
  guppi_set_tooltip(hbox, _("Change the data set used for the Y axis"));

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, OUR_PAD);
  gtk_box_pack_end(GTK_BOX(hbox), y_option_.widget(), TRUE, TRUE, OUR_PAD);

  gtk_box_pack_start(GTK_BOX(vbox),
                     hbox, FALSE, FALSE, OUR_PAD);

  ///

  x_optimize_toggle_ = gtk_check_button_new_with_label(_("Autosize X Axis"));

  guppi_set_tooltip(x_optimize_toggle_, _("When the data on either axis is changed, automatically adjust the X axis to best fit the X data."));

  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(x_optimize_toggle_), TRUE);

  gtk_box_pack_start(GTK_BOX(vbox),
                     x_optimize_toggle_, FALSE, FALSE, OUR_PAD);
  
  y_optimize_toggle_ = gtk_check_button_new_with_label(_("Autosize Y Axis"));

  guppi_set_tooltip(y_optimize_toggle_, _("When the data on either axis is changed, automatically adjust the Y axis to best fit the Y data."));

  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(y_optimize_toggle_), TRUE);

  gtk_box_pack_start(GTK_BOX(vbox),
                     y_optimize_toggle_, FALSE, FALSE, OUR_PAD);

  gtk_container_add(GTK_CONTAINER(data_widget_), vbox);
}

void
XyDataLayerEdit::ensure_top_widget()
{
  if (widget_ != 0)
    return;

  widget_ = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

  ensure_layer_widget();
  ensure_data_widget();

  gtk_box_pack_start(GTK_BOX(widget_), layer_widget_,
                     FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(widget_), data_widget_,
                     FALSE, FALSE, 0);

  gtk_widget_show_all(widget_);
}

void
XyDataLayerEdit::zero_widget_pointers()
{
  XyLayerEdit::zero_widget_pointers();
  data_widget_ = 0;
  y_optimize_toggle_ = 0;
  x_optimize_toggle_ = 0;
  // The data options monitor "destroy" for their widgets and zero if
  // it occurs
}

void 
XyDataLayerEdit::data_chosen_cb(gpointer emitter, gpointer data)
{
  XyDataLayerEdit* dle = static_cast<XyDataLayerEdit*>(data);
  
  dle->data_chosen(static_cast<DataOption*>(emitter));
}

void 
XyDataLayerEdit::data_chosen(DataOption* emitter)
{
  g_return_if_fail(state_ != 0);

  changing_data_ = true;

  if (emitter == &x_option_)
    {
      data_layer_->set_x_data(x_option_.get_data()); 
    }
  else if (emitter == &y_option_)
    {
      data_layer_->set_y_data(y_option_.get_data()); 
    }
  else 
    g_warning("Emitter unknown - should not happen in %s", __FUNCTION__);

  // The toggle buttons are 0 when we set the state but aren't showing 
  // the window yet.
  if (x_optimize_toggle_ != 0)
    {
      if (GTK_TOGGLE_BUTTON(x_optimize_toggle_)->active)
        state_->optimize_layers_axis(data_layer_, data_layer_->x_axis());
    }  

  if (y_optimize_toggle_ != 0)
    {
      if (GTK_TOGGLE_BUTTON(y_optimize_toggle_)->active)
        state_->optimize_layers_axis(data_layer_, data_layer_->y_axis());
    }

  changing_data_ = false;
}

class XyScatterEdit : public XyDataLayerEdit, 
                      public XyScatter::View
{
public:
  XyScatterEdit(XyScatter* layer, XyPlotState* state);
  virtual ~XyScatterEdit();

  // XyScatter::View
  virtual void change_styles(XyScatter* xys);

  // ::View
  virtual void destroy_model();

protected:

  void ensure_scatter_widget();

  virtual void ensure_top_widget();

  virtual void zero_widget_pointers();


private:
  XyScatter* scatter_;
  GtkWidget* scatter_widget_;

  static void line_layer_cb(GtkWidget*, gpointer data);
  void make_line_layer();  
};

XyScatterEdit::XyScatterEdit(XyScatter* layer, XyPlotState* state)
  : XyDataLayerEdit(layer, state), scatter_(layer),
    scatter_widget_(0)
{
  g_return_if_fail(scatter_ != 0);

  scatter_->xy_scatter_model.add_view(this);


}

XyScatterEdit::~XyScatterEdit()
{
  scatter_->xy_scatter_model.remove_view(this);
}

void 
XyScatterEdit::ensure_scatter_widget()
{
  if (scatter_widget_ != 0)
    return;

  scatter_widget_ = gtk_frame_new(_("Scatter"));

  GtkWidget* vbox = gtk_vbox_new(FALSE, OUR_PAD);

  GtkWidget* button = gtk_button_new_with_label(_("Make attached line plot layer"));

  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(line_layer_cb),
                     this);

  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, OUR_PAD);

  gtk_container_add(GTK_CONTAINER(scatter_widget_), vbox);
}

void 
XyScatterEdit::ensure_top_widget()
{
  if (widget_ != 0)
    return;

  widget_ = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

  ensure_layer_widget();
  ensure_data_widget();
  ensure_scatter_widget();

  gtk_box_pack_start(GTK_BOX(widget_), layer_widget_,
                     FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(widget_), data_widget_,
                     FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(widget_), scatter_widget_,
                     FALSE, FALSE, 0);

  gtk_widget_show_all(widget_);
}

void 
XyScatterEdit::zero_widget_pointers()
{
  XyDataLayerEdit::zero_widget_pointers();
  scatter_widget_ = 0;
}


/// XyScatter::View
void
XyScatterEdit::change_styles(XyScatter* xys)
{
  // nothing
}


/// ::View
void
XyScatterEdit::destroy_model()
{
#ifdef GNOME_ENABLE_DEBUG
  g_warning("XyScatter or marker palette destroyed from underneath XyScatterEdit");
#endif
}

///

void 
XyScatterEdit::line_layer_cb(GtkWidget* button, gpointer data)
{
  XyScatterEdit* se = static_cast<XyScatterEdit*>(data);
  
  se->make_line_layer();
}

void 
XyScatterEdit::make_line_layer()
{
  XyDataLayer* layer = new XyLine(state_->store());

  layer->share_datapair_with(scatter_);

  layer->set_x_axis(scatter_->x_axis());
  layer->set_y_axis(scatter_->y_axis());

  state_->add_layer_no_auto_axis(layer);
}

//////

class XyPriceEdit : public XyLayerEdit,
                    public XyPriceBars::View
{
public:
  XyPriceEdit(XyPriceBars* layer, XyPlotState* state) 
    : XyLayerEdit(layer, state), 
      price_layer_(layer), price_widget_(0),
      changing_data_(false), 
      optimize_toggle_(0)
    {
      option_.restrict_type(guppi_price_data_spec()->id());

      // We never disconnect since the DataOption dies with us.
      option_.connect(data_chosen_cb, this);

      option_.set_store(state_->store());

      option_.set_data(price_layer_->data());

      price_layer_->price_model.add_view(this);
    }

  virtual ~XyPriceEdit() {
    price_layer_->price_model.remove_view(this);
  }

  // XyPriceBars::View

  virtual void change_data(XyPriceBars* layer, 
                           Data* old_data, Data* new_data);
  
  virtual void change_data_values(XyPriceBars* layer, Data* data);

  // ::View
  virtual void destroy_model() {}

protected:
  XyPriceBars* price_layer_; // pure lazy, avoid casts.

  GtkWidget* price_widget_;

  void ensure_price_widget();

  virtual void ensure_top_widget();

  virtual void zero_widget_pointers();

private:
  bool changing_data_;

  DataOption option_;

  GtkWidget* optimize_toggle_;

  static void data_chosen_cb(gpointer emitter, gpointer data);
  void data_chosen(DataOption* emitter);  

  void make_data_hbox(GtkWidget* vbox,
                      const gchar* label,
                      const gchar* tooltip,
                      DataOption& option);
};

void 
XyPriceEdit::change_data(XyPriceBars* layer, 
                         Data* old_data, Data* new_data)
{
  if (!changing_data_)
    {
      option_.set_data(new_data);
    }
}
  
void 
XyPriceEdit::change_data_values(XyPriceBars* layer, Data* data)
{
  // Nothing
}

void
XyPriceEdit::make_data_hbox(GtkWidget* vbox,
                            const gchar* label_text,
                            const gchar* tooltip,
                            DataOption& option)
{

  GtkWidget* label = gtk_label_new(label_text);

  GtkWidget* hbox = gtk_hbox_new(GNOME_PAD, FALSE);
  
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, OUR_PAD);
  gtk_box_pack_end(GTK_BOX(hbox), option.widget(), TRUE, TRUE, OUR_PAD);

  guppi_set_tooltip(hbox,tooltip);

  gtk_box_pack_start(GTK_BOX(vbox), 
                     hbox, FALSE, FALSE, OUR_PAD);

}

void
XyPriceEdit::ensure_price_widget()
{
  if (price_widget_ != 0)
    return;

  price_widget_ = gtk_frame_new(_("Price Data"));

  GtkWidget* vbox = gtk_vbox_new(FALSE, OUR_PAD);

  make_data_hbox(vbox, _("Price Data:"), 
                 _("Change the price data used"), 
                 option_);

  ///

  optimize_toggle_ = gtk_check_button_new_with_label(_("Autosize To Fit Data"));

  guppi_set_tooltip(optimize_toggle_, _("When the data is changed, automatically adjust the axes to best fit the data."));

  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(optimize_toggle_), TRUE);

  gtk_box_pack_start(GTK_BOX(vbox),
                     optimize_toggle_, FALSE, FALSE, OUR_PAD);

  gtk_container_add(GTK_CONTAINER(price_widget_), vbox);
}

void
XyPriceEdit::ensure_top_widget()
{
  if (widget_ != 0)
    return;

  widget_ = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

  ensure_layer_widget();
  ensure_price_widget();

  gtk_box_pack_start(GTK_BOX(widget_), layer_widget_,
                     FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(widget_), price_widget_,
                     FALSE, FALSE, 0);

  gtk_widget_show_all(widget_);
}

void
XyPriceEdit::zero_widget_pointers()
{
  XyLayerEdit::zero_widget_pointers();
  price_widget_ = 0;
  optimize_toggle_ = 0;
  // The data options monitor "destroy" for their widgets and zero if
  // it occurs
}

void 
XyPriceEdit::data_chosen_cb(gpointer emitter, gpointer data)
{
  XyPriceEdit* dle = static_cast<XyPriceEdit*>(data);
  
  dle->data_chosen(static_cast<DataOption*>(emitter));
}

void 
XyPriceEdit::data_chosen(DataOption* emitter)
{
  g_return_if_fail(state_ != 0);

  changing_data_ = true;

  bool opt_prices = false;

  g_assert(emitter == &option_);

  price_layer_->set_data(option_.get_data());

  // The toggle button is 0 when we set the state but aren't showing 
  // the window yet.
  if (optimize_toggle_ != 0)
    {
      if (GTK_TOGGLE_BUTTON(optimize_toggle_)->active)
        {
          state_->optimize_layers_axis(price_layer_, price_layer_->x_axis());
          state_->optimize_layers_axis(price_layer_, price_layer_->y_axis());
        }
    } 

  changing_data_ = false;
}

///////////////////////////////////////

XyPlotEdit::XyPlotEdit()
  : dialog_(0), state_(0), notebook_(0)
{

}

XyPlotEdit::~XyPlotEdit()
{
  if (dialog_ != 0)
    close();

  release_state();
}

void 
XyPlotEdit::set_state(XyPlotState* state)
{
  release_state();

  state_ = state;
  
  if (state_ != 0)
    {
      state_->ref();
      state_->state_model.add_view(this);

      XyPlotState::iterator i = state_->begin();
      while (i != state_->end())
        {
          add_layer(state_, *i);

          ++i;
        }
    }
}

void
XyPlotEdit::add_view_for_layer(XyLayer* layer)
{
  g_assert(state_ != 0);
  g_assert(layer_views_.find(layer) == layer_views_.end());

  TypeRegistry* tr = layer->type_registry();

  if (tr->is_a(layer->type(), XyLayer::ScatterLayer))
    {
      layer_views_[layer] = 
        new XyScatterEdit((XyScatter*)layer->cast_to_type(XyLayer::ScatterLayer), 
                          state_);
    }
  else if (tr->is_a(layer->type(), XyLayer::DataLayer))
    {
      layer_views_[layer] = 
        new XyDataLayerEdit((XyDataLayer*)layer->cast_to_type(XyLayer::DataLayer), 
                            state_);
    }
  else if (tr->is_a(layer->type(), XyLayer::PriceLayer))
    {
      layer_views_[layer] = 
        new XyPriceEdit((XyPriceBars*)layer->cast_to_type(XyLayer::PriceLayer), 
                        state_);
    }
  else
    {
      layer_views_[layer] = new XyLayerEdit(layer, state_);
    }
}

void 
XyPlotEdit::add_page_for_layer(XyLayerEdit* le)
{
  g_assert(notebook_ != 0);

  gtk_notebook_prepend_page(GTK_NOTEBOOK(notebook_), le->widget(), 
                            le->name_label());
}

void 
XyPlotEdit::edit()
{
  g_return_if_fail(state_ != 0);

  if (dialog_ != 0) 
    {
      guppi_raise_and_show(GTK_WINDOW(dialog_));
      return;
    }

  dialog_ = gnome_dialog_new(_("XY Plot Layers Editor"),
                             GNOME_STOCK_BUTTON_CLOSE,
                             NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog_), TRUE);

  gtk_window_set_policy(GTK_WINDOW(dialog_), TRUE, TRUE, FALSE);

  gtk_signal_connect(GTK_OBJECT(dialog_),
                     "close",
                     GTK_SIGNAL_FUNC(close_cb),
                     this);

  notebook_ = gtk_notebook_new();

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox), 
                     notebook_, TRUE, TRUE, GNOME_PAD_SMALL);

  // We need to do this in the layer order; remember that layer_views_ is in 
  //  an arbitrary order
  XyPlotState::iterator i = state_->begin();
  while (i != state_->end())
    {
      map<XyLayer*,XyLayerEdit*>::iterator j = layer_views_.find(*i);      

      g_assert(j != layer_views_.end());

      add_page_for_layer(j->second);

      ++i;
    }

  gtk_widget_show_all(dialog_);
}

void 
XyPlotEdit::release_state()
{
  if (state_ != 0)
    {
      if (dialog_ != 0)
        {
          close();
        }

      map<XyLayer*,XyLayerEdit*>::iterator i = layer_views_.begin();
      while (i != layer_views_.end())
        {
          delete i->second;
          ++i;
        }
      layer_views_.clear();

      state_->state_model.remove_view(this);

      if (state_->unref() == 0)
        delete state_;

      state_ = 0;
    }
}

///// XyPlotState::View

void
XyPlotEdit::change_background(XyPlotState* state, const Color & bg)
{
}
void
XyPlotEdit::change_axis(XyPlotState* state, 
                           PlotUtil::PositionType pos, 
                           const Axis& axis)
{


}

void
XyPlotEdit::change_size(XyPlotState* state, double width, double height)
{
}
void
XyPlotEdit::change_name(XyPlotState* state, const string& name)
{
}
  
void
XyPlotEdit::remove_layer(XyPlotState* state, XyLayer* layer)
{
  map<XyLayer*,XyLayerEdit*>::iterator i = layer_views_.find(layer);

  g_assert(i != layer_views_.end());

  if (notebook_ != 0)
    {
      gint num = gtk_notebook_page_num(GTK_NOTEBOOK(notebook_), 
                                       i->second->widget());

      g_assert(num >= 0);

      gtk_notebook_remove_page(GTK_NOTEBOOK(notebook_), num);

      i->second->forget_widget();
    }

  delete i->second;

  layer_views_.erase(i);
}

void
XyPlotEdit::add_layer(XyPlotState* state, XyLayer* layer)
{
  add_view_for_layer(layer);

  if (dialog_ != 0)
    add_page_for_layer(layer_views_[layer]);
}

void
XyPlotEdit::raise_layer(XyPlotState* state, XyLayer* layer)
{
  map<XyLayer*,XyLayerEdit*>::iterator i = layer_views_.find(layer);

  g_assert(i != layer_views_.end());

  if (notebook_ != 0)
    {
      gint num = gtk_notebook_page_num(GTK_NOTEBOOK(notebook_), 
                                       i->second->widget());

      g_assert(num >= 0);

      gtk_notebook_reorder_child(GTK_NOTEBOOK(notebook_), 
                                 i->second->widget(),
                                 num-1);
    }
}

void
XyPlotEdit::lower_layer(XyPlotState* state, XyLayer* layer)
{
  map<XyLayer*,XyLayerEdit*>::iterator i = layer_views_.find(layer);

  g_assert(i != layer_views_.end());

  if (notebook_ != 0)
    {
      gint num = gtk_notebook_page_num(GTK_NOTEBOOK(notebook_), 
                                       i->second->widget());

      g_assert(num >= 0);

      gtk_notebook_reorder_child(GTK_NOTEBOOK(notebook_), 
                                 i->second->widget(),
                                 num+1);
    }
}


///// ::View

void 
XyPlotEdit::destroy_model()
{

}

///////

gint 
XyPlotEdit::close_cb(GtkWidget* w, gpointer data)
{
  XyPlotEdit* spe = static_cast<XyPlotEdit*>(data);
  
  return spe->close();
}

gint 
XyPlotEdit::close()
{
  gtk_widget_destroy(dialog_);
  dialog_ = 0;
  notebook_ = 0;

  map<XyLayer*,XyLayerEdit*>::iterator i = layer_views_.begin();
  while (i != layer_views_.end())
    {
      i->second->forget_widget();
      ++i;
    }

  return TRUE;
}

///////////

//////////////////// Axis Editor

XyAxisEdit::XyAxisEdit()
  : state_(0), dialog_(0), axis_option_(0),
    north_mi_(0), south_mi_(0), east_mi_(0), west_mi_(0),
    axis_edit_slot_(0), current_axis_widget_(0), 
    current_axis_edit_(&west_)
{
  // OK not to disconnect, because they won't emit after we're destroyed
  north_.connect(axisedit_changed_cb, this);
  south_.connect(axisedit_changed_cb, this);
  east_.connect(axisedit_changed_cb, this);
  west_.connect(axisedit_changed_cb, this);
}

XyAxisEdit::~XyAxisEdit()
{
  if (dialog_ != 0)
    gtk_widget_destroy(dialog_);
}

void
XyAxisEdit::change_background(XyPlotState* state, const Color & bg)
{


}

void
XyAxisEdit::change_axis(XyPlotState* state, 
                        PlotUtil::PositionType pos, 
                        const Axis& axis)
{


}

void
XyAxisEdit::change_size(XyPlotState* state, double width, double height)
{


}

void
XyAxisEdit::change_name(XyPlotState* state, const string& name)
{


}

void
XyAxisEdit::remove_layer(XyPlotState* state, XyLayer* layer)
{


}

void
XyAxisEdit::add_layer(XyPlotState* state, XyLayer* layer)
{


}

void
XyAxisEdit::raise_layer(XyPlotState* state, XyLayer* layer)
{

}

void
XyAxisEdit::lower_layer(XyPlotState* state, XyLayer* layer)
{


}

void
XyAxisEdit::destroy_model()
{

}


void 
XyAxisEdit::edit()
{
  if (dialog_ == 0)
    {
      dialog_ = gnome_dialog_new(_("XY Plot Axis Editor"),
                                 GNOME_STOCK_BUTTON_OK,
                                 GNOME_STOCK_BUTTON_APPLY,
                                 GNOME_STOCK_BUTTON_CLOSE,
                                 NULL);
      
      gnome_dialog_set_close(GNOME_DIALOG(dialog_), FALSE);
      
      gtk_window_set_policy(GTK_WINDOW(dialog_), TRUE, TRUE, FALSE);
      
      gtk_signal_connect(GTK_OBJECT(dialog_),
                         "close",
                         GTK_SIGNAL_FUNC(close_cb),
                         this);
   
      gtk_signal_connect(GTK_OBJECT(dialog_),
                         "clicked",
                         GTK_SIGNAL_FUNC(dialog_clicked_cb),
                         this);
   
      axis_option_ = gtk_option_menu_new();
      GtkWidget* menu = gtk_menu_new();

      north_mi_ = gtk_menu_item_new_with_label(_("North"));
      gtk_menu_append(GTK_MENU(menu), north_mi_);
      gtk_signal_connect(GTK_OBJECT(north_mi_),
                         "activate",
                         GTK_SIGNAL_FUNC(axis_selected_cb),
                         this);

      south_mi_ = gtk_menu_item_new_with_label(_("South"));
      gtk_menu_append(GTK_MENU(menu), south_mi_);
      gtk_signal_connect(GTK_OBJECT(south_mi_),
                         "activate",
                         GTK_SIGNAL_FUNC(axis_selected_cb),
                         this);


      east_mi_ = gtk_menu_item_new_with_label(_("East"));
      gtk_menu_append(GTK_MENU(menu), east_mi_);
      gtk_signal_connect(GTK_OBJECT(east_mi_),
                         "activate",
                         GTK_SIGNAL_FUNC(axis_selected_cb),
                         this);


      west_mi_ = gtk_menu_item_new_with_label(_("West"));
      gtk_menu_append(GTK_MENU(menu), west_mi_);
      gtk_signal_connect(GTK_OBJECT(west_mi_),
                         "activate",
                         GTK_SIGNAL_FUNC(axis_selected_cb),
                         this);

      gtk_option_menu_set_menu(GTK_OPTION_MENU(axis_option_), menu);

      gint history = 0;
      if (current_axis_edit_ == &north_)
        history = 0;      
      else if (current_axis_edit_ == &south_)
        history = 1;
      else if (current_axis_edit_ == &east_)
        history = 2;
      else if (current_axis_edit_ == &west_)
        history = 3;
      else
        g_warning("No current axis edit in %s", __FUNCTION__);

      gtk_option_menu_set_history(GTK_OPTION_MENU(axis_option_),
                                  history); 

      GtkWidget* box = guppi_make_labelled_widget(_("Editing Axis:"), axis_option_);
   
      gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox),
                         box, 
                         FALSE, FALSE, GNOME_PAD_SMALL);

      axis_edit_slot_ = gtk_frame_new("Bug!");

      gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox),
                         axis_edit_slot_, 
                         TRUE, TRUE, GNOME_PAD_SMALL);

      // To update the widgets; current_axis_edit_ remembers the 
      //  current edit if the user closes the dialog.
      set_current_axis_edit(current_axis_edit_);

      gtk_widget_show_all(GNOME_DIALOG(dialog_)->vbox);

      set_apply_sensitive(false);

      if (state_ == 0)
        {
          gtk_widget_set_sensitive(GNOME_DIALOG(dialog_)->vbox, FALSE);
        }
    }

  guppi_raise_and_show(GTK_WINDOW(dialog_));
}


void 
XyAxisEdit::set_state(XyPlotState* state)
{
  release_state();

  state_ = state;
  
  if (state_ != 0)
    {
      state_->ref();
      state_->state_model.add_view(this);

      north_.fill_from(state_->axis(PlotUtil::NORTH));
      south_.fill_from(state_->axis(PlotUtil::SOUTH));
      east_.fill_from(state_->axis(PlotUtil::EAST));
      west_.fill_from(state_->axis(PlotUtil::WEST));
    }
  else
    {
      if (dialog_ != 0)
        gtk_widget_set_sensitive(GNOME_DIALOG(dialog_)->vbox, FALSE);
    }

  set_apply_sensitive(false);
}

void 
XyAxisEdit::release_state()
{
  if (state_ != 0)
    {
      if (dialog_ != 0)
        {
          close();
        }

      state_->state_model.remove_view(this);

      if (state_->unref() == 0)
        delete state_;

      state_ = 0;
    }
}

gint 
XyAxisEdit::close_cb(GtkWidget* w, gpointer data)
{
  XyAxisEdit* ae = static_cast<XyAxisEdit*>(data);
  
  return ae->close();
}

gint 
XyAxisEdit::close()
{
  gtk_widget_destroy(dialog_);
  dialog_ = 0;
  current_axis_widget_ = 0;
  
  return TRUE;
}

void
XyAxisEdit::axis_selected_cb(GtkWidget* w, gpointer data)
{
  XyAxisEdit* ae = static_cast<XyAxisEdit*>(data);

  ae->axis_selected(w);
}

void 
XyAxisEdit::axis_selected(GtkWidget* mi)
{
  AxisEdit* ae = 0;

  if (mi == north_mi_)
    ae = &north_;
  else if (mi == south_mi_)
    ae = &south_;
  else if (mi == east_mi_)
    ae = &east_;
  else if (mi == west_mi_)
    ae = &west_;
  else
    g_assert_not_reached();

  set_current_axis_edit(ae);
}

void 
XyAxisEdit::set_current_axis_edit(AxisEdit* ae)
{
  g_return_if_fail(dialog_ != 0);
  g_return_if_fail(ae != 0);
  
  // ae may be == current_axis_edit_, but we need to update the widgets

  current_axis_edit_ = ae;

  GtkWidget* new_panel = ae->widget();

  if (new_panel == current_axis_widget_)
    return;
  else 
    {
      if (current_axis_widget_ != 0)
        {
          // This destroys the widget, but that's OK with AxisEdit and should remain so.
          gtk_container_remove(GTK_CONTAINER(axis_edit_slot_), current_axis_widget_);
        }
      
      current_axis_widget_ = new_panel;

      // This glosses over a bug rather than fixing it, but oh well.
      // (in the add below, the widget gets mapped but isn't GTK_WIDGET_VISIBLE?)
      gtk_widget_show_all(current_axis_widget_);

      gtk_container_add(GTK_CONTAINER(axis_edit_slot_), current_axis_widget_);

      const gchar* title = NULL;

      if (ae == &north_)
        title = _("North Axis");
      else if (ae == &south_)
        title = _("South Axis");
      else if (ae == &west_)
        title = _("West Axis");
      else 
        title = _("East Axis");

      gtk_frame_set_label(GTK_FRAME(axis_edit_slot_), title);
    }
}

void 
XyAxisEdit::axisedit_changed_cb(gpointer emitter, gpointer data)
{
  AxisEdit* ae = (AxisEdit*)emitter;
  XyAxisEdit* xae = (XyAxisEdit*)data;

  xae->set_apply_sensitive(true);
}

void 
XyAxisEdit::dialog_clicked_cb(GtkWidget* dialog, gint button, gpointer data)
{
  XyAxisEdit* xae = (XyAxisEdit*)data;

  switch (button)
    {
    case 0: // OK
      xae->apply();
      xae->close();
      break;
    case 1: // Apply
      xae->apply();
      break;
    case 2: // close
      xae->close();
      break;
    default:
      g_warning("Button %d clicked? WTF.", button);
      break;
    }
}

void 
XyAxisEdit::set_apply_sensitive(bool state)
{
  if (dialog_ == 0)
    return;

  gnome_dialog_set_sensitive(GNOME_DIALOG(dialog_), 0, state);
  gnome_dialog_set_sensitive(GNOME_DIALOG(dialog_), 1, state);
}

void 
XyAxisEdit::apply()
{
  Axis* a;

  a = state_->checkout_axis(PlotUtil::NORTH);
  if (a != 0)
    {
      north_.apply_to(a);
      state_->checkin_axis(PlotUtil::NORTH, a);
    }
  else
    g_warning("couldn't checkout north axis");

  a = state_->checkout_axis(PlotUtil::SOUTH);
  if (a != 0)
    {
      south_.apply_to(a);
      state_->checkin_axis(PlotUtil::SOUTH, a);
    }
  else
    g_warning("couldn't checkout south axis");

  a = state_->checkout_axis(PlotUtil::EAST);
  if (a != 0)
    {
      east_.apply_to(a);
      state_->checkin_axis(PlotUtil::EAST, a);
    }
  else
    g_warning("couldn't checkout east axis");

  a = state_->checkout_axis(PlotUtil::WEST);
  if (a != 0)
    {
      west_.apply_to(a);
      state_->checkin_axis(PlotUtil::WEST, a);
    }
  else
    g_warning("couldn't checkout west axis");

  set_apply_sensitive(false);
}





