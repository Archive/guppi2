// -*- C++ -*-

/* 
 * pieplot.h 
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "pieplot.h"
#include "gnome-canvas-arc.h"
#include <libgnomeui/gnome-canvas.h>
#include <math.h>

const char* PiePlot::ID = "guppi-piechart";

PiePlot::PiePlot(DataStore* datastore)
  : Plot(ID, datastore), canvas_(0), state_(0), group_(0), 
    bg_(0), xpos_(0.0), ypos_(0.0)
{
  set_state(new PiePlotState(datastore));

  // defaults
  Data* scalar_data = 0;
  Data* label_data = 0;

  // Default to first two data sets with the right type
  DataStore::iterator i = datastore->begin();
  while (i != datastore->end())
    {
      if ((*i)->is_a(Data::Scalar))
        {
          if (scalar_data == 0)
            {
              scalar_data = *i;
            }
          else if (label_data != 0)
            break;
        }
      else if ((*i)->is_a(Data::Label))
        {
          if (label_data == 0)
            {
              label_data = *i;
            }
          else if (scalar_data != 0)
            break;
        }
      ++i;
    }

  if (scalar_data != 0)
    state_->set_data(scalar_data);
#if 0 // not implemented
  if (label_data != 0)
    state_->set_labels(label_data);
#endif
}

PiePlot::PiePlot(PiePlotState* state)
  : Plot(ID, state->store()), canvas_(0), state_(0), group_(0),  
    bg_(0), xpos_(0.0), ypos_(0.0)
{
  set_state(state);
}

Plot* 
PiePlot::new_view()
{
  g_return_val_if_fail(state_ != 0, 0);
  return new PiePlot(state_);
}

PiePlot::~PiePlot()
{
  release_state();

  g_assert(state_ == 0);
  
  unrealize();
}

GnomeCanvasItem* 
PiePlot::item()
{
  g_return_val_if_fail(canvas_ != 0, 0);
  g_return_val_if_fail(group_ != 0, 0);

  return GNOME_CANVAS_ITEM(group_);
}

void
PiePlot::realize(GnomeCanvasGroup* group)
{
  g_return_if_fail(canvas_ == 0);
  g_return_if_fail(group != 0);
  g_return_if_fail(GNOME_IS_CANVAS_GROUP(group));
  g_return_if_fail(group_ == 0); // ensure not realized yet.

  canvas_ = GTK_WIDGET(GNOME_CANVAS_ITEM(group)->canvas);

  // Positions are just hardcoded for now

  group_ = 
    GNOME_CANVAS_GROUP(gnome_canvas_item_new(group,
                                             gnome_canvas_group_get_type(),
                                             "x", xpos_, "y", ypos_, NULL));
  
  bg_ = gnome_canvas_item_new(group_,
                              gnome_canvas_rect_get_type(),
                              NULL);

  gtk_signal_connect(GTK_OBJECT(group_),
                     "event",
                     GTK_SIGNAL_FUNC(event_cb),
                     this);

  // We are going to ref our objects, so that the order of destruction
  //  (us or canvas first) isn't important
  gtk_object_ref(GTK_OBJECT(group_));
  gtk_object_ref(GTK_OBJECT(bg_));

  change_background(state_, state_->background());    
  change_size(state_, state_->width(), state_->height());
}

void 
PiePlot::unrealize()
{
  vector<GnomeCanvasItem*>::iterator q = slices_.begin();
  while (q != slices_.end())
    {
      gtk_object_destroy(GTK_OBJECT(*q));
      ++q;
    }
  slices_.clear();
  q = slicelabels_.begin();
  while (q != slicelabels_.end())
    {
      gtk_object_destroy(GTK_OBJECT(*q));
      ++q;
    }
  slicelabels_.clear();
  
  if (bg_) gtk_object_destroy(GTK_OBJECT(bg_));
  
  // group last, since some objects are inside it.
  if (group_) gtk_object_destroy(GTK_OBJECT(group_));

  group_ = 0; 
  bg_ = 0;
}

void 
PiePlot::set_x(double x)
{
  xpos_ = x;

  if (group_ != 0)
    gnome_canvas_item_set(GNOME_CANVAS_ITEM(group_),
                          "x", xpos_, NULL);
}

void 
PiePlot::set_y(double y)
{
  ypos_ = y;

  if (group_ != 0)
    gnome_canvas_item_set(GNOME_CANVAS_ITEM(group_),
                          "y", ypos_, NULL);
}

void 
PiePlot::set_size(double width, double height)
{
  // FIXME this does two size-change notifications
  state_->set_width(width);
  state_->set_height(height);
}

double 
PiePlot::width()
{
  return state_->width();
}

double 
PiePlot::height()
{
  return state_->height();
}

void 
PiePlot::size_request(double* width, double* height)
{
  g_return_if_fail(width != 0);
  g_return_if_fail(height != 0);

  state_->size_request(width, height);
}

const string& 
PiePlot::name() const
{
  return state_->name();
}

void
PiePlot::release_state()
{
  if (state_ != 0)
    {
      edit_.set_state(0); // so it won't refer to a destroyed state

      state_->state_model.remove_view(this);

      guint refs = state_->unref();
      if (refs == 0)
        delete state_;

      state_ = 0;
    }
}

void
PiePlot::set_state(PiePlotState* state)
{
  g_return_if_fail(state != 0);

  release_state();
  
  state_ = state;

  state_->ref();  
  state_->state_model.add_view(this);

  edit_.set_state(state_);

  change_size(state_, state_->width(), state_->height());
  change_background(state_, state_->background());
}

void
PiePlot::redisplay_slices()
{
  if (canvas_ == 0) return; // not an error, but we can't do anything.

  if (state_ == 0)
    {
      vector<GnomeCanvasItem*>::iterator q = slices_.begin();
      while (q != slices_.end())
        {
          gtk_object_destroy(GTK_OBJECT(*q));
          ++q;
        }
      slices_.clear();

      q = slicelabels_.begin();
      while (q != slicelabels_.end())
        {
          gtk_object_destroy(GTK_OBJECT(*q));
          ++q;
        }
      slicelabels_.clear();

      return;
    }

  PiePlotState::iterator j = state_->begin();

  const guint nitems = slices_.size();
  guint item = 0;

  while (j != state_->end())
    {
      if (item >= nitems)
        {
          GnomeCanvasItem* ci = 
            gnome_canvas_item_new(group_,
                                  gnome_canvas_arc_get_type(),
                                  NULL);

          guppi_spew_on_destroy(GTK_OBJECT(ci));
          gtk_object_ref(GTK_OBJECT(ci));
          slices_.push_back(ci);
          
          ci = gnome_canvas_item_new(group_,
                                     gnome_canvas_text_get_type(),
                                     "font", "fixed",
                                     "fill_color", "black",
                                     "anchor", GTK_ANCHOR_CENTER,
                                     NULL);
          
          guppi_spew_on_destroy(GTK_OBJECT(ci));
          gtk_object_ref(GTK_OBJECT(ci));
          slicelabels_.push_back(ci);

          // cast to gint since size may be 0
          g_assert((gint(slices_.size()) - 1) == gint(item));
          g_assert((gint(slicelabels_.size()) - 1) == gint(item));
        }
      
      g_assert(item < slices_.size());
      g_assert(item < slicelabels_.size());
      
      gnome_canvas_item_set(slices_[item],
                            "x1", (*j).posX() - (*j).radius(),
                            "y1", (*j).posY() - (*j).radius(),
                            "x2", (*j).posX() + (*j).radius(),
                            "y2", (*j).posY() + (*j).radius(),
                            "outline_color", "black", // should be user-settable eventually
                            "width_units", 2.0, // outline width, shouldn't hardcode
                            "fill_color_rgba", (*j).color().rgba(),
                            "start", (*j).start(),
                            "distance", (*j).width(),
                            NULL);

      // finally set position and text of label
      gnome_canvas_item_set(slicelabels_[item],
                            "text", (*j).label().c_str(),
                            "x", (*j).labelX(), "y", (*j).labelY(),
                            NULL);

      ++j;
      ++item;
    }

  // item is now the index of the first unused item 
  //  - possibly one past the end of slices_
  //  - we keep this position in item and have
  //    a copy in item_cnt (for the label canvas)

  guint item_cnt = item;

  // Clean up any extra canvas items
  vector<GnomeCanvasItem*>::iterator erasefrom = slices_.begin() + item_cnt;
  while (item_cnt < nitems)
    {
      gtk_object_destroy(GTK_OBJECT(slices_[item_cnt]));
      slices_[item_cnt] = 0;
      ++item_cnt;
    }
  slices_.erase(erasefrom, slices_.end());

  g_assert(slices_.size() == state_->size());
  
  // now clean up labels (we may now use 'item' - it is not required anymore)
  erasefrom = slicelabels_.begin() + item;
  while (item < nitems)
    {
      gtk_object_destroy(GTK_OBJECT(slicelabels_[item]));
      slicelabels_[item] = 0;
      ++item;
    }
  slicelabels_.erase(erasefrom, slicelabels_.end());

  g_assert(slicelabels_.size() == state_->size());

  // Maybe this will kill a redraw bug (which I believe is the canvas's fault, 
  // but this shouldn't hurt...)
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(group_));
}

void 
PiePlot::change_slices(PiePlotState* state)
{
  redisplay_slices();
}

void 
PiePlot::change_data(PiePlotState* state, Data* data)
{
  // mostly covered by change_slices for now.
  if (data != 0)
    set_status(data->name());
}

void 
PiePlot::change_background(PiePlotState* state, const Color & bg)
{
  if (bg_ != 0)
    {
      gnome_canvas_item_set(bg_,
                            "fill_color_rgba", bg.rgba(),
                            NULL);
    }
}

void 
PiePlot::change_size(PiePlotState* state, double width, double height)
{
  if (bg_ != 0)
    {
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(bg_),
                            "x1", 0.0,
                            "y1", 0.0,
                            "x2", width,
                            "y2", height,
                            NULL);
    }

  redisplay_slices();

  plot_model.size_changed(this);
}

void 
PiePlot::change_name(PiePlotState* state, const string& name)
{
  plot_model.name_changed(this, name);
}

void 
PiePlot::destroy_model()
{
  state_ = 0;
}

gint 
PiePlot::event_cb(GnomeCanvasItem* item, GdkEvent* e, gpointer data)
{
  PiePlot* bp = static_cast<PiePlot*>(data);

  return bp->event(e);
}

gint 
PiePlot::event(GdkEvent* e)
{
  // FIXME once we have actions for pie plot...
  if (e->type == GDK_2BUTTON_PRESS) 
    {
      edit_.edit();
      return TRUE;
    }
  else 
    {
      return FALSE;
    }
}
