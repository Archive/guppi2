// -*- C++ -*-

/* 
 * plotdraw.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plotdraw.h"
#include "markerstyle.h"
#include "xylayers.h"
#include "scalardata.h"
#include "pointtiles.h"
#include "guppi-math.h"
#include "datagroup.h"
#include "datedata.h"

// FIXME should draw the open, open-close range of main bar, then close in a single 
// call to get a nice bevel; then add the ends of the main bar. Avoids ugliness when
// open/close are right on the end.

void 
guppi_draw_price_bar(RGB& rgb, const RGB::Context& ctx,
                     gint time, gint open, gint hi, gint lo, gint close)
{
  RGB::Point pts[6];
  
  // Main bar
  pts[0].x = time;
  pts[0].y = lo;

  pts[1].x = time;
  pts[1].y = hi;

  // open bar on left
  // + 1 to be at least one pixel
  pts[2].x = (gint)(time - ctx.line_width()*1.5 - 1);
  pts[2].y = open;

  pts[3].x = time;
  pts[3].y = open;

  // close bar on right
  pts[4].x = (gint)(time + ctx.line_width()*1.5 + 1);
  pts[4].y = close;

  pts[5].x = time;
  pts[5].y = close;

  rgb.paint_segments(6, pts, ctx);
}

void 
guppi_draw_price_bar_no_close(RGB& rgb, const RGB::Context& ctx,
                              gint time, gint open, gint hi, gint lo)
{
  RGB::Point pts[4];
  
  // Main bar
  pts[0].x = time;
  pts[0].y = lo;

  pts[1].x = time;
  pts[1].y = hi;

  // open bar on left
  // + 1 to be at least one pixel
  pts[2].x = (gint)(time - ctx.line_width()*1.5 - 1);
  pts[2].y = open;

  pts[3].x = time;
  pts[3].y = open;

  rgb.paint_segments(4, pts, ctx);
}

void 
guppi_draw_price_bar_no_open(RGB& rgb, const RGB::Context& ctx,
                             gint time, gint hi, gint lo, gint close)
{
  RGB::Point pts[4];
  
  // Main bar
  pts[0].x = time;
  pts[0].y = lo;

  pts[1].x = time;
  pts[1].y = hi;

  // close bar on right
  pts[2].x = (gint)(time + ctx.line_width()*1.5 + 1);
  pts[2].y = close;

  pts[3].x = time;
  pts[3].y = close;

  rgb.paint_segments(4, pts, ctx);
}

void 
guppi_draw_price_bar_no_open_or_close(RGB& rgb, const RGB::Context& ctx,
                                      gint time, gint hi, gint lo)
{
  rgb.paint_line(time, lo, time, hi, ctx);
}

void
guppi_draw_scatter(XyScatter* layer, 
                   RGB& rgb, const Affine& i2c, 
                   const Transform& xtrans, 
                   const Transform& ytrans,
                   double pixels_per_unit,
                   gint rgb_x0, gint rgb_y0)
{
  const guint N = layer->npoints();

  if (N == 0) 
    return;  

  // We want the inverse of the affine, to determine the redraw 
  //  region
  Affine* c2i = i2c.new_inverted();

  double vx0 = rgb_x0;
  double vx1 = vx0 + rgb.width();
  double vy0 = rgb_y0;
  double vy1 = vy0 + rgb.height();

  // FIXME OK, this is wrong if the affine has any rotation in it.
  // The problem is that the *bounding rectangle* of the
  // inverse-transformed (value-coordinates) microtile covers a larger
  // area than the original microtile, unless the microtile is at 0,
  // 90, 180, 270 degrees.  Can potentially solve this with PointTiles
  // because we will have an efficient way to get only points in the
  // transformed microtile, assuming I can write get_tiles_in_region().

  // However, for now we will bail if the affine has rotation.
#ifdef GNOME_ENABLE_DEBUG
  if (!c2i->rectilinear())
    {
      g_warning("Non-rectilinear affine in %s", __FUNCTION__);
      return;
    }
#endif

  c2i->get_transformed_bounds(&vx0, &vy0, &vx1, &vy1);
  
  delete c2i;
  c2i = 0;

  // Use transforms to go from item to value coords
  vx0 = xtrans.inverse(vx0);
  vy0 = ytrans.inverse(vy0);
  vx1 = xtrans.inverse(vx1);
  vy1 = ytrans.inverse(vy1);

  // Re-order vx* as necessary
  PlotUtil::canonicalize_rectangle(&vx0, &vy0, &vx1, &vy1);

  bool just_translate = i2c.translation_only();

  // Come up with our style stuff, as much as possible 
  // in advance.
  const SortedPair& pair = layer->sorted();

  MarkerPalette* palette = guppi_marker_palette();

  g_assert(palette != 0);

  const MarkerStyle* global_style = 0;
  const guint8* small_styles = 0;
  const guint16* large_styles = 0;
  if (layer->using_global_style())
    {
      global_style = palette->get(layer->global_style());
    }
  else if (layer->using_small_styles())
    {
      small_styles = layer->small_styles();
    }
  else 
    {
      large_styles = layer->large_styles();
    }

  //////////// Draw with tiles

  RGB::Context ctx(rgb);

  PointTiles* tiles = pair.tiles();

  if (tiles != 0)
    {
      Affine::Point p(0,0), p0(0,0), p1(0,0);

      vector<const PointTiles::Tile*> edgetiles;
      vector<const PointTiles::Tile*> intiles;

      // Add edge fuzz based on size of points we're drawing.
      
      const double largest_style = layer->largest_style();

      tiles->tiles_in_rect(edgetiles, intiles, vx0, vy0, vx1, vy1, largest_style);

      vector<const PointTiles::Tile*>::const_iterator ti = edgetiles.begin();
      vector<const PointTiles::Tile*>::const_iterator tend = edgetiles.end();

      // Get the bounds of the plot area, so we can set clipping on our
      // RGB buffer target.
      double sx1, sx2, sy1, sy2;
      
      xtrans.screen_bounds(&sx1,&sx2);
      ytrans.screen_bounds(&sy1,&sy2);

      p0.x = sx1;
      p0.y = sy1;

      p1.x = sx2;
      p1.y = sy2;

      i2c.transform(p0);
      i2c.transform(p1);

      PlotUtil::canonicalize_rectangle(&p0.x, &p0.y, &p1.x, &p1.y);

      int clipx = gint(p0.x - rgb_x0);
      int clipy = gint(p0.y - rgb_y0);

      // Get X and Y into the realm of possibility
      if (clipx < 0)
        clipx = 0;
      if (clipy < 0)
        clipy = 0;

      const int clipw = gint(p1.x - rgb_x0) - clipx;
      const int cliph = gint(p1.y - rgb_y0) - clipy;

      
      if (clipw < 0 || cliph < 0)
        {
          // the redraw region is empty.
          return;
        }
      else 
        {
          // This will truncate clipw and cliph which are almost
          // certainly off the drawing buffer.
          // if X or Y is too large they will also get nuked.
          ctx.clip(clipx, clipy, clipw, cliph);
        }

      if (ctx.clip_area() == 0)
        return; // the intersection of the transform bounds and the buffer was empty

#if 0
      g_debug("%u tiles to render in data region %g, %g  %g, %g", 
              edgetiles.size()+intiles.size(), vx0, vy0, vx1, vy1);
#endif

      while (ti != tend)
        {
          const PointTiles::Tile* t = *ti;


#ifdef GNOME_ENABLE_DEBUG
          static bool draw_tile = false; // turn on with gdb
          
          // breakpoint here.

          if (draw_tile)
            {
              // Draw the tile, for debugging entertainment
            
              gint cx1, cx2, cy1, cy2;

              p.x = xtrans.transform(t->x1);
              p.y = ytrans.transform(t->y1);
            
              i2c.transform(p);
            
              // This is sort of poorly thought-out and should be fixed.
              // Need to be more precise.
              cx1 = (int)rint(p.x);
              cy1 = (int)rint(p.y);
            
              // Convert to buffer indices - could be merged with affine 
              //  transform?
              cx1 -= rgb_x0;
              cy1 -= rgb_y0;

              p.x = xtrans.transform(t->x2);
              p.y = ytrans.transform(t->y2);
            
              i2c.transform(p);
            
              // This is sort of poorly thought-out and should be fixed.
              // Need to be more precise.
              cx2 = (int)rint(p.x);
              cy2 = (int)rint(p.y);
              
              // Convert to buffer indices - could be merged with affine 
              //  transform?
              cx2 -= rgb_x0;
              cy2 -= rgb_y0;
            
              ctx.set_color(Color(255, 0, 0, 128));

              PlotUtil::canonicalize_rectangle(&cx1, &cy1, &cx2, &cy2);
            
              rgb.paint_rect(cx1, cy1, cx2, cy2, ctx);
            }          
#endif


          ////////// Iterate over points in the tile

          vector<guint>::const_iterator ci = t->contents.begin();
          vector<guint>::const_iterator cend = t->contents.end();
          while (ci != cend)
            {
              p.x = pair.real_key_value(*ci);
              p.y = pair.real_other_value(*ci);

              // We no longer do any clipping, because
              // part of the marker for an off-buffer point 
              // might be on the buffer.
              // Instead the drawing routines clip the marker.
#if 0
              if (p.x < vx0 || p.x >= vx1 ||
                  p.y < vy0 || p.y >= vy1)
                {
                  ++ci;
                  continue;
                }
#endif

              // This moves point from a data value to canvas pixel coords

              p.x = xtrans.transform(p.x);
              p.y = ytrans.transform(p.y);

              if (just_translate)
                {
                  i2c.translate_transform(p);
                }
              else 
                {
                  i2c.transform(p);
                }

              // This is sort of poorly thought-out and should be fixed.
              // Need to be more precise.
              int cx = (int)rint(p.x);
              int cy = (int)rint(p.y);

              // Convert to buffer indices - could be merged with affine 
              //  transform?
              cx -= rgb_x0;
              cy -= rgb_y0;

              const MarkerStyle* s = 0;
              if (global_style) s = global_style;
              else if (small_styles)
                {
                  guint reali = *ci;
                  g_warn_if_fail(reali < N);
                  s = palette->get(small_styles[reali]);
                }
              else if (large_styles)
                {
                  guint reali = *ci;
                  g_warn_if_fail(reali < N);
                  s = palette->get(large_styles[reali]);
                }

              Color c;
              MarkerStyle::Type t = MarkerStyle::DOT;

              if (s)
                {
                  c = s->color();
                  t = s->type();
                }

              ctx.set_color(c);
              ctx.set_filled(false);
              
              if (t == MarkerStyle::DOT)
                {
                  // optimize this case, since size is irrelevant
                  rgb.paint_pixel(cx, cy, ctx);
                }      
              else 
                {
                  g_assert(s != 0); // because we default to DOT if no style

                  double markersize = 1.0;
                  if (s) 
                    markersize = s->size();

                  // The +0.5 is fine instead of rint() since markersize is always positive
                  const int cmarkerhalf = int(markersize*pixels_per_unit/2+0.5);

                  switch (t)
                    {
                    case MarkerStyle::FILLED_BOX:
                      ctx.set_filled(true); // fall thru
                    case MarkerStyle::BOX:
                      {
                        rgb.paint_rect(cx - cmarkerhalf, cy - cmarkerhalf,
                                       cx + cmarkerhalf, cy + cmarkerhalf,
                                       ctx);
                      }
                      break;

                    case MarkerStyle::FILLED_CIRCLE:
                      ctx.set_filled(true); // fall thru
                    case MarkerStyle::CIRCLE:
                      {
                        rgb.paint_arc(cx - cmarkerhalf, cy - cmarkerhalf, 
                                      cmarkerhalf*2, cmarkerhalf*2,
                                      0, 360*64, ctx);
                        
                      }
                      break;

                    case MarkerStyle::FILLED_DIAMOND:
                      ctx.set_filled(true); // fall thru
                    case MarkerStyle::DIAMOND:
                      {
                        const RGB::Point pts[5] = { 
                          { cx - cmarkerhalf, cy }, 
                          { cx, cy + cmarkerhalf },
                          { cx + cmarkerhalf, cy },
                          { cx, cy - cmarkerhalf },
                          { cx - cmarkerhalf, cy } 
                        };

                        rgb.paint_convex_poly(5, pts, ctx);
                      }
                      break;
                      
                    case MarkerStyle::PLUS:
                      rgb.paint_line(cx - cmarkerhalf, cy, cx + cmarkerhalf, cy, ctx);
                      rgb.paint_line(cx, cy + cmarkerhalf, cx, cy - cmarkerhalf, ctx);
                      break;
                      
                    case MarkerStyle::TIMES:
                      rgb.paint_line(cx - cmarkerhalf, cy + cmarkerhalf, cx + cmarkerhalf, cy - cmarkerhalf, ctx);
                      rgb.paint_line(cx + cmarkerhalf, cy + cmarkerhalf, cx - cmarkerhalf, cy - cmarkerhalf, ctx);
                      break;
                                          
                    case MarkerStyle::HORIZONTAL_TICK:
                      rgb.paint_line(cx - cmarkerhalf, cy, cx + cmarkerhalf, cy, ctx);
                      break;
                                      
                    case MarkerStyle::VERTICAL_TICK: 
                      rgb.paint_line(cx, cy + cmarkerhalf, cx, cy - cmarkerhalf, ctx);
                      break;

                    default:
                      g_warning("Style type not yet supported by Gnome frontend!");
                      break;
                    }
                }

              ++ci;
            }

          ++ti;

          // HACK for now, FIXME (we need to loop over two vectors, but intiles
          //  can be done more efficiently than edgetiles)
          if (ti == edgetiles.end())
            {
              ti = intiles.begin();
              tend = intiles.end();
            }
        }
    }
}


// barely worth it...
static inline void
data2buf(double data_x, double data_y, 
         gint* buf_x, gint* buf_y,
         const Transform& xtrans, 
         const Transform& ytrans,
         const Affine& i2c, 
         gint rgb_x0, gint rgb_y0)
{
  Affine::Point p(xtrans.transform(data_x),
                  ytrans.transform(data_y));

  i2c.transform(p);

  *buf_x = (int)rint(p.x);
  *buf_y = (int)rint(p.y);

  *buf_x -= rgb_x0;
  *buf_y -= rgb_y0;
}     

void 
guppi_draw_pricebars(XyPriceBars* layer, 
                     RGB& rgb, const Affine& i2c,
                     const Transform& xtrans, 
                     const Transform& ytrans,
                     double pixels_per_unit,
                     gint rgb_x0, gint rgb_y0)
{
  const guint N = layer->nbars();

  if (N == 0) 
    return;  

  // We want the inverse of the affine, to determine the redraw 
  //  region
  Affine* c2i = i2c.new_inverted();


  double vx0 = rgb_x0;
  double vx1 = vx0 + rgb.width();
  double vy0 = rgb_y0;
  double vy1 = vy0 + rgb.height();

  // FIXME OK, this is wrong if the affine has any rotation in it.
  // The problem is that the *bounding rectangle* of the
  // inverse-transformed (value-coordinates) microtile covers a larger
  // area than the original microtile, unless the microtile is at 0,
  // 90, 180, 270 degrees.  Can potentially solve this with PointTiles
  // because we will have an efficient way to get only points in the
  // transformed microtile, assuming I can write get_tiles_in_region().

  // However, for now we will bail if the affine has rotation.
#ifdef GNOME_ENABLE_DEBUG
  if (!c2i->rectilinear())
    {
      g_warning("Non-rectilinear affine in %s", __FUNCTION__);
      return;
    }
#endif

  c2i->get_transformed_bounds(&vx0, &vy0, &vx1, &vy1);
  
  delete c2i;
  c2i = 0;

  bool just_translate = i2c.translation_only();

  //// Extract time and price data

  DataGroup* price_data_group = layer->data()->cast_to_group();

  if (price_data_group == 0)
    return;

  Data* time_data = price_data_group->get_data((int)PriceDates); 

  const Data* hi_d = price_data_group->get_data((int)PriceHighs);
  const Data* lo_d = price_data_group->get_data((int)PriceLows);
  const Data* open_d = price_data_group->get_data((int)PriceOpens);
  const Data* close_d = price_data_group->get_data((int)PriceCloses);

  const ScalarData* hi_data = hi_d ? hi_d->cast_to_scalar() : 0;
  const ScalarData* lo_data = lo_d ? lo_d->cast_to_scalar() : 0;
  const ScalarData* open_data = open_d ? open_d->cast_to_scalar() : 0;
  const ScalarData* close_data = close_d ? close_d->cast_to_scalar() : 0;
  
  // Hi and lo are required; open/close are optional
  if (hi_data == 0   || lo_data == 0)
    return;
  
  // If there's no time data we just use the index of each price quad

  ScalarData* scalar_time_data = time_data ? time_data->cast_to_scalar() : 0;
  DateData* date_time_data = time_data ? time_data->cast_to_date() : 0;

  // We should have only one or the other
  g_assert((scalar_time_data && !date_time_data) || (date_time_data && !scalar_time_data));

  const double* sorted_time = scalar_time_data ? scalar_time_data->sorted_scalars() : 0;

  const double* hi = hi_data->scalars();
  const double* lo = lo_data->scalars();
  const double* open = open_data ? open_data->scalars() : 0;
  const double* close = close_data ? close_data->scalars() : 0;

  // if the datasets are empty this will happen
  if (hi == 0 || lo == 0)
    return;


  ///// Set up clipping

  RGB::Context ctx(rgb);

  int units_per_screen = (int)rint((1.0 / (xtrans.pixels_per_unit())) * xtrans.screen_size());

  gint line_width = units_per_screen > (12*30) ? 1 : 12 - (units_per_screen / 30); // 12 for 30 ups, 11 for 60, etc.
  
  line_width = CLAMP(line_width, 1, 5);

  ctx.set_line_width(line_width);

  // Get the bounds of the plot area, so we can set clipping on our
  // RGB buffer target.
  double sx1, sx2, sy1, sy2;
  Affine::Point p0, p1;
      
  xtrans.screen_bounds(&sx1,&sx2);
  ytrans.screen_bounds(&sy1,&sy2);

  p0.x = sx1;
  p0.y = sy1;

  p1.x = sx2;
  p1.y = sy2;

  i2c.transform(p0);
  i2c.transform(p1);

  PlotUtil::canonicalize_rectangle(&p0.x, &p0.y, &p1.x, &p1.y);

  int clipx = gint(p0.x - rgb_x0);
  int clipy = gint(p0.y - rgb_y0);

  // Get X and Y into the realm of possibility
  if (clipx < 0)
    clipx = 0;
  if (clipy < 0)
    clipy = 0;

  const int clipw = gint(p1.x - rgb_x0) - clipx;
  const int cliph = gint(p1.y - rgb_y0) - clipy;

      
  if (clipw < 0 || cliph < 0)
    {
      // the redraw region is empty.
      return;
    }
  else 
    {
      // This will truncate clipw and cliph which are almost
      // certainly off the drawing buffer.
      // if X or Y is too large they will also get nuked.
      ctx.clip(clipx, clipy, clipw, cliph);
    }

  if (ctx.clip_area() == 0)
    return; // the intersection of the transform bounds and the buffer was empty

  //////////// Draw the bars

  // Optimize me - we can skip before and stop after the clip area.
  // Could raise open/close tests out of the loop

  g_assert(time_data == 0 || N <= time_data->size());
  g_assert(hi != 0);
  g_assert(lo != 0);

  guint pt = 0;

  while (pt < N)
    {
      gint open_y, close_y, hi_y, lo_y;
      gint time_x;

      double timeval;

      if (sorted_time != 0)
        timeval = sorted_time[pt];
      else if (date_time_data != 0)
        timeval = date_time_data->get_date_julian(pt);
      else
        timeval = double(pt);

      data2buf(timeval, hi[pt],
               &time_x, &hi_y,
               xtrans, ytrans, i2c, rgb_x0, rgb_y0);

      data2buf(timeval, lo[pt],
               &time_x, &lo_y,
               xtrans, ytrans, i2c, rgb_x0, rgb_y0);

      if (open != 0)
        data2buf(timeval, open[pt],
                 &time_x, &open_y,
                 xtrans, ytrans, i2c, rgb_x0, rgb_y0);

      if (close != 0)
        data2buf(timeval, close[pt],
                 &time_x, &close_y,
                 xtrans, ytrans, i2c, rgb_x0, rgb_y0);

      Color c;

      ctx.set_color(c);

      if (open && close)
        guppi_draw_price_bar(rgb, ctx, time_x,
                             open_y, hi_y, lo_y, close_y);
      else if (open)
        guppi_draw_price_bar_no_close(rgb, ctx, time_x,
                                      open_y, hi_y, lo_y);
      else if (close)
        guppi_draw_price_bar_no_open(rgb, ctx, time_x,
                                     hi_y, lo_y, close_y);      
      else
        guppi_draw_price_bar_no_open_or_close(rgb, ctx, time_x,
                                              hi_y, lo_y);


      ++pt;
    }  
}


