// -*- C++ -*-

/* 
 * barplotstate.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_BARPLOTSTATE_H
#define GUPPI_BARPLOTSTATE_H

#include "util.h"
#include "data.h"
#include "datastore.h"
#include "plotutils.h"
#include "plotstate.h"

#include <vector>
#include <string>

class ScalarData;
class BarPlotState;
class LabelData;

class Bar {
public:
  Bar(BarPlotState* s) : state_(s), index_(0), 
    width_(10.0), pos_(0.0), color_(255,0,0)
    {}
  ~Bar() {}

  // "top" of bar in display coords (canvas item)
  double top() const;

  double width() const { return width_; }

  // Position relative to left side (if the bar plot is oriented normally)
  double pos() const { return pos_; }

  const Color & color() const { return color_; }

  // Setting should only be done by BarPlotState
  void set_color(const Color & c) { color_ = c; }
  void set_index(guint i) { index_ = i; }
  void set_width(double w) { width_ = w; }
  void set_pos(double p) { pos_ = p; }

private:
  BarPlotState* state_;
  guint index_; // which value in the ScalarData we represent
  double width_;
  double pos_;
  Color color_;
};

////////////////////////////

class BarPlotState : public PlotState, public Data::View {
public:
  BarPlotState(DataStore* ds);
  virtual ~BarPlotState();

  void set_data(Data* d);
  ScalarData* data();

  void set_labels(Data* d);
  LabelData* labels(); 
  
  DataStore* store() { return store_; }

  const Transform& trans() const { return trans_; }

  // position of any baseline; must be between 0.0 and width/height
  // FIXME hardcoded hack for now
  double baseline() const { return CLAMP(trans_.transform(0.0), 0.0, height_); }

  typedef vector<Bar>::iterator iterator;
  typedef vector<Bar>::const_iterator const_iterator;
  
  iterator begin() { return bars_.begin(); }
  iterator end() { return bars_.end(); }
  const_iterator begin() const { return bars_.begin(); }
  const_iterator end() const { return bars_.end(); }
  
  gsize size() { return bars_.size(); }

  const Color & background() const { return background_; }

  void set_background(const Color & c) { 
    background_ = c; 
    state_model.background_changed(this, background_); 
  }

  const string& name() const;
  void set_name(const string & name);

  // Whether to automatically set the name when the data changes
  bool name_from_data() const;
  void set_name_from_data(bool setting);

  // Width/Height are in display coords (e.g. canvas item coords)
  double width() const { return width_; }
  double height() const { return height_; }

  void set_width(double w); 
  void set_height(double h);

  void size_request(double* w, double* h);

  // The assigned height and width gets subdivided,
  // area is assigned to each component
  const Rectangle & axis_rect() const;
  const Rectangle & plot_rect() const;

  Axis* checkout_axis();
  void checkin_axis(Axis* a);

  const Axis& axis() const { return axis_; }

  double spacing() const { return spacing_; }
  void set_spacing(double spacing) { spacing_ = spacing; rebuild_bars(); }

  // Data::View

  virtual void change_values(Data* d);

  virtual void change_values(Data* d, const vector<guint> & which);

  virtual void change_name(Data* d, const string & name);

  // ::View

  virtual void destroy_model();

  class View : public ::View 
  {
  public:
    virtual void change_bars(BarPlotState* state) = 0;
    virtual void change_data(BarPlotState* state, Data* data) = 0;
    virtual void change_background(BarPlotState* state, const Color & bg) = 0;
    virtual void change_size(BarPlotState* state, double width, double height) = 0;
    virtual void change_name(BarPlotState* state, const string& name) = 0;
  };

  class Model : public ::Model<BarPlotState::View*> {
  public:
    void bars_changed(BarPlotState* state) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_bars(state);
	++i;
      }
      unlock_views();
    }
    void data_changed(BarPlotState* state, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data(state,d);
	++i;
      }
      unlock_views();
    }
    void background_changed(BarPlotState* state, const Color & bg) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_background(state,bg);
	++i;
      }
      unlock_views();
    }
    void size_changed(BarPlotState* state, double width, double height) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_size(state,width,height);
	++i;
      }
      unlock_views();
    }

    void name_changed(BarPlotState* state, const string & name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_name(state,name);
	++i;
      }
      unlock_views();
    }
  };
  
  Model state_model;  

private:
  ScalarData* data_;
  LabelData* labels_;

  DataStore* store_;

  vector<Bar> bars_;

  Color background_;

  double width_;
  double height_;

  // Goes from data values to item coords; always anchored at
  // plot_rect_.x, plot_rect_.y We use it to get bar height rather
  // than bar position.  Set to translate into the plot rectangle.
  Transform trans_;

  double spacing_;

  Axis axis_;

  bool axis_checked_out_;

  Rectangle axis_rect_;
  Rectangle plot_rect_;

  string name_;

  bool set_name_from_data_;

  void rebuild_bars();
  void grab_data(Data* d);
  void release_data();
  void grab_labels(Data* d);
  void release_labels();
  void recalc_layout();
  void update_name_from_data();
};

#endif
