// -*- C++ -*-

/* 
 * xyplotstate.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_XYPLOTSTATE_H
#define GUPPI_XYPLOTSTATE_H

#include "xylayers.h"
#include "plotstate.h"

///////////////////////////////////////////////

// OneAxis is internal to XYAxes but isn't an inner class 
//  since XYAxes is a subclass of OneAxis::View

class OneAxis {
public:
  OneAxis();
  ~OneAxis();

  OneAxis* clone();

  // To properly emit the "axis changed" signal, we need to do this
  // checkout thing. Views are notified of change on checkin.
  Axis* checkout_axis();

  void checkin_axis(Axis* a);

  const Axis& axis() const { return axis_; }

  void recalc_ticks(guint nticks);

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

  class View : public ::View 
  {
  public:  
    virtual void change_axis(OneAxis* oa, const Axis& axis) = 0;
  };

  class Model : public ::Model<OneAxis::View*> {
  public:
    void axis_changed(OneAxis* oa, const Axis & axis) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_axis(oa, axis);
	++i;
      }
      unlock_views();
    }
  };
  
  Model one_model;
  
private:
  RC rc_;
  Axis axis_;
  bool axis_checked_out_;
};

class XyAxes : public OneAxis::View {
public:
  XyAxes();
  virtual ~XyAxes();

  // Manipulate the stack of axis bounds; used to zoom in/out.
  void push_axis_bounds(vector<PlotUtil::PositionType>& axes,
                        vector<double>& starts,
                        vector<double>& stops);

  guint axis_bounds_stack_size() const;

  void pop_axis_bounds();

  // To properly emit the "axis changed" signal, we need to do this
  // checkout thing. Views are notified of change on checkin.
  Axis* checkout_axis(PlotUtil::PositionType pos);
  void checkin_axis(PlotUtil::PositionType pos, Axis* a);

  const Axis& axis(PlotUtil::PositionType pos) const;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

  void recalc_ticks(guint nxticks, guint nyticks);

  // OneAxis view
  virtual void change_axis(OneAxis* oa, const Axis& axis);
  virtual void destroy_model();

  class View : public ::View 
  {
  public:  
    virtual void change_axis(XyAxes* axes, PlotUtil::PositionType pos, 
                             const Axis& axis) = 0;
  };

  class Model : public ::Model<XyAxes::View*> {
  public:
    void axis_changed(XyAxes* axes, PlotUtil::PositionType pos, 
                      const Axis & axis) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_axis(axes,pos,axis);
	++i;
      }
      unlock_views();
    }
  };
  
  Model axes_model;  

  void share_with(PlotUtil::PositionType pos, XyAxes* partner);

  void hoard(PlotUtil::PositionType pos);

protected:
  OneAxis* one(PlotUtil::PositionType pos);

  void set_one(PlotUtil::PositionType pos, OneAxis* oa);

private:
  RC rc_;

  OneAxis* axes_[PlotUtil::PositionTypeEnd];

  // To save a stack of zoom regions
  struct AxisBounds
  {
    double starts[PlotUtil::PositionTypeEnd];
    double stops[PlotUtil::PositionTypeEnd];

    // positions in the above that are actually valid
    vector<PlotUtil::PositionType> set_axes;
    
    AxisBounds();

  };

  vector<AxisBounds> axis_bounds_stack_;

  void set_axis_bounds(const AxisBounds& ab);

  void grab_axis(OneAxis* oa);
  void release_an_axis(OneAxis** oa);
};


///////////////////////////////////////

class XyPlotState : public PlotState,
                    public XyAxes::View
{
public:
  XyPlotState(DataStore* ds);
  virtual ~XyPlotState();
    
  DataStore* store();

  typedef vector<XyLayer*>::iterator iterator;
  typedef vector<XyLayer*>::const_iterator const_iterator;

  // Iteration goes from the bottom to the top of the stack.

  iterator begin() { return layers_.begin(); }
  const_iterator begin() const { return layers_.begin(); }
  iterator end() { return layers_.end(); }
  const_iterator end() const { return layers_.end(); }

  // O(n) time
  iterator find(XyLayer* layer);
  const_iterator find(XyLayer* layer) const; 

  // Layers are added to the top of the stack.
  void add_layer(XyLayer* layer);
  void add_layer_no_auto_axis(XyLayer* layer);

  void lower_layer(iterator i); 
  void raise_layer(iterator i);
  void remove_layer(iterator i); // unreferences it - may destroy if no other refs...
  
  guint nlayers() const { return layers_.size(); }
  
  const Color & background() const;

  void set_background(const Color & c);

  const string& name() const;
  void set_name(const string & name);

  // Whether to automatically set the name when the data changes
  //  (name will be set to "X Data Name vs. Y Data Name" for example)
  // Depends on which layers are on the stack
  bool name_from_data() const;
  void set_name_from_data(bool setting);

  // Manipulate the stack of axis bounds; used to zoom in/out.

  void push_axis_bounds(vector<PlotUtil::PositionType>& axes,
                        vector<double>& starts,
                        vector<double>& stops);

  guint axis_bounds_stack_size() const;

  void pop_axis_bounds();

  // To properly emit the "axis changed" signal, we need to do this
  // checkout thing. Views are notified of change on checkin.
  Axis* checkout_axis(PlotUtil::PositionType pos);
  void checkin_axis(PlotUtil::PositionType pos, Axis* a);

  const Axis& axis(PlotUtil::PositionType pos) const;

  const Transform& trans(PlotUtil::PositionType pos) const;

  // Width/Height are in display coords (e.g. canvas item coords)
  double width() const { return width_; }
  double height() const { return height_; }

  void set_width(double w); 
  void set_height(double h);

  void size_request(double* w, double* h);

  // The assigned height and width gets subdivided,
  // area is assigned to each component
  const Rectangle & plot_rect() const; // center area

  const Rectangle & axis_rect(PlotUtil::PositionType pos) const; // axes around outside
  const Rectangle & axis_label_rect(PlotUtil::PositionType pos) const; // labels around axes

  // Iterate over layers, and get them all on the screen.
  void optimize_axis(PlotUtil::PositionType pos);
  void optimize_all_axes();

  // Optimize the two axes this layer uses for this layer, if possible.
  void optimize_layers_axes(XyLayer* layer); 
  // Just one of the axes
  void optimize_layers_axis(XyLayer* layer, PlotUtil::PositionType pos);

  // If found, return true and put them in fillme
  bool find_unused_axes(vector<PlotUtil::PositionType>& fillme);

  //// Sharing and Caring

  // Delete partner's whatever and install ours instead,
  //  so we are sharing.

  void share_axis_with(PlotUtil::PositionType pos, XyPlotState* partner);

  // Unref our whatever and install a newly-created one, 
  //  so we aren't sharing with any loser partners
  
  void hoard_axis(PlotUtil::PositionType pos);


  // XyAxes::View
  virtual void change_axis(XyAxes* axes, PlotUtil::PositionType pos, const Axis& axis);

  // ::View
  virtual void destroy_model();


  class View : public ::View 
  {
  public:
    virtual void change_background(XyPlotState* state, const Color & bg) = 0;
    virtual void change_axis(XyPlotState* state, 
                             PlotUtil::PositionType pos, 
                             const Axis& axis) = 0;
    // change_size() is actually change_layout() - width/height may be the same
    //  but the internal areas are different.
    virtual void change_size(XyPlotState* state, double width, double height) = 0;
    virtual void change_name(XyPlotState* state, const string& name) = 0;

    // Contents or order of the layer stack changed
    virtual void remove_layer(XyPlotState* state, XyLayer* layer) = 0;
    virtual void add_layer(XyPlotState* state, XyLayer* layer) = 0;
    virtual void raise_layer(XyPlotState* state, XyLayer* layer) = 0;
    virtual void lower_layer(XyPlotState* state, XyLayer* layer) = 0;
  };

  class Model : public ::Model<XyPlotState::View*> {
  public:
    void background_changed(XyPlotState* state, const Color & bg) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_background(state,bg);
	++i;
      }
      unlock_views();
    }

    void axis_changed(XyPlotState* state, PlotUtil::PositionType pos, 
                      const Axis & axis) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_axis(state,pos,axis);
	++i;
      }
      unlock_views();
    }

    void size_changed(XyPlotState* state, double width, double height) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_size(state,width,height);
	++i;
      }
      unlock_views();
    }

    void name_changed(XyPlotState* state, const string & name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_name(state,name);
	++i;
      }
      unlock_views();
    }
    void layer_added(XyPlotState* state, XyLayer* layer) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->add_layer(state, layer);
	++i;
      }
      unlock_views();
    }
    void layer_removed(XyPlotState* state, XyLayer* layer) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->remove_layer(state, layer);
	++i;
      }
      unlock_views();
    }
    void layer_raised(XyPlotState* state, XyLayer* layer) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->raise_layer(state, layer);
	++i;
      }
      unlock_views();
    }

    void layer_lowered(XyPlotState* state, XyLayer* layer) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->lower_layer(state, layer);
	++i;
      }
      unlock_views();
    }



  };
  
  Model state_model;  

protected:

private:
  DataStore* store_;

  XyAxes* axes_;

  RC rc_;

  Color background_;

  // in display coords (e.g. canvas item coords)
  double width_; 
  double height_; 

  bool need_recalc_layout_;
  Rectangle axis_rects_[PlotUtil::PositionTypeEnd];
  Rectangle axis_label_rects_[PlotUtil::PositionTypeEnd];
  Rectangle plot_rect_;

  // Like the BarPlot, these go from data values to a coordinate system 
  //  beginning in the top left with 0.0,0.0 and covering the 
  //  plot's entire size allocation.
  Transform transforms_[PlotUtil::PositionTypeEnd];

  string name_;

  bool set_name_from_data_;

  vector<XyLayer*> layers_;

  void update_name_from_data();
  void queue_layout();
  void recalc_layout();

  void grab_xyaxes(XyAxes* axes);

  void release_xyaxes();

  void grab_layer(XyLayer* layer);

  void release_layer(XyLayer* layer);

};


#endif
