// -*- C++ -*-

/* 
 * canvas-pricebars.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_CANVASPRICEBARS_H
#define GUPPI_CANVASPRICEBARS_H

#include <libgnomeui/gnome-canvas.h>

#include "plotutils.h"
#include "xyplotstate.h"

// there is no reason to maintain strict C in here, but keep C++-isms
//  to a minimum just in case.

#define GUPPI_TYPE_PRICEBARS            (guppi_pricebars_get_type ())
#define GUPPI_PRICEBARS(obj)            (GTK_CHECK_CAST ((obj), GUPPI_TYPE_PRICEBARS, GuppiPricebars))
#define GUPPI_PRICEBARS_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GUPPI_TYPE_PRICEBARS, GuppiPricebarsClass))
#define GUPPI_IS_PRICEBARS(obj)         (GTK_CHECK_TYPE ((obj), GUPPI_TYPE_PRICEBARS))
#define GUPPI_IS_PRICEBARS_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GUPPI_TYPE_PRICEBARS))

struct _GuppiPricebars;

// This is a callback used to retrieve the info for various operations.

typedef void (*PricebarsInfoFunc)(struct _GuppiPricebars* gs, 
                                  gpointer user_data, 
                                  XyPlotState** state,
                                  XyPriceBars** layer);

typedef struct _GuppiPricebars GuppiPricebars;
typedef struct _GuppiPricebarsClass GuppiPricebarsClass;

struct _GuppiPricebars {
  GnomeCanvasItem item;
  
  PricebarsInfoFunc get_info;
  gpointer info_data;

  // Since we compute the rectangle to render in the render function,
  //  we have to store this in update().
  double a[6];
};

struct _GuppiPricebarsClass {
  GnomeCanvasItemClass parent_class;
  
  
};


/* Standard Gtk function */
GtkType guppi_pricebars_get_type (void);

void guppi_pricebars_set_info_func(GuppiPricebars* gs, 
                                   PricebarsInfoFunc func,
                                   gpointer user_data);


#endif
