// -*- C++ -*-

/* 
 * barplotedit.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "barplotedit.h"
#include "scalardata.h"

BarPlotEdit::BarPlotEdit()
  : dialog_(0), state_(0), changing_data_(false)
{
  // We never disconnect since the DataOption dies with us.
  data_option_.connect(data_chosen_cb, this);
}

BarPlotEdit::~BarPlotEdit()
{
  release_state();
}

void
BarPlotEdit::edit()
{
  if (dialog_ != 0) return;

  dialog_ = gnome_dialog_new(_("Bar Plot Editor"),
                             GNOME_STOCK_BUTTON_CLOSE,
                             NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog_), TRUE);

  gtk_window_set_policy(GTK_WINDOW(dialog_), TRUE, TRUE, FALSE);

  gtk_signal_connect(GTK_OBJECT(dialog_),
                     "close",
                     GTK_SIGNAL_FUNC(close_cb),
                     this);

  GtkWidget* label = gtk_label_new(_("Data: "));
  
  GtkWidget* hbox = gtk_hbox_new(GNOME_PAD, FALSE);
  
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_end(GTK_BOX(hbox), data_option_.widget(), TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox), 
                     hbox, FALSE, FALSE, GNOME_PAD_SMALL);

  gtk_widget_show_all(dialog_);
}

void 
BarPlotEdit::change_bars(BarPlotState* state)
{
  if (dialog_ == 0) return;
  
  g_warning(__FUNCTION__);
}

void 
BarPlotEdit::change_data(BarPlotState* state, Data* data)
{
  if (changing_data_) return;

  data_option_.set_data(data);
}

void 
BarPlotEdit::change_background(BarPlotState* state, const Color & bg)
{
  // FIXME
}

void 
BarPlotEdit::change_size(BarPlotState* state, double width, double height)
{
  // FIXME
}

void 
BarPlotEdit::change_name(BarPlotState* state, const string& name)
{
  // FIXME
}

void 
BarPlotEdit::destroy_model()
{
  if (dialog_ != 0)
    {
      close();
    }
  state_ = 0;
}

void 
BarPlotEdit::set_state(BarPlotState* state)
{
  release_state();

  state_ = state;

  if (state_ != 0)
    {
      // We don't ref/unref the state; the BarPlot keeps up with that.
      state_->state_model.add_view(this);
      data_option_.set_store(state_->store());
    }
}

void
BarPlotEdit::release_state()
{
  if (state_ != 0)
    {
      state_->state_model.remove_view(this);
      data_option_.set_store(0);
      state_ = 0;
    }
}

void 
BarPlotEdit::data_chosen_cb(gpointer emitter, gpointer data)
{
  BarPlotEdit* bpe = static_cast<BarPlotEdit*>(data);

  bpe->data_chosen();
}

void 
BarPlotEdit::data_chosen()
{
  g_return_if_fail(state_ != 0);
  changing_data_ = true;
  state_->set_data(data_option_.get_data()); 
  changing_data_ = false;
}

gint 
BarPlotEdit::close_cb(GtkWidget* w, gpointer data)
{
  BarPlotEdit* bpe = static_cast<BarPlotEdit*>(data);
  
  return bpe->close();
}

gint 
BarPlotEdit::close()
{
  gtk_widget_destroy(dialog_);
  dialog_ = 0;

  return TRUE;
}
