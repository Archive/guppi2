// -*- C++ -*-

/* 
 * barplotedit.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_BARPLOTEDIT_H
#define GUPPI_BARPLOTEDIT_H

#include "barplotstate.h"
#include "dataoption.h"

#include <map>

#include "gnome-util.h"

class BarPlotEdit : public BarPlotState::View
{
public:
  BarPlotEdit();
  virtual ~BarPlotEdit();

  virtual void change_bars(BarPlotState* state);
  virtual void change_data(BarPlotState* state, Data* data);
  virtual void change_background(BarPlotState* state, const Color & bg);
  virtual void change_size(BarPlotState* state, double width, double height);
  virtual void change_name(BarPlotState* state, const string& name);  
  virtual void destroy_model();

  void set_state(BarPlotState* state);

  void edit();

private:
  GtkWidget* dialog_;

  BarPlotState* state_;

  DataOption data_option_;

  // We are the ones setting the current Data in the 
  // BarPlotState 
  bool changing_data_;

  static void data_chosen_cb(gpointer emitter, gpointer data);
  void data_chosen();

  static gint close_cb(GtkWidget* w, gpointer data);
  gint close();

  void release_state();
};

#endif
