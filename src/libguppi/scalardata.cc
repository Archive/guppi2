// -*- C++ -*-

/* 
 * scalardata.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "scalardata.h"
#include "xmltags.h"

#include <goose/RealSet.h>

#include "plotutils.h"
#include "pointtiles.h"

ScalarData::ScalarData()
  : Data(Scalar), set_(0), checked_out_(false)
{
  set_ = new RealSet;
}

ScalarData::ScalarData(RealSet* rs)
  : Data(Scalar), set_(rs), checked_out_(false)
{
  
}

ScalarData::~ScalarData()
{
#ifdef GNOME_ENABLE_DEBUG
  if (checked_out_) 
    g_warning("ScalarData deleted with outstanding checkout!");
#endif

  if (set_ != 0) 
    delete set_;
}

gsize 
ScalarData::size() const
{
  return set_->size();
}

const double* 
ScalarData::scalars() const
{
  return set_->data();
}

double 
ScalarData::min() const
{
  return set_->min();
}

double 
ScalarData::max() const
{
  return set_->max();
}

const double* 
ScalarData::sorted_scalars()
{
  return set_->sorted_data();
}

void
ScalarData::add(double d)
{
  set_->add(d);
  data_model.values_changed(this);
}

const string& 
ScalarData::name() const
{
  // The cast calls the label() which returns a reference 
  // instead of the one that returns a copy.
  return const_cast<const RealSet*>(set_)->label();
}

void 
ScalarData::set_name(const string & name)
{
  set_->set_label(name);
  data_model.name_changed(this, name);
}

void 
ScalarData::set_scalar(size_t index, double value)
{
  g_return_if_fail(index < set_->size());

  set_->set(index, value);

  vector<guint> which;
  which.push_back(index);
  data_model.values_changed(this, which);
}

double 
ScalarData::get_scalar(size_t index) const
{
  g_return_val_if_fail(index < set_->size(), 0.0);

  return set_->data()[index];
}


RealSet* 
ScalarData::checkout_realset()
{
  if (checked_out_) 
    return 0;
  else 
    {
      checked_out_ = true;
      return set_;
    }
}

void     
ScalarData::checkin_realset(RealSet* set, bool values_changed, bool other_changed)
{
  g_return_if_fail(checked_out_);
  g_return_if_fail(set == set_);

  if (values_changed)
    {
      data_model.values_changed(this);
    }
  if (other_changed)
    {
      data_model.name_changed(this, set_->label());
    }
  
  checked_out_ = false;
}

gsize 
ScalarData::byte_size() 
{ 
  return sizeof(ScalarData) + sizeof(RealSet); 
}

xmlNodePtr 
ScalarData::xml(xmlNodePtr parent) const
{
  xmlNodePtr data = 0;
  xmlNodePtr element = 0;

  data = xmlNewChild(parent, NULL, SCALAR_DATA_TAG, NULL);
  xmlSetProp(data, (const guchar*)"name", 
             (const xmlChar*)const_cast<const RealSet*>(set_)->label().c_str());
  
  gsize i = 0;
  gsize N = set_->size();
  while (i < N)
    {
      gchar buf[128];
      g_snprintf(buf,128,"%g",set_->data(i));
      string val = buf;
      
      element = xmlNewChild(data, NULL, DATA_ELEMENT_TAG, 
                            (const guchar*)val.c_str());

      ++i;
    }

  return data;
}

void 
ScalarData::set_xml(xmlNodePtr node)
{
  g_return_if_fail(node != 0);
  
  if (node->name == 0 || (g_ustrcmp(node->name, SCALAR_DATA_TAG) != 0))
    {
      g_warning("Bad node to ScalarData::set_xml()");
      return;
    }

  set_->clear();

  const gchar* nm = (const gchar*)xmlGetProp(node, (const guchar*)"name");

  if (nm != 0)
    {
      set_name(nm);
    }

  xmlNodePtr child = node->childs;
  while (child != 0)
    {
      if (child->name != 0)
        {
          if (g_ustrcmp(child->name, DATA_ELEMENT_TAG) == 0)
            {
              // The child of the element tag is its text content
              if (child->childs && child->childs->content)
                {
                  gchar* endptr = 0;
                  double num;
                  num = g_strtod((const gchar*)child->childs->content,
				 &endptr);
                  if (child->childs->content != (guchar*)endptr)
                    {
                      add(num);
                    }
                  else
                    {
                      g_warning("Didn't understand `%s' (expected real number)",
                                child->childs->content);
                    }
                }
              else
                {
                  g_warning("no content to element");
                }
            }
          else 
            {
              g_warning("weird name: %s", child->name);
            }
        }
      else 
        {
          g_warning("nameless child");
        }
      
      child = child->next;
    }
}


////////////////////////////// SortedPair

// Throughout, remember that keydata might be the same as otherdata, in which 
//  case we can optimize. This isn't done right now...

SortedPair::SortedPair()
  : keydata_(0), otherdata_(0),  
    real2sorted_(0), sorted2real_(0),
    keysorted_(0), othersorted_(0), tiles_(0)
{
  

}

SortedPair::~SortedPair()
{
  release_data(&keydata_);
  release_data(&otherdata_);

  clear();
}

void 
SortedPair::set_key(ScalarData* data)
{
  if (data == keydata_) return;

  release_data(&keydata_);

  keydata_ = data;

  clear();

  grab_data(&keydata_);
}

void 
SortedPair::set_other(ScalarData* data)
{
  if (data == otherdata_) return;

  release_data(&otherdata_);

  otherdata_ = data;

  clear();

  grab_data(&otherdata_);
}

#if 0
const double* 
SortedPair::keydata() const
{
  if (keydata_ == 0)
    return 0;
  else if (otherdata_ == 0)
    return keydata_->sorted_data();
  else if (keysorted_ == 0)
    {
      g_assert(otherdata_ && keydata_);

      recreate();
    }

  return keysorted_;
}

const double* 
SortedPair::otherdata() const
{
  if (otherdata_ == 0)
    return 0;
  else if (keydata_ == 0)
    return otherdata_->sorted_data();
  else if (othersorted_ == 0)
    {
      g_assert(otherdata_ && keydata_);

      recreate();
    }
  return othersorted_;
}
#endif

void 
SortedPair::change_values(Data* d, const vector<guint> & which)
{
#if 0
  g_debug("changed %u values, detected in SortedPair", which.size());

  ScalarData* sd = d->cast_to_scalar();
  
  g_return_if_fail(sd != 0);

  const double* vec = sd->scalars();

  vector<guint>::const_iterator i = which.begin();
  while (i != which.end())
    {
      // I am not sure this is OK; update() may not work 
      // if multiple values have changed. 
      //      update(*i, vec[*i], (sd == keydata_) ? true : false);
      
      Change change;

      change.realindex = *i;
      change.newval = vec[*i];
      change.is_key = (sd == keydata_) ? true : false;

      changes_.push_back(change);

      ++i;
    }
#else
  change_values(d);
#endif
}

void 
SortedPair::change_values(Data* d)
{
  // Size may have changed.
  clear();
}

void 
SortedPair::change_name(Data* d, const string & name)
{
  // We don't care.
}

void
SortedPair::destroy_model()
{
  g_warning("Data destroyed from under SortedPair!!");
}

void 
SortedPair::grab_data(ScalarData** dp)
{
  ScalarData* d = *dp;
  if (d != 0)
    {
      if (!d->data_model.view_known(this))
        {
          d->data_model.add_view(this);
        }
    }
  // We don't refcount the data for now, since we expect to be used
  // by another object that does; perhaps we should though.
}

void 
SortedPair::release_data(ScalarData** dp)
{
  ScalarData* d = *dp;

  if (d != 0)
    {
      if (d->data_model.view_known(this))
        {
          d->data_model.remove_view(this);
        }
    }
  *dp = 0;
}

void 
SortedPair::clear()
{
  changes_.clear();

  if (sorted2real_)
    delete [] sorted2real_;

  if (real2sorted_)
    delete [] real2sorted_;

  if (keysorted_)
    delete [] keysorted_;

  if (othersorted_)
    delete [] othersorted_;

  if (tiles_)
    delete tiles_;

  real2sorted_ = 0;
  sorted2real_ = 0;
  keysorted_ = 0;
  othersorted_ = 0;
  tiles_ = 0;
}

void 
SortedPair::recreate() const
{
  if (tiles_ != 0)
    g_warning("Recreating tiles we already have!");

  if (otherdata_ == 0 || keydata_ == 0) 
    return;

  const guint N = MIN(otherdata_->size(), keydata_->size());

  if (N == 0) return;

  othersorted_ = new double[N];
  keysorted_ = new double[N];
  sorted2real_ = new guint[N];
  real2sorted_ = new guint[N];

  const double* oldkey = keydata_->scalars();
  
  g_return_if_fail(oldkey != 0);
  
  memcpy(keysorted_, oldkey, N*sizeof(double));

  // Fill sorted2real_ with the original indices
  register guint i = 0;
  while (i < N)
    {
      sorted2real_[i] = i;
      ++i;
    }

  PlotUtil::sort_indices(keysorted_, sorted2real_, N);


  // We have to update real2sorted_
  // Assume i is the sorted index; we assign the sorted index 
  //  to the real index in real2sorted_ 
  i = 0;
  while (i < N)
    {
      real2sorted_[sorted2real_[i]] = i;
      ++i;
    }

  // Now we have to fill the othersorted_ array
  const double* oldother = otherdata_->scalars();
  g_return_if_fail(oldother != 0);

  i = 0;
  while (i < N)
    {
      othersorted_[real2sorted_[i]] = oldother[i];
      ++i;
    }

#ifdef GNOME_ENABLE_DEBUG_TOOSLOW
  // Now check everything for debugging...
  // As you might imagine this causes massive slowdown.

  g_debug("Performing insanely slow sanity check because debugging is on...");


  vector<bool> real2sorted_values(N);
  vector<bool> sorted2real_values(N);
  
  i = 0;
  while (i < N)
    {
      real2sorted_values[i] = false;
      sorted2real_values[i] = false;
      ++i;
    }
  
  i = 0;
  while (i < N)
    {
      // Check that keysorted_ is in order...
      if ((i+1) < N)
        g_assert(keysorted_[i] <= keysorted_[i+1]);

      // Check that converters are inverses...
      g_assert(real2sorted_[sorted2real_[i]] == i);
      g_assert(sorted2real_[real2sorted_[i]] == i);
      g_assert(real2sorted_[sorted2real_[i]] == sorted2real_[real2sorted_[i]]);

      // Check value uniqueness
      g_assert(real2sorted_values[real2sorted_[i]] == false);
      real2sorted_values[real2sorted_[i]] = true;
      
      g_assert(sorted2real_values[sorted2real_[i]] == false);
      sorted2real_values[sorted2real_[i]] = true;

      // Assume i is the real index, and check that values match
      g_assert(keysorted_[real_index_to_sorted(i)] == oldkey[i]);
      g_assert(othersorted_[real_index_to_sorted(i)] == oldother[i]);

      // Assume i is the sorted index, and check that values match
      g_assert(keysorted_[i] == oldkey[sorted_index_to_real(i)]);
      g_assert(othersorted_[i] == oldother[sorted_index_to_real(i)]);

      // Check that value-retrieval functions work

      // Assume i is real index
      g_assert(sorted_key_value(real_index_to_sorted(i)) == real_key_value(i));
      g_assert(sorted_other_value(real_index_to_sorted(i)) == real_other_value(i));

      // Assume i is sorted index
      g_assert(sorted_key_value(i) == real_key_value(sorted_index_to_real(i)));
      g_assert(sorted_other_value(i) == real_other_value(sorted_index_to_real(i)));

      ++i;
    }

  g_debug(" ... Done.");

#endif

  // Finally we create our tiles
  
  g_assert(tiles_ == 0); // check for memory leak

  tiles_ = new PointTiles;
  tiles_->set_points(keysorted_, othersorted_, 
                     sorted2real_, N,
                     keydata_->min(), otherdata_->min(), 
                     keydata_->max(), otherdata_->max());

  g_debug("Recreated sort and tiles.");

  // Well, shit. Here goes a few megs o' RAM. we need these 
  // to find the old tile when we update the tiles array.

  // Cute hack we could use: add "get old values" method to ScalarData,
  // which returns vector of old values that apply to the in-progress
  // view notification.
  // Real solution: change signature of change_values() to include the old value
  // as well. 
  // cute hack is way easier...

  // I've commented out the update() code for now though, since it's busted,
  // and replaced it with a simple clear() (recreating entire sort, and tiles,
  //  every time a value changes...)
 
#if 0
  delete [] keysorted_;
  delete [] othersorted_;

  keysorted_ = 0;
  othersorted_ = 0;
#endif
}

PointTiles* 
SortedPair::tiles() const
{
  if (tiles_ == 0 && keydata_ != 0 && otherdata_ != 0)
    recreate();

#if 0
  else if (changes_.size() > 1) // exact number is a guess; updating this number is maybe faster than recreate?
    recreate();
  else if (!changes_.empty())
    {
      g_debug("Only updating");
      vector<Change>::iterator i = changes_.begin();
      while (i != changes_.end())
        {
          update((*i).realindex, (*i).newval, (*i).is_key);
          
          ++i;
        }
      changes_.clear();
    }  
#endif

  return tiles_;
}

#if 0
static void print_guints(guint* array, guint count)
{
  printf("} ");
  guint i = 0;
  while (i < count)
    {
      printf("%u ", array[i]);
      ++i;
    }
  printf("\n");
}


static void print_doubles(double* array, guint count)
{
  printf("} ");
  guint i = 0;
  while (i < count)
    {
      printf("%g ", array[i]);
      ++i;
    }
  printf("\n");
}
#endif

void 
SortedPair::update(guint realindex, double newval, bool is_key)
{
  // Right now this handles two cases: when we keep othersorted_ and keysorted_ 
  //  around, and when we don't. Eventually we probably won't though.

#if 0
  // For now, we will just do it the insanely slow way
  g_warning("fixme - incredibly slow update; regenerating sorted array and tiles...");
  clear();
  return;
#endif

  // We're keeping it simple now.
  g_warning("Not supposed to be called, %s", __FUNCTION__);

  g_debug("Updating single point %u", realindex);

  // In all cases, update the tiles.
  if (othersorted_ && keysorted_)
    {
      g_assert(sorted2real_ != 0);
      g_assert(keydata_ != 0);
      g_assert(otherdata_ != 0);

      // FIXME store real index in tiles.

      tiles_->update(realindex, 
                     keysorted_[real2sorted_[realindex]],    // Old X value
                     othersorted_[real2sorted_[realindex]],  // Old Y value
                     keydata_->get_scalar(realindex),    // New X value
                     otherdata_->get_scalar(realindex)); // New Y value
    }

  if (!is_key)
    {
      //      g_debug("not key, no need for complex update");

      if (othersorted_ != 0)
        {
          g_assert(sorted2real_ != 0);
          const guint sortedi = real2sorted_[realindex];
          othersorted_[sortedi] = newval;
        }
    }
  else 
    {
      // FIXME Will the algorithm still work if other indices have changed
      // their value as well? 

      //      g_debug("updating key value");

      if (keysorted_ != 0)
        {
          g_assert(sorted2real_ != 0);

          const guint sorted_index = real2sorted_[realindex]; // get old sorted index
          const guint N = MIN(otherdata_->size(), keydata_->size());

          // Scan keysorted for the first value greater than or equal to the 
          //  new value... remember that no keysorted_ value has been changed yet.
          guint ge = PlotUtil::first_ge(keysorted_, N, newval);
          
          // Now we want to move realindex to the right
          // place in the sorted array, and move other values as needed 
          // to achieve that.
          
          if (sorted_index == ge)
            {
              //              g_debug("no need to adjust sort");
              keysorted_[sorted_index] = newval;
#ifdef GNOME_ENABLE_DEBUG
              if (sorted_index > 0)
                g_assert(keysorted_[sorted_index-1] < newval);
              if (sorted_index < (N-1))
                g_assert(keysorted_[sorted_index+1] >= newval);
#endif
            }
          else if (sorted_index > ge)
            {
              const guint count_to_move = sorted_index - ge;
              const guint src = ge;
              const guint dest = ge + 1;

              g_assert(count_to_move > 0);
              
              // We need to move the value to the left in the sorted 
              // array. Specifically, we need to move it to index ge,
              // after moving [ge, sorted_index) up one step.
              // If sorted_index > ge, we know that ge < N
              // so ge must be a valid index. Also we assume sorted_index is valid 
              // thus ge+1 is valid

              // g_debug("Updating assuming sorted index > ge");

              g_assert(ge < N);
              g_assert(sorted_index < N);
              g_assert(ge+1 < N);

              //              printf("keysorted_; real: %u sorted: %u from: %u to: %u count: %u newval: %g\n", realindex, sorted_index, src, dest, count_to_move, newval);
              //              print_doubles(&keysorted_[src], count_to_move+1);

              // Move [ge, sorted_index) up one
              // dest, src, bytes
              g_memmove(&keysorted_[dest], &keysorted_[src], sizeof(double) * count_to_move);

              // assign new value to keysorted_[ge]
              keysorted_[ge] = newval;
              
              //              print_doubles(&keysorted_[src], count_to_move+1);

              // Do the same for othersorted_; we have to obtain the new value

              double other_newval = otherdata_->get_scalar(realindex);

              g_memmove(&othersorted_[dest], &othersorted_[src], sizeof(double) * count_to_move);

              // assign new value to keysorted_[ge]
              othersorted_[ge] = other_newval;

              // For sorted2real_ and real2sorted_, we need to increment the sorted 
              //  indexes appropriately
              
              // sorted2real_ goes from sorted to real, so we want to modify it just 
              //  as we did the arrays of values.
              
              //              printf("sorted2real_\n");

              //              print_guints(&sorted2real_[src], MIN(count_to_move+4,N-src));

              g_memmove(&sorted2real_[dest], &sorted2real_[src], sizeof(guint) * count_to_move);
              
              sorted2real_[ge] = realindex;

              //              print_guints(&sorted2real_[src], MIN(count_to_move+4,N-src));

              // real2sorted_ goes from real to sorted, so we want to increment its contents,
              // using the newly-updated sorted2real_ to find what needs fixing

              guint i = 0; // ge
              while (i < N)
                {
                  real2sorted_[sorted2real_[i]] = i;

                  ++i;
                }

              // Now, pray a lot.
            }
          else if (sorted_index < ge)
            {
              // sorted_index < ge then we need to move the value to the right, 
              // to ge - 1. Then (sorted_index, ge) (which is potentially empty)
              // has to move down one notch into the vacated space.
              // We know that ge > 0 since sorted_index >= 0.
              // ge may be one past the array end.

              //              g_debug("Updating assuming sorted index < ge");

              g_assert(ge > 0);
              g_assert(ge <= N);
              g_assert(sorted_index < N);
              
              // If the value is staying in the same place, we just
              // need to update the data. This means (sorted_index,
              // ge) is empty.

              const guint count_to_move = ge - sorted_index - 1;

              if (count_to_move == 0)
                {
                  g_assert(realindex == sorted2real_[ge-1]);
                  keysorted_[ge-1] = newval;
                  othersorted_[ge-1] = otherdata_->get_scalar(realindex);
                }
              else
                {
                  const guint src = sorted_index + 1;
                  const guint dest = sorted_index;

                  //                  printf("keysorted_; real: %u sorted: %u from: %u to: %u count: %u newval: %g\n", realindex, sorted_index, src, dest, count_to_move, newval);
                  //                  print_doubles(&keysorted_[dest], count_to_move + 1);

                  g_assert(count_to_move > 0);

                  // Otherwise, we need to move some stuff around.
                  // dest, src, bytes
                  g_memmove(&keysorted_[dest], &keysorted_[src],
                            count_to_move * sizeof(double));
                  keysorted_[ge-1] = newval;

                  //                  print_doubles(&keysorted_[dest], count_to_move + 1);

                  g_memmove(&othersorted_[dest], &othersorted_[src],
                            count_to_move * sizeof(double));
                  othersorted_[ge-1] = otherdata_->get_scalar(realindex);
                  
                  
                  // sorted2real_ goes from sorted to real, so we want to modify it just 
                  //  as we did the arrays of values.
                  
                  //                  printf("sorted2real_\n");

                  //                  print_guints(&sorted2real_[dest], count_to_move + 1);

                  g_memmove(&sorted2real_[dest], &sorted2real_[src], sizeof(guint) * count_to_move);
                  
                  sorted2real_[ge-1] = realindex;
                  
                  //                  print_guints(&sorted2real_[dest], count_to_move + 1);

                  // real2sorted_ goes from real to sorted, we want to 
                  // update its contents...

                  guint i = 0; // sorted_index;
                  while (i < N) // ge
                    {
                      real2sorted_[sorted2real_[i]] = i;

                      ++i;
                    }

                  // Now, pray a lot.
                }
            }
          else
            g_assert_not_reached();


#ifdef GNOME_ENABLE_DEBUG_TOOSLOW
          // Now check everything for debugging...
          // As you might imagine this causes massive slowdown.
      
          g_debug("Performing insanely slow sanity check because debugging is on...");

          vector<bool> real2sorted_values(N);
          vector<bool> sorted2real_values(N);
      
          guint i = 0;
          while (i < N)
            {
              // init arrays to check for duplicates
              real2sorted_values[i] = false;
              sorted2real_values[i] = false;
              
              // Check that keysorted_ is in order...
              if ((i+1) < N)
                g_assert(keysorted_[i] <= keysorted_[i+1]);

              ++i;
            }
          
          // Check for duplicates
          i = 0;
          while (i < N)
            {
              if (sorted2real_values[sorted2real_[i]] == true)
                {
                  g_warning("duplicated %u in sorted2real_", sorted2real_[i]);
                }
              g_assert(sorted2real_values[sorted2real_[i]] == false);
              sorted2real_values[sorted2real_[i]] = true;

              if (real2sorted_values[real2sorted_[i]] == true)
                {
                  g_warning("duplicated %u in real2sorted_", real2sorted_[i]);
                }
              g_assert(real2sorted_values[real2sorted_[i]] == false);
              real2sorted_values[real2sorted_[i]] = true;
              
              ++i;
            }          

          i = 0;
          while (i < N)
            {
              // Check that real/sorted converters are inverses...
              g_assert(real2sorted_[sorted2real_[i]] == i);
              g_assert(sorted2real_[real2sorted_[i]] == i);
              g_assert(real2sorted_[sorted2real_[i]] == sorted2real_[real2sorted_[i]]);

              ++i;
            }

          const double* oldkey = keydata_->scalars();
          const double* oldother = otherdata_->scalars();

          i = 0;
          while (i < N)
            {
              // Assume i is the real index, and check that values match
              g_assert(keysorted_[real_index_to_sorted(i)] == oldkey[i]);
              g_assert(othersorted_[real_index_to_sorted(i)] == oldother[i]);
              
              // Assume i is the sorted index, and check that values match
              g_assert(keysorted_[i] == oldkey[sorted_index_to_real(i)]);
              g_assert(othersorted_[i] == oldother[sorted_index_to_real(i)]);
              
              // Check that value-retrieval functions work
              
              // Assume i is real index
              g_assert(sorted_key_value(real_index_to_sorted(i)) == real_key_value(i));
              g_assert(sorted_other_value(real_index_to_sorted(i)) == real_other_value(i));
          
              // Assume i is sorted index
              g_assert(sorted_key_value(i) == real_key_value(sorted_index_to_real(i)));
              g_assert(sorted_other_value(i) == real_other_value(sorted_index_to_real(i)));

              ++i;
            }
      
          g_debug(" ... Done.");
#endif
        }
    }
}

