// -*- C++ -*-

/* 
 * plotstore.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTSTORE_H
#define GUPPI_PLOTSTORE_H

#include "plot.h"

#include <set>
#include <vector>
#include <map>

// Repository for plot types and information associated with them.
// We use all the strings because it makes dynamic creation and registration 
//  of types via Guile simpler.

// FIXME Model/View so we can have a GUI editor for tools and actions.

class ToolBox;

class PlotStore {
public:
  PlotStore();
  ~PlotStore();

  ///////////
  // Note that we're adding and removing plot *types*,
  // but below we manage Tool and Action *instances*

  void add_plot(const string& id);

  void remove_plot(const string& id);
  
  // If you have any doubt, better check this before 
  //  add/remove...
  bool plot_exists(const string& id) const;

  ///////////

  // Tools and Actions are ref()/unref()'d...

  ///////////

  // Make this tool available for this plot.
  void add_tool(const string& id, Tool* t);

  void remove_tool(const string& id, const string& toolid);

  bool tool_exists(const string& id, const string& toolid) const;

  // Returns 0 if the tool doesn't exist.
  Tool* get_tool(const string& id, const string& toolid);

  //////////

  void add_action(const string& id, Action* a);
  
  void remove_action(const string& id, const string& actionid);

  bool action_exists(const string& id, const string& actionid) const;

  // Returns 0 if the action doesn't exist.
  Action* get_action(const string& id, const string& actionid);

  //////////

  void add_menuentry(const string& id, MenuEntry* me);
  
  void remove_menuentry(const string& id, const string& menuentryid);

  bool menuentry_exists(const string& id, const string& menuentryid) const;

  // Returns 0 if the entry doesn't exist.
  MenuEntry* get_menuentry(const string& id, const string& menuentryid);
  
  //////////

  vector<string> plot_types() const;

  vector<Tool*> tools(const string& id) const;

  vector<Action*> actions(const string& id) const;

  vector<MenuEntry*> menuentries(const string& id) const;

  //////////

  // Set to 0 for none.
  void set_current_tool(const string& id, Tool* t);

  // returns 0 if none.
  Tool* get_current_tool(const string& id);

  // Show the ToolBox for the plot type
  void show_toolbox(const string& id);

private:

  set<string> plots_;

  // These hash a plot ID to a map<tool/actionid,Tool*/Action*>
  GHashTable* tools_;
  GHashTable* actions_;
  GHashTable* menuentries_;

  // OK, got lazy and didn't use a hash table here... :-)
  map<string,Tool*> current_tools_;
  map<string,ToolBox*> toolboxes_;
};

PlotStore* guppi_plot_store();

#endif
