// -*- C++ -*-

/* 
 * plotstate.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plotstate.h"

PlotState::PlotState()
{

}

PlotState::~PlotState()
{
  g_assert(rc_.count() == 0);

#ifdef DEBUG_RC
  g_debug("PlotState %p deleted", this);
#endif


}

guint
PlotState::ref()
{
#ifdef DEBUG_RC
  g_debug("PlotState %p referenced", this);
#endif
  return rc_.ref();
}

guint
PlotState::unref()
{
#ifdef DEBUG_RC
  g_debug("PlotState %p unreferenced", this);
#endif
  return rc_.unref();
}

bool 
PlotState::get_resize_lock(gpointer view)
{
  if (resize_token_.token_available())
    {
      resize_token_.take_token(view);
      return true;
    }
  else 
    return false;
}

void 
PlotState::release_resize_lock(gpointer view)
{
  resize_token_.release_token(view);
}

