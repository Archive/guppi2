// -*- C++ -*-

/* 
 * xyplot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xyplot.h"
#include "canvas-scatter.h"
#include "canvas-axis.h"
#include "canvas-lineplot.h"
#include "canvas-pricebars.h"
#include "plotutils.h"
#include "xyplotstate.h"
#include "scalardata.h"
#include "tool.h"
#include "pointtiles.h"
#include "markerstyle.h"
#include "menuentry.h"
#include "plotstore.h"
#include "gfont.h"

////////// XyLayerView

class XyLayerView : public XyLayer::View 
{
public:
  XyLayerView(XyPlotState* state) : state_(state), item_(0) {}
  virtual ~XyLayerView() {}

  virtual GnomeCanvasItem* item() { return item_; };

  virtual void realize(GnomeCanvasGroup* group) = 0;
  virtual void unrealize() = 0;
  
  // update the canvas item 
  virtual void update() = 0;

  // XyLayer::View

  virtual void change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos) = 0;

  virtual void change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos) = 0;

  virtual void change_hidden(XyLayer* layer,
                             bool hidden) = 0;

  virtual void change_name(XyLayer* layer, 
                           const string& name) {}

  virtual void change_masked(XyLayer* layer, bool maskedness) {}

  // ::View
  virtual void destroy_model() {}

protected:
  XyPlotState* state_;
  GnomeCanvasItem* item_;

};

class XyDataLayerView : public XyLayerView,
                        public XyDataLayer::View
{
public:
  XyDataLayerView(XyPlotState* state) : XyLayerView(state) {}
  virtual ~XyDataLayerView() {}

  // XyDataLayer::View
  
  virtual void change_x_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data) = 0;
  virtual void change_y_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data) = 0;
  
  virtual void change_data_values(XyDataLayer* layer, Data* data) = 0;

};

class XyScatterView : public XyDataLayerView, 
                      public XyScatter::View,
                      public MarkerPalette::View
{
public:
  XyScatterView(XyPlotState* state, XyScatter* layer);
  virtual ~XyScatterView();

  virtual void realize(GnomeCanvasGroup* group);
  virtual void unrealize();

  virtual void update();

  // XyLayer::View

  virtual void change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_hidden(XyLayer* layer,
                             bool hidden);

  // XyDataLayer::View
  
  virtual void change_x_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data);

  virtual void change_y_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data);
  
  virtual void change_data_values(XyDataLayer* layer, Data* data);


  // XyScatter::View
  virtual void change_styles(XyScatter* xys);

  // MarkerPalette::View

  virtual void add_entry(MarkerPalette* palette, guint32 entryid);
  virtual void remove_entry(MarkerPalette* palette, guint32 entryid);
  virtual void change_entry(MarkerPalette* palette, guint32 entryid);
  virtual void change_current(MarkerPalette* palette, 
                              guint32 oldcurrent, 
                              guint32 newcurrent);

  // ::View
  virtual void destroy_model();

  static void info_func(struct _GuppiScatter* gs, 
                        gpointer user_data, 
                        XyPlotState** state,
                        XyScatter** xy);

private:
  XyScatter* layer_;

  void grab_palette();
  void release_palette();
};

XyScatterView::XyScatterView(XyPlotState* state, XyScatter* layer)
  : XyDataLayerView(state), layer_(layer)
{
  g_return_if_fail(layer_ != 0);

  layer_->ref();

  layer_->layer_model.add_view(this);
  layer_->data_layer_model.add_view(this);
  layer_->xy_scatter_model.add_view(this);

  grab_palette();
}

XyScatterView::~XyScatterView()
{
  if (item_ != 0)
    unrealize();

  layer_->layer_model.remove_view(this);
  layer_->data_layer_model.remove_view(this);
  layer_->xy_scatter_model.remove_view(this);

  if (layer_->unref() == 0)
    {
      delete layer_;
    }

  release_palette();
}

void
XyScatterView::realize(GnomeCanvasGroup* group)
{
  g_return_if_fail(item_ == 0);

  item_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group),
                                guppi_scatter_get_type(),
                                NULL);

  guppi_scatter_set_info_func(GUPPI_SCATTER(item_), info_func, this);
  
  // We are going to ref our objects, so that the order of destruction
  //  (us or canvas first) isn't important
  gtk_object_ref(GTK_OBJECT(item_));

  if (layer_->hidden())
    gnome_canvas_item_hide(item_);
}

void
XyScatterView::unrealize()
{
  if (item_ != 0) 
    gtk_object_destroy(GTK_OBJECT(item_));  
}

void 
XyScatterView::update()
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

/// XyLayer::View

void
XyScatterView::change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void
XyScatterView::change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void 
XyScatterView::change_hidden(XyLayer* layer,
                             bool hidden)
{
  if (item_ != 0)
    {
      if (hidden)
        gnome_canvas_item_hide(item_);
      else
        gnome_canvas_item_show(item_);
    }
}

/// XyDataLayer::View
  
void
XyScatterView::change_x_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void
XyScatterView::change_y_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}
  
void
XyScatterView::change_data_values(XyDataLayer* layer, Data* data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}


/// XyScatter::View
void
XyScatterView::change_styles(XyScatter* xys)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}


// MarkerPalette::View

void
XyScatterView::add_entry(MarkerPalette* palette, guint32 entryid)
{
  // don't care
}

void
XyScatterView::remove_entry(MarkerPalette* palette, guint32 entryid)
{
  // We have to redraw the points which used the entry with the default.
  change_entry(palette, entryid);
}

void
XyScatterView::change_entry(MarkerPalette* palette, guint32 entryid)
{
  if (item_ != 0)
    {
      if (layer_->style_in_use(entryid))
        {
          gnome_canvas_item_request_update(item_);
        }
    }
}

void
XyScatterView::change_current(MarkerPalette* palette, 
                              guint32 oldcurrent, 
                              guint32 newcurrent)
{
  // don't care
}


/// ::View
void
XyScatterView::destroy_model()
{
#ifdef GNOME_ENABLE_DEBUG
  g_warning("XyScatter or marker palette destroyed from underneath XyScatterView");
#endif
}


void 
XyScatterView::info_func(struct _GuppiScatter* gs, 
                         gpointer user_data, 
                         XyPlotState** state,
                         XyScatter** xy)
{
  XyScatterView* sv = static_cast<XyScatterView*>(user_data);
  
  *state = sv->state_;
  *xy = sv->layer_; 
}

void 
XyScatterView::grab_palette()
{
  MarkerPalette* p = guppi_marker_palette();
  if (p != 0)
    {
      p->ref();
      p->palette_model.add_view(this);
    }
}

void 
XyScatterView::release_palette()
{
  MarkerPalette* p = guppi_marker_palette();
  if (p != 0)
    {
      p->palette_model.remove_view(this);
      if (p->unref() == 0)
        delete p;
    }
}

//////////////////////////////


class XyLineView : public XyDataLayerView
                   //                   public XyLine::View
{
public:
  XyLineView(XyPlotState* state, XyLine* layer);
  virtual ~XyLineView();

  virtual void realize(GnomeCanvasGroup* group);
  virtual void unrealize();

  virtual void update();

  // XyLayer::View

  virtual void change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_hidden(XyLayer* layer,
                             bool hidden);

  // XyDataLayer::View
  
  virtual void change_x_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data);

  virtual void change_y_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data);
  
  virtual void change_data_values(XyDataLayer* layer, Data* data);

  // ::View
  virtual void destroy_model();

  static void info_func(struct _GuppiLineplot* gs, 
                        gpointer user_data, 
                        XyPlotState** state,
                        XyLine** xy);

private:
  XyLine* layer_;

};

XyLineView::XyLineView(XyPlotState* state, XyLine* layer)
  : XyDataLayerView(state), layer_(layer)
{
  g_return_if_fail(layer_ != 0);

  layer_->ref();

  layer_->layer_model.add_view(this);
  layer_->data_layer_model.add_view(this);
}

XyLineView::~XyLineView()
{
  if (item_ != 0)
    unrealize();

  layer_->layer_model.remove_view(this);
  layer_->data_layer_model.remove_view(this);

  if (layer_->unref() == 0)
    {
      delete layer_;
    }
}

void
XyLineView::realize(GnomeCanvasGroup* group)
{
  g_return_if_fail(item_ == 0);

  item_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group),
                                guppi_lineplot_get_type(),
                                NULL);

  guppi_lineplot_set_info_func(GUPPI_LINEPLOT(item_), info_func, this);
  
  // We are going to ref our objects, so that the order of destruction
  //  (us or canvas first) isn't important
  gtk_object_ref(GTK_OBJECT(item_));

  if (layer_->hidden())
    gnome_canvas_item_hide(item_);
}

void
XyLineView::unrealize()
{
  if (item_ != 0) 
    gtk_object_destroy(GTK_OBJECT(item_));  
}

void 
XyLineView::update()
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

/// XyLayer::View

void
XyLineView::change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void
XyLineView::change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void 
XyLineView::change_hidden(XyLayer* layer,
                             bool hidden)
{
  if (item_ != 0)
    {
      if (hidden)
        gnome_canvas_item_hide(item_);
      else
        gnome_canvas_item_show(item_);
    }
}

/// XyDataLayer::View
  
void
XyLineView::change_x_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void
XyLineView::change_y_data(XyDataLayer* layer, 
                             Data* old_data, Data* new_data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}
  
void
XyLineView::change_data_values(XyDataLayer* layer, Data* data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

/// ::View
void
XyLineView::destroy_model()
{
#ifdef GNOME_ENABLE_DEBUG
  g_warning("XyLine destroyed from underneath XyLineView");
#endif
}


void 
XyLineView::info_func(struct _GuppiLineplot* gs, 
                      gpointer user_data, 
                      XyPlotState** state,
                      XyLine** xy)
{
  XyLineView* sv = static_cast<XyLineView*>(user_data);
  
  *state = sv->state_;
  *xy = sv->layer_; 
}

//////////////////////////////


class XyPriceView : public XyLayerView,
                    public XyPriceBars::View
{
public:
  XyPriceView(XyPlotState* state, XyPriceBars* layer);
  virtual ~XyPriceView();

  virtual void realize(GnomeCanvasGroup* group);
  virtual void unrealize();

  virtual void update();

  // XyLayer::View

  virtual void change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos);

  virtual void change_hidden(XyLayer* layer,
                             bool hidden);

  // XyPriceBars::View

  virtual void change_data(XyPriceBars* layer, 
                           Data* old_data, Data* new_data);
  
  virtual void change_data_values(XyPriceBars* layer, Data* data);

  // ::View
  virtual void destroy_model();

  static void info_func(struct _GuppiPricebars* gs, 
                        gpointer user_data, 
                        XyPlotState** state,
                        XyPriceBars** xy);

private:
  XyPriceBars* layer_;

};

XyPriceView::XyPriceView(XyPlotState* state, XyPriceBars* layer)
  : XyLayerView(state), layer_(layer)
{
  g_return_if_fail(layer_ != 0);

  layer_->ref();

  layer_->layer_model.add_view(this);
  layer_->price_model.add_view(this);
}

XyPriceView::~XyPriceView()
{
  if (item_ != 0)
    unrealize();

  layer_->layer_model.remove_view(this);
  layer_->price_model.remove_view(this);

  if (layer_->unref() == 0)
    {
      delete layer_;
    }
}

void
XyPriceView::realize(GnomeCanvasGroup* group)
{
  g_return_if_fail(item_ == 0);

  item_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group),
                                guppi_pricebars_get_type(),
                                NULL);

  guppi_pricebars_set_info_func(GUPPI_PRICEBARS(item_), info_func, this);
  
  // We are going to ref our objects, so that the order of destruction
  //  (us or canvas first) isn't important
  gtk_object_ref(GTK_OBJECT(item_));

  if (layer_->hidden())
    gnome_canvas_item_hide(item_);
}

void
XyPriceView::unrealize()
{
  if (item_ != 0) 
    gtk_object_destroy(GTK_OBJECT(item_));  
}

void 
XyPriceView::update()
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

/// XyLayer::View

void
XyPriceView::change_x_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void
XyPriceView::change_y_axis(XyLayer* layer, 
                             PlotUtil::PositionType old_pos, 
                             PlotUtil::PositionType new_pos)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

void 
XyPriceView::change_hidden(XyLayer* layer,
                             bool hidden)
{
  if (item_ != 0)
    {
      if (hidden)
        gnome_canvas_item_hide(item_);
      else
        gnome_canvas_item_show(item_);
    }
}

/// XyPriceBars::View


void 
XyPriceView::change_data(XyPriceBars* layer, 
                         Data* old_data, Data* new_data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}
  
void
XyPriceView::change_data_values(XyPriceBars* layer, Data* data)
{
  if (item_ != 0)
    gnome_canvas_item_request_update(item_);
}

/// ::View
void
XyPriceView::destroy_model()
{
#ifdef GNOME_ENABLE_DEBUG
  g_warning("XyPriceBars destroyed from underneath XyPriceView");
#endif
}


void 
XyPriceView::info_func(struct _GuppiPricebars* gs, 
                      gpointer user_data, 
                      XyPlotState** state,
                      XyPriceBars** xy)
{
  XyPriceView* sv = static_cast<XyPriceView*>(user_data);
  
  *state = sv->state_;
  *xy = sv->layer_; 
}

////////////////////////////////////////////////

const char* XyPlot::ID = "guppi-xy";

XyPlot::XyPlot(DataStore* datastore)
  : Plot(ID, datastore), state_(0), 
    group_(0), layer_group_(0), bg_(0), 
    xpos_(0.0), ypos_(0.0),
    axis_edit_(0)
{
  memset(axes_,0,sizeof(GnomeCanvasItem*)*PlotUtil::PositionTypeEnd);
  memset(axis_labels_,0,sizeof(GnomeCanvasItem*)*PlotUtil::PositionTypeEnd);

  set_state(new XyPlotState(datastore));
}

XyPlot::XyPlot(XyPlotState* s)
  : Plot(ID, s->store()), state_(0), 
    group_(0), layer_group_(0), bg_(0), 
    xpos_(0.0), ypos_(0.0),
    axis_edit_(0)
{
  memset(axes_,0,sizeof(GnomeCanvasItem*)*PlotUtil::PositionTypeEnd);
  memset(axis_labels_,0,sizeof(GnomeCanvasItem*)*PlotUtil::PositionTypeEnd);

  set_state(s);
}

Plot* 
XyPlot::new_view()
{
  g_return_val_if_fail(state_ != 0, 0);
  return new XyPlot(state_);
}

XyPlot::~XyPlot()
{
  unrealize();

  release_state();
}

GnomeCanvasItem* 
XyPlot::item()
{
  return group_;
}

void 
XyPlot::realize(GnomeCanvasGroup* group)
{
  g_return_if_fail(group != 0);
  g_return_if_fail(GNOME_IS_CANVAS_GROUP(group));
  g_return_if_fail(group_ == 0); // ensure not realized yet.
  
  canvas_ = GTK_WIDGET(GNOME_CANVAS_ITEM(group)->canvas);

  g_return_if_fail(canvas_ != 0); // currently known since groups must have parents

  group_ = gnome_canvas_item_new(group,
                                 gnome_canvas_group_get_type(),
                                 "x", xpos_,
                                 "y", ypos_,
                                 NULL);


  bg_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                              gnome_canvas_rect_get_type(),
                              "x1", 0.0,
                              "y1", 0.0,
                              NULL);
                              
  // Layer group is above the background but below everything else.

  layer_group_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                                       gnome_canvas_group_get_type(),
                                       "x", 0.0,
                                       "y", 0.0,
                                       NULL);

  gint i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      axes_[i] = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                                       guppi_axis_get_type(),
                                       NULL);
      guppi_axis_set_info_func(GUPPI_AXIS(axes_[i]), axis_info_func, (gpointer)this);
      ++i;
    }

  axis_labels_[PlotUtil::NORTH] = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                                                        gnome_canvas_text_get_type(),
                                                        "font", "fixed",
                                                        "fill_color", "black",
                                                        "anchor", GTK_ANCHOR_CENTER,
                                                        NULL);

  axis_labels_[PlotUtil::SOUTH] = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                                                        gnome_canvas_text_get_type(),
                                                        "font", "fixed",
                                                        "fill_color", "black",
                                                        "anchor", GTK_ANCHOR_CENTER,
                                                        NULL);

  east_label_rotator_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                                              gnome_canvas_group_get_type(),
                                              NULL);

  west_label_rotator_ = gnome_canvas_item_new(GNOME_CANVAS_GROUP(group_),
                                              gnome_canvas_group_get_type(),
                                              NULL);


  axis_labels_[PlotUtil::EAST] = 
    gnome_canvas_item_new(GNOME_CANVAS_GROUP(east_label_rotator_),
                          gnome_canvas_text_get_type(),
                          "font", "fixed",
                          "fill_color", "black",
                          "anchor", GTK_ANCHOR_CENTER,
                          "x", 0.0, // So the position of the rotator group 
                          "y", 0.0, //  is also the anchor of the label.
                          NULL);

  
  axis_labels_[PlotUtil::WEST] = 
    gnome_canvas_item_new(GNOME_CANVAS_GROUP(west_label_rotator_),
                          gnome_canvas_text_get_type(),
                          "font", "fixed",
                          "fill_color", "black",
                          "anchor", GTK_ANCHOR_CENTER,
                          "x", 0.0, // So the position of the rotator group 
                          "y", 0.0, //  is also the anchor of the label.
                          NULL);
  


  {
    Affine affine;
    affine.rotate(270);
    
    gnome_canvas_item_affine_relative (axis_labels_[PlotUtil::WEST], affine.raw());

    Affine affine2;
    affine2.rotate(90);    

    gnome_canvas_item_affine_relative (axis_labels_[PlotUtil::EAST], affine2.raw());
  }

  /// Realize each layer; we need to realize in the order they are in in the plot state,
  /// not in the random order our map is in.

  XyPlotState::iterator li = state_->begin();
  while (li != state_->end())
    {
      map<XyLayer*,XyLayerView*>::iterator i = layer_views_.find(*li);

      if (i != layer_views_.end())           // remember we may not have a view for all layers.
        i->second->realize(GNOME_CANVAS_GROUP(layer_group_));
          
      ++li;
    }

  /// Final setup

  gtk_signal_connect(GTK_OBJECT(group_),
                     "event",
                     GTK_SIGNAL_FUNC(event_cb),
                     this);

  // We are going to ref our objects, so that the order of destruction
  //  (us or canvas first) isn't important
  gtk_object_ref(GTK_OBJECT(group_));
  gtk_object_ref(GTK_OBJECT(layer_group_));
  gtk_object_ref(GTK_OBJECT(bg_));
  gtk_object_ref(GTK_OBJECT(east_label_rotator_));
  gtk_object_ref(GTK_OBJECT(west_label_rotator_));

  i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      gtk_object_ref(GTK_OBJECT(axes_[i]));
      gtk_object_ref(GTK_OBJECT(axis_labels_[i]));

      ++i;
    }

  update_everything();
}

void 
XyPlot::unrealize()
{
  // Unrealize each layer
  {  
    map<XyLayer*,XyLayerView*>::iterator i = layer_views_.begin();
    while (i != layer_views_.end())
      {
        i->second->unrealize();
        
        ++i;
      }  
  }
  
  {
    int i = 0;
    while (i < PlotUtil::PositionTypeEnd)
      {
        if (axes_[i] != 0)
          gtk_object_destroy(GTK_OBJECT(axes_[i]));

        if (axis_labels_[i] != 0)
          gtk_object_destroy(GTK_OBJECT(axis_labels_[i]));
        
        axes_[i] = 0;
        axis_labels_[i] = 0;
        
        ++i;
      }
  }

  if (bg_) 
    gtk_object_destroy(GTK_OBJECT(bg_));

  // Destroy groups last, since some of the objects are inside them.

  if (east_label_rotator_)
    gtk_object_destroy(GTK_OBJECT(east_label_rotator_));

  if (west_label_rotator_)
    gtk_object_destroy(GTK_OBJECT(west_label_rotator_));

  if (layer_group_) 
    gtk_object_destroy(GTK_OBJECT(layer_group_));
  
  if (group_) 
    gtk_object_destroy(GTK_OBJECT(group_));

  bg_ = layer_group_ = group_ = west_label_rotator_ = east_label_rotator_ = 0;
}

void 
XyPlot::set_x(double x)
{
  xpos_ = x;

  if (group_ != 0)
    {
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(group_),
                            "x", xpos_, NULL);
    }

  // broken, dunno what this was
  //  change_size(state_, state_->width(), state_->height());
}

void 
XyPlot::set_y(double y)
{
  ypos_ = y;

  if (group_ != 0)
    {
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(group_),
                            "y", ypos_, NULL);
    }

  // broken, dunno what this was
  // change_size(state_, state_->width(), state_->height());
}

void 
XyPlot::set_size(double width, double height)
{
  // This will result in two "size changed" view notifications, 
  //  which is suboptimal but easily fixed later.
  state_->set_width(width);
  state_->set_height(height);
}

double 
XyPlot::width()
{
  return state_->width();
}

double 
XyPlot::height()
{
  return state_->height();
}

void 
XyPlot::size_request(double* width, double* height)
{
  g_return_if_fail(width != 0);
  g_return_if_fail(height != 0);

  state_->size_request(width, height);
}

void
XyPlot::axis_edit()
{
  // Demand-create, just to save some RAM and time
  if (axis_edit_ == 0)
    {
      axis_edit_ = new XyAxisEdit;
      axis_edit_->set_state(state_);
    }

  axis_edit_->edit();
}

const string& 
XyPlot::name() const
{
  return state_->name();
}

void
XyPlot::change_background(XyPlotState* state, const Color & bg)
{
  // This will eventually redraw the canvas items for each layer.

  if (bg_ != 0)
    {
      gnome_canvas_item_set(bg_,
                            "fill_color_rgba", bg.rgba(),
                            NULL);
    }
}

void 
XyPlot::change_axis(XyPlotState* state, 
                    PlotUtil::PositionType pos, 
                    const Axis& axis)
{
  update_all_layer_views();

  if (axes_[pos] != 0)
    {
      if (axis.hidden())
        {
          gnome_canvas_item_hide(axes_[pos]);
        }
      else
        {
          gnome_canvas_item_show(axes_[pos]);
          gnome_canvas_item_request_update(axes_[pos]);
        }
    }

  if (axis_labels_[pos] != 0)
    {
      if (axis.hidden() || axis.label_hidden() || !axis.labelled())
        {
          gnome_canvas_item_hide(axis_labels_[pos]);
        }
      else 
        {
          GFont* gfont = static_cast<GFont*>(axis.label_font());
          // Get 1.0 scale font, since GnomeCanvasText does scaling for us.
          GdkFont* gdkfont = gfont ? gfont->gdk_font(1.0) : 0;
          
          if (gdkfont != 0)
            {
              // keep in sync with else block
              gnome_canvas_item_set(axis_labels_[pos],
                                    "text", axis.label().c_str(),
                                    "font_gdk", gdkfont,
                                    NULL);
            }
          else
            {
              gnome_canvas_item_set(axis_labels_[pos],
                                    "text", axis.label().c_str(),
                                    "font", "fixed", // kinda bad - breaks our font cache.
                                    NULL);
            }

          gnome_canvas_item_show(axis_labels_[pos]);
        }
    }  
}

void 
XyPlot::change_size(XyPlotState* state, double width, double height)
{
  update_all_layer_views();

  if (bg_ != 0)
    {
      // this is pretty much just paranoia; GnomeCanvasRE doesn't like
      // 0 dimensions
      if (width <= 0.0 || height <= 0.0)
        gnome_canvas_item_hide(bg_);
      else 
        {
          gnome_canvas_item_set(bg_,
                                "x2", width,
                                NULL);
          
          gnome_canvas_item_set(bg_,
                                "y2", height,
                                NULL);
          gnome_canvas_item_show(bg_);
        }
    }

  gint i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      if (axes_[i] != 0)
        {
          gnome_canvas_item_request_update(axes_[i]);
        }

      ++i;
    }  

  i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      if (axis_labels_[i] != 0)
        {
          double centerx = 
            state_->axis_label_rect((PlotUtil::PositionType)i).x() + 
            state_->axis_label_rect((PlotUtil::PositionType)i).width()/2;
          double centery = 
            state_->axis_label_rect((PlotUtil::PositionType)i).y() + 
            state_->axis_label_rect((PlotUtil::PositionType)i).height()/2;

          GnomeCanvasItem* target = 0;

          if (i == PlotUtil::EAST)
            target = east_label_rotator_;
          else if (i == PlotUtil::WEST)
            target = west_label_rotator_;
          else
            target = axis_labels_[i];

          gnome_canvas_item_set(target,
                                "x", centerx, 
                                "y", centery,
                                NULL);
        }
      ++i;
    }

  plot_model.size_changed(this);
}

void 
XyPlot::change_name(XyPlotState* state, const string& name)
{
  plot_model.name_changed(this, name);
}

// Contents or order of the layer stack changed
void
XyPlot::remove_layer(XyPlotState* state, XyLayer* layer)
{
  destroy_layer_view(layer);
}

void
XyPlot::add_layer(XyPlotState* state, XyLayer* layer)
{
  create_layer_view(layer);
}

void
XyPlot::raise_layer(XyPlotState* state, XyLayer* layer)
{
  raise_layer_view(layer);
}

void
XyPlot::lower_layer(XyPlotState* state, XyLayer* layer)
{
  lower_layer_view(layer);
}

////////////

void
XyPlot::release_state()
{
  if (state_ != 0)
    {
      edit_.set_state(0);

      if (axis_edit_ != 0)
        axis_edit_->set_state(0);

      // Destroy all the layer views
      map<XyLayer*,XyLayerView*>::iterator i = layer_views_.begin();
      
      while (i != layer_views_.end())
        {
          delete i->second;

          ++i;
        }
      
      layer_views_.clear();

      state_->state_model.remove_view(this);

      guint refs = state_->unref();
      if (refs == 0)
        delete state_;

      state_ = 0;
    }
}


void 
XyPlot::set_state(XyPlotState* state)
{
  g_return_if_fail(state != 0);

  release_state();
  
  state_ = state;

  state_->ref();  
  state_->state_model.add_view(this);

  edit_.set_state(state_);
  if (axis_edit_ != 0)
    axis_edit_->set_state(state_);

  // Create views for each layer

  XyPlotState::iterator li = state_->begin();
  while (li != state_->end())
    {
      create_layer_view(*li);
      ++li;
    }

  update_everything();
}

void
XyPlot::update_everything()
{
  // Phony view notifications to get things updated...

  change_background(state_, state_->background());

  int i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      change_axis(state_, (PlotUtil::PositionType)i, state_->axis((PlotUtil::PositionType)i));

      ++i;
    }

  change_size(state_,state_->width(),state_->height());
  change_name(state_,state_->name());
}

XyPlotState*
XyPlot::state()
{
  return state_;
}

gint 
XyPlot::event_cb(GnomeCanvasItem* item, GdkEvent* e, gpointer data)
{
  XyPlot* sp = static_cast<XyPlot*>(data);

  return sp->event(e);
}

////////////// Event handler

gint 
XyPlot::event(GdkEvent* e)
{
  Tool* current_tool = guppi_plot_store()->get_current_tool(ID);

  if (current_tool)
    {
      return current_tool->event(this, e);
    }

  return FALSE;
}

////////////// Layer view map manager

void 
XyPlot::create_layer_view(XyLayer* layer)
{
  g_return_if_fail(state_ != 0);

  XyLayerView* view = 0;

  switch (layer->type())
    {
    case XyLayer::ScatterLayer:
      view = new XyScatterView(state_,
                               (XyScatter*)layer->cast_to_type(XyLayer::ScatterLayer));
      break;
    case XyLayer::LineLayer:
      view = new XyLineView(state_,
                            (XyLine*)layer->cast_to_type(XyLayer::LineLayer));
      break;

    case XyLayer::PriceLayer:
      view = new XyPriceView(state_,
                             (XyPriceBars*)layer->cast_to_type(XyLayer::PriceLayer));
      break;

    case XyLayer::RelationLayer:
    default:
      view = 0;
    }

  if (view != 0)
    {
      layer_views_[layer] = view;

      if (layer_group_ != 0) // ensure we are realized
        {
          view->realize(GNOME_CANVAS_GROUP(layer_group_));
        }
    }
}

void 
XyPlot::destroy_layer_view(XyLayer* layer)
{
  map<XyLayer*,XyLayerView*>::iterator i = layer_views_.find(layer);
  
  // Remember that we must gracefully handle the not-found case, since
  // there may be layers we don't understand what to do with...
  if (i != layer_views_.end())
    {
      delete i->second;
      layer_views_.erase(i);
    }
}

// Notice our implicit assumptions about stacking order: 
// layers are added to the top of the stack, and the canvas
//  adds items to the top of groups. 

void 
XyPlot::raise_layer_view(XyLayer* layer)
{
  map<XyLayer*,XyLayerView*>::iterator i = layer_views_.find(layer);

  // Remember that we must gracefully handle the not-found case, since
  // there may be layers we don't understand what to do with...
  if (i != layer_views_.end())
    {
      GnomeCanvasItem* item = i->second->item();

      if (item != 0) // ensure we are realized
        {
          gnome_canvas_item_raise(item,1);
        }
    }
}

void 
XyPlot::lower_layer_view(XyLayer* layer)
{
  map<XyLayer*,XyLayerView*>::iterator i = layer_views_.find(layer);

  // Remember that we must gracefully handle the not-found case, since
  // there may be layers we don't understand what to do with...
  if (i != layer_views_.end())
    {
      GnomeCanvasItem* item = i->second->item();

      if (item != 0) // ensure we are realized
        {
          gnome_canvas_item_lower(item,1);
        }
    }
}

void 
XyPlot::update_all_layer_views()
{
  if (group_ == 0)
    return; // not realized

  map<XyLayer*,XyLayerView*>::iterator i = layer_views_.begin();
  while (i != layer_views_.end())
    {
      i->second->update();

      ++i;
    }
}

void 
XyPlot::axis_info_func(struct _GuppiAxis* ga,
                       gpointer user_data,
                       const Axis** axis,
                       const Rectangle** rect,
                       const Transform** trans)
{
  XyPlot* xyp = static_cast<XyPlot*>(user_data);

  g_assert(xyp != 0);

  // Should be able to afford linear search through 4 items.
  int i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      if (xyp->axes_[i] == GNOME_CANVAS_ITEM(ga))
        break;

      ++i;
    }

  g_assert(i != PlotUtil::PositionTypeEnd);

  PlotUtil::PositionType pos = (PlotUtil::PositionType) i;

  if (axis)
    *axis = &xyp->state()->axis(pos);
  if (rect)
    *rect = &xyp->state()->axis_rect(pos);
  if (trans)
    *trans = &xyp->state()->trans(pos);
}

