// -*- C++ -*-

/* 
 * plot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plot.h"

Plot::Plot(const char* id, DataStore* ds)
  : id_(id), ds_(ds)
{
  
}

Plot::~Plot()
{
  g_return_if_fail (rc_.count() == 0);
}


guint 
Plot::ref() 
{
#ifdef DEBUG_RC
  g_debug("Referencing Plot %p, oldcount %u", this,rc_.count());
#endif
  return rc_.ref(); 
}

guint 
Plot::unref() 
{ 
#ifdef DEBUG_RC
  g_debug("Unreferencing Plot %p, oldcount %u", this,rc_.count());
#endif
  return rc_.unref(); 
}

void 
Plot::set_status(const string& status)
{
  plot_model.status_changed(this, status);
}

#include "plotstate.h"

bool
Plot::get_resize_lock()
{
  return plot_state()->get_resize_lock(static_cast<gpointer>(this));
}

void
Plot::release_resize_lock()
{
  return plot_state()->release_resize_lock(static_cast<gpointer>(this));
}
