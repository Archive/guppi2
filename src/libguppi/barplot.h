// -*- C++ -*-

/* 
 * barplot.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_BARPLOT_H
#define GUPPI_BARPLOT_H

#include "plot.h"
#include "barplotstate.h"
#include "barplotedit.h"
#include "tool.h"

class BarPlot : public Plot, public BarPlotState::View {
public:
  BarPlot(DataStore* ds);
  BarPlot(BarPlotState* state);
  virtual ~BarPlot();

  static const char* ID;

  virtual PlotState* plot_state() { return state_; }

  virtual Plot* new_view();

  virtual GnomeCanvasItem* item();

  virtual void realize(GnomeCanvasGroup* group);
  virtual void unrealize();

  virtual void set_x(double x);
  virtual void set_y(double y);

  virtual double x() const { return xpos_; }
  virtual double y() const { return ypos_; }

  virtual void set_size(double width, double height);

  virtual double width();
  virtual double height();

  virtual void size_request(double* width, double* height);

  const string& name() const;
  
  // BarPlotState::View

  virtual void change_bars(BarPlotState* state);
  virtual void change_data(BarPlotState* state, Data* data);
  virtual void change_background(BarPlotState* state, const Color & bg);
  virtual void change_size(BarPlotState* state, double width, double height);
  virtual void change_name(BarPlotState* state, const string& name);  

  // ::View

  virtual void destroy_model();

  void set_state(BarPlotState* state);
  BarPlotState* state() { return state_; }

private:
  GtkWidget* canvas_;

  // this saves enough info to recreate the same plot somewhere else.
  BarPlotState* state_;
  
  // group holding all the objects in the bar plot
  GnomeCanvasGroup* group_;

  // vector of bar items 
  vector<GnomeCanvasItem*> bars_;

  // background of the plot (a rectangle)
  GnomeCanvasItem* bg_;

  // base line
  GnomeCanvasItem* line_; 

  // Axis line, ticks, labels
  GnomeCanvasItem* axis_;

  BarPlotEdit edit_;

  double xpos_;
  double ypos_; 

  void release_state();
  void redisplay_bars();
  static gint event_cb(GnomeCanvasItem* item, GdkEvent* e, gpointer data);
  gint event(GdkEvent* e);

  static void axis_info_func(struct _GuppiAxis* ga,
                             gpointer user_data,
                             const Axis** axis,
                             const Rectangle** rect,
                             const Transform** trans);
};

#endif
