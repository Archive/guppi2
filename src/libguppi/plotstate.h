// -*- C++ -*-

/* 
 * plotstate.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTSTATE_H
#define GUPPI_PLOTSTATE_H

#include "util.h"
#include "refcount.h"
#include "magictoken.h"

class PlotState {
public:
  PlotState(); 
  virtual ~PlotState();

  // Only the corresponding Plot and smobs should do this refcounting thing
  guint ref();
  // returns remaining refs, if 0 then delete
  guint unref();

  // See plot.h/plotshell.h for explanation
  bool get_resize_lock(gpointer view);
  void release_resize_lock(gpointer view);

private:
  RC rc_;

  // only one view can resize at a time; otherwise 
  //  there are issues with size allocation...
  MagicToken resize_token_;

};

#endif
