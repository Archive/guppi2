// -*- C++ -*-

/* 
 * barplot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "barplot.h"
#include "canvas-axis.h"

const char* BarPlot::ID = "guppi-bar";

BarPlot::BarPlot(DataStore* datastore)
  : Plot(ID, datastore), canvas_(0), state_(0), group_(0), 
    bg_(0), line_(0), axis_(0),
    xpos_(0.0), ypos_(0.0)
{
  set_state(new BarPlotState(datastore));

  // defaults
  Data* scalar_data = 0;
  Data* label_data = 0;

  // Default to first two data sets with the right type
  DataStore::iterator i = datastore->begin();
  while (i != datastore->end())
    {
      if ((*i)->is_a(Data::Scalar))
        {
          if (scalar_data == 0)
            {
              scalar_data = *i;
            }
          else if (label_data != 0)
            break;
        }
      else if ((*i)->is_a(Data::Label))
        {
          if (label_data == 0)
            {
              label_data = *i;
            }
          else if (scalar_data != 0)
            break;
        }
      ++i;
    }

  if (scalar_data != 0)
    state_->set_data(scalar_data);
#if 0 // not implemented
  if (label_data != 0)
    state_->set_labels(label_data);
#endif
}

BarPlot::BarPlot(BarPlotState* state)
  : Plot(ID, state->store()), canvas_(0), state_(0), group_(0),  
    bg_(0), line_(0), axis_(0),
    xpos_(0.0), ypos_(0.0)
{
  set_state(state);
}

Plot* 
BarPlot::new_view()
{
  g_return_val_if_fail(state_ != 0, 0);
  return new BarPlot(state_);
}

BarPlot::~BarPlot()
{
  release_state();

  g_assert(state_ == 0);
  
  unrealize();
}

GnomeCanvasItem* 
BarPlot::item()
{
  g_return_val_if_fail(canvas_ != 0, 0);
  g_return_val_if_fail(group_ != 0, 0);

  return GNOME_CANVAS_ITEM(group_);
}

void
BarPlot::realize(GnomeCanvasGroup* group)
{
  g_return_if_fail(canvas_ == 0);
  g_return_if_fail(group != 0);
  g_return_if_fail(GNOME_IS_CANVAS_GROUP(group));
  g_return_if_fail(group_ == 0); // ensure not realized yet.

  canvas_ = GTK_WIDGET(GNOME_CANVAS_ITEM(group)->canvas);

  // Positions are just hardcoded for now

  group_ = 
    GNOME_CANVAS_GROUP(gnome_canvas_item_new(group,
                                             gnome_canvas_group_get_type(),
                                             "x", xpos_, "y", ypos_, NULL));
  
  bg_ = gnome_canvas_item_new(group_,
                              gnome_canvas_rect_get_type(),
                              NULL);

  line_ = gnome_canvas_item_new(group_,
                                gnome_canvas_line_get_type(),
                                "fill_color_rgba", 0xff,
                                NULL);

  axis_ = gnome_canvas_item_new(group_,
                                guppi_axis_get_type(),
                                NULL);

  guppi_axis_set_info_func(GUPPI_AXIS(axis_), axis_info_func, this);

  gtk_signal_connect(GTK_OBJECT(group_),
                     "event",
                     GTK_SIGNAL_FUNC(event_cb),
                     this);

  // We are going to ref our objects, so that the order of destruction
  //  (us or canvas first) isn't important
  gtk_object_ref(GTK_OBJECT(group_));
  gtk_object_ref(GTK_OBJECT(bg_));
  gtk_object_ref(GTK_OBJECT(line_));
  gtk_object_ref(GTK_OBJECT(axis_));

  change_background(state_, state_->background());    
  change_size(state_, state_->width(), state_->height());
}

void 
BarPlot::unrealize()
{
  vector<GnomeCanvasItem*>::iterator q = bars_.begin();
  while (q != bars_.end())
    {
      gtk_object_destroy(GTK_OBJECT(*q));
      ++q;
    }
  bars_.clear();
  
  if (bg_) gtk_object_destroy(GTK_OBJECT(bg_));
  if (line_) gtk_object_destroy(GTK_OBJECT(line_));
  if (axis_) gtk_object_destroy(GTK_OBJECT(axis_));
  
  // group last, since some objects are inside it.
  if (group_) gtk_object_destroy(GTK_OBJECT(group_));

  group_ = 0; 
  bg_ = 0;
  line_ = 0;
  axis_ = 0;
}

void 
BarPlot::set_x(double x)
{
  xpos_ = x;

  if (group_ != 0)
    gnome_canvas_item_set(GNOME_CANVAS_ITEM(group_),
                          "x", xpos_, NULL);
}

void 
BarPlot::set_y(double y)
{
  ypos_ = y;

  if (group_ != 0)
    gnome_canvas_item_set(GNOME_CANVAS_ITEM(group_),
                          "y", ypos_, NULL);
}

void 
BarPlot::set_size(double width, double height)
{
  // FIXME this does two size-change notifications
  state_->set_width(width);
  state_->set_height(height);
}

double 
BarPlot::width()
{
  return state_->width();
}

double 
BarPlot::height()
{
  return state_->height();
}

void 
BarPlot::size_request(double* width, double* height)
{
  g_return_if_fail(width != 0);
  g_return_if_fail(height != 0);

  state_->size_request(width, height);
}

const string& 
BarPlot::name() const
{
  return state_->name();
}

void
BarPlot::release_state()
{
  if (state_ != 0)
    {
      edit_.set_state(0); // so it won't refer to a destroyed state

      state_->state_model.remove_view(this);

      guint refs = state_->unref();
      if (refs == 0)
        delete state_;

      state_ = 0;
    }
}

void
BarPlot::set_state(BarPlotState* state)
{
  g_return_if_fail(state != 0);

  release_state();
  
  state_ = state;

  state_->ref();  
  state_->state_model.add_view(this);

  edit_.set_state(state_);

  change_size(state_, state_->width(), state_->height());
  change_background(state_, state_->background());
}

void
BarPlot::redisplay_bars()
{
  if (canvas_ == 0) return; // not an error, but we can't do anything.

  if (state_ == 0)
    {
      vector<GnomeCanvasItem*>::iterator q = bars_.begin();
      while (q != bars_.end())
        {
          gtk_object_destroy(GTK_OBJECT(*q));
          ++q;
        }
      bars_.clear();

      return;
    }

  const double height = state_->height();

  BarPlotState::iterator j = state_->begin();

  const guint nitems = bars_.size();
  guint item = 0;

  while (j != state_->end())
    {
      if (item >= nitems)
        {
          GnomeCanvasItem* ci = 
            gnome_canvas_item_new(group_,
                                  gnome_canvas_rect_get_type(),
                                  NULL);
          guppi_spew_on_destroy(GTK_OBJECT(ci));
          gtk_object_ref(GTK_OBJECT(ci));
          bars_.push_back(ci);

          // cast to gint since size may be 0
          g_assert((gint(bars_.size()) - 1) == gint(item));
        }

      g_assert(item < bars_.size());

      double y1 = state_->baseline();
      double y2 = (*j).top();

      gnome_canvas_item_set(bars_[item],
                            "x1", (double) (*j).pos(), 
                            "x2", (double) (*j).pos() + (*j).width(), 
                            "y1", (double) MIN(y1,y2),
                            "y2", (double) MAX(y1,y2), 
                            "outline_color", "black",
                            "fill_color_rgba",  (*j).color().rgba(),
                            NULL);
      ++j;
      ++item;
    }

  // item is now the index of the first unused item - possibly one past the end
  //  of bars_

  // Clean up any extra canvas items
  vector<GnomeCanvasItem*>::iterator erasefrom = bars_.begin() + item;
  while (item < nitems)
    {
      gtk_object_destroy(GTK_OBJECT(bars_[item]));
      bars_[item] = 0;
      ++item;
    }
  bars_.erase(erasefrom, bars_.end());

  g_assert(bars_.size() == state_->size());
  
  // Move the baseline

  GnomeCanvasPoints* points = gnome_canvas_points_new(2);
  
  points->coords[0] = state_->plot_rect().x();
  points->coords[1] = state_->baseline();
  points->coords[2] = state_->plot_rect().width();
  points->coords[3] = state_->baseline();

  gnome_canvas_item_set(line_, "points", points, NULL);
  
  gnome_canvas_points_free(points);  

  gnome_canvas_item_request_update(axis_);

  // Maybe this will kill a redraw bug (which I believe is the canvas's fault, 
  // but this shouldn't hurt...)
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(group_));
}

void 
BarPlot::change_bars(BarPlotState* state)
{
  redisplay_bars();
}

void 
BarPlot::change_data(BarPlotState* state, Data* data)
{
  // mostly covered by change_bars for now.
  if (data != 0)
    set_status(data->name());
}

void 
BarPlot::change_background(BarPlotState* state, const Color & bg)
{
  if (bg_ != 0)
    {
      gnome_canvas_item_set(bg_,
                            "fill_color_rgba", bg.rgba(),
                            NULL);
    }
}

void 
BarPlot::change_size(BarPlotState* state, double width, double height)
{
  if (bg_ != 0)
    {
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(bg_),
                            "x1", 0.0,
                            "y1", 0.0,
                            "x2", width,
                            "y2", height,
                            NULL);
    }
  redisplay_bars();

  plot_model.size_changed(this);
}

void 
BarPlot::change_name(BarPlotState* state, const string& name)
{
  plot_model.name_changed(this, name);
}

void 
BarPlot::destroy_model()
{
  state_ = 0;
}

gint 
BarPlot::event_cb(GnomeCanvasItem* item, GdkEvent* e, gpointer data)
{
  BarPlot* bp = static_cast<BarPlot*>(data);

  return bp->event(e);
}

gint 
BarPlot::event(GdkEvent* e)
{
  // FIXME once we have actions for bar plot...
  if (e->type == GDK_2BUTTON_PRESS) 
    {
      edit_.edit();
      return TRUE;
    }
  else 
    {
      return FALSE;
    }
}


void 
BarPlot::axis_info_func(struct _GuppiAxis* ga,
                        gpointer user_data,
                        const Axis** axis,
                        const Rectangle** rect,
                        const Transform** trans)
{
  BarPlot* bp = static_cast<BarPlot*>(user_data);

  g_assert(bp != 0);

  if (axis)
    *axis = &bp->state()->axis();
  if (rect)
    *rect = &bp->state()->axis_rect();
  if (trans)
    *trans = &bp->state()->trans();
}
