// -*- C++ -*-

/* 
 * util.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_UTIL_H
#define GUPPI_UTIL_H

#include <glib.h>

// This is evil; we need i18n but would prefer to skip Gnome.
// For now, this hack will work.

#ifdef _ 
#warning "_ already defined in util.h"
#else
#define _(x) x
#endif

#ifdef N_ 
#warning "N_ already defined in util.h"
#else
#define N_(x) x
#endif

#ifdef GNOME_ENABLE_DEBUG
#ifdef	__GNUC__
#define	g_debug(format, args...)	g_log (G_LOG_DOMAIN, \
					       G_LOG_LEVEL_DEBUG, \
					       format, ##args)
#else	/* !__GNUC__ */
static inline void
g_debug (const gchar *format,
	 ...)
{
  va_list args;
  va_start (args, format);
  g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, format, args);
  va_end (args);
}
#endif	/* !__GNUC__ */

#else /* GNOME_ENABLE_DEBUG */
#ifdef __GNUC__
#define g_debug(format, args...)
#else
static inline void 
g_debug (const gchar* format, ...) {}
#endif /* !GNUC */
#endif /* !GNOME_ENABLE_DEBUG */


#ifdef G_DISABLE_CHECKS

#define g_warn_if_fail(expr)

#else /* !G_DISABLE_CHECKS */

#ifdef __GNUC__

#define g_warn_if_fail(expr)		G_STMT_START{			\
     if (!(expr))							\
       {								\
	 g_log (G_LOG_DOMAIN,						\
		G_LOG_LEVEL_CRITICAL,					\
		"file %s: line %d (%s): assertion `%s' failed.",	\
		__FILE__,						\
		__LINE__,						\
		__PRETTY_FUNCTION__,					\
		#expr);							\
       };				}G_STMT_END

#else /* !__GNUC__ */

#define g_warn_if_fail(expr)		G_STMT_START{		\
     if (!(expr))						\
       {							\
	 g_log (G_LOG_DOMAIN,					\
		G_LOG_LEVEL_CRITICAL,				\
		"file %s: line %d: assertion `%s' failed.",	\
		__FILE__,					\
		__LINE__,					\
		#expr);						\
       };				}G_STMT_END

#endif /* !__GNUC__ */

#endif /* !G_DISABLE_CHECKS */

#ifdef GNOME_ENABLE_DEBUG
#define GUPPI_NOT_IMPLEMENTED g_warning("FIXME: %s not implemented", __FUNCTION__)
#else
#define GUPPI_NOT_IMPLEMENTED
#endif /* GNOME_ENABLE_DEBUG */

// Init the library
bool guppi_init_library();
// Vice-versa
void guppi_shutdown_library();

class VFont;

// Frontends fill in this vtable with appropriate
//  functions. 
class Frontend {
public:
  // Return a pointer to the default font.
  virtual VFont* default_font() = 0;


};

void guppi_register_frontend(Frontend* fe);

// Callers should check whether this returns 0
Frontend* guppi_frontend();

class DataGroupSpecStore;
DataGroupSpecStore* guppi_data_group_spec_store();

class DataGroupSpec;
DataGroupSpec* guppi_price_data_spec();

// slots in the price data spec
typedef enum {
  PriceDates,
  PriceOpens,
  PriceHighs,
  PriceLows,
  PriceCloses
} PriceDataSlotType;

#endif /* GUPPI_UTIL_H */
