// -*- C++ -*-

/* 
 * markerstyle.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_MARKERSTYLE_H
#define GUPPI_MARKERSTYLE_H

#include "plotutils.h"
#include "refcount.h"
#include "util.h"
#include "view.h"
#include <set>

// Instructions for rendering a marker on a scatterplot or
// whatever.
// Eventually it might be a virtual interface, if we want to have funky
// style types.

class MarkerStyle {
public:
  // OK, for now we'll have a limited range of 
  //  marker types, because it is just too potentially slow
  //  and annoying to deal with anything else.
  // If someone smart comes up with a better way someday,
  //  cool.
  // FIXME not all of them are supported yet.
  typedef enum {
    DOT,            // Single pixel point
    CIRCLE,         // Unfilled circle
    FILLED_CIRCLE,  // etc.
    BOX, 
    FILLED_BOX,
    DIAMOND, 
    FILLED_DIAMOND, 
    PLUS,            // +
    TIMES,           // x
    HORIZONTAL_TICK, // -
    VERTICAL_TICK    // |
  } Type;

  MarkerStyle() : size_(1.0), type_(DOT), name_(_("Anonymous Marker Style")) {}
  ~MarkerStyle() {}

  const Color& color() const { return color_; }
  void set_color(const Color& c) { color_ = c; }

  // size will standardly be in display coords
  // Makes no sense for some types of marker (notable DOT)
  double size() const { return size_; }
  void set_size(double size) { size_ = size; }

  Type type() const { return type_; }
  void set_type(Type t) { type_ = t; }

  bool operator==(const MarkerStyle & rhs) const {
    return 
      color_ == rhs.color_ &&
      size_  == rhs.size_  && 
      type_  == rhs.type_;
  }

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

  const string& name() const { return name_; }
  
  void set_name(const string& name) { name_ = name; }

private:
  RC rc_;
  Color color_;
  double size_; // width/height of marker
  Type type_;
  string name_;
};


class MarkerPalette {
public:
  MarkerPalette();
  ~MarkerPalette();

  // The default style ID; it should never be removed.
  static const guint32 DEFAULT;

  // Get the "current" marker, for brushing and the like.
  // You still have to be sure it exists with get(); 
  //  otherwise there would be weird race conditions.
  guint32 current() const;
  void set_current(guint32 id);

  // Returns an ID for accessing the MarkerStyle
  guint32 add(MarkerStyle* ms);

  // This will unref() the MarkerStyle, so hold a ref if you 
  //  want to keep it.
  void  remove(guint32 id);

  // This returns 0 if the palette entry no longer exists; this is OK
  // and expected. However, passing an id that has never been returned
  // from add() is not allowed.  You must check the return value!
  // Styles can be removed at any time.
  const MarkerStyle* get(guint32 id) const { 
#ifdef GNOME_ENABLE_DEBUG
    if (id >= markers_.size())
      {
        g_warning("Attempt to get style palette entry %u, which has never existed.\n",
                  id);
        return 0;
      }
#endif
    return markers_[id]; 
  }

  // can return 0 doubly: if the palette entry is gone, or if 
  //  someone else has it checked out.
  MarkerStyle* checkout(guint32 id);

  // Check a checked-out entry back in and notify views that 
  //  it has changed.
  void checkin(MarkerStyle* style, guint32 id);

  guint32 max_id() const;

  guint32 find(MarkerStyle* ms) const;

  class const_iterator;

  friend class const_iterator;

  class const_iterator {
  public:
    // Prefix
    const_iterator & operator++() 
      {
        guint32 end = p_->markers_.size();
        
        while (id_ < end)
          {
            ++id_;
            if (p_->markers_[id_] != 0) break;
          }
        return *this;
      }

    guint32 operator*()
      {
        return id_;
      }

    bool operator==(const const_iterator& rhs) {
      return (p_ == rhs.p_ && id_ == rhs.id_);
    }

    bool operator!=(const const_iterator& rhs) {
      return (id_ != rhs.id_ || p_ != rhs.p_);
    }

    const const_iterator& operator=(const const_iterator & rhs)
      {
        p_ = rhs.p_;
        id_ = rhs.id_;
        return *this;
      }

    const_iterator(const const_iterator& src)
      : p_(src.p_), id_(src.id_)
      {
      }

    const_iterator()
      : p_(0), id_(0)
      {

      }

    friend class MarkerPalette;

  private:
    const MarkerPalette* p_;
    guint32 id_;

    const_iterator(const MarkerPalette* p, guint32 id) 
      : p_(p), id_(id)
      {
      }
  };

  const_iterator begin() const { return const_iterator(this, DEFAULT); }
  const_iterator end() const { return const_iterator(this, markers_.size()); }

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

  class View : public ::View 
  {
  public:
    // You still have to check the return value of get(entryid) when you 
    //  deal with these notifications!
    virtual void add_entry(MarkerPalette* palette, guint32 entryid) = 0;
    virtual void remove_entry(MarkerPalette* palette, guint32 entryid) = 0;
    virtual void change_entry(MarkerPalette* palette, guint32 entryid) = 0;
    virtual void change_current(MarkerPalette* palette, 
                                guint32 oldcurrent, 
                                guint32 newcurrent) = 0;
  };

  class Model : public ::Model<MarkerPalette::View*> {
  public:
    void entry_added(MarkerPalette* palette, guint32 entryid) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->add_entry(palette, entryid);
	++i;
      }
      unlock_views();
    }
    void entry_removed(MarkerPalette* palette, guint32 entryid) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->remove_entry(palette, entryid);
	++i;
      }
      unlock_views();
    }
    void entry_changed(MarkerPalette* palette, guint32 entryid) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_entry(palette, entryid);
	++i;
      }
      unlock_views();
    }
    void current_changed(MarkerPalette* palette, guint32 oldcurrent, guint32 newcurrent) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_current(palette, oldcurrent, newcurrent);
	++i;
      }
      unlock_views();
    }
  };
  
  Model palette_model;  

private:
  RC rc_;

  vector<MarkerStyle*> markers_;
  
  vector<guint32> holes_;

  set<guint32> checkouts_;

  guint32 current_;

  void grab_style(MarkerStyle* style);
  void release_style(MarkerStyle* style);
};

// Get the global MarkerPalette. Mildly lame.
MarkerPalette* guppi_marker_palette();

#endif
