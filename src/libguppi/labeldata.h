// -*- C++ -*-

/* 
 * labeldata.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_LABELDATA_H
#define GUPPI_LABELDATA_H

#include "data.h"

class StringSet;

class LabelData : public Data {
public:

  LabelData();
  LabelData(StringSet* ss); // LabelData will own the StringSet
  virtual ~LabelData();

  // number of elements
  virtual gsize size() const;

  virtual const string& name() const;

  virtual void set_name(const string & name);

  void set_string(guint index, const string& value);
  const string& get_string(guint index) const;

  void add(const string& value);

  virtual xmlNodePtr xml(xmlNodePtr parent) const;
  
  void set_xml(xmlNodePtr node);

  const StringSet* stringset() const { return set_; }

  // checkin/checkout works just like ScalarData, see scalardata.h

  // Returns 0 if someone else has it
  StringSet* checkout_stringset();
  void checkin_stringset(StringSet* set, bool values_changed = true, bool other_changed = false);

  virtual gsize byte_size();

protected:
  StringSet* set_;

  bool checked_out_;

private:

  LabelData(const LabelData &);
  const LabelData& operator=(const LabelData&);
};

#endif
