// -*- C++ -*-

/* 
 * rangelist.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_RANGELIST_H
#define GUPPI_RANGELIST_H

#include "util.h"
#include <list>

// A rangelist stores a sorted list of non-overlapping
// ranges. Each range is associated with a value of type T.  Ranges
// are inclusive and may overlap infinitesimally at the edges
// (i.e. [start, stop])

template <class T>
class rangelist {
private:
  struct Range {
    Range() : start(0.0), stop(0.0) {}
    Range(double strt, double stp) : start(strt), stop(stp) {}
    double start;
    double stop;
  };

  typedef pair<Range,T> PairType;

  list<PairType> storage_;
  
  // smallest size range we bother to keep.  the smaller this is, the
  // more unbounded the size of the rangelist.
  double min_size_;

public:
  rangelist() : min_size_(1e-3) {}
  ~rangelist() {}

  // This insert could result in destroyed links; i.e. it invalidates
  // all iterators. It's also O(n) slow. Woo-hoo.
  void insert(double start, double stop, const T& value) {
    g_return_if_fail(stop >= start);

    // don't insert anything backward or tiny
    if ((stop - start) < min_size_)
      return;

    list<PairType>::iterator i = storage_.begin();
    while (i != storage_.end())
      {
        if (start > i->first.stop)
          {
            // No overlap; continue
            ++i;
            continue;
          }
        else if (stop < i->first.start)
          {
            // No overlap in entire remaining storage_; insert here
            storage_.insert(i, PairType(Range(start,stop), value));
            return;
          }
        else 
          {
            // the hard case; we overlap this one, and possibly more

            if (start <= i->first.start && stop < i->first.stop)
              {
                // situation:
                //    ------------ old
                // -------------   new
                // we know there's no preceding range that we overlap,
                // and no subsequent

                i->first.start = stop;
                storage_.insert(i, PairType(Range(start,stop), value));
                erase_if_too_small(i);
                return;
              }
            else if (start < i->first.stop && stop >= i->first.stop)
              {
                if (start > i->first.start)
                  {
                    // situation:
                    // ------------  ----  old
                    //    -------------    new
                    list<PairType>::iterator tmp = i;
                    // shrink the first one we overlap
                    i->first.stop = start;
                    ++i;
                    storage_.insert(i, PairType(Range(start,stop), value));
                    erase_if_too_small(tmp);
                  }
                else
                  {
                    // situation:
                    //    -----------  ----  old
                    // ------------------   new
                    // Overwrite the one we totally overlap
                    i->first.start = start;
                    i->first.stop = stop;
                    i->second = value;
                    ++i;
                  }

                // Could overlap subsequent ranges; nuke any we overlap totally, fix the last one.
                while (i != storage_.end())
                  {
                    if (start <= i->first.start && stop >= i->first.stop)
                      {
                        // must increment before we invalidate the iterator.
                        list<PairType>::iterator tmp = i;
                        ++tmp;
                        storage_.erase(i); // OK on list, bad on vector
                        i = tmp;
                        continue;
                      }
                    else if (stop > i->first.start)
                      {
                        i->first.start = stop;
                        erase_if_too_small(i);
                        return;
                      }
                    else 
                      {
                        return;
                      }
                  }
                return;
              }
            else if (start >= i->first.start && stop <= i->first.stop)
              {
                // situation:
                //  ---------------- old
                //     ---------     new
                // we need to split the old range
                
                list<PairType>::iterator oldi = i;
                T oldval = i->second;
                double oldstop = i->first.stop;
                i->first.stop = start; // first piece of old range
                // insert new range after first chunk from old range
                ++i;
                i = storage_.insert(i, PairType(Range(start,stop), value));

                erase_if_too_small(oldi);

                if ((oldstop - stop) > min_size_)
                  {
                    // insert second chunk from old range after the
                    // just-inserted new range
                    ++i;
                    storage_.insert(i, PairType(Range(stop,oldstop), oldval));
                  }
                return;
              }
#ifdef GNOME_ENABLE_DEBUG
            else
              {
                g_warning("Unhandled range relation. Old: %g - %g New: %g - %g", 
                          i->first.start, i->first.stop, start, stop);
                g_assert_not_reached();
              }
#endif
          }
          
        ++i;
      }
    
    // didn't find any overlap, push on the end
    storage_.push_back(PairType(Range(start,stop), value));
    return;
  }

  class iterator {
  public:
    iterator(list<PairType>::iterator i) : i_(i) {}

    iterator& operator++() { increment(); return *this; }
    iterator operator++(int) {
      iterator tmp = *this;
      increment();
      return tmp;
    }
    
    iterator& operator--() { decrement(); return *this; }
    iterator operator--(int) {
      iterator tmp = *this;
      decrement();
      return tmp;
    }

    void dereference(double* start, double* stop, T* t) {
      *start = i_->first.start;
      *stop = i_->first.stop;
      *t = i_->second;
    }

  private:
    void increment() {
      ++i_;
    }

    void decrement() {
      --i_;
    }

    list<PairType>::iterator i_;
    friend bool operator==<T>(const rangelist::iterator& x,
                              const rangelist::iterator& y);
    friend bool operator!=<T>(const rangelist::iterator& x,
                              const rangelist::iterator& y);
    
  };

  iterator begin() { return iterator(storage_.begin()); }
  iterator end() { return iterator(storage_.end()); }
  
  iterator iterator_at(double where) {
    list<PairType>::iterator i = storage_.begin();
    while (i != storage_.end())
      {
        if (where >= i->first.start && where <= i->first.stop)
          break;
        ++i;
      }
    return iterator(i);
  }

  void erase_if_too_small(list<PairType>::iterator& i) {
    g_return_if_fail(i != storage_.end());
    
    if ((i->first.stop - i->first.start) < min_size_)
      storage_.erase(i);
  }
  
};

template <class T>
inline bool operator==(const rangelist<T>::iterator& x,
                       const rangelist<T>::iterator& y) {
  return x.i_ == y.i_;
}

template <class T>
inline bool operator!=(const rangelist<T>::iterator& x,
                       const rangelist<T>::iterator& y) {
  return x.i_ != y.i_;
}


#endif
