
#include <gnome.h>

#include "gnome-action-manager.h"

static GtkWidget* app;

static gint delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data);
static void clicked_cb(GtkWidget* button, gpointer data);
static void action_cb(GnomeActionManager* am, const gchar* action_id, gpointer model, gpointer view, gpointer user_data);

/* Monitor signals */

static void action_invoked(GnomeActionManager* am, const gchar* action_id, 
                           gpointer model, gpointer view, gpointer user_data);

static void action_created(GnomeActionManager* am, const gchar* action_id, 
                           gpointer user_data);

static void action_destroyed(GnomeActionManager* am, const gchar* action_id, 
                             gpointer user_data);

static void group_created(GnomeActionManager* am, const gchar* group_id, 
                          gpointer user_data);

static void group_destroyed(GnomeActionManager* am, const gchar* group_id, 
                            gpointer user_data);

static void action_added(GnomeActionManager* am,
                         const gchar* group_id,
                         const gchar* action_id, 
                         gpointer user_data);

static void action_removed(GnomeActionManager* am,
                           const gchar* group_id,
                           const gchar* action_id,
                           gpointer user_data);


static void action_set_sensitive  (GnomeActionManager *am, const gchar* action_id, gboolean sensitivity, gpointer model, gpointer view, gpointer user_data);

static void action_toggled (GnomeActionManager *am, const gchar* action_id, gboolean active, gpointer model, gpointer view, gpointer user_data);


static GnomeActionManager* am;

int 
main(int argc, char** argv)
{
  GtkWidget* menubar;
  GtkWidget* button;
  GtkWidget* popup;
  GtkWidget* toolbar;

  gnome_init("am-test", "0.0", argc, argv);

  {
    guint i;

    printf("Testing random string creation:\n");

    i = 0;
    while (i < 100)
      {
        gchar* tmp = gnome_create_random_unique_string();

        printf("  %s\n", tmp);

        g_free(tmp);

        ++i;
      }
  }  

  app = gnome_app_new("am-test", "Testing Action Manager");

  gtk_signal_connect(GTK_OBJECT(app),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  am = gnome_action_manager_new();

  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_invoked",
                     GTK_SIGNAL_FUNC(action_invoked),
                     NULL);

  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_created",
                     GTK_SIGNAL_FUNC(action_created),
                     NULL);
     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_destroyed",
                     GTK_SIGNAL_FUNC(action_destroyed),
                     NULL);

     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "group_created",
                     GTK_SIGNAL_FUNC(group_created),
                     NULL);

     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "group_destroyed",
                     GTK_SIGNAL_FUNC(group_destroyed),
                     NULL);

     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_added",
                     GTK_SIGNAL_FUNC(action_added),
                     NULL);
     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_removed",
                     GTK_SIGNAL_FUNC(action_removed),
                     NULL);
     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_set_sensitive",
                     GTK_SIGNAL_FUNC(action_set_sensitive),
                     NULL);
     
  gtk_signal_connect(GTK_OBJECT(am), 
                     "action_toggled",
                     GTK_SIGNAL_FUNC(action_toggled),
                     NULL);

  gnome_action_manager_create_group(am, "menubar");
  gnome_action_manager_set_group_name(am, "menubar", "Menu Bar");

  gnome_action_manager_create_subtree(am, "foo_menu");
  gnome_action_manager_set_group_name(am, "foo_menu", "Foo");

  gnome_action_manager_create_subtree(am, "bar_menu");
  gnome_action_manager_set_group_name(am, "bar_menu", "Bar");

  gnome_action_manager_create_subtree(am, "history_menu");
  gnome_action_manager_set_group_name(am, "history_menu", "History");

  gnome_action_manager_create_group(am, "toolbar");
  gnome_action_manager_set_group_name(am, "toolbar", "Toolbar");

  gnome_action_manager_create_action(am, "action_number_one", action_cb, NULL);
  gnome_action_manager_set_name(am, "action_number_one", "Do Stuff");

  gnome_action_manager_create_action(am, "action_number_two", action_cb, NULL);
  gnome_action_manager_set_name(am, "action_number_two", "Do Other Stuff");

  gnome_action_manager_create_action(am, "desensitize_do_stuff", action_cb, NULL);
  gnome_action_manager_set_name(am, "desensitize_do_stuff", "Desensitize Do Stuff");

  gnome_action_manager_create_action(am, "sensitize_do_stuff", action_cb, NULL);
  gnome_action_manager_set_name(am, "sensitize_do_stuff", "Resensitize Do Stuff");

  gnome_action_manager_create_action(am, "add_new_action_to_foo", action_cb, NULL);
  gnome_action_manager_set_name(am, "add_new_action_to_foo", "Add new action to Foo menu");

  gnome_action_manager_create_action(am, "new_action", action_cb, NULL);
  gnome_action_manager_set_name(am, "new_action", "Newly-added action");

  gnome_action_manager_append(am, "foo_menu", "action_number_one");
  gnome_action_manager_append(am, "foo_menu", "action_number_two");
  gnome_action_manager_append(am, "foo_menu", "desensitize_do_stuff");
  gnome_action_manager_append(am, "bar_menu", "desensitize_do_stuff");
  gnome_action_manager_append(am, "bar_menu", "sensitize_do_stuff");
  gnome_action_manager_append(am, "bar_menu", "action_number_one");
  gnome_action_manager_append(am, "bar_menu", "add_new_action_to_foo");
  gnome_action_manager_append_group(am, "menubar", "foo_menu");
  gnome_action_manager_append_group(am, "menubar", "bar_menu");
  gnome_action_manager_append_group(am, "menubar", "history_menu");
  gnome_action_manager_append_group(am, "foo_menu", "history_menu");

  gnome_action_manager_append(am, "toolbar", "action_number_one");
  gnome_action_manager_append(am, "toolbar", "action_number_two");

/*  gnome_action_manager_set_history_group(am, "history_menu", NULL); */


  menubar = gnome_action_manager_create_menubar(am, "menubar", NULL, NULL);
  gnome_app_set_menus(GNOME_APP(app), GTK_MENU_BAR(menubar));

  button = gtk_button_new_with_label("Right-click for popup");
  gnome_app_set_contents(GNOME_APP(app), button);

  popup = gnome_action_manager_create_menu(am, "foo_menu", NULL, NULL);

  gnome_popup_menu_attach(popup, button, NULL);

#if 0
  toolbar = gnome_action_manager_make_toolbar(am, "toolbar", NULL, NULL, 
                                              GTK_ORIENTATION_HORIZONTAL);


  gnome_app_set_toolbar(GNOME_APP(app), GTK_TOOLBAR(toolbar));
#endif

  gtk_window_set_default_size(GTK_WINDOW(app), -1, 100);

  gtk_widget_show_all(app);

  gtk_main();

  gtk_object_destroy(GTK_OBJECT(am));

  return 0;
}

static void 
action_cb(GnomeActionManager* am, const gchar* action_id, gpointer model, gpointer view, gpointer user_data)
{
  printf("Action: %s\n", action_id);
#if 0

  if (strcmp(action_id, "desensitize_do_stuff") == 0)
    gnome_action_manager_set_sensitive(am, "action_number_one", FALSE);
  else if (strcmp(action_id, "sensitize_do_stuff") == 0)
    gnome_action_manager_set_sensitive(am, "action_number_one", TRUE);
  else if (strcmp(action_id, "add_new_action_to_foo") == 0)
    gnome_action_manager_append(am, "foo_menu", "new_action");
#endif
}

static void 
clicked_cb(GtkWidget* button, gpointer data)
{

}


static gint 
delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data)
{
  gtk_widget_destroy(app);
  gtk_main_quit();
  return FALSE;
}


static void action_invoked(GnomeActionManager* am, const gchar* action_id, 
                           gpointer model, gpointer view, gpointer user_data)
{
  printf("AM: %p invoked `%s' on model %p view %p user_data %p\n",
         am, action_id, model, view, user_data);

}

static void action_created(GnomeActionManager* am, const gchar* action_id, 
                           gpointer user_data)
{
  printf("AM: %p created action `%s' user_data %p\n", am, action_id, user_data);
}

static void action_destroyed(GnomeActionManager* am, const gchar* action_id, 
                             gpointer user_data)
{
  printf("AM: %p destroyed action `%s' user_data %p\n",  am, action_id, user_data);
}

static void group_created(GnomeActionManager* am, const gchar* group_id, 
                          gpointer user_data)
{
  printf("AM: %p created group `%s' user_data %p\n",  am, group_id, user_data);
}

static void group_destroyed(GnomeActionManager* am, const gchar* group_id, 
                            gpointer user_data)
{
  printf("AM: %p destroyed group `%s' user_data %p\n", am, group_id, user_data);
}

static void action_added(GnomeActionManager* am, 
                         const gchar* group_id,
                         const gchar* action_id, 
                         gpointer user_data)
{
  printf("AM: %p added `%s' to `%s' user_data %p\n", am, action_id, group_id, user_data);

}

static void action_removed(GnomeActionManager* am,
                           const gchar* group_id,
                           const gchar* action_id,
                           gpointer user_data)
{
  printf("AM: %p removed `%s' from `%s' user_data %p\n", am, action_id, group_id, user_data);
}


static void action_set_sensitive  (GnomeActionManager *am, const gchar* action_id, gboolean sensitivity, gpointer model, gpointer view, gpointer user_data)
{
  printf("AM: %p `%s' sensitivity set to %s for model %p view %p user_data %p\n", am, action_id, sensitivity ? "TRUE" : "FALSE", model, view, user_data);

}

static void action_toggled (GnomeActionManager *am, const gchar* action_id, gboolean active, gpointer model, gpointer view, gpointer user_data)
{
  printf("AM: %p `%s' toggled to %s for model %p view %p user_data %p\n", am, action_id, active ? "TRUE" : "FALSE", model, view, user_data);
}


