// -*- C++ -*-

/* 
 * refcount.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_REFCOUNT_H
#define GUPPI_REFCOUNT_H

#include <glib.h>

// Reference counting.
// Since this is used so often, I don't want to make it a base class;
//  classes that want to use it should have it as a member, then 
//  wrap ref and unref.

class RC {
public:
  RC() : refcount_(0) {}
#ifdef GNOME_ENABLE_DEBUG
  ~RC() { g_return_if_fail(refcount_ == 0); }
#endif

  // Both return the new count; for ref() you won't care usually, for unref()
  //  if it's 0 you should delete.
  guint ref();
  guint unref();

  guint count() const { return refcount_; }

private:
  guint refcount_;

  RC(const RC&);
  const RC& operator=(const RC&);
};

#endif
