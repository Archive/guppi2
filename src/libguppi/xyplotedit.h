// -*- C++ -*-

/* 
 * xyplotedit.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_XYPLOTEDIT_H
#define GUPPI_XYPLOTEDIT_H

#include "xyplotstate.h"
#include "dataoption.h"

#include "gnome-util.h"
#include "axisedit.h"

class XyLayerEdit;

class XyPlotEdit : public XyPlotState::View
{
public:
  XyPlotEdit();
  virtual ~XyPlotEdit();

  // XyPlotState::View

  virtual void change_background(XyPlotState* state, const Color & bg);
  virtual void change_axis(XyPlotState* state, 
                           PlotUtil::PositionType pos, 
                           const Axis& axis);
  virtual void change_size(XyPlotState* state, double width, double height);
  virtual void change_name(XyPlotState* state, const string& name);

  virtual void remove_layer(XyPlotState* state, XyLayer* layer);
  virtual void add_layer(XyPlotState* state, XyLayer* layer);
  virtual void raise_layer(XyPlotState* state, XyLayer* layer);
  virtual void lower_layer(XyPlotState* state, XyLayer* layer);

  // ::View
  virtual void destroy_model();

  // Other

  void set_state(XyPlotState* state);

  void edit();

private:
  GtkWidget* dialog_;

  XyPlotState* state_;

  GtkWidget* notebook_;

  map<XyLayer*,XyLayerEdit*> layer_views_;

#if 0
  // set when we're doing it ourselves
  bool changing_data_;

  DataOption x_option_;
  DataOption y_option_;

  GtkWidget* x_optimize_toggle_;
  GtkWidget* y_optimize_toggle_;

  static void data_chosen_cb(gpointer emitter, gpointer data);
  void data_chosen(DataOption* emitter);
#endif

  void release_state();

  static gint close_cb(GtkWidget* w, gpointer data);
  gint close();

  void add_view_for_layer(XyLayer* layer);
  void add_page_for_layer(XyLayerEdit* le);
};

class XyAxisEdit : public XyPlotState::View
{
public:
  XyAxisEdit();
  virtual ~XyAxisEdit();

  // XyPlotState::View

  virtual void change_background(XyPlotState* state, const Color & bg);
  virtual void change_axis(XyPlotState* state, 
                           PlotUtil::PositionType pos, 
                           const Axis& axis);
  virtual void change_size(XyPlotState* state, double width, double height);
  virtual void change_name(XyPlotState* state, const string& name);

  virtual void remove_layer(XyPlotState* state, XyLayer* layer);
  virtual void add_layer(XyPlotState* state, XyLayer* layer);
  virtual void raise_layer(XyPlotState* state, XyLayer* layer);
  virtual void lower_layer(XyPlotState* state, XyLayer* layer);

  // ::View
  virtual void destroy_model();

  void edit();

  void set_state(XyPlotState* state);

private:
  XyPlotState* state_;

  GtkWidget* dialog_;
  GtkWidget* axis_option_;

  GtkWidget* north_mi_;
  GtkWidget* south_mi_;
  GtkWidget* east_mi_;
  GtkWidget* west_mi_;

  GtkWidget* axis_edit_slot_;
  GtkWidget* current_axis_widget_;
  AxisEdit* current_axis_edit_;

  AxisEdit north_;
  AxisEdit south_;
  AxisEdit east_;
  AxisEdit west_;

  static gint close_cb(GtkWidget* w, gpointer data);
  gint close();

  static void axis_selected_cb(GtkWidget* w, gpointer data);
  void axis_selected(GtkWidget* mi);

  void set_current_axis_edit(AxisEdit* ae);

  void release_state();

  static void axisedit_changed_cb(gpointer emitter, gpointer data);
  
  static void dialog_clicked_cb(GtkWidget* dialog, gint button, gpointer data);

  void set_apply_sensitive(bool state);

  void apply();
};

#endif
