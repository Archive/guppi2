// -*- C++ -*-

/* 
 * canvas-axis.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_CANVASAXIS_H
#define GUPPI_CANVASAXIS_H

#include <libgnomeui/gnome-canvas.h>
#include <libart_lgpl/art_svp.h>

#include <plotutils.h>

// there is no reason to maintain strict C in here, but keep C++-isms
//  to a minimum just in case.

#define GUPPI_TYPE_AXIS            (guppi_axis_get_type ())
#define GUPPI_AXIS(obj)            (GTK_CHECK_CAST ((obj), GUPPI_TYPE_AXIS, GuppiAxis))
#define GUPPI_AXIS_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GUPPI_TYPE_AXIS, GuppiAxisClass))
#define GUPPI_IS_AXIS(obj)         (GTK_CHECK_TYPE ((obj), GUPPI_TYPE_AXIS))
#define GUPPI_IS_AXIS_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GUPPI_TYPE_AXIS))

struct _GuppiAxis;

typedef struct _GuppiAxis GuppiAxis;
typedef struct _GuppiAxisClass GuppiAxisClass;

  /* rectangle is rectangle the axis is to occupy; in item coords;
     Transform is from values to display, with display coordinates
     relative to ga->rect, i.e. rect->x() and rect->y() should be
     assumed to be 0.0,0.0 */ 

typedef void (*GuppiAxisInfoFunc) (GuppiAxis* ga,
                                   gpointer user_data,
                                   const Axis** axis,
                                   const Rectangle** rect,
                                   const Transform** trans);

struct _GuppiAxis {
  GnomeCanvasItem item;

  GuppiAxisInfoFunc info_func;
  gpointer user_data;

  /* Stuff filled in during update() */

  ArtSVP* axis_svp;

  Affine update_affine;

  /* In the "hideous hacks" category, we have...
     list of GnomeCanvasText used to render the tick labels.
  */
  GSList* texts;

  /* point 0, point 1, point 0, point 1, for each tick mark (4 ints per tick) */
  double* lines;

  guint nticks;
};

struct _GuppiAxisClass {
  GnomeCanvasItemClass parent_class;
  
};


/* Standard Gtk function */
GtkType guppi_axis_get_type (void);



void guppi_axis_set_info_func(GuppiAxis* ga, 
                              GuppiAxisInfoFunc info_func,
                              gpointer user_data);




#endif
