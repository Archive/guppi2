/* 
 * action-manager.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_ACTION_MANAGER_H
#define GUPPI_ACTION_MANAGER_H

#include <gnome.h>

BEGIN_GNOME_DECLS

/* This will be a GtkObject as soon as I get non-lazy */

typedef struct _GnomeActionManager GnomeActionManager;

struct _GnomeActionManager {
  /* All members very, very private */
  GHashTable* action_hash;
  GHashTable* view_hash;
};

/* You would have one ActionManager per kind of window, then create
   your toolbars, main menus, and popup menus from the same action
   manager; this lets you keep track of sensitivity, etc. */

GnomeActionManager* gnome_action_manager_new();
void gnome_action_manager_destroy(GnomeActionManager* am);

typedef void (* GnomeActionFunc) (GnomeActionManager* am, 
                                  const gchar* action_id, 
                                  gpointer model, 
                                  gpointer view, 
                                  gpointer user_data);



/* A plain action corresponds to a menu item or toolbar button that 
 * causes something to happen.
 */

void gnome_action_manager_add (GnomeActionManager* am,
                               const gchar* action_id,
                               GnomeActionFunc callback,
                               gpointer user_data);

/* A group corresponds to a menubar, toolbar, or submenu */
void gnome_action_manager_add_group (GnomeActionManager* am,
                                     const gchar* action_id);

/* A "dynamic" is a set of actions within some group that can change. For example, 
 * the recent documents on the bottom of the File menu group.
 */
#if 0
void gnome_action_manager_add_dynamic (GnomeActionManager* am,
                                       const gchar* action_id);
#endif

void gnome_action_manager_set_info (GnomeActionManager* am,
                                    const gchar* action_id,
                                    const gchar* user_visible_name,
                                    const gchar* editor_name,
                                    const gchar* tooltip,
                                    const gchar* small_icon,
                                    const gchar* large_icon,
                                    const gchar* stock_icon);

void gnome_action_manager_set_name (GnomeActionManager* am,
                                    const gchar* action_id,
                                    const gchar* user_visible_name);

void gnome_action_manager_set_editor_name (GnomeActionManager* am,
                                           const gchar* action_id,
                                           const gchar* editor_name);

void gnome_action_manager_set_tooltip (GnomeActionManager* am,
                                       const gchar* action_id,
                                       const gchar* tooltip);

void gnome_action_manager_set_small_icon (GnomeActionManager* am,
                                          const gchar* action_id,
                                          const gchar* filename);

void gnome_action_manager_set_large_icon (GnomeActionManager* am,
                                          const gchar* action_id,
                                          const gchar* filename);

void gnome_action_manager_set_stock_icon (GnomeActionManager* am,
                                          const gchar* action_id,
                                          const gchar* stock_icon);

void gnome_action_manager_remove_action (GnomeActionManager* am,
                                         const gchar* action_id);

void gnome_action_manager_append (GnomeActionManager* am,
                                  const gchar* group_id, /* Or dynamic */
                                  const gchar* action_id);

void gnome_action_manager_insert (GnomeActionManager* am,
                                  const gchar* group_id, /* Or dynamic */
                                  const gchar* action_id,
                                  gint position);

void gnome_action_manager_remove (GnomeActionManager* am,
                                  const gchar* group_id, /* or dynamic */
                                  const gchar* action_id);

void gnome_action_manager_invoke_action (GnomeActionManager* am,
                                         /* a real action, not a group or dynamic */
                                         const gchar* action_id,
                                         gpointer model,
                                         gpointer view);

void gnome_action_manager_set_sensitive (GnomeActionManager* am,
                                         const gchar* action_id,
                                         gboolean sensitive);

void gnome_action_manager_set_sensitive_by_model (GnomeActionManager* am,
                                                  const gchar* action_id,
                                                  gpointer model,
                                                  gboolean sensitive);

void gnome_action_manager_set_sensitive_by_view (GnomeActionManager* am,
                                                 const gchar* action_id,
                                                 gpointer view,
                                                 gboolean sensitive);

/* Create widgets that stay in sync with the actions or action groups */

GtkWidget* gnome_action_manager_make_menubar (GnomeActionManager* am,
                                              const gchar* group_id,
                                              gpointer model,
                                              gpointer view);

GtkWidget* gnome_action_manager_make_menu (GnomeActionManager* am,
                                           const gchar* group_id,
                                           gpointer model, 
                                           gpointer view);

GtkWidget* gnome_action_manager_make_toolbar (GnomeActionManager* am,
                                              const gchar* group_id,
                                              gpointer model,
                                              gpointer view,
                                              GtkOrientation orientation);

GtkWidget* gnome_action_manager_make_menuitem (GnomeActionManager* am,
                                               const gchar* action_id,
                                               gpointer model,
                                               gpointer view);

GtkWidget* gnome_action_manager_make_button (GnomeActionManager* am,
                                             const gchar* action_id,
                                             gpointer model, 
                                             gpointer view);

/* Build up an action manager group from a GnomeUIInfo */

void gnome_action_manager_fill_from_gnomeuiinfo (GnomeActionManager* am,
                                                 GnomeUIInfo* info);
                                                 
/* Set the bar to put menu item tooltips in for a given view;
   statusbar or appbar */
void gnome_action_manager_set_hint_bar(GnomeActionManager* am,
                                       gpointer view, 
                                       GtkWidget* bar);

/* Prepend recent actions to this group for the given view */
void gnome_action_manager_set_history_group(GnomeActionManager* am,
                                            const gchar* group_id,
                                            gpointer view);

/* Forget any info about the given view (hint bar, history group) */
void gnome_action_manager_forget_view(GnomeActionManager* am,
                                      gpointer view);


END_GNOME_DECLS

#endif
