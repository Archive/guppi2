// -*- C++ -*-

/* 
 * canvas-lineplot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomeui/gnome-canvas.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>
#include <libart_lgpl/art_rgb.h>
#include <libgnomeui/gnome-canvas-util.h>

#include "canvas-lineplot.h"
#include "util.h"
#include "rgbdraw.h"
#include "plotutils.h"
#include "pointtiles.h"
#include "xyplotstate.h"
#include "scalardata.h"

#include <math.h>

#include "guppi-math.h"

enum {
  ARG_0
};


static void guppi_lineplot_class_init (GuppiLineplotClass *klass);
static void guppi_lineplot_init       (GuppiLineplot      *gl);
static void guppi_lineplot_destroy    (GtkObject            *object);
static void guppi_lineplot_set_arg    (GtkObject            *object,
                                       GtkArg               *arg,
                                       guint                 arg_id);
static void guppi_lineplot_get_arg    (GtkObject            *object,
                                       GtkArg               *arg,
                                       guint                 arg_id);

static void   guppi_lineplot_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags);
static void   guppi_lineplot_realize     (GnomeCanvasItem *item);
static void   guppi_lineplot_unrealize   (GnomeCanvasItem *item);
static void   guppi_lineplot_draw        (GnomeCanvasItem *item, GdkDrawable *drawable,
                                          int x, int y, int width, int height);
static double guppi_lineplot_point       (GnomeCanvasItem *item, double x, double y,
                                          int cx, int cy, GnomeCanvasItem **actual_item);
static void   guppi_lineplot_translate   (GnomeCanvasItem *item, double dx, double dy);
static void   guppi_lineplot_bounds      (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);
static void   guppi_lineplot_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);


static GnomeCanvasItemClass *parent_class;


GtkType
guppi_lineplot_get_type (void)
{
  static GtkType gl_type = 0;

  if (!gl_type) {
    GtkTypeInfo gl_info = {
      "GuppiLineplot",
      sizeof (GuppiLineplot),
      sizeof (GuppiLineplotClass),
      (GtkClassInitFunc) guppi_lineplot_class_init,
      (GtkObjectInitFunc) guppi_lineplot_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    gl_type = gtk_type_unique (gnome_canvas_item_get_type (), &gl_info);
  }

  return gl_type;
}

static void
guppi_lineplot_class_init (GuppiLineplotClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = 
    (GnomeCanvasItemClass*)gtk_type_class (gnome_canvas_item_get_type ());

  object_class->destroy = guppi_lineplot_destroy;
  object_class->set_arg = guppi_lineplot_set_arg;
  object_class->get_arg = guppi_lineplot_get_arg;

  item_class->update = guppi_lineplot_update;
  item_class->realize = guppi_lineplot_realize;
  item_class->unrealize = guppi_lineplot_unrealize;
  item_class->draw = guppi_lineplot_draw;
  item_class->point = guppi_lineplot_point;
  item_class->translate = guppi_lineplot_translate;
  item_class->bounds = guppi_lineplot_bounds;

  item_class->render = guppi_lineplot_render;
}

static void
guppi_lineplot_init (GuppiLineplot *gl)
{
  gl->get_info = 0;
  gl->info_data = 0;
}

static void
guppi_lineplot_destroy (GtkObject *object)
{
  GuppiLineplot *gl;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GUPPI_IS_LINEPLOT(object));

  gl = GUPPI_LINEPLOT (object);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void 
get_info(GuppiLineplot* gl,                                 
         const Transform** xtrans, 
         const Transform** ytrans, 
         guint* N, 
         XyPlotState** state,
         XyLine** layer)
{
  if (gl->get_info)
    {
      XyPlotState* mystate;
      XyLine* mylayer;

      (*gl->get_info)(gl, gl->info_data, &mystate, &mylayer);
      
      g_assert(mystate != 0);
      g_assert(mylayer != 0);
      g_assert(mylayer->type() == XyLayer::LineLayer);

      ScalarData* xd = mylayer->x_data();
      ScalarData* yd = mylayer->y_data();
      const SortedPair& sorted = mylayer->sorted();
      
      if (xtrans)
        {
          *xtrans = &mystate->trans(mylayer->x_axis());
        }
      if (ytrans)
        {
          *ytrans = &mystate->trans(mylayer->y_axis());
        }
      if (N)
        {
          *N = mylayer->npoints();
        }
      if (state)
        {
          *state = mystate;
        }
      if (layer)
        {
          *layer = mylayer;
        }
    }
  else 
    {
      if (xtrans) *xtrans = 0;
      if (ytrans) *ytrans = 0;
      if (N) *N = 0;
      if (state) *state = 0;
      if (layer) *layer = 0;
    }      
}

static void
guppi_lineplot_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GuppiLineplot *gl;

  item = GNOME_CANVAS_ITEM (object);
  gl = GUPPI_LINEPLOT (object);

  switch (arg_id) {

  default:
    break;
  }
}

static void
guppi_lineplot_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

////// FIXME Optimize me please!

static void
guppi_lineplot_render (GnomeCanvasItem *item,
                       GnomeCanvasBuf *buf)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (item);

  const Transform* xtrans;
  const Transform* ytrans;
  guint N;
  const XyPlotState* state;
  const XyLine* layer;

  if (buf->is_bg)
    {
      gnome_canvas_buf_ensure_buf(buf);
      buf->is_bg = 0;
    }

  get_info(gl, &xtrans, &ytrans, &N, &(XyPlotState*)state, &(XyLine*)layer);

  if (N < 2) 
    return;  

  // consider the affine we got in update() (i2c)
  Affine i2c(gl->a);

  // We want the inverse of the affine, to determine the redraw 
  //  region
  Affine* c2i = i2c.new_inverted();

  double vx0 = buf->rect.x0;
  double vx1 = buf->rect.x1;
  double vy0 = buf->rect.y0;
  double vy1 = buf->rect.y1;

  // FIXME OK, this is wrong if the affine has any rotation in it.
  // The problem is that the *bounding rectangle* of the
  // inverse-transformed (value-coordinates) microtile covers a larger
  // area than the original microtile, unless the microtile is at 0,
  // 90, 180, 270 degrees.  Can potentially solve this with PointTiles
  // because we will have an efficient way to get only points in the
  // transformed microtile, assuming I can write get_tiles_in_region().

  // However, for now we will bail if the affine has rotation.
#ifdef GNOME_ENABLE_DEBUG
  if (!c2i->rectilinear())
    {
      g_warning("Non-rectilinear affine in %s", __FUNCTION__);
      return;
    }
#endif

  c2i->get_transformed_bounds(&vx0, &vy0, &vx1, &vy1);
  
  delete c2i;
  c2i = 0;

  // Use transforms to go from item to value coords
  vx0 = xtrans->inverse(vx0);
  vy0 = ytrans->inverse(vy0);
  vx1 = xtrans->inverse(vx1);
  vy1 = ytrans->inverse(vy1);

  // Re-order vx* as necessary
  PlotUtil::canonicalize_rectangle(&vx0, &vy0, &vx1, &vy1);

  //  g_debug("Rendering to data region %g,%g %g,%g", vx0, vy0, vx1, vy1);

  RGB rgb(buf->buf, buf->buf_rowstride, 
          (buf->rect.x1 - buf->rect.x0),
          (buf->rect.y1 - buf->rect.y0));

  bool just_translate = i2c.translation_only();

  const SortedPair& pair = layer->sorted();

  //////////// Draw with tiles

  // we only call this to force an update in the sorted data.
  // basically a hack.
  PointTiles* tiles = pair.tiles();

  // shouldn't happen, but paranoia.
  if (tiles == 0)
    return;

  RGB::Context ctx(rgb);

  Affine::Point p(0,0), p0(0,0), p1(0,0);

  // Get the bounds of the plot area, so we can set clipping on our
  // RGB buffer target.
  double sx1, sx2, sy1, sy2;
      
  xtrans->screen_bounds(&sx1,&sx2);
  ytrans->screen_bounds(&sy1,&sy2);

  p0.x = sx1;
  p0.y = sy1;

  p1.x = sx2;
  p1.y = sy2;

  i2c.transform(p0);
  i2c.transform(p1);

  PlotUtil::canonicalize_rectangle(&p0.x, &p0.y, &p1.x, &p1.y);

  int clipx = gint(p0.x - buf->rect.x0);
  int clipy = gint(p0.y - buf->rect.y0);

  // Get X and Y into the realm of possibility
  if (clipx < 0)
    clipx = 0;
  if (clipy < 0)
    clipy = 0;

  const int clipw = gint(p1.x - buf->rect.x0) - clipx;
  const int cliph = gint(p1.y - buf->rect.y0) - clipy;
      
  if (clipw < 0 || cliph < 0)
    {
      // the redraw region is empty.
      return;
    }
  else 
    {
      // This will truncate clipw and cliph which are almost
      // certainly off the drawing buffer.
      // if X or Y is too large they will also get nuked.
      ctx.clip(clipx, clipy, clipw, cliph);
    }

  if (ctx.clip_area() == 0)
    return; // the intersection of the transform bounds and the buffer was empty

  const int clipright = clipx + clipw;

  // Collect all the points... We just alloc a buffer large enough for all of them.
  RGB::Point* pts = new RGB::Point[N];

  // number of actually valid points.
  guint onscreen_points = 0;

  guint pi = 0;

  guint first_onscreen = 0;
  guint last_onscreen = 0;

  bool set_first = false;

  while (pi < N)
    {
      p.x = pair.sorted_key_value(pi);
      p.y = pair.sorted_other_value(pi);

      // This moves point from a data value to canvas pixel coords
      
      p.x = xtrans->transform(p.x);
      p.y = ytrans->transform(p.y);
      
      if (just_translate)
        {
          i2c.translate_transform(p);
        }
      else 
        {
          i2c.transform(p);
        }

      // This is sort of poorly thought-out and should be fixed.
      // Need to be more precise.
      int cx = (int)rint(p.x);
      int cy = (int)rint(p.y);
      
      // Convert to buffer indices - could be merged with affine 
      //  transform?
      cx -= buf->rect.x0;
      cy -= buf->rect.y0;
      
      pts[pi].x = cx;
      pts[pi].y = cy;
      
      if (cx >= clipx && 
          cx <= clipright)
        {
          ++onscreen_points;
          
          if (!set_first)
            {
              first_onscreen = pi;
              set_first = true;
            }

          last_onscreen = pi;
        }

      ++pi;
    }

  if (set_first)
    {
      ctx.set_color(Color(0,0,0));
      
      // Get one off to the left
      if (first_onscreen > 0)
        {
          --first_onscreen;
          ++onscreen_points;
        }

      // Get one off to the right
      if (last_onscreen < (N-1))
        {
          ++onscreen_points;
        }

      rgb.paint_line(onscreen_points, 
                     &pts[first_onscreen],
                     ctx);
    }

  delete [] pts;
  pts = 0;

#ifdef GNOME_ENABLE_DEBUG_DISABLE
  rgb.paint_rect(0,0, 
                 buf->rect.x1-buf->rect.x0-1, 
                 buf->rect.y1-buf->rect.y0-1, 
                 0xf, 0xff, 0x0, 0x2a);
#endif

#ifdef GNOME_ENABLE_DEBUG_DISABLE

  static int which = 0;

  guchar colors[3] = { 0x0, 0x0, 0x0 };
  
  colors[which] = 0xff;
#if 0
  rgb.paint_rect(0,0, 
                 buf->rect.x1-buf->rect.x0-1, 
                 buf->rect.y1-buf->rect.y0-1, 
                 colors[0], colors[1], colors[2], 0x2a);
#else
  rgb.set_pixel(0,0, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(0,1, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(0,2, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(1,0, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(2,0, colors[0], colors[1], colors[2], 0x2a);

  guint bottom = buf->rect.y1-buf->rect.y0-1;
  guint right = buf->rect.x1-buf->rect.x0-1;
  rgb.set_pixel(right, bottom, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(right,bottom-1, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(right,bottom-2, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(right-1,bottom, colors[0], colors[1], colors[2], 0x2a);
  rgb.set_pixel(right-2,bottom, colors[0], colors[1], colors[2], 0x2a);
#endif

  ++which;
  if (which > 2) which = 0;

#endif
}


//static GnomeCanvasItem* rect = 0;

static void
guppi_lineplot_update (GnomeCanvasItem *item, double *update_affine, ArtSVP *clip_path, int flags)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (item);

  if (parent_class->update)
    (* parent_class->update) (item, update_affine, clip_path, flags);

  if (item->canvas->aa) 
    {
      memcpy(gl->a, update_affine, sizeof(double)*6);

      gnome_canvas_item_reset_bounds (item);

      guint N;
      const Transform* xtrans;
      const Transform* ytrans;    

      get_info(gl, &xtrans, &ytrans, &N, 0, 0);

      // Get item coords bounds
      double dx1, dy1, dx2, dy2;
      xtrans->screen_bounds(&dx1, &dx2);
      ytrans->screen_bounds(&dy1, &dy2);

      PlotUtil::canonicalize_rectangle(&dx1, &dy1, &dx2, &dy2);
    
      // So now we have the bounds in Item coords; we apply the 
      // update_affine to them 

      Affine i2c(update_affine);

      Affine::Point nw(dx1,dy1), se(dx2,dy2);

      i2c.transform(nw);
      i2c.transform(se);

#if 0
      int x1, x2, y1, y2;
      gnome_canvas_item_i2w(item, &dx1, &dy1);
      gnome_canvas_item_i2w(item, &dx2, &dy2);
      gnome_canvas_w2c(item->canvas, dx1, dy1, &x1, &y1);
      gnome_canvas_w2c(item->canvas, dx2, dy2, &x2, &y2);
#endif

      // FIXME what if nw/se coords got re-ordered?

      gnome_canvas_update_bbox(item, 
                               static_cast<int>(floor(nw.x)), 
                               static_cast<int>(floor(nw.y)), 
                               static_cast<int>(ceil(se.x)), 
                               static_cast<int>(ceil(se.y)));

      g_debug("requesting redraw on %g,%g %g,%g", nw.x, nw.y, se.x, se.y);
    } 
  else 
    {
      g_warning("lineplot item requires AA mode");
    }
}

static void
guppi_lineplot_realize (GnomeCanvasItem *item)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (item);

  if (parent_class->realize)
    (* parent_class->realize) (item);
}

static void
guppi_lineplot_unrealize (GnomeCanvasItem *item)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (item);

  if (parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
guppi_lineplot_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
                     int x, int y, int width, int height)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (item);

  g_warning("GuppiLineplot does not work on the Gdk canvas");
}

static double
guppi_lineplot_point (GnomeCanvasItem *item, double x, double y,
                      int cx, int cy, GnomeCanvasItem **actual_item)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT(item);

  *actual_item = item;

  double x1, y1, x2, y2;

  guppi_lineplot_bounds(item, &x1, &y1, &x2, &y2);

  if (x <= x2 && x >= x1 && y <= y2 && y >= y1)
    return 0.0;
  else
    {
      double dx, dy;

      if (x > x2)
        dx = x - x2;
      else if (x < x1)
        dx = x1 - x;
      else 
        dx = 0.0;

      if (y > y2)
        dy = y - y2;
      else if (y < y1)
        dy = y1 - y;
      else 
        dy = 0.0;

      g_assert(dx > 0.0 || dy > 0.0);

      return PlotUtil::hypot(dx, dy);
    }
}

static void
guppi_lineplot_translate (GnomeCanvasItem *item, double dx, double dy)
{
  GuppiLineplot *gl;
  gl = GUPPI_LINEPLOT (item);

  g_warning("lineplot translate isn't implemented");
}

static void
guppi_lineplot_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2)
{
  GuppiLineplot *gl;

  gl = GUPPI_LINEPLOT (item);

  guint N;
  const Transform* xtrans;
  const Transform* ytrans;
    
  get_info(gl, &xtrans, &ytrans, &N, 0, 0);

  xtrans->screen_bounds(x1, x2);
  ytrans->screen_bounds(y1, y2);

  PlotUtil::canonicalize_rectangle(x1, y1, x2, y2);
    
  //  g_debug("Bounds: %g,%g %g,%g", *x1, *y1, *x2, *y2);
  
  // So now we have the bounds in Item coords
}


void 
guppi_lineplot_set_info_func(GuppiLineplot* gl, 
                             LineplotInfoFunc func,
                             gpointer user_data)
{
  g_return_if_fail(gl != 0);
  g_return_if_fail(GUPPI_IS_LINEPLOT(gl));

  gl->get_info = func;
  gl->info_data = user_data;
}



