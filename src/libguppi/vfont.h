// -*- C++ -*-

/* 
 * vfont.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_VFONT_H
#define GUPPI_VFONT_H

// A base class for handles to fonts. 
// Each frontend will have a single concrete subclass, and all VFont
//  instances in that frontend will be instances of the subclass as 
//  well.

#include "util.h"
#include "refcount.h"

class VFont {
public:
  virtual ~VFont() {}

  // Return the width of string str, unscaled.
  virtual double string_width(const gchar* str) const = 0;

  // Return the height of the font, unscaled.
  virtual double font_height() const = 0;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

protected:
  // Can't be instantiated, only subclasses can.
  //  (so this constructor is protected)
  VFont() {}

private:
  RC rc_;

  const VFont& operator=(const VFont&);
  VFont(const VFont &);

};

#endif
