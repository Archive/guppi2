// -*- C++ -*-

/* 
 * xyplot.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_XYPLOT_H
#define GUPPI_XYPLOT_H

#include "plot.h"

#include "xyplotstate.h"
#include "xyplotedit.h"

#include "tool.h"


class PointTiles;
class XyLayerView; // view onto a particular layer type; defined in .cc

class XyPlot : public Plot, 
               public XyPlotState::View
{
public:
  XyPlot(DataStore* ds);
  XyPlot(XyPlotState* state);
  virtual ~XyPlot();

  static const char* ID;

  virtual PlotState* plot_state() { return state_; }

  virtual Plot* new_view();

  virtual GnomeCanvasItem* item();

  virtual void realize(GnomeCanvasGroup* group);
  virtual void unrealize();

  virtual void set_x(double x);
  virtual void set_y(double y);

  virtual double x() const { return xpos_; }
  virtual double y() const { return ypos_; }

  virtual void set_size(double width, double height);

  virtual double width();
  virtual double height();

  virtual void size_request(double* width, double* height);

  void set_state(XyPlotState* state);

  const string& name() const;

  XyPlotState* state();

  void edit() { edit_.edit(); }
  void axis_edit();

  // FIXME views for different layer types, here or in auxiliary classes...

  // XyPlotState::View

  virtual void change_background(XyPlotState* state, const Color & bg);
  virtual void change_axis(XyPlotState* state, 
                           PlotUtil::PositionType pos, 
                           const Axis& axis);
  virtual void change_size(XyPlotState* state, double width, double height);
  virtual void change_name(XyPlotState* state, const string& name);
  
  virtual void remove_layer(XyPlotState* state, XyLayer* layer);
  virtual void add_layer(XyPlotState* state, XyLayer* layer);
  virtual void raise_layer(XyPlotState* state, XyLayer* layer);
  virtual void lower_layer(XyPlotState* state, XyLayer* layer);

  // ::View
  virtual void destroy_model() {} // could be palette, data, or state...

private:
  XyPlotState* state_;

  GtkWidget* canvas_;

  GnomeCanvasItem* group_;
  GnomeCanvasItem* layer_group_; // so we can manage layer's stacking order.
  GnomeCanvasItem* bg_;

  GnomeCanvasItem* axes_[PlotUtil::PositionTypeEnd];

  GnomeCanvasItem* axis_labels_[PlotUtil::PositionTypeEnd];

  // We need to rotate the Y label with respect to 
  // its center point.
  // So we put it in a group, and then center it on the group's origin.
  //  Since affine transforms are done with respect to item coords 0.0, 0.0,
  //  this makes things come out right.
  // Lame hack? Yes.
  GnomeCanvasItem* east_label_rotator_;
  GnomeCanvasItem* west_label_rotator_;

  // position of the group containing all the pieces of the plot
  double xpos_;
  double ypos_;
  
  XyPlotEdit edit_;
  XyAxisEdit* axis_edit_;

  map<XyLayer*,XyLayerView*> layer_views_;

  // Callbacks for the canvas-scatter

  static gint event_cb(GnomeCanvasItem* item, GdkEvent* e, gpointer data);
  gint event(GdkEvent* e);

  void update_everything();
  void release_state();

  void create_layer_view(XyLayer* layer);
  void destroy_layer_view(XyLayer* layer);
  void raise_layer_view(XyLayer* layer);
  void lower_layer_view(XyLayer* layer);

  void update_all_layer_views();

  static void axis_info_func(struct _GuppiAxis* ga,
                             gpointer user_data,
                             const Axis** axis,
                             const Rectangle** rect,
                             const Transform** trans);

  // I don't remember what this was for
  //  friend class XyAction;
};

#endif


