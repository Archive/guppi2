// -*- C++ -*-

/* 
 * tool.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "tool.h"
#include "plot.h"
#include "plotstore.h"

void 
Action::grab(Plot* p)
{
  Tool* t = guppi_plot_store()->get_current_tool(p->id());

  g_return_if_fail(t != 0);

  t->action_grab(p, this);
}

void 
Action::ungrab(Plot* p)
{
  Tool* t = guppi_plot_store()->get_current_tool(p->id());

  g_return_if_fail(t != 0);

  t->action_ungrab(p);
}

ActionChest::ActionChest()
{


}

ActionChest::~ActionChest()
{


}

void 
ActionChest::set_action(Action* a, gint button, GdkModifierType mask)
{
  actions_[button][mask] = a;
}

Action* 
ActionChest::get_action(gint button, GdkModifierType mask) const
{
  map<gint, map<GdkModifierType, Action*> >::const_iterator i = 
    actions_.find(button);

  if (i == actions_.end()) 
    return 0;
  else
    {
      map<GdkModifierType, Action*>::const_iterator j = i->second.find(mask);

      if (j == i->second.end())
        return 0;
      else 
        return j->second;
    }
}

Tool::Tool(const char* id) 
  : id_(id)
{

}

Tool::~Tool()
{
  
}

void 
Tool::set_action(const Action* a, gint button, GdkModifierType mask)
{
  // the principle of this const_cast is that we already have the non-const 
  // pointer in our actions_ vector; it isn't const with respect to us,
  // only with respect to the Plot user
  Action* nca = const_cast<Action*>(a);
  Action* old = chest_.get_action(button, mask);

  if (nca == old)
    return;

  chest_.set_action(nca, button, mask);
}

const Action* 
Tool::get_action(gint button, GdkModifierType mask) const
{
  return chest_.get_action(button, mask);
}

gint 
Tool::event(Plot* p, GdkEvent* e)
{
  map<Plot*,Action*>::iterator ga = grabs_.find(p);

  if (ga != grabs_.end())
    {
      return ga->second->event(p, e);
    }
  else if (e->type == GDK_BUTTON_PRESS || 
           e->type == GDK_2BUTTON_PRESS)
    {
      Action* action = 
        chest_.get_action((gint)e->button.button, 
                          (GdkModifierType)e->button.state);
      

      if (action != 0)
        return action->event(p, e);
      else 
        return FALSE;
    }
  else 
    {
      return FALSE;
    }
}

void 
Tool::action_grab(Plot* p, Action* a)
{
  g_return_if_fail(p != 0);

#ifdef GNOME_ENABLE_DEBUG
  map<Plot*,Action*>::iterator ga = grabs_.find(p);

  g_return_if_fail(ga == grabs_.end());
#endif

  // This just replaces any existing grab - not so good really...
  grabs_[p] = a;
}

void 
Tool::action_ungrab(Plot* p)
{
  g_return_if_fail(p != 0);

  map<Plot*,Action*>::iterator ga = grabs_.find(p);

  g_return_if_fail(ga != grabs_.end());

  if (ga != grabs_.end())
    grabs_.erase(ga);
}

////////////// 

ActionTool::ActionTool(const char* id, 
                       const char* name, const char* tooltip, 
                       const char* icon_name)
  : Tool(id), name_(name), tooltip_(tooltip), icon_(icon_name)
{


}

ActionTool::~ActionTool()
{

}

// user-visible name of the tool
const string& 
ActionTool::name() const
{
  return name_;
}

// tooltip describing the tool
const string& 
ActionTool::tooltip() const
{
  return tooltip_;
}

// pixmap for the tool
const string& 
ActionTool::icon_name() const
{
  return icon_;
}

// There is a default implementation of this that sees whether 
//  any actions are bound to this event and invokes them if so.
//  Subclasses can call the default implementation as appropriate.
gint 
ActionTool::event(Plot* p, GdkEvent* event)
{
  return Tool::event(p, event);
}

// Called when this tool is made current
void 
ActionTool::activate()
{
  
}

// Called when it's made non-current.
void 
ActionTool::deactivate()
{
  // grabs_ is private
  //   g_warn_if_fail(grabs_.empty());
}

