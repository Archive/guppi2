// -*- C++ -*-

/* 
 * callbacks.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "callbacks.h"

CallbackList::CallbackList(void* emitter)
  : emitter_(emitter)
{
  

}

CallbackList::~CallbackList()
{


}


void 
CallbackList::connect(Callback cb, void* data)
{
  Closure c;
  c.first = cb;
  c.second = data;

  callbacks_.push_back(c);
}

void 
CallbackList::disconnect(Callback cb)
{
  // This minor inefficiency should not matter

  vector<Closure>::iterator i = callbacks_.begin();
  while (i != callbacks_.end())
    {
      if (i->first == cb)
        break;
      
      ++i;
    }

  if (i != callbacks_.end())
    {
      callbacks_.erase(i);
    }
  else
    {
      g_warning("Disconnecting nonexistent callback!");
    }
}

void 
CallbackList::invoke()
{
  vector<Closure>::iterator i = callbacks_.begin();
  while (i != callbacks_.end())
    {

      (*i->first)(emitter_, i->second);

      ++i;
    }
}

