// -*- C++ -*-

/* 
 * fontoption.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "fontoption.h"


FontOption::FontOption()
  : option_(0), menu_(0), current_(0), 
    changing_font_(false), cblist_(this)
{
  

}

FontOption::~FontOption()
{

}

void 
FontOption::set_font(GFont* f)
{
  if (current_ == f) return;

  current_ = f;

  if (option_ != 0)
    {
      map<GFont*,gint>::iterator i = indexes_.find(current_);
      g_return_if_fail(i != indexes_.end());
      gtk_option_menu_set_history(GTK_OPTION_MENU(option_), i->second);
    }  

  // notify callback listeners.
  cblist_.invoke();
}

GFont* 
FontOption::get_font()
{
  return current_;
}

GtkWidget* 
FontOption::widget()
{
  if (option_ == 0)
    {
      option_ = gtk_option_menu_new();
 
      // Zero option_ if it's destroyed
      gtk_signal_connect(GTK_OBJECT(option_),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &option_);
 
      menu_ = 0; // should be already, though.
      
      make_font_menu();
      
      gtk_widget_show(option_);
    }

  return option_;
}

static void
set_widget_font(GtkWidget* w, GFont* gfont)
{ 
  GdkFont* font = gfont->gdk_font(1.0);

  if (font != 0)
    {
      gtk_widget_ensure_style(w);

      GtkStyle* style = gtk_style_copy(w->style);

      gdk_font_unref (style->font);
      
      style->font = font;

      gdk_font_ref(style->font);

      gtk_widget_set_style(w, style);

      gtk_style_unref(style);
    }
  else
    {
      g_warning("Couldn't find a good screen font for %s\n",
                gfont->font_name());
    }
}

void 
FontOption::make_font_menu()
{
  g_return_if_fail(option_ != 0);

  if (menu_ != 0) { // if the contents of the DataStore change
    gtk_widget_destroy(menu_);
    indexes_.clear();
  }

  menu_ = gtk_menu_new();

  GFontList* fl = guppi_get_font_list();

  g_assert(fl != 0);

  GFont* default_font = 0;
  Frontend* fe = guppi_frontend();
  if (fe != 0)
    {
      default_font = static_cast<GFont*>(fe->default_font());
    }

  GFontList::iterator i = fl->begin();
  GFontList::iterator end = fl->end();
  
  int active = -1;
  int count = 0;

  while (i < end)
    {
      GFont* gf = *i;

      GtkWidget* mi = gtk_menu_item_new();
      GtkWidget* l = gtk_label_new(gf->font_name());

      set_widget_font(l, gf);

      gtk_container_add(GTK_CONTAINER(mi), l);
      
      indexes_[gf] = count;

      gtk_menu_append(GTK_MENU(menu_), mi);

      gtk_object_set_data(GTK_OBJECT(mi), "Font", gf);

      gtk_signal_connect(GTK_OBJECT(mi), "activate",
                         GTK_SIGNAL_FUNC(font_activated_cb),
                         this);

      if (current_ == gf)
        {
          active = count;
        }

      if (!current_ && default_font && current_ == default_font)
        {
          active = count;
        }
      
      gtk_widget_show_all(mi);

      ++count;
      ++i;
    }

  gtk_option_menu_set_menu(GTK_OPTION_MENU(option_), menu_);

  if (active >= 0)
    {
      gtk_option_menu_set_history(GTK_OPTION_MENU(option_), active);
    }
}

void 
FontOption::font_activated_cb(GtkWidget* w, gpointer data)
{
  gpointer vd = gtk_object_get_data(GTK_OBJECT(w), "Font");

  // vd may be 0, for the "no data chosen" case!

  GFont* f = static_cast<GFont*>(vd);

  FontOption* fop = static_cast<FontOption*>(data);

  g_return_if_fail(fop != 0);

  fop->font_activated(f);
}

void 
FontOption::font_activated(GFont* f)
{
  set_font(f);
}

