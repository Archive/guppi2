// -*- C++ -*-

/* 
 * xyplotstate.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xyplotstate.h"
#include "scalardata.h"
#include "markerstyle.h"
#include "vfont.h"
#include "typeregistry.h"
#include "datagroup.h"
#include "datedata.h"

#include "guppi-math.h"

OneAxis::OneAxis()
  : axis_checked_out_(false)
{
  Frontend* fe = guppi_frontend();
  if (fe != 0)
    axis_.set_label_font(fe->default_font());
}

OneAxis::~OneAxis()
{

}

OneAxis* 
OneAxis::clone()
{
  OneAxis* retval = new OneAxis;

  retval->axis_ = axis_;

  return retval;
}

Axis* 
OneAxis::checkout_axis()
{
  if (axis_checked_out_) return 0;
  
  axis_checked_out_ = true;

  return &axis_;
}

void 
OneAxis::checkin_axis(Axis* a)
{
  g_return_if_fail(axis_checked_out_);
  g_return_if_fail(a == &axis_);

  axis_checked_out_ = false;

  one_model.axis_changed(this, axis_);
}                    

void 
OneAxis::recalc_ticks(guint nticks)
{
  axis_.generate_ticks(nticks, 4.0); // FIXME (hardcoded tick spacing)
 
  // Commenting this out is pretty much a huge hack.  We know that
  // ticks are only recalculated by XyPlotState (via XyAxes)
  // in response to axis change notifications. Thus all xy plot
  // state will already have been notified of the change, and they
  // will have queued a redraw on their canvas axis objects.  If we
  // leave this in, we get infinite recursion.

  //  half_model.axis_changed(this, axis_);
}

//////////////////////

XyAxes::XyAxes()
{
  
  axes_[PlotUtil::NORTH] = new OneAxis;
  axes_[PlotUtil::SOUTH] = new OneAxis;
  axes_[PlotUtil::EAST] = new OneAxis;
  axes_[PlotUtil::WEST] = new OneAxis;

  int i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      grab_axis(axes_[i]);

      Axis* a = axes_[i]->checkout_axis();  
      
      if (a != 0)
        {
          a->set_pos(static_cast<PlotUtil::PositionType>(i));
          a->set_hidden(true); // hidden until it's used.
          
          axes_[i]->checkin_axis(a);
        }
      else 
        g_warning("Couldn't check out axis to modify it!");

      ++i;
    }
}

XyAxes::~XyAxes()
{
  int i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      release_an_axis(&axes_[i]);

      ++i;
    }
}

void 
XyAxes::change_axis(OneAxis* oa, const Axis& axis)
{
  PlotUtil::PositionType pos = axis.pos();

  g_assert(oa == axes_[pos]);

  axes_model.axis_changed(this, pos, axis);
}

void 
XyAxes::destroy_model()
{
  // Nothing
}

void 
XyAxes::push_axis_bounds(vector<PlotUtil::PositionType>& axes,
                         vector<double>& starts,
                         vector<double>& stops)
{
  g_return_if_fail(axes.size() == starts.size());
  g_return_if_fail(axes.size() == stops.size());
  g_return_if_fail(!axes.empty());

  AxisBounds new_ab;
  AxisBounds old_ab;

  guint i = 0;

  while (i < axes.size())
    {
      PlotUtil::PositionType pos = axes[i];

      g_return_if_fail(pos < PlotUtil::PositionTypeEnd);

      old_ab.starts[pos] = axes_[pos]->axis().start();
      old_ab.stops[pos] = axes_[pos]->axis().stop();

      new_ab.starts[pos] = starts[i];
      new_ab.stops[pos] = stops[i];

      old_ab.set_axes.push_back(pos);
      new_ab.set_axes.push_back(pos);

      ++i;
    }

  set_axis_bounds(new_ab);

  axis_bounds_stack_.push_back(old_ab);
}

guint 
XyAxes::axis_bounds_stack_size() const
{
  return axis_bounds_stack_.size();
}

void 
XyAxes::pop_axis_bounds()
{
  if (axis_bounds_stack_.size() == 0)
    {
#ifdef GNOME_ENABLE_DEBUG
      g_warning("Attempt to pop from empty axis bounds stack!");
#endif
      return;
    }
  else 
    {
      set_axis_bounds(axis_bounds_stack_.back());
      axis_bounds_stack_.pop_back();
    }
}

Axis* 
XyAxes::checkout_axis(PlotUtil::PositionType pos)
{
  return axes_[pos]->checkout_axis();
}

void 
XyAxes::checkin_axis(PlotUtil::PositionType pos, Axis* a)
{
  axes_[pos]->checkin_axis(a);
}

const Axis& 
XyAxes::axis(PlotUtil::PositionType pos) const
{
  return axes_[pos]->axis();
}

void 
XyAxes::recalc_ticks(guint nxticks, guint nyticks)
{
  axes_[PlotUtil::NORTH]->recalc_ticks(nxticks);
  axes_[PlotUtil::SOUTH]->recalc_ticks(nxticks);
  axes_[PlotUtil::EAST]->recalc_ticks(nyticks);
  axes_[PlotUtil::WEST]->recalc_ticks(nyticks);
}

void 
XyAxes::share_with(PlotUtil::PositionType pos, XyAxes* partner)
{
  partner->set_one(pos, axes_[pos]);
}

void 
XyAxes::hoard(PlotUtil::PositionType pos)
{
  // Could optimize by not doing this if the refcount on our 
  //  axis is only 1

  OneAxis* mine = axes_[pos]->clone();

  set_one(pos, mine);
}

OneAxis* 
XyAxes::one(PlotUtil::PositionType pos)
{
  return axes_[pos];
}

void 
XyAxes::set_one(PlotUtil::PositionType pos, OneAxis* oa)
{
  g_assert(pos == oa->axis().pos());

  if (oa == axes_[pos])
    return;

  grab_axis(oa);

  release_an_axis(&axes_[pos]);

  axes_[pos] = oa;

  axes_model.axis_changed(this, pos, axes_[pos]->axis());
}

void 
XyAxes::set_axis_bounds(const AxisBounds& ab)
{
  vector<PlotUtil::PositionType>::const_iterator i = ab.set_axes.begin();
  
  while (i != ab.set_axes.end())
    {
      if (axes_[*i]->axis().start() != ab.starts[*i] ||
          axes_[*i]->axis().stop() != ab.stops[*i])
        {
          Axis* a = axes_[*i]->checkout_axis();  
          
          if (a != 0)
            {
              a->set_start(ab.starts[*i]);
              a->set_stop(ab.stops[*i]);

              axes_[*i]->checkin_axis(a);
            }
          else 
            g_warning("Couldn't check out axis to modify it!");
        }

      ++i;
    }
}

void 
XyAxes::grab_axis(OneAxis* oa)
{
  oa->ref();

  oa->one_model.add_view(this);
}

void 
XyAxes::release_an_axis(OneAxis** one)
{
  (*one)->one_model.remove_view(this);
  
  if ((*one)->unref() == 0)
    {
      delete *one;
    }
  
  *one = 0;
}

XyAxes::AxisBounds::AxisBounds()
{
  int i = 0;
  while (i < PlotUtil::PositionTypeEnd)
    {
      starts[i] = 0.0;
      stops[i] = 0.0;
      ++i;
    }
}

///////////////////////////////////////


XyPlotState::XyPlotState(DataStore* ds)
  : store_(ds), axes_(0), 
    background_(255, 255, 255), 
    width_(100.0), height_(100.0), 
    need_recalc_layout_(true),
    set_name_from_data_(true)
{
  g_return_if_fail(store_ != 0);

  axes_ = new XyAxes;

  grab_xyaxes(axes_);

  // Get some sane values in the rectangles
  recalc_layout();
}

XyPlotState::~XyPlotState()
{
  release_xyaxes();
}
    
DataStore* 
XyPlotState::store()
{
  return store_;
}

XyPlotState::iterator 
XyPlotState::find(XyLayer* layer)
{
  iterator i = begin();
  iterator e = end();

  while (i != e)
    {
      if (*i == layer)
        break;

      ++i;
    }

  return i;
}

XyPlotState::const_iterator 
XyPlotState::find(XyLayer* layer) const
{
  const_iterator i = begin();
  const_iterator e = end();

  while (i != e)
    {
      if (*i == layer)
        break;

      ++i;
    }

  return i;
}

void 
XyPlotState::add_layer(XyLayer* layer)
{
  vector<PlotUtil::PositionType> unused_axes;
  PlotUtil::PositionType best_x = PlotUtil::PositionTypeEnd;
  PlotUtil::PositionType best_y = PlotUtil::PositionTypeEnd;

  if (find_unused_axes(unused_axes))
    {
      vector<PlotUtil::PositionType>::const_iterator i = unused_axes.begin();
      while (i != unused_axes.end())
        {
          switch (*i) {
          case PlotUtil::WEST:
            best_y = PlotUtil::WEST;
            break;
          case PlotUtil::EAST:
            if (best_y == PlotUtil::PositionTypeEnd)
              best_y = PlotUtil::EAST;
            break;
          case PlotUtil::SOUTH:
            best_x = PlotUtil::SOUTH;
            break;
          case PlotUtil::NORTH:
            if (best_x == PlotUtil::PositionTypeEnd)
              best_x = PlotUtil::NORTH;
            break;
          default:
            g_assert_not_reached();
          };
          
          ++i;
        }
    }

  // We have to do this *after* we find the unused axes (otherwise
  //  the default values for this layer would confuse things)
  layers_.push_back(layer);
  grab_layer(layer);

  // if there were no unused axes, the defaults remain - which is
  // fine really.
  if (best_x != PlotUtil::PositionTypeEnd)
    {
      if (axis(best_x).hidden())
        {
          // Our axes are hidden by default; the first time they're used,
          //  they are unhidden.
          Axis* a = checkout_axis(best_x);
          
          if (a != 0)
            {
              a->set_hidden(false);

              checkin_axis(best_x, a);
            }
        }
      layer->set_x_axis(best_x);
      optimize_layers_axis(layer, best_x);
    }
  if (best_y != PlotUtil::PositionTypeEnd)
    {
      if (axis(best_y).hidden())
        {
          // Our axes are hidden by default; the first time they're used,
          //  they are unhidden.
          Axis* a = checkout_axis(best_y);
          
          if (a != 0)
            {
              a->set_hidden(false);

              checkin_axis(best_y, a);
            }
        }
      layer->set_y_axis(best_y);
      optimize_layers_axis(layer, best_y);
    }
  
  state_model.layer_added(this, layer);
}

void 
XyPlotState::add_layer_no_auto_axis(XyLayer* layer)
{
  layers_.push_back(layer);
  grab_layer(layer);

  state_model.layer_added(this, layer);
}

void 
XyPlotState::lower_layer(iterator i)
{
  g_return_if_fail(i != layers_.end());
  g_return_if_fail(layers_.size() > 0);

  if (i == layers_.begin())
    return;

  iterator prev = i - 1;

  XyLayer* tmp = *prev;
  *prev = *i;
  *i = tmp;

  state_model.layer_lowered(this, *prev);
}

void 
XyPlotState::raise_layer(iterator i)
{
  g_return_if_fail(i != layers_.end());
  g_return_if_fail(layers_.size() > 0);

  if (i == layers_.end()-1)
    return;

  iterator next = i + 1;

  XyLayer* tmp = *next;
  *next = *i;
  *i = tmp;

  state_model.layer_raised(this, *next);
}

void 
XyPlotState::remove_layer(iterator i)
{
  g_return_if_fail(i != layers_.end());
  g_return_if_fail(layers_.size() > 0);

  XyLayer* removed = *i;

  layers_.erase(i);

  state_model.layer_removed(this, removed);

  release_layer(removed);
}

const Color & 
XyPlotState::background() const
{
  return background_;
}

void 
XyPlotState::set_background(const Color & c)
{
  background_ = c; 
  state_model.background_changed(this, background_); 
}

const string& 
XyPlotState::name() const
{
  return name_;
}

void 
XyPlotState::set_name(const string & name)
{
  name_ = name;
  state_model.name_changed(this, name);
}

bool 
XyPlotState::name_from_data() const
{
  return set_name_from_data_;
}

void 
XyPlotState::set_name_from_data(bool setting)
{
  set_name_from_data_ = setting;
  if (setting)
    update_name_from_data();
}

void 
XyPlotState::push_axis_bounds(vector<PlotUtil::PositionType>& axes,
                              vector<double>& starts,
                              vector<double>& stops)
{
  axes_->push_axis_bounds(axes, starts, stops);
}

guint 
XyPlotState::axis_bounds_stack_size() const
{
  return axes_->axis_bounds_stack_size();
}

void 
XyPlotState::pop_axis_bounds()
{
  axes_->pop_axis_bounds();
}

Axis* 
XyPlotState::checkout_axis(PlotUtil::PositionType pos)
{
  return axes_->checkout_axis(pos);
}

void 
XyPlotState::checkin_axis(PlotUtil::PositionType pos, Axis* a)
{
  axes_->checkin_axis(pos, a);
}

const Axis& 
XyPlotState::axis(PlotUtil::PositionType pos) const
{
  return axes_->axis(pos);
}

const Transform& 
XyPlotState::trans(PlotUtil::PositionType pos) const
{
  return transforms_[pos];
}

void 
XyPlotState::set_width(double w)
{
  if (w <= PlotUtil::EPSILON) 
    w = PlotUtil::EPSILON;

  width_ = w;

  queue_layout();

  state_model.size_changed(this, width_, height_);
}

void 
XyPlotState::set_height(double h)
{
  if (h <= PlotUtil::EPSILON)
    h = PlotUtil::EPSILON;

  height_ = h;

  queue_layout();

  state_model.size_changed(this, width_, height_);
}

void 
XyPlotState::size_request(double* w, double* h)
{
  g_return_if_fail(w != 0);
  g_return_if_fail(h != 0);
  // For now, always request the current size
  *w = width_;
  *h = height_;
}

const Rectangle & 
XyPlotState::plot_rect() const
{
  if (need_recalc_layout_)
    ((XyPlotState*)this)->recalc_layout();

  return plot_rect_;
}

const Rectangle & 
XyPlotState::axis_rect(PlotUtil::PositionType pos) const
{
  if (need_recalc_layout_)
    ((XyPlotState*)this)->recalc_layout();

  return axis_rects_[pos];
}

const Rectangle & 
XyPlotState::axis_label_rect(PlotUtil::PositionType pos) const
{
  if (need_recalc_layout_)
    ((XyPlotState*)this)->recalc_layout();
  
  return axis_label_rects_[pos];
}

// Return true if a sensible min/max for the given axis can be extracted
//  from this layer, and set min/max; otherwise, ignore min/max and return false.
static bool get_min_max(XyLayer* layer, PlotUtil::PositionType pos, 
                        double* min, double* max)
{
  TypeRegistry* tr = XyLayer::type_registry();
  bool retval = false;
  g_assert(tr != 0); // since a layer exists

  int type_id = layer->type();

  if (tr->is_a(type_id, XyLayer::DataLayer))
    {
      XyDataLayer* dl = (XyDataLayer*) layer->cast_to_type(XyLayer::DataLayer);

      g_assert(dl != 0);

      if (pos == layer->x_axis())
        {
          ScalarData* sd = dl->x_data();
          if (sd != 0)
            {
              *min = sd->min();
              *max = sd->max();
              retval = true;
            }
        }
      else if (pos == layer->y_axis())
        {
          ScalarData* sd = dl->y_data();
          if (sd != 0)
            {
              *min = sd->min();
              *max = sd->max();
              retval = true;
            }
        }
    }
  else if (tr->is_a(type_id, XyLayer::PriceLayer))
    {
      XyPriceBars* pl = 
        (XyPriceBars*) layer->cast_to_type(XyLayer::PriceLayer);

      g_assert(pl != 0);

      if (pos == layer->x_axis())
        {
          DataGroup* dg = pl->data();
          Data* d = dg ? dg->get_data(PriceDates) : 0;
          if (d != 0)
            {
              if (d->is_a(Data::Scalar))
                {
                  ScalarData* sd = d->cast_to_scalar();

                  *min = sd->min();
                  *max = sd->max();
                  retval = true;
                }
              else if (d->is_a(Data::Date))
                {
                  DateData* dd = d->cast_to_date();

                  *min = (double)dd->min_julian();
                  *max = (double)dd->max_julian();
                  retval = true;
                }
            }
          else
            {
              // If there's no time data, just use sequential numbers.
              guint nbars = pl->nbars();
              if (nbars > 0)
                {
                  *min = 0;
                  *max = nbars;
                  retval = true;
                }
            }
        }
      else if (pos == layer->y_axis())
        {
          DataGroup* dg = pl->data();
          Data* hi_d = dg ? dg->get_data(PriceHighs) : 0;
          Data* lo_d = dg ? dg->get_data(PriceLows) : 0;
          if (hi_d && lo_d)
            {
              ScalarData* hi_sd = hi_d->cast_to_scalar();
              ScalarData* lo_sd = lo_d->cast_to_scalar();

              g_assert(hi_sd && lo_sd);

              *min = lo_sd->min();
              *max = hi_sd->max();
              retval = true;
            }
        }
    }

  // eventually we might have a way to get min/max from relation layers


  if (retval)
    {
      // make sure the axis isn't microscopic
      if ((*max - *min) < PlotUtil::EPSILON)
        {
          *max += 1.0;
          *min -= 1.0;
        }
    }

  return retval;
}

static void 
set_axis_range(XyAxes* axes, PlotUtil::PositionType pos, double min, double max)
{
  Axis* a = axes->checkout_axis(pos);
  
  if (a != 0)
    {
      a->set_start(min);
      a->set_stop(max);
      
      axes->checkin_axis(pos, a);
    }
  else
    g_warning("Axis checkout failed in %s", __FUNCTION__);
}

void 
XyPlotState::optimize_axis(PlotUtil::PositionType pos)
{
  double min = 0.0;
  double max = 0.0;
  bool first_found = false; // have we hit the first layer with min/max

  iterator i = begin();
  
  while (i != end())
    {
      double tmpmin = 0.0;
      double tmpmax = 0.0;

      if (get_min_max(*i, pos, &tmpmin, &tmpmax))
        {
          if (first_found)
            {
              min = MIN(min, tmpmin);
              max = MAX(max, tmpmax);
            }
          else
            {
              min = tmpmin;
              max = tmpmax;
            }

          first_found = true;          
        }

      ++i;
    }

  set_axis_range(axes_, pos, min, max);
}

void 
XyPlotState::optimize_all_axes()
{
  optimize_axis(PlotUtil::EAST);
  optimize_axis(PlotUtil::WEST);
  optimize_axis(PlotUtil::NORTH);
  optimize_axis(PlotUtil::SOUTH);
}

void 
XyPlotState::optimize_layers_axis(XyLayer* layer, PlotUtil::PositionType pos)
{
#ifdef GNOME_ENABLE_DEBUG
  if (PlotUtil::horizontal(pos))
    g_return_if_fail(layer->x_axis() == pos);
  else 
    g_return_if_fail(layer->y_axis() == pos);
#endif

  double min;
  double max;

  if (get_min_max(layer, pos, &min, &max))
    {
      set_axis_range(axes_, pos, min, max);
    }
}

void 
XyPlotState::optimize_layers_axes(XyLayer* layer)
{
  g_return_if_fail(layer != 0);
  
  PlotUtil::PositionType xaxis = layer->x_axis();
  PlotUtil::PositionType yaxis = layer->y_axis();
  double min;
  double max;
  
  if (get_min_max(layer, xaxis, &min, &max))
    {
      set_axis_range(axes_, xaxis, min, max);
    }

  if (get_min_max(layer, yaxis, &min, &max))
    {
      set_axis_range(axes_, yaxis, min, max);
    }
}

bool
XyPlotState::find_unused_axes(vector<PlotUtil::PositionType>& fillme)
{

  bool used[PlotUtil::PositionTypeEnd] = { 
    false, false, false, false 
  };

  // Scan for unused axes
  iterator i = begin();
  while (i != end())
    {
      used[(*i)->x_axis()] = true;
      used[(*i)->y_axis()] = true;

      ++i;
    }


  int j = 0;
  while (j < PlotUtil::PositionTypeEnd)
    {
      if (!used[j])
        fillme.push_back((PlotUtil::PositionType)j);

      ++j;
    }

  return !fillme.empty();
}

void 
XyPlotState::share_axis_with(PlotUtil::PositionType pos, 
                             XyPlotState* partner)
{
  axes_->share_with(pos, partner->axes_);
}
  
void 
XyPlotState::hoard_axis(PlotUtil::PositionType pos)
{
  axes_->hoard(pos);
}


// XyAxes::View
void 
XyPlotState::change_axis(XyAxes* axes, 
                         PlotUtil::PositionType pos, 
                         const Axis& axis)
{
  transforms_[pos].set_range_bounds(axis.start(), axis.stop());
  
  // pointless now, but will matter later (fonts, labels change)
  queue_layout();

  state_model.axis_changed(this, pos, axis);
}

// ::View
void 
XyPlotState::destroy_model()
{
  // Nothing
}

void 
XyPlotState::update_name_from_data()
{
  if (!set_name_from_data_)
    return;

  const_iterator i = begin();

  if (i != end())
    set_name((*i)->generate_name());
  else
    set_name(_("Untitled XY Plot"));
}

static void 
get_label_size(const Axis& axis, double* size)
{
  static double default_label_size = 20.0;

  if (axis.label_hidden() || axis.hidden() || !axis.labelled())
    {
      *size = 0.0;
      return;
    }

  VFont* vfont = axis.label_font();
  if (vfont == 0)
    {
      Frontend* fe = guppi_frontend();
      if (fe != 0)
        {
          vfont = fe->default_font();
        }
    }

  // Use font height regardless of axis orientation, because
  //  east/west axes are rotated.

  if (vfont != 0)
    *size = vfont->font_height();
  else
    *size = default_label_size;  

  *size += 5.0; //padding
}

void 
XyPlotState::queue_layout()
{
  need_recalc_layout_ = true;

  state_model.size_changed(this, width_, height_);
}

void 
XyPlotState::recalc_layout()
{
  // FIXME we need to do a better job if we don't get enough space.

  static double default_axis_size = 20.0;

  double label_sizes[PlotUtil::PositionTypeEnd];
  double axis_sizes[PlotUtil::PositionTypeEnd];

  int i = 0;
  
  while (i < PlotUtil::PositionTypeEnd)
    {
      get_label_size(axes_->axis((PlotUtil::PositionType)i), &label_sizes[i]);

      // FIXME this is kind of lame, obviously.
      if (axes_->axis((PlotUtil::PositionType)i).hidden())
        axis_sizes[i] = 0.0;
      else
        axis_sizes[i] = default_axis_size;

      ++i;
    }

  double plot_width = width_ 
    - label_sizes[PlotUtil::WEST] - label_sizes[PlotUtil::EAST]
    - axis_sizes[PlotUtil::WEST] - axis_sizes[PlotUtil::EAST];
    

  if (plot_width < 0.0)
    plot_width = 0.0;

  double plot_height = height_ 
    - label_sizes[PlotUtil::NORTH] - label_sizes[PlotUtil::SOUTH] 
    - axis_sizes[PlotUtil::NORTH] - axis_sizes[PlotUtil::SOUTH];

  if (plot_height < 0.0)
    plot_height = 0.0;

  axis_label_rects_[PlotUtil::NORTH].set(label_sizes[PlotUtil::WEST] + 
                                         axis_sizes[PlotUtil::WEST], 
                                         0.0,
                                         plot_width,
                                         label_sizes[PlotUtil::NORTH]);

  axis_rects_[PlotUtil::NORTH].set(axis_label_rects_[PlotUtil::NORTH].x(),
                                   axis_label_rects_[PlotUtil::NORTH].y() + 
                                   axis_label_rects_[PlotUtil::NORTH].height(),
                                   plot_width,
                                   axis_sizes[PlotUtil::NORTH]);

  axis_rects_[PlotUtil::SOUTH].set(axis_label_rects_[PlotUtil::NORTH].x(), 
                                   axis_rects_[PlotUtil::NORTH].y() + 
                                   axis_rects_[PlotUtil::NORTH].height() + plot_height,
                                   plot_width,
                                   axis_sizes[PlotUtil::SOUTH]);

  axis_label_rects_[PlotUtil::SOUTH].set(axis_label_rects_[PlotUtil::NORTH].x(),
                                         axis_rects_[PlotUtil::SOUTH].y() + 
                                         axis_rects_[PlotUtil::SOUTH].height(),
                                         plot_width,
                                         label_sizes[PlotUtil::SOUTH]);

  axis_label_rects_[PlotUtil::WEST].set(0.0,
                                        axis_rects_[PlotUtil::NORTH].y() + 
                                        axis_rects_[PlotUtil::NORTH].height(),
                                        label_sizes[PlotUtil::WEST],
                                        plot_height);

  axis_rects_[PlotUtil::WEST].set(axis_label_rects_[PlotUtil::WEST].x() + 
                                  axis_label_rects_[PlotUtil::WEST].width(),
                                  axis_label_rects_[PlotUtil::WEST].y(),
                                  axis_sizes[PlotUtil::WEST],
                                  plot_height);

  axis_rects_[PlotUtil::EAST].set(axis_rects_[PlotUtil::WEST].x() + 
                                  axis_rects_[PlotUtil::WEST].width() + plot_width,
                                  axis_label_rects_[PlotUtil::WEST].y(),
                                  axis_sizes[PlotUtil::EAST],
                                  plot_height);
  
  axis_label_rects_[PlotUtil::EAST].set(axis_rects_[PlotUtil::EAST].x() + 
                                        axis_rects_[PlotUtil::EAST].width(),
                                        axis_label_rects_[PlotUtil::EAST].y(),
                                        label_sizes[PlotUtil::EAST],
                                        plot_height);

  plot_rect_.set(axis_rects_[PlotUtil::WEST].x() + axis_rects_[PlotUtil::WEST].width(),
                 axis_rects_[PlotUtil::NORTH].y() + axis_rects_[PlotUtil::NORTH].height(),
                 plot_width,
                 plot_height);
                                                    
  // NOTE Y is backward due to X Windows coordinate system

  transforms_[PlotUtil::EAST].set_screen_bounds(plot_rect_.y() + plot_rect_.height(), plot_rect_.y());
  transforms_[PlotUtil::WEST].set_screen_bounds(plot_rect_.y() + plot_rect_.height(), plot_rect_.y());

  transforms_[PlotUtil::NORTH].set_screen_bounds(plot_rect_.x(), plot_rect_.x() + plot_rect_.width());
  transforms_[PlotUtil::SOUTH].set_screen_bounds(plot_rect_.x(), plot_rect_.x() + plot_rect_.width());

  // We don't want to get into infinite recursion when we're notified 
  //  of the change in ticks, so XYAxes doesn't emit an axes changed
  //  in response to this.
  guint xticks = static_cast<guint>(rint(plot_width / 60.0));
  guint yticks = static_cast<guint>(rint(plot_height / 50.0));
  axes_->recalc_ticks(MAX(xticks, 1), MAX(yticks, 1)); // at least one tick

  need_recalc_layout_ = false;
}

void 
XyPlotState::grab_xyaxes(XyAxes* axes)
{
  if (axes != 0)
    {
      axes->ref();
      
      axes->axes_model.add_view(this);
    }
}

void 
XyPlotState::release_xyaxes()
{
  if (axes_ != 0)
    {
      axes_->axes_model.remove_view(this);
      if (axes_->unref() == 0)
        {
          delete axes_;
        }
      axes_ = 0;
    }
}

void 
XyPlotState::grab_layer(XyLayer* layer)
{
  if (layer != 0)
    {
      layer->ref();
#if 0
      layer->layer_model.add_view(this);

      if (layer->type() == XyLayer::ScatterLayer)
        {
          XyScatter* ss = (XyScatter*)layer->cast_to_type(XyLayer::ScatterLayer);
          ss->xy_scatter_model.add_view(this);
          ss->data_layer_model.add_view(this);
        }
      else if (layer->type() == XyLayer::LineLayer)
        {
          XyLine* sl = (XyLine*)layer->cast_to_type(XyLayer::LineLayer);
          sl->data_layer_model.add_view(this);
        }
      else if (layer->type() == XyLayer::RelationLayer)
        {
          XyRelation* sr = (XyRelation*)layer->cast_to_type(XyLayer::RelationLayer);
          sr->xy_relation_model.add_view(this);
        }
#endif
    }
}

void 
XyPlotState::release_layer(XyLayer* layer)
{
  if (layer != 0)
    {
#if 0
      layer->layer_model.remove_view(this);

      if (layer->type() == XyLayer::ScatterLayer)
        {
          XyScatter* ss = (XyScatter*)layer->cast_to_type(XyLayer::ScatterLayer);
          ss->xy_scatter_model.remove_view(this);
          ss->data_layer_model.remove_view(this);
        }
      else if (layer->type() == XyLayer::LineLayer)
        {
          XyLine* sl = (XyLine*)layer->cast_to_type(XyLayer::LineLayer);
          sl->data_layer_model.remove_view(this);
        }
      else if (layer->type() == XyLayer::RelationLayer)
        {
          XyRelation* sr = (XyRelation*)layer->cast_to_type(XyLayer::RelationLayer);
          sr->xy_relation_model.remove_view(this);
        }
#endif 
      if (layer->unref() == 0)
        delete layer;
    }
}



