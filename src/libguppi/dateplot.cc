// -*- C++ -*-

/* 
 * dateplot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "dateplot.h"

DatePlot::DatePlot(DataStore* ds)
  : Plot(ds), canvas_(0)
{
  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  canvas_ = gnome_canvas_new ();
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  drawable_ = 
    GNOME_CANVAS_DRAWABLE(gnome_canvas_item_new(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas_)->root),
                                                gnome_canvas_drawable_get_type(),
                                                "x", 0.0,
                                                "y", 0.0,
                                                "anchor", GTK_ANCHOR_NE,
                                                "width", 100.0,
                                                "height", 100.0,
                                                NULL));

  gnome_canvas_item_new(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas_)->root),
                        gnome_canvas_rect_get_type(),
                        "x1", 10.0, "y1", 10.0, 
                        "x2", 110.0, "y2", 110.0, 
                        "outline_color", "red",
                        NULL);
}

DatePlot::~DatePlot()
{

}

GtkWidget* 
DatePlot::widget()
{
  return canvas_;
}


