// -*- C++ -*-

/* 
 * pieplotedit.cc
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "pieplotedit.h"
#include "scalardata.h"

PiePlotEdit::PiePlotEdit() : dialog_(0), state_(0), changing_data_(false) {
	// We never disconnect since the DataOption dies with us.
	data_option_.connect(data_chosen_cb, this);
}

PiePlotEdit::~PiePlotEdit() {
	release_state();
}

void PiePlotEdit::edit() {
	if (dialog_ != 0) return;

	dialog_ = gnome_dialog_new(_("Pie Chart Plot Editor"),
		GNOME_STOCK_BUTTON_CLOSE, NULL);

	gnome_dialog_set_close(GNOME_DIALOG(dialog_), TRUE);

	gtk_window_set_policy(GTK_WINDOW(dialog_), TRUE, TRUE, FALSE);

	gtk_signal_connect(GTK_OBJECT(dialog_), "close",
		GTK_SIGNAL_FUNC(close_cb), this);

	GtkWidget* label = gtk_label_new(_("Data: "));
  
	GtkWidget* hbox = gtk_hbox_new(GNOME_PAD, FALSE);
  
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_end(GTK_BOX(hbox), data_option_.widget(), TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox), 
		hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	gtk_widget_show_all(dialog_);
}

void PiePlotEdit::change_slices(PiePlotState* state) {
	if (dialog_ == 0) return;
  
	g_warning(__FUNCTION__);
}

void PiePlotEdit::change_data(PiePlotState* state, Data* data) {
	if (changing_data_) return;

	data_option_.set_data(data);
}

void PiePlotEdit::change_background(PiePlotState* state, const Color & bg)
{
	// FIXME
}

void PiePlotEdit::change_size(PiePlotState* state, double width, double height)
{
	// FIXME
}

void PiePlotEdit::change_name(PiePlotState* state, const string& name)
{
	// FIXME
}

void PiePlotEdit::destroy_model() {
	if (dialog_ != 0) {
		close();
	}
	state_ = 0;
}

void PiePlotEdit::set_state(PiePlotState* state) {
	release_state();

	state_ = state;

	if (state_ != 0) {
	// We don't ref/unref the state; the PiePlot keeps up with that.
		state_->state_model.add_view(this);
		data_option_.set_store(state_->store());
	}
}

void PiePlotEdit::release_state() {
	if (state_ != 0) {
		state_->state_model.remove_view(this);
		data_option_.set_store(0);
		state_ = 0;
	}
}

void PiePlotEdit::data_chosen_cb(gpointer emitter, gpointer data) {
	PiePlotEdit* bpe = static_cast<PiePlotEdit*>(data);

	bpe->data_chosen();
}

void PiePlotEdit::data_chosen() {
	g_return_if_fail(state_ != 0);
	changing_data_ = true;
	state_->set_data(data_option_.get_data()); 
	changing_data_ = false;
}

gint PiePlotEdit::close_cb(GtkWidget* w, gpointer data) {
	PiePlotEdit* bpe = static_cast<PiePlotEdit*>(data);
  
	return bpe->close();
}

gint PiePlotEdit::close() {
	gtk_widget_destroy(dialog_);
	dialog_ = 0;

	return TRUE;
}
