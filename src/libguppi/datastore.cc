// -*- C++ -*-

/* 
 * datastore.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "datastore.h"
#include "xmltags.h"

DataStore::DataStore()
{


}

DataStore::~DataStore()
{
  erase(begin(), end());
}

void 
DataStore::insert(Data* d)
{
  data_.push_back(d);
  grab_data(d);
  store_model.data_added(this, d);
}


DataStore::const_iterator 
DataStore::begin() const
{
  return data_.begin();
}

DataStore::const_iterator 
DataStore::end() const
{
  return data_.end();
}

DataStore::iterator 
DataStore::begin()
{
  return data_.begin();
}

DataStore::iterator 
DataStore::end()
{
  return data_.end();
}

DataStore::const_iterator 
DataStore::find(Data* d) const
{
  const_iterator i = begin();
  while (i != end())
    {
      if (*i == d) return i;
      ++i;
    }
  return end();
}

DataStore::iterator 
DataStore::find(Data* d)
{
  iterator i = begin();
  while (i != end())
    {
      if (*i == d) return i;
      ++i;
    }
  return end();
}

void 
DataStore::erase(iterator i)
{
  Data* d = *i;
  data_.erase(i); // must erase before release

  release_data(d);
}

void 
DataStore::erase(iterator b, iterator e)
{
  vector<Data*> to_release;

  iterator i = b;
  while (i != e)
    {
      to_release.push_back(*i);

      ++i;
    }

  data_.erase(b, e);  // must erase before release

  vector<Data*>::iterator j = to_release.begin();
  while (j != to_release.end())
    {
      release_data(*j);
      
      ++j;
    }
}

void 
DataStore::grab_data(Data* d)
{
  g_return_if_fail(d != 0);
  
  d->data_model.add_view(this);

  d->ref();
}

void 
DataStore::release_data(Data* d)
{
  g_return_if_fail(d != 0);

  d->data_model.remove_view(this);

  store_model.data_removed(this, d);

  if (d->unref() == 0) 
    delete d;
}


xmlNodePtr 
DataStore::xml(xmlNodePtr parent) const
{
  xmlNodePtr store = 0;
  xmlNodePtr data = 0;

  store = xmlNewChild(parent, NULL, DATA_STORE_TAG, NULL);
  
  const_iterator i = begin();
  const_iterator e = end();
  while (i != e)
    {
      data = (*i)->xml(store);

      ++i;
    }

  return store;
}

#include "scalardata.h"

void 
DataStore::set_xml(xmlNodePtr node)
{
  erase(begin(),end());
  merge_xml(node);
}

void 
DataStore::merge_xml(xmlNodePtr node)
{
  g_return_if_fail(node != 0);
  
  if (node->name == 0 || (g_ustrcmp(node->name, DATA_STORE_TAG) != 0))
    {
      g_warning("Bad node to DataStore::merge_xml()");
      return;
    }

  xmlNodePtr child = node->childs;
  while (child != 0)
    {
      if (child->name != 0)
        {
          if (g_ustrcmp(child->name, SCALAR_DATA_TAG) == 0)
            {
              ScalarData* sd = new ScalarData;
              sd->set_xml(child);
              insert(sd);
            }
        }
      
      child = child->next;
    }
}
