// -*- C++ -*-

/* 
 * canvas-lineplot.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_CANVASLINEPLOT_H
#define GUPPI_CANVASLINEPLOT_H

#include <libgnomeui/gnome-canvas.h>

#include "plotutils.h"
#include "xyplotstate.h"

// there is no reason to maintain strict C in here, but keep C++-isms
//  to a minimum just in case.

#define GUPPI_TYPE_LINEPLOT            (guppi_lineplot_get_type ())
#define GUPPI_LINEPLOT(obj)            (GTK_CHECK_CAST ((obj), GUPPI_TYPE_LINEPLOT, GuppiLineplot))
#define GUPPI_LINEPLOT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GUPPI_TYPE_LINEPLOT, GuppiLineplotClass))
#define GUPPI_IS_LINEPLOT(obj)         (GTK_CHECK_TYPE ((obj), GUPPI_TYPE_LINEPLOT))
#define GUPPI_IS_LINEPLOT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GUPPI_TYPE_LINEPLOT))

struct _GuppiLineplot;

// This is a callback used to retrieve the info for various operations.
// It used to retrieve more than the LineplotPlotState, 
// that's why it's a callback. Pointless now.
typedef void (*LineplotInfoFunc)(struct _GuppiLineplot* gs, 
                                 gpointer user_data, 
                                 XyPlotState** state,
                                 XyLine** layer);

class PointTiles;

typedef struct _GuppiLineplot GuppiLineplot;
typedef struct _GuppiLineplotClass GuppiLineplotClass;

struct _GuppiLineplot {
  GnomeCanvasItem item;
  
  LineplotInfoFunc get_info;
  gpointer info_data;

  // Since we compute the rectangle to render in the render function,
  //  we have to store this in update().
  double a[6];
};

struct _GuppiLineplotClass {
  GnomeCanvasItemClass parent_class;
  
  
};


/* Standard Gtk function */
GtkType guppi_lineplot_get_type (void);

void guppi_lineplot_set_info_func(GuppiLineplot* gs, 
                                  LineplotInfoFunc func,
                                  gpointer user_data);


#endif
