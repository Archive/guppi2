// -*- C++ -*-

/* 
 * dataimport.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "util.h"
#include <stdlib.h>
#include <string.h>
#include <string>
#include <vector>
#include <stdio.h>

#include <errno.h>

#include "dataimport.h"

//// A key attribute of all these functions is that they MUST NOT
//// CRASH, even if they get fed binary garbage.

static inline void
eat_whitespace(char** scan)
{
  while (**scan && isspace(**scan))
    ++(*scan);
}

static inline bool 
parse_double(char* scan, double* fillme)
{
  // Makes us a little more tolerant...
  eat_whitespace(&scan);

  char* endptr;
  
  *fillme = g_strtod(scan, &endptr);
  
  if (endptr != scan)
    {
      return true;
    }
  else 
    {
      return false;
    }
}

static void
parse_number_line(char* line, 
                  const char* sep, 
                  guint sep_len,
                  vector<double>& fillme)
{
  char* scan;

  scan = line;

  eat_whitespace(&scan);

  while (*scan)
    {
      char* token_end = strstr(scan, sep);

      if (token_end == 0)
        {
          double val;
          // No more separators; try to parse remaining stuff
          if (parse_double(scan, &val))
            {
              fillme.push_back(val);
            }
     
          // All done
          break;
        }
      else if (scan != token_end)
        {
          *token_end = '\0';
          
          double val;
          if (parse_double(scan, &val))
            {
              fillme.push_back(val);
            }
        }

      scan = token_end + sep_len;
    }
}

// Really brain-damaged for now
static string
guess_separator(char* line)
{
  eat_whitespace(&line);

  // Pick first whitespace field
  char* scan = line;
  while (*scan && !isspace(*scan))
    ++scan;
  
  if (*scan)
    {
      string whitespace;

      while (isspace(*scan))
        {
          whitespace += *scan;
          ++scan;
        }

      g_assert(!whitespace.empty());
        
      return whitespace;
    }

  // Guess some other random crap

  if (strchr(line, '/'))
    return string("/");
  else if (strchr(line, '|'))
    return string("|");
  else if (strchr(line, ',')) // last, since it might be inside a number too
    return string(",");
  else return " "; // why not
}

static bool 
looks_like_binary(const char* line)
{
  while (*line && isprint(*line))
    ++line;

  if (*line)
    return false;
  else 
    return true;
}

void
guppi_import_doubles(const gchar* filename,
                     vector<vector<double> >& columns,
                     WarningController& wc,
                     vector<string>& errors,
                     GooseProgress* progress)
{
  FILE* f = fopen(filename, "r");

  if (f == 0)
    {
      char* tmp = g_strdup_printf(_("Failed to open file %s: %s"),
                                  filename,
                                  strerror(errno));
      errors.push_back(tmp);
      g_free(tmp);
      return;
    }

  // Get first line to guess separator

  char buf[2048];
  string sep;

  buf[0] = '\0'; // paranoia

  if (fgets(buf, 2048, f))
    {
      if (looks_like_binary(buf))
        {
          char* tmp = g_strdup_printf(_("File %s looks like a binary file."),
                                      filename);
          errors.push_back(tmp);
          g_free(tmp);
          fclose(f);
          return;
        }

      sep = guess_separator(buf);
    }
  else
    {
      if (ferror(f))
        {
          char* tmp = g_strdup_printf(_("Failed to read file %s: %s"),
                                      filename,
                                      strerror(errno));
          errors.push_back(tmp);
          g_free(tmp);
          fclose(f);
          return;
        }
      else
        {
          char* tmp = g_strdup_printf(_("No data in file %s"),
                                      filename);
          errors.push_back(tmp);
          g_free(tmp);
          fclose(f);
          return;
        }
      g_assert_not_reached();
    }

  guint count = 0;
  guint n_vals = 0;
  do {
    ++count;
    
    vector<double> vals;

    parse_number_line(buf,
                      sep.c_str(),
                      sep.size(),
                      vals);
    if (vals.empty())
      {
        char* tmp = g_strdup_printf(_("No data on line %u"),
                                    count);
        wc.warn(tmp);
        g_free(tmp);
      }
    else
      n_vals += vals.size();

    while (columns.size() < vals.size())
      columns.push_back(vector<double>(0));

    guint ci = 0;
    while (ci < vals.size())
      {
        columns[ci].push_back(vals[ci]);
        ++ci;
      }

    if (count % 1000 == 0)
      {
        if (progress != 0)
          {
            progress->pulse();
            if (progress->cancelled())
              break;
          }
      }

  } while (fgets(buf, 2048, f));

  if (ferror(f))
    {
      char* tmp = g_strdup_printf(_("Failed to read file %s: %s"),
                                  filename,
                                  strerror(errno));
      errors.push_back(tmp);
      g_free(tmp);
      fclose(f);
      return;
    }

  fclose(f);

  if (progress)
    progress->stop();
}




