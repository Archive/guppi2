/* gnome-action-manager.c
 * Copyright (C) 1999 Havoc Pennington
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>
#include "gnome-action-manager.h"

/* Note that GroupInfo and ActionInfo share the first few members. */

struct _GnomeGroupInfo;
typedef struct _GnomeGroupInfo GnomeGroupInfo;

struct _GnomeGroupInfo {
  gchar* id;
  gboolean is_group;
  gchar* user_visible_name;
  gchar* editor_name;
  gchar* small_icon;
  gchar* stock_icon;
};

typedef struct _GnomeActionInfo GnomeActionInfo;

struct _GnomeActionInfo {
  gchar* id;
  gboolean is_group;
  GnomeActionFunc callback;
  gpointer user_data;
  GtkDestroyNotify destroy_user_data;
  gchar* user_visible_name;
  gchar* editor_name;
  gchar* tooltip;
  gchar* small_icon;
  gchar* large_icon;
  gchar* stock_icon;
};

typedef struct _GnomeActionManagerPrivate GnomeActionManagerPrivate;

struct _GnomeActionManagerPrivate {
  GHashTable* action_hash;
  GHashTable* group_hash;
  GHashTable* model_hash;
  GHashTable* view_hash;
};

#define GAM_ACTION_HASH(gam) (gam->priv->action_hash)
#define GAM_VIEW_HASH(gam) (gam->priv->view_hash)

enum {
  ACTION_INVOKED,
  ACTION_CREATED,
  ACTION_DESTROYED,
  GROUP_CREATED,
  GROUP_DESTROYED,
  ACTION_ADDED,
  ACTION_REMOVED,
  ACTION_SET_SENSITIVE,
  ACTION_TOGGLED,
  ACTION_NAME_CHANGED,
  LAST_SIGNAL
};

static void gnome_action_manager_init		(GnomeActionManager		 *am);
static void gnome_action_manager_class_init	(GnomeActionManagerClass	 *klass);
static void gnome_action_manager_finalize       (GtkObject* obj);

static void gnome_action_manager_real_action_invoked	(GnomeActionManager		 *am, const gchar* action_id, gpointer model, gpointer view);
static void gnome_action_manager_real_action_created	(GnomeActionManager		 *am, const gchar* action_id);
static void gnome_action_manager_real_action_destroyed	(GnomeActionManager		 *am, const gchar* action_id);
static void gnome_action_manager_real_group_created	(GnomeActionManager		 *am, const gchar* group_id);
static void gnome_action_manager_real_group_destroyed	(GnomeActionManager		 *am, const gchar* group_id);
static void gnome_action_manager_real_action_added	(GnomeActionManager		 *am, const gchar* group_id, const gchar* action_id);
static void gnome_action_manager_real_action_removed	(GnomeActionManager		 *am, const gchar* group_id, const gchar* action_id);
static void gnome_action_manager_real_action_set_sensitive    (GnomeActionManager* am, const gchar* action_id, gboolean sensitivity,  gpointer model, gpointer view);
static void gnome_action_manager_real_action_toggled    (GnomeActionManager* am, const gchar* action_id, gboolean active, gpointer model, gpointer view);


static GnomeActionInfo* lookup_action_info(GnomeActionManager* am, const gchar* action_id);

static GnomeActionInfo* must_lookup_action_info(GnomeActionManager* am, const gchar* action_id);


static GnomeGroupInfo* lookup_group_info(GnomeActionManager* am, const gchar* group_id);

static GnomeGroupInfo* must_lookup_group_info(GnomeActionManager* am, const gchar* group_id);


typedef void (*GtkSignal_NONE__POINTER_BOOL_POINTER_POINTER) (GtkObject * object,
                                                              gpointer arg1,
                                                              gboolean arg2,
                                                              gpointer arg3,
                                                              gpointer arg4,
                                                              gpointer user_data);

void 
gtk_marshal_NONE__POINTER_BOOL_POINTER_POINTER (GtkObject * object,
                                                GtkSignalFunc func,
                                                gpointer func_data,
                                                GtkArg * args)
{
  GtkSignal_NONE__POINTER_BOOL_POINTER_POINTER rfunc;
  rfunc = (GtkSignal_NONE__POINTER_BOOL_POINTER_POINTER) func;
  (*rfunc) (object,
	    GTK_VALUE_POINTER (args[0]),
	    GTK_VALUE_BOOL (args[1]),
	    GTK_VALUE_POINTER (args[2]),
	    GTK_VALUE_POINTER (args[3]),
	    func_data);
}

/************ Model, View forward declarations ******************/

struct _ModelInfo;
typedef struct _ModelInfo ModelInfo;


struct _ViewInfo;
typedef struct _ViewInfo ViewInfo;


/************ GroupInfo declarations *********************/


GnomeGroupInfo* group_info_new(GnomeActionManager* am, const gchar* id);
GnomeGroupInfo* group_info_new_subtree(GnomeActionManager* am, const gchar* id);
void            group_info_ref(GnomeGroupInfo* gi);
void            group_info_unref(GnomeGroupInfo* gi);
void            group_info_append(GnomeGroupInfo* gi, GnomeActionInfo* ai);
void            group_info_remove(GnomeGroupInfo* gi, GnomeActionInfo* ai);

void            group_info_append_group(GnomeGroupInfo* gi, GnomeGroupInfo* sub_gi);
void            group_info_remove_group(GnomeGroupInfo* gi, GnomeGroupInfo* sub_gi);
void            group_info_add_parent(GnomeGroupInfo* sub_gi, GnomeGroupInfo* gi);
void            group_info_remove_parent(GnomeGroupInfo* sub_gi, GnomeGroupInfo* gi);
gboolean        group_info_is_subtree(GnomeGroupInfo* gi);

typedef  void (*GroupInfoForeachFunc) (GnomeGroupInfo* child_as_group,
                                       GnomeActionInfo* child_as_action,
                                       gpointer data);

void            group_info_foreach(GnomeGroupInfo* gi, GroupInfoForeachFunc func, gpointer user_data);

GnomeActionManager* group_info_get_am(GnomeGroupInfo* gi);
void            group_info_set_name(GnomeGroupInfo* gi, const gchar* name);

/************ ActionInfo forward declarations ******************/

GnomeActionInfo* action_info_new(GnomeActionManager* am, const gchar* id);
void             action_info_ref(GnomeActionInfo* ai);
void             action_info_unref(GnomeActionInfo* ai);
void             action_info_set_callback(GnomeActionInfo* ai, GnomeActionFunc callback);
void             action_info_set_user_data(GnomeActionInfo* ai, gpointer user_data);
void             action_info_add_parent(GnomeActionInfo* ai, GnomeGroupInfo* gi);
void             action_info_remove_parent(GnomeActionInfo* ai, GnomeGroupInfo* gi);
GnomeActionManager*  action_info_get_am(GnomeActionInfo* ai);
GSList* group_info_get_children(GnomeGroupInfo* gi);
void            action_info_set_name(GnomeActionInfo* ai, const gchar* name);

/************ GnomeActionManager implementation *****************/

static GtkObjectClass *parent_class = NULL;
static guint action_manager_signals[LAST_SIGNAL] = { 0 };


GtkType
gnome_action_manager_get_type (void)
{
  static GtkType action_manager_type = 0;

  if (!action_manager_type)
    {
      static const GtkTypeInfo action_manager_info =
      {
        "GnomeActionManager",
        sizeof (GnomeActionManager),
        sizeof (GnomeActionManagerClass),
        (GtkClassInitFunc) gnome_action_manager_class_init,
        (GtkObjectInitFunc) gnome_action_manager_init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      action_manager_type = gtk_type_unique (gtk_object_get_type (), &action_manager_info);
    }

  return action_manager_type;
}

static void
gnome_action_manager_class_init (GnomeActionManagerClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  parent_class = gtk_type_class (gtk_object_get_type ());

  action_manager_signals[ACTION_INVOKED] = 
    gtk_signal_new ("action_invoked",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_invoked),
                    gtk_marshal_NONE__POINTER_POINTER_POINTER,
                    GTK_TYPE_NONE, 3, GTK_TYPE_STRING, 
                    GTK_TYPE_POINTER, GTK_TYPE_POINTER);

  action_manager_signals[ACTION_CREATED] = 
    gtk_signal_new ("action_created",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_created),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_STRING);

  action_manager_signals[ACTION_DESTROYED] = 
    gtk_signal_new ("action_destroyed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_destroyed),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_STRING);

  action_manager_signals[GROUP_CREATED] = 
    gtk_signal_new ("group_created",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, group_created),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_STRING);

  action_manager_signals[GROUP_DESTROYED] = 
    gtk_signal_new ("group_destroyed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, group_destroyed),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_STRING);

  action_manager_signals[ACTION_ADDED] = 
    gtk_signal_new ("action_added",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_added),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2, GTK_TYPE_STRING, GTK_TYPE_STRING);

  action_manager_signals[ACTION_REMOVED] = 
    gtk_signal_new ("action_removed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_removed),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2, GTK_TYPE_STRING, GTK_TYPE_STRING);

  action_manager_signals[ACTION_SET_SENSITIVE] = 
    gtk_signal_new ("action_set_sensitive",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_set_sensitive),
                    gtk_marshal_NONE__POINTER_BOOL_POINTER_POINTER,
                    GTK_TYPE_NONE, 4, GTK_TYPE_STRING, GTK_TYPE_BOOL,
                    GTK_TYPE_POINTER, GTK_TYPE_POINTER);

  action_manager_signals[ACTION_TOGGLED] = 
    gtk_signal_new ("action_toggled",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_toggled),
                    gtk_marshal_NONE__POINTER_BOOL_POINTER_POINTER,
                    GTK_TYPE_NONE, 4, GTK_TYPE_STRING, GTK_TYPE_BOOL,
                    GTK_TYPE_POINTER, GTK_TYPE_POINTER);

  action_manager_signals[ACTION_NAME_CHANGED] = 
    gtk_signal_new ("action_name_changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnomeActionManagerClass, action_name_changed),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2, GTK_TYPE_STRING, GTK_TYPE_STRING);



  gtk_object_class_add_signals (object_class, action_manager_signals, LAST_SIGNAL);

  object_class->finalize = gnome_action_manager_finalize;

  klass->action_invoked = gnome_action_manager_real_action_invoked;
  klass->action_created = gnome_action_manager_real_action_created;
  klass->action_destroyed = gnome_action_manager_real_action_destroyed;
  klass->group_created = gnome_action_manager_real_group_created;
  klass->group_destroyed = gnome_action_manager_real_group_destroyed;
  klass->action_added = gnome_action_manager_real_action_added;
  klass->action_removed = gnome_action_manager_real_action_removed;
  klass->action_set_sensitive = gnome_action_manager_real_action_set_sensitive;
  klass->action_toggled = gnome_action_manager_real_action_toggled;
}

static void
gnome_action_manager_init (GnomeActionManager *am)
{
  am->priv = g_new0(GnomeActionManagerPrivate, 1);
  
  am->priv->action_hash = g_hash_table_new(g_str_hash, g_str_equal);
  am->priv->group_hash = g_hash_table_new(g_str_hash, g_str_equal);
  am->priv->model_hash = g_hash_table_new(g_direct_hash, NULL);
  am->priv->view_hash = g_hash_table_new(g_direct_hash, NULL);
}

static void
action_hash_destroy_each(gpointer key, gpointer value, gpointer user_data)
{
  GnomeActionInfo* ai = value;

  action_info_unref(ai);
}

static void
group_hash_destroy_each(gpointer key, gpointer value, gpointer user_data)
{
  GnomeGroupInfo* gi = value;

  group_info_unref(gi);
}

static void 
gnome_action_manager_finalize       (GtkObject* obj)
{
  GnomeActionManager* am;

  am = GNOME_ACTION_MANAGER(obj);

  /* If groups have more children than actions have parents,
   * this destruction order is most efficient. 
   */

  g_hash_table_foreach(am->priv->group_hash,
                       group_hash_destroy_each, 
                       NULL);

  g_hash_table_foreach(am->priv->action_hash,
                       action_hash_destroy_each,
                       NULL);

  g_hash_table_destroy(am->priv->action_hash);
  g_hash_table_destroy(am->priv->group_hash);
  g_hash_table_destroy(am->priv->model_hash);
  g_hash_table_destroy(am->priv->view_hash);

  g_free(am->priv);
  am->priv = NULL;

  (*parent_class->finalize)(obj);
}

static void
gnome_action_manager_real_action_invoked (GnomeActionManager *am, const gchar* action_id,  gpointer model, gpointer view)
{
  GnomeActionInfo* ai;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
  
  ai = must_lookup_action_info(am, action_id);

  (*ai->callback)(am, ai->id, model, view, ai->user_data);
}

static void
gnome_action_manager_real_action_created (GnomeActionManager *am, const gchar* action_id)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));

}

static void
gnome_action_manager_real_action_destroyed (GnomeActionManager *am, const gchar* action_id)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  
}


static void
gnome_action_manager_real_group_created (GnomeActionManager *am, const gchar* group_id)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));

}

static void
gnome_action_manager_real_group_destroyed (GnomeActionManager *am, const gchar* group_id)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  
}


static void
gnome_action_manager_real_action_added (GnomeActionManager *am, 
                                        const gchar* group_id, 
                                        const gchar* action_id)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  
}

static void
gnome_action_manager_real_action_removed (GnomeActionManager *am, 
                                          const gchar* group_id, 
                                          const gchar* action_id)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  
}

static void 
gnome_action_manager_real_action_set_sensitive (GnomeActionManager* am, 
                                                const gchar* action_id, 
                                                gboolean sensitivity, 
                                                gpointer model, gpointer view)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));

}

static void 
gnome_action_manager_real_action_toggled (GnomeActionManager* am, 
                                          const gchar* action_id, 
                                          gboolean active, 
                                          gpointer model, gpointer view)
{
  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  
}


static GnomeActionInfo* 
lookup_action_info(GnomeActionManager* am, const gchar* action_id)
{
  gpointer key = 0;
  gpointer value = 0;

  if (g_hash_table_lookup_extended(am->priv->action_hash, 
                                   action_id, &key, &value))
    {
      g_assert(key == ((GnomeActionInfo*)value)->id); /* check our internals */
      return (GnomeActionInfo*)value;
    }  
  else
    return NULL;
}

static GnomeActionInfo* 
must_lookup_action_info(GnomeActionManager* am, const gchar* action_id)
{
  GnomeActionInfo* ai;

  ai = lookup_action_info(am, action_id);

  g_return_val_if_fail(ai != NULL, NULL);

  return ai;
}


static GnomeGroupInfo* 
lookup_group_info(GnomeActionManager* am, const gchar* group_id)
{
  gpointer key = 0;
  gpointer value = 0;

  if (g_hash_table_lookup_extended(am->priv->group_hash, 
                                   group_id, &key, &value))
    {
      g_assert(key == ((GnomeGroupInfo*)value)->id); /* check our internals */
      return (GnomeGroupInfo*)value;
    }  
  else
    return NULL;
}

static GnomeGroupInfo* 
must_lookup_group_info(GnomeActionManager* am, const gchar* group_id)
{
  GnomeGroupInfo* gi;

  gi = lookup_group_info(am, group_id);

  g_return_val_if_fail(gi != NULL, NULL);

  return gi;
}


/************* Public API ***********/

GnomeActionManager*
gnome_action_manager_new ()
{
  GnomeActionManager* am = gtk_type_new(gnome_action_manager_get_type());
  
  return am;
}

void       
gnome_action_manager_create_action (GnomeActionManager* am,
                                    const gchar* action_id,
                                    GnomeActionFunc callback,
                                    gpointer user_data)
{
  GnomeActionInfo* ai;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
 
  ai = action_info_new(am, action_id);

  action_info_set_callback(ai, callback);
  action_info_set_user_data(ai, user_data);

  /* note that ai->id is the hash key, so it shouldn't be freed, etc. */
  g_hash_table_insert(am->priv->action_hash, ai->id, ai); 

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_CREATED],
                  ai->id);
}


void       
gnome_action_manager_create_group (GnomeActionManager* am,
                                   const gchar* group_id)
{
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (group_id != NULL);
 
  gi = group_info_new(am, group_id);

  /* note that gi->id is the hash key, so it shouldn't be freed, etc. */
  g_hash_table_insert(am->priv->group_hash, gi->id, gi); 

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[GROUP_CREATED],
                  gi->id);
}

void       
gnome_action_manager_create_subtree (GnomeActionManager* am,
                                     const gchar* group_id)
{
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (group_id != NULL);
 
  gi = group_info_new_subtree(am, group_id);

  /* note that gi->id is the hash key, so it shouldn't be freed, etc. */
  g_hash_table_insert(am->priv->group_hash, gi->id, gi); 

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[GROUP_CREATED],
                  gi->id);
}

void       
gnome_action_manager_append (GnomeActionManager* am,
                             const gchar* group_id,
                             const gchar* action_id)
{
  GnomeActionInfo* ai;
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
  g_return_if_fail (group_id != NULL);

  ai = must_lookup_action_info(am, action_id);
  gi = must_lookup_group_info(am, group_id);
  
  group_info_append(gi, ai);
  
  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_ADDED],
                  gi->id, ai->id);
}

void       
gnome_action_manager_remove (GnomeActionManager* am,
                             const gchar* group_id,
                             const gchar* action_id)
{
  GnomeActionInfo* ai;
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
  g_return_if_fail (group_id != NULL);

  ai = must_lookup_action_info(am, action_id);
  gi = must_lookup_group_info(am, group_id);
  
  group_info_remove(gi, ai);
  
  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_REMOVED],
                  gi->id, ai->id);
}


void       
gnome_action_manager_append_group (GnomeActionManager* am,
                                   const gchar* group_id,
                                   const gchar* subgroup_id)
{
  GnomeGroupInfo* sub_gi;
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (subgroup_id != NULL);
  g_return_if_fail (group_id != NULL);

  sub_gi = must_lookup_group_info(am, subgroup_id);
  gi = must_lookup_group_info(am, group_id);
  
  group_info_append_group(gi, sub_gi);
  
  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_ADDED],
                  gi->id, sub_gi->id);
}

void       
gnome_action_manager_remove_group (GnomeActionManager* am,
                                   const gchar* group_id,
                                   const gchar* subgroup_id)
{
  GnomeGroupInfo* sub_gi;
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (subgroup_id != NULL);
  g_return_if_fail (group_id != NULL);

  sub_gi = must_lookup_group_info(am, subgroup_id);
  gi = must_lookup_group_info(am, group_id);
  
  group_info_remove_group(gi, sub_gi);
  
  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_REMOVED],
                  gi->id, sub_gi->id);
}


/* IMPORTANT: don't do any magic in these destroy functions 
 * that isn't done in the finalize function for the action manager
 */

void       
gnome_action_manager_destroy_action (GnomeActionManager* am,
                                     const gchar* action_id)
{
  GnomeActionInfo* ai;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
  
  ai = must_lookup_action_info(am, action_id);

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_DESTROYED],
                  ai->id);

  g_hash_table_remove(am->priv->action_hash, ai->id);

  action_info_unref(ai);
}

void       
gnome_action_manager_destroy_group (GnomeActionManager* am,
                                    const gchar* group_id)
{
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (group_id != NULL);

  gi = must_lookup_group_info(am, group_id);

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[GROUP_DESTROYED],
                  gi->id);

  g_hash_table_remove(am->priv->group_hash, gi->id);

  group_info_unref(gi);
}

void       
gnome_action_manager_invoke(GnomeActionManager* am,
                            const gchar* action_id,
                            gpointer model, 
                            gpointer view)
{
  GnomeActionInfo* ai;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
  
  ai = must_lookup_action_info(am, action_id);

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_INVOKED],
                  ai->id, model, view);
}

void       
gnome_action_manager_set_name (GnomeActionManager* am,
                               const gchar* action_id,
                               const gchar* name)
{
  GnomeActionInfo* ai;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (action_id != NULL);
  
  ai = must_lookup_action_info(am, action_id);

  action_info_set_name(ai, name);

  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_NAME_CHANGED],
                  ai->id, ai->user_visible_name);
}

void       
gnome_action_manager_set_group_name (GnomeActionManager* am,
                                     const gchar* group_id,
                                     const gchar* name)
{
  GnomeGroupInfo* gi;

  g_return_if_fail (am != NULL);
  g_return_if_fail (GNOME_IS_ACTION_MANAGER (am));
  g_return_if_fail (group_id != NULL);

  gi = must_lookup_group_info(am, group_id);
  
  group_info_set_name(gi, name);
  
  gtk_signal_emit(GTK_OBJECT(am), action_manager_signals[ACTION_NAME_CHANGED],
                  gi->id, gi->user_visible_name);
}


/************* GnomeActionInfo implementation *************/

typedef enum {
  ACTION_NORMAL,
  ACTION_TOGGLE,
  ACTION_RADIO
} ActionType;

typedef struct _GnomeRealActionInfo GnomeRealActionInfo;

struct _GnomeRealActionInfo {
  GnomeActionInfo info;
    
  GnomeActionManager* am;
  ActionType type;
  guint refcount;
  GSList* parents;
};

GnomeActionInfo* 
action_info_new(GnomeActionManager* am, const gchar* id)
{
  GnomeRealActionInfo* rai;

  rai = g_new0(GnomeRealActionInfo, 1);

  rai->info.id = g_strdup(id);
  rai->info.is_group = FALSE;

  rai->type = ACTION_NORMAL;
  rai->refcount = 1;
  rai->am = am;

  return (GnomeActionInfo*)rai;
}

void             
action_info_ref(GnomeActionInfo* ai)
{
  GnomeRealActionInfo* rai = (GnomeRealActionInfo*)ai;

  rai->refcount += 1;
}

void             
action_info_unref(GnomeActionInfo* ai)
{
  GnomeRealActionInfo* rai = (GnomeRealActionInfo*)ai;

  g_return_if_fail(rai->refcount > 0);

  rai->refcount -= 1;

  if (rai->refcount == 0)
    {
      /* Have to do this first, before we become invalid... */
      while (rai->parents)
        {
          group_info_remove(rai->parents->data, ai);
        }

      if (rai->info.id)
        g_free(rai->info.id);

      if (rai->info.user_visible_name)
        g_free(rai->info.user_visible_name);

      g_free(rai);
    }
}

void 
action_info_set_name(GnomeActionInfo* ai, const gchar* name)
{
  GnomeRealActionInfo* rai = (GnomeRealActionInfo*)ai;

  if (rai->info.user_visible_name)
    g_free(rai->info.user_visible_name);

  rai->info.user_visible_name = NULL;

  if (name != NULL)
    rai->info.user_visible_name = g_strdup(name);
}

void             
action_info_set_callback(GnomeActionInfo* ai, GnomeActionFunc callback)
{
  ai->callback = callback;
}

void             
action_info_set_user_data(GnomeActionInfo* ai, gpointer user_data)
{
  ai->user_data = user_data;
}

void             
action_info_add_parent(GnomeActionInfo* ai, GnomeGroupInfo* gi)
{
  GnomeRealActionInfo* rai = (GnomeRealActionInfo*)ai;

  rai->parents = g_slist_prepend(rai->parents, gi);
}

void             
action_info_remove_parent(GnomeActionInfo* ai, GnomeGroupInfo* gi)
{
  GnomeRealActionInfo* rai = (GnomeRealActionInfo*)ai;

  rai->parents = g_slist_remove(rai->parents, gi);
}

GnomeActionManager*
action_info_get_am(GnomeActionInfo* ai)
{
  GnomeRealActionInfo* rai = (GnomeRealActionInfo*)ai;

  return rai->am;
}

/************ GnomeGroupInfo implementation **************/

typedef enum {
  GROUP_NORMAL,
  GROUP_SUBTREE,
  GROUP_RADIO
} GroupType;

typedef struct _GnomeRealGroupInfo GnomeRealGroupInfo;

struct _GnomeRealGroupInfo {
  GnomeGroupInfo info;

  GnomeActionManager* am;
  GroupType type;
  guint refcount;
  GSList* children;
  GSList* parents;
};

GnomeGroupInfo* 
group_info_new(GnomeActionManager* am, const gchar* id)
{
  GnomeRealGroupInfo* rgi;

  rgi = g_new0(GnomeRealGroupInfo, 1);

  rgi->info.id = g_strdup(id);
  rgi->info.is_group = TRUE;

  rgi->type = GROUP_NORMAL;
  rgi->refcount = 1;
  rgi->am = am;

  return (GnomeGroupInfo*)rgi;
}

GnomeGroupInfo* 
group_info_new_subtree(GnomeActionManager* am, const gchar* id)
{
  GnomeRealGroupInfo* rgi;

  /* so it's kinda gross, big deal */
  rgi = (GnomeRealGroupInfo*) group_info_new(am, id);

  rgi->type = GROUP_SUBTREE;

  return (GnomeGroupInfo*)rgi;
}

void
group_info_ref(GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  rgi->refcount += 1;
}

void            
group_info_unref(GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  g_return_if_fail(rgi->refcount > 0);

  rgi->refcount -= 1;

  if (rgi->refcount == 0)
    {
      /* Have to do this first, before we become invalid... */
      while (rgi->children)
        {
          GnomeGroupInfo* sub_gi = rgi->children->data;
          GnomeActionInfo* ai = rgi->children->data;

          if (sub_gi->is_group)
            group_info_remove_group(gi, sub_gi);
          else
            group_info_remove(gi, ai);
        }

      /* This too... */
      while (rgi->parents)
        {
          group_info_remove_group(rgi->parents->data, gi);
        }
      
      if (rgi->info.id)
        g_free(rgi->info.id);

      if (rgi->info.user_visible_name)
        g_free(rgi->info.user_visible_name);
      
      g_free(rgi);
    }
}


void 
group_info_set_name(GnomeGroupInfo* gi, const gchar* name)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  if (rgi->info.user_visible_name)
    g_free(rgi->info.user_visible_name);

  rgi->info.user_visible_name = NULL;

  if (name != NULL)
    rgi->info.user_visible_name = g_strdup(name);
}

void             
group_info_add_parent(GnomeGroupInfo* sub_gi, GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* sub_rgi = (GnomeRealGroupInfo*)sub_gi;

  sub_rgi->parents = g_slist_prepend(sub_rgi->parents, gi);
}

void             
group_info_remove_parent(GnomeGroupInfo* sub_gi, GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* sub_rgi = (GnomeRealGroupInfo*)sub_gi;

  sub_rgi->parents = g_slist_remove(sub_rgi->parents, gi);
}

void            
group_info_append(GnomeGroupInfo* gi, GnomeActionInfo* ai)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  action_info_ref(ai);

  rgi->children = g_slist_append(rgi->children, ai);

  action_info_add_parent(ai, gi);
}

void            
group_info_remove(GnomeGroupInfo* gi, GnomeActionInfo* ai)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  rgi->children = g_slist_remove(rgi->children, ai);

  action_info_remove_parent(ai, gi);

  action_info_unref(ai);
}

void            
group_info_append_group(GnomeGroupInfo* gi, GnomeGroupInfo* sub_gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  g_return_if_fail(gi != sub_gi);

  group_info_ref(sub_gi);

  rgi->children = g_slist_append(rgi->children, sub_gi);

  group_info_add_parent(sub_gi, gi);
}

void            
group_info_remove_group(GnomeGroupInfo* gi, GnomeGroupInfo* sub_gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  g_return_if_fail(gi != sub_gi);

  rgi->children = g_slist_remove(rgi->children, sub_gi);

  group_info_remove_parent(sub_gi, gi);

  group_info_unref(sub_gi);
}

gboolean
group_info_is_subtree(GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  return (rgi->type == GROUP_SUBTREE);
}

void 
group_info_foreach(GnomeGroupInfo* gi, 
                   GroupInfoForeachFunc func, 
                   gpointer user_data)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;
  GSList* tmp;

  tmp = rgi->children;

  while (tmp != NULL)
    {
      GnomeGroupInfo* sub_gi = tmp->data;
      GnomeActionInfo* ai = tmp->data;
      
      if (sub_gi->is_group)
        ai = NULL;
      else
        sub_gi = NULL;

      (*func)(sub_gi, ai, user_data);

      tmp = g_slist_next(tmp);
    }
}


GnomeActionManager*
group_info_get_am(GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  return rgi->am;
}

GSList* 
group_info_get_children(GnomeGroupInfo* gi)
{
  GnomeRealGroupInfo* rgi = (GnomeRealGroupInfo*)gi;

  return rgi->children;
}

/************* Code to create widgets tied to an action ***/

/* Attaching data to widgets */

#define WIDGET_ACTION_KEY "{-*gnome-am-action*-}"
#define WIDGET_GROUP_KEY "{-*gnome-am-group*-}"
#define WIDGET_MODEL_KEY "{-*gnome-am-model*-}"
#define WIDGET_VIEW_KEY "{-*gnome-am-view*-}"
#define WIDGET_LABEL_KEY "*-{gnome-am-label}-*"
#define WIDGET_PIXMAP_KEY "*-{gnome-am-pixmap}-*"
#define WIDGET_PARENT_KEY "*-{gnome-am-parent}-*"

static GnomeActionInfo* 
widget_get_action(GtkWidget* w)
{
  GnomeActionInfo* ai = gtk_object_get_data(GTK_OBJECT(w), WIDGET_ACTION_KEY);
  g_return_val_if_fail(ai != NULL, NULL);
  return ai;
}

static GnomeGroupInfo* 
widget_get_group(GtkWidget* w)
{
  GnomeGroupInfo* gi = gtk_object_get_data(GTK_OBJECT(w), WIDGET_GROUP_KEY);
  g_return_val_if_fail(gi != NULL, NULL);
  return gi;
}

static gpointer 
widget_get_model(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_MODEL_KEY);
  return p;
}

static gpointer 
widget_get_view(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_VIEW_KEY);
  return p;
}

static GtkWidget*
widget_get_label(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_LABEL_KEY);
  return p;
}

static GtkWidget*
widget_get_pixmap(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_PIXMAP_KEY);
  return p;
}

static GnomeGroupInfo*
widget_get_parent(GtkWidget* w)
{
  gpointer p = gtk_object_get_data(GTK_OBJECT(w), WIDGET_PARENT_KEY);
  return p;
}

static void
widget_set_action(GtkWidget* w, GnomeActionInfo* ai)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_ACTION_KEY, ai);
}

static void
widget_set_group(GtkWidget* w, GnomeGroupInfo* gi)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_GROUP_KEY, gi);
}

static void
widget_set_model(GtkWidget* w, gpointer model)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_MODEL_KEY, model);
}

static void
widget_set_view(GtkWidget* w, gpointer view)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_VIEW_KEY, view);
}

static void
widget_set_label(GtkWidget* w, GtkWidget* label)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_LABEL_KEY, label);
}

static void 
widget_set_pixmap(GtkWidget* w, GtkWidget* pixmap)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_PIXMAP_KEY, pixmap);
}

static void 
widget_set_parent(GtkWidget* w, GnomeGroupInfo* parent)
{
  gtk_object_set_data(GTK_OBJECT(w), WIDGET_PARENT_KEY, parent);
}

/* Actual widget creation */

static void 
widget_invoke_callback(GtkWidget* w, gpointer data)
{
  GnomeActionInfo* ai = (GnomeActionInfo*)data;

  gnome_action_manager_invoke(action_info_get_am(ai), ai->id, 
                              widget_get_model(w),
                              widget_get_view(w));
}

static void 
widget_action_name_changed_callback(GnomeActionManager* am, 
                                    const gchar* action_id,
                                    const gchar* new_name, 
                                    gpointer data)
{
  GtkWidget* w = data;
  GnomeActionInfo* ai = widget_get_action(w);

  g_return_if_fail(ai != NULL);

  if (strcmp(action_id, ai->id) == 0)
    {
      GtkWidget* label = widget_get_label(w);

      g_return_if_fail(label != NULL);

      gtk_label_set_text(GTK_LABEL(label), new_name);
    }
}

static void
widget_action_added_callback(GnomeActionManager* am,
                             const gchar* group_id,
                             const gchar* action_id,
                             gpointer data)
{
  GtkWidget* w = data;
  GnomeActionInfo* new_ai;
  GnomeGroupInfo* gi;
  GnomeGroupInfo* new_gi;
  gint position;
  GSList* tmp;
  GSList* tmp2;

  g_return_if_fail(w != NULL);
  g_return_if_fail(GTK_IS_MENU_SHELL(w));
  
  gi = widget_get_group(w);

  new_ai = lookup_action_info(am, action_id);

  if (new_ai == NULL)
    new_gi = lookup_group_info(am, action_id);
  
  /* The complicating factor is that an action can be in a menu more
   *  than once; so we can't just insert the action wherever it
   *  appears in the group's child list 
   */

  /* Find where the menu doesn't match the child list */

  position = 0;
  tmp = group_info_get_children(gi);
  tmp2 = GTK_MENU_SHELL(w)->children;

  while (tmp != NULL)
    {
      GnomeGroupInfo* sub_gi = tmp->data;
      GnomeActionInfo* sub_ai = tmp->data;

      GtkWidget* mi = tmp2->data;

      g_return_if_fail(GTK_IS_MENU_ITEM(mi));

      if (sub_gi->is_group)
        sub_ai = NULL;
      else
        sub_gi = NULL;

      if (sub_ai != NULL)
        {
          GnomeActionInfo* menu_ai = widget_get_action(mi);

          if (menu_ai == NULL ||
              menu_ai != sub_ai)
            break;               /* We've found the discontinuity */
        }
      else
        {
          GnomeGroupInfo* menu_gi = widget_get_group(mi);

          g_assert(sub_gi != NULL);
          
          if (menu_gi == NULL || 
              menu_gi != sub_gi)
            break;  /* discontinuity */
        }

      tmp = g_slist_next(tmp);
      tmp2 = g_slist_next(tmp2);
      ++position;
    }

  g_warning("not creating menu item, FIXME");

  {
    GtkWidget* mi = NULL;

    gtk_menu_shell_insert(GTK_MENU_SHELL(w), position, mi);
  }
}

static GtkWidget*
create_label (const char* label_text, guint* keyval)
{
  guint kv;
  GtkWidget *label;

  label = gtk_accel_label_new (label_text);

  kv = gtk_label_parse_uline (GTK_LABEL (label), label_text);
  if (keyval)
    *keyval = kv;

  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_widget_show (label);

  return label;
}

static GtkWidget*
create_menuitem(const gchar* small_icon, const gchar* stock_icon,
                const gchar* user_visible_name)
{
  GtkWidget* mi = NULL;
  GtkWidget* pixmap = NULL;
  GtkWidget* label = NULL;

  g_return_val_if_fail(user_visible_name != NULL, NULL);

  if (small_icon || stock_icon)
    {
      if (gnome_config_get_bool("/Gnome/Icons/MenusUseIcons=true") &&
          gnome_preferences_get_menus_have_icons())
        {
          mi = gtk_pixmap_menu_item_new();

          if (stock_icon)
            {
              pixmap = gnome_stock_pixmap_widget(mi, stock_icon);
            }
          else if (small_icon)
            {
              gchar* fn = gnome_pixmap_file(small_icon);
              
              if (fn == NULL)
                g_warning("Couldn't find pixmap %s", small_icon);
              else
                {
                  pixmap = gnome_pixmap_new_from_file(fn);
                  g_free(fn);
                }
            }

          if (pixmap != NULL)
            {
              gtk_widget_show(pixmap);
              gtk_pixmap_menu_item_set_pixmap(GTK_PIXMAP_MENU_ITEM(mi),
                                              pixmap);
              widget_set_pixmap(mi, pixmap);
            }
        }
    }
  else
    {
      mi = gtk_menu_item_new();
    }

  label = create_label(user_visible_name, NULL);

  widget_set_label(mi, label);
 
  gtk_container_add(GTK_CONTAINER(mi), label);

  gtk_widget_show(mi);

  return mi;
}

static GtkWidget* 
am_create_action_menuitem(GnomeActionManager* am,
                          GnomeActionInfo* ai,
                          gpointer model, 
                          gpointer view)
{
  GtkWidget* mi;
  
  mi = create_menuitem(ai->small_icon, ai->stock_icon,
                       ai->user_visible_name);

  widget_set_action(mi, ai);
  widget_set_model(mi, model);
  widget_set_view(mi, view);

  gtk_signal_connect(GTK_OBJECT(mi), 
                     "activate",
                     GTK_SIGNAL_FUNC(widget_invoke_callback),
                     ai);

  gtk_signal_connect(GTK_OBJECT(am),
                     "action_name_changed",
                     GTK_SIGNAL_FUNC(widget_action_name_changed_callback),
                     mi);

  return mi;
}

static void
am_add_group_to_menu(GnomeActionManager* am,
                     GnomeGroupInfo* gi,
                     gpointer model,
                     gpointer view,
                     GtkWidget* menu)
{
  g_return_if_fail(GTK_IS_MENU_SHELL(menu));
  
  g_warning("%s not implemented", __FUNCTION__);
}

static GtkWidget* 
am_create_group_menuitem(GnomeActionManager* am,
                         GnomeGroupInfo* gi,
                         gpointer model, 
                         gpointer view);

static void
child_adding_foreach(GnomeGroupInfo* as_group, 
                     GnomeActionInfo* as_action,
                     gpointer data)
{
  GtkWidget* mi = NULL;
  GtkWidget* menu = data;
  gpointer model = widget_get_model(menu);
  gpointer view = widget_get_view(menu);

  g_return_if_fail(GTK_IS_MENU_SHELL(menu));

  if (as_group != NULL)
    {
      if (group_info_is_subtree(as_group))
        {
          mi = am_create_group_menuitem(group_info_get_am(as_group), 
                                        as_group, model, view);
        }
      else
        {
          am_add_group_to_menu(group_info_get_am(as_group), 
                               as_group,
                               model, view, menu);
        }

    }
  else
    {
      g_assert(as_action != NULL);

      mi = am_create_action_menuitem(action_info_get_am(as_action), 
                                     as_action, 
                                     model, view);

    }

  if (mi != NULL)
    {
      gtk_menu_shell_append(GTK_MENU_SHELL(menu), mi);
    }
}

static GtkWidget*
am_create_group_menu(GnomeActionManager* am,
                     GnomeGroupInfo* gi,
                     gpointer model, 
                     gpointer view)
{
  GtkWidget* menu;

  g_return_val_if_fail(group_info_is_subtree(gi), NULL);

  menu = gtk_menu_new();

  widget_set_group(menu, gi);
  widget_set_model(menu, model);
  widget_set_view(menu, view);

  group_info_foreach(gi, child_adding_foreach, menu);

  return menu;
}

static GtkWidget* 
am_create_group_menuitem(GnomeActionManager* am,
                         GnomeGroupInfo* gi,
                         gpointer model, 
                         gpointer view)
{
  GtkWidget* mi;
  GtkWidget* menu;

  g_return_val_if_fail(group_info_is_subtree(gi), NULL);
  
  mi = create_menuitem(gi->small_icon, gi->stock_icon,
                       gi->user_visible_name);

  widget_set_group(mi, gi);
  widget_set_model(mi, model);
  widget_set_view(mi, view);
  
  menu = am_create_group_menu(am, gi, model, view);

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(mi), menu);

  return mi;
}

static GtkWidget* 
am_create_group_menubar(GnomeActionManager* am,
                        GnomeGroupInfo* gi,
                        gpointer model,
                        gpointer view)
{
  GtkWidget* bar;

  g_return_val_if_fail(!group_info_is_subtree(gi), NULL);

  bar = gtk_menu_bar_new();

  widget_set_group(bar, gi);
  widget_set_model(bar, model);
  widget_set_view(bar, view);

  group_info_foreach(gi, child_adding_foreach, bar);

  return bar;
}

/* Public widget creation interface */

GtkWidget*
gnome_action_manager_create_menuitem(GnomeActionManager* am,
                                     const gchar* action_id,
                                     gpointer model, 
                                     gpointer view)
{
  /* FIXME handle subtrees */
  GnomeActionInfo* ai;

  ai = must_lookup_action_info(am, action_id);

  g_return_val_if_fail(ai->user_visible_name != NULL, NULL);

  return am_create_action_menuitem(am, ai, model, view);
}

GtkWidget* 
gnome_action_manager_create_menubar(GnomeActionManager* am,
                                    const gchar* group_id,
                                    gpointer model,
                                    gpointer view)
{
  GnomeGroupInfo* gi;

  gi = must_lookup_group_info(am, group_id);

  return am_create_group_menubar(am, gi, model, view);
}

GtkWidget* 
gnome_action_manager_create_menu(GnomeActionManager* am,
                                 const gchar* group_id,
                                 gpointer model,
                                 gpointer view)
{
  GnomeGroupInfo* gi;

  gi = must_lookup_group_info(am, group_id);

  g_return_val_if_fail(gi->user_visible_name != NULL, NULL);

  return am_create_group_menu(am, gi, model, view);
}

/******** Misc *****************/

#include <math.h>

gchar*     
gnome_create_random_unique_string (void)
{
  static const gchar pool[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()~{}:\"<>?[];',./_|+=-";
  static const guint poolsize = sizeof(pool);

  gchar buf[128];
  
  guint nchars;
  guint i;

  /* Too-short strings would encourage duplicates, too long would chew
   RAM.  From 10-40 characters gives an astronomical number of
   possibilities, with the 90 or so characters in the pool. */

  nchars = (guint)((float)rand()/((float)RAND_MAX/(float)30));
  
  nchars += 10;

  g_assert(nchars < 128);

  i = 0;
  while (i < nchars)
    {
      guint val = (guint)((float)rand()/((float)RAND_MAX/(float)(poolsize-2))+0.5);
      g_assert(val < poolsize);
      buf[i] = pool[val];
      g_assert(buf[i] != '\0');
      ++i;
    }

  buf[i] = '\0';

  return g_strdup(buf);
}
