// -*- C++ -*-

/* 
 * rgbdraw.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_RGBDRAW_H
#define GUPPI_RGBDRAW_H

#include <glib.h>
#include "plotutils.h"

// Functions to draw directly to an RGB buffer.  I'm guessing most of
// these should be inline, or they might cause a big speed hit in
// inner loops. However I could be wrong. Using lots of const to help
// the optimizer.  As usual, stealing lots of libart ideas. Thanks
// Raph!

// We'll use the convention: paint_* do width/height clipping 
//  for you, set_* do not. set_* in general don't use the Context
//  either. They are the low level interface.

// An "RGB" is a drawable RGB buffer with a fixed size.

class RGB {
public:
  RGB(guchar* b, guint r, guint width, guint height) 
    : buf_(b), rowstride_(r), w_(width), h_(height) {}
  ~RGB() {}

  // Like a GC; associated with a particular RGB, 
  //  so you must not use it when the RGB has been destroyed,
  //  or on a different RGB
  class Context {
  public:
    Context(RGB& rgb);
    ~Context();

    void set_filled(bool setting);
    
    bool filled() const { return filled_; }

    void set_color(const Color & c);

    const Color & color() const { return color_; }

    void set_line_width(guint width);
    guint line_width() const { return line_width_; }

    // Clip rectangle is automatically truncated to fit within the RGB
    // buffer.
    void clip(guint x, guint y, guint w, guint h);

    // Expand clipping to entire RGB buffer (the default)
    void unclip();
    
    // Returns ints because the drawing functions take those
    // so it's convenient
    void get_clip(gint* x, gint* y, gint* w, gint* h) const
      {
        *x = clipx_;
        *y = clipy_;
        *w = clipw_;
        *h = cliph_;
      }

    // Make buffer-relative *x and *y relative to the clipping region
    void clip_relative(gint* x, gint* y) const 
      {
        *x -= clipx_;
        *y -= clipy_;
      }     

    bool in_clip(gint x, gint y) const 
      {
        return (x >= int(clipx_) && 
                y >= int(clipy_) && 
                x < int(clipx_ + clipw_) && 
                y < int(clipy_ + cliph_));
      }

    guint clip_area() const { return clipw_ * cliph_; }

    // For internal use ONLY (by RGB:: functions)
    const void* hidden() const { return hidden_; }

  private:
    Color color_;
    RGB& rgb_;
    guint clipx_;
    guint clipy_;
    guint clipw_;
    guint cliph_;

    bool filled_;
    guint line_width_;

    // The internal implementation involves the rasterization library,
    // which I want to keep out of header files.
    void* hidden_;

    // Sync clip with hidden_
    void sync_internal_clip();

    // Private to prevent use.
    Context& operator=(const Context& rhs);
    Context(const Context&);
  };

  // For efficiency reasons, set_pixel does *not* do width/height
  // clipping, or use a context, or anything else. So get it right...
  void set_pixel(const guint col, const guint row, // x, y
                 const guchar r, const guchar g, const guchar b)
    {
      guchar* pixel = buf_ + rowstride_*row + col*3;
      *pixel++ = r;
      *pixel++ = g;
      *pixel++ = b;
    }

  void set_pixel(const guint col, const guint row,  // x, y 
                 const guchar r, const guchar g, const guchar b,
                 const int alpha)
    {
      int v;
      guchar* pixel = buf_ + rowstride_*row + col*3;
      v = *pixel;
      *pixel++ = v + (((r - v) * alpha + 0x80) >> 8);
      v = *pixel;
      *pixel++ =  v + (((g - v) * alpha + 0x80) >> 8);
      v = *pixel;
      *pixel++ =  v + (((b - v) * alpha + 0x80) >> 8);
    }

  void paint_pixel(const gint col, const gint row, 
                   const Context& ctx)
    {
      if (ctx.in_clip(col, row))
        {
          const Color& c = ctx.color();
          set_pixel(col, row, c.r(), c.g(), c.b(), c.a());
        }
    }

  // libart_lgpl has a much faster way to do this,
  //  but for now I'm keeping it simple.
  void set_run(const guint col, const guint row, const guint N,
               const guchar r, const guchar g, const guchar b)
    {
      guint i = 0;
      guchar* pixel = buf_ + rowstride_*row + col*3;
      while (i < N)
	{
	  *pixel++ = r;
	  *pixel++ = g;
	  *pixel++ = b;
          ++i;
	}
    }

  void set_run(const guint col, const guint row, const guint N,
               const guchar r, const guchar g, const guchar b,
               const int alpha)
    {
      guchar v;
      guint i = 0;
      guchar* pixel = buf_ + rowstride_*row + col*3;
      while (i < N)
	{
          v = *pixel;
          *pixel++ = v + (((r - v) * alpha + 0x80) >> 8);
          v = *pixel;
          *pixel++ = v + (((g - v) * alpha + 0x80) >> 8);
          v = *pixel;
          *pixel++ = v + (((b - v) * alpha + 0x80) >> 8);
          ++i;
	}
    }

  struct Point {
    gint x, y;
  };

  void paint_convex_poly(guint npoints, const Point points[], const Context& ctx);

  void paint_rect(gint x1, gint y1, 
                  gint x2, gint y2,
                  const Context & ctx);

  void paint_line(gint x1, gint y1, 
                  gint x2, gint y2,
                  const Context & ctx);

  void paint_line(guint npoints, 
                  const Point points[],
                  const Context & ctx);

  void paint_segments(guint npoints,
                      const Point points[],
                      const Context& ctx);

  void paint_arc(gint x, gint y, gint width, gint height, 
                 gint angle1, gint angle2, const Context& ctx);

  guint width() const { return w_; }
  guint height() const { return h_; }
  guint rowstride() const { return rowstride_; }

  // Using this isn't especially recommended.
  guchar* buf() { return buf_; }

private:
  guchar* buf_;
  const guint rowstride_;
  const guint w_;
  const guint h_;
};

#endif
