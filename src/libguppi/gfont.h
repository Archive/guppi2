// -*- C++ -*-

/* 
 * gfont.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_GFONT_H
#define GUPPI_GFONT_H

#include "gnome-util.h"
#include "vfont.h"
#include <vector>

class GFontInternal;
typedef struct _GnomeFont GnomeFont;

class GFont : public VFont {
public:
  virtual ~GFont();

  // Return the width of string str, unscaled.
  virtual double string_width(const gchar* str) const;

  // Return the height of the font, unscaled.
  virtual double font_height() const;

  // Return an X font that's nice for displaying at the given scale.
  // Scale is 1.0 for 1:1 ratio
  // If you're going to keep the GdkFont around, you should gdk_font_ref() it
  //  probably.
  GdkFont* gdk_font(double scale);

  const gchar* font_name() const;

  typedef enum {
    Light,
    Normal,
    Bold
  } FontWeightType;

  static GFont* get_font(const gchar* name, FontWeightType weight, bool italic, gint size);
  
  // Use only in printer-gnome.cc
  GnomeFont* gnome_font();

private:
  GFontInternal* internal_;

  GFont(GFontInternal*);

  const GFont& operator=(const GFont&);
  GFont(const GFont &);
  GFont();
  friend class FooBarBaz; // eliminate egcs warning about private constructors
};

class GFontList {
public:
  GFontList();
  ~GFontList();

  typedef vector<GFont*>::iterator iterator;
  
  iterator begin() { return list_.begin(); }
  iterator end() { return list_.end(); }

  guint ref();
  guint unref();

private:
  vector<GFont*> list_;
  RC rc_;
};

// Returns the global font list object, containing an unbolded,
// reasonably-sized, unitalic instance of each font Gnome knows
// about. List may be empty.  GFontList and the fonts it holds persist
// until they are unreferenced for the last time.

GFontList* 
guppi_get_font_list();

void
guppi_shutdown_fonts();

#endif
