// -*- C++ -*-

/* 
 * guppi-rint.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


// No include guards because this file should not be installed or included
//  in any other headers

#include <config.h>

// Without this, the compiler will build rint wrong
extern "C" {
double rint(double x);
}

// I'm not sure this really works (in particular, K&R suggest 
//  that it isn't guaranteed to)
#ifndef HAVE_RINT
static inline double 
rint(double x)
{
  if (x > 0.5)
    return (int)(x+0.5);
  else if (x < 0.5)
    return (int)(x-0.5);
  else
    return 0.0;
}
#endif
