// -*- C++ -*-

/* 
 * canvas-pricebars.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomeui/gnome-canvas.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>
#include <libart_lgpl/art_rgb.h>
#include <libgnomeui/gnome-canvas-util.h>

#include "canvas-pricebars.h"
#include "util.h"
#include "rgbdraw.h"
#include "plotutils.h"
#include "pointtiles.h"
#include "xyplotstate.h"
#include "scalardata.h"
#include "plotdraw.h"

#include <math.h>

#include "guppi-math.h"

enum {
  ARG_0
};


static void guppi_pricebars_class_init (GuppiPricebarsClass *klass);
static void guppi_pricebars_init       (GuppiPricebars      *gs);
static void guppi_pricebars_destroy    (GtkObject            *object);
static void guppi_pricebars_set_arg    (GtkObject            *object,
                                      GtkArg               *arg,
                                      guint                 arg_id);
static void guppi_pricebars_get_arg    (GtkObject            *object,
                                      GtkArg               *arg,
                                      guint                 arg_id);

static void   guppi_pricebars_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags);
static void   guppi_pricebars_realize     (GnomeCanvasItem *item);
static void   guppi_pricebars_unrealize   (GnomeCanvasItem *item);
static void   guppi_pricebars_draw        (GnomeCanvasItem *item, GdkDrawable *drawable,
                                         int x, int y, int width, int height);
static double guppi_pricebars_point       (GnomeCanvasItem *item, double x, double y,
                                         int cx, int cy, GnomeCanvasItem **actual_item);
static void   guppi_pricebars_translate   (GnomeCanvasItem *item, double dx, double dy);
static void   guppi_pricebars_bounds      (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);
static void   guppi_pricebars_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);


static GnomeCanvasItemClass *parent_class;


GtkType
guppi_pricebars_get_type (void)
{
  static GtkType gs_type = 0;

  if (!gs_type) {
    GtkTypeInfo gs_info = {
      "GuppiPricebars",
      sizeof (GuppiPricebars),
      sizeof (GuppiPricebarsClass),
      (GtkClassInitFunc) guppi_pricebars_class_init,
      (GtkObjectInitFunc) guppi_pricebars_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    gs_type = gtk_type_unique (gnome_canvas_item_get_type (), &gs_info);
  }

  return gs_type;
}

static void
guppi_pricebars_class_init (GuppiPricebarsClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = 
    (GnomeCanvasItemClass*)gtk_type_class (gnome_canvas_item_get_type ());

  object_class->destroy = guppi_pricebars_destroy;
  object_class->set_arg = guppi_pricebars_set_arg;
  object_class->get_arg = guppi_pricebars_get_arg;

  item_class->update = guppi_pricebars_update;
  item_class->realize = guppi_pricebars_realize;
  item_class->unrealize = guppi_pricebars_unrealize;
  item_class->draw = guppi_pricebars_draw;
  item_class->point = guppi_pricebars_point;
  item_class->translate = guppi_pricebars_translate;
  item_class->bounds = guppi_pricebars_bounds;

  item_class->render = guppi_pricebars_render;
}

static void
guppi_pricebars_init (GuppiPricebars *gs)
{
  gs->get_info = 0;
  gs->info_data = 0;
}

static void
guppi_pricebars_destroy (GtkObject *object)
{
  GuppiPricebars *gs;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GUPPI_IS_PRICEBARS(object));

  gs = GUPPI_PRICEBARS (object);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void 
get_info(GuppiPricebars* gs,                                 
         const Transform** xtrans, 
         const Transform** ytrans, 
         guint* N, 
         XyPlotState** state,
         XyPriceBars** layer)
{
  if (gs->get_info)
    {
      XyPlotState* mystate;
      XyPriceBars* mylayer;

      (*gs->get_info)(gs, gs->info_data, &mystate, &mylayer);
      
      g_assert(mystate != 0);
      g_assert(mylayer != 0);
      g_assert(mylayer->type() == XyLayer::PriceLayer);
      
      if (xtrans)
        {
          *xtrans = &mystate->trans(mylayer->x_axis());
        }
      if (ytrans)
        {
          *ytrans = &mystate->trans(mylayer->y_axis());
        }
      if (N)
        {
          *N = mylayer->nbars();
        }
      if (state)
        {
          *state = mystate;
        }
      if (layer)
        {
          *layer = mylayer;
        }
    }
  else 
    {
      if (xtrans) *xtrans = 0;
      if (ytrans) *ytrans = 0;
      if (N) *N = 0;
      if (state) *state = 0;
      if (layer) *layer = 0;
    }      
}

static void
guppi_pricebars_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GuppiPricebars *gs;

  item = GNOME_CANVAS_ITEM (object);
  gs = GUPPI_PRICEBARS (object);

  switch (arg_id) {

  default:
    break;
  }
}

static void
guppi_pricebars_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

// barely worth it...
static inline void
data2buf(double data_x, double data_y, 
         gint* buf_x, gint* buf_y,
         const Transform* xtrans, 
         const Transform* ytrans,
         const Affine& i2c, 
         GnomeCanvasBuf* buf)
{
  Affine::Point p(xtrans->transform(data_x),
                  ytrans->transform(data_y));

  i2c.transform(p);

  *buf_x = (int)rint(p.x);
  *buf_y = (int)rint(p.y);

  *buf_x -= buf->rect.x0;
  *buf_y -= buf->rect.y0;
}     

static void
guppi_pricebars_render (GnomeCanvasItem *item,
                        GnomeCanvasBuf *buf)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (item);

  const Transform* xtrans;
  const Transform* ytrans;
  guint N;
  XyPlotState* state;
  XyPriceBars* layer;

  if (buf->is_bg)
    {
      gnome_canvas_buf_ensure_buf(buf);
      buf->is_bg = 0;
    }

  get_info(gs, &xtrans, &ytrans, &N, &state, &layer);

  if (N == 0) 
    return;  

  // consider the affine we got in update() (i2c)
  Affine i2c(gs->a);

  RGB rgb(buf->buf, buf->buf_rowstride, 
          (buf->rect.x1 - buf->rect.x0),
          (buf->rect.y1 - buf->rect.y0));

  guppi_draw_pricebars(layer, rgb, i2c, *xtrans, *ytrans, 
                       item->canvas->pixels_per_unit, 
                       buf->rect.x0, buf->rect.y0);
}


//static GnomeCanvasItem* rect = 0;

static void
guppi_pricebars_update (GnomeCanvasItem *item, double *update_affine, ArtSVP *clip_path, int flags)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (item);

  if (parent_class->update)
    (* parent_class->update) (item, update_affine, clip_path, flags);

  if (item->canvas->aa) 
    {
      memcpy(gs->a, update_affine, sizeof(double)*6);

      gnome_canvas_item_reset_bounds (item);

      guint N;
      const Transform* xtrans;
      const Transform* ytrans;    

      get_info(gs, &xtrans, &ytrans, &N, 0, 0);

      // Get item coords bounds
      double dx1, dy1, dx2, dy2;
      xtrans->screen_bounds(&dx1, &dx2);
      ytrans->screen_bounds(&dy1, &dy2);

      PlotUtil::canonicalize_rectangle(&dx1, &dy1, &dx2, &dy2);
    
      // So now we have the bounds in Item coords; we apply the 
      // update_affine to them 

      Affine i2c(update_affine);

      Affine::Point nw(dx1,dy1), se(dx2,dy2);

      i2c.transform(nw);
      i2c.transform(se);

#if 0
      int x1, x2, y1, y2;
      gnome_canvas_item_i2w(item, &dx1, &dy1);
      gnome_canvas_item_i2w(item, &dx2, &dy2);
      gnome_canvas_w2c(item->canvas, dx1, dy1, &x1, &y1);
      gnome_canvas_w2c(item->canvas, dx2, dy2, &x2, &y2);
#endif

      // FIXME what if nw/se coords got re-ordered?

      gnome_canvas_update_bbox(item, 
                               static_cast<int>(floor(nw.x)), 
                               static_cast<int>(floor(nw.y)), 
                               static_cast<int>(ceil(se.x)), 
                               static_cast<int>(ceil(se.y)));

      g_debug("requesting redraw on %g,%g %g,%g", nw.x, nw.y, se.x, se.y);
    } 
  else 
    {
      g_warning("pricebars item requires AA mode");
    }
}

static void
guppi_pricebars_realize (GnomeCanvasItem *item)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (item);

  if (parent_class->realize)
    (* parent_class->realize) (item);
}

static void
guppi_pricebars_unrealize (GnomeCanvasItem *item)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (item);

  if (parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
guppi_pricebars_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
                    int x, int y, int width, int height)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (item);

  g_warning("GuppiPricebars does not work on the Gdk canvas");
}

static double
guppi_pricebars_point (GnomeCanvasItem *item, double x, double y,
                     int cx, int cy, GnomeCanvasItem **actual_item)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS(item);

  *actual_item = item;

  double x1, y1, x2, y2;

  guppi_pricebars_bounds(item, &x1, &y1, &x2, &y2);

  if (x <= x2 && x >= x1 && y <= y2 && y >= y1)
    return 0.0;
  else
    {
      double dx, dy;

      if (x > x2)
        dx = x - x2;
      else if (x < x1)
        dx = x1 - x;
      else 
        dx = 0.0;

      if (y > y2)
        dy = y - y2;
      else if (y < y1)
        dy = y1 - y;
      else 
        dy = 0.0;

      g_assert(dx > 0.0 || dy > 0.0);

      return PlotUtil::hypot(dx, dy);
    }
}

static void
guppi_pricebars_translate (GnomeCanvasItem *item, double dx, double dy)
{
  GuppiPricebars *gs;
  gs = GUPPI_PRICEBARS (item);

  g_warning("pricebars translate isn't implemented");
}

static void
guppi_pricebars_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2)
{
  GuppiPricebars *gs;

  gs = GUPPI_PRICEBARS (item);

  guint N;
  const Transform* xtrans;
  const Transform* ytrans;
    
  get_info(gs, &xtrans, &ytrans, &N, 0, 0);

  xtrans->screen_bounds(x1, x2);
  ytrans->screen_bounds(y1, y2);

  PlotUtil::canonicalize_rectangle(x1, y1, x2, y2);
    
  //  g_debug("Bounds: %g,%g %g,%g", *x1, *y1, *x2, *y2);
  
  // So now we have the bounds in Item coords
}


void 
guppi_pricebars_set_info_func(GuppiPricebars* gs, 
                            PricebarsInfoFunc func,
                            gpointer user_data)
{
  g_return_if_fail(gs != 0);
  g_return_if_fail(GUPPI_IS_PRICEBARS(gs));

  gs->get_info = func;
  gs->info_data = user_data;
}



