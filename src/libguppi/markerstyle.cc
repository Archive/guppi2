// -*- C++ -*-

/* 
 * markerstyle.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "markerstyle.h"

MarkerPalette::MarkerPalette()
  : current_(DEFAULT)
{
  MarkerStyle* ms = new MarkerStyle;

  ms->set_name(_("Default Marker Style"));

  guint32 id = add(ms);

  g_warn_if_fail(id == DEFAULT);
}

inline void 
MarkerPalette::grab_style(MarkerStyle* style)
{
  if (style != 0) 
    style->ref();
}

inline void 
MarkerPalette::release_style(MarkerStyle* style)
{
  if (style != 0)
    {
      if (style->unref() == 0)
        delete style;
    }
}

MarkerPalette::~MarkerPalette()
{
  vector<MarkerStyle*>::iterator i = markers_.begin();
  while (i != markers_.end())
    {
      release_style(*i);
      ++i;
    }
}

const guint32 MarkerPalette::DEFAULT = 0;

guint32 
MarkerPalette::current() const
{
  return current_;
}

void 
MarkerPalette::set_current(guint32 id)
{
  guint32 old = current_;
  current_ = id;

  palette_model.current_changed(this, old, current_);
}

// Returns an ID for accessing the MarkerStyle
guint32 
MarkerPalette::add(MarkerStyle* ms)
{
  guint32 retval = 0;

  if (!holes_.empty())
    {
      retval = holes_.back();

      g_warn_if_fail(retval < markers_.size());
      g_warn_if_fail(markers_[retval] == 0);

      markers_[retval] = ms;

      holes_.pop_back();
    }
  else
    {
      retval = markers_.size();
      markers_.push_back(ms);
    }
  
  grab_style(ms);

  palette_model.entry_added(this, retval);

  return retval;
}

void  
MarkerPalette::remove(guint32 id)
{
  g_return_if_fail(id < markers_.size());
  g_return_if_fail(markers_[id] != 0);
  g_return_if_fail(id != DEFAULT);

#ifdef GNOME_ENABLE_DEBUG
  set<guint32>::iterator i = checkouts_.find(id);
  if (i != checkouts_.end())
    {
      g_warning("Removing a checked-out MarkerStyle!!!");
    }
#endif

  palette_model.entry_removed(this, id);

  release_style(markers_[id]);

  markers_[id] = 0;

  holes_.push_back(id);
}

guint32 
MarkerPalette::find(MarkerStyle* ms) const
{
  guint32 id = 0;
  while (id < markers_.size())
    {
      if (markers_[id] == ms)
        return id;
      
      ++id;
    }

#ifdef GNOME_ENABLE_DEBUG
  g_warning("Style not found in marker palette!!");
#endif

  return MarkerPalette::DEFAULT;
}

guint32
MarkerPalette::max_id() const
{
  guint32 i = markers_.size();
  while (i > 0)
    {
      if (markers_[i] != 0) break;

      --i;
    }

  return i;
}

MarkerStyle* 
MarkerPalette::checkout(guint32 id)
{
#ifdef GNOME_ENABLE_DEBUG
  if (id >= markers_.size())
    {
      g_warning("Attempt to check out style palette entry %u, which has never existed.\n",
                id);
      return 0;
    }
#endif
    if (markers_[id] == 0) 
      return 0;
    else 
      {
        set<guint32>::iterator i = checkouts_.find(id);
        if (i == checkouts_.end())
          {
            checkouts_.insert(id);
            markers_[id]->ref(); // the checkouts set ref()s the style in case it gets removed.
            return markers_[id];
          }
        else 
          return 0;
      }
}


void 
MarkerPalette::checkin(MarkerStyle* style, guint32 id)
{
  g_return_if_fail(style != 0);

#ifdef GNOME_ENABLE_DEBUG
  if (id >= markers_.size())
    {
      g_warning("Attempt to check in style palette entry %u, which has never existed.\n",
                id);
      return;
    }
#endif

  set<guint32>::iterator i = checkouts_.find(id);

  if (i != checkouts_.end())
    {
      checkouts_.erase(i);

      if (style->unref() == 0)
        {
          g_assert(markers_[id] != style); // should have been removed
          delete style;
        }

      palette_model.entry_changed(this, id);
    }
#ifdef GNOME_ENABLE_DEBUG
  else
    {
      g_warning("Checked-in style was not checked out!");
    }
#endif
}







