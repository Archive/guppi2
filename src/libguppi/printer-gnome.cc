// -*- C++ -*-

/* 
 * printer-gnome.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "printer-gnome.h"

#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-printer-dialog.h>
#include "gfont.h"

GPrinter::GPrinter()
  : printer_(0), context_(0)
{
  printer_ = gnome_printer_new_generic_ps("test.ps"); // FIXME
  context_ = gnome_print_context_new(printer_);
}

GPrinter::GPrinter(GnomePrinter* printer)
  : printer_(printer), context_(0)
{
  g_return_if_fail(printer_ != 0);
  context_ = gnome_print_context_new(printer_);
}

GPrinter::~GPrinter()
{
  if (context_)
    gtk_object_destroy(GTK_OBJECT(context_));
  if (printer_)
    gtk_object_destroy(GTK_OBJECT(printer_));
}

void
GPrinter::newpath()
{
  gnome_print_newpath(context_);
}

void
GPrinter::moveto(double x, double y)
{
  gnome_print_moveto(context_, x, y);
}

void
GPrinter::lineto(double x, double y)
{
  gnome_print_lineto(context_,x,y);
}

void
GPrinter::curveto(double x1, double y1, double x2, double y2, double x3, double y3)
{
  gnome_print_curveto(context_, x1, y1, x2, y2, x3, y3);
}

void
GPrinter::closepath()
{
  gnome_print_closepath(context_);
}

void
GPrinter::setcolor(const Color & c)
{
  gnome_print_setrgbcolor(context_, 
                          (float)c.r()/(float)255, 
                          (float)c.g()/(float)255, 
                          (float)c.b()/(float)255);
}

void
GPrinter::fill()
{
  gnome_print_fill(context_);
}

void
GPrinter::eofill()
{
  gnome_print_eofill(context_);
}

void
GPrinter::setlinewidth(double w)
{
  gnome_print_setlinewidth(context_, w);
}

void
GPrinter::setmiterlimit(double limit)
{
  gnome_print_setmiterlimit(context_, limit);
}

void
GPrinter::setlinejoin(int jointype)
{
  gnome_print_setlinejoin(context_,jointype);
}

void
GPrinter::setlinecap(int captype)
{
  gnome_print_setlinecap(context_,captype);
}

void
GPrinter::setdash(int n_values, double* values, double offset)
{
  gnome_print_setdash(context_, n_values, values, offset);
}

void
GPrinter::strokepath()
{
  gnome_print_strokepath(context_);
}

void
GPrinter::stroke()
{
  gnome_print_stroke(context_);
}

void
GPrinter::setfont(VFont* font)
{
  GnomeFont* gnomefont = 0;

  gnomefont = static_cast<GFont*>(font)->gnome_font();

  if (gnomefont != 0)
    gnome_print_setfont(context_, gnomefont);
}

void
GPrinter::show(const char* text)
{
  gnome_print_show(context_, text);
}

void
GPrinter::concat(double matrix[6])
{
  gnome_print_concat(context_, matrix);
}

void
GPrinter::concat(const Affine& a)
{
  gnome_print_concat(context_, (double*)a.raw());
}

void
GPrinter::setmatrix(double matrix[6])
{
  gnome_print_setmatrix(context_, matrix);
}

void
GPrinter::setmatrix(const Affine& a)
{
  gnome_print_setmatrix(context_, (double*)a.raw());
}

void 
GPrinter::translate(double x, double y)
{
  gnome_print_translate(context_, x, y);
}

void 
GPrinter::rotate(double degrees)
{
  gnome_print_rotate(context_, degrees);
}

void 
GPrinter::scale(double xfactor, double yfactor)
{
  gnome_print_scale(context_, xfactor, yfactor);
}

void
GPrinter::gsave()
{
  gnome_print_gsave(context_);
}

void
GPrinter::grestore()
{
  gnome_print_grestore(context_);
}

void
GPrinter::clip()
{
  gnome_print_clip(context_);
}

void
GPrinter::eoclip()
{
  gnome_print_eoclip(context_);
}

void
GPrinter::grayimage(const guchar* data, int width, int height, int rowstride)
{
  gnome_print_grayimage(context_, (gchar*)data, width, height, rowstride);
}

void
GPrinter::rgbimage(const guchar* data, int width, int height, int rowstride)
{
  gnome_print_rgbimage(context_, (gchar*)data, width, height, rowstride);
}

void
GPrinter::showpage()
{
  gnome_print_showpage(context_);
}

bool
GPrinter::sync()
{
  g_return_val_if_fail(context_ != 0, false);

  return (gnome_print_context_close(context_) == 0);
}

////////////////////

struct print_cbdata {
  PrintAction* pa;
  bool destroy_pa;
};

static gint
dialog_close_cb(GtkWidget* dialog, print_cbdata* data)
{
  GnomePrinter* printer = gnome_printer_dialog_get_printer(GNOME_PRINTER_DIALOG(dialog));
  
  if (printer != NULL)
    {
      GPrinter* gprinter = new GPrinter(printer);
      
      data->pa->print(gprinter);

      delete gprinter; // owns the GnomePrinter
    }

  if (data->destroy_pa)
    delete data->pa;

  delete data;

  return FALSE;
}

void 
guppi_get_printer_and_print(PrintAction* pa, bool destroy_print_action)
{
  GtkWidget* d = gnome_printer_dialog_new();

  print_cbdata* data = new print_cbdata;

  data->pa = pa;
  data->destroy_pa = destroy_print_action;

  gtk_signal_connect(GTK_OBJECT(d), "close",
                     GTK_SIGNAL_FUNC(dialog_close_cb),
                     data);

  gnome_dialog_set_close(GNOME_DIALOG(d), TRUE);

  gtk_widget_show(d);
}
