// -*- C++ -*-

/* 
 * typeregistry.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "typeregistry.h"

TypeRegistry::TypeRegistry()
{

}

TypeRegistry::~TypeRegistry()
{

}

const int TypeRegistry::INVALID_TYPE = -1;

void 
TypeRegistry::add_builtin_type(int type_id,
                               int base_type_id,
                               const char* name)
{
  g_return_if_fail(type_id == (int)parents_.size());

  // Note: no assertion that the parent type already exists...

  parents_.push_back(base_type_id);
  names_.push_back(string(name));

  g_assert(names_.size() == parents_.size());
}

// returns type_id
int TypeRegistry::add_runtime_type(int base_type_id,
                                   const char* name)
{
  int type_id = (int)parents_.size();

  parents_.push_back(base_type_id);
  names_.push_back(string(name));

  g_assert(names_.size() == parents_.size());

  // In contrast to the builtin types, for runtime types the parent type 
  //  must exist.
  g_assert(base_type_id == INVALID_TYPE || is_a(type_id, base_type_id));
  g_assert(base_type_id == parent_type(type_id));

  return type_id;
}

bool 
TypeRegistry::is_valid(int type_id)
{
  return (type_id >= 0 && type_id < (int)parents_.size());
}

int 
TypeRegistry::parent_type(int type_id)
{
  g_return_val_if_fail(type_id > INVALID_TYPE, INVALID_TYPE);
  g_return_val_if_fail(type_id < (int)parents_.size(), INVALID_TYPE);

  if (type_id == INVALID_TYPE)
    return INVALID_TYPE;
  else
    return parents_[type_id];
}

bool 
TypeRegistry::is_a(int type_id, int parent_type_id)
{
  g_return_val_if_fail(type_id > INVALID_TYPE, false);
  g_return_val_if_fail(type_id < (int)parents_.size(), false);
  g_return_val_if_fail(parent_type_id > INVALID_TYPE, false);
  g_return_val_if_fail(parent_type_id < (int)parents_.size(), false);
  g_assert(names_.size() == parents_.size());

  int next_type_id = type_id;
  
  while (next_type_id != INVALID_TYPE)
    {
      if (next_type_id == parent_type_id)
        return true;
      else
        next_type_id = parents_[next_type_id];
    }
  
  return false;
}

string 
TypeRegistry::type_name(int type_id)
{
  g_return_val_if_fail(type_id >= INVALID_TYPE, "");
  g_return_val_if_fail(type_id < (int)names_.size(), "");
  g_assert(names_.size() == parents_.size());

  if (type_id == INVALID_TYPE)
    return "*Invalid Type*";
  else
    return names_[type_id];
}

void 
TypeRegistry::set_type_name(int type_id, const char* name)
{
  g_return_if_fail(type_id > INVALID_TYPE);
  g_return_if_fail(type_id < (int)names_.size());  

  names_[type_id] = name;
}
