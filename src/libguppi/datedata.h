// -*- C++ -*-

/* 
 * datedata.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATEDATA_H
#define GUPPI_DATEDATA_H

#include "data.h"

class DateSet;

class DateData : public Data {
public:
  DateData();
  DateData(DateSet* ss); // DateData will own the DateSet
  virtual ~DateData();

  // number of elements
  virtual gsize size() const;

  virtual const string& name() const;

  virtual void set_name(const string & name);

  void set_date(guint index, GDate gdate);
  GDate get_date(guint index) const;

  void set_date_julian(guint index, guint date);
  guint get_date_julian(guint index) const;

  // These are not implemented for real yet; FIXME
  guint min_julian() const;
  guint max_julian() const;

  string get_string(guint index) const;

  void add(const GDate& gdate);

  virtual xmlNodePtr xml(xmlNodePtr parent) const;
  
  void set_xml(xmlNodePtr node);

  const DateSet* dateset() const { return set_; }

  // checkin/checkout works just like ScalarData, see scalardata.h

  // Returns 0 if someone else has it
  DateSet* checkout_dateset();
  void checkin_dateset(DateSet* set, bool values_changed = true, bool other_changed = false);

  virtual gsize byte_size();

protected:
  DateSet* set_;

  bool checked_out_;

private:

  DateData(const DateData &);
  const DateData& operator=(const DateData&);
};

#endif
