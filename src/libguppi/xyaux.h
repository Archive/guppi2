// -*- C++ -*-

/* 
 * xyaux.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_XYAUX_H
#define GUPPI_XYAUX_H

// Utility classes used in XyPlotState

#include "util.h"
#include "data.h"
#include "datastore.h"
#include "plotutils.h"
#include "refcount.h"
#include <set>

class ScalarData;
class SortedPair;

// XyPair is a SortedPair, with associated marker styles.
// It is the part of the scatter plot that isn't related to the axes.
// It should not be used directly; it's a ScatterPlotState internal.
// If you use it directly the proper notifications will not be sent 
// to ScatterPlotState views.

class XyMarkers {
public:
  XyMarkers();
  ~XyMarkers();

  XyMarkers* clone() const;

  void reserve(guint size);

  guint size() const;

  void set_style(guint point, guint style_id);
  // will erase any previously-set styles
  void set_global_style(guint style_id);
  // Sets style for lots of points at once.
  void set_style(const vector<guint> & points, guint style_id);

  // Return the markersize of the largest style in the pair.
  // The returned value may be too large; we do some guessing to keep from
  // iterating over all the styles every time.
  // This is needed when deciding which region of a display to update.
  double largest_style();

  /// Report whether this style is in use for this plot.
  /// May report false positives, for the same reason largest_style() might.
  bool   style_in_use(guint32 styleid);

  // If any point uses a style with an ID > 255, we need two bytes 
  //  per point to store the styles. But we don't want the memory hit
  //  normally. So this mess is needed.
  bool using_global_style() const;
  guint global_style() const;
  bool using_small_styles() const;
  // if the arrays are non-zero and we aren't using the global style,
  //  they are at least npoints() in size
  const guint8* small_styles() const;
  const guint16* large_styles() const; 

  guint ref() { return rc_.ref(); }

  guint unref() { return rc_.unref(); }

  class View : public ::View 
  {
  public:
    virtual void change_styles(XyMarkers* markers) = 0;
  };

  class Model : public ::Model<XyMarkers::View*> {
  public:
    void styles_changed(XyMarkers* m) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_styles(m);
	++i;
      }
      unlock_views();
    }

  };
  
  Model markers_model;  

private:
  RC rc_;

  // Keep a bunch of guints in the smallest possible
  //  space
  class IndexVector {
  public:
    IndexVector() : using_8_(true), a8_(0), a16_(0), n_(0) {}
    ~IndexVector();

    // A copy constructor might be better, but they make me nervous
    // for this kind of complicated thing; not explicit enough.
    void copy_to(IndexVector& iv) const;

    // Must reserve() before using the IndexVector
    void reserve(guint N);

    guint size() const { return n_; }

    guint8* small() { return a8_; }
    guint16* large() { return a16_; }

    const guint8* small() const { return a8_; }
    const guint16* large() const { return a16_; }

    bool using_8() const { return using_8_; }

    void set_using_8(bool setting);

    void clear();

  private:
    bool using_8_;
    guint8* a8_;
    guint16* a16_;
    guint n_;

    IndexVector(const IndexVector&);
    const IndexVector& operator=(const IndexVector&);
  };

  bool have_global_style_; // do we need to save styles on a point-by-point basis?
  guint global_style_; // if not, here is the global style
  IndexVector styles_; // if so, here is the vector of styles for each point.

  // We cache some superset of the styles actually used in the plot,
  // so we can return the largest-sized one in a reasonable amount of
  // time.
  set<guint> styles_in_use_;

  // We have to keep track of the max number of points 
  //  anyone has reserved (if we're using a global style,
  //  we don't necessarily have a vector of this size though)

  guint nreserved_;
};

class XyPair {
public:
  XyPair();
  ~XyPair();

  void set_x_data(Data* d);
  ScalarData* x_data();
  const ScalarData* x_data() const;

  void set_y_data(Data* d);
  ScalarData* y_data();
  const ScalarData* y_data() const;

  const SortedPair& sorted() const { return *pair_; }

  // Number of points - generally the smaller of xdata->size() and
  // ydata->size()
  guint npoints() const;

  XyPair* clone() const;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }

  class View : public ::View 
  {
  public:
    virtual void change_x_data(XyPair* pair, Data* old_data, Data* data) = 0;
    virtual void change_y_data(XyPair* pair, Data* old_data, Data* data) = 0;
  };


  class Model : public ::Model<XyPair::View*> {
  public:
    void x_data_changed(XyPair* pair, Data* old_data, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_x_data(pair,old_data,d);
	++i;
      }
      unlock_views();
    }
    void y_data_changed(XyPair* pair, Data* old_data, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_y_data(pair,old_data,d);
	++i;
      }
      unlock_views();
    }

  };
  
  Model pair_model;  

private:  
  ScalarData* x_data_;
  ScalarData* y_data_;

  SortedPair* pair_;

  RC rc_;

  guint npoints_;

  void set_data(ScalarData** data, Data* d);
  void grab_data(Data* d);
  void release_data(Data* d);
  void recalc_N_points();
};


class XyLineStyles {
public:
  XyLineStyles();
  ~XyLineStyles();

  XyLineStyles* clone() const;


  void set_style(double start, double stop, guint style_id);

  guint ref() { return rc_.ref(); }

  guint unref() { return rc_.unref(); }

  class View : public ::View 
  {
  public:
    virtual void change_styles(XyLineStyles* ls) = 0;
  };

  class Model : public ::Model<XyLineStyles::View*> {
  public:
    void styles_changed(XyLineStyles* m) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_styles(m);
	++i;
      }
      unlock_views();
    }

  };
  
  Model linestyles_model;  

private:
  RC rc_;

  

};

#endif


