
#include <gnome.h>

#include "action-manager.h"

static GtkWidget* app;

static gint delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data);
static void clicked_cb(GtkWidget* button, gpointer data);
static void action_cb(GnomeActionManager* am, const gchar* action_id, gpointer model, gpointer view, gpointer user_data);

int 
main(int argc, char** argv)
{
  GnomeActionManager* am;
  GtkWidget* menubar;
  GtkWidget* button;
  GtkWidget* popup;
  GtkWidget* toolbar;

  gnome_init("am-test", "0.0", argc, argv);

  app = gnome_app_new("am-test", "Testing Action Manager");

  gtk_signal_connect(GTK_OBJECT(app),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  am = gnome_action_manager_new();

  gnome_action_manager_add_group(am, "menubar");
  gnome_action_manager_set_info(am, "menubar",
                                NULL,
                                "Main Menubar",
                                "Contains the app's menus",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add_group(am, "foo_menu");
  gnome_action_manager_set_info(am, "foo_menu",
                                "Foo",
                                "The Foo Menu",
                                "Contains menu items related to Foo",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add_group(am, "bar_menu");
  gnome_action_manager_set_info(am, "bar_menu",
                                "Bar",
                                "The Bar Menu",
                                "Contains menu items related to Bar",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add_group(am, "history_menu");
  gnome_action_manager_set_info(am, "history_menu",
                                "History",
                                "Recent Commands",
                                "List of recently invoked actions",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add_group(am, "toolbar");
  gnome_action_manager_set_info(am, "toolbar",
                                "Toolbar",
                                "Toolbar",
                                "Tools",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add(am, "action_number_one", action_cb, NULL);
  gnome_action_manager_set_info(am, "action_number_one",
                                "Do Stuff",
                                "Do Stuff Command",
                                "Does some stuff",
                                NULL,
                                NULL,
                                GNOME_STOCK_PIXMAP_SAVE);                                
  gnome_action_manager_add(am, "action_number_two", action_cb, NULL);
  gnome_action_manager_set_info(am, "action_number_two",
                                "Do Other Stuff",
                                "Do Other Stuff Command",
                                "Does some other stuff",
                                NULL,
                                NULL,
                                GNOME_STOCK_PIXMAP_OPEN);                                
  gnome_action_manager_add(am, "desensitize_do_stuff", action_cb, NULL);
  gnome_action_manager_set_info(am, "desensitize_do_stuff",
                                "Desensitize Do Stuff",
                                "Desensitize Do Stuff Command",
                                "Desensitizes the Do Stuff menu item",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add(am, "sensitize_do_stuff", action_cb, NULL);
  gnome_action_manager_set_info(am, "sensitize_do_stuff",
                                "Sensitize Do Stuff",
                                "Sensitize Do Stuff Command",
                                "Sensitizes the Do Stuff menu item",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add(am, "add_new_action_to_foo", action_cb, NULL);
  gnome_action_manager_set_info(am, "add_new_action_to_foo",
                                "Add new action to Foo menu",
                                "Add New To Foo",
                                "Adds a new action to the foo menu",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_add(am, "new_action", action_cb, NULL);
  gnome_action_manager_set_info(am, "new_action",
                                "A New Action",
                                "New Action",
                                "A New Action Added at Runtime",
                                NULL,
                                NULL,
                                NULL);                                

  gnome_action_manager_append(am, "foo_menu", "action_number_one");
  gnome_action_manager_append(am, "foo_menu", "action_number_two");
  gnome_action_manager_append(am, "foo_menu", "desensitize_do_stuff");
  gnome_action_manager_append(am, "bar_menu", "desensitize_do_stuff");
  gnome_action_manager_append(am, "bar_menu", "sensitize_do_stuff");
  gnome_action_manager_append(am, "bar_menu", "action_number_one");
  gnome_action_manager_append(am, "bar_menu", "add_new_action_to_foo");
  gnome_action_manager_append(am, "menubar", "foo_menu");
  gnome_action_manager_append(am, "menubar", "bar_menu");
  gnome_action_manager_append(am, "menubar", "history_menu");
  gnome_action_manager_append(am, "foo_menu", "history_menu");

  gnome_action_manager_append(am, "toolbar", "action_number_one");
  gnome_action_manager_append(am, "toolbar", "action_number_two");

  gnome_action_manager_set_history_group(am, "history_menu", NULL);

  menubar = gnome_action_manager_make_menubar(am, "menubar", NULL, NULL);
  toolbar = gnome_action_manager_make_toolbar(am, "toolbar", NULL, NULL, 
                                              GTK_ORIENTATION_HORIZONTAL);

  gnome_app_set_menus(GNOME_APP(app), GTK_MENU_BAR(menubar));
  gnome_app_set_toolbar(GNOME_APP(app), GTK_TOOLBAR(toolbar));

  button = gtk_button_new_with_label("Right-click for popup");
  gnome_app_set_contents(GNOME_APP(app), button);

  popup = gnome_action_manager_make_menu(am, "foo_menu", NULL, NULL);

  gnome_popup_menu_attach(popup, button, NULL);

  gtk_window_set_default_size(GTK_WINDOW(app), -1, 100);

  gtk_widget_show_all(app);

  gtk_main();

  return 0;
}

static void 
action_cb(GnomeActionManager* am, const gchar* action_id, gpointer model, gpointer view, gpointer user_data)
{
  printf("Action: %s\n", action_id);

  if (strcmp(action_id, "desensitize_do_stuff") == 0)
    gnome_action_manager_set_sensitive(am, "action_number_one", FALSE);
  else if (strcmp(action_id, "sensitize_do_stuff") == 0)
    gnome_action_manager_set_sensitive(am, "action_number_one", TRUE);
  else if (strcmp(action_id, "add_new_action_to_foo") == 0)
    gnome_action_manager_append(am, "foo_menu", "new_action");
}

static void 
clicked_cb(GtkWidget* button, gpointer data)
{

}


static gint 
delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data)
{
  gtk_widget_destroy(app);
  gtk_main_quit();
  return FALSE;
}


