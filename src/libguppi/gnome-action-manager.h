/* gnome-ActionManager.h
 * Copyright (C) 1999  Havoc Pennington
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GNOME_ACTIONMANAGER_H__
#define __GNOME_ACTIONMANAGER_H__

#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GNOME_TYPE_ACTION_MANAGER			(gnome_action_manager_get_type ())
#define GNOME_ACTION_MANAGER(obj)			(GTK_CHECK_CAST ((obj), GNOME_TYPE_ACTION_MANAGER, GnomeActionManager))
#define GNOME_ACTION_MANAGER_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_ACTION_MANAGER, GnomeActionManagerClass))
#define GNOME_IS_ACTION_MANAGER(obj)			(GTK_CHECK_TYPE ((obj), GNOME_TYPE_ACTION_MANAGER))
#define GNOME_IS_ACTION_MANAGER_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GNOME_TYPE_ACTION_MANAGER))

struct _GnomeActionManagerPrivate;

typedef struct _GnomeActionManager       GnomeActionManager;
typedef struct _GnomeActionManagerClass  GnomeActionManagerClass;

struct _GnomeActionManager
{
  GtkObject parent;

  struct _GnomeActionManagerPrivate* priv;
};

struct _GnomeActionManagerClass
{
  GtkObjectClass parent_class;

  void (*action_invoked)	(GnomeActionManager *am, const gchar* action_id, gpointer model, gpointer view);

  void (*action_created)	(GnomeActionManager *am, const gchar* action_id);
  void (*action_destroyed)	(GnomeActionManager *am, const gchar* action_id);

  void (*group_created)	(GnomeActionManager *am, const gchar* group_id);
  void (*group_destroyed)	(GnomeActionManager *am, const gchar* group_id);

  /* The same signals are used for groups, subtrees, and radio groups,
     even though the signals are called action_ */
  void (*action_added)	        (GnomeActionManager *am, const gchar* group_id, const gchar* action_id);

  void (*action_removed)	(GnomeActionManager *am, const gchar* group_id, const gchar* action_id);

  void (*action_set_sensitive)  (GnomeActionManager *am, const gchar* action_id, gboolean sensitivity, gpointer model, gpointer view);

  void (*action_toggled)        (GnomeActionManager *am, const gchar* action_id, gboolean active, gpointer model, gpointer view);

  void (*action_name_changed)   (GnomeActionManager *am, const gchar* action_id, 
                                 const gchar* new_name);
};

/* You may wonder why these are needed; basically they allow you to write the
 * default handler for the action_invoked and action_toggled signals, because
 * you want to go ahead and perform your action before the signal handlers run.
 */

typedef void (* GnomeActionFunc) (GnomeActionManager* am, 
                                  const gchar* action_id, 
                                  gpointer model, 
                                  gpointer view, 
                                  gpointer user_data);

typedef void (* GnomeToggleFunc) (GnomeActionManager* am,
                                  const gchar* action_id,
                                  gboolean active,
                                  gpointer model, 
                                  gpointer view, 
                                  gpointer user_data);

GtkType    gnome_action_manager_get_type (void);
GnomeActionManager *gnome_action_manager_new      (void);

void       gnome_action_manager_create_action (GnomeActionManager* am,
                                               const gchar* action_id,
                                               GnomeActionFunc callback,
                                               gpointer user_data);

void       gnome_action_manager_create_toggle (GnomeActionManager* am,
                                               const gchar* action_id,
                                               GnomeToggleFunc callback,
                                               gpointer user_data);

void       gnome_action_manager_create_radio  (GnomeActionManager* am,
                                               const gchar* action_id,
                                               GnomeToggleFunc callback,
                                               gpointer user_data);

void       gnome_action_manager_create_group  (GnomeActionManager* am,
                                               const gchar* group_id);

void       gnome_action_manager_create_subtree (GnomeActionManager* am,
                                                const gchar* subtree_id);

void       gnome_action_manager_create_radiogroup (GnomeActionManager* am,
                                                   const gchar* radiogroup);

void       gnome_action_manager_destroy_action (GnomeActionManager* am,
                                                const gchar* action_id);

void       gnome_action_manager_destroy_group (GnomeActionManager* am,
                                               const gchar* group_id);

void       gnome_action_manager_append (GnomeActionManager* am,
                                        const gchar* group_id,
                                        const gchar* action_id);

void       gnome_action_manager_remove (GnomeActionManager* am,
                                        const gchar* group_id,
                                        const gchar* action_id);

void       gnome_action_manager_append_group (GnomeActionManager* am,
                                              const gchar* group_id,
                                              const gchar* subgroup_id);

void       gnome_action_manager_remove_group (GnomeActionManager* am,
                                              const gchar* group_id,
                                              const gchar* subgroup_id);

void       gnome_action_manager_invoke (GnomeActionManager* am,
                                        const gchar* action_id,
                                        gpointer model, 
                                        gpointer view);


void       gnome_action_manager_set_name (GnomeActionManager* am,
                                          const gchar* action_id,
                                          const gchar* name);

void       gnome_action_manager_set_group_name (GnomeActionManager* am,
                                                const gchar* group_id,
                                                const gchar* name);


GtkWidget* gnome_action_manager_create_menuitem(GnomeActionManager* am,
                                                const gchar* action_id,
                                                gpointer model, 
                                                gpointer view);

GtkWidget* gnome_action_manager_create_menubar(GnomeActionManager* am,
                                               const gchar* group_id,
                                               gpointer model,
                                               gpointer view);

GtkWidget* gnome_action_manager_create_menu(GnomeActionManager* am,
                                            const gchar* subtree_group_id,
                                            gpointer model,
                                            gpointer view);



/* Belongs in another module - useful for dynamic action IDs. must
   free return value. */

gchar*     gnome_create_random_unique_string (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GNOME_ACTIONMANAGER_H__ */


