// -*- C++ -*-

/* 
 * canvas-drawable.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvas-drawable.h"

#include <string.h> /* for memcpy() */
#include <math.h>
#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_affine.h>
#include <libart_lgpl/art_pixbuf.h>
#include <libart_lgpl/art_rgb_pixbuf_affine.h>

enum {
  ARG_0,
  ARG_X,
  ARG_Y,
  ARG_WIDTH,
  ARG_HEIGHT,
  ARG_ANCHOR
};


static void gnome_canvas_drawable_class_init (GnomeCanvasDrawableClass *klass);
static void gnome_canvas_drawable_init       (GnomeCanvasDrawable      *drawable);
static void gnome_canvas_drawable_destroy    (GtkObject             *object);
static void gnome_canvas_drawable_set_arg    (GtkObject             *object,
                                              GtkArg                *arg,
                                              guint                  arg_id);
static void gnome_canvas_drawable_get_arg    (GtkObject             *object,
                                              GtkArg                *arg,
                                              guint                  arg_id);

static void   gnome_canvas_drawable_update      (GnomeCanvasItem *item, 
                                                 double *affine, 
                                                 ArtSVP *clip_path, 
                                                 int flags);
static void   gnome_canvas_drawable_realize     (GnomeCanvasItem *item);
static void   gnome_canvas_drawable_unrealize   (GnomeCanvasItem *item);
static void   gnome_canvas_drawable_draw        (GnomeCanvasItem *item, GdkDrawable *drawable,
                                                 int x, int y, int width, int height);
static double gnome_canvas_drawable_point       (GnomeCanvasItem *item, 
                                                 double x, double y,
                                                 int cx, int cy, 
                                                 GnomeCanvasItem **actual_item);
static void   gnome_canvas_drawable_translate   (GnomeCanvasItem *item, 
                                                 double dx, double dy);
static void   gnome_canvas_drawable_bounds      (GnomeCanvasItem *item, 
                                                 double *x1, double *y1, 
                                                 double *x2, double *y2);
static void   gnome_canvas_drawable_render      (GnomeCanvasItem *item, 
                                                 GnomeCanvasBuf *buf);


static GnomeCanvasItemClass *parent_class;


GtkType
gnome_canvas_drawable_get_type (void)
{
  static GtkType drawable_type = 0;

  if (!drawable_type) {
    GtkTypeInfo drawable_info = {
      "GnomeCanvasDrawable",
      sizeof (GnomeCanvasDrawable),
      sizeof (GnomeCanvasDrawableClass),
      (GtkClassInitFunc) gnome_canvas_drawable_class_init,
      (GtkObjectInitFunc) gnome_canvas_drawable_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    drawable_type = gtk_type_unique (gnome_canvas_item_get_type (), &drawable_info);
  }

  return drawable_type;
}

static void
gnome_canvas_drawable_class_init (GnomeCanvasDrawableClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = (GnomeCanvasItemClass *)gtk_type_class (gnome_canvas_item_get_type ());

  gtk_object_add_arg_type ("GnomeCanvasDrawable::x", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("GnomeCanvasDrawable::y", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("GnomeCanvasDrawable::width", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_WIDTH);
  gtk_object_add_arg_type ("GnomeCanvasDrawable::height", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_HEIGHT);
  gtk_object_add_arg_type ("GnomeCanvasDrawable::anchor", GTK_TYPE_ANCHOR_TYPE, GTK_ARG_READWRITE, ARG_ANCHOR);

  object_class->destroy = gnome_canvas_drawable_destroy;
  object_class->set_arg = gnome_canvas_drawable_set_arg;
  object_class->get_arg = gnome_canvas_drawable_get_arg;

  item_class->update = gnome_canvas_drawable_update;
  item_class->realize = gnome_canvas_drawable_realize;
  item_class->unrealize = gnome_canvas_drawable_unrealize;
  item_class->draw = gnome_canvas_drawable_draw;
  item_class->point = gnome_canvas_drawable_point;
  item_class->translate = gnome_canvas_drawable_translate;
  item_class->bounds = gnome_canvas_drawable_bounds;
  item_class->render = NULL; // gnome_canvas_drawable_render;
}

static void
gnome_canvas_drawable_init (GnomeCanvasDrawable *drawable)
{
  drawable->x = 0.0;
  drawable->y = 0.0;
  drawable->width = 0.0;
  drawable->height = 0.0;
  drawable->anchor = GTK_ANCHOR_CENTER;
  drawable->gc = NULL;
}

static void
free_pixmap_and_mask (GnomeCanvasDrawable *drawable)
{
  if (drawable->buffer)
    gdk_pixmap_unref(drawable->buffer);

  drawable->buffer = NULL;
  drawable->cwidth = 0;
  drawable->cheight = 0;
}

static void
gnome_canvas_drawable_destroy (GtkObject *object)
{
  GnomeCanvasDrawable *drawable;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_DRAWABLE (object));

  drawable = GNOME_CANVAS_DRAWABLE (object);

  free_pixmap_and_mask (drawable);

  if (drawable->pixbuf != NULL)
    art_pixbuf_free (drawable->pixbuf);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/* Get's the drawable bounds expressed as item-relative coordinates. */
static void
get_bounds_item_relative (GnomeCanvasDrawable *drawable, 
                          double *px1, double *py1, 
                          double *px2, double *py2)
{
  GnomeCanvasItem *item;
  double x, y;

  item = GNOME_CANVAS_ITEM (drawable);

  /* Get item coordinates */

  x = drawable->x;
  y = drawable->y;

  /* Anchor drawable */

  switch (drawable->anchor) {
  case GTK_ANCHOR_NW:
  case GTK_ANCHOR_W:
  case GTK_ANCHOR_SW:
    break;

  case GTK_ANCHOR_N:
  case GTK_ANCHOR_CENTER:
  case GTK_ANCHOR_S:
    x -= drawable->width / 2;
    break;

  case GTK_ANCHOR_NE:
  case GTK_ANCHOR_E:
  case GTK_ANCHOR_SE:
    x -= drawable->width;
    break;
  }

  switch (drawable->anchor) {
  case GTK_ANCHOR_NW:
  case GTK_ANCHOR_N:
  case GTK_ANCHOR_NE:
    break;

  case GTK_ANCHOR_W:
  case GTK_ANCHOR_CENTER:
  case GTK_ANCHOR_E:
    y -= drawable->height / 2;
    break;

  case GTK_ANCHOR_SW:
  case GTK_ANCHOR_S:
  case GTK_ANCHOR_SE:
    y -= drawable->height;
    break;
  }

  /* Bounds */

  *px1 = x;
  *py1 = y;
  *px2 = x + drawable->width;
  *py2 = y + drawable->height;
}

static void
get_bounds (GnomeCanvasDrawable *drawable, 
            double *px1, double *py1, 
            double *px2, double *py2)
{
  GnomeCanvasItem *item;
  double wx, wy;
  double i2c[6];
  ArtDRect i_bbox, c_bbox;

  item = GNOME_CANVAS_ITEM (drawable);

  gnome_canvas_item_i2c_affine (item, i2c);

  get_bounds_item_relative (drawable, &i_bbox.x0, &i_bbox.y0, &i_bbox.x1, &i_bbox.y1);
  art_drect_affine_transform (&c_bbox, &i_bbox, i2c);

  /* add a fudge factor */
  *px1 = c_bbox.x0 - 1;
  *py1 = c_bbox.y0 - 1;
  *px2 = c_bbox.x1 + 1;
  *py2 = c_bbox.y1 + 1;
}

static void
gnome_canvas_drawable_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasDrawable *drawable;
  int update;
  int calc_bounds;

  item = GNOME_CANVAS_ITEM (object);
  drawable = GNOME_CANVAS_DRAWABLE (object);

  update = FALSE;
  calc_bounds = FALSE;

  switch (arg_id) {

  case ARG_X:
    drawable->x = GTK_VALUE_DOUBLE (*arg);
    calc_bounds = TRUE;
    break;

  case ARG_Y:
    drawable->y = GTK_VALUE_DOUBLE (*arg);
    calc_bounds = TRUE;
    break;

  case ARG_WIDTH:
    drawable->width = fabs (GTK_VALUE_DOUBLE (*arg));
    update = TRUE;
    break;

  case ARG_HEIGHT:
    drawable->height = fabs (GTK_VALUE_DOUBLE (*arg));
    update = TRUE;
    break;

  case ARG_ANCHOR:
    drawable->anchor = (GtkAnchorType)GTK_VALUE_ENUM (*arg);
    update = TRUE;
    break;

  default:
    break;
  }

  if (update)
    gnome_canvas_item_request_update (item);
}

static void
gnome_canvas_drawable_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (object);

  switch (arg_id) {

  case ARG_X:
    GTK_VALUE_DOUBLE (*arg) = drawable->x;
    break;

  case ARG_Y:
    GTK_VALUE_DOUBLE (*arg) = drawable->y;
    break;

  case ARG_WIDTH:
    drawable->recreate_pixmap = TRUE;
    GTK_VALUE_DOUBLE (*arg) = drawable->width;
    break;

  case ARG_HEIGHT:
    drawable->recreate_pixmap = TRUE;
    GTK_VALUE_DOUBLE (*arg) = drawable->height;
    break;

  case ARG_ANCHOR:
    GTK_VALUE_ENUM (*arg) = drawable->anchor;
    break;

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void
gnome_canvas_drawable_update (GnomeCanvasItem *item, double *affine, 
                              ArtSVP *clip_path, int flags)
{
  GnomeCanvasDrawable *drawable;
  double x1, y1, x2, y2;
  ArtDRect i_bbox, c_bbox;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  if (parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);

  if (drawable->recreate_pixmap) 
    {
      free_pixmap_and_mask(drawable);
      /* only works for non-rotated, non-skewed transforms */
      drawable->cwidth = (int) (drawable->width * affine[0] + 0.5);
      drawable->cheight = (int) (drawable->height * affine[3] + 0.5);

      if (drawable->cwidth > 0 && drawable->cheight > 0)
        {
          drawable->buffer = gdk_pixmap_new(drawable->item.canvas->layout.bin_window,
                                            drawable->cwidth,
                                            drawable->cheight,
                                            gdk_rgb_get_visual()->depth);
          gdk_draw_rectangle(drawable->buffer, drawable->gc,
                             TRUE, 0, 0, drawable->cwidth, drawable->cheight);
        }
    }


  get_bounds_item_relative (drawable, &i_bbox.x0, &i_bbox.y0, &i_bbox.x1, &i_bbox.y1);
  art_drect_affine_transform (&c_bbox, &i_bbox, affine);

  /* these values only make sense in the non-rotated, non-skewed case */
  drawable->cx = (int) c_bbox.x0;
  drawable->cy = (int) c_bbox.y0;

  /* add a fudge factor */
  c_bbox.x0--;
  c_bbox.y0--;
  c_bbox.x1++;
  c_bbox.y1++;

  gnome_canvas_update_bbox (item, 
                            (int) c_bbox.x0, (int) c_bbox.y0, 
                            (int) c_bbox.x1, (int) c_bbox.y1);

  drawable->affine[0] = affine[0];
  drawable->affine[1] = affine[1];
  drawable->affine[2] = affine[2];
  drawable->affine[3] = affine[3];
  drawable->affine[4] = i_bbox.x0 * affine[0] + i_bbox.y0 * affine[2] + affine[4];
  drawable->affine[5] = i_bbox.x0 * affine[1] + i_bbox.y0 * affine[3] + affine[5];
}

static void
gnome_canvas_drawable_realize (GnomeCanvasItem *item)
{
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  if (parent_class->realize)
    (* parent_class->realize) (item);

  //  if (!item->canvas->aa)
    {
      drawable->gc = gdk_gc_new (item->canvas->layout.bin_window);
      gdk_gc_copy(drawable->gc, GTK_WIDGET(item->canvas)->style->white_gc);
    } 
}

static void
gnome_canvas_drawable_unrealize (GnomeCanvasItem *item)
{
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  if (drawable->gc)
    gdk_gc_unref (drawable->gc);

  free_pixmap_and_mask(drawable);

  if (parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
gnome_canvas_drawable_draw (GnomeCanvasItem *item, GdkDrawable *target,
                            int x, int y, int width, int height)
{
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  if (!drawable->buffer)
    return;

  gdk_draw_pixmap (target,
                   drawable->gc,
                   drawable->buffer,
                   0, 0,
                   drawable->cx - x,
                   drawable->cy - y,
                   drawable->cwidth,
                   drawable->cheight);
}

static double
gnome_canvas_drawable_point (GnomeCanvasItem *item, double x, double y,
                             int cx, int cy, GnomeCanvasItem **actual_item)
{
  GnomeCanvasDrawable *drawable;
  int x1, y1, x2, y2;
  int dx, dy;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  *actual_item = item;

  x1 = drawable->cx - item->canvas->close_enough;
  y1 = drawable->cy - item->canvas->close_enough;
  x2 = drawable->cx + drawable->cwidth - 1 + item->canvas->close_enough;
  y2 = drawable->cy + drawable->cheight - 1 + item->canvas->close_enough;

  if ((cx >= x1) && (cy >= y1) && (cx <= x2) && (cy <= y2))
    return 0.0;

  /* Point is outside drawable */

  x1 += item->canvas->close_enough;
  y1 += item->canvas->close_enough;
  x2 -= item->canvas->close_enough;
  y2 -= item->canvas->close_enough;

  if (cx < x1)
    dx = x1 - cx;
  else if (cx > x2)
    dx = cx - x2;
  else
    dx = 0;

  if (cy < y1)
    dy = y1 - cy;
  else if (cy > y2)
    dy = cy - y2;
  else
    dy = 0;

  return sqrt (dx * dx + dy * dy) / item->canvas->pixels_per_unit;
}

static void
gnome_canvas_drawable_translate (GnomeCanvasItem *item, double dx, double dy)
{
#ifdef OLD_XFORM
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  drawable->x += dx;
  drawable->y += dy;

  recalc_bounds (drawable);
#endif
}

static void
gnome_canvas_drawable_bounds (GnomeCanvasItem *item, 
                              double *x1, double *y1, 
                              double *x2, double *y2)
{
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  *x1 = drawable->x;
  *y1 = drawable->y;

  switch (drawable->anchor) {
  case GTK_ANCHOR_NW:
  case GTK_ANCHOR_W:
  case GTK_ANCHOR_SW:
    break;

  case GTK_ANCHOR_N:
  case GTK_ANCHOR_CENTER:
  case GTK_ANCHOR_S:
    *x1 -= drawable->width / 2.0;
    break;

  case GTK_ANCHOR_NE:
  case GTK_ANCHOR_E:
  case GTK_ANCHOR_SE:
    *x1 -= drawable->width;
    break;
  }

  switch (drawable->anchor) {
  case GTK_ANCHOR_NW:
  case GTK_ANCHOR_N:
  case GTK_ANCHOR_NE:
    break;

  case GTK_ANCHOR_W:
  case GTK_ANCHOR_CENTER:
  case GTK_ANCHOR_E:
    *y1 -= drawable->height / 2.0;
    break;

  case GTK_ANCHOR_SW:
  case GTK_ANCHOR_S:
  case GTK_ANCHOR_SE:
    *y1 -= drawable->height;
    break;
  }

  *x2 = *x1 + drawable->width;
  *y2 = *y1 + drawable->height;
}

#if 0
static void
gnome_canvas_drawable_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf)
{
  GnomeCanvasDrawable *drawable;

  drawable = GNOME_CANVAS_DRAWABLE (item);

  gnome_canvas_buf_ensure_buf (buf);

#ifdef VERBOSE
  {
    char str[128];
    art_affine_to_string (str, drawable->affine);
    g_print ("gnome_canvas_drawable_render %s\n", str);
  }
#endif

  art_rgb_pixbuf_affine (buf->buf,
                         buf->rect.x0, buf->rect.y0, buf->rect.x1, buf->rect.y1,
                         buf->buf_rowstride,
                         drawable->pixbuf,
                         drawable->affine,
                         ART_FILTER_NEAREST, NULL);

  buf->is_bg = 0;
}

/* This creates a new pixbuf from the imlib drawable. */
static ArtPixBuf *
pixbuf_from_imlib_drawable (GdkImlibDrawable *im)
{
  ArtPixBuf *pixbuf;
  art_u8 *pixels;
  int width, height, rowstride;
  int x, y;
  unsigned char *p_src, *p_alpha;
  art_u8 *p_dst;
  art_u8 r, g, b, alpha;
  art_u8 xr, xg, xb;

  if (im->alpha_data) {
    /* drawable has alpha data (not presently implemented in imlib as
       of 15 Dec 1998, but should happen soon. */
    width = im->rgb_width;
    height = im->rgb_height;
    rowstride = width * 4;
    pixels = art_alloc (rowstride * height);
    p_src = im->rgb_data;
    p_alpha = im->alpha_data;
    p_dst = pixels;
    for (y = 0; y < height; y++)
      for (x = 0; x < width; x++) {
        r = p_src[0];
        g = p_src[1];
        b = p_src[2];
        alpha = p_alpha[0];

        p_dst[0] = r;
        p_dst[1] = g;
        p_dst[2] = b;
        p_dst[3] = alpha;

        p_src += 3;
        p_alpha += 1;
        p_dst += 4;
      }
    return art_pixbuf_new_rgba (pixels, width, height, rowstride);
  } else if (im->shape_color.r >= 0 && im->shape_color.g >= 0 && im->shape_color.b >= 0) {
    /* drawable has one transparent color */
    width = im->rgb_width;
    height = im->rgb_height;
    rowstride = width * 4;
    pixels = art_alloc (rowstride * height);
    p_src = im->rgb_data;
    p_dst = pixels;
    xr = im->shape_color.r;
    xg = im->shape_color.g;
    xb = im->shape_color.b;
    for (y = 0; y < height; y++)
      for (x = 0; x < width; x++) {
        r = p_src[0];
        g = p_src[1];
        b = p_src[2];
        if (r == xr && g == xg && b == xb) {
          ((art_u32 *)p_dst)[0] = 0;
        } else {
          p_dst[0] = r;
          p_dst[1] = g;
          p_dst[2] = b;
          p_dst[3] = 255;
        }
        p_src += 3;
        p_dst += 4;
      }
    return art_pixbuf_new_rgba (pixels, width, height, rowstride);
  } else {
    /* drawable is solid rgb */
    width = im->rgb_width;
    height = im->rgb_height;
    rowstride = (width * 3 + 3) & -4;
    pixels = art_alloc (rowstride * height);
    p_src = im->rgb_data;
    p_dst = pixels;
    for (y = 0; y < height; y++) {
      memcpy (p_dst, p_src, width * 3);
      p_src += width * 3;
      p_dst += rowstride;
    }
    return art_pixbuf_new_rgb (pixels, width, height, rowstride);
  }
}
#endif

GdkPixmap* 
gnome_canvas_drawable_get_drawable(GnomeCanvasDrawable* d)
{
  g_return_val_if_fail(d != 0, 0);
  g_return_val_if_fail(GNOME_IS_CANVAS_DRAWABLE(d), 0);

  if (d->recreate_pixmap) return 0;

  return d->buffer;
}
