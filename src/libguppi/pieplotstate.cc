// -*- C++ -*-

/* 
 * pieplotstate.cc 
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "pieplotstate.h"

#include "vfont.h"
#include "scalardata.h"
#include "labeldata.h"

#include <math.h>

// TODO: this is just a work around for colors.
// how to get rgb values for strings such as "red"
// (similiar to command line tool "showrgb")?
// There must be some function already, but I didn't find it ...

#define RED	250,0,0
#define	YELLOW	255,255,0
#define BLUE	0,0,255
#define GREEN	0,255,0
#define CYAN	0,255,255
#define MAGENTA	255,0,255
#define ORANGE	255,165,0
#define KHAKI	240,230,140
#define SIENNA	160,82,45
#define CADET_BLUE 95,158,160

#define MAX_COLOR	10
Color piechart_colortable[] = { Color(RED), Color(YELLOW), Color(BLUE),
	Color(GREEN), Color(CYAN), Color(MAGENTA), Color(ORANGE),
	Color(KHAKI), Color(SIENNA), Color(CADET_BLUE) };

PiePlotState::PiePlotState(DataStore * ds)
  : data_(0), store_(ds), width_(100.0), height_(100.0),
    background_(255,255,255) 
{
  recalc_layout();
  g_return_if_fail(store_ != 0);
}

PiePlotState::~PiePlotState()
{
  release_data();
}

const string& 
PiePlotState::name() const
{
  return name_;
}

void 
PiePlotState::set_name(const string & name)
{
  name_ = name;
  state_model.name_changed(this, name);
}

bool 
PiePlotState::name_from_data() const
{
  return set_name_from_data_;
}

void 
PiePlotState::set_name_from_data(bool setting)
{
  set_name_from_data_ = setting;
  if (setting)
    update_name_from_data();
}

void 
PiePlotState::update_name_from_data()
{
  if (!set_name_from_data_)
    return;

  if (data_ == 0)
    {
      name_ = _("No data to chart");
    } else {
      const string & dname = data_ ? data_->name() : _("(no data)");

      guint bufsize = dname.size() + 50;
      char* buf = new char[bufsize];

      g_snprintf(buf, bufsize, _("Pie Chart of `%s'"), dname.c_str());
 
      set_name(buf);

      delete [] buf;
    }
}

void 
PiePlotState::grab_data(Data* d)
{
  if (d != 0)
    {
      data_ = d->cast_to_scalar();
    } else {
      data_ = 0;
    }

  if (data_ != 0)
    {
      data_->ref();

      data_->data_model.add_view(this);
    }      
}

void 
PiePlotState::release_data()
{
  if (data_ != 0)
    {
      data_->data_model.remove_view(this);
      if (data_->unref() == 0) delete data_;
      data_ = 0;
    }
}


void 
PiePlotState::grab_labels(Data* d)
{
  if (d != 0)
    {
      labels_ = d->cast_to_label();
    }
  else 
    {
      labels_ = 0;
    }

  if (labels_ != 0)
    {
      labels_->ref();
      
      labels_->data_model.add_view(this);
    } 
}

void 
PiePlotState::release_labels()
{
  if (labels_ != 0)
    {
      labels_->data_model.remove_view(this);
      if (labels_->unref() == 0) delete labels_;
      labels_ = 0;
    }
}

void 
PiePlotState::set_data(Data* d)
{
  if (data_ == (ScalarData*)d) return;

  release_data();

  grab_data(d);

  rebuild_slices();

  update_name_from_data();

  state_model.data_changed(this,data_);
}

ScalarData* 
PiePlotState::data()
{
  return data_;
}

void 
PiePlotState::set_labels(Data* d)
{
  g_warning("%s not implemented", __FUNCTION__);
}

LabelData* 
PiePlotState::labels()
{
  g_warning("%s not implemented", __FUNCTION__);

  return 0;
}

void 
PiePlotState::rebuild_slices()
{
  if (data_ == 0)
    {
      slices_.clear();
      state_model.slices_changed(this);
      return;
    }

  gsize N = data_->size();
  if (N == 0)
    {
      slices_.clear();
      state_model.slices_changed(this);
      return;
    }

  // Max number of slices is 100. If there are more datapoints, we
  // just assume the user didn't mean to do that and ignore the extra
  // data.
  if (N > 100)
    N = 100;

  g_assert (N > 0);

  const double * scalars = data_->scalars();
  g_return_if_fail(scalars != 0);

  // calculate sum
  double sum = 0.0;
  for (gsize i = 0; i < N; i++)
    {
      sum += ABS(scalars[i]); // consider negative scalars as positive
    }

  g_assert (width_ > 0.0);
  g_assert (height_ > 0.0);

  double centerX = width_ / 2;
  double centerY = height_ / 2;
  double radius = MIN(width_/2,height_/2) / 2;	// FIXME: FIXED radius for the moment

  double angle = 0.0;

  gsize j = 0;
  while (j < N)
    {
      g_assert(j <= slices_.size());

      if (j == slices_.size())
        {
          slices_.push_back(Slice(this));
          g_assert(j < slices_.size());
        }

      slices_[j].set_index(j);
      slices_[j].set_posX(centerX);
      slices_[j].set_posY(centerY);
      slices_[j].set_radius(radius);
      slices_[j].set_start(angle);
      slices_[j].set_color(piechart_colortable[j % MAX_COLOR]);

      double distance = (ABS(scalars[j]) / sum) * 360.0;
      slices_[j].set_width(distance);

      // calculate default label position

      // think we need this only here
#ifndef M_PI
#define	M_PI		3.14159265358979323846
#endif
#ifndef RAD
#define RAD(angle) (((angle)/180.)*M_PI)
#endif

      // first approach for center pos of label:
      double lX = (slices_[j]).posX() + (slices_[j]).labelRadius() * cos(RAD((slices_[j]).labelPos())); 
      double lY = (slices_[j]).posY() - (slices_[j]).labelRadius() * sin(RAD((slices_[j]).labelPos()));

      // make a label (currently the scalar and the %age)
      double per_cent = (double)floor((distance / 3.6) * 100.0);
      per_cent = (per_cent == 0.0 ? 0.0 : (per_cent / 100.0));
      gchar buf[128];
      if (scalars[j] < 0)
        g_snprintf(buf,128,"abs(%g) (%g%%)",scalars[j], per_cent);
      else
        g_snprintf(buf,128,"%g (%g%%)",scalars[j], per_cent);

      // find out width and height of label
      Frontend * fe = guppi_frontend();
      g_assert(fe != 0);
      VFont * vfont = fe->default_font();
      double lh = 0, lw = 0; // these are the defaults when no font is found
      if (vfont != 0) {
        lh = vfont->font_height();
        lw = vfont->string_width((const gchar *)buf);
      }

      // horizontal correction
      if ((slices_[j]).labelPos() < 90 || (slices_[j]).labelPos() > 270)
        lX += (lw > 0 ? (lw/2.0) : 0.0); // make a left align
      else
        lX -= (lw > 0 ? (lw/2.0) : 0.0); // make a right align

      // vertical correction
      if ((slices_[j]).labelPos() < 180)
        lY += (lh > 0 ? (lh/2.0) : 0.0); // make a bottom align
      else
        lX -= (lh > 0 ? (lh/2.0) : 0.0); // make a top align

      // thats it - set default label position
      slices_[j].set_labelX(lX);
      slices_[j].set_labelY(lY);

      slices_[j].set_label((const gchar *)buf); // set label

      ++j;
      angle += distance;
    }

  // Clean up extra pies
  if (slices_.size() > j)
    {
      vector<Slice>::iterator erasefrom = slices_.begin() + j;
      slices_.erase(erasefrom, slices_.end());
    }

  g_assert(j == slices_.size());

  state_model.slices_changed(this);
}

void 
PiePlotState::set_width(double w)
{
  if (w <= PlotUtil::EPSILON) return; // simply refuse to do this
  width_ = w;

  recalc_layout();
}

void 
PiePlotState::set_height(double h)
{
  if (h <= PlotUtil::EPSILON) return; // simply refuse to do this
  height_ = h;

  recalc_layout();
}

const Rectangle & 
PiePlotState::plot_rect() const
{
  return plot_rect_;
}

void 
PiePlotState::recalc_layout()
{
  plot_rect_.set(0.0, 0.0, width_, height_);

  rebuild_slices();

  state_model.size_changed(this, width_, height_);
}

void 
PiePlotState::size_request(double* w, double* h)
{
  g_return_if_fail(w != 0);
  g_return_if_fail(h != 0);
  // For now, always request the current size
  *w = width_;
  *h = height_;
}

/// Data::View


void 
PiePlotState::change_values(Data* d) 
{
  g_return_if_fail((ScalarData*)d == data_ || (LabelData*)d == labels_);
  rebuild_slices();
}

void 
PiePlotState::change_values(Data* d, const vector<guint> & which) 
{
  change_values(d);
}

void 
PiePlotState::change_name(Data* d, const string & name) 
{
  // Nothing
}

void 
PiePlotState::destroy_model()
{
  // Nothing
}
