// -*- C++ -*-

/* 
 * plotutils.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTUTILS_H
#define GUPPI_PLOTUTILS_H

#include <vector>
#include <string>
#include <math.h>
#include <glib.h>

class VFont;

// Eventually some of this will probably end up in a library, but for
// now it's easier to work on if it's here. Anyway don't name them in
// Guppi-specific ways and minimize the glib. No Gtk/Gnome allowed.

// Misc. utility function namespace
class PlotUtil {
public:
  // Return an array of nice round numbers approximately between min and max.
  // If range_locked == false, possibly adjust min and max a little.
  static void round_numbers (int goal, // desired number of values
                             double* min, double* max, 
                             bool range_locked, // whether min and max can be adjusted
                             const vector<int> & divisors,
                             vector<double> & results);
                   
  // Sorts xsorted keeping the ordered pairs together
  static void sort_axes(double* xsorted, double* ysorted, unsigned int N);

  // Sorts sorted keeping the indices matched up (internally implemented
  //  as a template, don't worry, I just don't like header file clutter)
  static void sort_indices(double* sorted, unsigned int* indices, unsigned int N);

  // xvalues *must* be sorted! yvalues need not be, obviously both
  // can't be. There is no reason xvalues has to actually be the X 
  // axis, if your Y axis is already sorted.
  // Values in the returned start, stop range will be within [xmin,xmax]
  // for xvalues, but yvalues in the range are not necessarily in [ymin,ymax]
  // (The Y arguments are so we could eventually sort yvalues and use it to 
  //  further narrow the range, if we felt like it)
  // Both start and stop set to N if there are no items in the range
  // stop is set to one past, like STL iterators, so the range is [start,stop)
  static void find_range(const double* xvalues, const double* yvalues,
                         unsigned int N,
                         // FIXME reorder these to have both mins, both maxes
                         double xmin, double xmax, double ymin, double ymax,
                         unsigned int* start, unsigned int* stop);

  // Return first element greater than or equal to target, using binary search
  // Return N if none
  static unsigned int first_ge(const double* values, unsigned int N, double target);

  static void canonicalize_rectangle(double* x1, double* y1,
                                     double* x2, double* y2);

  static void canonicalize_rectangle(int* x1, int* y1,
                                     int* x2, int* y2);

  // A generic "really small number" for use with floats
  static const double EPSILON;

  // Axis positions (don't change order; they are in order of defaultness)
  typedef enum {
    SOUTH,
    NORTH,
    WEST,
    EAST,
    PositionTypeEnd
  } PositionType;

  static bool horizontal(PositionType pos) { 
    return pos == SOUTH || pos == NORTH; 
  }

  static bool vertical(PositionType pos) { 
    return pos == EAST || pos == WEST; 
  }

  static const gchar* position_string(PositionType pos);

  // Portable hypot() (sqrt(x*x + y*y))
  static double hypot(double x, double y);
};


///////////// Transform data values to some sort of display values
///////////// Unlike Affine, it is one-dimensional, and has no rotation.

class Transform {
public:
  Transform() : s0_(0), s1_(1), c0_(0), c1_(1),
    pixels_per_unit_(0), b_(0)
    { recalc_transformation_coefficients(); }

  void set_screen_bounds(double s0, double s1) {
    s0_ = s0;
    s1_ = s1;
    recalc_transformation_coefficients();
  }

  void set_range_bounds(double c0, double c1) {
    c0_ = c0;
    c1_ = c1;
    recalc_transformation_coefficients();
  }

  // Transform coordinates => pixels
  double transform(double c) const { return (double)(pixels_per_unit_*c+b_); }

  // Transform pixels => coordinates
  double inverse(double s) const { return (s-b_)/pixels_per_unit_; }

  double pixels_per_unit() const { return pixels_per_unit_; }
  double units_per_pixel() const { return 1/pixels_per_unit_; }

  // amount of translation going from coordinates to pixels
  double translation() const { return b_; }

  double screen_size() const { return fabs(s1_ -s0_); }
  double range_size() const { return fabs(c1_-c0_); }

  void screen_bounds(double* start, double* stop) const {
    *start = s0_;
    *stop = s1_;
  }

  void range_bounds(double* start, double* stop) const {
    *start = c0_;
    *stop = c1_;
  }

  void shift_screen_bounds(double amount) {
    s0_ += amount;
    s1_ += amount;
    recalc_transformation_coefficients();
  }

private:
  void recalc_transformation_coefficients() {
    // avoid FPU exceptions and other insanity if user is dumb
    // Yes this basically sux
    if ( fabs(c0_-c1_) < PlotUtil::EPSILON ) {
      c1_ += PlotUtil::EPSILON*5; // *5 since c1_ could be < c0_
    }
    pixels_per_unit_ = (s1_-s0_)/(c1_-c0_);
    b_ = -c0_ * pixels_per_unit_ + s0_;
  }
    
  double s0_, s1_;    // screen/pixel bounds
  double c0_, c1_; // coordinate system range bounds

  double pixels_per_unit_;
  double b_; 
};


//////////// Affine transformation class: Derived from Raph Levien's libart_lgpl
class Affine {
public:

  struct Point {
    Point(double xv, double yv) : x(xv), y(yv) {}
    Point() : x(0.0), y(0.0) {}
    double x;
    double y;
  };

  Affine();
  Affine(const double a[6]);
  Affine(double,double,double,double,double,double);
  Affine(const Affine&);
  ~Affine();
  const Affine& operator=(const Affine&);
  
  void set_identity(); 
  
  void transform(Point& p) const { 
    const double xtmp = p.x;
    p.x = a[0]*p.x + a[2]*p.y + a[4]; 
    p.y = a[1]*xtmp + a[3]*p.y + a[5]; 
  }

  Affine* new_inverted() const;


  const double* raw() const { return a; }

  void set(const double src[6]);

  // In-place transformations
  
  void compose(const Affine& with);
  void compose(const double with[6]);
  void compose(const Transform & xtrans, const Transform & ytrans);

  void x_compose(const Transform & xtrans);
  void y_compose(const Transform & ytrans);

  // These compose the scale/rotate/translate with the 
  //  current scale/rotate/translate, they don't set them
  void scale(double sx, double sy);

  void rotate(double theta);

  void translate(double dx, double dy);
  
  // This allows optimization in many cases; 
  //  it isn't done automatically, since we'd then 
  //  need to check it inside of inner loops.
  // It's totally optional this way.
  bool translation_only() const {
    // Return true if the non-translate parts are identity
    return (a[0] == 1.0 && 
            a[1] == 0.0 &&
            a[2] == 0.0 && 
            a[3] == 1.0);
  }

  static const double EPSILON;

  // Whether the affine transforms a grid-aligned rectangle to 
  //  another grid-aligned rectangle.
  bool rectilinear () const;

  // If you check translation_only() you can use this
  void translate_transform(Point & p) const {
    p.x += a[4];
    p.y += a[5];
  }

  // Get the post-transformation bounds; args are "in-out"
  // Input values: a rectangle
  // Output: The smallest rectangle in which the transformed original 
  //         rectangle can be inscribed.
  void get_transformed_bounds(double* xmin, double* ymin,
                              double* xmax, double* ymax) const;

  double x_translation() const { return a[4]; }
  double y_translation() const { return a[5]; }

private:
  double a[6];

};



///////////// Rectangle

// Width and height are *added* to X and Y to get X2,Y2
class Rectangle {
public:
  Rectangle(double x, double y, double w, double h) 
    : x_(x), y_(y), w_(w), h_(h)
    {}

  Rectangle()
    : x_(0), y_(0), w_(0), h_(0)
    {}

  // Default copy constructor IS USED and SHOULD WORK

  double x() const { return x_; }
  double y() const { return y_; }
  double width() const { return w_; }
  double height() const { return h_; }
  double x2() const { return x_ + w_; }
  double y2() const { return y_ + h_; }

  void set(double x, double y, double w, double h) 
  {
    g_return_if_fail(w >= 0.0);
    g_return_if_fail(h >= 0.0);
    x_ = x;
    y_ = y;
    w_ = MAX(w,0.0);
    h_ = MAX(h,0.0);
  }
  
  void set_x(double v) { x_ = v; }
  void set_y(double v) { y_ = v; }
  void set_width(double v) { w_ = MAX(v, 0.0); }
  void set_height(double v) { h_ = MAX(v, 0.0); }

  void translate(double dx, double dy) {
    x_ += dx;
    y_ += dy;
  }

  bool inside(double x, double y)
    {
      return (x >= x_ && x <= (x_+w_) && 
              y >= y_ && y <= (y_+h_));
    }

  // Move all edges in or out by inc.  Correctly stops at 0 x 0
  // rectangle if inc is negative and less than w or h
  void move_edges(double inc);

private:
  double x_;
  double y_;
  double w_;
  double h_;
};

//////////////////// Color

class Color {
public:
  // r,g,b,a are in the range [0,255]
  // a == 255 means fully opaque, a == 0 means invisible
  Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255)
    : r_(r), g_(g), b_(b), a_(a)
    {
      
    }

  Color() 
    : r_(0), g_(0), b_(0), a_(255)
    {

    }

  // Default copy constructor IS USED and SHOULD WORK

  void set(unsigned char r, unsigned char g, unsigned char b)
    {
      r_ = r;
      g_ = g;
      b_ = b;
    }

  void set(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
    {
      r_ = r;
      g_ = g;
      b_ = b;
      a_ = a;
    }

  unsigned char r() const { return r_; }
  unsigned char g() const { return g_; }
  unsigned char b() const { return b_; }
  unsigned char a() const { return a_; }

  void set_r(unsigned char r) { r_ = r; }
  void set_g(unsigned char g) { g_ = g; }
  void set_b(unsigned char b) { b_ = b; }
  void set_a(unsigned char a) { a_ = a; }

  bool has_alpha() const { return a_ != 255; }

  guint32 rgba() const { 
    return (r_ << 24) + (g_ << 16) + (b_ << 8) + a_;
  }

  guint32 rgb() const {
    return (r_ << 16) + (g_ << 8) + b_;
  }

  void set_rgba(guint32 rgba) {
    r_ = rgba >> 24;
    g_ = (rgba >> 16) & 0xff;
    b_ = (rgba >> 8) & 0xff;
    a_ = rgba & 0xff;
  }

  void fill_pixel(unsigned char* pixel) {
    int v;
    v = *pixel;
    *pixel++ = v + (((r_ - v) * a_ + 0x80) >> 8);
    v = *pixel;
    *pixel++ =  v + (((g_ - v) * a_ + 0x80) >> 8);
    v = *pixel;
    *pixel++ =  v + (((b_ - v) * a_ + 0x80) >> 8);
  }    

  bool operator==(const Color& rhs) const
    {
      return (rgba() == rhs.rgba());
    }

private:
  unsigned char r_;
  unsigned char g_;
  unsigned char b_;
  unsigned char a_;
  
  
};

//////////////////// Tick mark

// Could be more lightweight; but you have at most maybe 100 of them,
//  so who cares. Make it nice.
class Tick {
public:
  Tick(); 

  ~Tick();

  Tick(const Tick& src);

  double pos() const { return pos_; }
  
  void set_pos(double pos) { pos_ = pos; }

  double len() const { return len_; }
  
  void set_len(double len) { len_ = len; }
  
  Color & color() { return color_; }

  const Color & color() const { return color_; }

  Color & label_color() { return label_color_; }

  const Color & label_color() const { return label_color_; }

  const string & label() const { return label_; }

  void set_label(const string & label) { label_ = label; }

  bool labelled() const { return !label_.empty(); }
  
  bool in() const { return in_; }
  bool out() const { return out_; }
  bool inout() const { return in_ && out_; }
  
  void set_in(bool setting) { in_ = setting; }
  void set_out(bool setting) { out_ = setting; }

  bool ruled() const { return ruled_; }
  
  void set_ruled(bool setting) { ruled_ = setting; }

  bool manual() const { return manual_; }

  void set_manual(bool setting) { manual_ = setting; }

  void set_font(VFont* vfont);
  VFont* font() const;

  Tick& operator=(const Tick&);

private:
  double pos_;
  double len_; 
  Color color_; 
  Color label_color_;
  string label_;
  VFont* font_;
  // can both be set
  unsigned int in_ : 1;
  unsigned int out_ : 1;
  // whether to draw a rule
  unsigned int ruled_ : 1;
  // whether this is a "manual" tick we shouldn't change
  // automatically
  unsigned int manual_ : 1;
};

//////////////////// Axis

class Axis {
public:
  Axis(double start, double stop) 
    : start_(start), stop_(stop), pos_(PlotUtil::EAST), label_font_(0), 
      default_tick_font_(0),
      line_(true), hidden_(false)
    {
      
    }

  Axis() 
    : start_(0.0), stop_(0.0), pos_(PlotUtil::EAST), label_font_(0), 
      default_tick_font_(0),
      line_(true), hidden_(false)
    {
      
    }

  ~Axis();

  Axis(const Axis& src);
  Axis& operator=(const Axis&);

  void set(double start, double stop);

  void set_start(double start);
  void set_stop(double stop);

  double start() const { return start_; }
  double stop() const { return stop_; }

  const vector<Tick> & ticks() const { return ticks_; }

  vector<Tick> & ticks() { return ticks_; }

  void set_ticks(const vector<Tick> & t) { ticks_ = t; }

  const string & label() const { return label_; }

  void set_label(const string & label) { label_ = label; }

  bool labelled() const { return !label_.empty(); }

  Color & label_color() { return label_color_; }

  const Color & label_color() const { return label_color_; }  

  VFont* label_font() const;
  void set_label_font(VFont* vfont);

  Color & line_color() { return line_color_; }

  const Color & line_color() const { return line_color_; }  

  bool line() const { return line_; }
  
  void set_line(bool setting) { line_ = setting; }

  PlotUtil::PositionType pos() const { return pos_; }
  void set_pos(PlotUtil::PositionType pos) { pos_ = pos; }

  bool horizontal() const { return PlotUtil::horizontal(pos_); }
  bool vertical() const { return PlotUtil::vertical(pos_); }

  // This will need extending eventually to accomodate major/minor ticks,
  //  etc.
  void generate_ticks(int nticks, double length);

  VFont* default_tick_font() const;
  void set_default_tick_font(VFont* vfont);

  bool hidden() const { return hidden_; }
  void set_hidden(bool setting) { hidden_ = setting; }

  bool label_hidden() const { return label_hidden_; }
  void set_label_hidden(bool setting) { label_hidden_ = setting; }

private:
  double start_;
  double stop_;
  vector<Tick> ticks_;
  string label_; 
  Color label_color_;
  Color line_color_;
  PlotUtil::PositionType pos_;

  VFont* label_font_;
  VFont* default_tick_font_;

  unsigned int line_ : 1;
  unsigned int hidden_ : 1;
  unsigned int label_hidden_ : 1;
};

//////////////////////////////////////////////////

#endif
