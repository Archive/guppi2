// -*- C++ -*-

/* 
 * pieplotedit.h 
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PIEPLOTEDIT_H
#define GUPPI_PIEPLOTEDIT_H

#include "pieplotstate.h"
#include "dataoption.h"

#include <map>

#include "gnome-util.h"

class PiePlotEdit : public PiePlotState::View {
	public:

	PiePlotEdit();
	virtual ~PiePlotEdit();

	virtual void change_slices(PiePlotState* state);
	virtual void change_data(PiePlotState* state, Data* data);
	virtual void change_background(PiePlotState* state, const Color & bg);
	virtual void change_size(PiePlotState* state, double width, double height);
	virtual void change_name(PiePlotState* state, const string& name);  
	virtual void destroy_model();

	void set_state(PiePlotState* state);

	void edit();

	private:

	GtkWidget* dialog_;

	PiePlotState* state_;

	DataOption data_option_;

	// We are the ones setting the current Data in the 
	// PiePlotState 
	bool changing_data_;

	static void data_chosen_cb(gpointer emitter, gpointer data);
	void data_chosen();

	static gint close_cb(GtkWidget* w, gpointer data);
	gint close();

	void release_state();
};

#endif
