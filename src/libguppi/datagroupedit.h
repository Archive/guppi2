// -*- C++ -*-

/* 
 * datagroupedit.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_DATAGROUPEDIT_H
#define GUPPI_DATAGROUPEDIT_H

#include "datagroup.h"
#include "gnome-util.h"

class DataOption;
class DataStore;

class DataGroupEdit : public Data::View {
public:
  DataGroupEdit();
  virtual ~DataGroupEdit();

  GtkWidget* widget(); 

  void set_data(DataGroup* dg);
  void set_store(DataStore* ds);

  DataGroup* data() { return data_; }

  virtual void change_values(Data* d, const vector<guint> & which);
  virtual void change_values(Data* d);
  virtual void change_name(Data* d, const string & name);

  virtual void destroy_model();
  
private:
  DataGroup* data_;
  DataStore* store_;

  GtkWidget* vbox_;

  GtkWidget* no_data_label_;

  vector<DataOption*> options_;

  bool changing_data_;

  void ensure_widgets();

  void clear_dataoptions();
  void rebuild_dataoptions();
  void refresh_dataoptions();

  static void data_chosen_cb(gpointer emitter, gpointer data);
  void data_chosen(DataOption* emitter);
};

#endif
