// -*- C++ -*-

/* 
 * barplotstate.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "barplotstate.h"

#include "scalardata.h"
#include "labeldata.h"

double
Bar::top() const 
{
  ScalarData* d = state_->data();
  if (d != 0)
    {
      return state_->trans().transform(d->get_scalar(index_));
    }
  else
    return 0.0; // shouldn't happen really
}

BarPlotState::BarPlotState(DataStore* ds)
  : data_(0), store_(ds), background_(255,255,255),
    width_(100.0), height_(100.0), spacing_(5.0),
    axis_checked_out_(false), set_name_from_data_(true)
{
  trans_.set_screen_bounds(0.0, height_);
  trans_.set_range_bounds(0.0, 100.0); // why not
  axis_.line_color().set(0,0,0,255);
  recalc_layout();
  g_return_if_fail(store_ != 0);
}

BarPlotState::~BarPlotState()
{
  release_data();
}

const string& 
BarPlotState::name() const
{
  return name_;
}

void 
BarPlotState::set_name(const string & name)
{
  name_ = name;
  state_model.name_changed(this, name);
}

bool 
BarPlotState::name_from_data() const
{
  return set_name_from_data_;
}

void 
BarPlotState::set_name_from_data(bool setting)
{
  set_name_from_data_ = setting;
  if (setting)
    update_name_from_data();
}

void 
BarPlotState::update_name_from_data()
{
  if (!set_name_from_data_)
    return;

  if (data_ == 0)
    {
      name_ = _("No data to chart");
    }
  else
    {
      const string & dname = data_ ? data_->name() : _("(no data)");

      guint bufsize = dname.size() + 50;
      char* buf = new char[bufsize];

      g_snprintf(buf, bufsize, _("Bar Plot of `%s'"), dname.c_str());
      
      set_name(buf);

      delete [] buf;
    }
}

void 
BarPlotState::grab_data(Data* d)
{
  if (d != 0)
    {
      data_ = d->cast_to_scalar();
    }
  else 
    {
      data_ = 0;
    }

  if (data_ != 0)
    {
      data_->ref();
      
      data_->data_model.add_view(this);
    }      
}

void 
BarPlotState::release_data()
{
  if (data_ != 0)
    {
      data_->data_model.remove_view(this);
      if (data_->unref() == 0) delete data_;
      data_ = 0;
    }
}


void 
BarPlotState::grab_labels(Data* d)
{
  if (d != 0)
    {
      labels_ = d->cast_to_label();
    }
  else 
    {
      labels_ = 0;
    }

  if (labels_ != 0)
    {
      labels_->ref();
      
      labels_->data_model.add_view(this);
    } 
}

void 
BarPlotState::release_labels()
{
  if (labels_ != 0)
    {
      labels_->data_model.remove_view(this);
      if (labels_->unref() == 0) delete labels_;
      labels_ = 0;
    }
}


void
BarPlotState::set_data(Data* d)
{
  if (data_ == (ScalarData*)d) return;

  release_data();

  grab_data(d);

  rebuild_bars();

  update_name_from_data();

  state_model.data_changed(this,data_);
}

ScalarData* 
BarPlotState::data()
{
  return data_;
}

void 
BarPlotState::set_labels(Data* d)
{
  g_warning("%s not implemented", __FUNCTION__);
}

LabelData* 
BarPlotState::labels()
{
  g_warning("%s not implemented", __FUNCTION__);

  return 0;
}


void
BarPlotState::rebuild_bars()
{
  if (data_ == 0) 
    {
      bars_.clear();
      state_model.bars_changed(this);
      return;
    }

  gsize N = data_->size();

  if (N == 0) 
    {
      bars_.clear();
      state_model.bars_changed(this);
      return;
    }

  g_assert (N > 0);

  const double* scalars = data_->scalars();
  g_return_if_fail(scalars != 0);
  
  double max = scalars[0];
  double min = scalars[0];
  double right_edge = plot_rect_.x() + plot_rect_.width(); // eventually could be height, if we permit re-orientation
  double pos = plot_rect_.x() + spacing_;

  gsize j = 0;
  while (j < N)
    {
      g_assert(j <= bars_.size());

      if (j == bars_.size())
        {
          bars_.push_back(Bar(this));
          g_assert(j < bars_.size());
        }

      bars_[j].set_index(j);
      bars_[j].set_pos(pos);
      // width of bar?

      pos += spacing_;
      pos += bars_[j].width();

      if (pos > right_edge)
        {
          // This bar goes off the edge - nuke it and bail FIXME this
          // is a sucky way to do things, should compute it in advance
          bars_.pop_back();
          break;
        }
      else 
        {
          if (scalars[j] > max) max = scalars[j];
          if (scalars[j] < min) min = scalars[j];
          ++j;
        }
    }
  
  // NOTE ORDER - the bars go up, X's y coords go down.
  trans_.set_range_bounds(max+1.0,min-1.0);

  // For now the axis is "auto," eventually it will
  // be manually adjustable.
  axis_.set_start(min);
  axis_.set_stop(max);
  axis_.generate_ticks(10,4.0); // FIXME
  
  // Clean up extra bars
  if (bars_.size() > j)
    {
      vector<Bar>::iterator erasefrom = bars_.begin() + j;
      bars_.erase(erasefrom, bars_.end());
    }

  g_debug("j: %u ", j);

  g_assert(j == bars_.size());

  state_model.bars_changed(this);
}


void 
BarPlotState::set_width(double w)
{
  if (w <= PlotUtil::EPSILON) return; // simply refuse to do this
  width_ = w;

  recalc_layout();
}

void 
BarPlotState::set_height(double h)
{
  if (h <= PlotUtil::EPSILON) return; // simply refuse to do this
  height_ = h;

  recalc_layout();
}

const Rectangle & 
BarPlotState::axis_rect() const
{
  return axis_rect_;
}

const Rectangle & 
BarPlotState::plot_rect() const
{
  return plot_rect_;
}

void 
BarPlotState::recalc_layout()
{
  // FIXME we're assuming a vertical bar plot, but that 
  // won't always be true.
  axis_.set_pos(PlotUtil::WEST);

  axis_.generate_ticks(height_/30.0, 5.0); // FIXME hardcoded

  // For now just leave a hardcoded space for the axis on the east side
  static const double axis_space = 20.0;

  axis_rect_.set(0.0, 0.0, axis_space, height_);

  plot_rect_.set(axis_space, 0.0, MAX(0.0,(width_-axis_space)), height_);

  trans_.set_screen_bounds(plot_rect_.y(), plot_rect_.y() + plot_rect_.height());

  rebuild_bars();

  state_model.size_changed(this, width_, height_);
}

Axis* 
BarPlotState::checkout_axis()
{
  if (axis_checked_out_) return 0;
  
  axis_checked_out_ = true;

  return &axis_;
}

void 
BarPlotState::checkin_axis(Axis* a)
{
  g_return_if_fail(axis_checked_out_);
  g_return_if_fail(a == &axis_);

  axis_checked_out_ = false;

  // FIXME add the other "signal"
  //state_model.axis_changed(this, x_axis_);
  state_model.size_changed(this, width_, height_);
}

void 
BarPlotState::size_request(double* w, double* h)
{
  g_return_if_fail(w != 0);
  g_return_if_fail(h != 0);
  // For now, always request the current size
  *w = width_;
  *h = height_;
}

/// Data::View


void 
BarPlotState::change_values(Data* d) 
{
  g_return_if_fail((ScalarData*)d == data_ || (LabelData*)d == labels_);
  rebuild_bars();
}

void 
BarPlotState::change_values(Data* d, const vector<guint> & which) 
{
  change_values(d);
}

void 
BarPlotState::change_name(Data* d, const string & name) 
{
  // Nothing
}

void 
BarPlotState::destroy_model()
{
  // Nothing
}
