// -*- C++ -*-

/* 
 * xyaux.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "xyaux.h"

#include "scalardata.h"
#include "markerstyle.h"

XyPair::XyPair()
  : x_data_(0), y_data_(0),  npoints_(0)
{
  pair_ = new SortedPair;
}

XyPair::~XyPair()
{
  delete pair_;
  pair_ = 0;

  if (x_data_ != 0)
    {
      release_data(x_data_);
      x_data_ = 0;
    }

  if (y_data_ != 0)
    {
      release_data(y_data_);
      y_data_ = 0;
    }

#ifdef DEBUG_RC
  g_debug("Deleting XyPair %p", this);
#endif
}

void 
XyPair::grab_data(Data* d)
{
  // In this function, remember that X and Y can be the same Data*
  if (d != 0)
    {
      d->ref();
      
      //      d->data_model.add_view(*this);
    }
}

void 
XyPair::release_data(Data* d)
{
  // In this function, remember that X and Y can be the same Data*
  if (d != 0)
    {
      //      d->data_model.remove_view(*this);
      if (d->unref() == 0) delete d;
    }
}

void 
XyPair::set_data(ScalarData** data, Data* d)
{
  g_return_if_fail(*data != (ScalarData*)d); // this should be checked before now.

  if (*data != 0)
    {
      release_data(*data);
      *data = 0;
    }

  if (d != 0)
    {
      *data = d->cast_to_scalar();
    }
  else 
    {
      *data = 0;
    }

  if (*data != 0)
    {
      grab_data(*data);
    }
}

void 
XyPair::set_x_data(Data* d)
{
  if (d == (Data*)x_data_) return;

  g_debug("Setting X data to %s", d ? d->name().c_str() : "(none)");

  Data* old_data = x_data_;

  set_data(&x_data_, d);
  
  pair_->set_key(x_data_ ? x_data_->cast_to_scalar() : 0);

  recalc_N_points();

  pair_model.x_data_changed(this,old_data,x_data_);
}


void 
XyPair::set_y_data(Data* d)
{
  if (d == (Data*)y_data_) return;

  g_debug("Setting Y data to `%s'", d ? d->name().c_str() : "(none)");

  Data* old_data = y_data_;

  set_data(&y_data_, d);

  pair_->set_other(y_data_ ? y_data_->cast_to_scalar() : 0);

  recalc_N_points();

  pair_model.y_data_changed(this,old_data,y_data_);
}

ScalarData* 
XyPair::x_data()
{
  return x_data_;
}

ScalarData* 
XyPair::y_data()
{
  return y_data_;
}


const ScalarData* 
XyPair::x_data() const
{
  return x_data_;
}

const ScalarData* 
XyPair::y_data() const
{
  return y_data_;
}


void 
XyPair::recalc_N_points()
{
  npoints_ = 0;
  if (x_data_ && y_data_)
    {
      npoints_ = MIN(x_data_->size(), y_data_->size());
    }
}

guint 
XyPair::npoints() const
{
#ifdef GNOME_ENABLE_DEBUG
  guint old = npoints_;
  ((XyPair*)this)->recalc_N_points();
  g_assert (old == npoints_);
#endif
  return npoints_;
}

XyPair* 
XyPair::clone() const
{
  XyPair* cl = new XyPair;

  cl->set_x_data(x_data_);
  cl->set_y_data(y_data_);
  
  return cl;
}

//////////////// Style goo

XyMarkers::XyMarkers()
 :  have_global_style_(true),
    global_style_(MarkerPalette::DEFAULT),
    nreserved_(0)
{


}

XyMarkers::~XyMarkers()
{

}

XyMarkers*
XyMarkers::clone() const
{
  // Note that we don't copy the reference count (that would be kind
  // of dumb)

  XyMarkers* retval = new XyMarkers;

  retval->have_global_style_ = have_global_style_;
  retval->global_style_      = global_style_;

  styles_.copy_to(retval->styles_);

  retval->styles_in_use_ = styles_in_use_;

  retval->nreserved_ = nreserved_;

  return retval;
}

void 
XyMarkers::set_style(guint point, guint style_id)
{
  vector<guint> foo;
  foo.push_back(point);

  set_style(foo, style_id);  
}

// will erase any previously-set styles
void 
XyMarkers::set_global_style(guint style_id)
{
  have_global_style_ = true;

  styles_.clear();
  styles_.set_using_8(true);

  global_style_ = style_id;

  styles_in_use_.clear(); // no need to cache these anymore

  markers_model.styles_changed(this);
}

// Sets style for lots of points at once.
void 
XyMarkers::set_style(const vector<guint> & points, guint style_id)
{
  if (have_global_style_)
    {
      have_global_style_ = false;
      styles_.reserve(nreserved_);
    }

  if (style_id > 255)
    {
      styles_.set_using_8(false);
    }

  // Because no one should be setting the style on 
  // points that don't exist.
  g_assert(nreserved_ > 0);

  g_assert(styles_.size() == nreserved_);

  guint8* small = 0;
  guint16* large = 0;

  if (styles_.using_8())
    small = styles_.small();
  else
    large = styles_.large();
  
  g_assert(small || large);

  vector<guint>::const_iterator i = points.begin();
  while (i != points.end())
    {
#ifdef GNOME_ENABLE_DEBUG
      g_assert (*i < nreserved_);
#endif

      if (small)
        small[*i] = (guint8)style_id;
      else
        large[*i] = (guint16)style_id;

      ++i;
    }

  // Add this to the set of styles that may be in use.
  styles_in_use_.insert(style_id);

  markers_model.styles_changed(this);
}

// If any point uses a style with an ID > 255, we need two bytes 
//  per point to store the styles. But we don't want the memory hit
//  normally. So this mess is needed.
bool 
XyMarkers::using_global_style() const
{
  return have_global_style_;
}

guint 
XyMarkers::global_style() const
{
  return global_style_;
}

bool 
XyMarkers::using_small_styles() const
{
  return styles_.using_8();
}

const guint8* 
XyMarkers::small_styles() const
{
  g_warn_if_fail(!have_global_style_);
  g_warn_if_fail(styles_.using_8());
  g_warn_if_fail(styles_.size() == nreserved_);
  return styles_.small();
}

const guint16* 
XyMarkers::large_styles() const
{
  g_warn_if_fail(!have_global_style_);
  g_warn_if_fail(!styles_.using_8());
  g_warn_if_fail(styles_.size() == nreserved_);
  return styles_.large();
}


// FIXME every some number of calls to this function, or maybe if
// styles_in_use_.size() gets large, we should try to blow away styles
// in styles_in_use_ that no longer are in use.
double 
XyMarkers::largest_style()
{
  MarkerPalette* palette = guppi_marker_palette();
  
  g_assert(palette != 0);

  if (have_global_style_)
    {    
      const MarkerStyle* global_style = 0;
      
      global_style = palette->get(global_style_);

      if (global_style)
        return global_style->size();
      else 
        return 1.0; //why not 
    }
  else
    {
      // Remember that styles_in_use_ may be a superset of styles actually in 
      // use, so max could be too large; but the book-keeping to fix that 
      // would be more expensive than having max too large, 90% of the time.
      double max = 0.0;
      set<guint>::iterator i = styles_in_use_.begin();
      while (i != styles_in_use_.end())
        {
          const MarkerStyle* s = palette->get(*i);
          if (s) 
            max = MAX(max, s->size());
          ++i;
        }
      return max;
    }
}

bool   
XyMarkers::style_in_use(guint32 styleid)
{
  if (have_global_style_)
    {    
      return (global_style_ == styleid);
    }
  else
    {
      // May give false positives
      set<guint>::iterator i = styles_in_use_.find(styleid);
      return (i != styles_in_use_.end());
    }
}

void
XyMarkers::reserve(guint size)
{
  nreserved_ = MAX(nreserved_, size);

  if (styles_.size() < nreserved_ && !have_global_style_)
    styles_.reserve(nreserved_);
}

guint
XyMarkers::size() const 
{
  return nreserved_;
}

////////// IndexVector
XyMarkers::IndexVector::~IndexVector() 
{
  if (a8_) 
    delete [] a8_;
  if (a16_)
    delete [] a16_;
}

void 
XyMarkers::IndexVector::copy_to(IndexVector& iv) const
{
  g_assert(&iv != this);

  iv.n_ = n_;

  if (iv.a8_ != 0)
    {
      delete [] iv.a8_;
      iv.a8_ = 0;
    }

  if (a8_ != 0)
    {
      iv.a8_ = new guint8[n_];
      memcpy(iv.a8_, a8_, n_);
    }


  if (iv.a16_ != 0)
    {
      delete [] iv.a16_;
      iv.a16_ = 0;
    }

  if (a16_ != 0)
    {
      iv.a16_ = new guint16[n_];
      memcpy(iv.a16_, a16_, n_*2);
    }

  iv.using_8_ = using_8_;
}

void 
XyMarkers::IndexVector::reserve(guint N)
{
  g_return_if_fail(N > 0);

  if (n_ == N) return;

  if (a8_)
    {
      guint8* a = new guint8[N];
      memset(a, 0, N);
      guint max = MIN(n_,N);
      guint i = 0;
      while (i < max)
        {
          a[i] = a8_[i];
          ++i;
        }
      delete [] a8_;
      a8_ = a;
    }
  else if (a16_)
    {
      guint16* a = new guint16[N];
      memset(a, 0, N*2);
      guint max = MIN(n_,N);
      guint i = 0;
      while (i < max)
        {
          a[i] = a16_[i];
          ++i;
        }
      delete [] a16_;
      a16_ = a;
    }
  else 
    {
      if (using_8_)
        {
          a8_ = new guint8[N];
          memset(a8_, 0, N);
        }
      else
        { 
          a16_ = new guint16[N];
          memset(a16_, 0, N*2);
        }
    }

  n_ = N;
}

void
XyMarkers::IndexVector::clear()
{
  if (a8_)
    delete [] a8_;
  if (a16_)
    delete [] a16_;
  
  a8_ = 0;
  a16_ = 0;

  n_ = 0;
}

void 
XyMarkers::IndexVector::set_using_8(bool setting) 
{
  if (setting && using_8_) return;
  if (!setting && !using_8_) return;

  using_8_ = setting;

  if (n_ == 0) return;

  if (a8_ && !using_8_)
    {
      a16_ = new guint16[n_];

      guint i = 0;
      while (i < n_)
        {
          a16_[i] = a8_[i];
          ++i;
        }
      delete [] a8_;
      a8_ = 0;
    }
  else if (a16_ && using_8_)
    {
      a8_ = new guint8[n_];

      guint i = 0;
      while (i < n_)
        {
          a8_[i] = a16_[i];
          ++i;
        }
      delete [] a16_;
      a16_ = 0;
    }
}

/////////////////////////////////////

// This is still here only to help me write ScatterAxes; feel free to delete soon.

#if 0

HalfAxes::HalfAxes()
  : axis_checked_out_(false)
{
  Frontend* fe = guppi_frontend();
  if (fe != 0)
    axis_.set_label_font(fe->default_font());
}

HalfAxes::~HalfAxes()
{

}

HalfAxes*
HalfAxes::clone()
{
  HalfAxes* retval = new HalfAxes;

  retval->axis_ = axis_;

  return retval;
}

Axis* 
HalfAxes::checkout_axis()
{
  if (axis_checked_out_) return 0;
  
  axis_checked_out_ = true;

  return &axis_;
}

void 
HalfAxes::checkin_axis(Axis* a)
{
  g_return_if_fail(axis_checked_out_);
  g_return_if_fail(a == &axis_);

  axis_checked_out_ = false;

  half_model.axis_changed(this, axis_);
}

void 
HalfAxes::recalc_ticks(guint nticks)
{
  axis_.generate_ticks(nticks, 4.0); // FIXME (hardcoded tick spacing)
 
  // Commenting this out is pretty much a huge hack.
  //  We know that ticks are only recalculated by ScatterPlotState (via XyAxes)
  //  in response to axis change notifications. Thus all scatter plot 
  //  state will already have been notified of the change, and 
  //  they will have queued a redraw on their canvas axis objects.
  // If we leave this in, we get infinite recursion.

  //  half_model.axis_changed(this, axis_);
}

//////////////////

XyAxes::XyAxes()
  : x_axis_(0), y_axis_(0)
{
  x_axis_ = new HalfAxes;
  y_axis_ = new HalfAxes;
  
  grab_axis(x_axis_);
  grab_axis(y_axis_);

  Axis* a = x_axis_->checkout_axis();  

  if (a != 0)
    {
      // FIXME only temporarily hard-coded
      a->set_pos(PlotUtil::SOUTH);

      x_axis_->checkin_axis(a);
    }
  else 
    g_warning("Couldn't check out X axis to modify it!");

  a = y_axis_->checkout_axis();
  
  if (a != 0)
    {
      a->set_pos(PlotUtil::WEST);

      y_axis_->checkin_axis(a);
    }
  else 
    g_warning("Couldn't check out Y axis to modify it!");
}

XyAxes::~XyAxes()
{
  release_x_axis();
  release_y_axis();
}

void 
XyAxes::set_axis_bounds(const AxisBounds& ab)
{
  g_assert(x_axis_ != 0);
  g_assert(y_axis_ != 0);

  Axis* a = x_axis_->checkout_axis();  

  if (a != 0)
    {
      a->set_start(ab.xstart);
      a->set_stop(ab.xstop);

      x_axis_->checkin_axis(a);
    }
  else 
    g_warning("Couldn't check out X axis to modify it!");

  a = y_axis_->checkout_axis();
  
  if (a != 0)
    {
      a->set_start(ab.ystart);
      a->set_stop(ab.ystop);

      y_axis_->checkin_axis(a);
    }
  else 
    g_warning("Couldn't check out Y axis to modify it!");
}

void 
XyAxes::push_axis_bounds(double xstart, double ystart, 
                         double xstop, double ystop)
{
  AxisBounds ab;

  ab.xstart = x_axis_->axis().start();
  ab.ystart = y_axis_->axis().start();
  ab.xstop = x_axis_->axis().stop();
  ab.ystop = y_axis_->axis().stop();

  axis_bounds_stack_.push_back(ab);

  // Set new bounds.

  ab.xstart = xstart;
  ab.ystart = ystart;
  ab.xstop = xstop;
  ab.ystop = ystop; 

  set_axis_bounds(ab);
}

guint 
XyAxes::axis_bounds_stack_size() const
{
  return axis_bounds_stack_.size();
}

void 
XyAxes::pop_axis_bounds()
{
  if (axis_bounds_stack_.size() == 0)
    {
#ifdef GNOME_ENABLE_DEBUG
      g_warning("Attempt to pop from empty axis bounds stack!");
#endif
      return;
    }
  else 
    {
      set_axis_bounds(axis_bounds_stack_.back());
      axis_bounds_stack_.pop_back();
    }
}

Axis* 
XyAxes::checkout_x_axis()
{
  g_assert(x_axis_ != 0);
  g_assert(y_axis_ != 0);

  return x_axis_->checkout_axis();
}

Axis* 
XyAxes::checkout_y_axis()
{
  g_assert(x_axis_ != 0);
  g_assert(y_axis_ != 0);

  return y_axis_->checkout_axis();
}

void 
XyAxes::checkin_x_axis(Axis* a)
{
  g_assert(x_axis_ != 0);
  g_assert(y_axis_ != 0);

  x_axis_->checkin_axis(a);
}

void 
XyAxes::checkin_y_axis(Axis* a)
{
  g_assert(x_axis_ != 0);
  g_assert(y_axis_ != 0);

  y_axis_->checkin_axis(a);
}

void 
XyAxes::recalc_ticks(guint nxticks, guint nyticks)
{
  g_assert(x_axis_ != 0);
  g_assert(y_axis_ != 0);

  x_axis_->recalc_ticks(nxticks);
  y_axis_->recalc_ticks(nyticks);
}

///

void 
XyAxes::change_axis(HalfAxes* half, const Axis& axis)
{
  if (half == x_axis_)
    axes_model.x_axis_changed(this, axis);
  else if (half == y_axis_) 
    axes_model.y_axis_changed(this, axis);
#ifdef GNOME_ENABLE_DEBUG
  else 
    g_warning("XyAxes notified of non-owned axis change");
#endif
}

void 
XyAxes::destroy_model()
{
#ifdef GNOME_ENABLE_DEBUG
  g_warning("Axis destroyed from under XyAxes!");
#endif
}

///

HalfAxes* 
XyAxes::xhalf()
{
  return x_axis_;
}

HalfAxes* 
XyAxes::yhalf()
{
  return y_axis_;
}

void 
XyAxes::set_xhalf(HalfAxes* half)
{
  if (half == x_axis_)
    return;

  grab_axis(half);

  release_x_axis();

  x_axis_ = half;

  axes_model.x_axis_changed(this, x_axis_->axis());
}

void 
XyAxes::set_yhalf(HalfAxes* half)
{
  if (half == y_axis_)
    return;

  grab_axis(half);

  release_y_axis();

  y_axis_ = half;

  axes_model.y_axis_changed(this, y_axis_->axis());
}

void 
XyAxes::grab_axis(HalfAxes* half)
{
  half->ref();

  half->half_model.add_view(*this);
}

void 
XyAxes::release_an_axis(HalfAxes** half)
{
  (*half)->half_model.remove_view(*this);
  
  if ((*half)->unref() == 0)
    {
      delete *half;
    }
  
  *half = 0;
}

void 
XyAxes::release_x_axis()
{
  release_an_axis(&x_axis_);
}

void 
XyAxes::release_y_axis()
{
  release_an_axis(&y_axis_);
}

///

void
XyAxes::share_x_with(XyAxes* partner)
{
  partner->set_xhalf(x_axis_);
}

void
XyAxes::share_y_with(XyAxes* partner)
{
  partner->set_yhalf(y_axis_);
}

void
XyAxes::hoard_x()
{
  // Could optimize by not doing this if the refcount on our 
  //  axis is only 1

  HalfAxes* mine = x_axis_->clone();

  set_xhalf(mine);
}

void
XyAxes::hoard_y()
{
  // Could optimize by not doing this if the refcount on our 
  //  axis is only 1

  HalfAxes* mine = y_axis_->clone();

  set_yhalf(mine);
}

#endif

