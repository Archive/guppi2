// -*- C++ -*-

/* 
 * callbacks.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CALLBACKS_H
#define GUPPI_CALLBACKS_H

#include <vector>
#include <pair.h>
#include <glib.h>

// This object manages simple callback lists.  It is not even remotely
// as powerful as Gtk signals, but less annoying to program,
// especially given C++ objects. We only support one callback signature,
// to avoid fooling with templates and marshallers and blah blah.

class CallbackList {
public:
  // emitter should be the high-level object 
  //  using the callbacklist to implement callbacks.
  CallbackList(void* emitter);
  ~CallbackList();

  // Callback receives user data, and a pointer to the 
  //  object which invokes the callbacks.
  typedef void (*Callback)(void* emitter, void* data);

  void connect(Callback cb, void* data);

  // Note that objects are responsible for disconnect - there's no 
  //  Gtk-like auto-disconnect-on-destroy
  void disconnect(Callback cb);

  void invoke();

private:
  typedef pair<Callback,void*> Closure;

  // Vector does disconnect a bit slow, but is faster to iterate over
  // and uses less storage.
  vector<Closure> callbacks_;

  void* emitter_;
  
  CallbackList(const CallbackList&);
  const CallbackList& operator=(const CallbackList&);
};

#endif // #ifdef GUPPI_CALLBACKS_H
