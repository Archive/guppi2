// -*- C++ -*-

/* 
 * xylayers.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_XYLAYERS_H
#define GUPPI_XYLAYERS_H

#include "util.h"
#include "data.h"
#include "datastore.h"
#include "plotutils.h"

#include "xyaux.h"

#include <vector>
#include <string>
#include <set>

class ScalarData;
class SortedPair;
class PointTiles;
class TypeRegistry;

// The xy plot keeps a stack of these; each one is attached 
// to two axes. Abstracts any xy plots, line plots, or functions.
class XyLayer {
public:
  typedef enum {
    ScatterLayer,
    LineLayer,
    DataLayer, // scatter and line are both type DataLayer
    RelationLayer,
    PriceLayer
    // other types can be added at runtime to the type registry
  } LayerType;

  LayerType type() const { return type_; }

  // Returns 0 if you cast to the wrong type
  gpointer cast_to_type(LayerType type);

  // Can be 0 if no layers exist. Registry is destroyed whenever no
  // layers exist; so user types must be re-registered... OK, this sucks.
  // will fix it eventually, but for now handle the 0 case right.
  static TypeRegistry* type_registry();

  virtual ~XyLayer();

  PlotUtil::PositionType x_axis() const;
  PlotUtil::PositionType y_axis() const;
  
  void set_x_axis(PlotUtil::PositionType pos);
  void set_y_axis(PlotUtil::PositionType pos);

  // Tools should be careful not to operate on hidden layers...
  void set_hidden(bool setting);
  bool hidden() const { return hidden_; }

  // Tools should also skip masked layers...
  void set_masked(bool setting);
  bool masked() const { return masked_; }

  void set_name(const string& name);
  const string& name() const { return name_; }

  // Return a reasonable name for a XyPlotState 
  //  containing this layer and/or the layer itself.
  virtual string generate_name() const = 0;

  class View : public ::View 
  {
  public:
    virtual void change_x_axis(XyLayer* layer, 
                               PlotUtil::PositionType old_pos, 
                               PlotUtil::PositionType new_pos) = 0;
    virtual void change_y_axis(XyLayer* layer, 
                               PlotUtil::PositionType old_pos, 
                               PlotUtil::PositionType new_pos) = 0;

    virtual void change_name(XyLayer* layer, 
                             const string& name) = 0;

    virtual void change_masked(XyLayer* layer, bool maskedness) = 0;
    virtual void change_hidden(XyLayer* layer, bool hiddenness) = 0;

  };

  class Model : public ::Model<XyLayer::View*> {
  public:
    void x_axis_changed(XyLayer* layer, 
                        PlotUtil::PositionType old_pos, 
                        PlotUtil::PositionType new_pos) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_x_axis(layer,old_pos,new_pos);
	++i;
      }
      unlock_views();
    }
    void y_axis_changed(XyLayer* layer, 
                        PlotUtil::PositionType old_pos, 
                        PlotUtil::PositionType new_pos) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_y_axis(layer,old_pos,new_pos);
	++i;
      }
      unlock_views();
    }
    void name_changed(XyLayer* layer, 
                      const string& name) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_name(layer,name);
	++i;
      }
      unlock_views();
    }
    void hidden_changed(XyLayer* layer, 
                        bool hidden) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_hidden(layer,hidden);
	++i;
      }
      unlock_views();
    }
    void masked_changed(XyLayer* layer, 
                        bool masked) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_masked(layer,masked);
	++i;
      }
      unlock_views();
    }
  };

  Model layer_model;

  guint ref() { return rc_.ref(); }
  guint unref() { return rc_.unref(); }
  
protected:  
  XyLayer(LayerType type);

private:
  RC rc_;

  LayerType type_;

  PlotUtil::PositionType x_axis_;
  PlotUtil::PositionType y_axis_;

  static TypeRegistry* type_registry_;

  string name_;

  bool hidden_;
  bool masked_;
};

class XyDataLayer : public XyLayer,
                    public XyPair::View,
                    public DataStore::View {
public:
  virtual ~XyDataLayer();

  const SortedPair& sorted() const; 

  // Data can be scalar or date eventually, 
  // but right now it is all scalar. will fix that soonish.
  void set_x_data(Data* d);
  ScalarData* x_data();
  const ScalarData* x_data() const;

  void set_y_data(Data* d);
  ScalarData* y_data();
  const ScalarData* y_data() const;

  // Number of points - generally the smaller of xdata->size() and
  // ydata->size()
  guint npoints() const;

  virtual string generate_name() const;

  // Use this for example to associate a line and scatter plot with 
  //  one another.
  void share_datapair_with(XyDataLayer* partner);
  void hoard_datapair();

  // XYPair::View

  virtual void change_x_data(XyPair* pair, Data* old_data, Data* data);
  virtual void change_y_data(XyPair* pair, Data* old_data, Data* data);

  // DataStore::View
  virtual void add_data(DataStore* ds, Data* d);
  virtual void remove_data(DataStore* ds, Data* d);
  virtual void change_data_values(DataStore* ds, 
                                  Data* d, 
                                  const vector<guint> & which);
  virtual void change_data_values(DataStore* ds, Data* d);
  virtual void change_data_name(DataStore* ds, Data* d, const string & name);

  // ::View

  virtual void destroy_model();

  class View : public ::View 
  {
  public:
    virtual void change_x_data(XyDataLayer* layer, 
                               Data* old_data, Data* new_data) = 0;
    virtual void change_y_data(XyDataLayer* layer, 
                               Data* old_data, Data* new_data) = 0;

    // Chain out the DataStore signals, to make it easier for 
    //  views to redraw when values change.
    virtual void change_data_values(XyDataLayer* layer, Data* data) = 0;
  };

  class Model : public ::Model<XyDataLayer::View*> {
  public:
    void x_data_changed(XyDataLayer* layer, Data* old_data, Data* data) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_x_data(layer, old_data, data);
	++i;
      }
      unlock_views();
    }
    void y_data_changed(XyDataLayer* layer,  Data* old_data, Data* data) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_y_data(layer, old_data, data);
	++i;
      }
      unlock_views();
    }
    void data_values_changed(XyDataLayer* layer, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data_values(layer,d);
	++i;
      }
      unlock_views();
    }    

  };

  Model data_layer_model;

protected:  
  XyDataLayer(LayerType type, DataStore* store);

  XyPair* xypair();
  void set_xypair(XyPair* pair);

  // Called when the number of points might have changed, 
  //  so subclasses can sync
  virtual void update_N_points();

private:
  DataStore* store_;
  XyPair* pair_;

  void grab_xypair(XyPair* pair);
  void release_xypair();

};


class XyScatter : public XyDataLayer,
                  public XyMarkers::View {
public:
  XyScatter(DataStore* ds);
  virtual ~XyScatter();
  
  //// Styles

  void set_style(guint point, guint style_id);
  // will erase any previously-set styles
  void set_global_style(guint style_id);
  // Sets style for lots of points at once.
  void set_style(const vector<guint> & points, guint style_id);

  // Return the markersize of the largest style in the xyplotstate.
  // The returned value may be too large; we do some guessing to keep from
  // iterating over all the styles every time.
  // This is needed when deciding which region of a display to update.
  double largest_style();

  /// Report whether this style is in use for this plot.
  /// May report false positives, for the same reason largest_style() might.
  bool   style_in_use(guint32 styleid);

  // If any point uses a style with an ID > 255, we need two bytes 
  //  per point to store the styles. But we don't want the memory hit
  //  normally. So this mess is needed.
  bool using_global_style() const;
  guint global_style() const;
  bool using_small_styles() const;

  // if the arrays are non-zero and we aren't using the global style,
  //  they are at least npoints() in size
  const guint8* small_styles() const;
  const guint16* large_styles() const;  

  // sharing
  void share_markers_with(XyScatter* partner);
  void hoard_markers();

  // XyMarkers::View
  virtual void change_styles(XyMarkers* markers);
  virtual void destroy_model();

  class View : public ::View 
  {
  public:
    // Means the styleid's for one or more points changed
    virtual void change_styles(XyScatter* ss) = 0;
  };

  class Model : public ::Model<XyScatter::View*> {
  public:
    void styles_changed(XyScatter* ss) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_styles(ss);
	++i;
      }
      unlock_views();
    }
  };

  Model xy_scatter_model;

private:
  XyMarkers* markers_;
  
  virtual void update_N_points();

  void grab_xymarkers(XyMarkers* markers);
  void release_xymarkers();
  XyMarkers* xymarkers();
  void set_xymarkers(XyMarkers* markers);

};

// and the line plot...
class XyLine : public XyDataLayer {
public:
  XyLine(DataStore* ds);
  virtual ~XyLine();

  // We will need some kind of view-notification here eventually
  // (when the line has colors, etc.)

private:
  virtual void update_N_points();
};

class XyRelation : public XyLayer {
public:
  XyRelation();
  virtual ~XyRelation();

  // 0 or more ys are added to the vector
  virtual void point(double x, vector<double>& ys) = 0;

  // Precision is the dx between x's we request a point from.
  double precision() const { return precision_; }
  void set_precision(double prec);

  virtual string generate_name() const;

  class View : public ::View 
  {
  public:
    // If anything changes - redraw is then needed
    virtual void change_relation(XyRelation* sr) = 0;
  };

  class Model : public ::Model<XyRelation::View*> {
  public:
    void relation_changed(XyRelation* sr) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_relation(sr);
	++i;
      }
      unlock_views();
    }
  };

  Model xy_relation_model;


private:
  double precision_;

};

class XyPriceBars : public XyLayer {
public:
  XyPriceBars(DataStore* ds);
  virtual ~XyPriceBars();

  void set_data(Data* d); // should be a DataGroup with price data in it
  DataGroup* data(); 
  const DataGroup* data() const;

  guint nbars() const;

  virtual string generate_name() const;

  // DataStore::View
  virtual void add_data(DataStore* ds, Data* d);
  virtual void remove_data(DataStore* ds, Data* d);
  virtual void change_data_values(DataStore* ds, 
                                  Data* d, 
                                  const vector<guint> & which);
  virtual void change_data_values(DataStore* ds, Data* d);
  virtual void change_data_name(DataStore* ds, Data* d, const string & name);

  // ::View

  virtual void destroy_model();

  class View : public ::View 
  {
  public:
    virtual void change_data(XyPriceBars* layer,
                             Data* old_data, Data* new_data) = 0;

    // Chain out the DataStore signals, to make it easier for 
    //  views to redraw when values change.
    virtual void change_data_values(XyPriceBars* layer, Data* data) = 0;
  };

  class Model : public ::Model<XyPriceBars::View*> {
  public:
    void data_changed(XyPriceBars* layer,
                      Data* old_data, Data* data) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data(layer, old_data, data);
	++i;
      }
      unlock_views();
    }

    void data_values_changed(XyPriceBars* layer, Data* d) {
      lock_views();
      iterator i = begin();
      while (i != end()) {
	(*i)->change_data_values(layer,d);
	++i;
      }
      unlock_views();
    }    

  };

  Model price_model;

protected:  

private:
  DataStore* store_;

  DataGroup* data_;

  void grab_data(Data* d);
  void release_data(Data* d);
  bool our_data(Data* d); // do we use this data
};

#endif

