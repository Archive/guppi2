/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* EZRubberband: rubberband object for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef EZ_RUBBERBAND_H
#define EZ_RUBBERBAND_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

BEGIN_GNOME_DECLS

#define EZ_TYPE_RUBBERBAND            (ez_rubberband_get_type ())
#define EZ_RUBBERBAND(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_RUBBERBAND, EZRubberband))
#define EZ_RUBBERBAND_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_RUBBERBAND, EZRubberbandClass))
#define EZ_IS_RUBBERBAND(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_RUBBERBAND))
#define EZ_IS_RUBBERBAND_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_RUBBERBAND))


typedef struct _EZRubberband EZRubberband;
typedef struct _EZRubberbandClass EZRubberbandClass;

/* Note that this is NOT a canvas item, just a GtkObject */

struct _EZRubberband {
  GtkObject object;
  
  GnomeCanvasItem* item;

  GnomeCanvasItem* rect;

  /* These are item coordinates for internal use.*/
  double x_origin, y_origin;

  guint32 last_time;

  /* Button whose release snaps rubberband. */
  gint button; 
  /* Pressing this cancels */
  gint cancel_button;
};

struct _EZRubberbandClass {
  GtkObjectClass parent_class;

  /* Coordinates are all item coordinates. */
  void (* moved)    (EZRubberband* rb, double x1, double y1, double x2, double y2);
  void (* snapped)  (EZRubberband* rb, double x1, double y1, double x2, double y2);
  void (* cancelled)(EZRubberband* rb); 
};

/* Standard Gtk function */
GtkType ez_rubberband_get_type (void);

/* The rubberband can only be used in a particular way. You start it on 
   the canvas, get it back, connect to snapped/cancelled; once it's 
   snapped or cancelled, you have to destroy it. There can be only
   one rubberband per canvas at a time, and you can use a rubberband
   only once. You will get either snapped or cancelled, never both. */

/* x and y are world coordinates, since you'll usually start
   with a button event. */

/* Rubberband coordinates are relative to the item you pass in, though the 
   rubberbanding is not confined to that item, cancelled/snapped will return
   coordinates relative to it. */

EZRubberband* 
ez_rubberband_new (GnomeCanvasItem*    item,
                   gdouble             x,
                   gdouble             y,
                   guint32             event_time, 
                   gint                button,         /* Button which started drag */
                   gint                cancel_button); /* Button to cancel, 0 for none */

END_GNOME_DECLS

#endif

