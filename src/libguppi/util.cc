// -*- C++ -*-

/* 
 * util.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "util.h"

#include "markerstyle.h"
#include "dataspecstore.h"
#include "datagroup.h"

static MarkerPalette* mp = 0;

MarkerPalette*
guppi_marker_palette()
{
  return mp;
}


static DataGroupSpecStore* spec_store = 0;

DataGroupSpecStore* 
guppi_data_group_spec_store()
{
  return spec_store;
}

bool
guppi_init_library()
{
  g_return_val_if_fail(mp == 0, false); // check for duplicate init

  mp = new MarkerPalette;
  mp->ref();

  spec_store = new DataGroupSpecStore;  
  spec_store->ref();

  return true;
}

void 
guppi_shutdown_library()
{
  if (mp != 0)
    {
      if (mp->unref() == 0)
        delete mp;

      mp = 0;
    }

  if (spec_store != 0)
    {
      if (spec_store->unref() == 0)
        delete spec_store;

      spec_store = 0;
    }
}

static Frontend* fe = 0;

void 
guppi_register_frontend(Frontend* fe_)
{
  g_return_if_fail(fe == 0);

  fe = fe_;
}

Frontend* 
guppi_frontend()
{
  return fe;
}

// Builtin group specs

static DataGroupSpec* price_data_spec = 0;

DataGroupSpec* 
guppi_price_data_spec()
{
  DataGroupSpecStore* store = guppi_data_group_spec_store();

#ifdef GNOME_ENABLE_DEBUG
  if (store == 0)
    {
      g_warning("Calling %s after shutting down or before initializing libguppi", __FUNCTION__);
      g_assert_not_reached();
      return 0;
    }
#endif

  if (price_data_spec == 0)
    {
      price_data_spec = new DataGroupSpec;

      price_data_spec->add_slot(Data::Date, _("Dates"));
      price_data_spec->add_slot(Data::Scalar, _("Opens"));
      price_data_spec->add_slot(Data::Scalar, _("Highs"));
      price_data_spec->add_slot(Data::Scalar, _("Lows"));
      price_data_spec->add_slot(Data::Scalar, _("Closes"));

      price_data_spec->set_name(_("Price Data"));

      price_data_spec->lock();

      store->insert(price_data_spec);
    }

  return price_data_spec;
}

