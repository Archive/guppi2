// -*- C++ -*-

/* 
 * pieplot.h 
 *
 * Copyright (C) 1999 Frank Koormann, Bernhard Reiter & Jan-Oliver Wagner
 *
 * Developed by Frank Koormann <fkoorman@usf.uos.de>,
 * Bernhard Reiter <breiter@usf.uos.de> and
 * Jan-Oliver Wagner <jwagner@usf.uos.de>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PIEPLOT_H
#define GUPPI_PIEPLOT_H

#include "plot.h"
#include "pieplotstate.h"
#include "pieplotedit.h"
#include "tool.h"

class PiePlot : public Plot, public PiePlotState::View {
public:
  PiePlot(DataStore* ds);
  PiePlot(PiePlotState* state);
  virtual ~PiePlot();

  static const char* ID;

  virtual PlotState* plot_state() { return state_; }

  virtual Plot* new_view();

  virtual GnomeCanvasItem* item();

  virtual void realize(GnomeCanvasGroup* group);
  virtual void unrealize();

  virtual void set_x(double x);
  virtual void set_y(double y);

  virtual double x() const { return xpos_; }
  virtual double y() const { return ypos_; }

  virtual void set_size(double width, double height);

  virtual double width();
  virtual double height();

  virtual void size_request(double* width, double* height);

  const string& name() const;
  
  // PiePlotState::View

  virtual void change_slices(PiePlotState* state);
  virtual void change_data(PiePlotState* state, Data* data);
  virtual void change_background(PiePlotState* state, const Color & bg);
  virtual void change_size(PiePlotState* state, double width, double height);
  virtual void change_name(PiePlotState* state, const string& name);  

  // ::View

  virtual void destroy_model();

  void set_state(PiePlotState* state);
  PiePlotState* state() { return state_; }

private:
  GtkWidget* canvas_;

  // this saves enough info to recreate the same plot somewhere else.
  PiePlotState* state_;
  
  // group holding all the objects in the pie plot
  GnomeCanvasGroup* group_;

  // vector of pies items 
  vector<GnomeCanvasItem*> slices_;
  vector<GnomeCanvasItem*> slicelabels_;

  // background of the plot (a rectangle)
  GnomeCanvasItem* bg_;

  PiePlotEdit edit_;

  double xpos_;
  double ypos_; 

  void release_state();
  void redisplay_slices();
  static gint event_cb(GnomeCanvasItem* item, GdkEvent* e, gpointer data);
  gint event(GdkEvent* e);

};


#endif
