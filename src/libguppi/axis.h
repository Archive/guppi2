// -*- C++ -*-

/* 
 * axis.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_AXIS_H
#define GUPPI_AXIS_H

#warning "This file isn't actually used yet. It's the future Axis replacement."

#include "util.h"
#include "plotutils.h"

// Generic Axis object. Used in several plot types.

class Axis {
public:
  Axis(double start, double stop) 
    : start_(start), stop_(stop), pos_(PlotUtil::EAST), label_font_(0), 
      default_tick_font_(0),
      line_(true), hidden_(false)
    {
      
    }

  Axis() 
    : start_(0.0), stop_(0.0), pos_(PlotUtil::EAST), label_font_(0), 
      default_tick_font_(0),
      line_(true), hidden_(false)
    {
      
    }

  ~Axis();

  Axis(const Axis& src);
  Axis& operator=(const Axis&);

  void set(double start, double stop);

  void set_start(double start);
  void set_stop(double stop);

  double start() const { return start_; }
  double stop() const { return stop_; }

  const vector<Tick> & ticks() const { return ticks_; }

  vector<Tick> & ticks() { return ticks_; }

  void set_ticks(const vector<Tick> & t) { ticks_ = t; }

  const string & label() const { return label_; }

  void set_label(const string & label) { label_ = label; }

  bool labelled() const { return !label_.empty(); }

  Color & label_color() { return label_color_; }

  const Color & label_color() const { return label_color_; }  

  VFont* label_font() const;
  void set_label_font(VFont* vfont);

  Color & line_color() { return line_color_; }

  const Color & line_color() const { return line_color_; }  

  bool line() const { return line_; }
  
  void set_line(bool setting) { line_ = setting; }

  PlotUtil::PositionType pos() const { return pos_; }
  void set_pos(PlotUtil::PositionType pos) { pos_ = pos; }

  bool horizontal() const { return PlotUtil::horizontal(pos_); }
  bool vertical() const { return PlotUtil::vertical(pos_); }

  // This will need extending eventually to accomodate major/minor ticks,
  //  etc.
  void generate_ticks(int nticks, double length);

  VFont* default_tick_font() const;
  void set_default_tick_font(VFont* vfont);

  bool hidden() const { return hidden_; }
  void set_hidden(bool setting) { hidden_ = setting; }

  bool label_hidden() const { return label_hidden_; }
  void set_label_hidden(bool setting) { label_hidden_ = setting; }

private:
  double start_;
  double stop_;
  vector<Tick> ticks_;
  string label_; 
  Color label_color_;
  Color line_color_;
  PlotUtil::PositionType pos_;

  VFont* label_font_;
  VFont* default_tick_font_;

  unsigned int line_ : 1;
  unsigned int hidden_ : 1;
  unsigned int label_hidden_ : 1;
};



#endif
