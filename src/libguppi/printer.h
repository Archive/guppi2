// -*- C++ -*-

/* 
 * printer.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PRINTER_H
#define GUPPI_PRINTER_H

#include "util.h"
#include "plotutils.h"
#include "vfont.h"

// A "Printer" is a batch-mode target similar to the libplot and
// gnome-print printer abstractions. There are functions to write a
// State object to such a target. It is basically copied from gnome-print, 
// and for now gnome-print will be the only concrete target.

// This is a single-use object; that is, you create it, print once,
//  then blow it away. You have to sync() at the end of use.

// Eventually I want to simplify the interface, so it's easier to implement
//  for new printer types.

class Printer {
public:
  Printer() {}
  virtual ~Printer() {}

  virtual void newpath() = 0;
  virtual void moveto(double x, double y) = 0;
  virtual void lineto(double x, double y) = 0;
  virtual void curveto(double x1, double y1, double x2, double y2, double x3, double y3) = 0;
  virtual void closepath() = 0;
  // alpha may not be supported.
  virtual void setcolor(const Color & c) = 0;

  virtual void fill() = 0;
  virtual void eofill() = 0;
  virtual void setlinewidth(double w) = 0;
  virtual void setmiterlimit(double limit) = 0;
  virtual void setlinejoin(int jointype) = 0;
  virtual void setlinecap(int captype) = 0;
  virtual void setdash(int n_values, double* values, double offset) = 0;
  virtual void strokepath() = 0;
  virtual void stroke() = 0;
  virtual void setfont(VFont* font) = 0;
  virtual void show(const char* text) = 0;
  virtual void concat(double matrix[6]) = 0;
  virtual void concat(const Affine&) = 0;
  virtual void setmatrix(double matrix[6]) = 0;
  virtual void setmatrix(const Affine&) = 0;
  virtual void translate(double x, double y) = 0;
  virtual void rotate(double degrees) = 0;
  virtual void scale(double xfactor, double yfactor) = 0;
  virtual void gsave() = 0;
  virtual void grestore() = 0;
  virtual void clip() = 0;
  virtual void eoclip() = 0;
  virtual void grayimage(const guchar* data, int width, int height, int rowstride) = 0;
  virtual void rgbimage(const guchar* data, int width, int height, int rowstride) = 0;
  virtual void showpage() = 0;

  // Actually write changes to disk or send to printer;
  //  may not succeed. Error messages will be improved eventually.
  virtual bool sync() = 0;

  void circle(double x, double y, double r);

private:
  
  const Printer& operator=(const Printer&);
  Printer(const Printer &);

};

#endif
