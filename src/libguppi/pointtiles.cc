// -*- C++ -*-

/* 
 * pointtiles.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "pointtiles.h"
#include "plotutils.h"

inline PointTiles::Tile* 
PointTiles::new_tile()
{
  if (tilepool_.empty())
    {
      Tile* ta = new Tile[100];

      tilechunks_.push_back(ta);

      Tile* end = &ta[100];
      while (ta != end)
        {
          tilepool_.push_back(ta);
          ++ta;
        }
    }
  
  Tile* t = tilepool_.back();
  tilepool_.pop_back();
  return t;
}

inline void  
PointTiles::free_tile(Tile* t)
{
  t->contents.clear();
  tilepool_.push_back(t);
}

PointTiles::PointTiles()
  : ppt_(100)
{


}

PointTiles::~PointTiles()
{
  vector<Band*>::iterator i = tiles_.begin();
  while (i != tiles_.end())
    {
      delete *i;
      ++i;
    }

  while (!tilechunks_.empty())
    {
      delete [] tilechunks_.back();
      tilechunks_.pop_back();
    }
}

void 
PointTiles::set_points_per_tile(guint ppt)
{
  g_return_if_fail(ppt > 0);
  ppt_ = ppt;
}

void 
PointTiles::set_points(const double* xvalues, const double* yvalues, 
                       const guint* converter, guint N,
                       double xmin, double ymin, double xmax, double ymax)
{
  g_return_if_fail(xvalues != 0);
  g_return_if_fail(yvalues != 0);
  g_return_if_fail(xmax >= xmin);
  g_return_if_fail(ymax >= ymin);

  // First clear existing tiles
  vector<Band*>::iterator vti = tiles_.begin();
  while (vti != tiles_.end())
    {
      vector<Tile*>::iterator ti = (*vti)->begin();
      while (ti != (*vti)->end())
        {
          free_tile(*ti);

          ++ti;
        }

      delete *vti;

      ++vti;
    }

  tiles_.clear();

  // Find the closest to xmin
  guint xstart = PlotUtil::first_ge(xvalues, N, xmin);

  // Then the first one ge xmax
  guint xstop = PlotUtil::first_ge(xvalues, N, xmax);
  
  // We want to include xmax and any points equal to it.
  while (xstop < N && xvalues[xstop] == xmax) 
    ++xstop;
  
  const guint npoints = xstop - xstart;
  const guint ntiles = npoints/ppt_ + 1; // target number of tiles
  // number of tiles per row/column
  const guint side = static_cast<guint>( sqrt(double(ntiles)) + 1.0 );
  const guint ppband = MAX(1, npoints/side); // points per band of tiles

#ifdef GNOME_ENABLE_DEBUG
  guint d_tiled_points = 0;
  guint d_tiles = 0;
  guint d_bands = 0;
#endif

  // Temporary storage for sorts. + 1 is paranoia
  guint xindices[ppband+1];
  double ysorted[ppband+1];

  while (xstart < xstop)
    {
      const guint bandstop = MIN(xstart+ppband, xstop);
      const guint chunk = bandstop - xstart;
      
      // We have our chunk of X points; now we need to make a copy and
      // an array of X points so we can sort by Y. Woo-hoo.
      guint i = xstart;
      while (i < bandstop)
        {
          xindices[i-xstart] = i;
          ++i;
        }
          
      memcpy(ysorted, &yvalues[xstart], sizeof(double)*chunk);
          
      // Now sort...
      PlotUtil::sort_indices(ysorted, xindices, chunk);
          
      const double x1 = xvalues[xstart];
      const double x2 = xvalues[bandstop-1]; // OK if it's the same as x1
          
      Band* band = new Band;
      band->x1 = x1;
      band->x2 = x2;
          
      const guint pptile = MAX(1,chunk/side); // at least 1
      const double bandcenter = x1 + (x2-x1)/2;

      guint ystart = 0;
      const guint ystop = chunk;
      while (ystart < ystop)
        {
          const guint tilestop =  MIN(ystart+pptile, ystop);
          Tile* t = new_tile();
          
          // All tiles in the band must have t->x1 >= x1 and 
          //  t->x2 <= x2.

          t->x1 = bandcenter; 
          t->x2 = bandcenter;
          t->y1 = ysorted[ystart];
          t->y2 = ysorted[tilestop-1]; // OK if it == y1
          
          while (ystart < tilestop)
            {
              if (ysorted[ystart] >= ymin && ysorted[ystart] <= ymax)
                {
                  const double x = xvalues[xindices[ystart]];
                  if (x < t->x1) t->x1 = x;
                  if (x > t->x2) t->x2 = x;
                  
                  t->contents.push_back(converter[xindices[ystart]]);
#ifdef GNOME_ENABLE_DEBUG
                  ++d_tiled_points;
#endif
                }

              ++ystart;
            }

          if (!t->contents.empty())
            {
              band->push_back(t);

#ifdef GNOME_ENABLE_DEBUG
              ++d_tiles;
#endif
            }
          else
            { 
              free_tile(t);
            }
        }

      if (!band->empty())
        {
          tiles_.push_back(band);         
#ifdef GNOME_ENABLE_DEBUG
          ++d_bands;
#endif
        }
      else
        {
          delete band;
        }

      xstart = bandstop;
    }

#ifdef GNOME_ENABLE_DEBUG  
  g_debug("%u of %u points tiled in %u tiles, %u vertical bands", 
          d_tiled_points, N, d_tiles, d_bands);
#endif

  check_invariants();
}

void
PointTiles::tiles_in_rect(vector<const PointTiles::Tile*> & edgetiles,
                          vector<const PointTiles::Tile*> & intiles,
                          double x1, double y1, double x2, double y2,
                          double edgefuzz) const
{
  g_return_if_fail(x2 >= x1);
  g_return_if_fail(y2 >= y1);
  g_return_if_fail(edgetiles.empty());
  g_return_if_fail(intiles.empty());
  g_return_if_fail(edgefuzz >= 0);

  bool inner_rect_has_area = true;
  if ((x2 - x1) <= edgefuzz*2 ||
      (y2 - y1) <= edgefuzz*2)
    {
      inner_rect_has_area = false;
    }

  double ix1 = x1 + edgefuzz;
  double iy1 = y1 + edgefuzz;
  double ix2 = x2 - edgefuzz;
  double iy2 = y2 - edgefuzz;

#ifdef GNOME_ENABLE_DEBUG
  if (inner_rect_has_area)
    {
      g_return_if_fail(ix2 > ix1);
      g_return_if_fail(iy2 > iy1);
    }
  else
    {
      g_return_if_fail(ix2 <= ix1 || iy2 <= iy1);
    }
#endif

  // Expand the original args to be the outer rectangle.
  x1 -= edgefuzz;
  y1 -= edgefuzz;
  x2 += edgefuzz;
  y2 += edgefuzz;

  vector<Band*>::const_iterator bi = tiles_.begin();
  vector<Band*>::const_iterator bend = tiles_.end();
  while (bi != bend)
    {
      // Skip all bands that we don't overlap.
      if ((*bi)->x2 < x1)
        {
          ++bi;
          continue;
        }
      else if ((*bi)->x1 > x2)
        {
          // Past any potential bands
          break;
        }
      else
        {
          vector<Tile*>::const_iterator ti = (*bi)->begin();
          vector<Tile*>::const_iterator tend = (*bi)->end();
          
          while (ti != tend)
            {
              const Tile* t = *ti;
              
              if (t->y2 < y1)
                {
                  ++ti;
                  continue;
                }
              else if (t->y1 > y2)
                {
                  // Past the last one
                  break;
                }
              else 
                {
                  if (inner_rect_has_area && 
                      t->x1 > ix1 && t->y1 > iy1 && 
                      t->x2 < ix2 && t->y2 < iy2)
                    {
                      // Tile is totally contained and doesn't overlap 
                      // the border region.
                      intiles.push_back(t);
                    }
                  else
                    {
                      edgetiles.push_back(t);
                    }
                }
              ++ti;
            }
        }

      ++bi;
    }
}
 
/*  
const PointTiles::Tile* 
PointTiles::tile_at_point(double x, double y) const
{
  return const_cast<PointTiles*>(this)->tile_at_point(x, y);
}
*/

PointTiles::Tile* 
PointTiles::tile_at_point(double x, double y)
{
  vector<Band*>::const_iterator bi = tiles_.begin();
  vector<Band*>::const_iterator bend = tiles_.end();
  while (bi != bend)
    {
      // Skip all bands that we don't overlap.
      if ((*bi)->x2 < x)
        {
          ++bi;
          continue;
        }
      else if ((*bi)->x1 > x)
        {
          // Past any potential bands
          break;
        }
      else 
        {
          vector<Tile*>::iterator ti = (*bi)->begin();
          vector<Tile*>::iterator tend = (*bi)->end();
          
          while (ti != tend)
            {
              Tile* t = *ti;
              
              if (t->y2 < y)
                {
                  ++ti;
                  continue;
                }
              else if (t->y1 > y)
                {
                  // Past the last one
                  break;
                }
              else 
                {
                  // Check the X values and return
                  // NOTE: the next tile(s) could *also* contain this point,
                  // if t->x2 == next_t->x1 - but for the purposes 
                  // of this function it isn't important.
                  if (x >= t->x1 && x <= t->x2)
                    return t;
                }
              ++ti;
            }
        }         
      
      ++bi;
    }

  return 0; // none found.
}

PointTiles::Tile* 
PointTiles::tile_at_point_and_index_in_band(double x, double y, guint index, PointTiles::Band* band)
{
  vector<Tile*>::iterator ti = band->begin();
  vector<Tile*>::iterator tend = band->end();
        
  while (ti != tend)
    {
      Tile* t = *ti;
              
      if (t->y2 < y)
        {
          ++ti;
          continue;
        }
      else if (t->y1 > y)
        {
          // Past the last one
          break;
        }
      else 
        {
          // Check the X values and return
          // NOTE: the next tile(s) could *also* contain this point,
          // if t->x2 == next_t->x1 - so we may need to 
          // disambiguate with index
          if (x >= t->x1 && x <= t->x2)
            {
              // Only a possibility - make sure, otherwise keep going.
              if (tile_contains_index(t, index))
                return t;
            }
        }

      ++ti;
    }
  
  return 0; // no possibility found
}

bool 
PointTiles::tile_contains_index(PointTiles::Tile* t, guint index)
{
  vector<guint>::const_iterator ii = t->contents.begin();
  while (ii != t->contents.end())
    {
      if (*ii == index)
        return true;
      ++ii;
    }
  return false;
}

//const PointTiles::Tile* 
PointTiles::Tile* 
PointTiles::tile_at_point_and_index(double x, double y, guint index)
{
  vector<Band*>::const_iterator bi = tiles_.begin();
  vector<Band*>::const_iterator bend = tiles_.end();

  while (bi != bend)
    {
      // Skip all bands that we don't overlap.
      if ((*bi)->x2 < x)
        {
          ++bi;
          continue;
        }
      else if ((*bi)->x1 > x)
        {
          // Past any potential bands
          break;
        }
      else 
        {
          // Try it - if not, then continue... next band(s) may overlap 
          Tile* t = tile_at_point_and_index_in_band(x, y, index, *bi);
	  
          if (t != 0)
            return t;
        }

      ++bi;
    }

  return 0; // none found - should not happen...
}

void 
PointTiles::update(guint index, double oldx, double oldy, double newx, double newy)
{
  Tile* ot = tile_at_point_and_index(oldx, oldy, index);
  Tile* nt = tile_at_point(newx, newy);

  if (ot == 0) 
    {
      g_warning("Request to update a point which is not on any existing tile!");
      check_invariants();  // try to get diagnostic info
      return;
    }

  if (ot == nt) return; 

  // Erase from old tile
  vector<guint>::iterator oti = ot->contents.begin();
  while (oti != ot->contents.end())
    {
      if (*oti == index)
        break;
      ++oti;
    }
 
  // If index and oldx and oldy were correct, we should have 
  // found the old index on this tile.
  g_assert(oti != ot->contents.end());
      
  ot->contents.erase(oti);

  // if (nt) just stick it in nt
  // else find which tiles we are in between and expand one of them
  // and its band.

  if (nt != 0)
    {
      vector<guint>::iterator nti = nt->contents.begin();
      while (nti != nt->contents.end())
        {
          if (*nti >= index)
            break;
          ++nti;
        }
      
      if (nti == nt->contents.end())
        {
          nt->contents.push_back(index);
        }
      else 
        {
          // shouldn't be equal, since ot != nt and 
          //  each index is only in one tile.
          g_assert(*nti > index);
      
          nt->contents.insert(nti, index);
        }
    }
  else 
    {
      // There was no tile where we wanted one to be; 
      // have to expand a tile.

      vector<Band*>::const_iterator bi = tiles_.begin();
      vector<Band*>::const_iterator blast = tiles_.end();
      vector<Band*>::const_iterator bend = tiles_.end();
      while (bi != bend)
        {
          blast = bi;
          // Skip all bands that we don't overlap.
          if ((*bi)->x2 < newx)
            {
              ++bi;
              continue;
            }
          else if ((*bi)->x1 > newx)
            {
              // Past any potential bands
              // We need to expand this band to the 
              // left and then go down it to find the tile.
              (*bi)->x1 = newx;
              break;
            }
          else 
            {
              break;
            }
          ++bi;
        }

      // See if we went past the end without finding a nice band.
      if (bi == bend)
        {
          bi = blast;
          // could happen if there were no bands, but that can't happen
          //  since we have a point to update.
          g_assert(bi != bend);
          g_assert((*bi)->x2 < newx);

          (*bi)->x2 = newx;
        }

      // We have a good band; now dive into it.
      vector<Tile*>::iterator ti = (*bi)->begin();
      vector<Tile*>::iterator tend = (*bi)->end();
      vector<Tile*>::iterator tlast = (*bi)->end();
      
      while (ti != tend)
        {
          tlast = ti;

          Tile* t = *ti;
          
          if (t->y2 < newy)
            {
              ++ti;
              continue;
            }
          else if (t->y1 > newy)
            {
              // Past the last one - expand the tile
              t->y1 = newy;
              break;
            }
          else 
            {
              break;
            }
          ++ti;
        } 

      // see if we went off the end sans tile
      if (ti == tend)
        {
          ti = tlast;

          // could happen if there were no tiles, but that can't happen
          //  since we have a point to update.
          g_assert(ti != tend);
          g_assert((*ti)->y2 < newy);

          (*ti)->y2 = newy;
        }

      Tile* t = *ti;

      // Now we have a tile; if it is not wide enough in the X direction,
      //  fix that.

      if (newx < t->x1)
        {
          t->x1 = newx;
        }
      else if (newx > t->x2)
        {
          t->x2 = newx;
        }

      g_assert(newx >= t->x1);
      g_assert(newx <= t->x2);
      g_assert(newy >= t->y1);
      g_assert(newy <= t->y2);

      g_assert(t->x1 >= (*bi)->x1);
      g_assert(t->x2 <= (*bi)->x2);

      // Add ourselves to this tile.

      vector<guint>::iterator nti = t->contents.begin();
      while (nti != t->contents.end())
        {
          if (*nti >= index)
            break;
          ++nti;
        }
      
      if (nti == t->contents.end())
        {
          t->contents.push_back(index);
        }
      else 
        {
          // shouldn't be equal, since ot != nt and 
          //  each index is only in one tile.
          g_assert(*nti > index);
      
          t->contents.insert(nti, index);
        }      

    } // else, there was no existing tile covering the new point. 

  check_invariants();
}

#ifdef GNOME_ENABLE_DEBUG
#include <set>
#endif

void
PointTiles::check_invariants()
{
#ifdef GNOME_ENABLE_DEBUG_TOOSLOW
  g_debug("Yet another really slow sanity check...");

  guint npoints = 0;
  set<guint> checkdups; // slow! slow! slow!

  vector<Band*>::const_iterator bi = tiles_.begin();
  vector<Band*>::const_iterator bend = tiles_.end();
  vector<Band*>::const_iterator bnext = bi + 1;
  while (bi != bend)
    {
      if (bnext != bend)
        {
          Band* lb = *bi;
          Band* rb = *bnext;

          g_assert(lb->x2 <= rb->x1);
        }
      
      vector<Tile*>::iterator ti = (*bi)->begin();
      vector<Tile*>::iterator tend = (*bi)->end();
      vector<Tile*>::iterator tnext = ti + 1;


      while (ti != tend)
        {
          if (tnext != tend)
            {
              Tile* upt = *ti;
              Tile* downt = *tnext;
              
              g_assert(upt->y2 <= downt->y1);
            }
          
          g_assert((*ti)->x1 >= (*bi)->x1);
          g_assert((*ti)->x2 <= (*bi)->x2);

          npoints += (*ti)->contents.size();

          vector<guint>::const_iterator ii = (*ti)->contents.begin();
          while (ii != (*ti)->contents.end())
            {
              if (checkdups.find(*ii) != checkdups.end())
                {
                  g_warning("%u is in the PointTiles twice!", *ii);
                  g_assert_not_reached();
                }

              checkdups.insert(*ii);

              ++ii;
            }

          ++ti;
          ++tnext;
        }         
      
      ++bi;
      ++bnext;
    }

  g_debug("%u points tiled... seems sane.", npoints);
#endif
}
