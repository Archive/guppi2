;; -*- scheme -*-
;;
;; Copyright (C) 1998 EMC Capital Management, Inc.
;;
;; Developed by Jon Trowbridge <trow@emccta.com> and
;; Havoc Pennington <hp@emccta.com>.
;;
;; This program is free software; you can redistribute it and/or 
;; modify it under the terms of the GNU General Public License as 
;; published by the Free Software Foundation; either version 2 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
;; USA
;;

;; This is the system-wide startup file for Guppi.

;; FIXME we need a better way to do the docs. It should be in C, and it should have i18n

(guppi-set-docs! 
 guppi-datastore 
 "Returns a handle for Guppi's global list of Data objects.")
(guppi-set-docs! 
 guppi-datastore->list 
 "Returns a list of Data objects contained in the datastore.")
(guppi-set-docs! 
 guppi-datastore-insert! 
 "Inserts the second argument (a data object) into the first (a data store object).")


;;;;;;; Create some tools!

;; Internal function, register a tool for plot type 'plot' with 
;; 'actionname' bound to button 1
(define (guppi-internal-create-tool plot actionname id icon)
  (define action (guppi-plotstore-get-action (guppi-plotstore) 
                                             plot
                                             actionname))
  (define at (guppi-make-actiontool id 
                                    (guppi-action-name action) 
                                    (guppi-action-tooltip action) 
                                    icon))
  (define tool (guppi-actiontool->tool at))

  (begin
    (guppi-plotstore-add-tool! (guppi-plotstore) plot tool)
    (guppi-tool-set-action! tool action
                            1 0)))

;; Default tools for the scatter plot.

(guppi-internal-create-tool "guppi-xy" "guppi-xy-telescope"
                            "telescope"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-axis-shrinker"
                            "axshrinker"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-axis-expander"
                            "axexpander"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-horizontal-panner"
                            "hpanner"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-vertical-panner"
                            "vpanner"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-brush"
                            "brush"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-pointid"
                            "pointid"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-styler"
                            "styler"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-recenter"
                            "recenter"
                            "fixme.png")


(guppi-internal-create-tool "guppi-xy" "guppi-xy-recenter-x"
                            "recenter-x"
                            "fixme.png")


(guppi-internal-create-tool "guppi-xy" "guppi-xy-recenter-y"
                            "recenter-y"
                            "fixme.png")

(guppi-internal-create-tool "guppi-xy" "guppi-xy-pointdrag"
                            "pointdrag"
                            "fixme.png")


;;;;;;;;;; Create some default entries in the scatterplot's marker palette
;;          (eventually we'll make these less lame...)

(define (guppi-internal-make-default-marker-palette)
  (define c (guppi-make-color))
  (define (make-style r g b name)
    (define ms (guppi-make-markerstyle))
    (begin
      (guppi-color-set! c r g b 255)
      (guppi-markerstyle-set-color! ms c)      
      (guppi-markerstyle-set-name! ms name)))

  (begin
    (make-style 255 0 0 "Red Pixel") 
    (make-style 0 255 0 "Green Pixel")
    (make-style 0 0 255 "Blue Pixel")))

(guppi-internal-make-default-marker-palette)


;;;;;;;;;;;;;;;;; Now add some menu entries...

;; Internal function, register a tool for plot type 'plot' with 
;; 'actionname' bound to button 1
(define (guppi-internal-create-menuentry plot id name tooltip code)
  (define me (guppi-make-menuentry id 
                                   name
                                   tooltip
                                   code))
  (begin
    (guppi-plotstore-add-menuentry! (guppi-plotstore) plot me)))

;; Default tools for the scatter plot.

(guppi-internal-create-menuentry "guppi-xy" 
                                 "guppi-xy-my-pointless-menu-item"
                                 "Pointless Guile Function"
                                 "Testing the ability to define menu items in scheme"
                                 (lambda () (display "Hello, World")))





