 // -*- C++ -*-

/* 
 * tooledit.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_TOOLEDIT_H
#define GUPPI_TOOLEDIT_H

// This is the window for selecting the current action on each 
//  button/modifier combo for a given tool.

#include "libguppi/plot.h"
#include <pair.h>

class ToolEdit {
public:  
  ToolEdit();
  ~ToolEdit();

  // Must call this before show()
  void set_tool(Tool* t);

  void show();
  void hide();
  
private:
  Tool* tool_;
  GtkWidget* window_;
  
  // one row of buttons
  class Row {
  public:
    Row(gint button, GdkModifierType mask, ToolEdit* owner, Tool* tool);
    ~Row();

    GtkWidget* widget();

    // can return 0
    const Action* active();

    gint button() { return button_; }
    GdkModifierType mask() { return mask_; }

  private:
    gint button_;
    GdkModifierType mask_;
    ToolEdit* owner_;
    Tool* tool_;
    GtkWidget* hbox_;

    typedef pair<const Action*,GtkWidget*> ActionButton;
    
    vector<ActionButton> buttons_;
    
    gint active_;
    
    bool self_toggle_; // don't respond to this toggle

    static void toggled_cb(GtkWidget* button, gpointer data);
    
    void toggled(GtkWidget* button);
  };

  vector<Row*> rows_;

  friend class Row;
  
  void row_changed(Row* r);  

  static gint delete_event_cb(GtkWidget* window, gpointer data);
};

#endif
