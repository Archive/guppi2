// -*- C++ -*-

/* 
 * plotlist.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_PLOTLIST_H
#define GUPPI_PLOTLIST_H

// This is actually a "Plot Shell List" but I didn't feel like typing
// that.

#include "guppi.h"
#include "plotshell.h"
#include <set>

class PlotList {
public:
  PlotList();
  virtual ~PlotList();

  void add(PlotShell* ps);
  
  void kill_me_please(PlotShell* ps);

  void get_plots(vector<Plot*> & plots);

private:
  set<PlotShell*> plots_;
  vector<PlotShell*> doomed_;

  void queue_cleanup();
  bool cleanup_pending_;
  guint idle_tag_;
  static gint deletion_idle(gpointer data);
  void cleanup();

};

PlotList* guppi_plotlist();

// Cleanup; called on program exit
void guppi_plotlist_shutdown();

// Create a new window for this plot, register it, etc.
PlotShell*
guppi_plot_shell_new(Plot* p);

#endif
