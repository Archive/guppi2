// -*- C++ -*-

/* 
 * guppi.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GUPPI_GUPPI_H
#define GUPPI_GUPPI_H

#include <config.h>

#include "libguppi/gnome-util.h"

#include <string>

void guppi_quit();
void guppi_setup_dialog(GtkWidget* dialog);

// Indicate that we're busy (clock cursor)
// Always sets busy on the main window; 
// if w is non-0, also sets/unsets the cursor on that widget.
void guppi_start_busy(GtkWidget* w = 0);
void guppi_end_busy(GtkWidget* w = 0);

void guppi_start_splash();
void guppi_end_splash();

#endif
