// -*- C++ -*-

/* 
 * corba_guppi.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CORBAGUPPI_H
#define GUPPI_CORBAGUPPI_H

#include "guppi.h"
extern "C" {
#include <libgnorba/gnorba.h>
           }

// return the ORB
CORBA_ORB guppi_orb();

// Return the environment
CORBA_Environment* guppi_corba_env();

// Return IOR for the Guile server
const gchar* guppi_guile_ior();

// Init things
void guppi_corba_init(int* argc, char** argv);
// Un-init things
void guppi_corba_shutdown();

// Pre-process errors; makes the code less CORBA-uglified.
// Also, no error is FALSE

typedef enum {
  GCE_NONE, // No error
  GCE_FATAL // The operation fatally didn't work
} GuppiCORBAErr;

GuppiCORBAErr guppi_corba_error(CORBA_Environment* ev);

// Get the error id from the last exception
const gchar* guppi_corba_strerror();


#endif
