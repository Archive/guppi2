// -*- C++ -*-

/* 
 * term.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "term.h"

#include <zvt/zvtterm.h>

#include "corba_guppi.h"

Term::Term()
  : dialog_(0), term_(0)
{
  
}

Term::~Term()
{
  close_term();
}

void
Term::close_term(bool already_destroyed)
{
  if (dialog_ && !already_destroyed) gtk_widget_destroy(dialog_);
  dialog_ = 0;
  term_ = 0;
}

void
Term::create_term()
{
  if (dialog_ != 0) 
    {
      // De-iconify and raise
      gdk_window_show(dialog_->window);
      gdk_window_raise(dialog_->window);
      return;
    }

  dialog_ = gnome_dialog_new(_("Guppi Interactive Guile Terminal"), 
                             GNOME_STOCK_BUTTON_CLOSE,
                             NULL);
  gnome_dialog_set_close(GNOME_DIALOG(dialog_), TRUE);

  guppi_setup_dialog(dialog_);

  term_ = zvt_term_new();
  zvt_term_set_size(ZVT_TERM(term_), 80, 25);

  zvt_term_set_scrollback(ZVT_TERM(term_), 10000);

  zvt_term_set_scroll_on_keystroke (ZVT_TERM(term_), TRUE);
  zvt_term_set_scroll_on_output (ZVT_TERM(term_), TRUE);

  gtk_signal_connect(GTK_OBJECT(dialog_),
                     "close",
                     GTK_SIGNAL_FUNC(close_cb),
                     this);

  gtk_signal_connect(GTK_OBJECT(term_),
                     "child_died",
                     GTK_SIGNAL_FUNC(child_died_cb),
                     this);

  GtkWidget* scrollbar = 
    gtk_vscrollbar_new (GTK_ADJUSTMENT (ZVT_TERM(term_)->adjustment));
  GTK_WIDGET_UNSET_FLAGS (scrollbar, GTK_CAN_FOCUS);

  GtkWidget* hbox = gtk_hbox_new(FALSE,0);

  gtk_box_pack_start(GTK_BOX(hbox), term_, 
                     TRUE, TRUE, 0);

  gtk_box_pack_end(GTK_BOX(hbox), scrollbar,
                   FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog_)->vbox),
                     hbox,
                     TRUE, TRUE, 0);   

  // Hack around term weirdness
  gtk_widget_realize(term_);
  
  gtk_widget_show_now(term_);

  while (gtk_events_pending())
    gtk_main_iteration();

  gtk_widget_show_all(dialog_);

  gtk_widget_show_now(dialog_);

  while (gtk_events_pending())
    gtk_main_iteration();

  while (gtk_events_pending())
    gtk_main_iteration();

  // make sure we are all synced up here. weirdo paranoia.
  fflush(stdout);
  fflush(stderr);

  int pid = zvt_term_forkpty (ZVT_TERM(term_), FALSE); // FALSE == no utmp
  switch (pid) {
  case -1:
    perror ("Error: unable to fork");
    break;
  case 0: 
    {
      // touching the GUI == flaming death in the child
      dialog_ = 0;
      term_ = 0;    
 
      gchar* path = gnome_is_program_in_path("gnome-guile-repl");
#ifdef GNOME_ENABLE_DEBUG
      if (path == 0)
        {
          if (g_file_exists("../guile-server/gnome-guile-repl"))
            path = g_strdup("../guile-server/gnome-guile-repl");
        }
#endif
      int err = 0;
      if (path != 0)
        {
          gchar* const argv[3] = {(gchar* const)path,
				  (gchar* const)guppi_guile_ior()};
          printf("Launching the Guile REPL with IOR: %s\n", argv[1]);
          err = execv(path, argv);
        }
      
      if (err != 0)
        fprintf(stderr, _("Failed to execute %s\n"), path);
      else 
        fprintf(stderr, _("gnome-guile-repl not found in path (maybe you need to make install)"));
      
      g_free(path);
     
      fflush(stderr);

      sleep(10); // so we can see the error, user can close the dialog though
 
      _exit(1);
    }
    break;
  default:
    // parent
    gtk_widget_show_all(dialog_);

    while (gtk_events_pending())
      gtk_main_iteration();

    break;
  }
}


gint 
Term::close_cb(GtkWidget* w, gpointer data)
{
  Term* t = static_cast<Term*>(data);
  t->close_term(true);
  return FALSE;
}

void 
Term::child_died_cb(GtkWidget* w, gpointer data)
{
  Term* t = static_cast<Term*>(data);

  // make sure stuff gets on screen or something, i dunno
  while (gtk_events_pending())
    gtk_main_iteration();

  sleep(1); // just long enough to read any errors

  t->close_term(false);
}
