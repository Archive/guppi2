;;; This is just a scratch pad for whatever I am testing right now.
;;; I checked it in since it's some sample Guppi scripting code.
;;; To use it, run (guppi-safe-load "testing.scm") from the Guppi terminal.

(define at (guppi-make-actiontool "mytool" "Custom Tool" "Does some stuff" "fixme.png"))

(define tool (guppi-actiontool->tool at))

(guppi-plotstore-add-tool! (guppi-plotstore) "guppi-xy" tool)

(guppi-plotstore-set-current-tool! (guppi-plotstore) "guppi-xy" tool)

(guppi-tool-set-action! tool (guppi-plotstore-get-action (guppi-plotstore) "guppi-xy" "guppi-xy-telescope") 1 0)

(define xy (guppi-make-xy))
(define s (guppi-xy-make-scatter-layer! xy))

; (define s (guppi-xy-make-scatter-layer! xy))
; (define s (guppi-xy-make-scatter-layer! xy))
; (define s (guppi-xy-make-scatter-layer! xy))
; (define s (guppi-xy-make-scatter-layer! xy))
; (define s (guppi-xy-make-scatter-layer! xy))
; (define s (guppi-xy-make-scatter-layer! xy))
; (define s (guppi-xy-make-scatter-layer! xy))

(guppi-xy-set-width! xy 400)
(guppi-xy-set-height! xy 600)

;;(guppi-scatter-set-x-start! s -300.0)
;;(guppi-scatter-set-x-stop! s 300.0)

;;(guppi-scatter-set-y-start! s -300.0)
;;(guppi-scatter-set-y-stop! s 300.0)

(define p (guppi-xy-make-plot xy))

(guppi-plot-make-window p)

(define c (guppi-make-color))
(guppi-color-set! c 255 0 0 255)

;; Fill palette past 255 entries, so we switch to two bytes per 
;;  palette index and make sure that works.
(define (fill-palette-with-junk)
  (define (loop i)
    (cond ((< i 0)
           #f)
          (#t
           (begin
             (guppi-make-markerstyle)
             (loop (- i 1))))))
  (loop 260))

(fill-palette-with-junk)

(define ms (guppi-make-markerstyle))
(guppi-markerstyle-set-color! ms c)

;;(guppi-scatter-set-global-style! s ms)

(define (make-center-points-vector scatter)
  (define (loop vect i val max)
    (cond ((>= val max)
           vect)
           (#t
            (loop (begin (vector-set! vect i val) 
                         vect)
                  (+ i 1) 
                  (+ val 1) 
                  max))))

  (let* ((npoints (guppi-xylayer-npoints scatter))
         (q1 (quotient npoints 4))
         (q3 (* (quotient npoints 4) 3))
         (vec (make-vector (- q3 q1))))
    (loop vec 0 q1 q3)))

(define (make-middle-red scatter)
  (guppi-xylayer-set-points-style! scatter ms (make-center-points-vector scatter)))

(define (finite-traverse setfunc scatter l)
  (cond 
   ((null? l)
    #f)
   (#t
    (begin
      (setfunc scatter (guppi-data->scalardata (car l)))
      (make-middle-red scatter)
      (display (car l))
      (newline)
      (guppi-permit-gui)
      (finite-traverse setfunc scatter (cdr l))))))

(define (loop-over func1 func2 scatter datalist)
  (letrec ((loop
            (lambda (rest) 
              (inner (cdr rest))))
           (inner
            (lambda (rest)
              (cond 
               ((null? rest) 
                #f)
               (#t
                (begin
                  (func1 scatter (guppi-data->scalardata (car rest)))
                  (display "X: ")
                  (newline)
                  (display (car rest))                  
                  (newline)
                  (display "Ys: ")
                  (newline)
                  (guppi-permit-gui) 
                  (finite-traverse func2 scatter datalist)
                  (loop rest)))))))
    (inner datalist)))
    
(define (do-forever thunk)
  (begin
    (thunk)
    (do-forever thunk)))

(define (once)
  (loop-over 
   guppi-xylayer-set-x! 
   guppi-xylayer-set-y! 
   s
   (guppi-datastore->list (guppi-datastore))))

(define (go) 
  (do-forever 
   once))
