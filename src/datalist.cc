// -*- C++ -*-

/* 
 * datalist.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "datalist.h"

#include "plotshell.h"
#include "libguppi/barplot.h"
#include "guppidata.h"
#include "plotlist.h"
#include "guppixmltags.h"
#include "libguppi/scalardata.h"
#include "goose/RealSet.h"

DataList::DataList(DataStore* ds)
  : store_(0), sw_(0), clist_(0)
{
  g_return_if_fail(ds != 0);

  sw_ = gtk_scrolled_window_new(NULL,NULL);
 
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw_), 
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);
 
  char* titles[] = {
    _("Name"),
    _("N"),
    _("Mean"),
    _("Std. Dev."),
    _("Maximum"),
    _("Minimum")
  };

  clist_ = gtk_clist_new_with_titles(sizeof(titles)/sizeof(titles[0]), 
                                     titles);

  gtk_container_add(GTK_CONTAINER(sw_), clist_);

  gtk_signal_connect(GTK_OBJECT(clist_),
                     "select_row",
                     GTK_SIGNAL_FUNC(select_row_cb),
                     this);

  set_store(ds);
}

void 
DataList::set_store(DataStore* ds)
{
  release_store();

  store_ = ds;

  gtk_clist_freeze(GTK_CLIST(clist_));
  DataStore::iterator i = store_->begin();
  while (i != store_->end())
    {
      append_data(*i);
      ++i;
    }
  gtk_clist_thaw(GTK_CLIST(clist_));

  store_->store_model.add_view(this);
}

void 
DataList::release_store()
{
  if (store_ != 0)
    {
      store_->store_model.remove_view(this);
      store_ = 0;
    }
}

DataList::~DataList()
{
  release_store();
}

GtkWidget* 
DataList::widget()
{
  return sw_;
}

void 
DataList::append_data(Data* d)
{
  g_return_if_fail(d != 0);

  char* text[6];
  text[0] = const_cast<char*>(d->name().c_str());

  // These have to be in scope when we append
  // Each one needs its own buffer, since we 
  //  need all the buffers at once to stuff into the
  //  CList.
  
  gchar sizebuf[100];
  gchar meanbuf[100];
  gchar sdevbuf[100];
  gchar maxbuf[100];
  gchar minbuf[100];

  if (d->is_a(Data::Group))
    {
      text[1] = "-";
    }
  else
    {
      g_snprintf(sizebuf,100,"%u",d->size());
      text[1] = &sizebuf[0];
    }  

  if (d->is_a(Data::Scalar))
    {
      ScalarData* sd = d->cast_to_scalar();

      g_assert(sd != 0);

      if (sd->realset()->size() > 0)
        {
          g_snprintf(meanbuf,100,"%.3g",sd->realset()->mean());
          text[2] = &meanbuf[0];
          
          g_snprintf(sdevbuf,100,"%.3g",sd->realset()->sdev());
          text[3] = &sdevbuf[0];
          
          g_snprintf(maxbuf,100,"%.3g",sd->realset()->max());
          text[4] = &maxbuf[0];
          
          g_snprintf(minbuf,100,"%.3g",sd->realset()->min());
          text[5] = &minbuf[0];  
        }
      else
        {
          text[2] = text[3] = text[4] = text[5] = "-";
        }
    }
  else
    {
      text[2] = text[3] = text[4] = text[5] = "-";
    }

  gint row = gtk_clist_append(GTK_CLIST(clist_), text);
  
  gtk_clist_set_row_data(GTK_CLIST(clist_), row, d);

  // If we were really anal, we'd do this more often, but we're not.
  //  Well, I'm not.
  gtk_clist_columns_autosize(GTK_CLIST(clist_));
}

void 
DataList::add_data(DataStore* ds, Data* d)
{
  append_data(d);
}

void 
DataList::remove_data(DataStore* ds, Data* d)
{
  gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_), d);
  g_warn_if_fail(row >= 0);
  if (row == -1) return; // want to leave this in always, even with debugging off

  gtk_clist_remove(GTK_CLIST(clist_), row);

  if (edit_.data() == d)
    edit_.set_data(0);
}

void
DataList::update_numbers(gint row, Data* d)
{
  if (d->is_a(Data::Scalar))
    {
      gchar buf[100];
      ScalarData* sd = d->cast_to_scalar();

      g_assert(sd != 0);

      // Each one needs its own buffer, since we 
      //  need all the buffers at once to stuff into the
      //  CList.

      g_snprintf(buf,100,"%u",sd->realset()->size());

      gtk_clist_set_text(GTK_CLIST(clist_), row, 1, buf);
      
      if (sd->realset()->size() > 0)
        {
          g_snprintf(buf,100,"%.3g",sd->realset()->mean());
          
          gtk_clist_set_text(GTK_CLIST(clist_), row, 2, buf);
          
          g_snprintf(buf,100,"%.3g",sd->realset()->sdev());
          
          gtk_clist_set_text(GTK_CLIST(clist_), row, 3, buf);      
          
          g_snprintf(buf,100,"%.3g",sd->realset()->max());
          
          gtk_clist_set_text(GTK_CLIST(clist_), row, 4, buf);      
          
          g_snprintf(buf,100,"%.3g",sd->realset()->min());
          
          gtk_clist_set_text(GTK_CLIST(clist_), row, 5, buf);       
        }
      else 
        {
          int i = 2;
          while (i < 6)
            {
              gtk_clist_set_text(GTK_CLIST(clist_), row, i, "");
              ++i;
            }
        }
    }
}

void 
DataList::change_data_values(DataStore* ds, Data* d)
{
  gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_), d);
  g_warn_if_fail(row >= 0);
  if (row == -1) return; // want to leave this in always, even with debugging off
  
  update_numbers(row, d);
}

void 
DataList::change_data_values(DataStore* ds, Data* d, const vector<guint> & which)
{
  gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_), d);
  g_warn_if_fail(row >= 0);
  if (row == -1) return; // want to leave this in always, even with debugging off
  
  update_numbers(row, d);
}

void 
DataList::change_data_name(DataStore* ds, Data* d, const string & name)
{
  gint row = gtk_clist_find_row_from_data(GTK_CLIST(clist_), d);
  g_warn_if_fail(row >= 0);
  if (row == -1) return; // want to leave this in always, even with debugging off

  gtk_clist_set_text(GTK_CLIST(clist_), row, 0, d->name().c_str());
}

void 
DataList::destroy_model()
{
  store_ = 0;

  gtk_clist_clear(GTK_CLIST(clist_));
}


void 
DataList::select_row_cb(GtkWidget* w, int row, int col, 
                        GdkEvent* event, gpointer data)
{
  DataList* dl = static_cast<DataList*>(data);

  dl->select_row(row);
}

void 
DataList::select_row(int row)
{
  gpointer data = gtk_clist_get_row_data(GTK_CLIST(clist_), row);

  g_return_if_fail(data != 0);

  Data* d = static_cast<Data*>(data);

  edit_.set_data(d);
}

xmlNodePtr 
DataList::xml(xmlNodePtr parent) const
{
  xmlNodePtr dl = 0;
  xmlNodePtr store = 0;

  dl = xmlNewChild(parent, NULL, DATA_LIST_TAG, NULL);

  if (store_ != 0)
    {
      store = store_->xml(dl);
    }

  return dl;
}


void 
DataList::set_xml(xmlNodePtr node)
{
  g_return_if_fail(node != 0);
  
  if (node->name == 0 || (g_ustrcmp(node->name, DATA_LIST_TAG) != 0))
    {
      g_warning("Bad node to DataList::set_xml()");
      return;
    }

  DataStore* ds = guppi_datastore();
  
  xmlNodePtr child = node->childs;
  while (child != 0)
    {
      if (child->name != 0)
        {
          if (g_ustrcmp(child->name, DATA_STORE_TAG) == 0)
            {
              ds->set_xml(child);
            }
        }
      
      child = child->next;
    }
}


