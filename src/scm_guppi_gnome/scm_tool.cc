// -*- C++ -*-

/* 
 * scm_tool.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/tool.h"

#include "scm_guppi_gnome.h"

///////////////////// Scheme for Tool and Action objects

static long action_type_tag;

#define SCM_TO_ACTION(obj) (reinterpret_cast<Action*>(SCM_CDR(obj)))
#define ACTION_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == action_type_tag)

bool     
scm_actionp (SCM obj)
{
  return ACTION_P(obj);
}

Action* 
scm2action (SCM obj)
{
  if (!ACTION_P(obj)) return 0;
  return SCM_TO_ACTION(obj);
}

SCM      
action2scm (Action* act)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, act);
  SCM_SETCAR (smob, action_type_tag);

  act->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_action (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_action (SCM obj)
{
  Action* act = SCM_TO_ACTION(obj);

  // This is problematic, so we'll just not report it.
  //  should be small enough not to matter...
  static const scm_sizet size = 0;

  gh_defer_ints();

  if (act->unref() == 0)
    delete act;

  gh_allow_ints();

  return size;
}

static int
print_action (SCM obj, SCM port, scm_print_state *pstate)
{
  Action* act = SCM_TO_ACTION(obj);

  string rep;

  rep += "#<Action: ";
  rep += act->name();
  rep += " id: ";
  rep += act->id();
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns action_funcs = {
  mark_action, 
  free_action, 
  print_action, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on Action

GUPPI_PROC(actionp,"guppi-action?",1,0,0,(SCM action))
{
  return gh_bool2scm(ACTION_P(action));
}

GUPPI_PROC(actionname,"guppi-action-name",1,0,0,(SCM action))
{
  SCM_ASSERT(ACTION_P(action), action, SCM_ARG1, "guppi-action-name");  

  Action* a = SCM_TO_ACTION(action);

  return gh_str02scm((gchar*)a->name().c_str());
}


GUPPI_PROC(actiontooltip,"guppi-action-tooltip",1,0,0,(SCM action))
{
  SCM_ASSERT(ACTION_P(action), action, SCM_ARG1, "guppi-action-tooltip");  

  Action* a = SCM_TO_ACTION(action);

  return gh_str02scm((gchar*)a->tooltip().c_str());
}


/////////////////////// Tool


static long tool_type_tag;

#define SCM_TO_TOOL(obj) (reinterpret_cast<Tool*>(SCM_CDR(obj)))
#define TOOL_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == tool_type_tag)

bool     
scm_toolp (SCM obj)
{
  return TOOL_P(obj);
}

Tool* 
scm2tool (SCM obj)
{
  if (!TOOL_P(obj)) return 0;
  return SCM_TO_TOOL(obj);
}

SCM      
tool2scm (Tool* tl)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, tl);
  SCM_SETCAR (smob, tool_type_tag);

  tl->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_tool (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_tool (SCM obj)
{
  Tool* tl = SCM_TO_TOOL(obj);

  // This is problematic, so we'll just not report it.
  //  should be small enough not to matter...
  static const scm_sizet size = 0;

  gh_defer_ints();

  if (tl->unref() == 0)
    delete tl;

  gh_allow_ints();

  return size;
}

static int
print_tool (SCM obj, SCM port, scm_print_state *pstate)
{
  Tool* tl = SCM_TO_TOOL(obj);

  string rep;
  gchar buf[128];

  rep += "#<Tool: ";
  rep += tl->name();
  rep += " id: ";
  rep += tl->id();
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns tool_funcs = {
  mark_tool, 
  free_tool, 
  print_tool, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on Tool

GUPPI_PROC(toolp,"guppi-tool?",1,0,0,(SCM tool))
{
  return gh_bool2scm(TOOL_P(tool));
}


GUPPI_PROC(toolsetaction,"guppi-tool-set-action!",4,0,0,(SCM tool, SCM action, 
                                                         SCM button, SCM modmask))
{
  SCM_ASSERT(TOOL_P(tool), tool, SCM_ARG1, "guppi-tool-set-action!");
  SCM_ASSERT(ACTION_P(action), action, SCM_ARG2, "guppi-tool-set-action!");
  SCM_ASSERT(gh_number_p(button), button, SCM_ARG3, "guppi-tool-set-action!");
  //  SCM_ASSERT(modmask, modmask, SCM_ARG4, "guppi-tool-set-action!");

  gh_defer_ints();

  SCM obj;

  Tool* tl = SCM_TO_TOOL(tool);

  Action* a = SCM_TO_ACTION(action);

  int butnum = gh_scm2int(button);

  if (butnum < 1 || butnum > 5)
    {
      gh_allow_ints();
      guppi_scm_error("guppi-tool-set-action!", _("Button number out of range; must be between 1 and 5"));
    }

  // FIXME don't ignore the modmask! (figure out what type it is...)
  
  tl->set_action(a, butnum, (GdkModifierType)0);

  gh_allow_ints();
  return SCM_UNSPECIFIED;
}

///////////// Action Tool

static long actiontool_type_tag;

#define SCM_TO_ACTIONTOOL(obj) (reinterpret_cast<ActionTool*>(SCM_CDR(obj)))
#define ACTIONTOOL_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == actiontool_type_tag)

bool     
scm_actiontoolp (SCM obj)
{
  return ACTIONTOOL_P(obj);
}

ActionTool* 
scm2actiontool (SCM obj)
{
  if (!ACTIONTOOL_P(obj)) return 0;
  return SCM_TO_ACTIONTOOL(obj);
}

SCM      
actiontool2scm (ActionTool* tl)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, tl);
  SCM_SETCAR (smob, actiontool_type_tag);

  tl->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_actiontool (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_actiontool (SCM obj)
{
  ActionTool* tl = SCM_TO_ACTIONTOOL(obj);

  // This is problematic, so we'll just not report it.
  //  should be small enough not to matter...
  static const scm_sizet size = 0;

  gh_defer_ints();

  if (tl->unref() == 0)
    delete tl;

  gh_allow_ints();

  return size;
}

static int
print_actiontool (SCM obj, SCM port, scm_print_state *pstate)
{
  ActionTool* tl = SCM_TO_ACTIONTOOL(obj);

  string rep;
  gchar buf[128];

  rep += "#<ActionTool: ";
  rep += tl->name();
  rep += " id: ";
  rep += tl->id();
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns actiontool_funcs = {
  mark_actiontool, 
  free_actiontool, 
  print_actiontool, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on ActionTool

GUPPI_PROC(actiontoolp,"guppi-actiontool?",1,0,0,(SCM actiontool))
{
  return gh_bool2scm(ACTIONTOOL_P(actiontool));
}

GUPPI_PROC(makeactiontool,"guppi-make-actiontool",4,0,0,(SCM id, SCM name, SCM tooltip, SCM icon))
{
  SCM_ASSERT(gh_string_p(id), id, SCM_ARG1, "guppi-make-action-tool");
  SCM_ASSERT(gh_string_p(name), name, SCM_ARG2, "guppi-make-action-tool");
  SCM_ASSERT(gh_string_p(tooltip), tooltip, SCM_ARG3, "guppi-make-action-tool");
  SCM_ASSERT(gh_string_p(icon), icon, SCM_ARG4, "guppi-make-action-tool");

  gh_defer_ints();

  SCM obj;

  char* ids = gh_scm2newstr(id, NULL);
  char* names = gh_scm2newstr(name, NULL);
  char* tooltips = gh_scm2newstr(tooltip, NULL);
  char* icons = gh_scm2newstr(icon, NULL);

  ActionTool* at = new ActionTool(ids, names, tooltips, icons);

  free(ids);
  free(names);
  free(tooltips);
  free(icons);

  obj = actiontool2scm(at);
  
  gh_allow_ints();
  return obj;
}


GUPPI_PROC(actiontool2tool,"guppi-actiontool->tool",1,0,0,(SCM actiontool))
{
  SCM_ASSERT(ACTIONTOOL_P(actiontool), actiontool, SCM_ARG1, "guppi-actiontool->tool");

  gh_defer_ints();

  SCM obj;

  ActionTool* at = SCM_TO_ACTIONTOOL(actiontool);

  obj = tool2scm(at);
  
  gh_allow_ints();
  return obj;
}

//////////////// Init

void
init_scm_tool()
{
  tool_type_tag = scm_newsmob(&tool_funcs);
  action_type_tag = scm_newsmob(&action_funcs);
  actiontool_type_tag = scm_newsmob(&actiontool_funcs);

#include "scm_tool.x"
}




