// -*- C++ -*-

/* 
 * scm_xy.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/xyplotstate.h"
#include "libguppi/xyplot.h"
#include "libguppi/scalardata.h"
#include "libguppi/markerstyle.h"

#include "scm_guppi_gnome.h"

extern SCM plot2scm(Plot*);

// FIXME eventually should make a XyPlot smob, not a Plot
GUPPI_PROC(make_xy_plot,"guppi-xy-make-plot",1,0,0,(SCM xy))
{
  SCM_ASSERT(scm_xyp(xy), xy, SCM_ARG1, "guppi-xy-make-plot");

  XyPlotState* sc = scm2xy(xy);  

  gh_defer_ints();

  SCM plot = plot2scm(new XyPlot(sc));

  gh_allow_ints();

  return plot;
}

//////////////// Init

void
init_scm_xy()
{

#include "scm_xy.x"
}
