// -*- C++ -*-

/* 
 * scm_tool.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/menuentry.h"

#include "scm_guppi_gnome.h"


// A special MenuEntry subclass for scheme...
class SchemeMenuEntry : public MenuEntry {
public:
  static const guint32 SCHEME_TYPEMAGIC;

  SchemeMenuEntry(const char* id, const char* name, const char* tooltip) 
    : MenuEntry(id, name, tooltip), code_(SCM_BOOL_F) {}
  virtual ~SchemeMenuEntry() {}

  void set_scheme_code(SCM thunk) {
    code_ = thunk;
  }

  // Do the GC thing
  void mark() {
    if (code_ != SCM_BOOL_F)
      scm_gc_mark(code_);
  }

  virtual void invoke(Plot* p) {
    string output;
    string error;
    string result;
    guppi_safe_apply_thunk_with_output(code_,
                                       output, error, result);

    // FIXME eventually this can be a lot nicer; result
    // should be shown in statusbar for example...

    if (!error.empty())
      {
        GtkWidget* dialog = gnome_error_dialog(error.c_str());
        gtk_window_set_title(GTK_WINDOW(dialog), 
                             _("Guile Command Error"));
      }
    if (!output.empty())
      {
        GtkWidget* dialog = gnome_ok_dialog(output.c_str());
        gtk_window_set_title(GTK_WINDOW(dialog), 
                             _("Guile Command Output"));
      }
  }

private:
  SCM code_;
};

// Any old number
const guint32 SchemeMenuEntry::SCHEME_TYPEMAGIC = 0xff00ff00;

///////////////////// Scheme for adding custom menu items.
///////////////////// First, MenuEntry for the Plot popup menu...

static long menuentry_type_tag;

#define SCM_TO_MENUENTRY(obj) (reinterpret_cast<MenuEntry*>(SCM_CDR(obj)))
#define MENUENTRY_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == menuentry_type_tag)

bool     
scm_menuentryp (SCM obj)
{
  return MENUENTRY_P(obj);
}

MenuEntry* 
scm2menuentry (SCM obj)
{
  if (!MENUENTRY_P(obj)) return 0;
  return SCM_TO_MENUENTRY(obj);
}

SCM      
menuentry2scm (MenuEntry* me)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, me);
  SCM_SETCAR (smob, menuentry_type_tag);

  me->ref();

  gh_allow_ints();
  return smob;
}

// mark the contained thunk if our menu entry is defined in Scheme
static SCM
mark_menuentry (SCM obj)
{
  MenuEntry* me = SCM_TO_MENUENTRY(obj);

  if (me->typemagic() == SchemeMenuEntry::SCHEME_TYPEMAGIC)
    {
      SchemeMenuEntry* sme = static_cast<SchemeMenuEntry*>(me);

      sme->mark();
    }

  return SCM_BOOL_F; // not important
}

static scm_sizet
free_menuentry (SCM obj)
{
  MenuEntry* me = SCM_TO_MENUENTRY(obj);

  // This is problematic, so we'll just not report it.
  //  should be small enough not to matter...
  static const scm_sizet size = 0;

  gh_defer_ints();

  if (me->unref() == 0)
    delete me;

  gh_allow_ints();

  return size;
}

static int
print_menuentry (SCM obj, SCM port, scm_print_state *pstate)
{
  MenuEntry* me = SCM_TO_MENUENTRY(obj);

  string rep;
  gchar buf[128];

  rep += "#<MenuEntry: ";
  rep += me->name();
  rep += " id: ";
  rep += me->id();
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns menuentry_funcs = {
  mark_menuentry, 
  free_menuentry, 
  print_menuentry, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on MenuEntry

GUPPI_PROC(menuentryp,"guppi-menuentry?",1,0,0,(SCM menuentry))
{
  return gh_bool2scm(MENUENTRY_P(menuentry));
}

GUPPI_PROC(make_menuentry,"guppi-make-menuentry",4,0,0,(SCM id, SCM name, SCM tooltip, SCM code))
{
  SCM_ASSERT(gh_string_p(id), id, SCM_ARG1, "guppi-make-menuentry");
  SCM_ASSERT(gh_string_p(name), name, SCM_ARG2, "guppi-make-menuentry");
  SCM_ASSERT(gh_string_p(tooltip), tooltip, SCM_ARG3, "guppi-make-menuentry");
  SCM_ASSERT(gh_procedure_p(code), code, SCM_ARG4, "guppi-make-menuentry");

  gh_defer_ints();
  
  char* id_s = gh_scm2newstr(id, NULL);
  char* name_s = gh_scm2newstr(name, NULL);
  char* tooltip_s = gh_scm2newstr(tooltip, NULL);

  SchemeMenuEntry* sme = new SchemeMenuEntry(id_s, name_s, tooltip_s);

  free(id_s);
  free(name_s);
  free(tooltip_s);

  sme->set_scheme_code(code);

  gh_allow_ints();
  return menuentry2scm(sme);
}

//////////////// Init

void
init_scm_menuentry()
{
  menuentry_type_tag = scm_newsmob(&menuentry_funcs);

#include "scm_menuentry.x"
}




