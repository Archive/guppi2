// -*- C++ -*-

/* 
 * scm_guppi_gnome.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "scm_guppi_gnome.h"


GUPPI_PROC(guppi_permit_gui,"guppi-permit-gui",0,0,0,())
{
  while (gtk_events_pending())
    gtk_main_iteration();

  // Just once more to let idle functions kick in - 
  // FIXME there is probably a better/working way.
  // I think idle functions kick in anyway.
  // Note that we are worried about the one-shot variety,
  // as in the canvas update.
  //  gtk_main_iteration();

  return SCM_UNSPECIFIED;
}

// These are here to avoid tons of pointless header files.
extern void init_scm_xy();
extern void init_scm_plot();
extern void init_scm_plotstore();
extern void init_scm_tool();
extern void init_scm_menuentry();

// Anything registered in here will be in the "Guppi" module
static void 
real_init()
{
  guppi_scm_init();

#include "scm_guppi_gnome.x"

  init_scm_plot();
  init_scm_xy();
  init_scm_plotstore();
  init_scm_tool();
  init_scm_menuentry();
}

void 
guppi_gnome_scm_init ()
{
  real_init();
}

