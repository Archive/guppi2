// -*- C++ -*-

/* 
 * scm_plotstore.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/plotstore.h"
#include "libguppi/tool.h"

#include "scm_guppi_gnome.h"

///////////////////// Scheme for the Plot interface


static long plotstore_type_tag;

#define SCM_TO_PLOTSTORE(obj) (reinterpret_cast<PlotStore*>(SCM_CDR(obj)))
#define PLOTSTORE_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == plotstore_type_tag)

bool     
scm_plotstorep (SCM obj)
{
  return PLOTSTORE_P(obj);
}

PlotStore* 
scm2plotstore (SCM obj)
{
  if (!PLOTSTORE_P(obj)) return 0;
  return SCM_TO_PLOTSTORE(obj);
}

SCM      
plotstore2scm (PlotStore* plt)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, plt);
  SCM_SETCAR (smob, plotstore_type_tag);


  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_plotstore (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_plotstore (SCM obj)
{
  PlotStore* plt = SCM_TO_PLOTSTORE(obj);

  static const scm_sizet size = 0;

  gh_defer_ints();

  // just a handle, we don't free the global PlotStore...

  gh_allow_ints();

  return size;
}

static int
print_plotstore (SCM obj, SCM port, scm_print_state *pstate)
{
  PlotStore* plt = SCM_TO_PLOTSTORE(obj);

  string rep;
  gchar buf[128];

  rep += "#<Guppi global PlotStore object>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns plotstore_funcs = {
  mark_plotstore, 
  free_plotstore, 
  print_plotstore, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on PlotStore

GUPPI_PROC(plotstorep,"guppi-plotstore?",1,0,0,(SCM plotstore))
{
  return gh_bool2scm(PLOTSTORE_P(plotstore));
}


GUPPI_PROC(get_plotstore,"guppi-plotstore",0,0,0,())
{
  return plotstore2scm(guppi_plot_store());
}

// Return a list of all actions available on this plot or plot ID
GUPPI_PROC(plotstoreactions,"guppi-plotstore-plot-actions",2,0,0,(SCM plotstore, SCM plot))
{
  SCM_ASSERT(PLOTSTORE_P(plotstore), 
             plotstore, SCM_ARG1, "guppi-plotstore-plot-actions");


  SCM_ASSERT((gh_string_p(plot) || scm_plotp(plot)), 
             plot, SCM_ARG2, "guppi-plotstore-plot-actions");

  gh_defer_ints();
  
  PlotStore* ps = SCM_TO_PLOTSTORE(plotstore);

  string id;
  
  if (gh_string_p(plot))
    {
      char* str = gh_scm2newstr(plot, NULL);

      g_assert(str != 0);

      id = str;

      free(str);
    }
  else 
    {
      Plot* p = scm2plot(plot);
      
      id = p->id();
    }

  if (!ps->plot_exists(id))
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plotstore-plot-actions", _("Unknown plot type"));
    }

  vector<Action*> vec = ps->actions(id);
  
  SCM list = SCM_EOL;
  SCM *pos;

  pos = &list;

  vector<Action*>::iterator i = vec.begin();
  while (i != vec.end())
    {
      *pos = scm_cons (action2scm(*i), SCM_EOL);
      pos = SCM_CDRLOC (*pos);
      
      ++i;
    }

#if 0
  SCM list = SCM_LIST0;

  vector<Action*>::iterator i = vec.begin();
  while (i != vec.end())
    {
      list = gh_cons(action2scm(*i), list);

      ++i;
    }
#endif  

  gh_allow_ints();

  return list;
}

// Make a new tool available
GUPPI_PROC(plotstoreaddtool,"guppi-plotstore-add-tool!",3,0,0,(SCM plotstore, SCM plot, SCM tool))
{
  SCM_ASSERT(PLOTSTORE_P(plotstore), 
             plotstore, SCM_ARG1, "guppi-plotstore-add-tool!");

  SCM_ASSERT((gh_string_p(plot) || scm_plotp(plot)), 
             plot, SCM_ARG2, "guppi-plotstore-add-tool!");

  SCM_ASSERT(scm_toolp(tool), 
             tool, SCM_ARG3, "guppi-plotstore-add-tool!");


  gh_defer_ints();
  
  PlotStore* ps = SCM_TO_PLOTSTORE(plotstore);

  string id;
  
  if (gh_string_p(plot))
    {
      char* str = gh_scm2newstr(plot, NULL);

      g_assert(str != 0);

      id = str;

      free(str);
    }
  else 
    {
      Plot* p = scm2plot(plot);
      
      id = p->id();
    }

  if (!ps->plot_exists(id))
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plotstore-add-tool!", _("Unknown plot type"));
    }

  ps->add_tool(id, scm2tool(tool));

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


// Set the current tool for a plot type
GUPPI_PROC(plotstoresetcurrenttool,"guppi-plotstore-set-current-tool!",3,0,0,(SCM plotstore, SCM plot, SCM tool))
{
  SCM_ASSERT(PLOTSTORE_P(plotstore), 
             plotstore, SCM_ARG1, "guppi-plotstore-set-current-tool!");

  SCM_ASSERT((gh_string_p(plot) || scm_plotp(plot)), 
             plot, SCM_ARG2, "guppi-plotstore-set-current-tool!");

  SCM_ASSERT(scm_toolp(tool), 
             tool, SCM_ARG3, "guppi-plotstore-set-current-tool!");


  gh_defer_ints();
  
  PlotStore* ps = SCM_TO_PLOTSTORE(plotstore);

  string id;
  
  if (gh_string_p(plot))
    {
      char* str = gh_scm2newstr(plot, NULL);

      g_assert(str != 0);

      id = str;

      free(str);
    }
  else 
    {
      Plot* p = scm2plot(plot);
      
      id = p->id();
    }

  if (!ps->plot_exists(id))
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plotstore-set-current-tool!", _("Unknown plot type"));
    }

  ps->set_current_tool(id, scm2tool(tool));

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

// Return the action with the requested ID
GUPPI_PROC(plotstoregetaction,"guppi-plotstore-get-action",3,0,0,(SCM plotstore, SCM plot, SCM action))
{
  SCM_ASSERT(PLOTSTORE_P(plotstore), 
             plotstore, SCM_ARG1, "guppi-plotstore-get-action");


  SCM_ASSERT((gh_string_p(plot) || scm_plotp(plot)), 
             plot, SCM_ARG2, "guppi-plotstore-get-action");

  SCM_ASSERT(gh_string_p(action), 
             action, SCM_ARG3, "guppi-plotstore-get-action");


  gh_defer_ints();
  
  PlotStore* ps = SCM_TO_PLOTSTORE(plotstore);

  string id;
  
  if (gh_string_p(plot))
    {
      char* str = gh_scm2newstr(plot, NULL);

      g_assert(str != 0);

      id = str;

      free(str);
    }
  else 
    {
      Plot* p = scm2plot(plot);
      
      id = p->id();
    }

  if (!ps->plot_exists(id))
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plotstore-get-action", _("Unknown plot type"));
    }

  string actionid;

  char* str = gh_scm2newstr(action, NULL);

  actionid = str;

  free(str);

  Action* a = ps->get_action(id, actionid);

  if (a == 0)
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plotstore-get-action", _("Unknown Action ID"));
    }

  gh_allow_ints();

  return action2scm(a);
}

// Make a new MenuEntry available
GUPPI_PROC(plotstore_add_menuentry,"guppi-plotstore-add-menuentry!",3,0,0,(SCM plotstore, SCM plot, SCM menuentry))
{
  SCM_ASSERT(PLOTSTORE_P(plotstore), 
             plotstore, SCM_ARG1, "guppi-plotstore-add-menuentry!");

  SCM_ASSERT((gh_string_p(plot) || scm_plotp(plot)), 
             plot, SCM_ARG2, "guppi-plotstore-add-menuentry!");

  SCM_ASSERT(scm_menuentryp(menuentry), 
             menuentry, SCM_ARG3, "guppi-plotstore-add-menuentry!");


  gh_defer_ints();
  
  PlotStore* ps = SCM_TO_PLOTSTORE(plotstore);

  string id;
  
  if (gh_string_p(plot))
    {
      char* str = gh_scm2newstr(plot, NULL);

      g_assert(str != 0);

      id = str;

      free(str);
    }
  else 
    {
      Plot* p = scm2plot(plot);
      
      id = p->id();
    }

  if (!ps->plot_exists(id))
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plotstore-add-menuentry!", _("Unknown plot type"));
    }

  ps->add_menuentry(id, scm2menuentry(menuentry));

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


//////////////// Init

void
init_scm_plotstore()
{
  plotstore_type_tag = scm_newsmob(&plotstore_funcs);

#include "scm_plotstore.x"
}
