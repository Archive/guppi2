// -*- C++ -*-

/* 
 * scm_plot.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/plot.h"

#include "scm_guppi_gnome.h"

///////////////////// Scheme for the Plot interface


static long plot_type_tag;

#define SCM_TO_PLOT(obj) (reinterpret_cast<Plot*>(SCM_CDR(obj)))
#define PLOT_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == plot_type_tag)

bool     
scm_plotp (SCM obj)
{
  return PLOT_P(obj);
}

Plot* 
scm2plot (SCM obj)
{
  if (!PLOT_P(obj)) return 0;
  return SCM_TO_PLOT(obj);
}

SCM      
plot2scm (Plot* plt)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, plt);
  SCM_SETCAR (smob, plot_type_tag);

  plt->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_plot (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_plot (SCM obj)
{
  Plot* plt = SCM_TO_PLOT(obj);

  // we never allocate plots from scheme, since it's a virtual interface.
  // The object is always a handle to an object initially created as some base 
  //  class, or created in C++.
  static const scm_sizet size = 0;

  gh_defer_ints();

  if (plt->unref() == 0)
    delete plt;

  gh_allow_ints();

  return size;
}

static int
print_plot (SCM obj, SCM port, scm_print_state *pstate)
{
  Plot* plt = SCM_TO_PLOT(obj);

  string rep;
  gchar buf[128];

  rep += "#<Plot at ";
  g_snprintf(buf,128,"%g",plt->x());
  rep += buf;
  rep += ",";
  g_snprintf(buf,128,"%g",plt->y());
  rep += buf;
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns plot_funcs = {
  mark_plot, 
  free_plot, 
  print_plot, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on Plot

GUPPI_PROC(plotp,"guppi-plot?",1,0,0,(SCM plot))
{
  return gh_bool2scm(PLOT_P(plot));
}

GUPPI_PROC(plot_set_x,"guppi-plot-set-x!",2,0,0,(SCM plot, SCM num))

{  
  SCM_ASSERT(PLOT_P(plot), plot, SCM_ARG1, "guppi-plot-set-x!");
  SCM_ASSERT(gh_number_p(num), num, SCM_ARG2, "guppi-plot-set-x!");

  gh_defer_ints();
  Plot* plt = SCM_TO_PLOT(plot);  
  
  plt->set_x(gh_scm2double(num));

  gh_allow_ints();
  return SCM_UNSPECIFIED;
}


GUPPI_PROC(plot_set_y,"guppi-plot-set-y!",2,0,0,(SCM plot, SCM num))

{  
  SCM_ASSERT(PLOT_P(plot), plot, SCM_ARG1, "guppi-plot-set-y!");
  SCM_ASSERT(gh_number_p(num), num, SCM_ARG2, "guppi-plot-set-y!");

  gh_defer_ints();
  Plot* plt = SCM_TO_PLOT(plot);  
  
  plt->set_y(gh_scm2double(num));

  gh_allow_ints();
  return SCM_UNSPECIFIED;

}

GUPPI_PROC(plot_set_size,"guppi-plot-set-size!",3,0,0,(SCM plot, SCM width, SCM height))
{
  SCM_ASSERT(PLOT_P(plot), plot, SCM_ARG1, "guppi-plot-set-size!");
  SCM_ASSERT(gh_number_p(width), width, SCM_ARG2, "guppi-plot-set-size!");
  SCM_ASSERT(gh_number_p(height), height, SCM_ARG2, "guppi-plot-set-size!");

  gh_defer_ints();
  Plot* plt = SCM_TO_PLOT(plot);  
  
  double w = gh_scm2double(width);
  double h = gh_scm2double(height);

  if (w < 0.0 || h < 0.0)
    {
      gh_allow_ints();
      guppi_scm_error("guppi-plot-set-size!", _("Plot width and height must be greater than 0"));
    }

  plt->set_size(w,h);

  gh_allow_ints();
  return SCM_UNSPECIFIED;
}

//////////////// Init

void
init_scm_plot()
{
  plot_type_tag = scm_newsmob(&plot_funcs);

#include "scm_plot.x"
}
