// -*- C++ -*-

/* 
 * scm_guppi_gnome.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_SCMGUPPI_GNOME_H
#define GUPPI_SCMGUPPI_GNOME_H

#include "scm_guppi/scm_guppi.h"

#include "libguppi/gnome-util.h"

// Registers all the scheme stuff in this library.
void guppi_gnome_scm_init ();

////////// 

// Lame stuff that shouldn't be in one big header

class Plot;

bool scm_plotp (SCM obj);

Plot* scm2plot (SCM obj);

SCM plot2scm (Plot* plt);


class Action;

bool scm_actionp (SCM obj);

Action* scm2action (SCM obj);

SCM action2scm (Action* act);


class Tool;

bool scm_toolp (SCM obj);

Tool* scm2tool (SCM obj);

SCM tool2scm (Tool* tl);

class MenuEntry;

bool scm_menuentryp (SCM obj);

MenuEntry* scm2menuentry (SCM obj);

SCM menuentry2scm (MenuEntry* me);

#endif
