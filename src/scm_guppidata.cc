// -*- C++ -*-

/* 
 * scm_guppidata.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/data.h"
#include "libguppi/datastore.h"
#include "libguppi/scalardata.h"

#include "scm_guppi/scm_guppi.h"

#include "guppidata.h"

///////////////////// Global datastore handle

static long datastore_type_tag;

#define SCM_TO_DATASTORE(obj) (reinterpret_cast<DataStore*>(SCM_CDR(obj)))
#define DATASTORE_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == datastore_type_tag)

bool     
scm_datastorep (SCM obj)
{
  return DATASTORE_P(obj);
}

DataStore* 
scm2datastore (SCM obj)
{
  if (!DATASTORE_P(obj)) return 0;
  return SCM_TO_DATASTORE(obj);
}

SCM      
datastore2scm (DataStore* ds)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, ds);
  SCM_SETCAR (smob, datastore_type_tag);
  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_datastore (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_datastore (SCM obj)
{
  return 0; // it was just a handle
}

static int
print_datastore (SCM obj, SCM port, scm_print_state *pstate)
{
  DataStore* ds = SCM_TO_DATASTORE(obj);

  string rep;

  rep += "#< Guppi Global DataStore (";
  char buf[128];
  sprintf(buf, "%u", ds->size());
  rep += buf;
  rep += " data objects)>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns datastore_funcs = {
  mark_datastore, 
  free_datastore, 
  print_datastore, 
  0 // means we can never be equal? 
};


GUPPI_PROC(get_guppi_datastore,"guppi-datastore",0,0,0,())
{
  return datastore2scm(guppi_datastore());
}


GUPPI_PROC(datastorep,"guppi-datastore?",1,0,0,(SCM store))
{
  return gh_bool2scm(DATASTORE_P(store));
}

/////////////////////////////// Operations on DataStore

GUPPI_PROC(datastore_insert,"guppi-datastore-insert!",2,0,0,(SCM store, SCM data))
{
  SCM_ASSERT(DATASTORE_P(store), store, SCM_ARG1, "guppi-datastore-insert!");
  SCM_ASSERT(scm_datap(data), data, SCM_ARG2, "guppi-datastore-insert!");
 
  gh_defer_ints();

  DataStore* ds = SCM_TO_DATASTORE(store);
  Data* d = scm2data(data);

  ds->insert(d);

  gh_allow_ints();

  return SCM_UNSPECIFIED; 
}

GUPPI_PROC(datastore_list,"guppi-datastore->list",1,0,0,(SCM store))
{
  SCM_ASSERT(DATASTORE_P(store), store, SCM_ARG1, "guppi-datastore->list");
 
  gh_defer_ints();

  DataStore* ds = SCM_TO_DATASTORE(store);

  SCM list = SCM_LIST0;

  DataStore::iterator i = ds->begin();
  while (i != ds->end())
    {
      list = gh_cons(data2scm(*i), list);

      ++i;
    }

  gh_allow_ints();

  return list;
}

GUPPI_PROC(datastore_size,"guppi-datastore-size",1,0,0,(SCM store))
{
  SCM_ASSERT(DATASTORE_P(store), store, SCM_ARG1, "guppi-datastore-size");
 
  gh_defer_ints();

  DataStore* ds = SCM_TO_DATASTORE(store);

  gsize sz = ds->size();

  gh_allow_ints();

  return gh_int2scm((int)sz);
}


// This is mildly lame... should be a generic DataStore object in 
//  libguppi-scm then use that as an argument to a guppi-make-scatter
//  in that module
#include "libguppi/xyplotstate.h"

GUPPI_PROC(make_scatter,"guppi-make-xy",0,0,0,())
{
  gh_defer_ints();
  XyPlotState* xyps = new XyPlotState(guppi_datastore());
  gh_allow_ints();
  return xy2scm(xyps);
}

//////////////// Init

void
init_scm_guppidata()
{
  datastore_type_tag = scm_newsmob(&datastore_funcs);

#include "scm_guppidata.x"
}


