// -*- C++ -*-

/* 
 * plot-component.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTCOMPONENT_H
#define GUPPI_PLOTCOMPONENT_H

#include <vector>

#include <libguppi/plot.h>
#include <libguppi/datastore.h>

extern "C" {
#include <bonobo/gnome-main.h>
}

class PlotComponent;

// A view onto a single plot component
// Permanently "attached" to its component.

// This is basically the same code as "PlotShell" in ../plotshell.h,
//  eventually we might add an abstraction so it's not cut-and-pasted 

class PlotView : public Plot::View {
public:
  PlotView(PlotComponent* owner);
  virtual ~PlotView();

  // Access the widget; Miguel, should PlotView 
  //  destroy this or should we just let it die with its 
  //  container?
  GtkWidget* widget();

  // PlotComponent takes ownership of the plot (a plot is the 
  //  "view" object from libguppi, we are just a thin wrapper around it)
  void set_plot(Plot* p);

  Plot* plot() { return plot_; }

  // These functions are invoked when the underlying libguppi object changes
  // Read the doc/guppi-internals.texi document for details on model/view setup

  // Plot::View
  virtual void change_size(Plot* p);
  virtual void change_status(Plot* p, const string& status);
  virtual void change_name(Plot* p, const string& name);

  // ::View
  virtual void destroy_model();  

private:
  PlotComponent* owner_;
  Plot* plot_;

  GtkWidget* canvas_;
};


// The state of the component, and a list of all its views.
// Also used to implement VectorFactory IDL (DataStore is the factory/repository)

class PlotComponent {
public:
  PlotComponent(const char* plot_type);
  ~PlotComponent();

  // Create a new view onto the component
  PlotView* new_view();
  
  // Destroy an already-created view.
  // All views are automatically destroyed if you 
  //  destroy the component.
  void destroy_view(PlotView* view);

  DataStore* datastore() { return datastore_; }
  
  // Returns TRUE on success (plot type exists)...
  bool set_plot_type(const char* plot_type);
  static bool plot_type_supported(const char* plot_type);

  PlotState* plot_state();
  
  CORBA_Object state_object();

private:
  string plot_type_;
  vector<PlotView*> views_;
  DataStore* datastore_;
  PlotState* state_;
  CORBA_Object state_object_; // corba wrapper around state_

  Plot* new_plot_of_current_type();
  void release_state();
  void grab_state(PlotState* ps);
  
};


#endif
