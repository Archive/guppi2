// -*- C++ -*-

/* 
 * cplot-vector.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

//////////// This file implements CORBA wrappers around Guppi objects, 
//////////// for IDL Plot::Vector and Plot::VectorFactory

#ifndef GUPPI_CPLOTVECTOR_H
#define GUPPI_CPLOTVECTOR_H

extern "C" {
#include <bonobo/gnome-unknown.h>
#include "PlotComponent.h"
}
#include "corba-util.h"
#include <libguppi/datastore.h>

typedef gpointer PlotVectorFactory;
typedef gpointer PlotVector;

PlotVectorFactory* plot_vector_factory_new(DataStore* ds);
void plot_vector_factory_destroy(PlotVectorFactory* servant);
// return value is not a copy; do not release
CORBA_Object plot_vector_factory_get_reference(PlotVectorFactory* servant);
PlotVectorFactory* plot_vector_factory_get_servant(CORBA_Object reference);

PlotVector* plot_vector_get_servant(CORBA_Object reference);
CORBA_Object plot_vector_get_reference(PlotVector* servant);
Data* plot_vector_get_data(PlotVector* servant);

#endif


