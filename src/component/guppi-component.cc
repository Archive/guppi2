// -*- C++ -*-

/* 
 * guppi-component.cc 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "bonoguppi.h"
#include "corba-init.h"
#include <goose/Exception.h>

#include <config.h>

int
main(int argc, char * argv[])
{
  // Initialize the i18n stuff

  // Also inits Gnome
  bonoguppi_corba_init(&argc, argv);

  // Init libguppi-gnome
  if (!guppi_gnome_init_library())
    {
      GtkWidget* dialog = gnome_error_dialog(_("Failure initializing libraries for plot component; exiting."));
      gnome_dialog_run(GNOME_DIALOG(dialog));
      exit(1);
    }

  // Init the Bonobo factory
  bonoguppi_init_server_factory();

  try {
    gtk_main();

    // Shut down stuff here...
    
    // Finally, shut down libguppi-gnome
    guppi_gnome_shutdown_library();
  }
  catch (Exception & e) {
    cerr << "Unhandled exception: " << e.what() << endl;
    exit(1);
  }

  exit(0);
}


void
bonoguppi_quit()
{
  g_return_if_fail(gtk_main_level() == 1);

  bonoguppi_corba_shutdown();
  gtk_main_quit();
}

