// -*- C++ -*-

// hacked-up version of Bonobo test program by Miguel

#include <gnome.h>
extern "C" {
#include <libgnorba/gnorba.h>
#include <bonobo/gnome-bonobo.h>
}
#include "PlotComponent.h"

CORBA_Environment ev;
CORBA_ORB orb;

char *server_goadid = "Guppi_component";

typedef struct {
  GtkWidget *app;
  GnomeContainer *container;
  GnomeClientSite* client_site;
  GtkWidget *box;
  GnomeUnknownClient *server;
  CORBA_Object plot_component;
} Application;


static void 
check_errors()
{
  bool die = false;

  switch (ev._major)
    {
    case CORBA_NO_EXCEPTION:
      break;
    case CORBA_USER_EXCEPTION:
      g_warning("User exception: %s", CORBA_exception_id(&ev));
      die = true;
      break;
    default:
      g_warning("System exception: %s", CORBA_exception_id(&ev));
      die = true;
      break;
    }

  CORBA_exception_free(&ev);

  if (die)
    g_assert_not_reached(); // dump core.
}


static GnomeUnknownClient *
launch_server (GnomeContainer *container, char *goadid, GnomeClientSite* client_site)
{
  GnomeUnknownClient *object_server;
	
  printf ("Launching...\n");
  object_server = gnome_unknown_activate_with_goad_id (NULL, goadid, 0, NULL);
  printf ("Return: %p\n", object_server);
  if (!object_server){
    g_warning (_("Can not activate object_server\n"));
    return NULL;
  }

  if (!gnome_client_site_bind_bonobo_object (client_site, object_server)){
    g_warning (_("Can not bind object server to client_site\n"));
    return NULL;
  }

  return object_server;
}

static GnomeUnknownClient *
add_cmd (GtkWidget *widget, Application *app, char *server_goadid)
{
  GtkWidget *frame, *socket, *w;
  GnomeViewFrame* viewframe;
  GNOME_View view;
  GNOME_View_windowid id;
	
  if (app->server == NULL)
    {
      app->server = launch_server (app->container, server_goadid, app->client_site);
      if (app->server == NULL)
        return NULL;

      app->plot_component = 
        GNOME_Unknown_query_interface(GNOME_UNKNOWN(app->server)->object,
                                      "IDL:GNOME/Plot/PlotComponent:1.0",
                                      &ev);
      check_errors();

      GNOME_Plot_PlotComponent_set_plot_type(app->plot_component, "Bar",
                                             &ev);
      check_errors();
    }

  viewframe = gnome_bonobo_object_client_new_view (app->server, app->client_site);

  w = gnome_view_frame_get_wrapper(GNOME_VIEW_FRAME(viewframe));

  frame = gtk_frame_new ("View");
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (app->box), frame, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (frame), w);

  gtk_widget_show_all (frame);

  return app->server;
}

static void
add_demo_cmd (GtkWidget *widget, Application *app)
{
  add_cmd (widget, app, server_goadid);
}

static void
exit_cmd (void)
{
  gtk_main_quit ();
}

static void
create_vectors(GtkWidget* widget, Application* app)
{
  if (app->server == 0)
    {
      gnome_ok_dialog("you have to create an object first");
      return;
    }

  GNOME_Plot_VectorFactory vf;

  vf = GNOME_Plot_PlotComponent_get_vector_factory(app->plot_component, &ev);

  check_errors();

  int i = 0;
  while (i < 5)
    {
      printf("%d\n", i);

      GNOME_Plot_Vector v = GNOME_Plot_VectorFactory_create_numeric_vector(vf, &ev);

      check_errors();
      
      double data[] = { 1.0, 3.0, 20.0, 10.0, 40.0, 60.0, 20.0, 25.0, 3.0 };
      guint data_size = sizeof(data)/sizeof(data[0]);
      GNOME_Plot_DoubleSeq seq;
      seq._length = data_size;
      seq._maximum = data_size;
      seq._buffer = data;
      
      GNOME_Plot_Vector_set_from_double_seq(v, 0, &seq, &ev);

      check_errors();

      // Read back and check
      GNOME_Plot_DoubleSeq* gotten =
        GNOME_Plot_Vector_get_as_double_seq(v, 0, data_size, &ev);

      check_errors();

#if 0      // Doesn't work right now - suspect ORBit bug...
      guint z = 0;
      while (z < data_size)
        {
          // might lose precision over the corba link
          g_assert(fabs(data[z] - gotten->_buffer[z]) < 1e-12);
          ++z;
        }
#endif     
 
      CORBA_free(gotten);

      // Check size
      CORBA_unsigned_long size = GNOME_Plot_Vector_size(v, &ev);

      check_errors();

      printf("Received size %u, set %u values\n", size, data_size);

      g_assert(size == data_size);

      // For the first, try setting it as the data for the bar plot.
      if (i == 0)
        {
          CORBA_Object plotstate = GNOME_Plot_PlotComponent_get_plot(app->plot_component, &ev);
          check_errors();
          
          GNOME_Plot_CategoricalPlot_set_scalar_data(plotstate, v, &ev);
          check_errors();

          CORBA_Object_release(plotstate, &ev);
          check_errors();
        }
      // For the last two, try destroying them.

      if (i > 2)
        {
          GNOME_Plot_Vector_destroy(v, &ev);
          check_errors();
        }

      // This leaks the vectors we didn't destroy
      CORBA_Object_release(v, &ev);
      check_errors();
      
      ++i;
    }

  // Free our object reference.
  CORBA_Object_release(vf, &ev);
  check_errors();
}

static void new_app(GtkWidget* w, gpointer data);


static GnomeUIInfo container_file_menu [] = {
  GNOMEUIINFO_ITEM_NONE(N_("_Create a new window"), NULL, new_app),
  GNOMEUIINFO_ITEM_NONE(N_("_Add a new object/view"), NULL, add_demo_cmd),
  GNOMEUIINFO_ITEM_NONE(N_("Create Test _Vectors"), NULL, create_vectors),
  GNOMEUIINFO_ITEM_STOCK (N_("Exit"), NULL, exit_cmd, GNOME_STOCK_PIXMAP_QUIT),
  GNOMEUIINFO_END
};

static GnomeUIInfo container_main_menu [] = {
  GNOMEUIINFO_MENU_FILE_TREE (container_file_menu),
  GNOMEUIINFO_END
};

static Application *
application_new (void)
{
  Application *app;

  app = g_new0 (Application, 1);
  app->app = gnome_app_new ("plot_comp_test", "Testing Plot Component");
  app->container = GNOME_CONTAINER (gnome_container_new ());
  app->client_site = gnome_client_site_new (app->container);
  gnome_container_add (app->container, GNOME_UNKNOWN (app->client_site));

  app->box = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (app->box);
  gnome_app_set_contents (GNOME_APP (app->app), app->box);
  gnome_app_create_menus_with_data (GNOME_APP (app->app), container_main_menu, app);
  gtk_widget_show (app->app);

  app->server = NULL;

  return app;
}

static void new_app(GtkWidget* w, gpointer data)
{
  application_new();
}

int
main (int argc, char *argv [])
{
  Application *app;

  if (argc != 1){
    server_goadid = argv [1];
  }
	
  CORBA_exception_init (&ev);
	
  gnome_CORBA_init ("plot_comp_test", "1.0", &argc, argv, 0, &ev);
  orb = gnome_CORBA_ORB ();
	
  if (bonobo_init (orb, NULL, NULL) == FALSE)
    g_error (_("Can not bonobo_init\n"));

  app = application_new ();
	
  gtk_main ();

  CORBA_exception_free (&ev);

  return 0;
}
