// -*- C++ -*-

/* 
 * corba-util.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "corba-util.h"

static GHashTable *object_to_servant = 0;
static GHashTable *servant_to_object = 0;

CORBA_Object
reference_from_servant (PortableServer_Servant servant)
{
  g_return_val_if_fail (servant != NULL, NULL);
  
  if (!servant_to_object)
    return NULL;
  
  return (CORBA_Object)g_hash_table_lookup (servant_to_object, servant);
}

PortableServer_Servant
servant_from_reference (CORBA_Object object)
{
  g_return_val_if_fail (object != CORBA_OBJECT_NIL, NULL);
  
  if (!servant_to_object)
    return NULL;
  
  return g_hash_table_lookup (object_to_servant, object);
}


static void
reference_bind_to_servant (CORBA_Object object, void *servant)
{
  g_return_if_fail (object != CORBA_OBJECT_NIL);
  g_return_if_fail (servant != NULL);
  
  if (!servant_to_object){
    servant_to_object = g_hash_table_new (g_direct_hash, g_direct_equal);
    object_to_servant = g_hash_table_new (g_direct_hash, g_direct_equal);
  }
  
  g_hash_table_insert (servant_to_object, servant, object);
  g_hash_table_insert (object_to_servant, object, servant);
}

static void
drop_binding_by_servant (void *servant)
{
  void *object;
  g_return_if_fail (servant != NULL);
  
  object = g_hash_table_lookup (servant_to_object, servant);
  
  g_hash_table_remove (servant_to_object, servant);
  g_hash_table_remove (object_to_servant, object);
}

CORBA_Object 
activate_servant(PortableServer_Servant servant)
{
  CORBA_Object o;
  CORBA_Environment ev;

  g_return_val_if_fail (servant != NULL, CORBA_OBJECT_NIL);
	
  CORBA_exception_init(&ev);

  CORBA_free (PortableServer_POA_activate_object(bonobo_poa(), 
                                                 servant, &ev));

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_warning("Failed to activate servant in %s", __FUNCTION__);
      CORBA_exception_free(&ev);
      return CORBA_OBJECT_NIL;
    }
  else
    CORBA_exception_free(&ev);

  o = PortableServer_POA_servant_to_reference (bonobo_poa(), 
                                               servant, &ev);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_warning("OOOPS! can't translate servant to reference in %s", __FUNCTION__);
      CORBA_exception_free(&ev);
      return CORBA_OBJECT_NIL;
    }
  else 
    CORBA_exception_free(&ev);

  reference_bind_to_servant(o, servant);

  return o;
}

PortableServer_ObjectId*
servant_to_id(PortableServer_Servant servant)
{
  PortableServer_ObjectId* oid;
  CORBA_Environment ev;
	
  CORBA_exception_init(&ev);

  oid = PortableServer_POA_servant_to_id (bonobo_poa(), 
                                          servant, &ev);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_warning("OOOPS! error in %s", __FUNCTION__);
      CORBA_exception_free(&ev);
      return 0; // what is a NIL value for an object ID?
    }
  else 
    {
      CORBA_exception_free(&ev);
  
      return oid;
    }
}

void
free_servant(PortableServer_Servant servant)
{
  CORBA_Environment ev;
  CORBA_Object o;
  PortableServer_ObjectId* oid;

  g_return_if_fail (servant != NULL);
	
  CORBA_exception_init(&ev);

  o = servant_from_reference(servant);
  oid = servant_to_id(servant);

  // gnome-object.c just uses servant as the argument here. OK?
  PortableServer_POA_deactivate_object(bonobo_poa(), oid, &ev);

  CORBA_free(oid);

  CORBA_Object_release(o, &ev);

  drop_binding_by_servant(servant);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_warning("OOOPS! object deactivation error in %s", __FUNCTION__);
      CORBA_exception_free(&ev);
    }
  else 
    {
      CORBA_exception_free(&ev);
    }
}


