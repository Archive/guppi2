// -*- C++ -*-

/* 
 * cplot-component.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "cplot-component.h"
#include "PlotComponent.h"
#include <goose/RealSet.h>
#include <libguppi/scalardata.h>
#include <libguppi/barplotstate.h>
#include "corba-util.h"
#include "cplot-vector.h"
#include <bonobo/gnome-main.h>

//////////////////////////////////// CPlotComponent object and PlotComponent CORBA interface
typedef struct _CPlotPriv CPlotPriv;
struct _CPlotPriv {
  PlotVectorFactory* pvf;   // the servant for the vector_factory
};

#define PRIV(cplot) ((CPlotPriv*)cplot->priv)

static GnomeUnknownClass* parent_class = NULL;

static void cplot_component_class_init(CPlotComponentClass* klass);
static void cplot_component_init(CPlotComponent* cpc);
static void cplot_component_destroy(GtkObject *object);

/* The entry point vectors for the server we provide */
static POA_GNOME_Plot_PlotComponent__epv gnome_plotcomponent_epv;
static POA_GNOME_Plot_PlotComponent__vepv gnome_plotcomponent_vepv;


static CORBA_boolean
impl_GNOME_Plot_PlotComponent_plot_type_supported(PortableServer_Servant servant,
                                                  const CORBA_char * plot_type,
                                                  CORBA_Environment *ev)
{
  CPlotComponent *cpc = CPLOT_COMPONENT (gnome_unknown_from_servant (servant));  

  return PlotComponent::plot_type_supported(plot_type);
}

static void
impl_GNOME_Plot_PlotComponent_set_plot_type(PortableServer_Servant servant,
                                            const CORBA_char * plot_type,
                                            CORBA_Environment *ev)
{
  CPlotComponent *cpc = CPLOT_COMPONENT (gnome_unknown_from_servant (servant));  

  // FIXME if this returns false, must raise an exception!
  bool result = cpc->pc->set_plot_type(plot_type);

  g_assert(result);
}

static GNOME_Plot_PlotState
impl_GNOME_Plot_PlotComponent_get_plot(PortableServer_Servant servant,
                                       CORBA_Environment *ev)
{
  CPlotComponent *cpc = CPLOT_COMPONENT (gnome_unknown_from_servant (servant));  

  return cpc->pc->state_object();
}

static GNOME_Plot_VectorFactory
impl_GNOME_Plot_PlotComponent_get_vector_factory(PortableServer_Servant servant,
                                                 CORBA_Environment *ev)
{
  CPlotComponent *cpc = CPLOT_COMPONENT (gnome_unknown_from_servant (servant));  

  return CORBA_Object_duplicate(plot_vector_factory_get_reference(PRIV(cpc)->pvf), ev);
}

#define FILL_EPV(method_name) gnome_plotcomponent_epv.method_name = impl_GNOME_Plot_PlotComponent_##method_name

static void
init_corba_class (void)
{
  FILL_EPV(plot_type_supported);
  FILL_EPV(set_plot_type);
  FILL_EPV(get_plot);
  FILL_EPV(get_vector_factory);
  
  /* Setup the vector of epvs */
  gnome_plotcomponent_vepv.GNOME_Unknown_epv = &gnome_unknown_epv;
  gnome_plotcomponent_vepv.GNOME_Plot_PlotComponent_epv = &gnome_plotcomponent_epv;
}
#undef FILL_EPV

static CORBA_Object
create_cplot_component (GnomeUnknown *object)
{
  POA_GNOME_Plot_PlotComponent *servant;
  CORBA_Object o;
  
  servant = g_new0 (POA_GNOME_Plot_PlotComponent, 1);
  servant->vepv = &gnome_plotcomponent_vepv;
  
  POA_GNOME_Plot_PlotComponent__init ((POA_GNOME_Plot_PlotComponent*) servant, 
                                      &object->ev);
  if (object->ev._major != CORBA_NO_EXCEPTION){
    g_free (servant);
    return CORBA_OBJECT_NIL;
  }
  
  return gnome_unknown_activate_servant (object, servant);
}


GtkType
cplot_component_get_type (void)
{
  static GtkType type = 0;

  if (!type){
    GtkTypeInfo info = {
      "IDL:GNOME/Plot/PlotComponent:1.0",
      sizeof (CPlotComponent),
      sizeof (CPlotComponentClass),
      (GtkClassInitFunc) cplot_component_class_init,
      (GtkObjectInitFunc) cplot_component_init,
      NULL, /* reserved 1 */
      NULL, /* reserved 2 */
      (GtkClassInitFunc) NULL
    };

    type = gtk_type_unique (gnome_unknown_get_type (), &info);
  }

  return type;
}

static void
cplot_component_class_init (CPlotComponentClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GnomeUnknownClass* gobject_class = (GnomeUnknownClass*) klass;

  parent_class = (GnomeUnknownClass*)gtk_type_class (gnome_unknown_get_type ());
	
  object_class->destroy = cplot_component_destroy;

  init_corba_class();
}

static void
cplot_component_init (CPlotComponent* cpc)
{
  cpc->pc = 0;
  cpc->priv = 0;
}

static void
cplot_component_destroy (GtkObject *object)
{
  CPlotComponent *cpc = CPLOT_COMPONENT (object);

  plot_vector_factory_destroy(PRIV(cpc)->pvf);

  g_free(cpc->priv);

  delete cpc->pc;
  cpc->pc = 0;

  GTK_OBJECT_CLASS (parent_class)->destroy(object);
}

CPlotComponent *
cplot_component_construct (CPlotComponent* cpc, 
                           GNOME_Plot_PlotComponent corba_pc,
                           const gchar* default_plot_type)
{
  gnome_unknown_construct (GNOME_UNKNOWN (cpc), corba_pc);

  cpc->pc = new PlotComponent(default_plot_type);

  cpc->priv = g_new(CPlotPriv, 1);
 
  PRIV(cpc)->pvf = plot_vector_factory_new(cpc->pc->datastore());

  return cpc;
}

CPlotComponent *
cplot_component_new(const char* default_plot_type)
{
  GNOME_Plot_PlotComponent corba_pc;
  CPlotComponent* cpc;

  if (!PlotComponent::plot_type_supported(default_plot_type))
    return NULL;

  cpc = (CPlotComponent*)gtk_type_new (cplot_component_get_type ());

  corba_pc = create_cplot_component (GNOME_UNKNOWN (cpc));
  if (corba_pc == CORBA_OBJECT_NIL){
    gtk_object_destroy (GTK_OBJECT (cpc));
    return NULL;
  }
  
  return cplot_component_construct (cpc, corba_pc, default_plot_type);
}
