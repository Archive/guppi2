// -*- C++ -*-

/* 
 * corba-init.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_CORBAINIT_H
#define GUPPI_CORBAINIT_H

#include "bonoguppi.h"

#include <gtk/gtk.h>

// this header should just be fixed to have BEGIN_GNOME_DECLS in it.
extern "C" {
#include <libgnorba/gnorba.h>
}

// return the ORB
CORBA_ORB bonoguppi_orb();

// Return the environment
CORBA_Environment* bonoguppi_corba_env();

// Init things
void bonoguppi_corba_init(int* argc, char** argv);
// Un-init things
void bonoguppi_corba_shutdown();

// Init bonobo bits; maybe can go in the bonoguppi_corba_init() function?
// (Depends on whether libguppi needs initing first...)
void bonoguppi_init_server_factory();

// Pre-process errors; makes the code less CORBA-uglified.
// Also, no error is FALSE

typedef enum {
  GCE_NONE, // No error
  GCE_FATAL // The operation fatally didn't work
} BonoGuppiCORBAErr;

BonoGuppiCORBAErr bonoguppi_corba_error(CORBA_Environment* ev);

// Get the error id from the last exception
const gchar* bonoguppi_corba_strerror();


#endif
