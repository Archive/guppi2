// -*- C++ -*-

/* 
 * cplot-vector.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "cplot-vector.h"
#include "plot-component.h"
#include <libguppi/scalardata.h>
#include <goose/RealSet.h>

//////////////////////////////////// First, we have all the annoying little auxiliary
//////////////////////////////////// CORBA interfaces

////// Start with Vector

static PortableServer_ServantBase__epv vector_base_epv = {
  NULL, NULL, NULL 
};

static POA_GNOME_Plot_Vector__epv gnome_vector_epv;

static POA_GNOME_Plot_Vector__vepv gnome_vector_vepv = 
{ 
  &vector_base_epv, 
  &gnome_vector_epv 
};

typedef struct _PrivateVector PrivateVector;
struct _PrivateVector {
  POA_GNOME_Plot_Vector servant;
  DataStore* store;
  Data* data;
};

// Note that this is our finalization function...
static void 
impl_GNOME_Plot_Vector__destroy(PortableServer_Servant servant,
                                CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  DataStore::iterator i = pv->store->find(pv->data);
  if (i != pv->store->end())
    {
      pv->store->erase(i);
    }
  else
    g_warning("OOOPS! non-existent data being destroyed...");


  POA_GNOME_Plot_Vector__fini((POA_GNOME_Plot_Vector*)servant, ev);

  g_free(pv);

  g_debug("Vector finalized.");
}

// But this is exported to clients...
static void 
impl_GNOME_Plot_Vector_destroy(PortableServer_Servant servant,
                               CORBA_Environment *ev)
{
  g_debug("Deactivating Vector servant");
  free_servant(servant);
}

static CORBA_char *
impl_GNOME_Plot_Vector_get_name(PortableServer_Servant servant,
                                CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  return CORBA_string_dup(const_cast<CORBA_char*>(pv->data->name().c_str()));
}


static void
impl_GNOME_Plot_Vector_set_name(PortableServer_Servant servant,
                                const CORBA_char * name,
                                CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  pv->data->set_name(name);
}


static CORBA_unsigned_long
impl_GNOME_Plot_Vector_size(PortableServer_Servant servant,
                            CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  return pv->data->size();
}

static GNOME_Plot_DoubleSeq*
impl_GNOME_Plot_Vector_get_as_double_seq(PortableServer_Servant servant,
                                         CORBA_unsigned_long start_index,
                                         CORBA_unsigned_long count,
                                         CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  ScalarData* sd = pv->data->cast_to_scalar();
  
  // Returning NULL is not allowed...

  GNOME_Plot_DoubleSeq* retval = GNOME_Plot_DoubleSeq__alloc();
  retval->_buffer = CORBA_sequence_CORBA_double_allocbuf(count);
  retval->_length = 0;
  retval->_maximum = count;

  if (sd == NULL)
    {
      g_warning("CoercionError");
      
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
                          ex_GNOME_Plot_CoercionError,
                          NULL);
      return retval;
    }

  if ((start_index + count) > sd->size())
    {
      g_warning("OutOfRange error");

      CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
                          ex_GNOME_Plot_OutOfRange,
                          NULL);
      return retval;
    }

  const double* data = sd->scalars();

  memcpy(retval->_buffer, &data[start_index], count*sizeof(double));
  retval->_length = count;

  guint z = 0;
  while (z < count)
    {
      g_assert(retval->_buffer[z] == data[start_index+z]);
      ++z;
    }

  return retval;
}


static void
impl_GNOME_Plot_Vector_set_from_double_seq(PortableServer_Servant servant,
                                           CORBA_unsigned_long start_index,
                                           const GNOME_Plot_DoubleSeq* seq,
                                           CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  ScalarData* sd = pv->data->cast_to_scalar();

  if (sd == NULL)
    {
      g_warning("CoercionError");
      
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
                          ex_GNOME_Plot_CoercionError,
                          NULL);
      return;
    }

  guint needed_len = start_index + seq->_length;

  RealSet* rs = sd->checkout_realset();
  g_return_if_fail(rs != 0);

  if (needed_len > sd->size())
    rs->resize(needed_len);

#if 0 
  // An off-by-one in old versions of goose makes this throw an exception;
  // we will uncomment it after a reasonable interval.
  rs->set(start_index, seq->_buffer, seq->_length);
#else
  guint i = 0;
  while (i < seq->_length)
    {
      rs->set(start_index+i, seq->_buffer[i]);
      ++i;
    }
#endif

  sd->checkin_realset(rs, true, true);
}


static GNOME_Plot_IntSeq*
impl_GNOME_Plot_Vector_get_as_int_seq(PortableServer_Servant servant,
                                      CORBA_unsigned_long start_index,
                                      CORBA_unsigned_long count,
                                      CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  g_warning("%s not implemented", __FUNCTION__);

  return CORBA_OBJECT_NIL; // FIXME
}


static void
impl_GNOME_Plot_Vector_set_from_int_seq(PortableServer_Servant servant,
                                        CORBA_unsigned_long start_index,
                                        const GNOME_Plot_IntSeq* seq,
                                        CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  g_warning("%s not implemented", __FUNCTION__);
}



static GNOME_Plot_Vector_StringSeq*
impl_GNOME_Plot_Vector_get_as_string_seq(PortableServer_Servant servant,
                                         CORBA_unsigned_long start_index,
                                         CORBA_unsigned_long count,
                                         CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  g_warning("%s not implemented", __FUNCTION__);

  return CORBA_OBJECT_NIL; // FIXME
}


static void
impl_GNOME_Plot_Vector_set_from_string_seq(PortableServer_Servant servant,
                                           CORBA_unsigned_long start_index,
                                           const GNOME_Plot_Vector_StringSeq* seq,
                                           CORBA_Environment *ev)
{
  PrivateVector* pv = static_cast<PrivateVector*>(servant);

  g_warning("%s not implemented", __FUNCTION__);
}


#define FILL_EPV(method_name) gnome_vector_epv.method_name = impl_GNOME_Plot_Vector_##method_name

static void
init_vector_corba_class()
{
  // note __destroy, not _destroy
  vector_base_epv.finalize = impl_GNOME_Plot_Vector__destroy;

  FILL_EPV(destroy);
  FILL_EPV(get_name);
  FILL_EPV(set_name);
  FILL_EPV(size);
  FILL_EPV(get_as_double_seq);
  FILL_EPV(set_from_double_seq);
  FILL_EPV(get_as_int_seq);
  FILL_EPV(set_from_int_seq);
  FILL_EPV(get_as_string_seq);
  FILL_EPV(set_from_string_seq);
  
  // vepv is already filled
}
#undef FILL_EPV

static PrivateVector*
create_vector (CORBA_Object* op)
{
  POA_GNOME_Plot_Vector *servant;
  CORBA_Environment ev;
  PrivateVector* pv;

  static bool class_inited = false;
  if (!class_inited)
    {
      init_vector_corba_class();
      class_inited = true;
    }

  pv = g_new0 (PrivateVector, 1);
  servant = &pv->servant;
  servant->vepv = &gnome_vector_vepv;
  
  pv->store = 0;
  pv->data = 0;

  CORBA_exception_init(&ev);

  POA_GNOME_Plot_Vector__init (servant, 
                               &ev);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_warning("Error initializing Vector servant");
      g_free (pv);
      CORBA_exception_free(&ev);
      return 0;
    }
  else 
    {
      CORBA_exception_free(&ev);
    }

  *op = activate_servant(static_cast<PortableServer_Servant>(servant));

  if (*op == CORBA_OBJECT_NIL)
    {
      g_warning("Failed to activate Vector servant");
      g_free(pv);
      return 0;
    }

  g_debug("Created a Vector");

  // All went well, we hope...
  return pv;
}


///////////////////////////////////// VectorFactory


static PortableServer_ServantBase__epv vectorfactory_base_epv = {
  NULL, NULL, NULL 
};

static POA_GNOME_Plot_VectorFactory__epv gnome_vectorfactory_epv;

static POA_GNOME_Plot_VectorFactory__vepv gnome_vectorfactory_vepv = 
{ 
  &vectorfactory_base_epv, 
  &gnome_vectorfactory_epv 
};

typedef struct _PrivateVectorFactory PrivateVectorFactory;
struct _PrivateVectorFactory {
  POA_GNOME_Plot_VectorFactory servant;
  DataStore* store;
};


static void 
impl_GNOME_Plot_VectorFactory__destroy(PortableServer_Servant servant,
                                       CORBA_Environment *ev)
{
  PrivateVectorFactory* pvf = static_cast<PrivateVectorFactory*>(servant);

  if (pvf->store->size() > 0)
    {
      g_warning("Destroying vector factory with vectors still outstanding!");
    }

  POA_GNOME_Plot_VectorFactory__fini((POA_GNOME_Plot_VectorFactory*)servant, ev);

  g_free(pvf);  

  g_debug("Destroyed VectorFactory");
}

static GNOME_Plot_Vector
impl_GNOME_Plot_VectorFactory_create_numeric_vector(PortableServer_Servant servant,
                                                    CORBA_Environment *ev)
{
  g_debug("Creating numeric vector");

  PrivateVectorFactory* pvf = static_cast<PrivateVectorFactory*>(servant);

  ScalarData* sd = new ScalarData;

  sd->set_name(_("Unnamed"));

  pvf->store->insert(sd);

  CORBA_Object o;
  
  PrivateVector* pv = create_vector(&o);
  
  g_assert(pv != 0);

  pv->data = sd;
  pv->store = pvf->store;

  CORBA_Object retval = CORBA_Object_duplicate(o, ev);

  return retval;
}

static GNOME_Plot_Vector
impl_GNOME_Plot_VectorFactory_create_string_vector(PortableServer_Servant servant,
                                                   CORBA_Environment *ev)
{
  PrivateVectorFactory* pvf = static_cast<PrivateVectorFactory*>(servant);

  // Guppi doesn't have string containers yet, but Goose does so 
  // it's fairly trivial just have to get around to it.

  g_warning("%s unimplemented", __FUNCTION__);

  return CORBA_OBJECT_NIL;
}

#define FILL_EPV(method_name) gnome_vectorfactory_epv.method_name = impl_GNOME_Plot_VectorFactory_##method_name

static void
init_vectorfactory_corba_class()
{
  vectorfactory_base_epv.finalize = impl_GNOME_Plot_VectorFactory__destroy;

  FILL_EPV(create_numeric_vector);
  FILL_EPV(create_string_vector);
  // vepv is already filled
}
#undef FILL_EPV

static PrivateVectorFactory*
create_vectorfactory (CORBA_Object* op)
{
  POA_GNOME_Plot_VectorFactory *servant;
  CORBA_Environment ev;
  PrivateVectorFactory* pvf;

  static bool class_inited = false;
  if (!class_inited)
    {
      init_vectorfactory_corba_class();
      class_inited = true;
    }

  pvf = g_new0 (PrivateVectorFactory, 1);
  servant = (POA_GNOME_Plot_VectorFactory*)pvf;
  servant->vepv = &gnome_vectorfactory_vepv;
  
  pvf->store = 0;

  CORBA_exception_init(&ev);

  POA_GNOME_Plot_VectorFactory__init ((POA_GNOME_Plot_VectorFactory*) servant, 
                                      &ev);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_free (pvf);
      CORBA_exception_free(&ev);
      return CORBA_OBJECT_NIL;
    }
  else 
    {
      CORBA_exception_free(&ev);
    }

  *op = activate_servant(static_cast<PortableServer_Servant>(pvf));

  if (*op == CORBA_OBJECT_NIL)
    {
      g_warning("Failed to activate VectorFactory servant");
      g_free(pvf);
      return 0;
    }

  g_debug("Created VectorFactory");

  return pvf;
}

/////// Public interface

PlotVectorFactory* 
plot_vector_factory_new(DataStore* ds)
{
  CORBA_Object o;
  PrivateVectorFactory* retval =  create_vectorfactory(&o);
  retval->store = ds;

  return (PlotVectorFactory*) retval;
}
void 
plot_vector_factory_destroy(PlotVectorFactory* servant)
{
  free_servant(servant);
}

// return value is not a copy; do not release
CORBA_Object 
plot_vector_factory_get_reference(PlotVectorFactory* servant)
{
  return reference_from_servant(servant);
}

PlotVectorFactory* 
plot_vector_factory_get_servant(CORBA_Object reference)
{
  return (PlotVectorFactory*)servant_from_reference(reference);
}

PlotVector* 
plot_vector_get_servant(CORBA_Object reference)
{
  return (PlotVector*)servant_from_reference(reference);
}

CORBA_Object 
plot_vector_get_reference(PlotVector* servant)
{
  return reference_from_servant(servant);
}

Data* 
plot_vector_get_data(PlotVector* servant)
{
  PrivateVector* pv = (PrivateVector*)servant;

  return pv->data;
}

