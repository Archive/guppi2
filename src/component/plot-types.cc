// -*- C++ -*-

/* 
 * plot-types.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "plot-types.h"
#include "plot-component.h"
#include "PlotComponent.h"
#include <libguppi/barplotstate.h>
#include <libguppi/barplot.h>
#include <libguppi/xyplotstate.h>
#include <libguppi/xyplot.h>
#include "corba-util.h"
#include "cplot-vector.h"

/////////////////////////////////////////// CORBA objects

//////////////// PlotState wrappers (specific plot-type interfaces)
//////////////// Note that there are only real servants for the 
//////////////// most derived classes; PlotState, TwoDPlot, etc. can't
//////////////// be created.

//// XyPlot

static PortableServer_ServantBase__epv xyplot_base_epv = {
  NULL, NULL, NULL 
};

static POA_GNOME_Plot_PlotState__epv xyplot_plotstate_epv;
static POA_GNOME_Plot_TwoDPlot__epv xyplot_twod_epv;
static POA_GNOME_Plot_ScatterPlot__epv gnome_xyplot_epv;

static POA_GNOME_Plot_ScatterPlot__vepv gnome_xyplot_vepv = 
{ 
  &xyplot_base_epv, 
  &xyplot_plotstate_epv,
  &xyplot_twod_epv,
  &gnome_xyplot_epv 
};

typedef struct _PrivateXyPlot PrivateXyPlot;
struct _PrivateXyPlot {
  POA_GNOME_Plot_ScatterPlot servant;
  XyPlotState* state;
};


static void impl_GNOME_Plot_ScatterPlot__destroy(PortableServer_Servant servant,
                                                 CORBA_Environment *ev)
{
  g_debug("XyPlot destroyed");
  POA_GNOME_Plot_ScatterPlot__fini((POA_GNOME_Plot_ScatterPlot*)servant, ev);
  g_free(servant);
}

static CORBA_char *
impl_GNOME_Plot_ScatterPlot_get_title(PortableServer_Servant servant,
                                      CORBA_Environment *ev)
{
  CORBA_char* retval;

  retval = CORBA_string_dup(((PrivateXyPlot*)servant)->state->name().c_str());

  return retval;
}

static void
impl_GNOME_Plot_ScatterPlot_set_title(PortableServer_Servant servant,
                                      const CORBA_char * title,
                                      CORBA_Environment *ev)
{
  XyPlotState* sps = ((PrivateXyPlot*)servant)->state;

  sps->set_name(title);
}

static void
impl_GNOME_Plot_ScatterPlot_set_x_data(PortableServer_Servant servant,
                                       GNOME_Plot_Vector value,
                                       CORBA_Environment *ev)
{
  PlotVector* pv = plot_vector_get_servant(value);

  g_return_if_fail(pv != 0);

  XyPlotState* sps = ((PrivateXyPlot*)servant)->state;

  // sps->set_x_data(plot_vector_get_data(pv));
  g_warning("%s not implemented", __FUNCTION__);
}

static void
impl_GNOME_Plot_ScatterPlot_set_y_data(PortableServer_Servant servant,
                                       GNOME_Plot_Vector value,
                                       CORBA_Environment *ev)
{
  PlotVector* pv = plot_vector_get_servant(value);

  g_return_if_fail(pv != 0);

  XyPlotState* sps = ((PrivateXyPlot*)servant)->state;

  //  sps->set_y_data(plot_vector_get_data(pv));
  g_warning("%s not implemented", __FUNCTION__);
}


static void
init_xyplot_corba_class()
{
  xyplot_base_epv.finalize = impl_GNOME_Plot_ScatterPlot__destroy;

  xyplot_plotstate_epv.get_title = impl_GNOME_Plot_ScatterPlot_get_title;
  xyplot_plotstate_epv.set_title = impl_GNOME_Plot_ScatterPlot_set_title;
  
  xyplot_twod_epv.set_x_data = impl_GNOME_Plot_ScatterPlot_set_x_data;
  xyplot_twod_epv.set_y_data = impl_GNOME_Plot_ScatterPlot_set_y_data;
  
  // vepv is already filled
}

static CORBA_Object
wrap_xyplot (XyPlotState* state)
{
  POA_GNOME_Plot_ScatterPlot *servant;
  CORBA_Object o;
  CORBA_Environment ev;
  PrivateXyPlot* psp;

  static bool class_inited = false;
  if (!class_inited)
    {
      init_xyplot_corba_class();
      class_inited = true;
    }

  psp = g_new0 (PrivateXyPlot, 1);
  servant = (POA_GNOME_Plot_ScatterPlot*)psp;
  servant->vepv = &gnome_xyplot_vepv;
  
  psp->state = state;

  CORBA_exception_init(&ev);

  POA_GNOME_Plot_ScatterPlot__init ((POA_GNOME_Plot_ScatterPlot*) servant, 
                                    &ev);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_free (psp);
      CORBA_exception_free(&ev);
      return CORBA_OBJECT_NIL;
    }
  else 
    {
      CORBA_exception_free(&ev);
    }

  o = activate_servant(static_cast<PortableServer_Servant>(psp));

  if (o == CORBA_OBJECT_NIL)
    {
      g_warning("Failed to activate XyPlot servant");
      g_free(psp);
      return 0;
    }

  g_debug("Created XyPlot");

  return o;
}

//// BarPlot

static PortableServer_ServantBase__epv barplot_base_epv = {
  NULL, NULL, NULL 
};

static POA_GNOME_Plot_PlotState__epv barplot_plotstate_epv;
static POA_GNOME_Plot_CategoricalPlot__epv barplot_categorical_epv;
static POA_GNOME_Plot_BarPlot__epv gnome_barplot_epv;

static POA_GNOME_Plot_BarPlot__vepv gnome_barplot_vepv = 
{ 
  &barplot_base_epv, 
  &barplot_plotstate_epv,
  &barplot_categorical_epv,
  &gnome_barplot_epv 
};

typedef struct _PrivateBarPlot PrivateBarPlot;
struct _PrivateBarPlot {
  POA_GNOME_Plot_BarPlot servant;
  BarPlotState* state;
};


static void
impl_GNOME_Plot_BarPlot__destroy(PortableServer_Servant servant, CORBA_Environment *ev)
{
  g_debug("BarPlot destroyed");
  POA_GNOME_Plot_BarPlot__fini((POA_GNOME_Plot_BarPlot*)servant, ev);
  g_free(servant);
}

static CORBA_char*
impl_GNOME_Plot_BarPlot_get_title(PortableServer_Servant servant,
                                  CORBA_Environment *ev)
{
  CORBA_char* retval;

  retval = CORBA_string_dup(((PrivateBarPlot*)servant)->state->name().c_str());

  return retval;
}

static void
impl_GNOME_Plot_BarPlot_set_title(PortableServer_Servant servant,
                                  const CORBA_char * title,
                                  CORBA_Environment *ev)
{
  BarPlotState* bps = ((PrivateBarPlot*)servant)->state;

  bps->set_name(title);
}

static void
impl_GNOME_Plot_BarPlot_set_categorical_data(PortableServer_Servant servant,
                                             GNOME_Plot_Vector value,
                                             CORBA_Environment *ev)
{
  g_warning("FIXME %s not implemented", __FUNCTION__);
}

static void
impl_GNOME_Plot_BarPlot_set_scalar_data(PortableServer_Servant servant,
                                        GNOME_Plot_Vector value,
                                        CORBA_Environment *ev)
{
  PlotVector* pv = plot_vector_get_servant(value);

  g_return_if_fail(pv != 0);

  BarPlotState* bps = ((PrivateBarPlot*)servant)->state;

  bps->set_data(plot_vector_get_data(pv));
}

static void
init_barplot_corba_class()
{
  barplot_base_epv.finalize = impl_GNOME_Plot_BarPlot__destroy;

  barplot_plotstate_epv.get_title = impl_GNOME_Plot_BarPlot_get_title;
  barplot_plotstate_epv.set_title = impl_GNOME_Plot_BarPlot_set_title;
  
  barplot_categorical_epv.set_categorical_data = impl_GNOME_Plot_BarPlot_set_categorical_data;
  barplot_categorical_epv.set_scalar_data = impl_GNOME_Plot_BarPlot_set_scalar_data;
  
  // vepv is already filled
}

static CORBA_Object
wrap_barplot (BarPlotState* state)
{
  POA_GNOME_Plot_BarPlot *servant;
  CORBA_Object o;
  CORBA_Environment ev;
  PrivateBarPlot* pvb;

  static bool class_inited = false;
  if (!class_inited)
    {
      init_barplot_corba_class();
      class_inited = true;
    }

  pvb = g_new0 (PrivateBarPlot, 1);
  servant = (POA_GNOME_Plot_BarPlot*)pvb;
  servant->vepv = &gnome_barplot_vepv;
  
  pvb->state = state;

  CORBA_exception_init(&ev);

  POA_GNOME_Plot_BarPlot__init ((POA_GNOME_Plot_BarPlot*) servant, 
                                &ev);

  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_free (pvb);
      CORBA_exception_free(&ev);
      return CORBA_OBJECT_NIL;
    }
  else 
    {
      CORBA_exception_free(&ev);
    }

  o = activate_servant(static_cast<PortableServer_Servant>(pvb));

  if (o == CORBA_OBJECT_NIL)
    {
      g_warning("Failed to activate BarPlot servant");
      g_free(pvb);
      return 0;
    }

  g_debug("Created BarPlot");

  return o;
}


/////////////////////////////////////////// Functions

/////////////// Bar plot functions

static PlotState* bar_plot_new_state_func(DataStore* ds) 
{ 
  BarPlotState* ps = new BarPlotState(ds);

  // A default
  if (ds->size() > 0) 
    ps->set_data(*(ds->begin()));

  return ps;
}



static Plot* bar_plot_s2p_func(PlotState* ps)
{
  return new BarPlot(static_cast<BarPlotState*>(ps));
}

static CORBA_Object bar_plot_corba_func(PlotState* ps)
{
  return wrap_barplot((BarPlotState*)ps);
}

/////////////// Xy plot functions

static PlotState* xy_plot_new_state_func(DataStore* ds) 
{ 
  XyPlotState* ps = new XyPlotState(ds);

  // For now, the XY plot is always a scatter plot; we add just one layer.

  XyLayer* xyl = new XyScatter(ds);

  ps->add_layer(xyl);

  return ps;
}

static Plot* xy_plot_s2p_func(PlotState* ps)
{
  return new XyPlot(static_cast<XyPlotState*>(ps));
}

static CORBA_Object xy_plot_corba_func(PlotState* ps)
{
  return wrap_xyplot((XyPlotState*)ps);
}


//////////////////////////////////////////////// Table of types

// Keep these in alphabetical order, if there are ever a lot 
//  we can binary search.
static PlotTypeData plot_types[] = {
  { "Bar", bar_plot_new_state_func, bar_plot_s2p_func, bar_plot_corba_func },
  { "Scatter", xy_plot_new_state_func, xy_plot_s2p_func, xy_plot_corba_func }
};

static const guint n_plot_types = sizeof(plot_types)/sizeof(plot_types[0]);

const PlotTypeData* 
bonoguppi_find_plot_type(const char* name)
{
  // If we get more than a few plot types, this should probably be 
  // reimplemented with a hash table.

  guint i = 0;
  while (i < n_plot_types)
    {
      if (g_strcasecmp(plot_types[i].name, name) == 0)
        return &plot_types[i];
      
      ++i;
    }
  
  return 0;
}
