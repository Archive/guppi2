// -*- C++ -*-

/* 
 * corba-init.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "corba-init.h"
#include "cplot-component.h"

extern "C" {
#include <bonobo/bonobo.h>
#include <bonobo/gnome-main.h>
#include <bonobo/gnome-bonobo-object.h>
#include <bonobo/gnome-component-factory.h>
#include <bonobo/gtk-interfaces.h>
}

#include "config.h"

static CORBA_ORB orb = CORBA_OBJECT_NIL;
static CORBA_Environment ev;
static GnomeComponentFactory* factory = 0;

CORBA_ORB 
bonoguppi_orb()
{
  return orb;
}

CORBA_Environment*
bonoguppi_corba_env()
{
  return &ev;
}

static void die(const char* err)
{
  fprintf(stderr, "%s\n", err);
  exit(1);
}

static GnomeView *
guppi_view_factory (GnomeBonoboObject *bonobo_object, CORBA_Object obj, void *data)
{
	GnomeView *view;
	GtkWidget *widget;

        // Assuming we will be passed the user data from 
        //  gnome_bonobo_object_new, I don't really know...
        CPlotComponent* cpc = CPLOT_COMPONENT(data);

        PlotView* pv = cpc->pc->new_view();

	widget = pv->widget();

	gtk_widget_show (widget);
	view = gnome_view_new (widget);

	return view;
}

/*
 * Creates new Guppi component instances
 */
static GnomeUnknown *
guppi_factory (GnomeComponentFactory *factory, void *closure)
{
	GnomeBonoboObject *server;

        CPlotComponent* cpc;
        
        cpc = cplot_component_new("Bar");
        
	server = gnome_bonobo_object_new (guppi_view_factory, cpc);
        
        gtk_object_add_interface(GTK_OBJECT(server), GTK_OBJECT(cpc));
        
	if (server == NULL){
          GtkWidget* dialog = gnome_error_dialog(_("Could not create the Guppi plot component; exiting."));
          gnome_dialog_run(GNOME_DIALOG(dialog));
          die("Could not create the Guppi Gnome BonoboObject");
	}
	return (GnomeUnknown *) server;
}

/*
 * Registers the Guppi factory with the name server
 */
void
bonoguppi_init_server_factory (void)
{
	factory = gnome_component_factory_new ("Guppi_server_factory", guppi_factory, NULL);

        // Since Gnome is up at this point, we can use a dialog if there's an error.

        if (factory == 0)
          {
            GtkWidget* dialog = gnome_error_dialog(_("Could not create the Guppi plot component factory; exiting."));
            gnome_dialog_run(GNOME_DIALOG(dialog));
            die("Could not create the Guppi component factory!");
          }
}

void 
bonoguppi_corba_init(int* argc, char** argv)
{
  CORBA_exception_init (&ev);
  
  orb = gnome_CORBA_init("guppi-component", VERSION, argc, argv, GNORBA_INIT_SERVER_FUNC, &ev);

  if (bonoguppi_corba_error(&ev))
    {
      fprintf(stderr, "Exception: %s\n", bonoguppi_corba_strerror());
      die("Couldn't get ORB, aborting...");
    }
  if (bonobo_init (orb, NULL, NULL) == FALSE)
    {
      fprintf(stderr, "Bonobo could not init\n");
      die ("Bonobo bootstrap failed");
    }
}

void 
bonoguppi_corba_shutdown()
{
#if 0 // More cruft from regular guppi (release Guile object)
  if (gg != CORBA_OBJECT_NIL)
    {
      CORBA_Object_release(gg, &ev);
      gg = CORBA_OBJECT_NIL;
    }
#endif

  if (orb != CORBA_OBJECT_NIL)
    {
      CORBA_Object_release((CORBA_Object)orb, &ev);
      orb = CORBA_OBJECT_NIL;
    }
}

struct {
  gchar* id;
} last_ex = { 0 };

BonoGuppiCORBAErr 
bonoguppi_corba_error(CORBA_Environment* ev)
{
  BonoGuppiCORBAErr retval = GCE_NONE;
  g_free(last_ex.id);
  last_ex.id = 0;
  switch (ev->_major)
    {
    case CORBA_NO_EXCEPTION:
      retval = GCE_NONE;
      break;
    case CORBA_USER_EXCEPTION:
      retval = GCE_FATAL;
      last_ex.id = g_strdup(CORBA_exception_id(ev));
      break;
    default:
      retval = GCE_FATAL;
      last_ex.id = g_strdup(CORBA_exception_id(ev));
      break;
    }

  CORBA_exception_free(ev);

  return retval;
}

const gchar* 
bonoguppi_corba_strerror()
{
  if (last_ex.id) return last_ex.id;
  else return _("(no error recorded)");
}
