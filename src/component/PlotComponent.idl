// This is just a draft; it includes Gnumeric stuff too.
// The real IDL will be split into separate files and 
// moved to another location.

#include <gnome-unknown.idl>
#if 0
#include <GnomeObject.idl>
#include <gnome-factory.idl>
#include <gnome-advise.idl>
#include <gnome-storage.idl>
#include <gnome-persist.idl>
#include <gnome-moniker.idl>
#include <gnome-container.idl>
#include <gnome-client-site.idl>
#include <gnome-component.idl>
#endif

module GNOME {

  module Plot {
    
    typedef sequence<double> DoubleSeq;
    typedef sequence<long>   IntSeq;
    typedef sequence<string> StringSeq;
 
    exception OutOfRange {};
    exception CoercionError {};

    /////////////////////////////////////////////////////////

    interface Vector {
      // Destroy the vector
      void destroy();

      // The column or row label.
      string get_name();
      void   set_name(in string name);

      unsigned long size();

      // the Vector grows to accomodate any data you place in it.

      // Return the values as a sequence of doubles coercing as necessary.
      DoubleSeq get_as_double_seq(in unsigned long start_index, 
				  in unsigned long count)
	raises(OutOfRange,CoercionError);
      
      // Set the values from a sequence of doubles, coercing as necessary.
      void set_from_double_seq(in unsigned long start_index,
			       in DoubleSeq seq)
	raises(CoercionError);
      
      // Return the values as a sequence of ints, coercing as necessary.
      IntSeq get_as_int_seq(in unsigned long start_index, 
			    in unsigned long count)
	raises(OutOfRange,CoercionError);
      
      // Set the values from a sequence of ints, coercing as necessary.
      void set_from_int_seq(in unsigned long start_index, 
			    in IntSeq seq)
	raises(CoercionError);
      
      typedef sequence<string> StringSeq;
      
      // Return the values as a sequence of strings, coercing as necessary.
      StringSeq get_as_string_seq(in unsigned long start_index, 
				  in unsigned long count)
	raises(OutOfRange,CoercionError);
      
      // Set the values from a sequence of string coercing as necessary.
      // If the vector is numeric, and you set it from strings, the strings
      //  had better look like numbers.
      void set_from_string_seq(in unsigned long start_index,
			       in StringSeq seq)
	raises(CoercionError);
    };

    ////////////////////////////////////////////////////

    // Guppi will implement one VectorFactory for each PlotComponent.
    // Vectors can be re-used with the same PlotComponent,
    //  even if you change the plot type.
    // VectorFactory is obtained from PlotComponent::get_vector_factory()

    interface VectorFactory {
      // Creates an empty Vector of numbers for use in a Plot
      Vector create_numeric_vector();

      // Creates a vector of strings
      Vector create_string_vector();
    };

    ///////////////////////////////////////////////////

    // Base class; all plots can do this
    // A PlotState is obtained from a PlotComponent, by setting the 
    // type of the component
    
    // It's called PlotState to make clear its relationship to a 
    // PlotView and a PlotComponent. PlotComponent has one PlotState
    // at a time; the PlotState holds information specific to the type of 
    //  plot being displayed. PlotView has an associated GnomeCanvas to 
    //  be embedded in applications.

    interface PlotState {    
      // The title of the plot; there will be some default
      //  based on the names of the data vectors, if possible.
      string get_title();
      void   set_title(in string title);
    };

    interface TwoDPlot : PlotState {
      // Raises an exception if this vector doesn't 
      //  want to be coerced to double.
      void set_x_data(in Vector value)
	raises (CoercionError);

      void set_y_data(in Vector value)
	raises (CoercionError);

      // FIXME
    };

    interface ThreeDPlot : PlotState {
      // FIXME
    };

    interface ScatterPlot : TwoDPlot {
      // FIXME
    };

    // a "CategoricalPlot" shows categorical data with a value for
    // each category. Bar charts, pie charts, and dot plots are
    // examples.
    interface CategoricalPlot : PlotState {
      // Must be a vector of strings
      void set_categorical_data(in Vector value)
        raises (CoercionError);

      // Must be a vector of numbers; should 
      //  have an element for each category (but we will
      //  try to do something sensible if not)
      void set_scalar_data(in Vector value)
        raises (CoercionError);
    };

    interface BarPlot : CategoricalPlot {
      // FIXME
    };

    //////////////////////////////////////// 

    // This is the main interface Guppi presents
    
    interface PlotComponent : GNOME::unknown {

      exception UnknownPlotType {};

      // Imagine that plot types are represented with magic strings
      //  like "ScatterPlot" and "BarChart"; could also use 
      //  an enum if you want.

      // Ask if the plot type is supported by the component
      boolean plot_type_supported(in string plot_type);

      // Set the subclass of PlotState to display;
      // implicitly creates a new PlotState instance
      //  and destroys any old PlotState instance
      // On the client side, you have to CORBA_Object_release()
      // any references you held to the old PlotState of course
      void    set_plot_type(in string plot_type)
        raises (UnknownPlotType);

      // Return the current plot instance; 
      //  will be of the type specified with set_plot_type(),
      //  or the type specified when creating the PlotComponent.
      PlotState get_plot();

      // Return the VectorFactory for use with this component
      VectorFactory get_vector_factory();

    };

  };
  
};






