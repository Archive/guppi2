// -*- C++ -*-

/* 
 * plot-component.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "plot-component.h"
#include "plot-types.h"
#include "corba-util.h"
#include <libguppi/plotstate.h>

PlotView::PlotView(PlotComponent* owner)
  : owner_(owner), plot_(0), canvas_(0)
{
  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  canvas_ = gnome_canvas_new_aa ();
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();
}

PlotView::~PlotView()
{
  if (plot_) 
    {
      plot_->plot_model.remove_view(this);
      plot_->unrealize();
      if (plot_->unref() == 0)
        delete plot_;
    }
  // FIXME Should we destroy canvas_, assume it's done, or connect to "destroy" 
  //  and decide on that basis?
}

GtkWidget* 
PlotView::widget()
{
  return canvas_;
}

void 
PlotView::set_plot(Plot* p)
{
  g_return_if_fail(p != 0);

  // To change the type of the plot, we need to unrealize the old 
  // plot and install this one.

  if (plot_ != 0)
    {
      plot_->plot_model.remove_view(this);
      plot_->unrealize();
      if (plot_->unref() == 0)
        delete plot_;

      plot_ = 0;
    }

  g_return_if_fail(plot_ == 0);

  plot_ = p;

  plot_->ref();                         

  plot_->plot_model.add_view(this);

  plot_->realize(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas_)->root));

  change_size(plot_);
  change_name(plot_, plot_->name());
}

////////////////////////// 

void 
PlotView::change_size(Plot* p)
{
  g_return_if_fail(p == plot_);

  g_debug("View %p got change size, %g x %g", this, plot_->width(), plot_->height());

  gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas_), 0, 0, plot_->width(), plot_->height());
}

void 
PlotView::change_status(Plot* p, const string& status)
{
  // Here the statusbar or something should be set to "status.c_str()" if possible.
  // e.g.  gnome_appbar_set_status(GNOME_APPBAR(appbar_), status.c_str());
}

void 
PlotView::change_name(Plot* p, const string& name)
{
  // here you could set the window title or something to "name.c_str()"
}

void 
PlotView::destroy_model()
{
  // This happens when the plot_ is destroyed....
  // not supposed to happen without destroying the view also.
  plot_ = 0;  
  g_warning("Plot destroyed from underneath PlotView!");
  g_assert_not_reached();
}


///////////////////////////

PlotComponent::PlotComponent(const char* plot_type)
  : plot_type_(""), datastore_(0), state_(0), state_object_(CORBA_OBJECT_NIL)
{
  datastore_ = new DataStore;

  if (!set_plot_type(plot_type))
    g_warning("Unknown plot type!");
}

PlotComponent::~PlotComponent()
{
  vector<PlotView*>::iterator i = views_.begin();
  while (i != views_.end())
    {
      delete *i;
      ++i;
    }

  release_state();
  
  delete datastore_;
}

PlotView* 
PlotComponent::new_view()
{
  PlotView* pv = new PlotView(this);
      
  Plot* p = new_plot_of_current_type();
  
  g_return_val_if_fail(p != 0, 0);
  
  pv->set_plot(p);
  
  views_.push_back(pv);
  
  return pv;
}
  
void 
PlotComponent::destroy_view(PlotView* view)
{
  vector<PlotView*>::iterator i = views_.begin();
  while (i != views_.end())
    {
      if (*i == view)
        break;

      ++i;
    }

  if (i != views_.end())
    {
      views_.erase(i);
    }
  else
    {
      g_warning("View not found! very bad...");
    }

  delete view;
}

bool 
PlotComponent::set_plot_type(const char* plot_type)
{
  const PlotTypeData* ptd = bonoguppi_find_plot_type(plot_type);

  if (ptd == 0)
    return false;

  // Save the type so we know how to create new views.
  plot_type_ = plot_type;

  release_state(); // lose the old state and state_object_

  PlotState* ps = (*ptd->new_func)(datastore_);
  
  grab_state(ps); // sets state_

  g_assert(state_ != 0);  

  // If views already exist, we have to switch them over 
  //  to the new type.
  if (!views_.empty())
    {
      Plot* p = (*ptd->s2p_func)(state_);

      // Fill in the first view with the new Plot, 
      //  then copy that view to all the others.

      vector<PlotView*>::iterator i = views_.begin();
      (*i)->set_plot(p);
      PlotView* previous = *i;
      ++i;
      while (i != views_.end())
        {
          (*i)->set_plot(previous->plot()->new_view());
         previous = *i;
          ++i;
        }
    }

  return true;
}

bool 
PlotComponent::plot_type_supported(const char* plot_type)
{
  return (bonoguppi_find_plot_type(plot_type) != 0);
}

Plot* 
PlotComponent::new_plot_of_current_type()
{
  g_assert(state_ != 0);

  const PlotTypeData* ptd = bonoguppi_find_plot_type(plot_type_.c_str());

  if (ptd == 0)
    return 0;

  Plot* p = (*ptd->s2p_func)(state_);

  return p;
}

PlotState* 
PlotComponent::plot_state()
{
  return state_;
}

void 
PlotComponent::release_state()
{
  if (state_ != 0)
    {
      if (state_object_ != CORBA_OBJECT_NIL)
        {
          PortableServer_Servant servant = servant_from_reference(state_object_);
          if (servant != 0)
            {
              free_servant(servant);
            }
          state_object_ = CORBA_OBJECT_NIL;          
        }

      if (state_->unref() == 0)
        delete state_;

      state_ = 0;
    }
  else 
    g_assert(state_object_ == CORBA_OBJECT_NIL);
}

void 
PlotComponent::grab_state(PlotState* ps)
{
  g_return_if_fail(state_ == 0);

  ps->ref();
  state_ = ps;
}

CORBA_Object 
PlotComponent::state_object()
{
  g_assert(state_ != 0);

  if (state_object_ == CORBA_OBJECT_NIL)
    {
      const PlotTypeData* ptd = bonoguppi_find_plot_type(plot_type_.c_str());

      if (ptd == 0)
        {
          g_warning("Should not happen: don't know my own type.");
          return CORBA_OBJECT_NIL;
        }

      state_object_ = (*ptd->corba_func)(state_);
    }
  
  CORBA_Environment ev;
  CORBA_exception_init(&ev);
  CORBA_Object copy = CORBA_Object_duplicate(state_object_, &ev);
  
  if (ev._major != CORBA_NO_EXCEPTION)
    {
      g_warning("OOOPS! failed to duplicate plot state object in %s", __FUNCTION__);
      CORBA_exception_free(&ev);
    }
  else 
    {
      CORBA_exception_free(&ev);
    }

  return copy;
}
