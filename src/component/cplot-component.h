// -*- C++ -*-

/* 
 * cplot-component.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

//////////// 

// This file and its .c wraps the C++ PlotComponent object with a
// GnomeComponent subclass, and implements the IDL PlotComponent object.
// This is called CPlotComponent to alleviate the confusion somewhat (i.e. 
// the C component object, as opposed to the C++ one).

////////////

#ifndef GUPPI_CORBAGLUE_H
#define GUPPI_CORBAGLUE_H

#include <gtk/gtkobject.h>
#include <gtk/gtkwidget.h>
#include <bonobo/gnome-unknown.h>

#include "plot-component.h"

#define CPLOT_COMPONENT_TYPE        (cplot_component_get_type ())
#define CPLOT_COMPONENT(o)          (GTK_CHECK_CAST ((o), CPLOT_COMPONENT_TYPE, CPlotComponent))
#define CPLOT_COMPONENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), CPLOT_COMPONENT_TYPE, CPlotComponentClass))
#define CPLOT_IS_COMPONENT(o)       (GTK_CHECK_TYPE ((o), CPLOT_COMPONENT_TYPE))
#define CPLOT_IS_COMPONENT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), CPLOT_COMPONENT_TYPE))

struct _CPlotComponent;
typedef struct _CPlotComponent CPlotComponent;
					
struct _CPlotComponent {
  GnomeUnknown component;

  PlotComponent* pc;

  gpointer priv;
};

typedef struct {
  GnomeUnknownClass parent_class;

} CPlotComponentClass;

GtkType         cplot_component_get_type  (void);
CPlotComponent *cplot_component_new       (const char* default_plot_type);

#endif


