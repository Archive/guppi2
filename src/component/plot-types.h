// -*- C++ -*-

/* 
 * plot-types.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_PLOTTYPES_H
#define GUPPI_PLOTTYPES_H

// Don't use this directly; it's used in plot-component.cc

#include <libguppi/plot.h>
extern "C" {
#include <bonobo/gnome-main.h>
}

typedef PlotState* (* NewPlotFunc) (DataStore* ds);
typedef Plot* (* State2PlotFunc) (PlotState* ps);
typedef CORBA_Object (* CORBAWrapFunc) (PlotState* ps);

typedef struct _PlotTypeData PlotTypeData;
struct _PlotTypeData {
  const char* name;
  NewPlotFunc new_func;     // new plot state
  State2PlotFunc s2p_func;  // new plot to display state
  CORBAWrapFunc corba_func; // corba wrapper for state
};

const PlotTypeData* bonoguppi_find_plot_type(const char* name);



#endif
