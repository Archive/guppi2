// -*- C++ -*-

/* 
 * scm_data.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/data.h"
#include "libguppi/datastore.h"
#include "libguppi/scalardata.h"

#include "scm_guppi.h"

///////////////////// Data objects

static long data_type_tag;

#define SCM_TO_DATA(obj) (reinterpret_cast<Data*>(SCM_CDR(obj)))
#define DATA_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == data_type_tag)

bool     
scm_datap (SCM obj)
{
  return DATA_P(obj);
}

Data* 
scm2data (SCM obj)
{
  if (!DATA_P(obj)) return 0;
  return SCM_TO_DATA(obj);
}

SCM      
data2scm (Data* d)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, d);
  SCM_SETCAR (smob, data_type_tag);

  // We do this on ref and unref, even though 
  //  we may be only a handle. 
  // This at least maintains sanity.
  scm_done_malloc(d->byte_size());

  d->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_data (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_data (SCM obj)
{
  Data* d = SCM_TO_DATA(obj);

  // We do the size thing even if we aren't actually an allocation.
  // Otherwise there's no way to know when to do it.
  // It also gets bumped for every ref.
  scm_sizet size = d->byte_size();

  gh_defer_ints();
  if (d->unref() == 0)
    {
      delete d;
    }
  gh_allow_ints();

  return size;
}

static int
print_data (SCM obj, SCM port, scm_print_state *pstate)
{
  Data* d = SCM_TO_DATA(obj);

  string rep;

  rep += "#< Data `";
  rep += d->name();
  rep += "' (";
  char buf[128];
  sprintf(buf, "%u", d->size());
  rep += buf;
  rep += " elements)>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns data_funcs = {
  mark_data, 
  free_data, 
  print_data, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on Data


GUPPI_PROC(datap,"guppi-data?",1,0,0,(SCM data))
{
  return gh_bool2scm(DATA_P(data));
}

GUPPI_PROC(data_is_scalar, "guppi-data-scalar?",1,0,0,(SCM data))
{
  SCM_ASSERT(DATA_P(data), data, SCM_ARG1, "guppi-data-scalar?");

  return gh_bool2scm((SCM_TO_DATA(data)->is_a(Data::Scalar)));
}

GUPPI_PROC(data_size,"guppi-data-size",1,0,0,(SCM data))
{
  SCM_ASSERT(DATA_P(data), data, SCM_ARG1, "guppi-data-size");
 
  gh_defer_ints();

  Data* d = SCM_TO_DATA(data);

  gsize sz = d->size();

  gh_allow_ints();

  return gh_int2scm((int)sz);
}


GUPPI_PROC(data_name,"guppi-data-name",1,0,0,(SCM data))
{
  SCM_ASSERT(DATA_P(data), data, SCM_ARG1, "guppi-data-name");
 
  gh_defer_ints();

  Data* d = SCM_TO_DATA(data);

  const string & nm = d->name();

  gh_allow_ints();

  return gh_str02scm(const_cast<char*>(nm.c_str()));
}


GUPPI_PROC(set_data_name,"guppi-data-set-name!",2,0,0,(SCM data, SCM name))
{
  SCM_ASSERT(DATA_P(data), data, SCM_ARG1, "guppi-data-set-name!");
  SCM_ASSERT(gh_string_p(name), name, SCM_ARG2, "guppi-data-set-name!");
 
  gh_defer_ints();

  Data* d = SCM_TO_DATA(data);

  char* nm = gh_scm2newstr(name, NULL);

  d->set_name(nm);

  free(nm);

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

//////////////////////// ScalarData wrapper

static long scalardata_type_tag;

#define SCM_TO_SCALARDATA(obj) (reinterpret_cast<ScalarData*>(SCM_CDR(obj)))
#define SCALARDATA_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == scalardata_type_tag)

bool     
scm_scalardatap (SCM obj)
{
  return SCALARDATA_P(obj);
}

ScalarData* 
scm2scalardata (SCM obj)
{
  if (!SCALARDATA_P(obj)) return 0;
  return SCM_TO_SCALARDATA(obj);
}

SCM      
scalardata2scm (ScalarData* d)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, d);
  SCM_SETCAR (smob, scalardata_type_tag);

  // We do this on ref and unref, even though 
  //  we may be only a handle. 
  // This at least maintains sanity.
  scm_done_malloc(d->byte_size());

  d->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_scalardata (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_scalardata (SCM obj)
{
  ScalarData* d = SCM_TO_SCALARDATA(obj);

  // We do the size thing even if we aren't actually an allocation.
  // Otherwise there's no way to know when to do it.
  // It also gets bumped for every ref.
  scm_sizet size = d->byte_size();

  gh_defer_ints();
  if (d->unref() == 0)
    {
      delete d;
    }
  gh_allow_ints();

  return size;
}

static int
print_scalardata (SCM obj, SCM port, scm_print_state *pstate)
{
  ScalarData* d = SCM_TO_SCALARDATA(obj);

  string rep;

  rep += "#< Scalar Data `";
  rep += d->name();
  rep += "' (";
  char buf[128];
  sprintf(buf, "%u", d->size());
  rep += buf;
  rep += " elements)>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns scalardata_funcs = {
  mark_scalardata, 
  free_scalardata, 
  print_scalardata, 
  0 // means we can never be equal? 
};


/////////////////// Scalar Data operations


GUPPI_PROC(scalardatap,"guppi-scalardata?",1,0,0,(SCM data))
{
  return gh_bool2scm(SCALARDATA_P(data));
}

GUPPI_PROC(scalardata_to_data,"guppi-scalardata->data",1,0,0,(SCM data))
{
  SCM_ASSERT(SCALARDATA_P(data), data, SCM_ARG1, "guppi-scalardata->data");
 
  gh_defer_ints();

  ScalarData* d = SCM_TO_SCALARDATA(data);

  SCM upcast = data2scm(d);

  gh_allow_ints();

  return upcast;
}

GUPPI_PROC(data_to_scalardata,"guppi-data->scalardata",1,0,0,(SCM data))
{
  SCM_ASSERT(DATA_P(data), data, SCM_ARG1, "guppi-data->scalardata");
 
  gh_defer_ints();

  Data* d = SCM_TO_DATA(data);

  if (d->is_a(Data::Scalar))
    {
      gh_allow_ints();
      guppi_scm_error("guppi-data->scalardata", _("Invalid conversion to Scalar Data (Data object does not contain scalars)")); 
      return SCM_UNSPECIFIED; // not reached, I think
    }

  ScalarData* sd = d->cast_to_scalar();

  g_assert(sd != 0);

  SCM scalar = scalardata2scm(sd);

  gh_allow_ints();

  return scalar;
}

GUPPI_PROC(scalardata_checkout,"guppi-scalardata-checkout-realset",1,0,0,(SCM data))
{
  SCM_ASSERT(SCALARDATA_P(data), data, SCM_ARG1, "guppi-scalardata-checkout-realset");
 
  gh_defer_ints();

  ScalarData* d = SCM_TO_SCALARDATA(data);

  RealSet* rs = d->checkout_realset();

  gh_allow_ints();

  if (rs == 0)
    {
      guppi_scm_tagged_error("already-checked-out", "guppi-scalardata-checkout-realset", _("RealSet checked out by someone else."));
      return SCM_UNSPECIFIED; // not reached
    }
  else 
    {
      return realset2scm_handle(rs);
    }
}

GUPPI_PROC(scalardata_checkin,"guppi-scalardata-checkin-realset", 2,2,0,(SCM data, SCM realset, SCM values_changed, SCM other_changed))
{
  SCM_ASSERT(SCALARDATA_P(data), data, SCM_ARG1, "guppi-scalardata-checkin-realset");
  SCM_ASSERT(scm_realsetp(realset), realset, SCM_ARG2, "guppi-scalardata-checkin-realset");
  if (!SCM_UNBNDP(values_changed))
    SCM_ASSERT(gh_boolean_p(values_changed), values_changed, SCM_ARG3, "guppi-scalardata-checkin-realset");

  if (!SCM_UNBNDP(other_changed))
    SCM_ASSERT(gh_boolean_p(other_changed), other_changed, SCM_ARG4, "guppi-scalardata-checkin-realset");


  gh_defer_ints();

  bool vc = true, oc = false;

  if (!SCM_UNBNDP(values_changed))
    vc = gh_scm2bool(values_changed);
  
  if (!SCM_UNBNDP(other_changed))
    oc = gh_scm2bool(other_changed);

  RealSet* rs = scm2realset(realset);
  ScalarData* d = SCM_TO_SCALARDATA(data);
  
  if (d->realset() != rs)
    {
      gh_allow_ints();
      guppi_scm_tagged_error("realset-scalardata-mismatch", 
                             "guppi-scalardata-checkin-realset",
                             _("Attempt to checkin a realset that doesn't belong to this scalardata object"));
    }
  else 
    {
      d->checkin_realset(rs, vc, oc);
      gh_allow_ints();
    }
  
  // FIXME we need to enforce that the realset is not used after this 

  return SCM_UNSPECIFIED;
}



//////////////// Init

void
init_scm_data()
{
  data_type_tag = scm_newsmob(&data_funcs);
  scalardata_type_tag = scm_newsmob(&scalardata_funcs);

#include "scm_data.x"
}
