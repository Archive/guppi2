// -*- C++ -*-

/* 
 * scm_guppi.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 * Portions Copyright (C) 1998 Maciej Stachowiak and Greg J. Badros
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "scm_guppi.h"
#include <goose/guilegoose.h>

void 
guppi_scm_tagged_error(const char* tag, const char *subr, const char *msg)
{
  scm_error(gh_symbol2scm(const_cast<char*>(tag)), const_cast<char*>(subr), 
            "%s",
	    gh_list(gh_str02scm(const_cast<char*>(msg)), SCM_UNDEFINED),
	    gh_list(gh_str02scm(const_cast<char*>(msg)), SCM_UNDEFINED));
}

void 
guppi_scm_error(const char *subr, const char *msg)
{
  guppi_scm_tagged_error("guppi-error", subr, msg);
}

// This docs stuff is experimental, I don't know 
// if it will hurt efficiency too much or otherwise suck

GUPPI_SYMBOL(docstring_symbol, "docstring");

GUPPI_PROC(set_docs,"guppi-set-docs!",2,0,0,(SCM obj, SCM docs))
{
  SCM_ASSERT(gh_string_p(docs), docs, SCM_ARG2, "guppi-set-docs!");

  if (gh_procedure_p(obj))
    {
      scm_set_procedure_property_x(obj, docstring_symbol, docs); 
    }
  else 
    {
      scm_set_object_property_x(obj, docstring_symbol, docs); 
    }
  
  return SCM_UNSPECIFIED;
}

void 
guppi_set_docs(SCM obj, const char* docs)
{
  set_docs(obj, gh_str02scm(const_cast<char*>(docs))); 
}

// returns a scheme string, or SCM_BOOL_F if no docs
GUPPI_PROC(guppi_get_docs,"guppi-get-docs",1,0,0,(SCM obj))
{
  if (gh_procedure_p(obj))
    {
      return scm_procedure_property(obj, docstring_symbol);
    }
  else 
    {
      return scm_object_property(obj, docstring_symbol);
    }
}

GUPPI_PROC(guppi_help,"guppi-help", 0,1,0,(SCM obj))
{
  if (SCM_UNBNDP(obj))
    {
      // There were no arguments
      return gh_str02scm(_("Type '(guppi-help <proc>)' for help on procedure <proc>.\nTo see a list of Guppi procedures use (apropos \"guppi\").\nGoose procedures are also available."));
    }
  else 
    {
      SCM help = guppi_get_docs(obj);

      if (gh_string_p(help))
        {
          return help;
        }
      else 
        {
          return gh_str02scm(_("No help available for that object."));
        }
    }
}

//////////////////////////// 
// All the below stuff is from SCWM. I don't really understand 
//  Guile internals well enough to know what the hell I'm typing
//  in, but this feature is needed, so...
// Copyright 1998 Maciej Stachowiak and Greg J. Badros
// I did s/scwm/guppi/g just for looks ;-)

// Stuff from guile-compat

// Only 1.3 is supported right now so no configure check
#define HAVE_SCM_THE_LAST_STACK_FLUID 1

#ifdef HAVE_SCM_THE_LAST_STACK_FLUID
extern "C" {
  /* from libguile/fluids.h --07/01/98 gjb */
SCM scm_fluid_ref SCM_P ((SCM fluid));
SCM scm_fluid_set_x SCM_P ((SCM fluid, SCM value));
           }
#define DEREF_LAST_STACK scm_fluid_ref(gh_cdr(scm_the_last_stack_fluid))
#define SET_LAST_STACK(X) scm_fluid_set_x (gh_cdr (scm_the_last_stack_fluid), (X))

#else
#define DEREF_LAST_STACK gh_cdr(scm_the_last_stack_var)
#define SET_LAST_STACK(X) gh_set_cdr_x(scm_the_last_stack_var, (X))
#endif

#undef USE_STACKJMPBUF

struct cwdr_no_unwind_handler_data {
  int run_handler;
  SCM tag, args;
};

static SCM
cwdr_no_unwind_handler (void *data, SCM tag, SCM args)
{
  struct cwdr_no_unwind_handler_data *c = 
    (struct cwdr_no_unwind_handler_data *) data;

  c->run_handler = 1;
  c->tag = tag;
  c->args = args;
  return SCM_UNSPECIFIED;
}

static SCM 
scm_internal_cwdr_no_unwind (scm_catch_body_t body, void *body_data,
			     scm_catch_handler_t handler, void *handler_data,
			     SCM_STACKITEM *stack_start)
{
#ifdef USE_STACKJMPBUF
  scm_contregs static_jmpbuf;
#endif
  int old_ints_disabled = scm_ints_disabled;
  SCM old_rootcont, old_winds;
  struct cwdr_no_unwind_handler_data my_handler_data;
  SCM answer = SCM_UNSPECIFIED;

  /* Create a fresh root continuation.  */
  {
    SCM new_rootcont;
    SCM_NEWCELL (new_rootcont);
    SCM_REDEFER_INTS;
#ifdef USE_STACKJMPBUF
    SCM_SETJMPBUF (new_rootcont, &static_jmpbuf);
#else
    SCM_SETJMPBUF (new_rootcont,
		   scm_must_malloc ((long) sizeof (scm_contregs),
				    "inferior root continuation"));
#endif
    SCM_SETCAR (new_rootcont, scm_tc7_contin);
    SCM_DYNENV (new_rootcont) = SCM_EOL;
    SCM_BASE (new_rootcont) = stack_start;
    SCM_SEQ (new_rootcont) = (SCM)-1; // warning: negative value in unsigned int
#ifdef DEBUG_EXTENSIONS
    SCM_DFRAME (new_rootcont) = 0;
#endif
    old_rootcont = scm_rootcont;
    scm_rootcont = new_rootcont;
    SCM_REALLOW_INTS;
  }

#ifdef DEBUG_EXTENSIONS
  SCM_DFRAME (old_rootcont) = scm_last_debug_frame;
  scm_last_debug_frame = 0;
#endif

  {
    my_handler_data.run_handler = 0;
    answer = scm_internal_catch (SCM_BOOL_T,
				 body, body_data,
				 cwdr_no_unwind_handler, &my_handler_data);
  }

  SCM_REDEFER_INTS;
#ifdef USE_STACKCJMPBUF
  SCM_SETJMPBUF (scm_rootcont, NULL);
#endif
#ifdef DEBUG_EXTENSIONS
  scm_last_debug_frame = SCM_DFRAME (old_rootcont);
#endif
  scm_rootcont = old_rootcont;
  SCM_REALLOW_INTS;
  scm_ints_disabled = old_ints_disabled;

  /* Now run the real handler iff the body did a throw. */
  if (my_handler_data.run_handler)
    return handler (handler_data, my_handler_data.tag, my_handler_data.args);
  else
    return answer;
}

// Stuff from callbacks.c

struct guppi_body_apply_data {
  SCM proc;
  SCM args;
};

SCM 
guppi_handle_error (void *data, SCM tag, SCM throw_args)
{
  SCM port = scm_def_errp;

  // FIXME: figure out what this conditional means.
  if (scm_ilength (throw_args) >= 3)
    {
      SCM stack = DEREF_LAST_STACK;
      SCM subr = gh_car (throw_args);
      SCM message = SCM_CADR (throw_args);
      SCM args = SCM_CADDR (throw_args);

      scm_newline(port);
      scm_display_backtrace (stack, port, SCM_UNDEFINED, SCM_UNDEFINED);
      scm_newline(port);
      scm_display_error (stack, port, subr, message, args, SCM_EOL);
      return SCM_BOOL_F;
    }
  else
    {
      scm_puts ("uncaught throw to ", port);
      scm_prin1 (tag, port, 0);
      scm_puts (": ", port);
      scm_prin1 (throw_args, port, 1);
      scm_putc ('\n', port);
      exit (2);
    }
  return SCM_UNSPECIFIED; // guppi returns error hook results
}

static SCM
guppi_body_apply (void *body_data)
{
  struct guppi_body_apply_data *ad = (struct guppi_body_apply_data *) body_data;
  return gh_apply(ad->proc, ad->args);
}


/* Use scm_internal_cwdr to establish a new dynamic root - this causes
   all throws to be caught and prevents continuations from exiting the
   dynamic scope of the callback. This is needed to prevent callbacks
   from disrupting guppi's flow control, which would likely cause a
   crash. Use scm_internal_stack_catch to save the stack so we can
   display a backtrace. scm_internal_stack_cwdr is the combination of
   both. Note that the current implementation causes three(!) distinct
   catch-like constructs to be used; this may have negative, perhaps
   even significantly so, performance implications. */

struct cwssdr_data
{
  SCM tag;
  scm_catch_body_t body;
  void *data;
  scm_catch_handler_t handler;
};

static SCM
cwssdr_body (void *data)
{
  struct cwssdr_data *d = (struct cwssdr_data *) data;
  return scm_internal_stack_catch (d->tag, d->body, d->data, d->handler, 
				  NULL);
}

SCM
scm_internal_stack_cwdr (scm_catch_body_t body,
			 void *body_data,
			 scm_catch_handler_t handler,
			 void *handler_data,
			 SCM_STACKITEM *stack_item)
{
  struct cwssdr_data d;
  d.tag = SCM_BOOL_T;
  d.body = body;
  d.data = body_data;
  d.handler = handler;
  return scm_internal_cwdr_no_unwind (cwssdr_body, &d, 
                                      handler, handler_data, 
                                      stack_item);
}

SCM
guppi_safe_apply_thunk(SCM proc)
{
  SCM_STACKITEM stack_item;
  struct guppi_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = SCM_EOL;

  return scm_internal_stack_cwdr(guppi_body_apply, &apply_data,
				 guppi_handle_error, (gpointer)"guppi",
				 &stack_item);
}

SCM
guppi_safe_apply (SCM proc, SCM args)
{
  SCM_STACKITEM stack_item;
  struct guppi_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = args;

  return scm_internal_stack_cwdr(guppi_body_apply, &apply_data,
				 guppi_handle_error, (gpointer)"guppi",
				 &stack_item);
}


SCM
guppi_safe_apply_message_only (SCM proc, SCM args)
{
  SCM_STACKITEM stack_item;
  struct guppi_body_apply_data apply_data;

  apply_data.proc = proc;
  apply_data.args = args;

  return scm_internal_cwdr_no_unwind(guppi_body_apply, &apply_data,
                                     scm_handle_by_message_noexit, 
                                     (gpointer)"guppi",
                                     &stack_item);
}


SCM
guppi_safe_call0 (SCM thunk)
{
  return guppi_safe_apply (thunk, SCM_EOL);
}


SCM
guppi_safe_call1 (SCM proc, SCM arg)
{
  /* This means w must cons (albeit only once) on each callback of
     size one - seems lame. */
  return guppi_safe_apply (proc, gh_list(arg, SCM_UNDEFINED));
}

SCM
guppi_safe_call2 (SCM proc, SCM arg1, SCM arg2)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return guppi_safe_apply (proc, gh_list(arg1, arg2, SCM_UNDEFINED));
}

SCM
guppi_safe_call3 (SCM proc, SCM arg1, SCM arg2, SCM arg3)
{
  /* This means w must cons (albeit only once) on each callback of
     size two - seems lame. */
  return guppi_safe_apply (proc, gh_list(arg1, arg2, arg3, SCM_UNDEFINED));
}

/* Slightly tricky - we want to catch errors per expression, but only
   establish a new dynamic root per load operation, as it's perfectly
   OK for a file to invoke a continuation created by a different
   expression in the file as far as guppi is concerned. So we set a
   dynamic root for the whole load operation, but also catch on each
   eval. */


static SCM
guppi_body_eval_x (void *body_data)
{
  SCM expr = *(SCM *) body_data;
  return scm_eval_x (expr);
}


inline static SCM 
guppi_catching_eval_x (SCM expr) {
  return scm_internal_stack_catch (SCM_BOOL_T, guppi_body_eval_x, &expr,
                                   guppi_handle_error, (gpointer)"guppi");
}

inline static SCM 
guppi_catching_load_from_port (SCM port)
{
  SCM expr;
  SCM answer = SCM_UNSPECIFIED;

  while (!SCM_EOF_OBJECT_P(expr = scm_read (port))) {  
    answer = guppi_catching_eval_x (expr);
  }
  scm_close_port (port);

  return answer;
}

static SCM
guppi_body_load (void *body_data)
{
  SCM filename = *(SCM *) body_data;
  SCM port = scm_open_file (filename, gh_str02scm("r"));
  return guppi_catching_load_from_port (port);
}

static SCM
guppi_body_eval_str (void *body_data)
{
  char *string = (char *) body_data;
  SCM port = scm_mkstrport (SCM_MAKINUM (0), gh_str02scm(string), 
			    SCM_OPN | SCM_RDNG, "guppi_eval_str");
  return guppi_catching_load_from_port (port);
}

GUPPI_PROC(safe_load, "guppi-safe-load", 1, 0, 0,
           (SCM fname))
     /** Load file FNAME while trapping and displaying errors.
Each individual top-level-expression is evaluated separately and all
errors are trapped and displayed.  You Should use this procedure if
you need to make sure most of a file loads, even if it may contain
errors. */
{
  SCM_STACKITEM stack_item;
  if (!gh_string_p(fname)) {
    scm_wrong_type_arg(s_safe_load, 1, fname);
  }

  return scm_internal_cwdr_no_unwind(guppi_body_load, &fname,
				     scm_handle_by_message_noexit,
				     (gpointer)"guppi", 
				     &stack_item);
}

SCM 
guppi_safe_load (char *filename)
{
  return safe_load(gh_str02scm(filename));
}

SCM 
guppi_eval_str (const char *exp)
{
  SCM_STACKITEM stack_item;
  return scm_internal_cwdr_no_unwind(guppi_body_eval_str, 
                                     const_cast<char*>(exp),
				     scm_handle_by_message_noexit, 
                                     const_cast<char*>("guppi"), 
				     &stack_item);
}


// These two functions are also in ../corba_guppi.cc, which is lame.

static SCM make_output_strport(char *fname)
{
  return scm_mkstrport(SCM_INUM0, scm_make_string(SCM_MAKINUM(30), 
                                                  SCM_UNDEFINED),
                       SCM_OPN | SCM_WRTNG,
                       fname);
}


static SCM get_strport_string(SCM port)
{
  SCM answer;
  {
    gh_defer_ints();
    answer = scm_makfromstr (SCM_CHARS (gh_cdr (SCM_STREAM (port))),
                             SCM_INUM (gh_car (SCM_STREAM (port))),
                             0);
    gh_allow_ints();
  }
  return answer;
}

// output context is an abstraction for evaling an expression
//  and collecting its output.

struct OutputContext {
  SCM o_port, e_port;
  SCM saved_def_e_port;
};

static void
init_output_context(OutputContext* oc)
{       
  /* Temporarily redirect output and error to string ports. 
     Note that the port setting functions return the current previous
     port. */
  oc->o_port=scm_set_current_output_port(make_output_strport("guile_eval"));
  oc->e_port=scm_set_current_error_port(make_output_strport("guile_eval"));

  /* Workaround for a problem with older Guiles */
  oc->saved_def_e_port = scm_def_errp;
  scm_def_errp = scm_current_error_port();
  

}

static void
get_output(OutputContext* oc,
           SCM val,
           string& output, 
           string& error,
           string& result)
{
  SCM str_val = SCM_UNDEFINED;
  unsigned char *ret, *out, *err;

  str_val=scm_strprint_obj(val);
  ret = (unsigned char *) gh_scm2newstr(str_val, NULL);

  /* restore output and error ports. */
  oc->o_port=scm_set_current_output_port(oc->o_port);
  oc->e_port=scm_set_current_error_port(oc->e_port);
  scm_def_errp = oc->saved_def_e_port;

  /* Retrieve output and errors */
  out = (unsigned char *) gh_scm2newstr(get_strport_string(oc->o_port),
                                        NULL);
  err = (unsigned char *) gh_scm2newstr(get_strport_string(oc->e_port),
                                        NULL);

  if (out != 0)
    output = (const char*)out;
  if (err != 0)
    error = (const char*)err;
  if (ret != 0)
    result = (const char*)ret;

  free(out);
  free(err);
  free(ret);
}

void
guppi_eval_str_with_output(const char* expr, 
                           string& output, 
                           string& error,
                           string& result)
{
  OutputContext oc;
  SCM val;

  init_output_context(&oc);
  
  /* Evaluate the request expression and free it. */
  val = guppi_eval_str(expr);

  get_output(&oc, val, output, error, result);
}

SCM 
guppi_safe_apply_with_output(SCM proc, SCM args, 
                             string& output, string& error, string& result)
{
  OutputContext oc;
  SCM val;
  
  init_output_context(&oc);
  
  val = guppi_safe_apply(proc, args);

  get_output(&oc, val, output, error, result);

  return val;
}

SCM 
guppi_safe_apply_thunk_with_output(SCM proc,
                                   string& output, string& error, string& result)
{
  OutputContext oc;
  SCM val;

  init_output_context(&oc);
  
  val = guppi_safe_apply_thunk(proc);

  get_output(&oc, val, output, error, result);

  return val;
}

// These could both be made faster, but this is fine for small enums
bool
guppi_string_to_enum_val(const ScmEnumInfo* info, const char* str, gint* retval)
{
  int i = 0;
  while (i < info->nvals)
    {
      if (g_strcasecmp(info->vals[i].name, str) == 0)
        {
          *retval = info->vals[i].val;
          return true;
        }

      ++i;
    }
  
  return false;
}

bool
guppi_enum_val_to_string(const ScmEnumInfo* info, gint val, const char** retval)
{
  int i = 0;
  while (i < info->nvals)
    {
      if (info->vals[i].val == val)
        {
          *retval = info->vals[i].name;
          return true;
        }

      ++i;
    }
  
  return false;
}

/////////////////////// Init call


// These are here to avoid tons of pointless header files.
extern void init_scm_data();
extern void init_scm_plotutil();
extern void init_scm_marker();
extern void init_scm_xystate();

// Anything registered in here will be in the "Guppi" module
static void 
real_init()
{
  scm_init_goose();

#include "scm_guppi.x"

  init_scm_data();
  init_scm_plotutil();
  init_scm_marker();
  init_scm_xystate();
}

void 
guppi_scm_init ()
{
  //  scm_register_module_xxx("guppi guppi", real_init);

  real_init();
}


