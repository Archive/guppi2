// -*- C++ -*-

/* 
 * scm_xy.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/xyplotstate.h"
#include "libguppi/scalardata.h"
#include "libguppi/markerstyle.h"
#include "libguppi/typeregistry.h"

#include "scm_guppi.h"


///////////////////// Xyplot state smob

static long xy_type_tag;

#define SCM_TO_XY(obj) (reinterpret_cast<XyPlotState*>(SCM_CDR(obj)))
#define XY_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == xy_type_tag)

bool     
scm_xyp (SCM obj)
{
  return XY_P(obj);
}

XyPlotState* 
scm2xy (SCM obj)
{
  if (!XY_P(obj)) return 0;
  return SCM_TO_XY(obj);
}

SCM      
xy2scm (XyPlotState* sc)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, sc);
  SCM_SETCAR (smob, xy_type_tag);

  // Better to report too much than too little.
  // We have to report here because on unref we have to 
  // know how much to unreport.
  scm_done_malloc(sizeof(XyPlotState));

  sc->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_xy (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_xy (SCM obj)
{
  XyPlotState* sc = SCM_TO_XY(obj);

  // We do the size thing even if we aren't actually an allocation.
  // Otherwise there's no way to know when to do it.
  // It also gets bumped for every ref.
  static const scm_sizet size = sizeof(XyPlotState);

  gh_defer_ints();
  if (sc->unref() == 0)
    {
      delete sc;
    }
  gh_allow_ints();

  return size;
}

static int
print_xy (SCM obj, SCM port, scm_print_state *pstate)
{
  XyPlotState* sc = SCM_TO_XY(obj);

  string rep;

  rep += "#<Xy>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns xy_funcs = {
  mark_xy, 
  free_xy, 
  print_xy, 
  0 // means we can never be equal? 
};

static ScmEnumVal position_type_enum[] = {
  { "north", PlotUtil::NORTH },
  { "south", PlotUtil::SOUTH },
  { "east", PlotUtil::EAST },
  { "west", PlotUtil::WEST }
};

static ScmEnumInfo position_type_enum_info = {
  "North-south-east-west position",
  sizeof(position_type_enum)/sizeof(position_type_enum[0]),
  position_type_enum
};

/////////////////////// Operations on Xy

GUPPI_PROC(xyp,"guppi-xy?",1,0,0,(SCM xy))
{
  return gh_bool2scm(XY_P(xy));
}

extern bool scm_scalardatap (SCM obj);
extern ScalarData* scm2scalardata (SCM obj);

GUPPI_PROC(xy_set_start,"guppi-xy-set-start!",3,0,0,(SCM xy, SCM pos, SCM num))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-set-start!");
  SCM_ASSERT(gh_symbol_p(pos), pos, SCM_ARG2, "guppi-xy-set-start!");
  SCM_ASSERT(gh_number_p(num), num, SCM_ARG3, "guppi-xy-set-start!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  
  char* posname = gh_symbol2newstr(pos, NULL);
  gint posval = 0;

  if (!guppi_string_to_enum_val(&position_type_enum_info, posname, &posval))
    {
      free(posname);
      gh_allow_ints();
      guppi_scm_error("guppi-xy-set-start!", _("Bad value for axis position"));
      return SCM_UNSPECIFIED; // not reached
    }

  free(posname);
  
  Axis* ax = sc->checkout_axis((PlotUtil::PositionType)posval);

  if (ax != 0)
    {
      ax->set_start(gh_scm2double(num));
      
      sc->checkin_axis((PlotUtil::PositionType)posval, ax);
    }
  else 
    {
      gh_allow_ints();
      guppi_scm_tagged_error("already-checked-out", "guppi-xy-set-start!",
                             _("Axis is checked out by someone else"));
      return SCM_UNSPECIFIED;
    }
  
  gh_allow_ints();
  return SCM_UNSPECIFIED;
}

GUPPI_PROC(xy_set_stop,"guppi-xy-set-stop!",3,0,0,(SCM xy, SCM pos, SCM num))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-set-stop!");
  SCM_ASSERT(gh_symbol_p(pos), pos, SCM_ARG2, "guppi-xy-set-stop!");
  SCM_ASSERT(gh_number_p(num), num, SCM_ARG3, "guppi-xy-set-stop!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  
  char* posname = gh_symbol2newstr(pos, NULL);
  gint posval = 0;

  if (!guppi_string_to_enum_val(&position_type_enum_info, posname, &posval))
    {
      free(posname);
      gh_allow_ints();
      guppi_scm_error("guppi-xy-set-stop!", _("Bad value for axis position"));
      return SCM_UNSPECIFIED; // not reached
    }

  free(posname);
  
  Axis* ax = sc->checkout_axis((PlotUtil::PositionType)posval);

  if (ax != 0)
    {
      ax->set_stop(gh_scm2double(num));
      
      sc->checkin_axis((PlotUtil::PositionType)posval, ax);
    }
  else 
    {
      gh_allow_ints();
      guppi_scm_tagged_error("already-checked-out", "guppi-xy-set-stop!",
                             _("Axis is checked out by someone else"));
      return SCM_UNSPECIFIED;
    }
  
  gh_allow_ints();
  return SCM_UNSPECIFIED;
}

GUPPI_PROC(xy_set_width,"guppi-xy-set-width!",2,0,0,(SCM xy, SCM num))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-set-width!");
  SCM_ASSERT(gh_number_p(num), num, SCM_ARG2, "guppi-xy-set-width!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  

  double w = gh_scm2double(num);
  if (w < 0.0)
    {
      gh_allow_ints();
      guppi_scm_error("guppi-xy-set-width!", _("Xy width must be positive"));
      return SCM_UNSPECIFIED; // not reached
    }
  
  sc->set_width(w);

  gh_allow_ints();
  return SCM_UNSPECIFIED;
}


GUPPI_PROC(xy_set_height,"guppi-xy-set-height!",2,0,0,(SCM xy, SCM num))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-set-height!");
  SCM_ASSERT(gh_number_p(num), num, SCM_ARG2, "guppi-xy-set-height!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  

  double h = gh_scm2double(num);
  if (h < 0.0)
    {
      gh_allow_ints();
      guppi_scm_error("guppi-xy-set-height!", _("Xy height must be positive"));
      return SCM_UNSPECIFIED; // not reached
    }
    
  sc->set_height(h);

  gh_allow_ints();
  return SCM_UNSPECIFIED;
}

#if 0

/// Sharing stuff between xys

GUPPI_PROC(xy_share_x_axis,"guppi-xy-share-x-axis!",2,0,0,(SCM donor, SCM recipient))

{  
  SCM_ASSERT(XY_P(donor), donor, SCM_ARG1, "guppi-xy-share-x-axis!");
  SCM_ASSERT(XY_P(recipient), recipient, SCM_ARG2, "guppi-xy-share-x-axis!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(donor);  
  XyPlotState* rc = SCM_TO_XY(recipient);

  sc->share_x_axis_with(rc);

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


GUPPI_PROC(xy_share_y_axis,"guppi-xy-share-y-axis!",2,0,0,(SCM donor, SCM recipient))

{  
  SCM_ASSERT(XY_P(donor), donor, SCM_ARG1, "guppi-xy-share-y-axis!");
  SCM_ASSERT(XY_P(recipient), recipient, SCM_ARG2, "guppi-xy-share-y-axis!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(donor);  
  XyPlotState* rc = SCM_TO_XY(recipient);

  sc->share_y_axis_with(rc);

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


GUPPI_PROC(xy_share_markers,"guppi-xy-share-markers!",2,0,0,(SCM donor, SCM recipient))

{  
  SCM_ASSERT(XY_P(donor), donor, SCM_ARG1, "guppi-xy-share-markers!");
  SCM_ASSERT(XY_P(recipient), recipient, SCM_ARG2, "guppi-xy-share-markers!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(donor);  
  XyPlotState* rc = SCM_TO_XY(recipient);

  sc->share_markers_with(rc);

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


GUPPI_PROC(xy_hoard_markers,"guppi-xy-hoard-markers!",1,0,0,(SCM xy))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-hoard-markers!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  

  sc->hoard_markers();

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


GUPPI_PROC(xy_hoard_x_axis,"guppi-xy-hoard-x-axis!",1,0,0,(SCM xy))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-hoard-x-axis!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  

  sc->hoard_x_axis();

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}


GUPPI_PROC(xy_hoard_y_axis,"guppi-xy-hoard-y-axis!",1,0,0,(SCM xy))

{  
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-hoard-y-axis!");

  gh_defer_ints();
  XyPlotState* sc = SCM_TO_XY(xy);  

  sc->hoard_y_axis();

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

#endif


extern bool scm_markerstylep(SCM obj);
extern guint32 scm2markerstyle(SCM obj);

///////////// Layer smob

static long xylayer_type_tag;

#define SCM_TO_XYLAYER(obj) (reinterpret_cast<XyLayer*>(SCM_CDR(obj)))
#define XYLAYER_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == xylayer_type_tag)

bool     
scm_xylayerp (SCM obj)
{
  return XYLAYER_P(obj);
}

XyLayer* 
scm2xylayer (SCM obj)
{
  if (!XYLAYER_P(obj)) return 0;
  return SCM_TO_XYLAYER(obj);
}

SCM      
xylayer2scm (XyLayer* sc)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, sc);
  SCM_SETCAR (smob, xylayer_type_tag);

  // Better to report too much than too little.
  // We have to report here because on unref we have to 
  // know how much to unreport.
  scm_done_malloc(sizeof(XyLayer));

  sc->ref();

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_xylayer (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_xylayer (SCM obj)
{
  XyLayer* sc = SCM_TO_XYLAYER(obj);

  // We do the size thing even if we aren't actually an allocation.
  // Otherwise there's no way to know when to do it.
  // It also gets bumped for every ref.
  static const scm_sizet size = sizeof(XyLayer);

  gh_defer_ints();
  if (sc->unref() == 0)
    {
      delete sc;
    }
  gh_allow_ints();

  return size;
}

static int
print_xylayer (SCM obj, SCM port, scm_print_state *pstate)
{
  XyLayer* sc = SCM_TO_XYLAYER(obj);

  string rep;

  rep += "#<Xylayer>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns xylayer_funcs = {
  mark_xylayer, 
  free_xylayer, 
  print_xylayer, 
  0 // means we can never be equal? 
};

GUPPI_PROC(layerp,"guppi-xylayer?",1,0,0,(SCM layer))
{
  return gh_bool2scm(XYLAYER_P(layer));
}

GUPPI_PROC(xy_create_scatter_layer, "guppi-xy-make-scatter-layer!",1,0,0,(SCM xy))
{
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-make-scatter-layer!");

  gh_defer_ints();

  XyPlotState* state = scm2xy(xy);

  XyScatter* sc = new XyScatter(state->store());
  SCM layer = xylayer2scm(sc);

  state->add_layer(sc);

  gh_allow_ints();

  return layer;
}

GUPPI_PROC(xy_create_line_layer, "guppi-xy-make-line-layer!",1,0,0,(SCM xy))
{
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-make-line-layer!");

  gh_defer_ints();

  XyPlotState* state = scm2xy(xy);

  XyLine* ln = new XyLine(state->store());
  SCM layer = xylayer2scm(ln);

  state->add_layer(ln);

  gh_allow_ints();

  return layer;
}

GUPPI_PROC(xy_create_price_layer, "guppi-xy-make-pricebar-layer!",1,0,0,(SCM xy))
{
  SCM_ASSERT(XY_P(xy), xy, SCM_ARG1, "guppi-xy-make-pricebar-layer!");

  gh_defer_ints();

  XyPlotState* state = scm2xy(xy);

  XyPriceBars* pb = new XyPriceBars(state->store());
  SCM layer = xylayer2scm(pb);

  state->add_layer(pb);

  gh_allow_ints();

  return layer;
}

GUPPI_PROC(layer_is_scatter,"guppi-xylayer-scatter?",1,0,0,(SCM layer))
{
  SCM_ASSERT(XYLAYER_P(layer), layer, SCM_ARG1, "guppi-xylayer-scatter?");

  XyLayer* xl = SCM_TO_XYLAYER(layer);  

  if (xl->type_registry()->is_a(xl->type(), XyLayer::ScatterLayer))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}

GUPPI_PROC(layer_is_line,"guppi-xylayer-line?",1,0,0,(SCM layer))
{
  SCM_ASSERT(XYLAYER_P(layer), layer, SCM_ARG1, "guppi-xylayer-line?");

  XyLayer* xl = SCM_TO_XYLAYER(layer);  

  if (xl->type_registry()->is_a(xl->type(), XyLayer::LineLayer))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}


GUPPI_PROC(layer_is_pricebars,"guppi-xylayer-pricebars?",1,0,0,(SCM layer))
{
  SCM_ASSERT(XYLAYER_P(layer), layer, SCM_ARG1, "guppi-xylayer-pricebars?");

  XyLayer* xl = SCM_TO_XYLAYER(layer);  

  if (xl->type_registry()->is_a(xl->type(), XyLayer::PriceLayer))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}

GUPPI_PROC(layer_set_x,"guppi-xylayer-set-x!",2,0,0,(SCM layer, SCM xdata))

{  
  SCM_ASSERT(XYLAYER_P(layer), layer, SCM_ARG1, "guppi-xylayer-set-x!");
  SCM_ASSERT(scm_scalardatap(xdata), xdata, SCM_ARG2, "guppi-xylayer-set-x!");

  gh_defer_ints();
  XyLayer* xl = SCM_TO_XYLAYER(layer);  

  if (!xl->type_registry()->is_a(xl->type(), XyLayer::DataLayer))
    {
      gh_allow_ints()
      guppi_scm_error("guppi-xylayer-set-x!", _("Wrong type of layer; can't set X values"));
      return SCM_UNSPECIFIED; // not reached
    }

  XyDataLayer* dl = (XyDataLayer*)xl->cast_to_type(XyLayer::DataLayer);

  g_assert(dl != 0);

  dl->set_x_data(scm2scalardata(xdata));
  
  gh_allow_ints();
  return SCM_UNSPECIFIED;
}


GUPPI_PROC(layer_set_y,"guppi-xylayer-set-y!",2,0,0,(SCM layer, SCM ydata))

{  
  SCM_ASSERT(XYLAYER_P(layer), layer, SCM_ARG1, "guppi-xylayer-set-y!");
  SCM_ASSERT(scm_scalardatap(ydata), ydata, SCM_ARG2, "guppi-xylayer-set-y!");

  gh_defer_ints();
  XyLayer* xl = SCM_TO_XYLAYER(layer);  

  if (!xl->type_registry()->is_a(xl->type(), XyLayer::DataLayer))
    {
      gh_allow_ints()
      guppi_scm_error("guppi-xylayer-set-x!", _("Wrong type of layer; can't set Y values"));
      return SCM_UNSPECIFIED; // not reached
    }

  XyDataLayer* dl = (XyDataLayer*)xl->cast_to_type(XyLayer::DataLayer);

  g_assert(dl != 0);

  dl->set_y_data(scm2scalardata(ydata));
  
  gh_allow_ints();
  return SCM_UNSPECIFIED;
}


GUPPI_PROC(layer_npoints,"guppi-xylayer-npoints",1,0,0,(SCM xylayer))

{  
  SCM_ASSERT(XYLAYER_P(xylayer), xylayer, SCM_ARG1, "guppi-xylayer-npoints");

  gh_defer_ints();
  XyLayer* xl = SCM_TO_XYLAYER(xylayer);  

  if (!xl->type_registry()->is_a(xl->type(), XyLayer::DataLayer))
    {
      gh_allow_ints()
      guppi_scm_error("guppi-xylayer-npoints!", _("Wrong type of layer; can't get number of points"));
      return SCM_UNSPECIFIED; // not reached
    }

  XyDataLayer* dl = (XyDataLayer*)xl->cast_to_type(XyLayer::DataLayer);

  g_assert(dl != 0);

  guint npoints = dl->npoints();

  gh_allow_ints();

  return gh_int2scm(npoints);
}

GUPPI_PROC(layer_set_global_style,"guppi-xylayer-set-global-style!",2,0,0,(SCM xylayer, SCM markerstyle))
{
  SCM_ASSERT(XYLAYER_P(xylayer), xylayer, SCM_ARG1, "guppi-xylayer-set-global-style!");
  SCM_ASSERT(scm_markerstylep(markerstyle), markerstyle, SCM_ARG2, "guppi-xylayer-set-global-style!");

  gh_defer_ints();

  XyLayer* xl = SCM_TO_XYLAYER(xylayer);  

  if (!xl->type_registry()->is_a(xl->type(), XyLayer::ScatterLayer))
    {
      gh_allow_ints()
      guppi_scm_error("guppi-xylayer-set-global-style!", _("Wrong type of layer; can't set style"));
      return SCM_UNSPECIFIED; // not reached
    }

  XyScatter* sc = (XyScatter*)xl->cast_to_type(XyLayer::ScatterLayer);

  g_assert(sc != 0);

  guint32 id = scm2markerstyle(markerstyle);

  MarkerPalette* palette = guppi_marker_palette();

  const MarkerStyle* ms = palette->get(id);

  if (ms == 0)
    {
      gh_allow_ints();
      guppi_scm_tagged_error("markerstyle-invalid", "guppi-xylayer-set-global-style!", 
                             "This MarkerStyle is no longer in the marker palette - perhaps you removed it?");
      return SCM_UNSPECIFIED; // not reached
    }

  sc->set_global_style(id);

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

// This is pretty much inherently really slow.
// But maybe it could be improved.
GUPPI_PROC(xylayer_set_styles, "guppi-xylayer-set-points-style!",3,0,0,(SCM xylayer, SCM markerstyle, SCM points))
{
  SCM_ASSERT(XYLAYER_P(xylayer), xylayer, SCM_ARG1, "guppi-xylayer-set-points-style!");
  SCM_ASSERT(scm_markerstylep(markerstyle), markerstyle, SCM_ARG2, "guppi-xylayer-set-points-style!");
  SCM_ASSERT(gh_vector_p(points), points, SCM_ARG3, "guppi-xylayer-set-points-style!");

  gh_defer_ints();

  XyLayer* xl = SCM_TO_XYLAYER(xylayer);  

  if (!xl->type_registry()->is_a(xl->type(), XyLayer::ScatterLayer))
    {
      gh_allow_ints()
      guppi_scm_error("guppi-xylayer-set-global-style!", _("Wrong type of layer; can't set style"));
      return SCM_UNSPECIFIED; // not reached
    }

  XyScatter* sc = (XyScatter*)xl->cast_to_type(XyLayer::ScatterLayer);

  g_assert(sc != 0);
  
  gulong N = gh_vector_length(points);

  if (N == 0)
    {
      // Nothing to be done.
      return SCM_UNSPECIFIED;
    }

  double* elts = gh_scm2doubles(points);
  
  vector<guint> toset;
  guint npoints = sc->npoints();

  gulong i = 0;
  while (i < N)
    {
      if (elts[i] >= npoints)
        {
          gh_allow_ints();
          free(elts);
          guppi_scm_tagged_error("point-out-of-range", "guppi-xylayer-set-points-style!",
                                 "One of the points in the vector is not in the xylayer");
          return SCM_UNSPECIFIED; // not reached
        }

      toset.push_back(elts[i]);
      ++i;
    }

  free(elts);

  guint32 id = scm2markerstyle(markerstyle);

  MarkerPalette* palette = guppi_marker_palette();

  const MarkerStyle* ms = palette->get(id);

  if (ms == 0)
    {
      gh_allow_ints();
      guppi_scm_tagged_error("markerstyle-invalid", "guppi-xylayer-set-points-style!", 
                             "This MarkerStyle is no longer in the marker palette - perhaps you removed it?");
      return SCM_UNSPECIFIED; // not reached
    }

  sc->set_style(toset, id);
  
  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

//////////////// Init

void
init_scm_xystate()
{
  xy_type_tag = scm_newsmob(&xy_funcs);
  xylayer_type_tag = scm_newsmob(&xylayer_funcs);

#include "scm_xystate.x"
}
