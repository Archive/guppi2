// -*- C++ -*-

/* 
 * scm_plotutil.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/plotutils.h"

#include "scm_guppi.h"


///////////////////// Scheme for the PlotUtil stuff


//////// Axis

static long axis_type_tag;

#define SCM_TO_AXIS(obj) (reinterpret_cast<Axis*>(SCM_CDR(obj)))
#define AXIS_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == axis_type_tag)

bool     
scm_axisp (SCM obj)
{
  return AXIS_P(obj);
}

Axis* 
scm2axis (SCM obj)
{
  if (!AXIS_P(obj)) return 0;
  return SCM_TO_AXIS(obj);
}

SCM      
axis2scm (Axis* ax)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, ax);
  SCM_SETCAR (smob, axis_type_tag);

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_axis (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_axis (SCM obj)
{
  Axis* ax = SCM_TO_AXIS(obj);

  static const scm_sizet size = sizeof(Axis);

  gh_defer_ints();
  delete ax;
  gh_allow_ints();

  return size;
}

static int
print_axis (SCM obj, SCM port, scm_print_state *pstate)
{
  Axis* ax = SCM_TO_AXIS(obj);

  string rep;

  rep += "#<Axis>";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns axis_funcs = {
  mark_axis, 
  free_axis, 
  print_axis, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on Axis

GUPPI_PROC(axisp,"guppi-axis?",1,0,0,(SCM axis))
{
  return gh_bool2scm(AXIS_P(axis));
}


GUPPI_PROC(make_axis,"guppi-make-axis",0,0,0,())
{
  gh_defer_ints();
  Axis* sps = new Axis;
  scm_done_malloc(sizeof(Axis));
  gh_allow_ints();
  return axis2scm(sps);
}


//////// Color

static long color_type_tag;

#define SCM_TO_COLOR(obj) (reinterpret_cast<Color*>(SCM_CDR(obj)))
#define COLOR_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == color_type_tag)

bool     
scm_colorp (SCM obj)
{
  return COLOR_P(obj);
}

Color* 
scm2color (SCM obj)
{
  if (!COLOR_P(obj)) return 0;
  return SCM_TO_COLOR(obj);
}

SCM      
color2scm (Color* col)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, col);
  SCM_SETCAR (smob, color_type_tag);

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_color (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_color (SCM obj)
{
  Color* col = SCM_TO_COLOR(obj);

  static const scm_sizet size = sizeof(Color);

  gh_defer_ints();
  delete col;
  gh_allow_ints();

  return size;
}

static int
print_color (SCM obj, SCM port, scm_print_state *pstate)
{
  Color* col = SCM_TO_COLOR(obj);

  string rep;

  rep += "#<Color ";
  
  char buf[128];
  g_snprintf(buf, 128, "r: %u g: %u b: %u a: %u",
             (int)col->r(), (int)col->g(), (int)col->b(), (int)col->a());

  rep += buf;
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns color_funcs = {
  mark_color, 
  free_color, 
  print_color, 
  0 // means we can never be equal? (FIXME colors should be able to be equal?) 
};

//// Color routines

GUPPI_PROC(colorp,"guppi-color?",1,0,0,(SCM color))
{
  return gh_bool2scm(COLOR_P(color));
}

GUPPI_PROC(make_color,"guppi-make-color",0,0,0,())
{
  gh_defer_ints();
  Color* col = new Color;
  scm_done_malloc(sizeof(Color));
  gh_allow_ints();
  return color2scm(col);
}

GUPPI_PROC(set_color,"guppi-color-set!",5,0,0,(SCM color, SCM red,  SCM green, SCM blue, SCM alpha))
{
  SCM_ASSERT(COLOR_P(color), color, SCM_ARG1, "guppi-color-set!");
  SCM_ASSERT(gh_number_p(red), red, SCM_ARG2, "guppi-color-set!");
  SCM_ASSERT(gh_number_p(green), green, SCM_ARG3, "guppi-color-set!");
  SCM_ASSERT(gh_number_p(blue), blue, SCM_ARG4, "guppi-color-set!");
  SCM_ASSERT(gh_number_p(alpha), alpha, SCM_ARG5, "guppi-color-set!");

  gh_defer_ints();

  Color* col = SCM_TO_COLOR(color);

  int r = gh_scm2int(red);
  int g = gh_scm2int(green);
  int b = gh_scm2int(blue);
  int a = gh_scm2int(alpha);

  if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255 || a < 0 || a > 255)
    guppi_scm_tagged_error("color-out-of-range", "guppi-color-set!", 
                           "RGBA elements must be in the range 0 to 255");

  col->set(r,g,b,a);
  
  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

// FIXME set/get each component, set/get RGBA packed int

//////////////// Init

void
init_scm_plotutil()
{
  axis_type_tag = scm_newsmob(&axis_funcs);
  color_type_tag = scm_newsmob(&color_funcs);

#include "scm_plotutil.x"
}
