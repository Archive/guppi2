// -*- C++ -*-

/* 
 * scm_guppi.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_SCMGUPPI_H
#define GUPPI_SCMGUPPI_H

extern "C" {
#include <stdlib.h>
#include <libguile.h>
#include <guile/gh.h>
}

#include <goose/guilegoose.h>

#include "libguppi/util.h"

// There are ideas from scwm strewn all through here.

// Registers all the scheme stuff in this library.
void guppi_scm_init ();

void guppi_scm_error(const char *subr, const char *msg);
// Tagged errors can be caught by catch; (untagged errors can too, but they 
//  have the generic tag "guppi-error")
void guppi_scm_tagged_error(const char* tag, const char *subr, const char *msg);
SCM guppi_eval_str(const char* exp);
SCM guppi_safe_load (char *filename);

void
guppi_eval_str_with_output(const char* exp, 
                           string& output, 
                           string& error,
                           string& result);

void guppi_set_docs(SCM obj, const char* docs);
// returns a scheme string, or SCM_BOOL_F if no docs
SCM guppi_get_docs(SCM obj); 


SCM guppi_safe_apply (SCM proc, SCM args);
SCM guppi_safe_apply_thunk(SCM proc);
SCM guppi_safe_apply_message_only (SCM proc, SCM args);
SCM guppi_safe_apply_with_output(SCM proc, SCM args, 
                                 string& output, string& error, string& result);
SCM guppi_safe_apply_thunk_with_output(SCM proc,
                                       string& output, string& error, string& result);

extern "C" {
typedef struct _ScmEnumVal {
  char* name;
  gint val;
} ScmEnumVal;

typedef struct _ScmEnumInfo {
  char* enum_name;
  const int nvals;
  ScmEnumVal* vals;
} ScmEnumInfo;
}

// Return true if the lookup succeeded
bool guppi_string_to_enum_val(const ScmEnumInfo* info, const char* str, gint* retval);
bool guppi_enum_val_to_string(const ScmEnumInfo* info, gint val, const char** retval);

// Replace Guile's SCM_PROC with a C++-friendly version (nice casts)
#undef SCM_PROC 
#ifndef SCM_MAGIC_SNARFER
#define SCM_PROC(RANAME, STR, REQ, OPT, VAR, CFN)  \
        static char RANAME[]=STR
#else
#define SCM_PROC(RANAME, STR, REQ, OPT, VAR, CFN)  \
%%%     scm_make_gsubr (RANAME, REQ, OPT, VAR, reinterpret_cast<SCM(*)(...)>(CFN))
#endif

#ifndef SCM_MAGIC_SNARFER
#define GUPPI_PROC(fname,primname, req, opt, var, ARGLIST) \
        SCM_PROC(s_ ## fname, primname, req, opt, var, fname); \
SCM fname ARGLIST
#else
#define GUPPI_PROC(fname,primname, req, opt, var, ARGLIST) \
        SCM_PROC(s_ ## fname, primname, req, opt, var, fname);
#endif

#define GUPPI_SYMBOL(cname,scheme_name) SCM_SYMBOL(cname,scheme_name)
#define GUPPI_GLOBAL_SYMBOL(cname,scheme_name) SCM_GLOBAL_SYMBOL(cname,scheme_name)


/////////////

// Now cram all the stuff that should be in a different file. :-)

class XyPlotState;

bool scm_xyp (SCM obj);

XyPlotState* scm2xy (SCM obj);

SCM xy2scm (XyPlotState* sc);

class Data;

bool scm_datap (SCM obj);

Data* scm2data (SCM obj);

SCM data2scm (Data* d);

#endif
