// -*- C++ -*-

/* 
 * scm_marker.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "libguppi/markerstyle.h"

#include "scm_guppi.h"

///////////////////// smob for a MarkerStyle
///////////////////// This smob encapsulates a palette ID, so we don't have to fool with
///////////////////// memory management

static long markerstyle_type_tag;

#define SCM_TO_MARKERSTYLE(obj) ((guint32)(SCM_CDR(obj)))
#define MARKERSTYLE_P(value)    (SCM_NIMP (value) && SCM_CAR(value) == markerstyle_type_tag)

bool     
scm_markerstylep (SCM obj)
{
  return MARKERSTYLE_P(obj);
}

guint32
scm2markerstyle (SCM obj)
{
  if (!MARKERSTYLE_P(obj)) return 0;
  return SCM_TO_MARKERSTYLE(obj);
}

SCM      
markerstyle2scm (guint32 id)
{
  gh_defer_ints();
  SCM smob;
  SCM_NEWCELL (smob);
  SCM_SETCDR (smob, id);
  SCM_SETCAR (smob, markerstyle_type_tag);

  gh_allow_ints();
  return smob;
}

// This function should mark any SCM objects we reference,
//  at this time none. The return value is automatically marked 
//  by Guile, this is some kind of optimization; so we can
//  return one of the objects.
static SCM
mark_markerstyle (SCM obj)
{
  //  scm_gc_mark (image->name);
  //  return image->update_func;
  return SCM_BOOL_F; // not important
}

static scm_sizet
free_markerstyle (SCM obj)
{
  return 0; // it's just a palette ID
}

static int
print_markerstyle (SCM obj, SCM port, scm_print_state *pstate)
{
  guint32 id = SCM_TO_MARKERSTYLE(obj);

  string rep;

  rep += "#<Marker Palette Entry ";
    
  char buf[128];
  g_snprintf(buf,128,"%u", id);

  rep += buf;
  rep += ">";

  scm_puts (const_cast<char*>(rep.c_str()), port);

  /* non-zero means success */
  return 1;
}

static scm_smobfuns markerstyle_funcs = {
  mark_markerstyle, 
  free_markerstyle, 
  print_markerstyle, 
  0 // means we can never be equal? 
};


/////////////////////// Operations on Markerstyle

GUPPI_PROC(markerstylep,"guppi-markerstyle?",1,0,0,(SCM markerstyle))
{
  return gh_bool2scm(MARKERSTYLE_P(markerstyle));
}


GUPPI_PROC(make_markerstyle,"guppi-make-markerstyle",0,0,0,())
{
  gh_defer_ints();
  MarkerStyle* ms = new MarkerStyle; // don't report this malloc to scheme, we don't delete it

  MarkerPalette* palette = guppi_marker_palette();

  guint32 id = palette->add(ms);

  gh_allow_ints();
  return markerstyle2scm(id);
}

GUPPI_PROC(remove_markerstyle,"guppi-markerstyle-remove",1,0,0,(SCM markerstyle))
{
  SCM_ASSERT(MARKERSTYLE_P(markerstyle), markerstyle, SCM_ARG1, "guppi-markerstyle-remove");

  gh_defer_ints();

  guint32 id = SCM_TO_MARKERSTYLE(markerstyle);

  MarkerPalette* palette = guppi_marker_palette();

  palette->remove(id);

  gh_allow_ints();

  return SCM_UNSPECIFIED;
}

extern bool scm_colorp(SCM color);
extern Color* scm2color (SCM obj);

GUPPI_PROC(set_markerstyle_color,"guppi-markerstyle-set-color!",2,0,0,(SCM markerstyle, SCM color))
{
  SCM_ASSERT(MARKERSTYLE_P(markerstyle), markerstyle, SCM_ARG1, "guppi-markerstyle-set-color!");
  SCM_ASSERT(scm_colorp(color), color, SCM_ARG2, "guppi-markerstyle-set-color!");

  gh_defer_ints();

  guint32 id = SCM_TO_MARKERSTYLE(markerstyle);

  Color* c = scm2color(color);

  MarkerPalette* palette = guppi_marker_palette();

  MarkerStyle* ms = palette->checkout(id);

  if (ms == 0)
    guppi_scm_tagged_error("markerstyle-invalid", "guppi-markerstyle-set-color!", 
                           "This MarkerStyle is checked out by someone else or no longer in the marker palette - perhaps you removed it?");
  
  ms->set_color(*c);

  palette->checkin(ms, id);

  gh_allow_ints();

  return SCM_UNSPECIFIED;  
}

extern SCM color2scm (Color* col);

GUPPI_PROC(get_markerstyle_color,"guppi-markerstyle-get-color",1,0,0,(SCM markerstyle))
{
  SCM_ASSERT(MARKERSTYLE_P(markerstyle), markerstyle, SCM_ARG1, "guppi-markerstyle-get-color");

  gh_defer_ints();

  guint32 id = SCM_TO_MARKERSTYLE(markerstyle);

  MarkerPalette* palette = guppi_marker_palette();

  const MarkerStyle* ms = palette->get(id);

  if (ms == 0)
    guppi_scm_tagged_error("markerstyle-invalid", "guppi-markerstyle-get-color", 
                           "This MarkerStyle is no longer in the marker palette - perhaps you removed it?");
  
  Color* c = new Color;

  scm_done_malloc(sizeof(Color));

  *c = ms->color();

  gh_allow_ints();

  return color2scm(c);
}


GUPPI_PROC(set_markerstyle_name,"guppi-markerstyle-set-name!",2,0,0,(SCM markerstyle, SCM name))
{
  SCM_ASSERT(MARKERSTYLE_P(markerstyle), markerstyle, SCM_ARG1, "guppi-markerstyle-set-name!");
  SCM_ASSERT(gh_string_p(name), name, SCM_ARG2, "guppi-markerstyle-set-name!");

  gh_defer_ints();

  guint32 id = SCM_TO_MARKERSTYLE(markerstyle);

  char* nm = gh_scm2newstr(name, NULL);

  MarkerPalette* palette = guppi_marker_palette();

  MarkerStyle* ms = palette->checkout(id);

  if (ms == 0)
    guppi_scm_tagged_error("markerstyle-invalid", "guppi-markerstyle-set-name!", 
                           "This MarkerStyle is checked out by someone else or no longer in the marker palette - perhaps you removed it?");
  
  ms->set_name(nm);

  free(nm);
  nm = 0;

  palette->checkin(ms, id);

  gh_allow_ints();

  return SCM_UNSPECIFIED;  
}



//////////////// Init

void
init_scm_marker()
{
  markerstyle_type_tag = scm_newsmob(&markerstyle_funcs);

#include "scm_marker.x"
}
